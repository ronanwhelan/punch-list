<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 26/4/16
 * Time: 11:01 AM
 */

namespace app\Rowsys\PunchList;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class Attachment {

    public function uploadPhotoAttachment($file,$file_name,$item_number){

        $fileName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        $fileName = $file_name.'.'.$fileExtension;
        $mimeTypes = ['bmp', 'gif', 'jpeg', 'jpg', 'tif', 'tiff', 'ico',];
        $folderName = '';
        $fileExtension = strtolower($fileExtension);
        if(in_array($fileExtension,$mimeTypes)){
            $image_normal = Image::make($file)->rotate(-90)->widen(800, function ($constraint) {$constraint->upsize();});
            //$image_normal = Image::make($file)->resize(600, null);
            //$image_thumb = Image::make($file)->resize(200, 150);
            // $image_thumb = Image::make($file)->crop(100,100);
            $image_normal = $image_normal->stream();
            //$image_thumb = $image_thumb->stream();
            $folderName = 'project-1/punchlist/'.$item_number.'/img/';
            Storage::disk('s3')->put('project-1/punchlist/'.$item_number.'/img/'.$fileName, $image_normal->__toString(),'public');

        }else{
            Storage::put('project-1/punchlist/'.$item_number.'/other/'.$fileName, file_get_contents($file->getRealPath()) ,'public');
            $folderName = 'project-1/punchlist/'.$item_number.'/other/';
        }

        $data = [$fileName,$fileExtension,$folderName];

        return $data;

    }
}
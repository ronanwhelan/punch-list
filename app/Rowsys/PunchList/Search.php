<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 30/3/16
 * Time: 3:17 PM
 */

namespace app\Rowsys\PunchList;


use App\Models\PunchList\Item;
use App\Models\Stage;
use App\Models\Task;
use App\Models\UserFilter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Search
{

    protected $request;
    protected $query;

    function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Return the Object Query
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Return the Object Query
     * @return mixed
     */
    public function getQueryToSql()
    {
        $sql = clone $this->query;

        return $sql->toSql();
    }

    /*
     * Build and return the main query
     */
    public function buildAndReturnMainQuery()
    {
        //TODO add project id
        $query = Item::where('project_id', 1);
        $this->getMainFilterQuery($query);

        return $this->query;
    }

    /**
     * Add the users Filter to the Query
     */
    public function addUserFilterToRequest()
    {
/*        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $selectedAreasArray = [$userFilter->filter_area_ids];
        $selectedGroupsArray = [$userFilter->filter_group_ids];
        $selectedTypesArray = [$userFilter->filter_type_ids];
        $selectedStagesArray = [$userFilter->filter_stage_ids];

        if ($userFilter->item_filter_area_ids !== null && $userFilter->item_filter_area_ids !== '') {
          //  $this->request['search_areas'] = explode(',', $selectedAreasArray[0]);
        }
        if ($userFilter->item_filter_group_ids !== null && $userFilter->item_filter_group_ids !== '') {
            //$this->request['search_groups'] = explode(',', $selectedGroupsArray[0]);
        }
        if ($userFilter->item_filter_type_ids !== null && $userFilter->item_filter_type_ids !== '') {
           //$this->request['search_types'] = explode(',', $selectedTypesArray[0]);
        }
        if ($userFilter->item_filter_stage_ids !== null && $userFilter->item_filter_stage_ids !== '') {
           // $this->request['search_stages'] = explode(',', $selectedStagesArray[0]);
        }*/
    }

    /*
     * Get Main Search Filter $query
     * Search Filter that includes Area, System, Group, Task Type and search String
     */
    public function getMainFilterQuery(Builder $query)
    {


        // --- NUMBER TEXT FIELD---
        $search_has_task_number = $this->request->has('search_number');
        if ($search_has_task_number) {
            $searchNumber = $this->request->search_task_number;
        }
        // --- AREAS AND SYSTEMS ---
        $search_has_areas = $this->request->has('search_areas');
        if ($search_has_areas) {
            $searchAreas = $this->request->search_areas;
        }
        $search_has_systems = $this->request->has('search_systems');
        if ($search_has_systems) {
            $searchSystems = $this->request->search_systems;
        }
        // --- GROUPS AND TYPES ---
        $search_has_groups = $this->request->has('search_groups');
        if ($search_has_groups) {
            $searchGroups = $this->request->search_groups;
        }
        $search_has_types = $this->request->has('search_types');
        if ($search_has_types) {
            $searchTypes = $this->request->search_types;
        }
        // --- CATEGORY ---
        $search_has_category = $this->request->has('search_category');
        if ($search_has_category) {
            $searchCategory = $this->request->search_category;
        }

        //===========================================================

        //NUMBER AND DESCRIPTION
        if ($search_has_task_number) {
            $searchText = $this->request->search_number;
            $pieces = explode(" ", $searchText);
            $query = $query->where('number', 'LIKE', '%' . trim($pieces[0]) . '%');
            for ($i = 1; $i < sizeof($pieces); $i++) {
                $query = $query->where('number', 'LIKE', '%' . $pieces[$i] . '%');
            }
            $query = $query->orWhere('finding', 'LIKE', '%' . trim($pieces[0]) . '%');
            for ($i = 1; $i < sizeof($pieces); $i++) {
                $query = $query->where('finding', 'LIKE', '%' . trim($pieces[$i]) . '%');
            }
            $query = $query->orWhere('description', 'LIKE', '%' . trim($pieces[0]) . '%');
            for ($i = 1; $i < sizeof($pieces); $i++) {
                $query = $query->where('description', 'LIKE', '%' . trim($pieces[$i]) . '%');
            }
            $query = $query->orWhere('due_date_updated_note', 'LIKE', '%' . trim($pieces[0]) . '%');
            for ($i = 1; $i < sizeof($pieces); $i++) {
                $query = $query->where('due_date_updated_note', 'LIKE', '%' . trim($pieces[$i]) . '%');
            }
            //old - $searchNumber = '%' . $this->request->search_task_number . '%';$query = $query->where('number', 'like', $searchNumber)->orWhere('description', 'like', $searchNumber);
        }

        //AREA and SYSTEMS
        //has no areas no systems
        if (!$search_has_areas & !$search_has_systems) {
            //Default to ALL
        } else {
            //has areas but no systems
            if ($search_has_areas & !$search_has_systems) {
                $query = $query->whereIn('area_id', $searchAreas);
            } else {
                if ($search_has_systems) {
                    $query = $query->whereIn('system_id', $searchSystems);
                }
            }
        }
        //GROUPS and TYPES
        //has no groups no types
        if (!$search_has_groups & !$search_has_types) {
            //Default to ALL
        } else {
            if ($search_has_types) {
                $query = $query->whereIn('type_id', $searchTypes);
            } else {
                if ($search_has_groups) {
                    $query = $query->whereIn('group_id', $searchGroups);
                }
            }
        }
        //CATEGORIES
        if ($search_has_category) {
            $query = $query->where('category_id', $searchCategory);
        }

        $this->query = $query;

    }

    /*
    * Get Main Complete Filter $query
    * Search Filter that looks at the complete status
    */
    public function addCompleteFilterQuery()
    {
        $search_has_complete = $this->request->has('inlineRadioOptions');
        $completeSelection = $this->request->inlineRadioOptions;
        if ($search_has_complete) {
            if ($completeSelection === "all") {
                $this->query->where('own_status', '<', 3);
            }
            if ($completeSelection === "done") {
                $this->query->where('own_status', '=', 2);
            }
            if ($completeSelection === "not-done") {
                $this->query->where('own_status', '=', 1);
            }

        }
    }



    /*
    * Ignore Items marked forget
    */
    public function addForget()
    {
        //$this->query->where('own_status', '<', 3);
        $this->query->where('forget', 0);
    }

    /*
    * Ignore Items marked forget
    */
    public function addPriority()
    {
        $search_has_priority = $this->request->has('search_priority');
        if($search_has_priority){
            $this->query->where('priority_id', $this->request->search_priority);
        }
    }

    /*
    * Activation Status 0=proposed, 1=accepted , 2=rejected
    */
    public function addActivationStatus()
    {
        $this->query->where('activated_status', $this->request->search_activated_status);
    }


    /*
    * Get Time Query
    * Search Filter based ont eh time range given
    */
    public function addTimeFilterQuery()
    {
        $search_has_from_date_range = $this->request->has('search_from_date');
        $search_has_to_date_range = $this->request->has('search_from_date');
        $search_has_time = $this->request->has('search_time');
        $searchTime = (int)$this->request->search_time;

        if ($searchTime !== 6) {
            $today = Carbon::now();
            if ($search_has_time) {
                if ($searchTime === 1 || $searchTime === 0) {
                    //Default to ALL
                } else {
                    //1 Week
                    if ($searchTime === 2) {
                        $today = $today->addWeeks(1);
                    }
                    //2 weeks
                    if ($searchTime === 3) {
                        $today = $today->addWeeks(2);
                    }
                    //4 Weeks
                    if ($searchTime === 4) {
                        $today = $today->addWeeks(4);
                    }
                    //8 Weeks
                    if ($searchTime === 5) {
                        $today = $today->addWeeks(8);
                    }
                    $this->query->where('due_date', '<', $today);
                }
            }
        } elseif ($searchTime === 6) {
            if ($search_has_from_date_range || ($search_has_to_date_range)) {
                $this->query->where('due_date', '>', $this->request->search_from_date)
                    ->where('due_date', '<', $this->request->search_to_date);

            }
            if ($search_has_from_date_range) {
                // $this->query->where('next_td', '<', $this->request->search_to_date);
            }
        }
    }

    /**
     * Sort By
     */
    public function addOrderBy()
    {
        $search_has_sort_by = $this->request->has('search_order_by');
        if ($search_has_sort_by) {
            $sortBy = (int)$this->request->search_order_by;
            switch ($sortBy) {
                case 1:
                    $this->query->orderBy('due_date', 'ASC');
                    break;
                case 2:
                    $this->query->orderBy('number', 'ASC');
                    break;
                case 3:
                    $this->query->orderBy('area_id', 'ASC');
                    break;
                case 4:
                    $this->query->orderBy('system_id', 'ASC');
                    break;
                case 5:
                    $this->query->orderBy('group_id', 'ASC');
                    break;
                case 6:
                    $this->query->orderBy('type_id', 'ASC');
                    break;
                case 7:
                    $this->query->orderBy('category_id', 'ASC');
                    break;
            }
        }
        //$this->query->orderBy('number', 'ASC');
    }

    /**
     * Check to see if the request has any links e.g from the dashboard
     * if so add the required query
     */
    public function checkForAnyAppLinks()
    {
        if ($this->request->has('items-past-due')) {
            $this->query->addPastDue();
        }
        if ($this->request->has('dashboard_due_in_weeks')) {
            $this->query->addDueInWeeks($this->request->dashboard_due_in_weeks);
        }

        if ($this->request->has('last-seven-days')) {
            $today = Carbon::now();
            $sevenDaysAgo = $today->subDays(7);
            $this->query->where('raised_date','<', $sevenDaysAgo)->where('complete', 0);
        }


        //My Teams Items
        //Responsible
        $user = Auth::user();

        $userRoles = $user->roles->lists('id')->toArray();
        if ($this->request->has('items-team-resp')) {
            $this->query->whereIn('res_role_id', $userRoles)->where('complete', 0);
        }
        //Assigned
        if ($this->request->has('items-team-assigned')) {
            $this->query->whereIn('assigned_role_id', $userRoles)->where('complete', 0);
        }
        //Urgent
        if ($this->request->has('items-team-urgent')) {
            $this->query->where('urgent', 1)->where('complete', 0);
        }

        //My Items
        if ($this->request->has('items-my-resp')) {
            $this->query->where('res_user_id', $user->id)->where('complete', 0);
        }
        //Assigned
        if ($this->request->has('items-my-assigned')) {
            $this->query->where('assigned_user_id', $user->id)->where('complete', 0);
        }

    }

    /**
     * Add Past Due to the Query
     */
    public function addPastDue()
    {
        $today = Carbon::now();
        $this->query->where('next_td', '<', $today)->where('complete', 0);
    }

    /**
     * check search with company ifo
     */
    public function addCompany()
    {
        $userInSearch = 0;
        $userCompanyId = \Auth::user()->company_id;
        // --- Owner Party  ---
        $search_has_owner = $this->request->has('search_raised_by');
        if ($search_has_owner) {
            $searchOwner = $this->request->search_raised_by;

           $this->query->where('raised_by_company_id', $searchOwner);
            if((int)$searchOwner === $userCompanyId ){
                $userInSearch = 1;
            }
        }
       // --- Responsible Party ---
        $search_has_res = $this->request->has('search_responsible');
        if ($search_has_res) {
            $searchRes = $this->request->search_responsible;
           $this->query->where('res_company_id', $searchRes);
            if((int)$searchRes === $userCompanyId ){
                $userInSearch = 1;
            }
        }

        // --- Assigned Party ---
        $search_has_assigned = $this->request->has('search_assignee');
        if ($search_has_assigned) {
            $searchAssignee = $this->request->search_assignee;
             $this->query->where('assigned_company_id', $searchRes);
            if ((int)$searchAssignee === $userCompanyId) {
                $userInSearch = 1;
            }
        }

        if($userInSearch === 0){
            $this->query->where('privacy', 1);
        }

    }

    /**
     * check search with role
     */
    public function addRole()
    {
        $userInSearch = 0;
        $userCompanyId = \Auth::user()->company_id;

        // --- Owner Party  ---
        $search_has_owner = $this->request->has('search_raised_by');
        if ($search_has_owner) {
            $searchOwner = $this->request->search_raised_by;
            $this->query->where('raised_by_role_id', $searchOwner);
        }
        // --- Responsible Party ---
        $search_has_res = $this->request->has('search_responsible');
        if ($search_has_res) {
            $searchRes = $this->request->search_responsible;
            $this->query->where('res_role_id', $searchRes);
        }

        // --- Assigned Party ---
        $search_has_assigned = $this->request->has('search_assignee');
        if ($search_has_assigned) {
            $searchAssignee = $this->request->search_assignee;
            $this->query->where('assigned_role_id', $searchAssignee);
        }

    }



    /**
     * Add Privacy
     */
    public function addPrivacy()
    {
        $this->query->where('privacy', 1);
    }
    /**
     * Add Privacy
     */
    public function orPublic()
    {
        $this->query->orWhere('privacy', 1);
    }

    /**
     * View items tha the user is connected to
     * e.g is the owner, responsible or assigned party
     */
    public function associatedItems()
    {
        $companyId = \Auth::user()->company_id;

        $this->query->where(function ($query) use ($companyId) {
            $query->orWhere('raised_by_company_id', 1)
                ->orWhere('res_company_id', 1)
                ->orWhere('assigned_company_id', 1);
        });
    }

    /**
     * handle user searching with responsible party AND assigned party
     */
    public function searchResAndAssignedParty($responsibleId, $assignedId)
    {
        $userCompanyId = \Auth::user()->company_id;
        $this->query->where('raised_by_company_id', $userCompanyId)
            ->Where('res_company_id', $responsibleId)
            ->Where('assigned_company_id', $assignedId);

    }

    /**
     * handle user searching with responsible party
     */
    public function searchResponsibleParty($companyId)
    {
        $userCompanyId = \Auth::user()->company_id;
        if ($userCompanyId === $companyId) {
            $this->query->Where('res_company_id', $companyId);
        } else {
            $this->query->where('raised_by_company_id', $userCompanyId)
                ->Where('res_company_id', $companyId);
        }

    }

    /**
     * handle user searching with Assigned party
     */
    public function searchAssignedParty($companyId)
    {
        $userCompanyId = \Auth::user()->company_id;
        if ($userCompanyId === $companyId) {
            $this->query->Where('assigned_company_id', $companyId);
        } else {
            $this->query->where('assigned_company_id', $userCompanyId)
                ->Where('res_company_id', $companyId);
        }

    }


    /**
     * Add Past Due to the Query
     */
    public function addDueInWeeks($numOfWeeks)
    {
        $today = Carbon::now();
        $dateInTheFuture = $today->addWeeks((int)$numOfWeeks);
        $today = Carbon::now();
        $this->query->where('next_td', '>', $today)
            ->where('next_td', '<', $dateInTheFuture)
            ->where('complete', 0);
    }

    public function saveLastSearch()
    {
        $user = \Auth::user();
        $userFilterExists = UserFilter::where('user_id', $user->id)->exists();

        if (!$userFilterExists) {
            $userFilter = new UserFilter();
            $userFilter->user_id = $user->id;
            $userFilter->save();
        }
        $userFilterExists = UserFilter::where('user_id', $user->id)->exists();
        if ($userFilterExists) {
            $userFilter = UserFilter::where('user_id', $user->id)->first();

            // --- NUMBER TEXT FIELD---
            $userFilter->search_text = '';
            $search_has_task_number = $this->request->has('search_task_number');
            if ($search_has_task_number) {
                $searchNumber = $this->request->search_task_number;
                $userFilter->item_search_text = $searchNumber;
                //$this->request->session()->put('text', $searchNumber);
            }
            //Areas
            $userFilter->search_area_ids = '';
            //\Session::put('sessionAreas', []);
            $search_has_areas = $this->request->has('search_areas');
            if ($search_has_areas) {
                $searchAreas = $this->request->search_areas;
                $userFilter->item_search_area_ids = implode(',', $searchAreas);
                //\Session::put('sessionAreas', implode(',', $searchAreas));
            }
            //Systems
            $userFilter->search_system_ids = '';
            //\Session::put('sessionSystems', []);
            $search_has_systems = $this->request->has('search_systems');
            if ($search_has_systems) {
                $searchSystems = $this->request->search_systems;
                $userFilter->item_search_system_ids = implode(',', $searchSystems);
            }
            //Groups
            $userFilter->search_group_ids = '';
            $search_has_groups = $this->request->has('search_groups');
            if ($search_has_groups) {
                $searchGroups = $this->request->search_groups;
                $userFilter->item_search_group_ids = implode(',', $searchGroups);
                //\Session::put('sessionGroups', $searchGroups);
            }
            //Types
            $userFilter->search_type_ids = '';
            $search_has_types = $this->request->has('search_types');
            if ($search_has_types) {
                $searchTypes = $this->request->search_types;
                $userFilter->item_search_type_ids = implode(',', $searchTypes);
                //$this->request->session()->put('sessionTypes ', $searchTypes);
            }
            //Stages
            $stages = Stage::get();
            if ($this->request->search_gen === '1') {
                $userFilter->item_search_stage_ids = $stages[0]->id;
            }
            if ($this->request->search_exe === '1') {
                $userFilter->item_search_stage_ids = $stages[1]->id;
            }
            if ($this->request->search_gen === '1' && $this->request->search_exe === '1') {
                $userFilter->item_search_stage_ids = '';
            }

            //Complete
            //$userFilter->search_complete = 0;
            $search_has_complete = $this->request->has('inlineRadioOptions');
            $completeSelection = $this->request->inlineRadioOptions;
            if ($search_has_complete) {
                if ($completeSelection === "done") {
                    //$userFilter->search_complete = 1;
                }
            }

            $userFilter->save();
        }

    }


}
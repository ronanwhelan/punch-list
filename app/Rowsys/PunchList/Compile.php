<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 29/6/16
 * Time: 1:53 PM
 */

namespace App\Rowsys\PunchList;


use App\Jobs\Punchlist\CompileItems;
use App\Models\PunchList\Category;
use App\Models\PunchList\Group;
use App\Models\PunchList\Item;
use App\Models\PunchList\SystemScheduleDates;
use App\Models\PunchList\Type;
use App\Models\ScheduleDate;
use App\Models\System;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Compile
{

    use DispatchesJobs;


    /**
     * ------------------------------------------------
     *    Compile All the Punch List Items
     *  -----------------------------------------------
     *
     *  Updates the target date, status and tag number and description
     */
    public function compileAllItems()
    {
        $compileJob = new CompileItems();
        $this->dispatch($compileJob);
    }


    public function compileItems()
    {
        Item::orderBy('id')->where('priority_id', '<' ,4)->chunk(500, function ($items) {
            foreach ($items as $item) {
                $this->compileItem($item->id);
            }
        });
        $message = "items compiled";

        echo "item compiled";

        return $message;
    }

    public function compileItem($id)
    {
        $item = Item::find($id);

        if ($item->imported === 0) {
            $dateUpdated = $item->due_date_updated;
            if ($dateUpdated === 0) {
                $systemId = $item->system_id;
                $datesLink = SystemScheduleDates::where('system_id', $systemId)->first();
                if (sizeof($datesLink) > 0) {
                    $stepNum = $item->linked_milestone;
                    $columnLink = 'm' . $stepNum . '_schedule_link_number';
                    $columnLag = 'm' . $stepNum . '_lag';
                    $columnChoice = 'm' . $stepNum . '_schedule_choice';
                    $dates = ScheduleDate::where('name', $datesLink->$columnLink)->first();
                    if (sizeof($dates) > 0)  {
                        $item->error = 0;
                        if (strpos($datesLink->$columnChoice, 'tart')) {
                            $dueDate = $dates->start_date;
                        } else {
                            $dueDate = $dates->finish_date;
                        }
                        // ====  Due Date  =====
                        //Remove Lag and Lead
                        $itemLag = $item->lag_days;
                        $calcLag = 0;
                        $linkLead = $datesLink->$columnLag;
                        if ($itemLag < 0) {
                            $calcLag = $itemLag - $linkLead;
                            $dueDate = $dueDate->addDays($calcLag);
                        } else {
                            $calcLag = $itemLag + $linkLead;
                            $dueDate = $dueDate->addDays($calcLag);
                        }
                        $item->due_date = $dueDate;
                        $item->original_due_date = $dueDate;
                        $item->res_target_date = $dueDate;

                    } else {
                        $item->error = 1;
                    }
                }
            }
        }else {

            // echo " compile item";
            //Number
            //Genre
            $category = Category::find($item->category_id);
            $type = Type::find($item->type_id);
            $group = Group::find($type->group_id);
            $system = System::find($item->system_id);
            $item->number = $system->tag . '.' . $category->short_name .'-'.$item->increment_number;
            $item->description = $group->name . ' ' . $type->name . ' on ' . $system->description;

        }

        $item->save();
        //echo "item compiled";
        $message = "item compiled";

        return $message;
    }

}
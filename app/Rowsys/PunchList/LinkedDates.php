<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 23/11/17
 * Time: 12:55 PM
 */

namespace app\Rowsys\PunchList;


use App\Models\Milestone;
use App\Models\PunchList\SystemScheduleDates;
use App\Models\System;
use App\Models\SystemToMilestoneLink;

class LinkedDates
{


    public function __construct( )
    {

    }


    public static function syncLinks($activity){

        $systems = System::get();

        $milestones = Milestone::get();

        foreach ( $systems as $system  ){

            $punchListLinks = SystemScheduleDates::where('system_id',$system->id)->get();

            if( ! isset($punchListLinks) || sizeof($punchListLinks) < 1){
                //create row for system
                SystemScheduleDates::create([
                    'system_id' => $system->id
                ]);
            }


            $loop = 0;
            for ($i = 2; $i < sizeof($milestones); $i++) {//$i+=2 increment by 2

                $columnLink = 'm' . $i . '_schedule_link_number';
                $columnLag = 'm' . $i . '_lag';
                $columnChoice = 'm' . $i . '_schedule_choice';
                $columnStageId = 'm' . $i . '_stage_id';
                $columnTaskId = 'm' . $i . '_task_type_id';

                $mainAppLink = SystemToMilestoneLink::where('system_id',$system->id)
                    ->where('milestone_id',$milestones[$loop] )
                    ->first();

                $activityName = '';
                if($activity !== ''){
                    $activityName = $activity;
                }
                $dateChoice = 'start';
                $bufferDays = 0;

                if($i < 3){
                    $activityName = '';
                }

                if( isset($mainAppLink )){
                    $activityName = $mainAppLink->actitivty_name;
                    $dateChoice = $mainAppLink->date_choice;
                    $bufferDays = $mainAppLink->buffer_days;
                }

                SystemScheduleDates::where('system_id',$system->id)->update([

                    $columnLink => $activityName, //
                    $columnLag => $bufferDays,
                    $columnChoice => $dateChoice,
                    $columnStageId => 1,
                    $columnTaskId => $i,
                ]);

                $loop ++;

            }

            $mainAppLinks = SystemToMilestoneLink::where('system_id',$system->id)->get();

           if(isset($mainAppLinks) && sizeof($mainAppLinks) > 0){
               //update current row for system

           }else{ //create new entry fo system


           }


        }

        return 'im here';


    }









    public static function milestoneDatesForSystem($systemId){

        $data = collect();

        //get the schedule link for the system
        //get the milestones
        //get the config for milestone to stage and type
        $milestones = Milestone::get();

        $links = SystemToMilestoneLink::where('system_id',$systemId)->get();

        $linkedTypeId = 4;

        $calcFinishDate = null;

        foreach ( $milestones as $milestone){

            //check if there is a link for the milestone
            //if so then get the date form the milestone.
        }

        $data = [
            'calcTargetDate' => $calcFinishDate,
            'dates' => [
                ['milestone' => 'M1', 'Date' => '2018-09-05']
            ]
        ];

        //loop over the milestones - if there is a type then get the
        //date for that milestone - push to the return array




    }

}
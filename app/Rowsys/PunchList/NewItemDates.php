<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 4/12/17
 * Time: 11:48 AM
 */

namespace App\Rowsys\PunchList;


use App\Models\Activity;
use App\Models\Project\SystemMilestone;
use App\Models\PunchList\SystemScheduleDates;
use App\Models\SystemToMilestoneLink;
use Carbon\Carbon;

class NewItemDates
{


    public static function forSystem($system_id){

        //table = punch_list_system_schedule_links
        $dates = SystemScheduleDates::where('system_id', $system_id)->first();

        $milestones = SystemMilestone::lists('name')->toArray();

        $systemLinks = SystemToMilestoneLink::where('system_id', $system_id)
            ->with('milestone')->get();

        $datesArray = [];
        $system_current_phase = '';
        $milestoneDate = '';
        $milestoneApplicable = 0;
        $milestoneStatus = 0;
        $linkDate = '';

        // Selections
        //Cat 1 setup
        $catA_Date = '';
        $catA_step = '';
        $catA_step_number = 0;
        $columnStatus = 'm1_status';
        $columnDate = 'm1_date';
        $catA_step_txt = 'default';

        //Cat B
        $today = Carbon::now();
        $a_step_found = false;


        $dataOut = collect();
        $system_current_phase = 'unknown';
        $catA_Date = null;
        $catA_step_txt  = 'unknown';
        $catA_step_number  = 'unknown';
        $datesDescription  = 'unknown';
        $stepCount = 1;

        foreach ($systemLinks as $systemLink) {

            $date = Activity::where('name',$systemLink->activity_name)
                // ->where('date_choice',$systemLink->date_choice)
                ->first();

            array_push($datesArray, [ $systemLink->milestone->name,  $date->start_date->format('d-m-Y'), $systemLink->milestone->id]);

            $stepCount ++;
        }

        $data = [
            'datesArray' => $datesArray,
            'system_current_phase' => $system_current_phase,
            'cat_a_date' => $catA_Date,
            'cat_a_step' => $catA_step_txt,
            'cat_a_step_number' => $catA_step_number,
            'datesDescription' => $datesDescription,

        ];

        return $data;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 18/3/16
 * Time: 1:05 PM
 */

namespace app\Rowsys\Import;


use Illuminate\Http\Request;

class TasksImport extends \Maatwebsite\Excel\Files\ExcelFile {

    protected $file;
    protected $delimiter  = ',';
    protected $enclosure  = '"';
    protected $lineEnding = '\r\n';
    protected $fileLocation = '';

    function __construct(Request $request) {

        $this->file = $request->file('file');

        $path = $this->file->getRealPath();
        $filename = $this->file->getClientOriginalName();

        $pub_path = public_path();
        $extension = $this->file->getClientOriginalExtension();

        $this->fileLocation =  $pub_path.$filename;

    }

    public function getFile()
    {
        return $this->fileLocation;
        //return storage_path('exports') . '/file.csv';
    }

    public function getFilters()
    {
        return [
            'chunk'
        ];
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 19/3/16
 * Time: 2:08 PM
 */

namespace app\Rowsys\Admin\Files;

use App\Models\Task;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\System;
use App\Models\Group;
use App\Models\TaskType;
use App\Models\TaskRule;
use App\Models\Stage;
use App\Models\TaskColor;
use App\Models\SchedulerDate;
use App\Models\TaskExtension;
use App\Rowsys\Tracker\Classes\Compile;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Mockery\CountValidator\Exception;

class ImportTasksFromFile
{

    protected $filePath;
    protected $databaseName;

    /**
     *
     */
    //TODO add Project number at the moment is set to 1
    function __construct($filePath)
    {
        $this->databaseName = env('DB_DATABASE');
        $this->filePath = $filePath;
    }

    //*************************************************
    // ------- TEMP TABLE --------------
    //Make a tmp table for the import
    public function makeTempTaskTable()
    {
        $message = "";
        if(Schema::hasTable('tasks_tmp') === false){
            $query = 'CREATE TABLE tasks_tmp LIKE tasks';
            $message = \DB::getPdo()->exec($query);
        }

        return $message;
    }

    //Clear the Temp Schedule Table
    public function clearTempTaskTable()
    {
        $query = 'DELETE FROM tasks_tmp ';
        $message = \DB::getPdo()->exec($query);
        return $message;
    }

    public function resetAutoIncrementNumber(){
        $query = 'ALTER TABLE tasks_tmp AUTO_INCREMENT = 1';
        $message = \DB::getPdo()->exec($query);
        return $message;
    }

    //Delete temp table
    public function deleteTempTaskTable()
    {
        $query  = 'DROP TABLE IF EXISTS tasks_tmp';
        $message = \DB::getPdo()->exec($query);

        return $message;
    }

    //*************************************************
    // ------ IMPORT FILE TO TEMP TABLE -----------
    /**
     *  Import file into temp table
     * @return int
     */
    public function importToTmpTableFromFile()
    {
        try{
            $query = sprintf("
            LOAD DATA LOCAL INFILE '%s'
            INTO TABLE tasks_tmp
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            IGNORE 1 LINES
            (@stage,@system_tag,@type_name,@sub_type_name,@add_on_text,@add_on_description,@document_number,
            @schedule_link_number,@lag,@schedule_choice,@target_hrs,@task_owner)
            SET
            id = null,
            number = CONCAT(@stage, '.', @system_tag, '.',@type_name,if(@add_on_text = '','',CONCAT('.',@add_on_text))),
            description = 'not compiled',
            add_on = TRIM(@add_on_text),
            add_on_desc = TRIM(@add_on_description),
            stage_id = (SELECT id FROM stages WHERE short_name = TRIM(@stage) limit 1),
            area_id = (SELECT area_id FROM systems WHERE tag = TRIM(@system_tag) limit 1),
            system_id = (SELECT id FROM systems WHERE tag = TRIM(@system_tag) limit 1),
            group_id = (SELECT group_id FROM task_types WHERE short_name = TRIM(@type_name) limit 1),
            task_type_id = (SELECT id FROM task_types WHERE short_name = TRIM(@type_name) limit 1),
            task_sub_type_id = (SELECT id FROM task_sub_types WHERE name = TRIM(@sub_type_name) limit 1),
            target_val = @target_hrs,
            document_number = @document_number,
            group_owner_id = (SELECT id FROM areas WHERE short_name = TRIM(@task_owner) limit 1),
            assigned_role_id = (SELECT id FROM areas WHERE short_name = TRIM(@task_owner) limit 1),
            lag_days = @lag,
            compile_error = 2,
            schedule_number = TRIM(@schedule_link_number),
            schedule_date_choice = TRIM(@schedule_choice)", addslashes($this->filePath));
            $message = \DB::getPdo()->exec($query);

        }catch (Exception $e){
            $message = "Something went wrong - " . $e;
        }


        return $message;
    }

    //*************************************************
    // ------ VALIDATE IMPORTED DATA -----------

    //*************************************************
    // ------ INSERT TEMP TABLE DATA INTO TASK TABLE  -----------
    // add the new tasks from the import table
    public function addTasks()
    {
        $query = "INSERT INTO tasks(
                number,description,add_on,add_on_desc,document_number,
                stage_id,area_id,system_id,group_id,task_type_id,task_sub_type_id,
                target_val,
                group_owner_id,assigned_role_id,
                project_id,
                schedule_number,schedule_date_choice,schedule_lag_days,
                created_at,updated_at
                )
                SELECT
                number,description,add_on,add_on_desc,document_number,
                stage_id,area_id,system_id,group_id,task_type_id,task_sub_type_id,
                target_val,
                group_owner_id,assigned_role_id,
                1,
                schedule_number,schedule_date_choice,schedule_lag_days,
                created_at,updated_at
                FROM tasks_tmp";
        $message = \DB::getPdo()->exec($query);

        return $message;
    }

    //*************************************************
    // ------ INSERT TEMP TABLE DATA INTO TASK TABLE  -----------
    // add the new tasks from the import table
    public function old_addTasks()
    {
        $query = "INSERT INTO tasks
        (number,description,prim_date,base_date, project_id,
         schedule_number,schedule_start_date,schedule_finish_date,schedule_date_choice,schedule_lag_days,
         area_id,system_id,task_type_id,group_id,stage_id,created_at,updated_at,compile_error)
        SELECT number,description,base_Date,base_Date,1,
        schedule_task_id,start_Date,finish_Date,date_choice,date_lag,
        (SELECT id FROM areas WHERE name = task_imports.area),
        (SELECT id FROM systems WHERE tag = task_imports.system),
        (SELECT id FROM task_types WHERE short_name = task_imports.task_type ),
        (SELECT id FROM groups WHERE description = task_imports.task_group) ,
        (SELECT id FROM stages WHERE name = task_imports.stage ),
        now(),
        now(),
        0
        FROM task_imports
        WHERE task_imports.number NOT IN (SELECT number FROM tasks)";
        $message = \DB::getPdo()->exec($query);

        return $message;
    }


    //*************************************************
    // ------ DELETE IMPORT FILE -----------
    //Delete the Temp Schedule Table
    public function deleteTempImportTasksFile()
    {
        $message = File::delete($this->filePath);

        return $message;
    }


    //*************************************************
    // ------ OLD IMPORT CODE -----------

    public function old_importToTmpTableFromFile()
    {
        $query = sprintf("
                LOAD DATA LOCAL INFILE '%s'
                INTO TABLE task_imports
                FIELDS TERMINATED BY ','
                ENCLOSED BY '\"'
                IGNORE 1 LINES
                (@number,@description,@area,@system,@sys_desc,
                @task_group,@task_type,
                @task_name,@stage,@base_date,
                @schedule_number,@schedule_description,@choice,@lag,@start_date,@finish_date)
                SET count = (SELECT id FROM systems WHERE tag = '50.M2.R03'),
                number = TRIM(@number),
                description = TRIM(@description),
                area = TRIM(@area),
                system = TRIM(@system),
                sys_desc = TRIM(@sys_desc),
                task_group = TRIM(@task_group),
                task_type = TRIM(@task_type),
                task_name = TRIM(@task_name),
                stage = TRIM(@stage),
                id = null,
                date_string = @base_date,
                base_date = STR_TO_DATE(@base_date,'%%d-%%b-%%y'),
                schedule_task_id = @schedule_number,
                schedule_task_desc = @schedule_description,
                date_choice = @choice,
                date_lag = @lag,
                start_date = STR_TO_DATE(@start_date,'%%d-%%b-%%y'),
                finish_date = STR_TO_DATE(@finish_date,'%%d-%%b-%%y')", addslashes($this->filePath));
        $message = \DB::getPdo()->exec($query);

        return $message;
    }

    //Filter out the Areas from the import table and add any new Areas
    public function makeTableLikeTaskTable($tableName)
    {
        $query = sprintf("CREATE TABLE '.$this->databaseName.'.%s LIKE '.$this->databaseName.'.tasks", $tableName);
        //$query  = 'CREATE TABLE `novartis-asia`.tasks_tmp1 LIKE `novartis-asia`.tasks';
        \DB::getPdo()->exec($query);
    }

    //Clear the Temp table
    public function clearTmpTable()
    {
        $query = 'DELETE FROM task_imports';
        $message = \DB::getPdo()->exec($query);
    }

    //Clear the Temp table
    public function clearTestingTable()
    {
        $query = 'DELETE FROM test_import';
        $message = \DB::getPdo()->exec($query);
    }

    //Clear the Temp table
    public function clearTaskTable()
    {
        $query = 'DELETE FROM tasks';
        $message = \DB::getPdo()->exec($query);
    }


    //Filter out the Areas from the import table and add any new Areas
    public function updateTasksHoursToDefault($tableName)
    {
        $query = sprintf("update '.$this->databaseName.'.%s SET target_val = 40 WHERE target_val = 0", $tableName);
        //$query  = 'CREATE TABLE `novartis-asia`.tasks_tmp1 LIKE `novartis-asia`.tasks';
        \DB::getPdo()->exec($query);

    }

    //Update the hours to 40
    //update `novartis-asia`.tasks SET target_val = 40 WHERE target_val = 0;

    //Filter out the Areas from the import table and add any new Areas
    public function addAreas()
    {
        $query = "INSERT INTO areas (name, description,project_id,created_at,updated_at) SELECT area, 'No Desc, Imported from File',1,now(),now()
        FROM task_imports WHERE task_imports.area  NOT IN (SELECT name FROM areas)  GROUP BY area";
        $message = \DB::getPdo()->exec($query);

        return $message;
    }

    //Filter out the Systems from the import table and add any new Areas
    public function addSystems()
    {
        $query = "INSERT INTO systems (tag, description,area_id,project_id,created_at,updated_at)
        SELECT system, sys_desc,(SELECT id FROM areas WHERE name = task_imports.area GROUP by name),1,now(),now()
        FROM task_imports WHERE task_imports.system  NOT IN (SELECT tag FROM systems)  GROUP BY system";
        $message = \DB::getPdo()->exec($query);

        return $message;
    }

    //Filter out the Areas from the import table and add any new Areas
    public function updateBaseDateIfImportHadNoDate()
    {
        $query = "UPDATE task_imports SET base_date = NOW() WHERE base_date = 0";
        \DB::getPdo()->exec($query);
    }


    //Imports the data into the schedule dates table from the imports table
    public function importScheduleDatesFromImportTable()
    {
        $query = "INSERT IGNORE INTO schedule_dates(number,description,start_date,finish_date,system_id)
        SELECT schedule_task_id,schedule_task_desc,start_date,finish_date,
        (SELECT id FROM systems WHERE tag = system )
        FROM task_imports WHERE task_imports.schedule_task_id NOT IN (SELECT number FROM schedule_dates)";
        $message = \DB::getPdo()->exec($query);

        return $message;

    }

    //Imports to the schedule link table from the import table
    public function importScheduleLinkTableFromImportTable()
    {
        $query = "INSERT INTO task_schedule_links( task_id,schedule_id,date_choice,lag,target_date,base_date,schedule_start_date,schedule_finish_date)
        SELECT (SELECT id FROM tasks WHERE task_imports.number  = tasks.number group by number),
        (SELECT id FROM schedule_dates WHERE task_imports.schedule_task_id = schedule_dates.number group by number ),
        date_choice,date_lag,base_date,base_date,start_date,finish_date FROM task_imports ";
        $message = \DB::getPdo()->exec($query);

        return $message;
    }


    //Filter out the Task Groups from the import table and add any new Areas
    public function addTaskGroups()
    {


    }

    //Filter out the Task Types from the import table and add any new Areas
    public function addTaskTypes()
    {
        $query = "INSERT INTO task_types (short_name,name, description,group_id,project_id,created_at,updated_at,stage_add_rule)
        SELECT task_type,'imported' ,'imported no desc',
        (SELECT id FROM groups WHERE description = task_imports.task_group GROUP by description),1,now(),now(),1
        FROM task_imports WHERE task_type NOT IN (SELECT short_name FROM task_types) GROUP BY task_type";
        $message = \DB::getPdo()->exec($query);

        return $message;
    }


    //Add a task Extension for the new imported tasks
    public function addTaskExtensions()
    {


    }


    public function importOld($filePath)
    {
        /*
         *----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
         *                                          CABLE                                    |       TYPE     |                                  ORIGIN                                       |                                DESTINATION
         * ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
         *   0        1        2      3       4       5         6         7         8        |    9       10          11       12           13           14           15        16        17       18       19         20           21          22         23        24
         * group   cableTag Number   Desc   length   power  purpose   technology   c_note     |  type    cores  |   O_Tag    O_Decs     O_device    O_device_desc  O_plug    O_sock    O_gland | D_Tag   D_desc    d_device   D_device_desc   d_plug    d_sock    d_gland
         *-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
         */

        $message = 'Did not Process';

        $col = array(
            "number" => 1,
            "projectId" => 2,
            "area_Id" => 3,
            "system_id" => 4,
            "group_id" => 5,
            "task_type_id" => 6,
            "c_attribute" => 7,
            "c_note" => 8,
            "type" => 9,
            "cores" => 10,
            "o_tag" => 11,
            "o_desc" => 12,
            "o_dev_tag" => 13,
            "o_dev_desc" => 14,
            "o_plug" => 15,
            "o_sock" => 16,
            "o_gland" => 17,
            "d_tag" => 18,
            "d_desc" => 19,
            "d_dev_tag" => 20,
            "d_dev_desc" => 21,
            "d_plug" => 22,
            "d_sock" => 23,
            "d_gland" => 24,
        );


        if (!file_exists($filePath) || !is_readable($filePath)) {
            return "Cant Open File";
        }
        // Attempt to import the file
        if (($handle = fopen($filePath, "r")) !== false) {
            fgetcsv($handle);
        }
        // skip Header row
        $cable_num_added = 0;
        $row_number = 1;
        try {

            while (($data = fgetcsv($handle, 1000, ",")) !== false) {

                $task = new Task;

                //Values that come from the Form
                $task->number = $data[$col['number']];

                $task->project_id = 1;

                $type = Area::firstOrCreate(array('tag' => $data[$col['system']], 'type' => $data[$col['type']]));
                $task->Area()->associate($type);
                $task->area_id = (int)$system->area_id;


                $task->system_id = (int)$request->system;


                $task->group_id = $taskType->group_id;


                $task->task_type_id = (int)$taskType->id;


                $task->stage_id = $stage->id;


                $task->gen_applicable = 1;
                $task->gen_td = Carbon::now();
                $task->gen_perc = 0;
                $task->gen_hrs = 0;

                $task->rev_applicable = 1;
                $task->rev_td = Carbon::now();
                $task->rev_perc = 0;
                $task->rev_hrs = 0;

                $task->re_issu_applicable = 1;
                $task->re_issu_td = Carbon::now();
                $task->re_issu_perc = 0;
                $task->re_issu_hrs = 0;

                $task->s_off_applicable = 1;
                $task->s_off_td = Carbon::now();
                $task->s_off_perc = 0;
                $task->s_off_hrs = 0;

                //Assigned Hours
                $task->target_val = 0;
                $task->earned_val = 0;

                //Owner
                $task->group_owner_id = 1;

                //Automation
                $task->auto_id = 1;

                //Next Target Date
                $task->next_td = Carbon::now();//$task->next_td = new \DateTime('2016-05-09');

                //Next Target Date
                $task->last_td = Carbon::now();//$task->next_td = new \DateTime('2016-05-09');

                //Prime Date - Target Completion Date
                $task->prim_date = $targetDates[$increment];

                $task->base_date = Carbon::now()->addMonths(2);

                $task->base_dev_days = '0 Days';

                //Value that are set to default values
                $task->complete = 0;
                $task->complete_date = Carbon::now();

                // --- Compile Error ---
                $task->compile_error = 0;
                if ($stageDateError || $noTaskInSchedulerError) {
                    $task->compile_error = 1;
                }

                //------------------------
                //   save the new task
                //------------------------
                //todo is this needed? as the applying the rule saves the task?
                $task->save();

                //---------------------------
                //   Add Task Extension
                //---------------------------
                $taskExtension = new TaskExtension;
                $taskExtension->task_id = $task->id;
                $taskExtension->error_code = $task->compile_error;
                $taskExtension->doc_id = 0;
                $taskExtension->who_completed_id = 1;
                $taskExtension->who_created_id = 1;
                $taskExtension->note = $request->note;
                $taskExtension->push();

                //---------------------------
                //   Apply Rule to New Task
                //---------------------------
                $task->applyTaskRule();

                //Import each row
                $cable_num_added++;
                $row_number++;

            }

            $message = $cable_num_added . ' Tasks Added';

            fclose($handle);

            if ($cable_num_added == 0) {
                $success = 1;
                //return Redirect::back()->with('flash_message', '<h4> No Changes - file and database are the same</h4>')->with('success', $success);

            } else {
                $success = 1;
                //return Redirect::back()->with('flash_message', '<h3> Success! ' . $cable_num_added . ' cables added</h3>')->with('success', $success);

            }

        } catch (\Exception $e) {
            $message = 'Exception at Import Tasks From File :' . $e;
            fclose($handle);

        }

        return $message;
    }

}
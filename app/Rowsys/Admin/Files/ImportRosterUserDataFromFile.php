<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 14/4/16
 * Time: 4:18 PM
 */

namespace app\Rowsys\Admin\Files;


use Illuminate\Support\Facades\File;

class ImportRosterUserDataFromFile {

    protected $filePath;
    protected $databaseName;

    /**
     *
     */
    //TODO add Project number at the moment is set to 1
    function __construct($filePath)
    {
        $this->filePath = $filePath;
        $this->databaseName = env('DB_DATABASE');
    }

    //Clear the Schedule Table
    public function clearTable(){
        $query  = 'DELETE FROM roster_user_area_hours';
        $message = \DB::getPdo()->exec($query);
        return $message;
    }
    //Clear the Schedule Table
    public function copyTempTableToTable(){
        $query  = 'INSERT INTO roster_user_area_hours 
        ( number, description, start_date,finish_date,imported_data) SELECT number, description, start_date,finish_date,imported_data 
        FROM roster_user_area_hours_tmp';
        $message = \DB::getPdo()->exec($query);
        return $message;
    }

    //======= SCHEDULE DATES TEMP TABLE =======
    //create Temp Schedule Table
    public function createTempTable(){
        $query  = 'CREATE TABLE roster_user_area_hours_tmp LIKE roster_user_area_hours';
        $message = \DB::getPdo()->exec($query);
        return $message;
    }

    //Clear the Temp Schedule Table
    public function clearTempTable(){
        $query  = 'DELETE FROM roster_user_area_hours_tmp';
        $message = \DB::getPdo()->exec($query);
        return $message;
    }
    //Delete the Temp Schedule Table
    public function deleteTempTable(){
        $query  = 'DROP TABLE IF EXISTS roster_user_area_hours_tmp;';
        $message = \DB::getPdo()->exec($query);
        return $message;
    }

    /**
     *  Make a tmp table for the import
     * @return int
     */
    public function importToScheduleDatesTable(){

        $query = sprintf("
                LOAD DATA LOCAL INFILE '%s'
                INTO TABLE schedule_dates
                FIELDS TERMINATED BY ','
                ENCLOSED BY '\"'
                IGNORE 2 LINES
                (@activity_id,@activity_status,@wbs_code,@description,@start_date,@finish_date,@last_col)
                SET id = null,
                number = TRIM(@activity_id),
                description = TRIM(@description),
                start_date = STR_TO_DATE(
                if(@start_date = '' , CONCAT(SUBSTRING_INDEX(@finish_date, ' ', 1), ' 08:00:00'),CONCAT(SUBSTRING_INDEX(@start_date, ' ', 1), '08:00:00'))
                ,'%%d/%%m/%%Y %%H:%%i:%%s'),
                string_start_date = @start_date,
                finish_date = STR_TO_DATE(
                if(@finish_date = '' , CONCAT(SUBSTRING_INDEX(@start_date, ' ', 1), ' 18:00:00'),
                 CONCAT(SUBSTRING_INDEX(@finish_date, ' ', 1), '18:00:00'))
                  ,'%%d/%%m/%%Y %%H:%%i:%%s'),
                string_finish_date =@finish_date,
                imported_data = 1,
                created_at = now(),
                updated_at = now()
                 ", addslashes($this->filePath));


        $message = \DB::getPdo()->exec($query);
        return $message;
    }

    /**
     *  Make a tmp table for the import
     * @return int
     */
    public function importBasicFileToTempScheduleDatesTable(){

        $query = sprintf("
                LOAD DATA LOCAL INFILE '%s'
                INTO TABLE schedule_dates_tmp
                FIELDS TERMINATED BY ','
                ENCLOSED BY '\"'
                IGNORE 1 LINES
                (@activity_id,@description,@start_date,@finish_date,@last_col)
                SET id = null,
                number = TRIM(@activity_id),
                description = TRIM(@description),
                start_date = STR_TO_DATE(
                if(@start_date = '' , CONCAT(SUBSTRING_INDEX(@finish_date, ' ', 1), ' 08:00:00'),CONCAT(SUBSTRING_INDEX(@start_date, ' ', 1), '08:00:00'))
                ,'%%Y-%%m-%%d %%H:%%i:%%s'),
                string_start_date = @start_date,
                finish_date = STR_TO_DATE(
                if(@finish_date = '' , CONCAT(SUBSTRING_INDEX(@start_date, ' ', 1), ' 18:00:00'),
                 CONCAT(SUBSTRING_INDEX(@finish_date, ' ', 1), '18:00:00'))
                 ,'%%Y-%%m-%%d %%H:%%i:%%s'),
                string_finish_date =@finish_date,
                imported_data = 1,
                created_at = now(),
                updated_at = now()
                 ", addslashes($this->filePath));


        $message = \DB::getPdo()->exec($query);
        return $message;
    }

    //Delete the Temp Schedule Table
    public function deleteTempImportScheduleFile(){
        $message = "";
        $message = File::delete($this->filePath);

        return $message;
    }


}
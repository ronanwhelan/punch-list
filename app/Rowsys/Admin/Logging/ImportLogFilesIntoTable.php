<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 24/3/16
 * Time: 9:55 AM
 */

namespace app\Rowsys\Admin\Logging;

use App\Models\JobLog;
use Symfony\Component\HttpFoundation\File\File;

class ImportLogFilesIntoTable {


    /**
     *  Make a tmp table for the import
     * @return int
     */
    public function importLogFilesToLoggingTable(){
        //$filePath = \Storage::disk('local')->get('JobsLog.txt');
        $path = base_path();
        $filePath = $path.'/storage/app/JobsLog.txt';
        $query = sprintf("
                LOAD DATA LOCAL INFILE '%s'
                INTO TABLE `novartis-asia`.job_logs
                FIELDS TERMINATED BY '|'
                ENCLOSED BY '\"'
                (started_at,@type,@description,@user_id)
                SET id = null,
                type = TRIM(@type),
                description = TRIM(@description),
                user_id = TRIM(@user_id),
                created_at = now(),
                updated_at = now()",
            addslashes($filePath));
        $message = \DB::getPdo()->exec($query);
        return $message;
    }



    //Clear out the log files
    public function clearLogFiles(){
        $path = base_path();
        $filePath = $path.'/storage/app/JobsLog.txt';
        \File::delete($filePath);

    }

    //Clear out the log files
    public function deleteOldLogsFromTable($weeks){
       // $jobLogs = JobLog::all();
    }

}
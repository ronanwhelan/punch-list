<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 24/3/16
 * Time: 2:46 PM
 */

namespace app\Rowsys\Tracker\Classes;

use App\Models\Project;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskStepPhase;
use App\Models\UserFilter;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\System;
use App\Models\Group;
use App\Models\TaskType;
use App\Models\TaskRule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class TaskStats
{
    protected $stats = [];
    protected $groups = [];
    protected $genTaskStats = [];
    protected $taskBreakDownByGroup = [];
    protected $groupStatsInTheNextTwoWeeks = [];
    protected $TaskBreakDownByGroupTaskType = [];


    /**
     * Returns the Statistics for the Search Result for the Users
     * Dashboard based on the users Filter
     * @param Builder $query
     * @return mixed
     */
    public function getUserDashBoardStatistics(Builder $query)
    {

        //User Selections
        $user = \Auth::getUser();
        $userAreas = $user->area;
        $userGroups = $user->group;
        $userTypes = $user->type;
        $userStages = $user->stage;

        // ======= STATS =================
        $taskGroup = [];
        //--------Complete, Done and Percentage Done----------
        $statQuery = clone $query;
        $total = $statQuery->count();
        $statQuery = clone $query;
        $totalNotDone = $statQuery->where('complete', 0)->count();
        //$userStats['stats'] = ['query' => $query->toSql(),'statQuery' => $statQuery->toSql()];return $userStats;

        $statQuery = clone $query;
        $completed = $statQuery->where('complete', '=', 1)->count();
        $leftToDo = (int)$total - $completed;
        $percDone = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 3) * 100;
            if ($percDone < 1) {
                //$percDone = 0;
            }
        }
        //------------------PAST DUE -----------------------------
        $today = Carbon::now();
        $statQuery = clone $query;
        $pastDueByGroup = [];
        $pastDue = 0;
        if ($user->type !== '') {
            $statQuery = clone $query;
            $groupIds = $statQuery->where('complete', '=', 0)
                ->where('next_td', '<', $today)
                ->groupBy('group_id')
                ->lists('group_id');
            $groups = Group::find(explode(',', $groupIds));
        } else {
            if (count($user->group) > 0 && $user->group !== '') {
                $groups = Group::find(explode(',', $user->group));
            } else {
                $statQuery = clone $query;
                $groupIds = $statQuery->where('complete', '=', 0)->where('next_td', '<', $today)->groupBy('group_id')->lists('group_id');
                $groups = Group::whereIn('id', $groupIds)->get();
            }
        }
        foreach ($groups as $group) {
            $statQuery = clone $query;
            $pastDueCount = $statQuery->where('complete', '=', 0)
                ->where('next_td', '<', $today)
                ->where('group_id', '=', $group->id)
                ->count();

            if ($pastDueCount > 0) {
                $pastDueByGroup[$group->name] = $pastDueCount;
            }

            $pastDue = $pastDue + $pastDueCount;
        }
        $today = Carbon::now();
        $statQuery = clone $query;
        $totalPastDue = $statQuery->where('next_td', '=<', $today)->where('complete', '=', 0)->count();

        $today = Carbon::now();
        $statQuery = clone $query;
        $pastDue = $statQuery->where('complete', '=', 0)->where('next_td', '<=', $today)->count();
        $pastDuePercent = 0;
        if ($pastDue > 0) {
            $pastDuePercent = round(($pastDue / $total), 3) * 100;
            if ($pastDuePercent < 1) {
                //$pastDuePercent = 0;
            }
        }

        //Filtered - Earned Hours
        $statQuery = clone $query;
        $totalHrs = $statQuery->sum('target_val');
        $statQuery = clone $query;
        $earnedHrs = $statQuery->sum('earned_val');
        $earnedHrsPerc = 0;
        if($totalHrs > 0){
            $earnedHrsPerc = round(($earnedHrs/$totalHrs * 100),1);
        }

        //---------------------TIME -------------------------
        //Due within 1 Week
        $today = Carbon::now();
        $statQuery = clone $query;
        $oneWeekFromToday = $today->addWeeks(1);
        $dueWithinOneWeek = $statQuery->where('complete', '=', 0)
            ->where('next_td', '<=', $oneWeekFromToday)
            ->count();
        $dueWithinOneWeek = $dueWithinOneWeek - $pastDue;


        //Due within 2 Weeks
        $today = Carbon::now();
        $statQuery = clone $query;
        $twoWeeksFromToday = $today->addWeeks(2);
        $dueWithinTwoWeeks = $statQuery->where('complete', '=', 0)
            ->where('next_td', '<=', $twoWeeksFromToday)
            ->count();
        $dueWithinTwoWeeks = $dueWithinTwoWeeks - $pastDue;

        //Due within 3 Weeks
        $today = Carbon::now();
        $statQuery = clone $query;
        $threeWeeksFromToday = $today->addWeeks(3);
        $dueWithinThreeWeeks = $statQuery->where('complete', '=', 0)
            ->where('next_td', '<=', $threeWeeksFromToday)
            ->count();
        $dueWithinThreeWeeks = $dueWithinThreeWeeks - $pastDue;

        //Due within 1 Month
        $today = Carbon::today();
        $statQuery = clone $query;
        $oneMonthFromToday = $today->addWeeks(4);
        $dueWithinOneMonth = $statQuery->where('complete', '=', 0)
            ->where('next_td', '<=', $oneMonthFromToday)
            ->count();
        $dueWithinOneMonth = $dueWithinOneMonth - $pastDue;

        //Due within 2 Months
        $today = Carbon::now();
        $statQuery = clone $query;
        $twoMonthFromToday = $today->addWeeks(8);
        $dueWithinTwoMonths = $statQuery->where('complete', '=', 0)
            ->where('next_td', '<=', $twoMonthFromToday)
            ->count();
        $dueWithinTwoMonths = $dueWithinTwoMonths - $pastDue;

        $today = Carbon::now();
        $twoWeeksAgo = $today->subWeeks(2);
        $addedInTheLast2Weeks = $statQuery->where('created_at', '>', $twoWeeksAgo)->count();
        $completedInTheLast2Weeks = $statQuery->where('complete', '=', 1)->where('created_at', '>', $twoWeeksAgo)->count();

        //Show the user some values rather than zero - see when some tasks are due
        //and how many
        $tasksDueCountForDisplay = $dueWithinOneWeek;
        $tasksDueTextForDisplay = 'Due within One Week';
        $numOfWeeksForLink = 1;

        if ($dueWithinOneWeek === 0) {
            $tasksDueCountForDisplay = $dueWithinTwoWeeks;
            $tasksDueTextForDisplay = 'Due within Two weeks';
            $numOfWeeksForLink = 2;
        }
        if ($dueWithinTwoWeeks === 0) {
            $tasksDueCountForDisplay = $dueWithinThreeWeeks;
            $tasksDueTextForDisplay = 'Due within Three weeks';
            $numOfWeeksForLink = 3;
        }
        if ($dueWithinThreeWeeks === 0) {
            $tasksDueCountForDisplay = $dueWithinOneMonth;
            $tasksDueTextForDisplay = 'Due within One Month';
            $numOfWeeksForLink = 4;
        }
        if ($dueWithinOneMonth === 0) {
            $tasksDueCountForDisplay = $dueWithinTwoMonths;
            $tasksDueTextForDisplay = 'Due within Two Months';
            $numOfWeeksForLink = 8;
        }
        //------------------ % Due of my Task  -----------------------------
        $duePerc = 0;
        if ($total > 0) {
            $duePerc = round(($tasksDueCountForDisplay / $total), 3) * 100;
            if ($duePerc < 1) {
                //$duePerc = 0;
            }
        }

        //Static / Unfiltered Stats
        $nonFilteredQuery = Task::where('project_id', 1);
        //----------Project Flagged -----------
        $statQuery = clone $nonFilteredQuery;
        $flagCount = $statQuery->where('flag', '=', 1)->count();
        $statQuery = clone $nonFilteredQuery;
        $flaggedDone = $statQuery->where('flag', '=', 1)->where('complete', '=', 1)->count();
        $flaggedPerc = 0;
        if($flagCount > 0){
            $flaggedPerc = ceil($flaggedDone/$flagCount * 100);
        }



        //----------My Flagged -----------
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $myFlagCount = 0;
        $myFlaggedDone = 0;
        $myFlaggedPerc = 0;
        $myFlagOpenCount = 0;
        if (count($userFilter) > 0) {
            $currentListString = $userFilter->task_flag_list;
            $currentListArray = explode(',', $currentListString);
            $statQuery = clone $nonFilteredQuery;
            $myFlagCount = $statQuery->whereIn('id', $currentListArray)->count();
            $statQuery = clone $nonFilteredQuery;
            $myFlagOpenCount = $statQuery->whereIn('id', $currentListArray)->where('complete', '=', 0)->count();
            $statQuery = clone $nonFilteredQuery;
            $myFlaggedDone = $statQuery->whereIn('id', $currentListArray)->where('complete', '=', 1)->count();
            $myFlaggedPerc = 0;
            if($myFlagCount > 0){
                $myFlaggedPerc = ceil($myFlaggedDone/$myFlagCount);
            }
        }

        //My Tasks
        $statQuery = clone $nonFilteredQuery;
        $myTaskCount = $statQuery->where('assigned_user_id', $user->id)->count();
        $statQuery = clone $nonFilteredQuery;
        $myOpenTaskCount = $statQuery->where('assigned_user_id', $user->id)->where('complete', '=', 0)->count();
        $statQuery = clone $nonFilteredQuery;
        $myTaskDone = $statQuery->where('assigned_user_id', $user->id)->where('complete', '=', 1)->count();
        $myTaskPerc = 0;
        if($myTaskCount > 0){
            $myTaskPerc = ceil($myTaskDone/$myTaskCount * 100);
        }


        //Roles Tasks
        $userRoles = $user->roles->lists('id')->toArray();
        $statQuery = clone $nonFilteredQuery;
        $roleTaskCount  = $statQuery->whereIn('group_owner_id', $userRoles)->count();
        $statQuery = clone $nonFilteredQuery;
        $roleOpenTaskCount  = $statQuery->whereIn('group_owner_id', $userRoles)->where('complete', '=', 0)->count();
        $statQuery = clone $nonFilteredQuery;
        $roleTaskDone = $statQuery->whereIn('group_owner_id', $userRoles)->where('complete', '=', 1)->count();
        $roleTaskPerc = 0;
        if($roleTaskCount > 0){
            $roleTaskPerc = ceil($roleTaskDone/$roleTaskCount * 100);
        }






        $statQuery = clone $nonFilteredQuery;
        $teamTotalHrs = $statQuery->whereIn('group_owner_id', $userRoles)->sum('target_val');
        if($teamTotalHrs < 1){
            $teamTotalHrs = 0;
        }
        $statQuery = clone $nonFilteredQuery;
        $teamEarnedHrs = $statQuery->whereIn('group_owner_id', $userRoles)->sum('earned_val');
        if($teamEarnedHrs < 1){
            $teamTotalHrs = 0;
        }
        $teamEarnedHrsPerc = 0;
        if($teamTotalHrs > 0){
            $teamEarnedHrsPerc = round(($teamEarnedHrs/$teamTotalHrs * 100),1);
        }



        $userStats['stats'] =
            [
                'query' => $query->toSql(),
                'userName' => $user->name,
                'total' => $total,
                'totalNotDone' => $totalNotDone,
                'completed' => $completed,
                'leftToDo' => $leftToDo,
                'percDone' => $percDone,
                'pastDue' => $pastDue,
                'pastDueByGroup' => $pastDueByGroup,
                'dueWithinOneWeek' => $dueWithinOneWeek,
                'dueWithinTwoWeeks' => $dueWithinTwoWeeks,
                'dueWithinThreeWeeks' => $dueWithinThreeWeeks,
                'dueWithinOneMonth' => $dueWithinOneMonth,
                'dueWithinTwoMonths' => $dueWithinTwoMonths,
                'addedInTheLastTwoWeeks' => $addedInTheLast2Weeks,
                'completedInTheLastTwoWeeks' => $completedInTheLast2Weeks,
                'tasksDueCountForDisplay' => $tasksDueCountForDisplay,
                'tasksDueTextForDisplay' => $tasksDueTextForDisplay,
                'pastDuePercent' => $pastDuePercent,
                'numOfWeeksForLink' => $numOfWeeksForLink,
                'duePerc' => $duePerc,

                'flagCount' => $flagCount,
                'flaggedDone' => $flaggedDone,
                'flaggedPerc' => $flaggedPerc,

                'myFlagCount' => $myFlagCount,
                'myFlagOpenCount' => $myFlagOpenCount,
                'myFlaggedDone' => $myFlaggedDone,
                'myFlaggedPerc' => $myFlaggedPerc,


                'myTaskCount' => $myTaskCount,
                'myOpenTaskCount' => $myOpenTaskCount,
                'myTaskDone' => $myTaskDone,
                'myTaskPerc' => $myTaskPerc,

                'roleTaskCount' => $roleTaskCount,
                'roleOpenTaskCount' => $roleOpenTaskCount,
                'roleTaskDone' => $roleTaskDone,
                'roleTaskPerc' => $roleTaskPerc,

                'targetHrs' => $totalHrs,
                'earnedHrs' => $earnedHrs,
                'earnedPerc' => $earnedHrsPerc,

                'teamTargetHrs' => $teamTotalHrs,
                'teamEarnedHrs' => $teamEarnedHrs,
                'teamEarnedPerc' => $teamEarnedHrsPerc,

            ];

        $userStats['groups'] = $taskGroup;

        return $userStats;
    }


    public function getSearchStats(Builder $query)
    {
        //Total
        $queryClone = clone $query;
        $total = $queryClone->count();

        //Complete and %
        $queryClone = clone $query;
        $complete = $queryClone->where('complete', '=', 1)->count();
        $completePercent = 0;
        if ($total > 0) {
            $completePercent = ($complete / $total) * 100  ;
            if ($completePercent < 1) {
                //$completePercent = '< 1';
            } else {
                $completePercent = round($completePercent, 1);
            }
        }
        $today = Carbon::now();
        $queryClone = clone $query;
        $pastDue = $queryClone->where('next_td', '<', $today)->where('complete', '=', 0)->count();
        $today = Carbon::now();
        $queryClone = clone $query;
        $nonRecoverable = $queryClone->where('last_td', '<', $today)->where('complete', '=', 0)->count();

        $today = Carbon::now();
        $queryClone = clone $query;
        $inSevenDays = $today->addWeeks(1);
        $dueInSevenDays = $queryClone->where('next_td', '<', $inSevenDays)->where('complete', '=', 0)->count();

        $today = Carbon::now();
        $queryClone = clone $query;
        $inFourteenDays = $today->addWeeks(2);
        $dueInFourteenDays = $queryClone->where('next_td', '<', $inFourteenDays)->where('complete', '=', 0)->count();

        $today = Carbon::now();
        $queryClone = clone $query;
        $oneMonthFromToday = $today->addWeeks(4);
        $dueInOneMonth = $queryClone->where('next_td', '<', $oneMonthFromToday)->where('complete', '=', 0)->count();



        $taskStepPhaseStats = [];
        $queryClone = clone $query;


        $data = [
            'total' => $total,
            'completePercent' => $completePercent,
            'complete' => $complete,
            'pastDue' => $pastDue,
            'nonRecoverable' => $nonRecoverable,
            'dueInSevenDays' => $dueInSevenDays - $pastDue,
            'dueInFourteenDays' => $dueInFourteenDays - $pastDue,
            'dueInOneMonth' => $dueInOneMonth - $pastDue,
            '$taskStepPhaseStats' => $taskStepPhaseStats,
            'query' => $query->toSql(),
        ];

        return $data;
    }

//get the stats for the tasks step phases
    public function getTaskStepStats(Builder $query){

        //Active
        $queryClone = clone $query;
        $activeCount = $queryClone->where('gen_perc', '>', 0)->where('s_off_perc', '<', 100)->count();
        //Not Active
        $queryClone = clone $query;
        $notActiveCount = $queryClone->where('gen_perc', '=', 0)->count();


        //----  PREP STAGE ----
        $queryClone = clone $query;
        $genActiveCount = $queryClone->where('stage_id',1)->where('gen_perc', '>', 0)->where('s_off_perc', '<', 100)->count();
        //in Execution
        $queryClone = clone $query;
        $inGenCount = $queryClone->where('stage_id',1)
            ->where('gen_perc', '>', 0)->where('gen_perc', '<', 100)
            ->where('rev_perc', '=', 0)
            ->where('re_issu_perc', '=', 0)
            ->where('s_off_perc', '=', 0)
            ->count();
        //in review
        $queryClone = clone $query;
        $inReviewCount = $queryClone->where('stage_id',1)
            ->where('gen_perc', '=', 100)
            ->where('rev_perc', '<', 100)
            ->where('re_issu_perc', '=', 0)
            ->where('s_off_perc', '=', 0)
            ->count();
        //In Re-Issue
        $queryClone = clone $query;
        $inReIssueCount = $queryClone->where('stage_id',1)
            ->where('gen_perc', '=', 100)
            ->where('rev_perc', '=', 100)
            ->where('re_issu_perc', '<', 100)
            ->where('s_off_perc', '=', 0)
            ->count();
        //In Sign Off
        $queryClone = clone $query;
        $inSignOff = $queryClone->where('stage_id',1)
            ->where('gen_perc', '=', 100)
            ->where('rev_perc', '=', 100)
            ->where('re_issu_perc', '=', 100)
            ->where('s_off_perc', '<', 100)
            ->count();


        //----  EXEC STAGE ----
        $queryClone = clone $query;
        $execActiveCount = $queryClone->where('stage_id',2)->where('gen_perc', '>', 0)->where('s_off_perc', '<', 100)->count();
        $queryClone = clone $query;
        $execInGenCount = $queryClone->where('stage_id',2)
            ->where('gen_perc', '>', 0)->where('gen_perc', '<', 100)
            ->where('rev_perc', '=', 0)
            ->where('re_issu_perc', '=', 0)
            ->where('s_off_perc', '=', 0)
            ->count();
        //in review
        $queryClone = clone $query;
        $execInReviewCount = $queryClone->where('stage_id',2)
            ->where('gen_perc', '=', 100)
            ->where('rev_perc', '<', 100)
            ->where('re_issu_perc', '=', 0)
            ->where('s_off_perc', '=', 0)
            ->count();
        //In Re-Issue
        $queryClone = clone $query;
        $execInReIssueCount = $queryClone->where('stage_id',2)
            ->where('gen_perc', '=', 100)
            ->where('rev_perc', '=', 100)
            ->where('re_issu_perc', '<', 100)
            ->where('s_off_perc', '=', 0)
            ->count();
        //In Sign Off
        $queryClone = clone $query;
        $execInSignOff = $queryClone->where('stage_id',2)
            ->where('gen_perc', '=', 100)
            ->where('rev_perc', '=', 100)
            ->where('re_issu_perc', '=', 100)
            ->where('s_off_perc', '<', 100)
            ->count();

        $data = [
            'activeCount' => $activeCount,
            'notActiveCount' => $notActiveCount,

            'genActiveCount' => $genActiveCount,
            'inGenCount' => $inGenCount,
            'inReviewCount' => $inReviewCount,
            'inReIssueCount' => $inReIssueCount,
            'inSignOff' => $inSignOff,

            'execActiveCount' => $execActiveCount,
            'execInGenCount' => $execInGenCount,
            'execInReviewCount' => $execInReviewCount,
            'execInReIssueCount' => $execInReIssueCount,
            'execInSignOff' => $execInSignOff,
        ];

        return $data;

    }
    //get the stats for the tasks step phases
    public function getStepPhaseStats(Builder $query){

        //Owner Stats
        $queryClone = clone $query;
        $listOfTaskIds = $queryClone->get()->lists('id');
        //$taskSteps = TaskStepPhase::whereIn('task_id',$taskIds)->count();
        $groups = Group::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $ownerPhaseData = [];
        foreach($groups as $group){
            $step2Count = TaskStepPhase::whereIn('task_id',$listOfTaskIds)
                ->where('step_number',2)
                ->where('break_down_count',0)
                ->Where(function ($query) use ($group){
                    $query->where('p1_owner_id',$group->id)->where('p1_status',0)
                        ->orWhere('p2_owner_id',$group->id)->where('p2_status',0)
                        ->orWhere('p3_owner_id',$group->id)->where('p3_status',0)
                        ->orWhere('p4_owner_id',$group->id)->where('p4_status',0)
                        ->orWhere('p5_owner_id',$group->id)->where('p5_status',0);
                })
                ->count();

            $step4Count = TaskStepPhase::whereIn('task_id',$listOfTaskIds)
                ->where('step_number',4)
                ->where('break_down_count',0)
                ->Where(function ($query) use ($group){
                    $query->where('p1_owner_id',$group->id)->where('p1_status',0)
                        ->orWhere('p2_owner_id',$group->id)->where('p2_status',0)
                        ->orWhere('p3_owner_id',$group->id)->where('p3_status',0)
                        ->orWhere('p4_owner_id',$group->id)->where('p4_status',0)
                        ->orWhere('p5_owner_id',$group->id)->where('p5_status',0);
                })
                ->count();

            if($step2Count > 0 || $step4Count > 0 ){
                $ownerData = [$group->name,$step2Count,$step4Count];
                array_push($ownerPhaseData,$ownerData);
            }
        }

        //Active
        $queryClone = clone $query;
        $activeCount = $queryClone->where('gen_perc', '>', 0)->where('s_off_perc', '<', 100)->count();
        //Not Active
        $queryClone = clone $query;
        $notActiveCount = $queryClone->where('gen_perc', '=', 0)->count();

        //in review
        $queryClone = clone $query;
        $inReviewCount = $queryClone->where('gen_perc', '=', 100)->where('rev_perc', '<', 100)->count();

        //In Re-Issue
        $queryClone = clone $query;
        $inReIssueCount = $queryClone->where('rev_perc', '=', 100)->where('re_issu_perc', '<', 100)->count();

        //In Sign Off
        $queryClone = clone $query;
        $inSignOff = $queryClone->where('re_issu_perc', '=', 100)->where('s_off_perc', '<', 100)->count();

        $data = [
            'activeCount' => $activeCount,
            'notActiveCount' => $notActiveCount,
            'inReviewCount' => $inReviewCount,
            'inReIssueCount' => $inReIssueCount,
            'inSignOff' => $inSignOff,
            'ownerPhaseData' => $ownerPhaseData,
            'query' => $query->toSql(),
            'query2' => $queryClone->toSql(),
        ];

        return $data;

    }


    //Get the stats for when the user selects
    // the past due link on the dashboard
    public function getPastDueStatsForTaskView(Builder $query)
    {

        //Get the main query and pass in with filter.

    }

    //Get the Stats for the step phase
    public function ownerStepPhaseStats(Builder $query, $ownerId)
    {


    }


    public function getStats()
    {
        $this->getGenTaskStats();
        array_push($this->stats, ['general' => $this->genTaskStats]);

        $this->getTaskGroupStatsForNextTwoWeeks();
        array_push($this->stats, ['groups' => $this->groups]);
        array_push($this->stats, ['groupIn2Weeks' => $this->groupStatsInTheNextTwoWeeks]);

        Return $this->stats;
    }


    //Returns the Total, Completed,Left to Do and Complete % of all tasks
    public function getGenTaskStats()
    {
        $total = Task::count();
        $completed = Task::where('complete', '=', 1)->count();
        $leftToDo = (int)$total - $completed;
        $percDone = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 4);
        }

        //Based on Today's Date
        $today = Carbon::now();
        //Due within 2 Weeks
        $twoWeeksFromToday = $today->addWeeks(2);
        $dueWithinTwoWeeks = Task::where('complete', '=', 0)->where('next_td', '<', $twoWeeksFromToday)->count();

        $today = Carbon::now();
        $twoWeeksAgo = $today->subWeeks(2);
        $addedInTheLast2Weeks = Task::where('created_at', '>', $twoWeeksAgo)->count();
        $completedInTheLast2Weeks = Task::where('complete', '=', 1)->where('created_at', '>', $twoWeeksAgo)->count();

        array_push($this->genTaskStats,
            [
                'total' => $total,
                'completed' => $completed,
                'leftToDo' => $leftToDo,
                'percDone' => $percDone,
                'dueWithinTwoWeeks' => $dueWithinTwoWeeks,
                'addedInTheLastTwoWeeks' => $addedInTheLast2Weeks,
                'completedInTheLastTwoWeeks' => $completedInTheLast2Weeks,
            ]);

        return $this->genTaskStats;

    }


    /* ------------------------------------------
     *  Users Dashboard - Stats
     * ------------------------------------------
     */
    public function getUserTaskStats()
    {
        //User Selections
        $user = \Auth::getUser();
        $userAreas = $user->area;
        $userGroups = $user->group;
        $userStages = $user->stage;

        $taskGroup = [];

        $query = New Task();
        $mainQuery = New Task();

        $taskGroup = $this->getPastDueUserTaskBreakDownByGroup($userAreas, $userGroups, $userStages);

        if ($userAreas !== '') {
            $userAreas = explode(",", $userAreas);
            //$query = $query->whereIn('area_id', $userAreas);
            $mainQuery = $mainQuery->whereIn('area_id', $userAreas);
        }
        if ($userGroups !== '') {
            $userGroups = explode(",", $userGroups);
            //$query = $query->$query->whereIn('id', $userGroups)->get();
            $mainQuery = $mainQuery->whereIn('group_id', $userGroups);
        }
        if ($userStages !== '') {
            $userStages = explode(",", $userStages);
            //$query = $query->whereIn('stage_id', $userStages);
            $mainQuery = $mainQuery->whereIn('stage_id', $userStages);
        }

        $query = new Task();
        $queryStat = $mainQuery;
        $total = $queryStat->count();
        $completed = $query->where('complete', '=', 1)->count();
        $leftToDo = (int)$total - $completed;

        $percDone = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 4);
            if ($percDone < 1) {
                $percDone = '<1';
            }
        }

        //Past Due
        $today = Carbon::now();
        $pastDue = $queryStat->where('complete', '=', 0)
            ->where('next_td', '<', $today)
            ->count();

        //Due within 1 Week
        $today = Carbon::now();
        $oneWeekFromToday = $today->addWeeks(1);
        $dueWithinOneWeek = $queryStat->where('complete', '=', 0)
            ->where('next_td', '<', $oneWeekFromToday)
            ->count();
        $dueWithinOneWeek = $dueWithinOneWeek - $pastDue;

        //Due within 2 Weeks
        $today = Carbon::now();
        $twoWeeksFromToday = $today->addWeeks(2);
        $dueWithinTwoWeeks = $query->where('complete', '=', 0)
            ->where('next_td', '<=', $twoWeeksFromToday)
            ->count();
        $dueWithinTwoWeeks = $dueWithinTwoWeeks - $pastDue;

        //Due within 3 Weeks
        $today = Carbon::now();
        $threeWeeksFromToday = $today->addWeeks(3);
        $dueWithinThreeWeeks = $query->where('complete', '=', 0)
            ->where('next_td', '<=', $threeWeeksFromToday)
            ->count();
        $dueWithinThreeWeeks = $dueWithinThreeWeeks - $pastDue;

        //Due within 1 Month
        $today = Carbon::today();
        $oneMonthFromToday = $today->addWeeks(4);
        $dueWithinOneMonth = $query->where('complete', '=', 0)
            ->where('next_td', '<=', $oneMonthFromToday)
            ->count();
        $dueWithinOneMonth = $dueWithinOneMonth - $pastDue;

        //Due within 2 Months
        $today = Carbon::now();
        $twoMonthFromToday = $today->addWeeks(8);
        $dueWithinTwoMonths = $query->where('complete', '=', 0)
            ->where('next_td', '<=', $twoMonthFromToday)
            ->count();
        $dueWithinTwoMonths = $dueWithinTwoMonths - $pastDue;


        $today = Carbon::now();
        $twoWeeksAgo = $today->subWeeks(2);
        $addedInTheLast2Weeks = $query->where('created_at', '>', $twoWeeksAgo)->count();
        $completedInTheLast2Weeks = $query->where('complete', '=', 1)->where('created_at', '>', $twoWeeksAgo)->count();

        //Show the user some values rather than zero - see when some tasks are due
        //and how many
        $tasksDueCountForDisplay = $dueWithinOneWeek;
        $tasksDueTextForDisplay = 'Due within One Week';

        if ($dueWithinOneWeek === 0) {
            $tasksDueCountForDisplay = $dueWithinTwoWeeks;
            $tasksDueTextForDisplay = 'Due within Two weeks';
        }
        if ($dueWithinTwoWeeks === 0) {
            $tasksDueCountForDisplay = $dueWithinThreeWeeks;
            $tasksDueTextForDisplay = 'Due within Three weeks';
        }
        if ($dueWithinThreeWeeks === 0) {
            $tasksDueCountForDisplay = $dueWithinOneMonth;
            $tasksDueTextForDisplay = 'Due within One Month';
        }
        if ($dueWithinOneMonth === 0) {
            $tasksDueCountForDisplay = $dueWithinOneMonth;
            $tasksDueTextForDisplay = 'Due within Two Months';
        }

        $userStats['stats'] =
            [
                'query' => $query->toSql(),
                'mainQuery' => $mainQuery->toSql(),
                'userName' => $user->name,
                'total' => $total,
                'completed' => $completed,
                'leftToDo' => $leftToDo,
                'percDone' => $percDone,
                'pastDue' => $pastDue,
                'dueWithinOneWeek' => $dueWithinOneWeek,
                'dueWithinTwoWeeks' => $dueWithinTwoWeeks,
                'dueWithinThreeWeeks' => $dueWithinThreeWeeks,
                'dueWithinOneMonth' => $dueWithinOneMonth,
                'dueWithinTwoMonths' => $dueWithinTwoMonths,
                'addedInTheLastTwoWeeks' => $addedInTheLast2Weeks,
                'completedInTheLastTwoWeeks' => $completedInTheLast2Weeks,
                'tasksDueCountForDisplay' => $tasksDueCountForDisplay,
                'tasksDueTextForDisplay' => $tasksDueTextForDisplay,
            ];

        $userStats['groups'] = $taskGroup;

        return $userStats;

    }

    /*------------------------------------------
     *   Users Dashboard - Task by Group
     * ------------------------------------------
     */
    public function getPastDueUserTaskBreakDownByGroup($areas, $groups, $stages)
    {
        $groupQuery = new Group();
        $taskQuery = new Task();
        if ($areas !== '') {
            $areas = explode(",", $areas);
            // $taskQuery = $taskQuery->whereIn('area_id', [1]);
        }
        if ($groups !== '') {
            $groups = explode(",", $groups);
            $groupQuery->whereIn('group_id', $groups);
            $groupList = $groupQuery->whereIn('id', $groups)->where('complete', '=', 0)->get();
        } else {
            $groupList = $groupQuery->get();
        }
        if ($stages !== '') {
            $stages = explode(",", $stages);
            $taskQuery->whereIn('stage_id', $stages)->where('complete', '=', 0);
        }
        // $taskQuery = $taskQuery->whereIn('area_id', [13]);

        $groupCountArray = [];
        for ($i = 0; $i < sizeof($groupList); $i++) {

            $groupCount = $taskQuery->where('group_id', '=', $groupList[$i]->id)->count();

            array_push($groupCountArray, [$groupList[$i]->name => $groupCount]);
            $groupCountArray[$i] = [$groupList[$i]->name => $groupCount];
        }


        return $groupCountArray;
    }


    //USERS - Returns the Total, Completed,Left to Do and Complete % of all tasks
    public function getUserTaskStat($id)
    {

    }


    public function getTaskBreakDownByGroup()
    {
        $groupList = Group::all();
        $groupCountArray = [];
        for ($i = 0; $i < sizeof($groupList); $i++) {
            $groupCount = Task::where('group_id', '=', $groupList[$i]->id)->count();
            array_push($groupCountArray, [$groupList[$i]->name => $groupCount]);
            $groupCountArray[$i] = [$groupList[$i]->name => $groupCount];
        }
        array_push($this->taskBreakDownByGroup, $groupCountArray);

        return $this->taskBreakDownByGroup;

    }


    public function getAreaBreakDown()
    {
        $data = [];
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        for ($i = 0; $i < sizeof($areas); $i++) {
            $count = Task::where('area_id', '=', $areas[$i]->id)->count();
            if ($count > 0) {
                array_push($data, [$areas[$i]->name, $count]);
            }

        }

        return $data;

    }

    //----------------------------------------
    //              PROJECT
    //----------------------------------------
    /**
     *
     * Returns signal Model  Bar Graph Data - Progress bar chart
     *
     * @return array
     */
    public function getProjectBarHrsGraphData($id)
    {
        $stages = Stage::get();
        $data = [];
        $today = Carbon::now();
        $project = Project::find($id);

        /*----------------------------------
                     PREPARATION
        ----------------------------------*/
        $prepTargetPercentage = 0;
        $prepTotalTargetedHours = Task::where('project_id', '=', 1)
        ->where('stage_id', $stages[0]->id)
        ->sum('target_val');

        $prepTargetedHoursToday = Task::where('project_id', '=', 1)
            ->where('last_td', '<=', $today)
            ->where('stage_id', $stages[0]->id)
            ->sum('target_val');
        if ($prepTotalTargetedHours > 0) {
            $prepTargetPercentage = round(($prepTargetedHoursToday / $prepTotalTargetedHours), 3);
        }
        $prepActualEarnedPercentage = 0;
        $prepEarnedHoursToday = Task::where('project_id', '=', 1)
            //->where('next_td', '<=', $today)
            ->where('stage_id', $stages[0]->id)
            ->sum('earned_val');
        if ($prepTotalTargetedHours > 0) {
            $prepActualEarnedPercentage = round(($prepEarnedHoursToday / $prepTotalTargetedHours), 3);
        }
        /*----------------------------------
                     EXECUTION
        ----------------------------------*/
        $exeTargetPercentage = 0;
        $exeTotalTargetedHours = Task::where('project_id', '=', 1)
            ->where('stage_id', $stages[1]->id)
            ->sum('target_val');
        $exeTargetedHoursToday = Task::where('project_id', '=', 1)
            ->where('last_td', '<=', $today)
            ->where('stage_id', $stages[1]->id)
            ->sum('target_val');
        if ($exeTotalTargetedHours > 0) {
            $exeTargetPercentage = round(($exeTargetedHoursToday / $exeTotalTargetedHours), 3);
        }
        $exeActualEarnedPercentage = 0;
        $exeEarnedHoursToday = Task::where('project_id', '=', 1)
            //->where('next_td', '<=', $today)
            ->where('stage_id', $stages[1]->id)
            ->sum('earned_val');
        if ($exeTotalTargetedHours > 0) {
            $exeActualEarnedPercentage = round(($exeEarnedHoursToday / $exeTotalTargetedHours), 3);
        }

        array_push($data, ['Project', $prepTargetPercentage, $prepActualEarnedPercentage, $exeTargetPercentage, $exeActualEarnedPercentage]);

        return $data;
    }

    public function old_getProjectBarHrsGraphData($id)
    {
        $stages = Stage::get();
        $data = [];
        $today = Carbon::now();
        $project = Project::find($id);
        $prepDonePercent = 0;
        $prepBasePercent = 0;
        $exeDonePercent = 0;
        $execBasePercent = 0;

        /*----------------------------------
                     PREPARATION
        ----------------------------------*/
        //Where the Project Should Be
        //total number or hours for the project
        //total hours that should be targeted today
        //total hours earned by today
        $prepTargetPercent = 0;
        $px1 = Task::where('project_id', '=', 1)
            ->where('stage_id', $stages[0]->id)
            ->sum('target_val');

        //Prep Targeted % by today
        if ($px1 > 0) {
            $px2 = Task::where('project_id', '=', 1)
                ->where('stage_id', $stages[0]->id)
                ->sum('earned_val');
            $prepTargetPercent = round(($px2 / $px1),3);
        }

        //Where the project is
        //Prep Target %
        $prepActualPercent  = 0;
        $py1 = Task::where('project_id', '=', 1)
            ->where('next_td', '<', $today)
            ->where('stage_id', $stages[0]->id)
            ->sum('target_val');
        if ($py1 > 0) {
            $py2 = Task::where('project_id', '=', 1)
                ->where('next_td', '<', $today)
                ->where('stage_id', $stages[0]->id)
                ->sum('earned_val');
            $prepActualPercent = round(($py2 / $px1) , 3);
        }

        //EXECUTION
        $exeTargetPercent = 0;
        $ex1 = Task::where('project_id', '=', 1)
            ->where('stage_id', $stages[1]->id)
            ->sum('target_val');
        //Prep Actual %
        if ($ex1 > 0) {
            $ex2 = Task::where('project_id', '=', 1)
                ->where('stage_id', $stages[1]->id)
                ->sum('earned_val');
            $exeTargetPercent = round(($ex2 / $ex1), 3);
        }
        //Prep Target %
        $execActualPercent = 0;
        $ey1 = Task::where('project_id', '=', 1)
            ->where('stage_id', $stages[1]->id)
            ->where('next_td', '<', $today)
            ->sum('target_val');
        if ($ey1 > 0) {
            $ey2 = Task::where('project_id', '=', 1)
                ->where('next_td', '<', $today)
                ->where('stage_id', $stages[1]->id)
                ->sum('earned_val');
            $execActualPercent = round(($ey2 / $ey1) , 3);
        }

        $x = round($px2/$px1,3);

        //return $x;

        $out = [$px1,$px2,$py1,$py2,$px2/$px1];
        //return $out;

        array_push($data, ['Project', $prepTargetPercent, $prepActualPercent, $exeTargetPercent, $execActualPercent]);

        return $data;
    }

    /**
     *
     * Returns The gen Stat data for the Model e.g Past Due, num of tasks etc
     *
     * @return array
     */
    public function getProjectGenData($id)
    {

        $stages = Stage::get();

        //PREPARATION STATS
        $today = Carbon::now();
        $past_due_count = Task::where('next_td', '<', $today)
            ->where('stage_id', $stages[0]->id)
            ->where('complete', 0)
            ->count();
        $data['prep_past_due_count'] = $past_due_count;
        // ---------   GROUPS ---------
        $data['prep_past_due_group'] = [];
        // ---------   AREA - TASK GROUPS ---------
        $groupPastDueCount = $this->getProjectGroupLateList($id, $stages[0]->id);
        $data['prep_past_due_group'] = $groupPastDueCount;
        //-----  Gen Stats --------
        $total = Task::where('stage_id', $stages[0]->id)->count();
        $data['prep_total'] = $total;
        $completed = Task::where('stage_id', $stages[0]->id)->where('complete', '=', 1)->count();
        $data['prep_completed'] = $completed;
        $leftToDo = (int)$total - $completed;
        $data['prep_left_to_do'] = $leftToDo;
        $data['prep_done_percent'] = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 4);
            $data['prep_done_percent'] = $percDone;
        }
        //Based on Today's Date
        $today = Carbon::now();
        //Due within 2 Weeks
        $twoWeeksFromToday = $today->addDays(14);
        $today = Carbon::now();
        $dueWithinTwoWeeks = Task::where('stage_id', $stages[0]->id)->where('complete', '=', 0)
            ->where('next_td', '>=', $today)
            ->where('next_td', '<', $twoWeeksFromToday)
            ->count();
        $data['prep_due_in_two_weeks'] = $dueWithinTwoWeeks;


        //EXECUTION STATS
        $today = Carbon::now();
        $past_due_count = Task::where('next_td', '<', $today)
            ->where('stage_id', $stages[1]->id)
            ->where('complete', '=', 0)
            ->count();
        $data['exe_past_due_count'] = $past_due_count;
        // ---------   GROUPS ---------
        $data['exe_past_due_group'] = [];
        // ---------   AREA - TASK GROUPS ---------
        $groupPastDueCount = $this->getProjectGroupLateList($id, $stages[1]->id);
        $data['exe_past_due_group'] = $groupPastDueCount;

        //-----  Gen Stats --------
        $total = Task::where('stage_id', $stages[1]->id)->count();
        $data['exe_total'] = $total;

        $completed = Task::where('stage_id', $stages[1]->id)->where('complete', '=', 1)->count();
        $data['exe_completed'] = $completed;

        $leftToDo = (int)$total - $completed;
        $data['exe_left_to_do'] = $leftToDo;

        $data['exe_done_percent'] = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 4);
            $data['exe_done_percent'] = $percDone;
        }
        //Based on Today's Date
        $today = Carbon::now();
        //Due within 2 Weeks
        $twoWeeksFromToday = $today->addDays(14);
        $today = Carbon::now();
        $dueWithinTwoWeeks = Task::where('stage_id', $stages[1]->id)->where('complete', '=', 0)
            ->where('next_td', '>=', $today)
            ->where('next_td', '<', $twoWeeksFromToday)
            ->count();
        $data['exe_due_in_two_weeks'] = $dueWithinTwoWeeks;

        return $data;
    }

    /**
     *
     * Returns The Late List Task Group For an Area
     *
     * @return array
     */
    public function getProjectGroupLateList($id, $stage_id)
    {
        $today = Carbon::now();
        $data = [];
        $past_due_group_list = Task::where('next_td', '<', $today)
            ->groupBy('group_id')
            ->with('group')
            ->where('stage_id', '=', $stage_id)
            ->where('complete', '=', 0)
            ->get();

        for ($i = 0; $i < sizeof($past_due_group_list); $i++) {
            $groupCount = Task::where('group_id', '=', $past_due_group_list[$i]->group_id)
                ->where('next_td', '<', $today)
                ->where('stage_id', '=', $stage_id)
                // ->groupBy('group_id')
                ->where('complete', '=', 0)
                ->count();
            if (intval($groupCount) > 0) {
                //array_push($data, [$past_due_group_list[$i]->group->name,$groupCount]);
            }
            $data[$past_due_group_list[$i]->group->name] = $groupCount;
        }

        return $data;
    }
    /**
     *
     * Returns All Models Bar Graph Data
     *
     * @return array
     */
    //TODO Delete
    /*    public function getProjectsBarHrsGraphData(Model $model, $model_type)
        {
            $modelList = $model::all();
            $model_type_id = $model_type . '_id';
            $data = [];
            $today = Carbon::now();
            $basePercent = 0;
            $donePercent = 0;
            for ($i = 0; $i < sizeof($modelList); $i++) {
                $modelTargetHrs = Task::where($model_type_id, '=', $modelList[$i]->id)
                    ->sum('target_val');
                if ($modelTargetHrs > 0) {
                    $donePercent = ceil($modelTargetHrs / $modelTargetHrs * 100);
                    $earnedHrs = Task::where($model_type_id, '=', $modelList[$i]->id)
                        ->where('next_td', '<', $today)
                        ->sum('earned_val');
                    $basePercent = ceil($earnedHrs / $modelTargetHrs * 100);
                }
                if ($model_type === ' system') {
                    array_push($data, [$modelList[$i]->tag, (int)$donePercent, (int)$basePercent]);
                } else {
                    array_push($data, [$modelList[$i]->name, (int)$donePercent, (int)$basePercent]);
                }
            }

            return $data;
        }*/

    //----------------------------------------
    //              AREAS
    //----------------------------------------
    /**
     *
     * Returns All Area Bar Graph Data
     *
     * @return array
     */
    public function getAllAreasBarHrsGraphData()
    {
        $areaList = Area::all();
        $totalTargetHrs = Task::sum('target_val');
        $data = [];
        $today = Carbon::now();
        $basePercent = 0;
        $donePercent = 0;
        for ($i = 0; $i < sizeof($areaList); $i++) {

            $areaTargetHrs = Task::where('area_id', '=', $areaList[$i]->id)->sum('target_val');
            if ($areaTargetHrs > 0) {
                $donePercent = ceil($areaTargetHrs / $totalTargetHrs * 100);
                $earnedHrs = Task::where('area_id', '=', $areaList[$i]->id)
                    ->where('next_td', '<', $today)
                    ->sum('earned_val');
                $basePercent = ceil($earnedHrs / $totalTargetHrs * 100);
            }
            array_push($data, [$areaList[$i]->name, (int)13, (int)$basePercent]);
        }

        return $data;
    }

    /**
     *
     * Returns signal Area Bar Graph Data
     *
     * @return array
     */
    public function getAreaBarHrsGraphData($id)
    {

        $area = Area::find($id);
        $stages = Stage::get();
        $data = [];
        $today = Carbon::now();
        $project = Project::find($id);

        /*----------------------------------
                     PREPARATION
        ----------------------------------*/
        $prepTargetPercentage = 0;
        $prepTotalTargetedHours = Task::where('project_id', '=', 1)
            ->where('stage_id', $stages[0]->id)
            ->where('group_owner_id', '=', $area->id)
            ->sum('target_val');

        $prepTargetedHoursToday = Task::where('project_id', '=', 1)
            ->where('last_td', '<=', $today)
            ->where('group_owner_id', '=', $area->id)
            ->where('stage_id', $stages[0]->id)
            ->sum('target_val');

        if ($prepTotalTargetedHours > 0) {
            $prepTargetPercentage = round(($prepTargetedHoursToday / $prepTotalTargetedHours), 2);
        }
        $prepActualEarnedPercentage = 0;
        $prepEarnedHoursToday = Task::where('project_id', '=', 1)
           // ->where('last_td', '<=', $today)
            ->where('group_owner_id', '=', $area->id)
            ->where('stage_id', $stages[0]->id)
            ->sum('earned_val');
        if ($prepTotalTargetedHours > 0) {
            $prepActualEarnedPercentage = round(($prepEarnedHoursToday / $prepTotalTargetedHours), 2);
        }

        /*----------------------------------
                     EXECUTION
        ----------------------------------*/
        $exeTargetPercentage = 0;
        $exeTotalTargetedHours = Task::where('project_id', '=', 1)
            ->where('stage_id', $stages[1]->id)
            ->where('group_owner_id', '=', $area->id)
            ->sum('target_val');
        $exeTargetedHoursToday = Task::where('project_id', '=', 1)
            ->where('next_td', '<=', $today)
            ->where('group_owner_id', '=', $area->id)
            ->where('stage_id', $stages[1]->id)
            ->sum('target_val');
        if ($exeTotalTargetedHours > 0) {
            $exeTargetPercentage = round(($exeTargetedHoursToday / $exeTotalTargetedHours), 2);
        }
        $exeActualEarnedPercentage = 0;
        $exeTotalTargetedHours = Task::where('project_id', '=', 1)
            ->where('stage_id', $stages[1]->id)
            ->where('group_owner_id', '=', $area->id)
            ->sum('target_val');
        $exeEarnedHoursToday = Task::where('project_id', '=', 1)
           // ->where('next_td', '<=', $today)
            ->where('group_owner_id', '=', $area->id)
            ->where('stage_id', $stages[1]->id)
            ->sum('earned_val');
        if ($exeTotalTargetedHours > 0) {
            $exeActualEarnedPercentage = round(($exeEarnedHoursToday / $exeTotalTargetedHours), 2);
        }

        array_push($data, ['Project', $prepTargetPercentage, $prepActualEarnedPercentage, $exeTargetPercentage, $exeActualEarnedPercentage]);

        return $data;
    }

    public function old_getAreaBarHrsGraphData($id)
    {
        $area = Area::find($id);
        $stages = Stage::get();
        $data = [];
        $today = Carbon::now();
        $project = Project::find($id);
        $prepDonePercent = 0;
        $prepBasePercent = 0;
        $exeDonePercent = 0;
        $execBasePercent = 0;

        //PREPARATION
        $prepTargetHrs = Task::where('project_id', '=', 1)
            ->where('group_owner_id', '=', $area->id)
            ->where('stage_id', $stages[0]->id)
            ->sum('target_val');
        //Prep Actual %
        if ($prepTargetHrs > 0) {
            $prepEarnedHrs = Task::where('project_id', '=', 1)
                ->where('group_owner_id', '=', $area->id)
                ->where('stage_id', $stages[0]->id)
                ->sum('earned_val');
            $prepDonePercent = round(($prepEarnedHrs / $prepTargetHrs) , 1);
        }
        //Prep Target %
        $prepTargetHrs = Task::where('project_id', '=', 1)
            ->where('group_owner_id', '=', $area->id)
            ->where('next_td', '<', $today)
            ->where('stage_id', $stages[0]->id)
            ->sum('target_val');
        if ($prepTargetHrs > 0) {
            $prepEarnedHrs = Task::where('project_id', '=', 1)
                ->where('group_owner_id', '=', $area->id)
                ->where('next_td', '<', $today)
                ->where('stage_id', $stages[0]->id)
                ->sum('earned_val');
            $prepBasePercent = round(($prepEarnedHrs / $prepTargetHrs) , 1);
        }

        //EXECUTION
        $prepTargetHrs = Task::where('project_id', '=', 1)
            ->where('group_owner_id', '=', $area->id)
            ->where('stage_id', $stages[1]->id)
            ->sum('target_val');
        //Prep Actual %
        if ($prepTargetHrs > 0) {
            $prepEarnedHrs = Task::where('project_id', '=', 1)
                ->where('group_owner_id', '=', $area->id)
                ->where('stage_id', $stages[1]->id)
                ->sum('earned_val');
            $exeDonePercent = round(($prepEarnedHrs / $prepTargetHrs) , 1);
        }
        //Prep Target %
        $prepTargetHrs = Task::where('project_id', '=', 1)
            ->where('group_owner_id', '=', $area->id)
            ->where('next_td', '<', $today)
            ->where('stage_id', $stages[1]->id)
            ->sum('target_val');
        if ($prepTargetHrs > 0) {
            $prepEarnedHrs = Task::where('project_id', '=', 1)
                ->where('group_owner_id', '=', $area->id)
                ->where('next_td', '<', $today)
                ->where('stage_id', $stages[1]->id)
                ->sum('earned_val');
            $execBasePercent = round(($prepEarnedHrs / $prepTargetHrs) , 1);
        }

        array_push($data, ['Project', $prepBasePercent, $prepDonePercent, $execBasePercent, $exeDonePercent]);

        return $data;
    }

    /**
     *
     * Returns The gen Stat data for the Area e.g Past Due, num of tasks etc
     *
     * @return array
     */
    public function getAreaStageGenData($area_id, $stage_id)
    {
        //PREPARATION STATS
        $today = Carbon::now();
        $past_due_count = Task::where('next_td', '<', $today)
            ->where('group_owner_id', '=', $area_id)
            ->where('stage_id', $stage_id)
            ->where('complete', 0)
            ->count();
        $data['past_due_count'] = $past_due_count;
        // ---------   GROUPS ---------
        $data['past_due_group'] = [];
        // ---------   AREA - TASK GROUPS ---------
        $groupPastDueCount = $this->getAreaGroupLateList($area_id, $stage_id);
        $data['past_due_group'] = $groupPastDueCount;


        //-----  Gen Stats --------
        $total = Task::where('stage_id', $stage_id)->where('group_owner_id', '=', $area_id)->count();
        $data['total'] = $total;
        $completed = Task::where('stage_id', $stage_id)
            ->where('group_owner_id', '=', $area_id)
            ->where('complete', '=', 1)->count();

        $data['completed'] = $completed;
        $leftToDo = (int)$total - $completed;
        $data['left_to_do'] = $leftToDo;
        $data['done_percent'] = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 2);
            $data['done_percent'] = $percDone;
        }
        //Based on Today's Date
        $today = Carbon::now();
        //Due within 2 Weeks
        $twoWeeksFromToday = $today->addWeeks(2);
        $today = Carbon::now();
        $dueWithinTwoWeeks = Task::where('stage_id', $stage_id)
            ->where('group_owner_id', '=', $area_id)
            ->where('complete', '=', 0)
            ->where('next_td', '>=', $today)
            ->where('next_td', '<', $twoWeeksFromToday)
            ->count();
        $data['due_in_two_weeks'] = $dueWithinTwoWeeks;

        return $data;

    }

    //----------------------------------------------
    //              GROUPS - Stats
    //
    // GROUPS BAR CHARTS, PAST DUE and INFO
    // - i.e Area, Group, System
    //----------------------------------------------
    /**
     *
     * Returns signal Model  Bar Graph Data
     *
     * @return array
     */
    public function getGroupBarHrsGraphData($group_id, $area_id)
    {
        $stages = Stage::get();
        $area_id = (int)$area_id;
        $group_id = (int)$group_id;
        $data = [];
        $today = Carbon::now();
        $prepDonePercent = 0;
        $prepBasePercent = 0;
        $exeDonePercent = 0;
        $exeBasePercent = 0;
        //PREPARATION
        $prepTargetHrs = Task::where('project_id', '=', 1)
            ->where('group_owner_id', '=', $area_id)
            ->where('group_id', '=', $group_id)
            ->where('stage_id', $stages[0]->id)
            ->sum('target_val');
        //Prep Actual %
        if ($prepTargetHrs > 0) {
            $prepEarnedHrs = Task::where('project_id', '=', 1)
                ->where('group_owner_id', '=', $area_id)
                ->where('group_id', '=', $group_id)
                ->where('stage_id', $stages[0]->id)
                ->sum('earned_val');
            $prepDonePercent = round(($prepEarnedHrs / $prepTargetHrs) / 100, 9);
        }
        //Prep Target %
        $prepTargetHrs = Task::where('project_id', '=', 1)
            ->where('group_owner_id', '=', $area_id)
            ->where('group_id', '=', $group_id)
            ->where('next_td', '<', $today)
            ->where('stage_id', $stages[0]->id)
            ->sum('target_val');
        if ($prepTargetHrs > 0) {
            $prepEarnedHrs = Task::where('project_id', '=', 1)
                ->where('group_owner_id', '=', $area_id)
                ->where('group_id', '=', $group_id)
                ->where('next_td', '<', $today)
                ->where('stage_id', $stages[0]->id)
                ->sum('earned_val');
            $prepBasePercent = round(($prepEarnedHrs / $prepTargetHrs) / 100, 5);
        }
        //EXECUTION
        $prepTargetHrs = Task::where('project_id', '=', 1)
            ->where('group_owner_id', '=', $area_id)
            ->where('group_id', '=', $group_id)
            ->where('stage_id', $stages[1]->id)
            ->sum('target_val');
        //Prep Actual %
        if ($prepTargetHrs > 0) {
            $prepEarnedHrs = Task::where('project_id', '=', 1)
                ->where('group_owner_id', '=', $area_id)
                ->where('group_id', '=', $group_id)
                ->where('stage_id', $stages[1]->id)
                ->sum('earned_val');
            $exeDonePercent = round(($prepEarnedHrs / $prepTargetHrs) / 100, 9);
        }
        //Prep Target %
        $prepTargetHrs = Task::where('project_id', '=', 1)
            ->where('group_owner_id', '=', $area_id)
            ->where('group_id', '=', $group_id)
            ->where('next_td', '<', $today)
            ->where('stage_id', $stages[1]->id)
            ->sum('target_val');
        if ($prepTargetHrs > 0) {
            $prepEarnedHrs = Task::where('project_id', '=', 1)
                ->where('group_owner_id', '=', $area_id)
                ->where('group_id', '=', $group_id)
                ->where('next_td', '<', $today)
                ->where('stage_id', $stages[1]->id)
                ->sum('earned_val');
            $exeBasePercent = round(($prepEarnedHrs / $prepTargetHrs) / 100, 5);
        }
        array_push($data, ['Project', $prepBasePercent, $prepDonePercent, $exeBasePercent, $exeDonePercent]);

        return $data;

    }

    /**
     *
     * Returns The gen Stat data for the Area e.g Past Due, num of tasks etc
     *
     * @return array
     */
    public function getAreaGroupStageGenData($area_id, $group_id, $stage_id)
    {
        //PREPARATION STATS
        $today = Carbon::now();
        $past_due_count = Task::where('next_td', '<', $today)
            ->where('group_id', '=', $group_id)
            ->where('group_owner_id', '=', $area_id)
            ->where('stage_id', $stage_id)
            ->where('complete', 0)
            ->count();
        $data['past_due_count'] = $past_due_count;
        // ---------   GROUPS ---------
        $data['past_due_group'] = [];
        // ---------   AREA - TASK GROUPS ---------
        $groupPastDueCount = $this->getAreaGroupTypeStageLateList($area_id, $group_id, $stage_id);
        $data['past_due_group'] = $groupPastDueCount;
        //-----  Gen Stats --------
        $total = Task::where('stage_id', $stage_id)
            ->where('group_owner_id', '=', $area_id)
            ->where('group_id', '=', $group_id)
            ->count();
        $data['total'] = $total;
        $completed = Task::where('stage_id', $stage_id)
            ->where('group_id', '=', $group_id)
            ->where('group_owner_id', '=', $area_id)
            ->where('complete', '=', 1)->count();
        $data['completed'] = $completed;
        $leftToDo = (int)$total - $completed;
        $data['left_to_do'] = $leftToDo;
        $data['done_percent'] = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 4);
            $data['done_percent'] = $percDone;
        }
        //Based on Today's Date
        $today = Carbon::now();
        //Due within 2 Weeks
        $twoWeeksFromToday = $today->addWeeks(2);
        $dueWithinTwoWeeks = Task::where('stage_id', $stage_id)
            ->where('group_id', '=', $group_id)
            ->where('group_owner_id', '=', $area_id)
            ->where('complete', '=', 0)
            ->where('next_td', '<', $twoWeeksFromToday)
            ->count();
        $data['due_in_two_weeks'] = $dueWithinTwoWeeks;

        return $data;

    }

    public function getAreaGroupTypeStageLateList($area_id, $group_id, $stage_id)
    {
        $today = Carbon::now();
        $data = [];
        $past_due_task_type_list = Task::where('area_id', '=', $area_id)
            ->where('stage_id', '=', $stage_id)
            ->where('group_id', '=', $group_id)
            ->where('next_td', '<', $today)
            ->groupBy('task_type_id')
            ->with('taskType')
            ->where('complete', '=', 0)
            ->get();
        for ($i = 0; $i < sizeof($past_due_task_type_list); $i++) {
            $groupCount = Task::where('task_type_id', '=', $past_due_task_type_list[$i]->task_type_id)
                ->where('stage_id', '=', $stage_id)
                ->where('group_id', '=', $group_id)
                ->where('area_id', '=', $area_id)
                ->where('next_td', '<', $today)
                ->where('complete', '=', 0)
                ->count();

            $data[$past_due_task_type_list[$i]->TaskType->name] = $groupCount;
        }

        return $data;
    }


    //----------------------------------------
    //              Task GROUP totals
    //----------------------------------------

    /**
     * Returns the number of task to be done in the next two weeks
     * Broken down by the Task Group
     * @return array
     */
    public function getTaskGroupStatsForNextTwoWeeks()
    {
        $groupList = Group::all();
        $total = 0;
        $groupCountArray = [];
        $today = Carbon::now();
        $twoWeeksFromToday = $today->addWeeks(2);
        for ($i = 0; $i < sizeof($groupList); $i++) {
            $groupCount = Task::where('complete', '=', 0)
                ->where('group_id', '=', $groupList[$i]->id)
                ->where('next_td', '<', $twoWeeksFromToday)->count();
            array_push($groupCountArray, [$groupList[$i]->name => $groupCount]);
            $groupCountArray[$i] = [$groupList[$i]->name => $groupCount];
            $total = $total + $groupCount;
        }
        array_push($this->groupStatsInTheNextTwoWeeks, $groupCountArray);
        array_push($this->groupStatsInTheNextTwoWeeks, $total);

        return $this->groupStatsInTheNextTwoWeeks;
    }

    /**
     *
     * Returns each Group number of tasks
     *
     * @return array
     */
    public function getGroupBreakDown()
    {
        //$g = Task::groupBy('group_id')->get();
        $groupList = Group::all();
        $data = [];
        for ($i = 0; $i < sizeof($groupList); $i++) {
            $groupCount = Task::where('group_id', '=', $groupList[$i]->id)->count();
            if (intval($groupCount) > 0) {
                array_push($data, [$groupList[$i]->name . '(' . $groupCount . ')', $groupCount]);
                //$data[$i]  = [$groupList[$i]->name.'('.$groupCount.')', $groupCount];
            }
        }
        $collection = collect($data);
        $sorted = $collection->sortBy('price');
        $sorted->values()->all();

        return $sorted->values()->all();
    }

    /**
     *  Returns each Group number of tasks that are complete
     * @return array
     */
    public function getGroupBreakDownCompleted()
    {
        //$g = Task::groupBy('group_id')->get();
        $groupList = Group::all();
        $data = [];
        for ($i = 0; $i < sizeof($groupList); $i++) {
            $groupCount = Task::where('group_id', '=', $groupList[$i]->id)->where('complete', '=', 1)->count();
            if (intval($groupCount) > 0) {
                array_push($data, [$groupList[$i]->name . '(' . $groupCount . ')', $groupCount]);
                //$data[$i]  = [$groupList[$i]->name.'('.$groupCount.')', $groupCount];
            }
        }
        $collection = collect($data);
        $sorted = $collection->sortBy('price');
        $sorted->values()->all();

        return $sorted->values()->all();
    }

    /**
     * Returns each Group number of tasks that are NOT complete
     * @return array
     */
    public function getGroupBreakDownNotComplete()
    {
        //$g = Task::groupBy('group_id')->get();
        $groupList = Group::all();
        $data = [];
        for ($i = 0; $i < sizeof($groupList); $i++) {
            $groupCount = Task::where('group_id', '=', $groupList[$i]->id)->where('complete', '=', 0)->count();
            if (intval($groupCount) > 0) {
                array_push($data, [$groupList[$i]->name . '(' . $groupCount . ')', $groupCount]);
                //$data[$i]  = [$groupList[$i]->name.'('.$groupCount.')', $groupCount];
            }
        }
        $collection = collect($data);
        $sorted = $collection->sortBy('price');
        $sorted->values()->all();

        return $sorted->values()->all();
    }


    //----------------------------------------
    //         Each GROUP by Task Type
    //----------------------------------------
    public function getTaskBreakDownByGroupTaskType()
    {
        $groupList = Group::all();
        $groupArray = [];
        $groupTaskTypeArray = [];
        $total = 0;
        for ($i = 0; $i < sizeof($groupList); $i++) {
            $taskTypes = TaskType::where('group_id', '=', $groupList[$i]->id)->whereNotIn('name', array('N/A'))->get();
            for ($j = 0; $j < sizeof($taskTypes); $j++) {
                $taskTypeCount = Task::where('task_type_id', '=', $taskTypes[$j]->id)->count();
                array_push($groupTaskTypeArray, [$taskTypes[$j]->short_name => $taskTypeCount]);
                $total = $total + $taskTypeCount;
            }
            array_push($groupArray, [$groupList[$i]->name => $groupTaskTypeArray]);
            //array_push($groupArray,[$groupList[$i]->description => $groupTaskTypeArray,'Total' =>$total]);
            $total = 0;
        }
        array_push($this->TaskBreakDownByGroupTaskType, $groupArray);

        return $this->TaskBreakDownByGroupTaskType;
    }


    /**
     * Returns the Group Tasks number by Task Type
     * @param $id
     * @return array
     */
    public function getGroupTaskTypeBreakDown($id)
    {
        $group = Group::find($id);
        $data = [];
        $taskTypes = TaskType::where('group_id', '=', $group->id)->whereNotIn('name', array('N/A'))->get();
        for ($i = 0; $i < sizeof($taskTypes); $i++) {
            $count = Task::where('task_type_id', '=', $taskTypes[$i]->id)->count();
            array_push($data, [$taskTypes[$i]->name, $count]);
        }

        return $data;
    }


    //----------------------------------------
    //                  SYSTEMS
    //----------------------------------------
    /**
     * Returns the Group Tasks number by Task Type
     * @param $id
     * @return array
     */
    public function getSystemTaskTypeBreakDown($id)
    {
        $system = System::find($id);
        $data = [];
        $pois = Task::select(\DB::raw("task_type_id,count(*) as count "))
            ->where('system_id', '=', 10)
            ->groupBy('task_type_id')
            ->with('taskType')
            ->get();

        return $pois;

        //task_type_id,count(*) as count FROM `novartis-asia`.tasks  WHERE system_id = 10 group by task_type_id

        $tasks = Task::where('system_id', '=', $id)->groupBy('task_type_id')->get();
        for ($i = 0; $i < sizeof($tasks); $i++) {
            $count = Task::where('task_type_id', '=', $tasks[$i]->id)->count();
            array_push($data, [$tasks[$i]->name, $count]);
        }

        return $data;
    }



    //----------------------------------------
    //              HOURS
    //----------------------------------------
    /**
     *
     * Returns each Group sum of Hours
     *
     * @return array
     */
    public function getGroupBreakDownHrs()
    {
        $groupList = Group::whereNotIn('name', array('N/A'))->get();
        $data = [];
        for ($i = 0; $i < sizeof($groupList); $i++) {
            //$total = DB::table('users')->sum('votes');
            $targetHrs = Task::where('group_id', '=', $groupList[$i]->id)->sum('target_val');
            $earnedHrs = Task::where('group_id', '=', $groupList[$i]->id)->sum('earned_val');
            if ($targetHrs > 0) {
                array_push($data, [$groupList[$i]->name, (int)$targetHrs, (int)$earnedHrs]);
            }


        }

        return $data;
    }



    /**
     *
     * Returns All Models Bar Graph Data
     *
     * @return array
     */
    //TODO delete this
    /*    public function getAllGroupsBarHrsGraphData(Model $model, $model_type, $area_id)
        {
            $modelList = $model::all();
            $model_type_id = $model_type . '_id';
            $data = [];
            $today = Carbon::now();
            $basePercent = 0;
            $donePercent = 0;
            for ($i = 0; $i < sizeof($modelList); $i++) {
                $modelTargetHrs = Task::where($model_type_id, '=', $modelList[$i]->id)
                    ->where('area_id', '=', $area_id)
                    ->sum('target_val');
                if ($modelTargetHrs > 0) {
                    $donePercent = ceil($modelTargetHrs / $modelTargetHrs * 100);
                    $earnedHrs = Task::where($model_type_id, '=', $modelList[$i]->id)
                        ->where('area_id', '=', $area_id)
                        ->where('next_td', '<', $today)
                        ->sum('earned_val');
                    $basePercent = ceil($earnedHrs / $modelTargetHrs * 100);
                }
                if ($model_type === ' system') {
                    array_push($data, [$modelList[$i]->tag, (int)$donePercent, (int)$basePercent]);
                } else {
                    array_push($data, [$modelList[$i]->name, (int)$donePercent, (int)$basePercent]);
                }
            }

            return $data;
        }*/
    //----------------------------------------------
    //
    //              SYSTEMS
    //
    //----------------------------------------------
    /**
     *
     * Returns All Models Bar Graph Data
     *
     * @return array
     */
    public function getAllSystemsBarHrsGraphData(Model $model, $model_type, $area_id)
    {
        $modelList = $model::all();
        $model_type_id = $model_type . '_id';
        $data = [];
        $today = Carbon::now();
        $basePercent = 0;
        $donePercent = 0;
        for ($i = 0; $i < sizeof($modelList); $i++) {
            $modelTargetHrs = Task::where($model_type_id, '=', $modelList[$i]->id)
                ->where('area_id', '=', $area_id)
                ->sum('target_val');
            if ($modelTargetHrs > 0) {
                $donePercent = ceil($modelTargetHrs / $modelTargetHrs * 100);
                $earnedHrs = Task::where($model_type_id, '=', $modelList[$i]->id)
                    ->where('area_id', '=', $area_id)
                    ->where('next_td', '<', $today)
                    ->sum('earned_val');
                $basePercent = ceil($earnedHrs / $modelTargetHrs * 100);
            }
            if ($model_type === ' system') {
                array_push($data, [$modelList[$i]->tag, (int)$donePercent, (int)$basePercent]);
            } else {
                array_push($data, [$modelList[$i]->name, (int)$donePercent, (int)$basePercent]);
            }
        }

        return $data;
    }

    /**
     *
     * Returns signal Model  Bar Graph Data
     *
     * @return array
     */
    public function getSystemBarHrsGraphData(Model $model, $model_type, $id, $area_id)
    {
        $data = [];
        $today = Carbon::now();
        $model = $model::find($id);
        $model_type_id = $model_type . '_id';
        $basePercent = 0;
        $donePercent = 0;
        $totalTargetHrs = Task::where($model_type_id, '=', $model->id)->where('area_id', '=', $area_id)->sum('target_val');

        $modelTargetHrs = Task::where($model_type_id, '=', $model->id)->sum('target_val');

        if ($totalTargetHrs > 0) {
            $donePercent = ceil($modelTargetHrs / $totalTargetHrs * 100);

            $earnedHrs = Task::where($model_type_id, '=', $model->id)
                ->where('area_id', '=', $area_id)
                ->where('next_td', '<', $today)
                ->sum('earned_val');
            $basePercent = ceil($earnedHrs / $totalTargetHrs * 100);
        }

        if ($model_type === ' system') {
            // array_push($data, [$model->tag, (int)12, (int)24]);
        } else {
            // array_push($data, [$model->name, (int)12, (int)24]);
        }
        array_push($data, [$model->name, (int)$donePercent, (int)$basePercent]);

        return $data;
    }

    /**
     *
     * Returns The gen Stat data for the Model e.g Past Due, num of tasks etc
     *
     * @return array
     */
    public function getSystemGenData(Model $model, $model_type, $id, $group_id)
    {
        $model = $model::find($id);
        $model_type_id = $model_type . '_id';

        $today = Carbon::now();
        $query = Task::where($model_type_id, '=', $model->id)
            ->where('next_td', '<', $today)->where('complete', '=', 0)->where('group_id', '=', $group_id);;

        $past_due_count = $query->count();

        $data['past_due_count'] = $past_due_count;

        // ---------   GROUPS ---------
        $data['past_due_group'] = [];
        // ---------   AREA - TASK GROUPS ---------
        if ($model_type === 'area') {
            $groupPastDueCount = $this->getAreaGroupLateList($id, 1);
            $data['past_due_group'] = $groupPastDueCount;
        }
        // ---------   TASK GROUP - TASK TYPES ---------
        if ($model_type === 'group') {
            $groupPastDueCount = $this->getTaskGroupTaskTypeLateList($id, $group_id);
            $data['past_due_group'] = $groupPastDueCount;
        }

        //-----  Gen Stats --------
        $query = Task::where($model_type_id, '=', $id)->where('group_id', '=', $group_id);;
        $total = $query->count();
        $data['total'] = $total;


        $completed = Task::where($model_type_id, '=', $model->id)
            ->where('group_id', '=', $group_id)
            ->where('complete', '=', 1)
            ->count();
        $completed = $query->where('complete', '=', 1)->count();
        $data['completed'] = $completed;

        $leftToDo = (int)$total - $completed;
        $data['left_to_do'] = $leftToDo;

        $data['done_percent'] = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 4);
            $data['done_percent'] = $percDone;
        }

        //Based on Today's Date
        $today = Carbon::now();
        //Due within 2 Weeks
        $twoWeeksFromToday = $today->addWeeks(2);
        $dueWithinTwoWeeks = Task::where($model_type_id, '=', $model->id)
            // ->where('area_id', '=', $area_id)
            ->where('complete', '=', 0)
            ->where('next_td', '<', $twoWeeksFromToday)
            ->count();
        $dueWithinTwoWeeksx = $query->where($model_type_id, '=', $model->id)
            ->where('group_id', '=', $group_id)
            ->where('complete', '=', 0)
            ->where('next_td', '<', $twoWeeksFromToday)
            ->count();

        $data['due_in_two_weeks'] = $dueWithinTwoWeeks;

        return $data;
    }

    public function getSystemPageStatsForGroup($group_id)
    {
        $data = [];
        $systemArray = Task::where('group_id', $group_id)->groupBy('system_id')->lists('system_id');
        $systems = System::find($systemArray->toArray());
        $taskCount = Task::where('group_id', '=', $group_id)
            ->count();
        $data['system_count'] = $systems->count();
        $data['task_count'] = $taskCount;
        $today = Carbon::now();


        $dataTempArray = [];
        for ($i = 0; $i < sizeof($systems); $i++) {

            $taskCount = Task::where('system_id', '=', $systems[$i]->id)->where('complete', '=', 0)->count();
            $taskComplete = Task::where('system_id', '=', $systems[$i]->id)->where('complete', '=', 1)->count();;
            $lateTasks = Task::where('system_id', '=', $systems[$i]->id)
                ->where('complete', '=', 0)
                ->where('next_td', '<', $today)
                ->count();

            $percentComplete = ceil($taskComplete / $taskCount) * 100;

            /*            array_push($dataTempArray, [
                            [$systems[$i]->tag . ' ,'.$systems[$i]->description],
                            [$percentComplete],
                            [$taskCount],
                            [$taskComplete],
                            [$lateTasks]
                        ]);*/

            array_push($dataTempArray, [
                ['system_tag' => $systems[$i]->tag],
                ['system_desc' => $systems[$i]->description],
                ['complete_percent' => $percentComplete],
                ['task_count' => $taskCount],
                ['task_complete' => $taskComplete],
                ['late_task_count' => $lateTasks]
            ]);

            /*           $dataTempArray[$systems[$i]->tag] =  [
                            ['system_tag'=>$systems[$i]->tag],
                            ['system_desc'=>$systems[$i]->description],
                            ['complete_percent'=>$percentComplete],
                            ['task_count'=>$taskCount],
                            ['task_complete'=>$taskComplete],
                            ['late_task_count'=>$lateTasks]
                        ];*/

            $data['data'] = $dataTempArray;
        }

        return $data;
    }
    /*
     * ==============================================================
     *                          MODELS
     * ==============================================================
     */
    //----------------------------------------------
    // MODELS BAR CHARTS, PAST DUE and INFO
    // - i.e Area, Group, System
    //----------------------------------------------
    /**
     *
     * Returns All Models Bar Graph Data
     *
     * @return array
     */
    public function getAllModelsBarHrsGraphData(Model $model, $model_type)
    {
        $modelList = $model::all();
        $model_type_id = $model_type . '_id';
        $data = [];
        $today = Carbon::now();
        $basePercent = 0;
        $donePercent = 0;
        for ($i = 0; $i < sizeof($modelList); $i++) {
            $modelTargetHrs = Task::where($model_type_id, '=', $modelList[$i]->id)
                ->sum('target_val');
            if ($modelTargetHrs > 0) {
                $donePercent = ceil($modelTargetHrs / $modelTargetHrs * 100);
                $earnedHrs = Task::where($model_type_id, '=', $modelList[$i]->id)
                    ->where('next_td', '<', $today)
                    ->sum('earned_val');
                $basePercent = ceil($earnedHrs / $modelTargetHrs * 100);
            }
            if ($model_type === ' system') {
                array_push($data, [$modelList[$i]->tag, (int)15, (int)$basePercent]);
            } else {
                array_push($data, [$modelList[$i]->name, (int)16, (int)$basePercent]);
            }
        }

        return $data;
    }

    /**
     *
     * Returns signal Model  Bar Graph Data
     *
     * @return array
     */
    public function getModelBarHrsGraphData(Model $model, $model_type, $id)
    {
        $model = $model::find($id);
        $model_type_id = $model_type . '_id';
        $totalTargetHrs = Task::where($model_type_id, '=', $model->id)
            ->sum('target_val');
        $data = [];
        $today = Carbon::now();
        $basePercent = 0;
        $donePercent = 0;
        $modelTargetHrs = Task::where($model_type_id, '=', $model->id)
            ->sum('target_val');
        if ($modelTargetHrs > 0) {

            $donePercent = ceil($modelTargetHrs / $totalTargetHrs);

            $earnedHrs = Task::where($model_type_id, '=', $model->id)
                ->where('next_td', '<', $today)
                ->sum('earned_val');
            $basePercent = ceil($earnedHrs / $totalTargetHrs * 100);
        }

        if ($basePercent === 0) {
            $basePercent = 1;
        }

        if ($model_type === ' system') {
            // array_push($data, [$model->tag, (int)12, (int)24]);
        } else {
            // array_push($data, [$model->name, (int)12, (int)24]);
        }

        array_push($data, [$model->name, (int)$donePercent, (int)$basePercent]);

        return $data;
    }

    /**
     *
     * Returns The gen Stat data for the Model e.g Past Due, num of tasks etc
     *
     * @return array
     */
    public function getModelGenData(Model $model, $model_type, $id, $stage_id)
    {
        $stages = Stage::get();
        foreach ($stages as $stage) {

        }
        $model = $model::find($id);
        $model_type_id = $model_type . '_id';

        $today = Carbon::now();
        $past_due_count = Task::where($model_type_id, '=', $id)
            ->where('stage_id', '=', $stage_id)
            ->where('next_td', '<', $today)
            ->where('complete', '=', 0)
            ->count();


        $data['past_due_count'] = $past_due_count;
        $dataExe['past_due_count'] = $past_due_count;

        // ---------   GROUPS ---------
        $data['past_due_group'] = [];
        // ---------   AREA - TASK GROUPS ---------
        if ($model_type === 'area') {
            $groupPastDueCount = $this->getAreaGroupLateList($id, $stage_id);
            $data['past_due_group'] = $groupPastDueCount;
            $dataExe['past_due_group'] = $groupPastDueCount;
        }
        // ---------   TASK GROUP - TASK TYPES ---------
        if ($model_type === 'group') {
            $groupPastDueCount = $this->getTaskGroupTaskTypeLateList($id, 1);
            $data['past_due_group'] = $groupPastDueCount;
            $dataExe['past_due_group'] = $groupPastDueCount;
        }

        //-----  Gen Stats --------
        $total = Task::where($model_type_id, '=', $id)
            ->where('stage_id', '=', $stage_id)
            ->count();
        $data['total'] = $total;
        $dataExe['total'] = $total;

        $completed = Task::where($model_type_id, '=', $model->id)
            ->where('complete', '=', $stage_id)
            ->where('stage_id', '=', $stage_id)
            ->count();
        $data['completed'] = $completed;
        $dataExe['completed'] = $completed;

        $leftToDo = (int)$total - $completed;
        $data['left_to_do'] = $leftToDo;
        $dataExe['left_to_do'] = $leftToDo;

        $data['done_percent'] = 0;
        $dataExe['done_percent'] = 0;
        if ($total > 0) {
            $percDone = round(($completed / $total), 4);
            $data['done_percent'] = $percDone;
            $dataExe['done_percent'] = $percDone;
        }

        //Based on Today's Date
        $today = Carbon::now();
        //Due within 2 Weeks
        $twoWeeksFromToday = $today->addWeeks(2);
        $dueWithinTwoWeeks = Task::where($model_type_id, '=', $model->id)
            ->where('complete', '=', 0)
            ->where('stage_id', '=', 1)
            ->where('next_td', '<', $twoWeeksFromToday)
            ->count();
        $data['due_in_two_weeks'] = $dueWithinTwoWeeks;
        $dataExe['due_in_two_weeks'] = $dueWithinTwoWeeks;

        $areaStats = ['gen' => $data, 'exe' => $dataExe];


        return $data;
    }


    //----------------------------------------
    //       SUB GROUPS FOR LATE LIST
    //----------------------------------------


    /**
     *
     * Returns The Late List Task Group For an Area
     *
     * @return array
     */
    public function getAreaGroupLateList($area_id, $stage_id)
    {
        $today = Carbon::now();
        $data = [];
        $past_due_group_list = Task::where('group_owner_id', '=', $area_id)
            ->where('stage_id', '=', $stage_id)
            ->where('next_td', '<', $today)
            ->groupBy('group_id')
            ->with('group')
            ->where('complete', '=', 0)
            ->get();
        for ($i = 0; $i < sizeof($past_due_group_list); $i++) {
            $groupCount = Task::where('group_id', '=', $past_due_group_list[$i]->group_id)
                ->where('stage_id', '=', $stage_id)
                ->where('group_owner_id', '=', $area_id)
                ->where('next_td', '<', $today)
                ->where('complete', '=', 0)
                ->count();

            $data[$past_due_group_list[$i]->group->name] = $groupCount;
        }

        return $data;
    }


    /**
     *
     * Returns The Late List Task Group For an Area
     *
     * @return array
     */
    public function getTaskGroupTaskTypeLateList($id, $area_id)
    {
        $today = Carbon::now();
        $data = [];
        $query = Task::where('group_id', '=', $id)
            ->where('next_td', '<', $today)
            ->groupBy('task_type_id')
            ->with('taskType')
            ->where('complete', '=', 0);

        if ($area_id > 0) {
            $query = $query->where('area_id', $area_id);
        }

        $past_due_task_type_list = $query->get();


        for ($i = 0; $i < sizeof($past_due_task_type_list); $i++) {
            $groupCount = Task::where('task_type_id', '=', $past_due_task_type_list[$i]->task_type_id)
                ->where('group_id', '=', $id)
                ->where('next_td', '<', $today)
                ->groupBy('task_type_id')
                ->where('complete', '=', 0)
                ->count();
            if (intval($groupCount) > 0) {
                //array_push($data, [$past_due_group_list[$i]->group->name,$groupCount]);
            }
            $data[$past_due_task_type_list[$i]->taskType->short_name] = $groupCount;
        }

        return $data;
    }


    /**
     *
     * Returns each Group sum of Hours
     *
     * @return array
     */
    public function getAreasBarHrs()
    {
        $areaList = Area::all();
        $totalTargetHrs = Task::sum('target_val');
        $data = [];
        $today = Carbon::now();
        $basePercent = 0;
        $donePercent = 0;
        if ($totalTargetHrs > 0) {

            for ($i = 0; $i < sizeof($areaList); $i++) {

                $areaCompleteTargetHrs = Task::where('area_id', '=', $areaList[$i]->id)->sum('earned_val');
                $donePercent = ceil($areaCompleteTargetHrs / $totalTargetHrs * 100);

                $earnedHrs = Task::where('area_id', '=', $areaList[$i]->id)->where('next_td', '<',
                    $today)->sum('target_val');
                $basePercent = ceil($earnedHrs / $totalTargetHrs * 100);

                $name = $areaList[$i]->description . ' (' . $areaList[$i]->name . ')';

                // array_push($data, [$name, (int)$donePercent, (int)$basePercent]);
                array_push($data, [$name, $basePercent, $donePercent]);

            }
        }

        return $data;
    }


    public function totalNumOfTasks()
    {

        $num = Task::get();
    }


}
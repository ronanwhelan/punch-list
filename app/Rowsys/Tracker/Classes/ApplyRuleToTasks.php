<?php

namespace App\Rowsys\Tracker\Classes;

use App\Models\ScheduleDate;
use App\Models\TaskRule;
use App\Models\Task;
use App\Models\TaskScheduleLink;
use App\Models\TaskStepPhase;
use App\Models\TaskTypeStepPhase;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Psy\Test\CodeCleaner\CallTimePassByReferencePassTest;

/**
 * Class ApplyRuleToTasks
 * @package App\Rowsys\Tracker\Classes
 */
class ApplyRuleToTasks
{
    protected $rule;
    protected $message;
    protected $projectId;
    protected $nextTargetDate;
    protected $updates = [];
    protected $taskDates = [];
    protected $numOfTasksUpdated;
    protected $jobFailed;

    //todo ALL CRON Jobs - applying rules to tasks
    /**
     *
     */
    function __construct()
    {
        //not used for now
    }

    /**
     * ---------------------------------
     *    Apply the Rule to ALl The Tasks
     *  ---------------------------------
     *
     * Applies the Given Rule to all related Tasks
     *
     * Uses the Chunk Method to not eat up all the RAM
     *
     * @param TaskRule $rule
     */
    public function applyThisRuleToAllRelatedTasks(TaskRule $rule)
    {
        $this->numOfTasksUpdated = 0;
        $this->rule = $rule;
        $count = 0;

        /** --  From laravel documents  --
         *  If you need to process a lot (thousands) of Eloquent records,
         *  using the chunk command will allow you to do without eating all of your RAM
         */
        //$this->numOfTasksUpdated = $this->rule->id . $this->rule->stage_id ;
        //Find an Update al Effected Tasks
        Task::where('task_type_id', '=', $rule->task_type_id)
            ->where('stage_id', '=', $rule->stage_id)
            ->where('complete', '=', 0)
            ->orderBy('id','ASC')
            ->chunk(1500, function ($tasks) use($count) {
                foreach ($tasks as $task) {
                    $this->applyThisRuleToThisTask($this->rule, $task);
                    $count ++;
                    echo $count . PHP_EOL;
                }
            });
    }

    /**
     * ---------------------------------------
     *    Apply the Rule to a Single Task
     *  -------------------------------------
     *
     *  Applies the Given Rule to the given task
     *
     * @param Task $task
     * @param TaskRule $rule
     */
    public function applyThisRuleToThisTask(TaskRule $rule, Task $task)
    {
        $updates = $this->GetThisTaskUpdatesForThisRule($task, $rule);

        $this->applyUpdatesToTask($updates, $task);
        $this->numOfTasksUpdated++;
    }


    /**
     *  ------------------------------------------------
     *   Apply the Rule to the Task with This ID
     *  -----------------------------------------------
     * @param $taskId
     */
    public function applyTheRuleToThisTaskId($taskId)
    {
        $task = Task::find($taskId);
        $this->compileThisTask($task);

    }

    /**
     * ------------------------------------------------
     *    Compile the Give Task
     *  -----------------------------------------------
     * Apply the Rule to the Given Task
     * @param Task $task
     */
    public function compileThisTask(Task $task)
    {
        $rule = TaskRule::where('task_type_id', '=', $task->task_type_id)->where('stage_id', '=', $task->stage_id)->first();

        if (isset($rule)) {
            $updates = $this->GetThisTaskUpdatesForThisRule($task, $rule);
            $this->applyUpdatesToTask($updates, $task);
            $this->message = "";
        } else {
            $this->message = "No Task Rule for this Task Type";
            $this->jobFailed = true;
            $task->compile_error = 2;
            $task->save();
        }
    }

    /**
     *  ------------------------------------------------
     *   Compile these tasks
     *  -----------------------------------------------
     * @param $tasks
     * @internal param $taskId
     */
    public function compileTheseTasks($tasks)
    {
        foreach ($tasks as $task){
            $this->compileThisTask($task);
        }
    }


    /**
     * ------------------------------------------------
     *    Get the Task Updates After Applying the Rule
     *  -----------------------------------------------
     *
     *  Applies the Given Rule to the given task
     * @param Task $task
     * @param TaskRule $rule
     * @return array
     */
    public function GetThisTaskUpdatesForThisRule(Task $task, TaskRule $rule)
    {
        $noScheduleDate = false;
        $this->updates['error'] = 0;
        $dateForRule = $task->prim_date;
        $step1Date = $task->prim_date;
        $step2To4Date = $task->prim_date;

        $scheduleDate = ScheduleDate::where('number', '=', trim($task->schedule_number))->first();

        //see if multi_date is turned on and the Task is an Execution stag - if not do nothing else
        // the Execution step must be assigned to the start date and the other steps to the end date.
        $multiDateOn = false;
        $multiDateConfigChoice = \Config::get('figaro.multi-date', 'false');
        if ($rule->multi_date === 1 && $task->stage_id === 2 && $multiDateConfigChoice) {
            $multiDateOn = true;
        }


        if (isset($scheduleDate)) {

            $newTargetDate = clone $scheduleDate->finish_date;

            if ($task->schedule_date_choice === 'start' || $task->schedule_date_choice === 'Start') {
                $newTargetDate = clone $scheduleDate->start_date;
            }
            if ($task->schedule_lag_days > 0) {
                $days = (int)$task->schedule_lag_days;
                $newTargetDate->addDays($days);
            }
            if ($task->schedule_lag_days < 0) {
                $days = -(int)$task->schedule_lag_days;
                $newTargetDate->subDays($days);
            }

            $this->updates['prim_date'] = clone $newTargetDate;
            $dateForRule = clone $newTargetDate;
            $step1Date = clone $newTargetDate;
            $step2To4Date = clone $newTargetDate;

            if ($multiDateOn) {

                $step1Date = clone $scheduleDate->start_date;
                $step2To4Date = clone $scheduleDate->finish_date;

                if ($task->schedule_lag_days > 0) {
                    $days = (int)$task->schedule_lag_days;
                    $step1Date->addDays($days);
                    $step2To4Date->addDays($days);
                }

                if ($task->schedule_lag_days < 0) {
                    $days = -(int)$task->schedule_lag_days;
                    $step1Date->subDays($days);
                    $step2To4Date->subDays($days);
                }
            }
        } //If no Schedule Date error the task
        else {
            $this->updates['error'] = 1;
            $noScheduleDate = true;
            $this->updates['prim_date'] = $task->prim_date;
            //echo 'error: no schedule date'.PHP_EOL;
        }
        //The date to use for the Rule
        $appDate = Carbon::create(2000, 1, 1, 0, 0, 0);
        if ($task->revised_projected_date > $appDate) {
            //$dateForRule = $task->revised_projected_date;
        }

        //-------------------------------
        //Number & Description
        //-------------------------------
        $systemTag = '';
        $systemDesc = '';
        if ($task->system_id !== 0) {
            $systemTag = $task->system->tag;
            if ($task->system->tag !== 'N/A') {
                $systemDesc = $task->system->description;
                $systemTag = $systemTag . '.';
            } else {
                $systemTag = '';
                $systemDesc = '';
            }
        }

        //sub type
        $subTypeNumber = '';
        $subTypeDesc = '';
        //echo $task->task_sub_type_id. "---".PHP_EOL;
        if ($task->task_sub_type_id !== 0) {
            if ($task->taskSubType->name !== 'N/A') {
                $subTypeNumber = '.' . $task->taskSubType->name;
                $subTypeDesc = ' ' . $task->taskSubType->description;
            }
        }
        //Add on
        $addOn = '';
        $addOnDesc = '';
        if ($task->add_on !== '') {
            $addOn = '.' . $task->add_on;
            $addOnDesc = ' ' . $task->add_on_desc;
        }
        //Stage

        $number = $task->stage->short_name . '.' . $systemTag . $task->taskType->short_name . $subTypeNumber . $addOn;
        $description = $task->stage->name . ' of  ' . $systemDesc . ' ' . $task->taskType->name . ' ' . $subTypeDesc . ' ' . $task->add_on_desc . ' ' . $task->document_number;

        $this->updates['number'] = $number;
        $this->updates['description'] = $description;

        $this->updates['tag'] = $task->stage->name . '.' . $task->taskType->name . '.' . $task->id;


        // -- Set the Date For each Step
        // ------ GEN STEP  ---------
        $cloneOfDateForRule = clone $step1Date;
        //$cloneOfDateForRule = clone $dateForRule;
        $lagDays = $task->lag_days + $rule->gen_ex_buffer_days;
        if ($lagDays >= 0) {
            $this->updates['gen_td'] = $cloneOfDateForRule->addDays($lagDays);
        } else {
            $this->updates['gen_td'] = $cloneOfDateForRule->subDays(-$lagDays);
        }

        // ------ REV STEP  ---------
        $cloneOfDateForRule = clone $step2To4Date;
        //$cloneOfDateForRule = clone $dateForRule;
        $lagDays = $rule->rev_buffer_days + $task->lag_days;
        if ($lagDays >= 0) {
            $this->updates['rev_td'] = $cloneOfDateForRule->addDays($lagDays);
        } else {
            $this->updates['rev_td'] = $cloneOfDateForRule->subDays(-$lagDays);
        }
        // ------ RE-ISSU STEP  ---------
        $cloneOfDateForRule = clone $step2To4Date;
        //$cloneOfDateForRule = clone $dateForRule;
        $lagDays = $rule->re_issu_buffer_days + $task->lag_days;
        if ($lagDays >= 0) {
            $this->updates['re_issu_td'] = $cloneOfDateForRule->addDays($lagDays);
        } else {
            $this->updates['re_issu_td'] = $cloneOfDateForRule->subDays(-$lagDays);
        }
        // ------ S-OFF STEP  ---------
        $cloneOfDateForRule = clone $step2To4Date;
        //$cloneOfDateForRule = clone $dateForRule;
        $lagDays = $rule->s_off_buffer_days + $task->lag_days;
        if ($lagDays >= 0) {
            $this->updates['s_off_td'] = $cloneOfDateForRule->addDays($lagDays);
        } else {
            $this->updates['s_off_td'] = $cloneOfDateForRule->subDays(-$lagDays);
        }

        // -----------------------
        //  Step Percentage,
        //  Earned Hrs
        // -----------------------
        //dd($this->rule->gen_ex_status);
        // Gen / EX Step
        $stepStatusCount = 0;
        $stepCombinedStatus = 0;
        if ((int)$rule->gen_ex_applicable === 0) {
            $this->updates['gen_applicable'] = 0;
            $this->updates['gen_hrs'] = 0;
            $this->updates['gen_target_hrs'] = 0;
            $this->updates['step_1_status'] = 0;
            $this->updates['step_1_owner_id'] = 1;
        } else {
            if($task->gen_perc > 99){
                $task->gen_perc = 100;
            }
            $this->updates['gen_applicable'] = 1;
            $hrs = (($rule->gen_ex_hrs_perc * $task->target_val) / 100) * ($task->gen_perc / 100);
            $this->updates['gen_hrs'] = $hrs;
            $this->updates['gen_target_hrs'] = ($rule->gen_ex_hrs_perc * $task->target_val) / 100;
            //Step Phase
            $stepPhase1 = TaskStepPhase::where('task_id', $task->id)->where('step_number', 1)->first();
            if (count($stepPhase1) > 0) {
                $step1TaskStepPhase = $this->compileTaskStepPhase($stepPhase1);
                $this->updates['step_1_owner_id'] = $step1TaskStepPhase[0];
                $this->updates['step_1_status'] = $step1TaskStepPhase[1];
                $step1Status = $step1TaskStepPhase[1];
            } else {
                $this->updates['step_1_owner_id'] = 1;
                $step1Status = $task->gen_perc;
                $this->updates['step_1_status'] = $task->gen_perc;
            }
            $stepStatusCount++;
            $stepCombinedStatus = $stepCombinedStatus + $step1Status;
        }


        // Review Step
        if ((int)$rule->rev_applicable === 0) {
            $this->updates['rev_applicable'] = 0;
            $this->updates['rev_hrs'] = 0;
            $this->updates['rev_target_hrs'] = 0;
            $this->updates['step_2_status'] = 0;
            $this->updates['step_2_owner_id'] = 1;
        } else {
            if($task->rev_perc > 99){
                $task->rev_perc = 100;
            }
            $this->updates['rev_applicable'] = 1;
            $hrs = (($rule->rev_hrs_perc * $task->target_val) / 100) * ($task->rev_perc / 100);
            $this->updates['rev_hrs'] = $hrs;
            $this->updates['rev_target_hrs'] = ($rule->rev_hrs_perc * $task->target_val) / 100;

            //Step Phase
            $stepPhase2 = TaskStepPhase::where('task_id', $task->id)->where('step_number', 2)->first();
            if (count($stepPhase2) > 0) {

                $step2TaskStepPhase = $this->compileTaskStepPhase($stepPhase2);

                $this->updates['step_2_owner_id'] = $step2TaskStepPhase[0];
                $this->updates['step_2_status'] = $step2TaskStepPhase[1];
                $step2Status = $step2TaskStepPhase[1];
            } else {
                $this->updates['step_2_owner_id'] = 1;
                $this->updates['step_2_status'] = $task->rev_perc;
                $step2Status = $task->rev_perc;
            }
            $stepStatusCount++;
            $stepCombinedStatus = $stepCombinedStatus + $step2Status;
        }

        // Re Issue Step
        if ((int)$rule->re_issu_applicable === 0) {
            $this->updates['re_issu_applicable'] = 0;
            $this->updates['re_issu_hrs'] = 0;
            $this->updates['re_issu_target_hrs'] = 0;
            $this->updates['step_3_status'] = 0;
            $this->updates['step_3_owner_id'] = 1;
        } else {
            if($task->re_issu_perc > 99){
                $task->re_issu_perc = 100;
            }
            $this->updates['re_issu_applicable'] = 1;
            $hrs = (($rule->re_issu_hrs_perc * $task->target_val) / 100) * ($task->re_issu_perc / 100);
            $this->updates['re_issu_hrs'] = $hrs;
            $this->updates['re_issu_target_hrs'] = ($rule->re_issu_hrs_perc * $task->target_val) / 100;
            //Step Phase
            $stepPhase3 = TaskStepPhase::where('task_id', $task->id)->where('step_number', 3)->first();
            if (count($stepPhase3) > 0) {
                $step3TaskStepPhase = $this->compileTaskStepPhase($stepPhase3);
                $this->updates['step_3_owner_id'] = $step3TaskStepPhase[0];
                $this->updates['step_3_status'] = $step3TaskStepPhase[1];
                $step3Status = $step3TaskStepPhase[1];
            } else {
                $this->updates['step_3_owner_id'] = 1;
                $this->updates['step_3_status'] = $task->re_issu_perc;
                $step3Status = $task->re_issu_perc;
            }
            $stepStatusCount++;
            $stepCombinedStatus = $stepCombinedStatus + $step3Status;
        }


        // Sign Off Step
        if ((int)$rule->s_off_applicable === 0) {
            $this->updates['s_off_applicable'] = 0;
            $this->updates['s_off_hrs'] = 0;
            $this->updates['s_off_target_hrs'] = 0;
            $this->updates['step_4_status'] = 0;
            $this->updates['step_4_owner_id'] = 1;
        } else {
            if($task->s_off_perc > 99){
                $task->s_off_perc = 100;
            }

            $this->updates['s_off_applicable'] = 1;
            $hrs = (($rule->s_off_hrs_perc * $task->target_val) / 100) * ($task->s_off_perc / 100);
            $this->updates['s_off_hrs'] = $hrs;
            $this->updates['s_off_target_hrs'] = ($rule->s_off_hrs_perc * $task->target_val) / 100;
            //Step Phase
            $stepPhase4 = TaskStepPhase::where('task_id', $task->id)->where('step_number', 4)->first();
            if (count($stepPhase4) > 0) {
                $step4TaskStepPhase = $this->compileTaskStepPhase($stepPhase4);
                $this->updates['step_4_owner_id'] = $step4TaskStepPhase[0];
                $this->updates['step_4_status'] = $step4TaskStepPhase[1];
                $step4Status = $step4TaskStepPhase[1];
            } else {
                $this->updates['step_4_owner_id'] = 1;
                $this->updates['step_4_status'] = $task->s_off_perc;
                $step4Status = $task->s_off_perc;
            }
            $stepStatusCount++;
            $stepCombinedStatus = $stepCombinedStatus + $step4Status;
        }


        // ----------------------
        //  Next Target Date
        // ----------------------

        //Changed for Novartis - if the step % > 0 then the next td applicable steps target date is the 'Next Target Date'
        $overDueTasks = \Config::get('figaro.over-due-tasks', 'false');
        //
        $holdPosition = 0;

        if ($overDueTasks === true) {
            //Step 1 Date
            $this->updates['next_td'] = $this->updates['gen_td'];

            //Step 2 Date
            if ($task->gen_perc > 0 || $rule->gen_ex_applicable === 0) {
                $holdPosition = 1;
                $this->updates['next_td'] = $this->updates['rev_td'];
            }
            //Step 3 Date
            if (($task->rev_perc > 0 || $rule->rev_applicable === 0) && $holdPosition === 1) {
                $holdPosition = 2;
                $this->updates['next_td'] = $this->updates['re_issu_td'];

            }
            //Step 4 Date
            if (($task->re_issu_perc > 0 || $rule->re_issu_applicable === 0) && $holdPosition === 2) {
                $holdPosition = 3;
                $this->updates['next_td'] = $this->updates['s_off_td'];

            }
            if (($task->s_off_perc > 99 || $rule->s_off_applicable === 0) && $holdPosition === 3) {
                $holdPosition = 4;

            }

        } else {

            $this->updates['next_td'] = $this->updates['gen_td'];
            if ($task->gen_perc > 99 || $rule->gen_ex_applicable === 0) {
                $holdPosition = 1;
                $this->updates['next_td'] = $this->updates['rev_td'];
            }
            if (($task->rev_perc > 99 || $rule->rev_applicable === 0) && $holdPosition === 1) {
                $holdPosition = 2;
                $this->updates['next_td'] = $this->updates['re_issu_td'];
            }
            if (($task->re_issu_perc > 99 || $rule->re_issu_applicable === 0) && $holdPosition === 2) {
                $holdPosition = 3;
                $this->updates['next_td'] = $this->updates['s_off_td'];
            }
            if (($task->s_off_perc > 99 || $rule->s_off_applicable === 0) && $holdPosition === 3) {
                $holdPosition = 4;
            }

        }


        // ----------------------
        //  Last Target Date
        // ----------------------
        // this is used to to calculate the Deviation from Baseline - in days
        //Deviation from base line is the difference in days form the baseline to the last target date.
        // Viewable by whom??
        $lastDate = $this->updates['gen_td'];
        if ($rule->gen_ex_applicable === 1) {
            $lastDate = $this->updates['gen_td'];
        }
        if ($rule->rev_applicable === 1) {
            $lastDate = $this->updates['rev_td'];
        }
        if ($rule->re_issu_applicable === 1) {
            $lastDate = $this->updates['re_issu_td'];
        }
        if ($rule->s_off_applicable === 1) {
            $lastDate = $this->updates['s_off_td'];
        }
        $this->updates['last_td'] = $lastDate;

        // ----------------------
        //  Deviation From base Line - in days
        // ----------------------
        $this->updates['base_dev_days'] = $lastDate->diffInDays($task->base_date, false);

        // ----------------------
        //  Status
        // ----------------------
        $this->updates['status'] = ceil($stepCombinedStatus / $stepStatusCount);

        // ----------------------
        //  Projected Date
        // ----------------------
        $today = Carbon::now();
        $this->updates['projected_date'] = $task->prim_date;
        if ($task->prim_date <= $today) {
            $this->updates['projected_date'] = $today->addWeeks(1);
        }
        if ($task->complete) {
            $this->updates['projected_date'] = $task->complete_date;
        }
        // ----------------------
        //  Earned Value
        // ----------------------
        $this->updates['earned_val'] =
            $this->updates['gen_hrs'] +
            $this->updates['rev_hrs'] +
            $this->updates['re_issu_hrs'] +
            $this->updates['s_off_hrs'];



      $completeHold = 0;
        if ($task->gen_perc > 99 || $rule->gen_ex_applicable === 0) {
            $completeHold = 1;
        }
        if (($task->rev_perc > 99 || $rule->rev_applicable === 0) && $completeHold === 1) {
            $completeHold = 2;
        }
        if (($task->re_issu_perc > 99 || $rule->re_issu_applicable === 0) && $completeHold === 2) {
            $completeHold = 3;
        }
        if (($task->s_off_perc > 99 || $rule->s_off_applicable === 0) && $completeHold === 3) {
            $completeHold = 4;
        }

        // Complete Status (and date if completed)
        if ($completeHold === 4) {
            $this->updates['complete'] = 1;
            $this->updates['complete_date'] = Carbon::now();
        } else {
            $this->updates['complete'] = 0;
            $this->updates['complete_date'] = Carbon::create(2000, 1, 1, 0, 0, 0);
        }


        UpdateTaskEarnedValues::updateRuleValuesForEarnedValue($task,$rule);

        UpdateTaskEarnedValues::updateHrsForEarnedValue($task);

        return $this->updates;
    }

    /**
     * ------------------------------------------------
     *    Apply Updates to the Task and Save the Task
     *  -----------------------------------------------
     *
     * First get the New updates for the task,
     * then apply them to the task and save the task
     *
     * @param $updates
     * @param $task
     */
    public function applyUpdatesToTask($updates, $task)
    {
        // Update Task Parameters and save
        $task->number = $updates['number'];
        $task->description = $updates['description'];
        $task->prim_date = $updates['prim_date'];
        //$task->base_date = $updates['base_date'];
        //$task->tag = $this->updates['tag'];
        //Step 1
        $task->gen_applicable = $updates['gen_applicable'];
        $task->gen_td = $updates['gen_td'];
        $task->gen_hrs = $updates['gen_hrs'];
        $task->gen_target_hrs = $updates['gen_target_hrs'];
        $task->gen_perc = $updates['step_1_status'];
        //Step 2
        $task->rev_applicable = $updates['rev_applicable'];
        $task->rev_td = $updates['rev_td'];
        $task->rev_hrs = $updates['rev_hrs'];
        $task->rev_target_hrs = $updates['rev_target_hrs'];
        $task->rev_perc = $updates['step_2_status'];
        //Step 3
        $task->re_issu_applicable = $updates['re_issu_applicable'];
        $task->re_issu_td = $updates['re_issu_td'];
        $task->re_issu_hrs = $updates['re_issu_hrs'];
        $task->re_issu_target_hrs = $updates['re_issu_target_hrs'];
        $task->re_issu_perc = $updates['step_3_status'];
        //Step 4
        $task->s_off_applicable = $updates['s_off_applicable'];
        $task->s_off_td = $updates['s_off_td'];
        $task->s_off_hrs = $updates['s_off_hrs'];
        $task->s_off_target_hrs = $updates['s_off_target_hrs'];
        $task->s_off_perc = $updates['step_4_status'];

        $task->earned_val = $updates['earned_val'];
        $task->next_td = $updates['next_td'];
        $task->last_td = $updates['last_td'];
        $task->projected_date = $updates['projected_date'];
        $task->base_dev_days = $updates['base_dev_days'];
        $task->complete = $updates['complete'];
        $task->complete_date = $updates['complete_date'];
        $task->compile_error = $updates['error'];
        $task->status = $updates['status'];
        $task->step_1_owner_id = $updates['step_1_owner_id'];
        $task->step_2_owner_id = $updates['step_2_owner_id'];
        $task->step_3_owner_id = $updates['step_3_owner_id'];
        $task->step_4_owner_id = $updates['step_4_owner_id'];

        if($task->compiled === 0){
            $task->compiled_at = Carbon::now();
            $task->compiled = 1;
        }

        $task->save();

    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }


    /**
     * @param TaskStepPhase $taskStepPhase
     * @return int
     *
     * Compiles the Task Step Phase and returns the current owner id for the task
     */
    public function compileTaskStepPhase(TaskStepPhase $taskStepPhase)
    {

        $taskTypeStepPhase = TaskTypeStepPhase::find((int)$taskStepPhase->task_type_step_id);

        $taskStepPhase->p1_owner_id = $taskTypeStepPhase->p1_owner_id;
        $taskStepPhase->p2_owner_id = $taskTypeStepPhase->p2_owner_id;
        $taskStepPhase->p3_owner_id = $taskTypeStepPhase->p3_owner_id;
        $taskStepPhase->p4_owner_id = $taskTypeStepPhase->p4_owner_id;
        $taskStepPhase->p5_owner_id = $taskTypeStepPhase->p5_owner_id;
        $taskStepPhase->p6_owner_id = $taskTypeStepPhase->p6_owner_id;
        $taskStepPhase->p7_owner_id = $taskTypeStepPhase->p7_owner_id;
        $taskStepPhase->p8_owner_id = $taskTypeStepPhase->p8_owner_id;
        $taskStepPhase->p9_owner_id = $taskTypeStepPhase->p9_owner_id;
        $taskStepPhase->p10_owner_id = $taskTypeStepPhase->p10_owner_id;

        //update the Current Owner
        $count = $taskStepPhase->p1_status +
            $taskStepPhase->p2_status +
            $taskStepPhase->p3_status +
            $taskStepPhase->p4_status +
            $taskStepPhase->p5_status +
            $taskStepPhase->p6_status +
            $taskStepPhase->p7_status +
            $taskStepPhase->p8_status +
            $taskStepPhase->p9_status +
            $taskStepPhase->p10_status;

        $taskStepPhase->break_down_count = $taskTypeStepPhase->break_down_count;

        if ((int)$taskTypeStepPhase->break_down_count === 0) {
            //status
            if ($count > 0) {
                $status = round($count / $taskStepPhase->phase_count, 2) * 100;
                //$status = round($count/$taskStepPhase->phase_count,2);
            } else {
                $status = 0;
            }

            if ($status > 100) {
                $status = 100;
            }
            if ($status < 0) {
                $status = 0;
            }

            $taskStepPhase->status = $status;
            $nextPosition = $count + 1;
            $nextOwner = 'p' . $nextPosition . '_owner_id';

            $currentOwnerId = $taskTypeStepPhase->$nextOwner;
            if ($currentOwnerId === 0 || $count > 10) {

                $currentOwnerId = 1;
            }

        } else {

            $status = $taskStepPhase->status;
            $taskStepPhase->status = $status;
            $currentOwnerId = 1;
        }

        $taskStepPhase->save();

        $data = [$currentOwnerId, $status];

        return $data;

    }

}
<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 9/7/16
 * Time: 5:33 PM
 */

namespace App\Rowsys\Tracker\Classes;


use App\Models\Area;
use App\Models\Role;
use App\Models\Roster\AreaHour;
use App\Models\Roster\UserAreaHour;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskEarnedValue;
use App\Models\TaskRule;
use App\Models\TaskType;
use App\Models\UserFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class Charts
{
    protected $monthsFull = array(
        'Jan',
        'Feb',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    );
    protected $monthsShort = array(
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
    );
    protected $years = ['2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023'];

    protected $loopYearCountArray = [
        '2016' => 1,
        '2017' => 2,
        '2018' => 3,
        '2019' => 4,
        '2020' => 5,
        '2021' => 6,
    ];

    protected $loopYearHolder = 1;


    /**
     *  Return for each week the sum hours for the project
     *  by stage and teams
     *
     * @param $stages
     * @param $teams
     */
    public function sumHoursWeeklyBreakdown($stages, $teams)
    {

        $teamIds = Area::lists('id')->toArray();
        $stageIds = Stage::lists('id')->toArray();
        $query = "select distinct dates.weekNumber,dates.monthNumber,dates.year,ifnull(tasks.value,0)  value
                from (select distinct week(selected_date) as weekNumber, month(selected_date) as monthNumber, year(selected_date) as year
                from (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
                where selected_date between (select min(last_td) from tasks) and (select max(last_td) from tasks)) dates
                left outer join (select week(t1.last_td) as weekNumber, month(t1.last_td) as monthNumber, year(t1.last_td) as year, SUM(t1.target_val) as value
                from tasks t1
                where t1.stage_id IN  (%s) and t1.group_owner_id IN  (%s)
                group by year, monthNumber,weekNumber,stage_id) tasks
                ON tasks.weekNumber = dates.weekNumber and tasks.monthNumber = dates.monthNumber and tasks.year = dates.year
                order by dates.year,dates.monthNumber,dates.weekNumber;";

        $queryWithReplace = sprintf($query, implode(',', $stageIds), implode(',', $teamIds));
        //dd($queryWithReplace);
        $results = \DB::select(\DB::raw("$queryWithReplace"), array());
        dd($results);
    }

    /**
     *  Return for each week the sum hours for the project
     *  by stage and teams
     *
     * @param $stages
     * @param $teams
     */
    public function sumHoursWeeklyBreakdownFromTo($stages, $teams, $from, $to, $fieldToSum)
    {

        $teamIds = Area::lists('id')->toArray();
        $stageIds = Stage::lists('id')->toArray();
        $from = '2016-01-01';
        $to = '2016-12-31';
        $fieldToSum = 't1.target_val';//$fieldToSum = 't1.earned_val';

        $query = "select distinct dates.weekNumber,dates.monthNumber,dates.year,ifnull(tasks.value,0)  value
                from (select distinct week(selected_date) as weekNumber, month(selected_date) as monthNumber, year(selected_date) as year
                from (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
                where selected_date between '%s' and '%s') dates
                left outer join (select week(t1.last_td) as weekNumber, month(t1.last_td) as monthNumber, year(t1.last_td) as year, SUM(%s) as value
                from tasks t1
                where t1.stage_id IN  (%s) and t1.group_owner_id IN  (%s)
                group by year, monthNumber,weekNumber,stage_id) tasks
                ON tasks.weekNumber = dates.weekNumber and tasks.monthNumber = dates.monthNumber and tasks.year = dates.year
                order by dates.year,dates.monthNumber,dates.weekNumber;";

        $queryWithReplace = sprintf($query, $from, $to, $fieldToSum, implode(',', $stageIds), implode(',', $teamIds));
        dd($queryWithReplace);
        $results = \DB::select(\DB::raw("$queryWithReplace"), array());
        dd($results);
    }


    public function getFirstAndLastMonth($startMonth, $endMonth, $teams)
    {

        //Start Month
        if (isset($startMonth) && $startMonth !== '') {
            $monthSplit = explode("-", $startMonth);
            $startDate = Carbon::createFromDate($monthSplit[1], $monthSplit[0], '01');
        } else {
            $startDate = Task::whereIn('group_owner_id', $teams)->orderBy('last_td', 'asc')->get()->take(1)->lists('last_td')[0];
        }

        //End Month
        if (isset($endMonth) && $endMonth !== '') {
            $monthSplit = explode("-", $endMonth);
            $endDate = Carbon::createFromDate($monthSplit[1], $monthSplit[0], '01');

        } else {
            $endDate = Task::whereIn('group_owner_id', $teams)->orderBy('last_td', 'desc')->get()->take(1)->lists('last_td')[0];
        }

        $monthCount = $startDate->diffInMonths($endDate) + 2;
        $startMonthNum = $startDate->month - 1;
        $endMonthNum = $endDate->month + 2;

        return [
            'startMonth' => $startMonthNum,
            'finishMonth' => $endMonthNum,
            'count' => $monthCount,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'startYear' => $startDate->year,
        ];

    }

    /* -------------------------------------
            AREA MONTHLY - Projected Bar Graph
    ----------------------------------------*/

    public function areaProjectedBarChartData($teams, $fromMonth, $toMonth)
    {

        //$this->sumHoursWeeklyBreakdownFromTo('2016-01-01','2016-12-12',[],[],'t1.target_val');
        //$this->sumHoursWeeklyBreakdown([],[]);
        $barChartData = [];
        $teamIds = $teams;
//
//        $prepData = \DB::table('tasks')->select(\DB::raw('month(last_td) as monthNumber,SUM(target_val) as value,year(last_td) as year'))
//            ->where('stage_id', 1)
//            ->whereIn('group_owner_id', $teamIds)
//            ->groupBy('year')->groupBy('monthNumber')->orderBy('last_td', 'ASC')->get();
//        //$prepData = \DB::select('select month(last_td) as monthNumber,SUM(target_val) as value,year(last_td) as year from `tasks` where `stage_id` = 1 group by `year`, `monthNumber` order by year(last_td),MonthNumber');
//        //dd($prepData);
//
//        $exeData = \DB::table('tasks')->select(\DB::raw('month(last_td) as monthNumber,SUM(target_val) as value,year(last_td) as year'))
//            ->where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
//            ->groupBy('year')->groupBy('monthNumber')->orderBy('last_td', 'ASC')->get();
//        //$exeData = \DB::select('select month(last_td) as monthNumber,SUM(target_val) as value,year(last_td) as year from `tasks` where `stage_id` = 2 group by `year`, `monthNumber` order by year(last_td),MonthNumber');
        //dd($prepData);



        if ( true) {

            $labels = [];
            $prepAccum = 0;
            $exeAccum = 0;
            $prepAndExeAccum = 0;
            $spentAccum = 0;
            $earnedAccum = 0;
            $rosterAccum = 0;
            $dataForTaskStarted = 0;
            /* STACKED BAR CHART  */
            $c1Bar1 = [];
            $c1Bar2 = [];
            $c1Line1 = [];
            $c1Line2 = [];
            $c1Line3 = [];
            $c2Line1 = [];
            $c2Line2 = [];
            $c2Line3 = [];
            $c2Line4 = [];
            $c2Line5 = [];
            $c3Bar1 = [];
            $c3Bar2 = [];
            $c3Line1 = [];
            $c3Line2 = [];
            $c3Line3 = [];
            $c4Line1 = [];
            $c4Line2 = [];
            $c4Line3 = [];
            $c4Line4 = [];
            $c4Line5 = [];
            $earnedPerMonth = [];
            $spentPerMonth = [];
            $rosterPerMonth = [];
            $inCurrentMonth = false;
            $taskEarnedValue = 0;


            $startAndEndMonthDetails = $this->getFirstAndLastMonth($fromMonth, $toMonth, $teamIds);
            $timeQuery = Task::query();
            $timeQuery->select(\DB::raw('last_td,week(last_td,1) as weekNumber,month(last_td) as monthNumber,year(last_td) as year,count(*) as value'))
                ->where('next_td', '>', $startAndEndMonthDetails['startDate'])->where('last_td', '<', $startAndEndMonthDetails['endDate'])
                ->groupBy('year')->groupBy('weekNumber')->orderBy('year', 'ASC');
            $taskTimeDataWeeks = $timeQuery->get();

            //return $taskTimeDataWeeks;

            $startMonthYear =  $taskTimeDataWeeks[0]['monthNumber'].'-'.$taskTimeDataWeeks[0]['year'];
            $endMonthYear =  $taskTimeDataWeeks[(sizeof($taskTimeDataWeeks)-1)]['monthNumber'].'-'.$taskTimeDataWeeks[(sizeof($taskTimeDataWeeks)-1)]['year'];

            $startAndEndMonthDetails = $this->getFirstAndLastMonth($startMonthYear, $endMonthYear, $teamIds);

            $loopMonth = $startAndEndMonthDetails['startMonth'];

            $todaysMonth = Carbon::now()->month;
            $todaysYear = Carbon::now()->year;

            $loopYear = $startAndEndMonthDetails['startYear'];

            $loopYearShort = str_split($loopYear)[2].str_split($loopYear)[3];

            $chartFinishMonth = $startAndEndMonthDetails['count'];

            $this->loopYearHolder = array_search($loopYear, $this->years) + 1;

            if($loopMonth == 0){
                $loopMonth = 1;
            }

            for ($i = 1; $i <= $chartFinishMonth; $i++) {

                /* LABELS  */
                array_push($labels, $this->monthsShort[$loopMonth - 1] . '-' . $loopYearShort);

                //----------        STAGE 1 HOURS -----------------
                //Target
                //$s1TargetValMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)->whereMonth('last_td', '=', $loopMonth)->whereYear('last_td', '=', $loopYear)->sum('target_val');
                $s1Step1TargetMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('gen_td', '=', $loopMonth)->whereYear('gen_td', '=', $loopYear)
                    ->sum('gen_target_hrs');

                $s1Step2TargetMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('rev_td', '=', $loopMonth)->whereYear('rev_td', '=', $loopYear)
                    ->sum('rev_target_hrs');
                $s1Step3TargetMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('re_issu_td', '=', $loopMonth)->whereYear('re_issu_td', '=', $loopYear)
                    ->sum('re_issu_target_hrs');
                $s1Step4TargetMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('s_off_td', '=', $loopMonth)->whereYear('s_off_td', '=', $loopYear)
                    ->sum('s_off_target_hrs');

                $s1TotalTargetHours = $s1Step1TargetMonthCount + $s1Step2TargetMonthCount + $s1Step3TargetMonthCount + $s1Step4TargetMonthCount;


/*                //Earned
                $s1Step1EarnedMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('gen_complete_date', '=', $loopMonth)->whereYear('gen_complete_date', '=', $loopYear)
                    //->where('gen_complete_date', '>', $startOfYear)
                    ->where('gen_perc', '>=', 100)
                    ->sum('gen_hrs');
                // ->toSql();
                //dd($s1Step1EarnedMonthCount);
                $s1Step2EarnedMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('rev_complete_date', '=', $loopMonth)->whereYear('rev_complete_date', '=', $loopYear)
                    //->where('rev_complete_date', '>', $startOfYear)
                    ->where('rev_perc', '>=', 100)->sum('rev_hrs');
                $s1Step3EarnedMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('re_issu_complete_date', '=', $loopMonth)->whereYear('re_issu_complete_date', '=', $loopYear)
                    //->where('re_issu_complete_date', '>', $startOfYear)
                    ->where('re_issu_perc', '>=', 100)->sum('re_issu_hrs');
                $s1Step4EarnedMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('s_off_complete_date', '=', $loopMonth)->whereYear('s_off_complete_date', '=', $loopYear)
                    //->where('s_off_complete_date', '>', $startOfYear)
                    ->where('s_off_perc', '>=', 100)->sum('s_off_hrs');


                $s1TotalEarnedStepHours = $s1Step1EarnedMonthCount + $s1Step2EarnedMonthCount + $s1Step3EarnedMonthCount + $s1Step4EarnedMonthCount;*/


                //----------        STAGE 2 HOURS   -----------------
                //Projected
                $s2TargetValMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('last_td', '=', $loopMonth)->whereYear('last_td', '=', $loopYear)->sum('target_val');
                //Target
                $s2Step1TargetMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('gen_td', '=', $loopMonth)->whereYear('gen_td', '=', $loopYear)
                    ->sum('gen_target_hrs');
                $s2Step2TargetMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('rev_td', '=', $loopMonth)->whereYear('rev_td', '=', $loopYear)
                    ->sum('rev_target_hrs');
                $s2Step3TargetMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('re_issu_td', '=', $loopMonth)->whereYear('re_issu_td', '=', $loopYear)
                    ->sum('re_issu_target_hrs');
                $s2Step4TargetMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('s_off_td', '=', $loopMonth)->whereYear('s_off_td', '=', $loopYear)
                    ->sum('s_off_target_hrs');

                $s2TotalTargetHours = $s2Step1TargetMonthCount + $s2Step2TargetMonthCount + $s2Step3TargetMonthCount + $s2Step4TargetMonthCount;



   /*             //Earned
                $s2Step1EarnedMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('gen_complete_date', '=', $loopMonth)->whereYear('gen_complete_date', '=', $loopYear)
                    //->where('gen_complete_date', '>', $startOfYear)
                    ->where('gen_perc', '>=', 100)->sum('gen_hrs');
                $s2Step2EarnedMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('rev_complete_date', '=', $loopMonth)->whereYear('rev_complete_date', '=', $loopYear)
                    //->where('rev_complete_date', '>', $startOfYear)
                    ->where('rev_perc', '>=', 100)->sum('rev_hrs');
                $s2Step3EarnedMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('re_issu_complete_date', '=', $loopMonth)->whereYear('re_issu_complete_date', '=', $loopYear)
                    //->where('re_issu_complete_date', '>', $startOfYear)
                    ->where('re_issu_perc', '>=', 100)->sum('re_issu_hrs');
                $s2Step4EarnedMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)
                    ->whereMonth('s_off_complete_date', '=', $loopMonth)->whereYear('s_off_complete_date', '=', $loopYear)
                    //->where('s_off_complete_date', '>', $startOfYear)
                    ->where('s_off_perc', '>=', 100)->sum('s_off_hrs');

                $s2TotalEarnedStepHours = $s2Step1EarnedMonthCount + $s2Step2EarnedMonthCount + $s2Step3EarnedMonthCount + $s2Step4EarnedMonthCount;*/


                //Earned Value
                $taskEarnedValue  = (int) TaskEarnedValue::whereIn('group_owner_id', $teamIds)
                    ->whereMonth('created_at', '=', $loopMonth)->whereYear('created_at', '=', $loopYear)
                    ->sum('earned_hrs');
                //array_push($earnedPerMonth, round(($s1TotalEarnedStepHours + $s2TotalEarnedStepHours), 1));
                array_push($earnedPerMonth, $taskEarnedValue);




                //$s1EarnedValMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)->whereMonth('last_td', '=', $loopMonth)->whereYear('last_td', '=', $loopYear)->sum('earned_val');
                //$s2EarnedValMonthCount = Task::where('stage_id', 2)->whereIn('group_owner_id', $teamIds)->whereMonth('last_td', '=', $loopMonth)->whereYear('last_td', '=', $loopYear)->sum('earned_val');

                //----------    ROSTER   -----------------
                $rosterCount = (int)AreaHour::whereIn('area_id', $teamIds)->where('month_num', $loopMonth)->where('year', $loopYear)->sum('hours');

                array_push($rosterPerMonth, $rosterCount);


                //-----  VALUES FOR GRAPHS  ----
                $prepValue = (int)$s1TotalTargetHours;
                $prepAccum = $prepAccum + $prepValue;
                $exeValue = (int)$s2TotalTargetHours;
                $exeAccum = $exeAccum + $exeValue;
                $prepAndExeAccum = $prepAndExeAccum + $prepValue + $exeValue;
                $roster = (int)$rosterCount;
                $rosterAccum = $rosterAccum + $rosterCount;
                $earned = $taskEarnedValue;
                $earnedAccum = $earnedAccum + $earned;

                $spent = (int)UserAreaHour::whereIn('area_id', $teamIds)->where('month_num', $loopMonth)->where('year', $loopYear)->sum('hours');

                $spentPerMonth = $spent;

                $spentAccum = $spentAccum + $spent;
                //$spent = UserAreaHour::whereIn('area_id',$teamIds)->where('month_num',3)->where('year',$loopYear)->sum('hours');

                if ((int)$prepValue > 0 || (int)$exeValue > 0) {
                    $dataForTaskStarted = 1;
                    $dataHasStarted = true;
                }



                /* -----------------------------
                 *   CHART 1
                  -----------------------------*/
                //return $spent;
                /* CHART 1 -STACKED BAR CHART  */
                array_push($c1Bar1, ((int)$prepValue + (int)$exeValue));

                //Roster
                array_push($c1Line1, $roster);

                //Earned and Spent Amount
                if ( ! $inCurrentMonth) {
                    array_push($c1Line3, $spent);
                    array_push($c1Line2, $earned);
                }

                /* -----------------------------
                 *   CHART 2
                  -----------------------------*/
                /* CHART 2 - ACCUM LINE CHART  */
                array_push($c2Line1, $prepAndExeAccum);
                array_push($c2Line2, $exeAccum);
                array_push($c2Line3, $rosterAccum);
                if ($earned > 0 || true) {
                    array_push($c2Line4, $earnedAccum);
                }
                if ($spent > 0 || true) {
                    array_push($c2Line5, $spentAccum);
                }

                /* -----------------------------
                 *   CHART 3
                  -----------------------------*/
                /* CHART 3 - NON STACKED BAR CHART  */
                array_push($c3Bar1, (int)$prepValue);
                array_push($c3Bar2, (int)$exeValue);
                array_push($c3Line1, (int)$roster);

                /* -----------------------------
                 *   CHART 4
                  -----------------------------*/
                /* CHART 4 - NON STACKED BAR CHART  */
                array_push($c4Line1, (int)$roster);
                if ( ! $inCurrentMonth ) {
                    array_push($c4Line2, $earned);
                    array_push($c4Line3, $spent);

                }


                //Loop Month And Year
                $loopMonth++;
                if ($loopMonth > 12) {
                    $this->loopYearHolder++;
                    $loopMonth = 1;
                }
                if ($this->loopYearHolder === 1) {
                    $loopYear = '2016';
                    $loopYearShort = '16';
                }
                if ($this->loopYearHolder === 2) {
                    $loopYear = '2017';
                    $loopYearShort = '17';
                }
                if ($this->loopYearHolder === 3) {
                    $loopYear = '2018';
                    $loopYearShort = '18';
                }
                if ($this->loopYearHolder === 4) {
                    $loopYear = '2019';
                    $loopYearShort = '19';
                }
                if ($this->loopYearHolder === 5) {
                    $loopYear = '2020';
                    $loopYearShort = '20';
                }
                if ($this->loopYearHolder === 6) {
                    $loopYear = '2021';
                    $loopYearShort = '21';
                }

                if ($todaysMonth === ((int)$loopMonth-1) && $todaysYear == $loopYear) {
                    $inCurrentMonth = true;
                }

            }// END FOR LOOP


            $data = [];
            array_push($data, $labels);
            //1,2,3,4,5
            array_push($data, $c1Bar1);
            array_push($data, $c1Bar2);
            array_push($data, $c1Line1);
            array_push($data, $c1Line2);
            array_push($data, $c1Line3);
            //6,7,8,9,10
            array_push($data, $c2Line1);
            array_push($data, $c2Line2);
            array_push($data, $c2Line3);
            array_push($data, $c2Line4);
            array_push($data, $c2Line5);
            //11,12,13,14,15
            array_push($data, $c3Bar1);
            array_push($data, $c3Bar2);
            array_push($data, $c3Line1);
            //array_push($data, $c3Line2);
            //array_push($data, $c3Line3);
            //16,17,18,19,20
            array_push($data, $c4Line1);
            array_push($data, $c4Line2);
            array_push($data, $c4Line3);
            array_push($data, (int)Task::whereIn('group_owner_id', $teamIds)->sum('target_val'));
            array_push($data, $startAndEndMonthDetails['startDate']->format('d-m-Y'));
            array_push($data, $startAndEndMonthDetails['endDate']->addMonth(1)->format('d-m-Y'));
            array_push($data, $earnedPerMonth);
            array_push($data, $spentPerMonth);
            array_push($data, $rosterPerMonth);//22
            array_push($data, ['spentTotal' => $spentAccum]);//23
            array_push($data, ['rosterTotal' => AreaHour::sum('hours')]);//24
            array_push($data, ['projectTotalTarget' => Task::sum('target_val')]);//25
            array_push($data, ['projectTotalPrepTarget' => Task::where('stage_id', 1)->sum('target_val')]);//26
            array_push($data, ['projectTotalExecTarget' => Task::where('stage_id', 2)->sum('target_val')]);//27
            array_push($data, ['projectTotalEarned' => $earnedAccum]);//28
            return $data;

        }
        return $barChartData;
    }


    /* -------------------------------------
        AREA WEEKLY- Projected Bar Graph
    ----------------------------------------*/

    public function areasWeeklyProjectedBarChartData($teams, $fromMonth, $toMonth)
    {
        $barChartData = [];
        $teamIds = $teams;

        $labels = [];
        $prepAccum = 0;
        $exeAccum = 0;
        $prepAndExeAccum = 0;
        $spentAccum = 0;
        $earnedAccum = 0;
        $rosterAccum = 0;
        $dataForTaskStarted = 0;
        /* STACKED BAR CHART  */
        $c1Bar1 = [];
        $c1Bar2 = [];
        $c1Line1 = [];
        $c1Line2 = [];
        $c1Line3 = [];
        $c2Line1 = [];
        $c2Line2 = [];
        $c2Line3 = [];
        $c2Line4 = [];
        $c2Line5 = [];
        $c3Bar1 = [];
        $c3Bar2 = [];
        $c3Line1 = [];
        $c3Line2 = [];
        $c3Line3 = [];
        $c4Line1 = [];
        $c4Line2 = [];
        $c4Line3 = [];
        $c4Line4 = [];
        $c4Line5 = [];
        $earnedPerMonth = [];
        $spentPerMonth = [];
        $rosterPerMonth = [];


        /* ----------------------------------------------------
         *  Calculate the Start and Finish Dates
          ----------------------------------------------------*/
        //Month Details
        $startAndEndMonthDetails = $this->getFirstAndLastMonth($fromMonth, $toMonth, $teamIds);

        /* ----------------------------------------------------
         *  Get weekly Data
          ----------------------------------------------------*/
        $timeQuery = Task::query();
        //$timeQuery = $statQuery->select(\DB::raw('last_td,week(last_td) as weekNumber,month(last_td) as monthNumber,year(last_td) as year,count(*) as value'))->groupBy('year')->groupBy('weekNumber')->orderBy('year', 'ASC');
        $timeQuery->select(\DB::raw('last_td,week(last_td,1) as weekNumber,month(last_td) as monthNumber,year(last_td) as year,count(*) as value'))
            ->where('next_td', '>', $startAndEndMonthDetails['startDate'])->where('last_td', '<', $startAndEndMonthDetails['endDate'])
            ->whereIn('group_owner_id', $teamIds)
            ->groupBy('year')->groupBy('weekNumber')->orderBy('year', 'ASC');
        $taskTimeDataWeeks = $timeQuery->get();

        if (sizeof($taskTimeDataWeeks) > 0) {

            $startAndEndPoints = $this->startAndEndWeekNumber($taskTimeDataWeeks);

            $loopWeek = $startAndEndPoints['startWeek'];
            $loopYear = $startAndEndPoints['startYear'];
            $yearLoopCount = $this->loopYearCountArray[$startAndEndPoints['startYear']];
            $rosterHold = 0;
            $newMonth = true;
            $loopMonthHolder = 0;
            // return $startAndEndPoints;
            $numOfWeeksFroLoop = ($startAndEndPoints['numberOfWeeks'] + 2);

            $todaysWeek = Carbon::now()->weekOfYear;
            $todaysYear = Carbon::now()->year;
            $inCurrentWeek = false;

            if ($startAndEndPoints['numberOfWeeks'] > 0) {

                for ($i = $startAndEndPoints['startWeek'];
                     $i <= $numOfWeeksFroLoop;
                     $i++) {

                    $monthNum = Task::select(\DB::raw('MONTH(last_td) as MonthNumber'))
                        ->whereRaw('week(last_td,1) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)
                        ->first();

                    $loopMonth = (int)$monthNum['MonthNumber'];

                    $countOfWeeksRow = \DB::table('roster_weeks_in_month')//->where('month_num', $loopMonth)->where('year', '=', $loopYear)
                        ->where('month_num', $loopMonth)->where('year', $loopYear)
                        ->first();

                    $numOfWeeksOInMonth = 4;
                    if (isset($countOfWeeksRow) && (int)$countOfWeeksRow->week_count > 1) {
                        $numOfWeeksOInMonth = (int)$countOfWeeksRow->week_count;
                    }

                    /* LABELS  */
                    $dateTime = new \DateTime();
                    $dateTime->setISODate($loopYear, $loopWeek);
                    $month = $dateTime->format('n');
                    $day = $dateTime->format('d');
                    //array_push($labels, 'wk '.$loopWeek. '-' .$this->monthsShort[$month].'-'. $loopYear);
                    array_push($labels, '' . ($day) . ' ' . $this->monthsShort[$month - 1] . '-' . $loopYear);

                    //----------        STAGE 1 HOURS -----------------
                    //Target
                    //$s1TargetValMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)->whereMonth('last_td', '=', $loopMonth)->whereYear('last_td', '=', $loopYear)->sum('target_val');
                    $s1Step1TargetMonthCount = Task::where('stage_id', 1)
                        ->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(last_td,1) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)
                        ->sum('gen_target_hrs');

                    $s1Step2TargetMonthCount = Task::where('stage_id', 1)
                        ->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(last_td,1) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)
                        ->sum('rev_target_hrs');

                    $s1Step3TargetMonthCount = Task::where('stage_id', 1)
                        ->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(last_td,1) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)
                        ->sum('re_issu_target_hrs');

                    $s1Step4TargetMonthCount = Task::where('stage_id', 1)
                        ->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(last_td,1) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)
                        ->sum('s_off_target_hrs');

                    $s1TotalTargetHours = $s1Step1TargetMonthCount + $s1Step2TargetMonthCount + $s1Step3TargetMonthCount + $s1Step4TargetMonthCount;


/*                    //Earned
                    $s1Step1EarnedMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(gen_complete_date,1) = ?', [$loopWeek])->whereYear('gen_complete_date', '=', $loopYear)
                        ->where('gen_perc', '>=', 100)
                        ->sum('gen_hrs');

                    $s1Step2EarnedMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(rev_complete_date,1) = ?', [$loopWeek])->whereYear('rev_complete_date', '=', $loopYear)
                        ->where('rev_perc', '>=', 100)->sum('rev_hrs');

                    $s1Step3EarnedMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(re_issu_complete_date,1) = ?', [$loopWeek])->whereYear('re_issu_complete_date', '=', $loopYear)
                        ->where('re_issu_perc', '>=', 100)->sum('re_issu_hrs');

                    $s1Step4EarnedMonthCount = Task::where('stage_id', 1)->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(s_off_complete_date,1) = ?', [$loopWeek])->whereYear('s_off_complete_date', '=', $loopYear)
                        ->where('s_off_perc', '>=', 100)->sum('s_off_hrs');

                    $s1TotalEarnedStepHours = $s1Step1EarnedMonthCount + $s1Step2EarnedMonthCount + $s1Step3EarnedMonthCount + $s1Step4EarnedMonthCount;*/


                    //----------        STAGE 2 HOURS   -----------------
                    //Projected
                    $s2TargetValMonthCount = Task::where('stage_id', 2)
                        ->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(last_td,1) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)
                        ->sum('target_val');
                    //Target
                    $s2Step1TargetMonthCount = Task::where('stage_id', 2)
                        ->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(gen_td,1) = ?', [$loopWeek])->whereYear('gen_td', '=', $loopYear)
                        ->sum('gen_target_hrs');
                    $s2Step2TargetMonthCount = Task::where('stage_id', 2)
                        ->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(rev_td,1) = ?', [$loopWeek])->whereYear('rev_td', '=', $loopYear)
                        ->sum('rev_target_hrs');
                    $s2Step3TargetMonthCount = Task::where('stage_id', 2)
                        ->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(re_issu_td,1) = ?', [$loopWeek])->whereYear('re_issu_td', '=', $loopYear)
                        ->sum('re_issu_target_hrs');
                    $s2Step4TargetMonthCount = Task::where('stage_id', 2)
                        ->whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(s_off_td,1) = ?', [$loopWeek])->whereYear('s_off_td', '=', $loopYear)
                        ->sum('s_off_target_hrs');

                    $s2TotalTargetHours = $s2Step1TargetMonthCount + $s2Step2TargetMonthCount + $s2Step3TargetMonthCount + $s2Step4TargetMonthCount;


                    //Earned Value
                    $taskEarnedValue  = (int) TaskEarnedValue::whereIn('group_owner_id', $teamIds)
                        ->whereRaw('week(created_at,1) = ?', [$loopWeek])->whereYear('created_at', '=', $loopYear)
                        ->sum('earned_hrs');
                    //array_push($earnedPerMonth, round(($s1TotalEarnedStepHours + $s2TotalEarnedStepHours), 1));
                    array_push($earnedPerMonth, $taskEarnedValue);



                    //----------    ROSTER   -----------------
                    $rosterCount = (int)AreaHour::whereIn('area_id', $teamIds)
                        //->whereRaw('week(month_date,1) = ?', [$loopWeek])->whereYear('month_date', '=', $loopYear)
                        ->where('month_num', $loopMonth)->where('year', $loopYear)
                        ->sum('hours');

                    if ($rosterCount > 0) {
                        $rosterHold = $rosterCount / $numOfWeeksOInMonth;
                    }
                    $roster = $rosterHold;
                    $rosterAccum = $rosterAccum + ($rosterCount / $numOfWeeksOInMonth);
                    array_push($rosterPerMonth, $roster);

                    //-----  VALUES FOR GRAPHS  ----
                    $prepValue = (int)$s1TotalTargetHours;
                    $prepAccum = $prepAccum + $prepValue;
                    $exeValue = (int)$s2TotalTargetHours;
                    $exeAccum = $exeAccum + $exeValue;
                    $prepAndExeAccum = $prepAndExeAccum + $prepValue + $exeValue;

                    $earned = $taskEarnedValue;
                    $earnedAccum = $earnedAccum + $earned;

                    $spent = (int)UserAreaHour::whereIn('area_id', $teamIds)
                        //->whereRaw('week(month_date) = ?', [$loopWeek])->whereYear('month_date', '=', $loopYear)
                        ->where('month_num', $loopMonth)->where('year', $loopYear)
                        ->sum('hours');
                    //$spentPerMonth = (((int)$earnedPerMonth * 50) / 100);
                    $spentPerMonth = $spent / $numOfWeeksOInMonth;
                    $spentAccum = $spentAccum + $spentPerMonth;

                    //$spent = UserAreaHour::whereIn('area_id',$teamIds)->where('month_num',3)->where('year',$loopYear)->sum('hours');

                    if ((int)$prepValue > 0 || (int)$exeValue > 0) {
                        $dataForTaskStarted = 1;
                        $dataHasStarted = true;
                    }


                    /* -----------------------------
                     *   CHART 1
                      -----------------------------*/
                    //return $spent;
                    /* CHART 1 -STACKED BAR CHART  */
                    array_push($c1Bar1, ((int)$prepValue + (int)$exeValue));

                    //Roster
                    array_push($c1Line1, $roster);



                    //Earned and Spent Amount
                    if (! $inCurrentWeek) {
                        array_push($c1Line3, $spent);
                        array_push($c1Line2, $earned);
                    }

                    /* -----------------------------
                     *   CHART 2
                      -----------------------------*/
                    /* CHART 2 - ACCUM LINE CHART  */
                    array_push($c2Line1, $prepAndExeAccum);
                    array_push($c2Line2, $exeAccum);
                    array_push($c2Line3, $rosterAccum);
                    if ($earned > 0 || true) {
                        array_push($c2Line4, $earnedAccum);
                    }
                    if ($spent > 0 || true) {
                        array_push($c2Line5, $spentAccum);
                    }

                    /* -----------------------------
                     *   CHART 3
                      -----------------------------*/
                    /* CHART 3 - NON STACKED BAR CHART  */
                    array_push($c3Bar1, (int)$prepValue);
                    array_push($c3Bar2, (int)$exeValue);
                    array_push($c3Line1, (int)$roster);

                    /* -----------------------------
                     *   CHART 4
                      -----------------------------*/
                    /* CHART 4 - NON STACKED BAR CHART  */

                    array_push($c4Line1, (int)$roster);
                    if (! $inCurrentWeek) {
                        array_push($c4Line2, $earned);
                        array_push($c4Line3, $spent);
                    }

                    $loopWeek++;
                    if ($loopWeek > 52) {
                        $loopWeek = 0;
                        $yearLoopCount++;
                    }

                    if ($yearLoopCount === 1) {
                        $loopYear = '2016';
                        $loopYearShort = '16';
                    }
                    if ($yearLoopCount === 2) {
                        $loopYear = '2017';
                        $loopYearShort = '17';
                    }
                    if ($yearLoopCount === 3) {
                        $loopYear = '2018';
                        $loopYearShort = '18';
                    }
                    if ($yearLoopCount === 4) {
                        $loopYear = '2019';
                        $loopYearShort = '19';
                    }
                    if ($yearLoopCount === 5) {
                        $loopYear = '2020';
                        $loopYearShort = '20';
                    }
                    if ($yearLoopCount === 6) {
                        $loopYear = '2021';
                        $loopYearShort = '21';
                    }

                    // $loopMonth++;
                    if ($loopMonth > 13) {
                        // $loopMonth = 1;
                    }

                    if ($todaysWeek == ((int)$loopWeek -1) && $todaysYear == $loopYear) {
                        $inCurrentWeek = true;
                    }
                }


                $data = [];
                array_push($data, $labels);
                //1,2,3,4,5
                array_push($data, $c1Bar1);
                array_push($data, $c1Bar2);
                array_push($data, $c1Line1);
                array_push($data, $c1Line2);
                array_push($data, $c1Line3);
                //6,7,8,9,10
                array_push($data, $c2Line1);
                array_push($data, $c2Line2);
                array_push($data, $c2Line3);
                array_push($data, $c2Line4);
                array_push($data, $c2Line5);
                //11,12,13,14,15
                array_push($data, $c3Bar1);
                array_push($data, $c3Bar2);
                array_push($data, $c3Line1);
                //array_push($data, $c3Line2);
                //array_push($data, $c3Line3);
                //16,17,18,19,20
                array_push($data, $c4Line1);
                array_push($data, $c4Line2);
                array_push($data, $c4Line3);
                array_push($data, (int)Task::whereIn('group_owner_id', $teamIds)->sum('target_val'));
                array_push($data, $fromMonth);
                array_push($data, $toMonth);
                array_push($data, $earnedPerMonth);
                array_push($data, $spentPerMonth);
                array_push($data, $rosterPerMonth);
                array_push($data, ['spentTotal' => $spentAccum]); //23
                array_push($data, ['rosterTotal' => AreaHour::whereIn('area_id', $teamIds)->sum('hours')]);//24
                array_push($data, ['projectTotalTarget' => Task::whereIn('group_owner_id', $teamIds)->sum('target_val')]);//25
                array_push($data, ['projectTotalPrepTarget' => Task::whereIn('group_owner_id', $teamIds)->where('stage_id', 1)->sum('target_val')]);//26
                array_push($data, ['projectTotalExecTarget' => Task::whereIn('group_owner_id', $teamIds)->where('stage_id', 2)->sum('target_val')]);//27
                array_push($data, ['projectTotalEarned' => $earnedAccum]);//28


                return $data;
            }

        }
        return $barChartData;
    }


    //Returns the Start and End month for the projected bar graph
    public function startAndEndMonthsForProjectedGraph($prepBarData, $execBarData)
    {
        $startMonthPrep = 0;
        $startMonthExec = 0;
        $startYearPrep = 0;
        $startYearExec = 0;

        //Find Starting Month Number and Year
        if (count($prepBarData) > 0) {
            $startMonthPrep = $prepBarData[0]->monthNumber;
            $startYearPrep = $prepBarData[0]->year;
        }
        if (count($execBarData) > 0) {
            $startMonthExec = $execBarData[0]->monthNumber;
            $startYearExec = $execBarData[0]->year;
        }
        //Month
        $startMonth = $startMonthPrep;
        if ($startMonthExec < $startMonthPrep && $startMonthExec !== 0) {
            $startMonth = $startMonthExec;
        }
        //Year
        $startYear = $startYearPrep;
        if ($startYearExec < $startYearPrep && $startYearExec !== 0) {
            $startYear = $startYearExec;
        }

        $pEndMonth = $this->findEndMonthNumber($prepBarData, $startYear);
        $eEndMonth = $this->findEndMonthNumber($execBarData, $startYear);
        $endMonthNum = $eEndMonth;
        if ($pEndMonth > $eEndMonth) {
            $endMonthNum = $pEndMonth;
        }

        $data = ['startMonth' => $startMonth, 'finishMonth' => $endMonthNum,'startYear' => $startYear];

        return $data;

    }


    public function findEndMonthNumber($dataArray, $startYear)
    {
        $yearHolder = 1;
        $endMonthNum = 1;
        for ($i = 0; $i < sizeof($dataArray); $i++) {
            if ($dataArray[$i]->year > $startYear) {
                $yearHolder = $yearHolder + 1;
            }
            if ($yearHolder === 1) {
                $endMonthNum = $dataArray[$i]->monthNumber;
            } else {
                $endMonthNum = $dataArray[$i]->monthNumber + 12;
            }
        }

        return $endMonthNum;


    }

    /* -------------------------------------
           Category Tabulation Report Data
    ----------------------------------------*/
    public function taskCategoryTabulationReportData(Request $request)
    {
        $data = [];
        $stageNum = 0;//0 = both , 1 = prep , 2 = exec

        $mainQuery = Task::where('project_id', 1);
        if ($request->has('stage') && (int)$request->stage > 0) {
            $mainQuery->where('stage_id', $request->stage);
            $stageNum = (int)$request->stage;
        };
        if ($request->has('group') && (int)$request->group > 0) {
            $mainQuery->whereIn('group_id', $request->group);
        };
        if ($request->has('type') && (int)$request->type > 0) {
            $mainQuery->whereIn('task_type_id', $request->type);
        };
        if ($request->has('role')) {
            $mainQuery->whereIn('group_owner_id', $request->role);
            $rolesIds = Role::whereIn('id', $request->role)->lists('id');
        } else {
            $rolesIds = Role::lists('id');
        };

        //Category List
        $statQuery = clone $mainQuery;
        $categoryIds = $statQuery->groupBy('task_type_id')->lists('task_type_id');
        $categories = TaskType::whereIn('id', $categoryIds)->orderBy('name', 'ASC')->get();

        //Date to work from - if date entered use that otherwise use today
        $dateToUseForSteps = Carbon::now();
        $startOfYear = Carbon::create(2016, 1, 2, 0, 0, 0);
        if ($request->has('to_date')) {
            $dateTimeX = $request->to_date . ' 00';
            $date = Carbon::createFromFormat('Y-m-d H', $dateTimeX);
            $dateToUseForSteps = $date;
        };

        foreach ($categories as $category) {
            //Category Name
            $categoryData['categoryName'] = $category->name . ' (' . $category->short_name . ')';
            $rule = TaskRule::where('task_type_id', $category->id)->where('stage_id', 1)->first();

            //Prep Steps
            if ($stageNum === 1 || $stageNum === 0) {
                //Step 1
                $prepStep1TodoCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('gen_applicable', 1)
                    ->where('gen_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $prepStep1PastDueCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('gen_applicable', 1)
                    ->where('gen_td', '<', $dateToUseForSteps)
                    ->where('gen_perc', '<', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $prepStep1DoneCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('gen_applicable', 1)
                    ->where('gen_perc', '=', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                //Step 2
                $prepStep2TodoCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('rev_applicable', 1)
                    ->where('rev_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $prepStep2PastDueCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('rev_applicable', 1)
                    ->where('rev_td', '<', $dateToUseForSteps)
                    ->where('rev_perc', '<', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $prepStep2DoneCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('rev_applicable', 1)
                    ->where('rev_perc', '=', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                //Step 3
                $prepStep3TodoCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('re_issu_applicable', 1)
                    ->where('re_issu_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $prepStep3PastDueCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('re_issu_applicable', 1)
                    ->where('re_issu_td', '<', $dateToUseForSteps)
                    ->where('re_issu_perc', '<', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $prepStep3DoneCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('re_issu_applicable', 1)
                    ->where('re_issu_perc', '=', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                //Step 4
                $prepStep4TodoCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('s_off_applicable', 1)
                    ->where('s_off_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $prepStep4PastDueCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('s_off_applicable', 1)
                    ->where('s_off_td', '<', $dateToUseForSteps)
                    ->where('s_off_perc', '<', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $prepStep4DoneCount = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('s_off_applicable', 1)
                    ->where('s_off_perc', '=', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();


                $total = $prepStep1TodoCount + $prepStep1DoneCount +
                    $prepStep2TodoCount + $prepStep2DoneCount +
                    $prepStep3TodoCount + $prepStep3DoneCount +
                    $prepStep4TodoCount + $prepStep4DoneCount;

                //$baseLineTotal = \DB::select("select count(*) as count from tasks where s_off_td < base_date and stage_id = 1");
                $baseLineTotal = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('base_date', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();

                $taskTotal = Task::where('stage_id', 1)
                    ->where('task_type_id', $category->id)
                    ->where('last_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();

                if (isset($rule) && $total > 0) {
                    array_push($data, [
                        $category->name . ' (' . $category->short_name . ')',
                        'Prep',
                        $prepStep1TodoCount,
                        $prepStep1PastDueCount,
                        $prepStep1DoneCount,

                        $prepStep2TodoCount,
                        $prepStep2PastDueCount,
                        $prepStep2DoneCount,

                        $prepStep3TodoCount,
                        $prepStep3PastDueCount,
                        $prepStep3DoneCount,

                        $prepStep4TodoCount,
                        $prepStep4PastDueCount,
                        $prepStep4DoneCount,

                        $baseLineTotal,
                        $taskTotal,
                        $rule->gen_ex_applicable,
                        $rule->rev_applicable,
                        $rule->re_issu_applicable,
                        $rule->s_off_applicable,

                    ]);
                }
            }

            //Execution Step
            $rule = TaskRule::where('task_type_id', $category->id)->where('stage_id', 2)->first();
            if ($stageNum === 2 || $stageNum === 0) {
                //Step 1
                $exeStep1TodoCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('gen_applicable', 1)
                    ->where('gen_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $exeStep1PastDueCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('gen_applicable', 1)
                    ->where('gen_td', '<', $dateToUseForSteps)
                    ->where('gen_perc', '<', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $exeStep1DoneCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('gen_applicable', 1)
                    ->where('gen_perc', '=', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();


                //Step 2
                $exeStep2TodoCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('rev_applicable', 1)
                    ->where('rev_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $exeStep2PastDueCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('rev_applicable', 1)
                    ->where('rev_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $exeStep2DoneCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('rev_applicable', 1)
                    ->where('rev_perc', '=', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                //Step 3
                $exeStep3TodoCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('re_issu_applicable', 1)
                    ->where('re_issu_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $exeStep3PastDueCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('re_issu_applicable', 1)
                    ->where('re_issu_td', '<', $dateToUseForSteps)
                    ->where('re_issu_perc', '<', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $exeStep3DoneCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('re_issu_applicable', 1)
                    ->where('re_issu_perc', '=', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                //Step 4
                $exeStep4TodoCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('s_off_applicable', 1)
                    ->where('s_off_td', '<', $dateToUseForSteps)
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $exeStep4PastDueCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('s_off_applicable', 1)
                    ->where('s_off_td', '<', $dateToUseForSteps)
                    ->where('s_off_perc', '<', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();
                $exeStep4DoneCount = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('s_off_applicable', 1)
                    ->where('s_off_perc', '=', '100')
                    ->whereIn('group_owner_id', $rolesIds)
                    ->count();

                $total = $exeStep1TodoCount + $exeStep1DoneCount +
                    $exeStep2TodoCount + $exeStep2DoneCount +
                    $exeStep3TodoCount + $exeStep3DoneCount +
                    $exeStep4TodoCount + $exeStep4DoneCount;

                //$baseLineTotal = \DB::select("select count(*) as count from tasks where s_off_td < base_date and stage_id = 2");
                $baseLineTotal = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('base_date', '<', $dateToUseForSteps)
                    ->count();
                $taskTotal = Task::where('stage_id', 2)
                    ->where('task_type_id', $category->id)
                    ->where('last_td', '<', $dateToUseForSteps)
                    ->count();

                if (isset($rule) && $total > 0) {
                    array_push($data, [
                        $category->name,
                        'Exe',
                        $exeStep1TodoCount,
                        $exeStep1PastDueCount,
                        $exeStep1DoneCount,

                        $exeStep2TodoCount,
                        $exeStep2PastDueCount,
                        $exeStep2DoneCount,

                        $exeStep3TodoCount,
                        $exeStep3PastDueCount,
                        $exeStep3DoneCount,

                        $exeStep4TodoCount,
                        $exeStep4PastDueCount,
                        $exeStep4DoneCount,

                        $baseLineTotal,
                        $taskTotal,
                        $rule->gen_ex_applicable,
                        $rule->rev_applicable,
                        $rule->re_issu_applicable,
                        $rule->s_off_applicable,
                    ]);
                }
            }

        }


        return $data;
    }


    /* -------------------------------------
               Progress Overview
    ----------------------------------------*/

    public function areaProgressOverview(Request $request)
    {

        $data = [];
        $date = Carbon::now();

        $mainQuery = Task::where('project_id', 1);
        if ($request->has('stage') && (int)$request->stage > 0) {
            $mainQuery->where('stage_id', $request->stage);
        };
        if ($request->has('group') && (int)$request->group > 0) {
            $mainQuery->whereIn('group_id', $request->group);
        };
        if ($request->has('type') && (int)$request->type > 0) {
            $mainQuery->whereIn('task_type_id', $request->type);
        };
        if ($request->has('role')) {
            $mainQuery->whereIn('group_owner_id', $request->role);
        };

        $weeklyQuery = clone $mainQuery;
        if ($request->has('to_date')) {
            $dateTimeX = $request->to_date . ' 00';
            $date = Carbon::createFromFormat('Y-m-d H', $dateTimeX);
            // $mainQuery->where('last_td', '<', $date);
        } else {
            $date = Carbon::now();
            // $mainQuery->where('last_td', '<', $date);
        };

        $stage = $request->stage;
        $group = $request->group;
        $type = $request->type;
        $role = $request->role;
        $to_date = $request->to_date;

        //$areaIds = Task::groupBy('group_owner_id')->lists('group_owner_id')->toArray();
        //$areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->whereIn('id', $areaIds)->get();

        $today = Carbon::now();
        $labels = [];
        $c1Data1 = [];
        $c1Data2 = [];
        $c1Data3 = [];
        $c1Data4 = [];
        $c1Data5 = [];
        //$c2Data1 = [];$c2Data2 = [];$c2Data3 = [];$c2Data4 = [];$c2Data5 = [];
        // $c3Data1 = [];$c3Data2 = [];$c3Data3 = [];$c3Data4 = [];$c3Data5 = [];

        $total = 0;
        $totalDone = 0;
        $totalNotDone = 0;
        $totalPerc = 0;
        $totalTarget = 0;
        $pastDue = 0;
        $nonRecoverable = 0;


        $staticStats = [];

        //Group By Month - If Needed
        //$statQuery = clone $mainQuery;
        //$timeQuery = $statQuery->select(\DB::raw('month(last_td) as monthNumber,year(last_td) as year,count(*) as value'))->groupBy('year')->groupBy('monthNumber')->orderBy('year', 'ASC');
        //$taskTimeDataMonth = Task::select(\DB::raw('month(last_td) as monthNumber,year(last_td) as year,count(*) as value'));
        //$taskTimeDataMonth = $timeQuery->get();

        //Group by Week
        $statQuery = clone $weeklyQuery;

        //$timeQuery = $statQuery->select(\DB::raw('last_td,week(last_td) as weekNumber,month(last_td) as monthNumber,year(last_td) as year,count(*) as value'))->groupBy('year')->groupBy('weekNumber')->orderBy('year', 'ASC');
        $timeQuery = $statQuery->select(\DB::raw('last_td,week(last_td) as weekNumber,month(last_td) as monthNumber,year(last_td) as year,count(*) as value'))
            ->where('next_td', '>', '2017-01-01')
            // $timeQuery = $statQuery->select(\DB::raw('last_td,week(last_td) as weekNumber,year(last_td) as year,count(*) as value'))
            ->groupBy('year')->groupBy('weekNumber')->orderBy('year', 'ASC');
        $taskTimeDataWeeks = $timeQuery->get();

        //$startAndEndPoints = $this->startAndEndWeekNumber($taskTimeDataWeeks);
        //return $startAndEndPoints;

        $startAndEndPoints = [];

        if (sizeof($taskTimeDataWeeks) > 0) {

            $startAndEndPoints = $this->startAndEndWeekNumber($taskTimeDataWeeks);
            //$startAndEndPoints = $this->startAndEndMonthNumber($taskTimeData);
            //
            //$loopMonth = $startAndEndPoints['startMonth'];
            $loopWeek = $startAndEndPoints['startWeek'];
            $loopYear = '2017';
            $loopYearShort = '17';
            $loopTurnedOver = 0;
            $data1Accum = 0;
            $data2Accum = 0;
            $yearLoopCount = 0;

            $testCount = 0;
            $today = Carbon::now();

            if ($startAndEndPoints['numberOfWeeks'] > 0) {

                for ($i = $startAndEndPoints['startWeek']; $i <= $startAndEndPoints['numberOfWeeks']; $i++) {

                    /* LABELS  */
                    $dateTime = new \DateTime();
                    $dateTime->setISODate($loopYear, $loopWeek);
                    $month = $dateTime->format('n');
                    $day = $dateTime->format('d');
                    //array_push($labels, 'wk '.$loopWeek. '-' .$this->monthsShort[$month].'-'. $loopYear);
                    array_push($labels, '' . ($day) . ' ' . $this->monthsShort[$month - 1] . '-' . $loopYear);

                    //Data 1 - target
                    $statQuery = clone $weeklyQuery;
                    $data1Count = $statQuery->whereRaw('week(last_td) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)->count();
                    //$data1Count = Task::whereMonth('last_td', '=', $loopMonth)->whereYear('last_td', '=', $loopYear)->where('complete', 0)->count();
                    array_push($c1Data1, $data1Count);
                    //Data 2 - Done
                    $statQuery = clone $weeklyQuery;
                    $data2Count = $statQuery->whereRaw('week(last_td) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)->where('complete', 1)->count();
                    //$data2Count = Task::whereMonth('last_td', '=', $loopMonth)->whereYear('last_td', '=', $loopYear)->where('complete', 1)->count();
                    array_push($c1Data2, $data2Count);
                    //Data 3
                    $data1Accum = $data1Accum + $data1Count;
                    array_push($c1Data3, $data1Count);

                    //Past Due
                    $statQuery = clone $weeklyQuery;
                    $pastDueLoop = $statQuery->whereRaw('week(last_td) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)->where('complete', 0)
                        ->where('next_td', '<', $today)->count();
                    //Non Recoverable - i.e all steps are past due
                    $statQuery = clone $mainQuery;
                    $nonRecoverableLoop = $statQuery->whereRaw('week(last_td) = ?', [$loopWeek])->whereYear('last_td', '=', $loopYear)
                        ->where('complete', 0)->where('last_td', '<', $today)->count();

                    $loopWeek++;
                    if ($loopWeek > 52) {
                        $loopWeek = 0;
                        $yearLoopCount++;
                    }

                    if ($yearLoopCount === 1) {
                        $loopYear = '2018';
                        $loopYearShort = '18';
                    }
                    if ($yearLoopCount === 2) {
                        $loopYear = '2019';
                        $loopYearShort = '19';
                    }
                    if ($yearLoopCount === 3) {
                        $loopYear = '2020';
                        $loopYearShort = '20';
                    }
                    /* --------------------------
                                STATS
                    ------------------------------*/
                    //totals
                    if ($data1Count > 0) {
                        $total = $total + $data1Count;
                    }

                    $totalDone = $totalDone + $data2Count;
                    if ($totalDone > 0) {
                        if ($totalDone > 0) {
                            $totalNotDone = $total - $totalDone;
                        }
                    } else {
                        $totalNotDone = $total;
                    }


                    $totalPerc = 0;
                    if ($total > 0) {
                        $totalPerc = round(($totalDone / $total * 100), 1);
                    }
                    $pastDue = $pastDue + $pastDueLoop;
                    $nonRecoverable = $nonRecoverable + $nonRecoverableLoop;

                    $testCount++;
                }
            }
        }


        //return $testCount;
        /* --------------------------
                GRAPH DATA
        ------------------------------*/
        array_push($data, $labels);
        array_push($data, $c1Data1);
        array_push($data, $c1Data2);
        array_push($data, $c1Data3);
        array_push($data, $c1Data4);

        /* --------------------------
                STATIC STATS
        ------------------------------*/
        $defaultDate = Carbon::createFromFormat('Y-m-d H', '2017-01-02 00');

        $statQuery = clone $mainQuery;
        $staticStats['total'] = $total = (int)$statQuery->count();// $staticStats['total'] = $total;

        $statQuery = clone $mainQuery;
        $staticStats['totalDone'] = $totalDone = (int)$statQuery->where('complete', 1)->where('last_td', '<', $date)->count();//  $staticStats['totalDone'] = $totalDone;

        $statQuery = clone $mainQuery;
        $staticStats['totalNotDone'] = (int)$statQuery->where('complete', 0)->where('last_td', '<', $date)->count();// $staticStats['totalNotDone'] = $totalNotDone;

        $staticStats['totalPerc'] = 0;
        if ($staticStats['total'] > 0) {
            $staticStats['totalPerc'] = round(($staticStats['totalDone'] / $staticStats['total'] * 100), 1);//   $staticStats['totalPerc'] = $totalPerc;
        }

        $staticStats['notComplete'] = $staticStats['totalNotDone'];

        $statQuery = clone $mainQuery;
        $staticStats['pastDue'] = (int)$statQuery->where('complete', 0)->where('next_td', '<', $today)->count();

        $statQuery = clone $mainQuery;
        $staticStats['nonRecoverable'] = (int)$statQuery->where('complete', 0)->where('last_td', '<', $today)->count();


        //Target Hours Overview
        $statQuery = clone $mainQuery;
        $staticStats['noHoursAssignedComplete'] = (int)$statQuery->where('target_val', 0)->where('complete', '=', 1)->count();
        $statQuery = clone $mainQuery;
        $staticStats['noHoursAssignedNotComplete'] = (int)$statQuery->where('target_val', 0)->where('complete', '=', 0)->count();
        $statQuery = clone $mainQuery;
        $staticStats['completeWithHours'] = (int)$statQuery->where('target_val', '>', 0)->where('complete', '=', 1)->count();
        $statQuery = clone $mainQuery;
        $staticStats['notCompleteWithHours'] = (int)$statQuery->where('target_val', '>', 0)->where('complete', '=', 0)->count();


        // ======= STEP STATUS ======
        $statQuery = clone $mainQuery;
        $complete = (int)$statQuery->where('complete', '=', 1)->count();
        //$staticStats['totalDone'] = $complete;
        //Not started
        $statQuery = clone $mainQuery;
        $notStarted = (int)$statQuery
            ->where('gen_perc', '=', 0)
            ->where('rev_perc', '=', 0)
            ->where('re_issu_perc', '=', 0)
            ->where('s_off_perc', '=', 0)
            ->where('last_td', '<', $date)
            ->count();
        $staticStats['notStarted'] = $notStarted;

        //Left To Do - not complete
        $statQuery = clone $mainQuery;
        $leftToDo = (int)$statQuery->where('complete', '=', 0)->count();


        $notStartedPerc = 0;
        if ($total > 0) {
            $notStartedPerc = round(($notStarted / $total * 100), 1);
        }
        $staticStats['notStartedPerc'] = $notStartedPerc;

        //--------- Step 1 Stats ---------
        $statQuery = clone $mainQuery;
        //$staticStats['inStep1'] = (int)$statQuery->where('gen_applicable', 1)->where('gen_complete_date', '<', $defaultDate)->where('gen_perc', '>', 0)->where('complete', '=', 0)->count();
        //$staticStats['inStep1'] = (int)$statQuery->where('gen_applicable', 1)->where('gen_perc', '=', 0)->count();
        //$staticStats['inStep1'] = (int)$statQuery->where('gen_perc', '>', 0)->where('complete', '=', 0)->count();
        $staticStats['inStep1'] = (int)$statQuery
            ->where('gen_perc', '>', 0)
            ->where('gen_perc', '<', 100)
            ->where('rev_perc', '=', 0)
            ->where('re_issu_perc', '=', 0)
            ->where('s_off_perc', '=', 0)
            ->count();
        $step1Perc = 0;
        if ($total > 0) {
            $step1Perc = round(($staticStats['inStep1'] / $total * 100), 1);
        }
        $staticStats['step1Perc'] = $step1Perc;

        //--------- Step 2 Stats ---------
        $statQuery = clone $mainQuery;

        $staticStats['inStep2'] = (int)$statQuery
            ->where('rev_perc', '<', 100)->where('rev_applicable', '=', 1)
            ->where('re_issu_perc', '=', 0)
            ->where('s_off_perc', '=', 0)
            ->where(function ($query) use ($defaultDate) {
                $query->where('gen_perc', '=', 100)->where('gen_applicable', '=', 1)->orWhere('gen_applicable', 0);
            })
            ->count();


        $step2Perc = 0;
        if ($total > 0) {
            $step2Perc = round(($staticStats['inStep2'] / $total * 100), 1);
        }
        $staticStats['step2Perc'] = $step2Perc;


        // ---------  Step 3 Stats  ---------
        $statQuery = clone $mainQuery;

        $staticStats['inStep3'] = (int)$statQuery
            ->where('re_issu_perc', '<', 100)->where('re_issu_applicable', '=', 1)
            ->where('s_off_perc', '=', 0)
            ->where(function ($query) use ($defaultDate) {
                $query->where('rev_perc', '=', 100)->where('rev_applicable', '=', 1)->orWhere('rev_applicable', 0);
            })
            ->where(function ($query) use ($defaultDate) {
                $query->where('gen_perc', '=', 100)->where('gen_applicable', '=', 1)->orWhere('gen_applicable', 0);
            })
            ->count();

        $step3Perc = 0;
        if ($total > 0) {
            $step3Perc = round(($staticStats['inStep3'] / $total * 100), 1);
        }
        $staticStats['step3Perc'] = $step3Perc;


        //---------  Step 4 Stats ---------
        $statQuery = clone $mainQuery;
        $step4Perc = 0;
        $staticStats['inStep4'] = $staticStats['total'] -
            (
                $staticStats['inStep1'] +
                $staticStats['inStep2'] +
                $staticStats['inStep3'] +
                $staticStats['notStarted'] +
                $staticStats['totalDone']
            );

        if ((int)$staticStats['inStep4'] < 0) {
            $staticStats['inStep4'] = 0;
        }


        if ($leftToDo > 0) {
            if ($total > 0) {
                $step4Perc = round(($staticStats['inStep4'] / $total * 100), 1);
            } else {
                $step4Perc = 0;
            }

        }
        $staticStats['step4Perc'] = $step4Perc;
        if ($step4Perc < 0) {
            $staticStats['step4Perc'] = 0;
        }


        $statQuery = clone $mainQuery;
        $started = $staticStats['inStep1'] + $staticStats['inStep2'] + $staticStats['inStep3'] + $staticStats['inStep4'];
        $staticStats['started'] = $started;


        array_push($data, $staticStats);

        return $data;


        /* -------------------     OLD CODE     --------------------------
       foreach ($areas as $area) {

           $s1DueCount = Task::where('stage_id', 1)->where('group_owner_id', $area->id)
               ->where('last_td', '<', $today)->where('complete', 0)->count();
           $s2DueCount = Task::where('stage_id', 2)->where('group_owner_id', $area->id)
               ->where('last_td', '<', $today)->where('complete', 0)->count();

           if ($s1DueCount > 0 || $s2DueCount > 0 || true) {
               array_push($labels, $area->name);
               array_push($c1Data1, $s1DueCount);
               array_push($c1Data2, $s2DueCount);
           }
           $s1DoneCount = Task::where('stage_id', 1)->where('group_owner_id', $area->id)
               ->where('last_td', '<', $today)->where('complete', 1)->count();
           $s2DoneCount = Task::where('stage_id', 2)->where('group_owner_id', $area->id)
               ->where('last_td', '<', $today)->where('complete', 1)->count();
           array_push($c1Data3, $s1DoneCount);
           array_push($c1Data4, $s2DoneCount);

           $s1Count = Task::where('stage_id', 1)->where('group_owner_id', $area->id)->count();
           $s1CompleteCount = Task::where('stage_id', 1)->where('group_owner_id', $area->id)->where('complete', 1)->count();
           $s1Percent = 0;
           if ($s1Count > 0) {
               $s1Percent = $s1CompleteCount / $s1Count * 100;
           }
           $s2Count = Task::where('stage_id', 2)->where('group_owner_id', $area->id)->count();
           $s2CompleteCount = Task::where('stage_id', 2)->where('group_owner_id', $area->id)->where('complete', 1)->count();
           $s2Percent = 0;
           if ($s2Count > 0) {
               $s2Percent = $s2CompleteCount / $s2Count * 100;
           }

           array_push($c2Data1, round($s1Percent, 2));
           array_push($c2Data2, round($s2Percent, 2));

       }
       array_push($data, $labels);
       array_push($data, $c1Data1);
       array_push($data, $c1Data2);
       array_push($data, $c1Data3);
       array_push($data, $c1Data4);
       array_push($data, $c2Data1);
       array_push($data, $c2Data2);

       return $data;

       */
    }

    public function startAndEndMonthNumber($taskTimeData)
    {
        $data = [];
        $startMonth = $taskTimeData[0]->monthNumber;
        $startYear = $taskTimeData[0]->year;

        $endMonth = 1;
        $endYear = 1;

        foreach ($taskTimeData as $endMonth) {

            $endYear = $endMonth->year;
            if ($endYear > $startYear) {
                $endMonth = $endMonth->monthNumber + 12;
            } else {
                $endMonth = $endMonth->monthNumber;
            }
        }
        $data = ['startMonth' => $startMonth, 'startYear' => $startYear, 'endMonth' => $endMonth, 'endYear' => $endYear,];

        return $data;

    }

    public function startAndEndWeekNumber($taskTimeData)
    {
        $data = [];
        $dataStartWeek = $taskTimeData[0]->weekNumber;
        $dataStartYear = $taskTimeData[0]->year;

        $dataEndYear = $taskTimeData[sizeof($taskTimeData) - 1]->year;
        $dataEndWeek = $taskTimeData[sizeof($taskTimeData) - 1]->weekNumber;

        $yearCount = (int)$dataEndYear - (int)$dataStartYear;
        $numberOfWeeks = 0;

        if ($yearCount === 0) {
            $numberOfWeeks = $dataEndWeek;
        }
        if ($yearCount === 1) {
            $numberOfWeeks = 52 + $dataEndWeek;
        }
        if ($yearCount === 2) {
            $numberOfWeeks = (52 - $dataStartWeek) + 52 + $dataEndWeek + $dataStartWeek;
        }

        $data = ['startWeek' => $dataStartWeek, 'startYear' => $dataStartYear, 'endWeek' => $dataEndWeek, 'endYear' => $dataEndYear, 'numberOfWeeks' => $numberOfWeeks];

        return $data;

    }


    public function areaProjectedLineChartData()
    {
        $barChartData = [];


        return $barChartData;

    }


    public function combineProjectedBarAndLineGraphData($prepBarData, $execBarData)
    {
        $chartData = [];
        $startMonth = 1;
        $startMonthPrep = 0;
        $startMonthExec = 0;
        $startYear = 1;
        $startYearPrep = 0;
        $startYearExec = 0;
        $endMonthNum = 0;


        //Find Starting Month Number and Year
        if (count($prepBarData) > 0) {
            $startMonthPrep = $prepBarData[0]->monthNumber;
            $startYearPrep = $prepBarData[0]->year;
        }
        if (count($execBarData) > 0) {
            $startMonthExec = $execBarData[0]->monthNumber;
            $startYearExec = $execBarData[0]->year;
        }
        //Month
        $startMonth = $startMonthPrep;
        if ($startMonthExec < $startMonthPrep && $startMonthExec !== 0) {
            $startMonth = $startMonthExec;
        }
        //Year
        $startYear = $startYearPrep;
        if ($startYearExec < $startYearPrep && $startYearExec !== 0) {
            $startYear = $startYearExec;
        }

        $pEndMonth = $this->findEndMonthNumber($prepBarData, $startYear, $startMonth);
        $eEndMonth = $this->findEndMonthNumber($execBarData, $startYear, $startMonth);
        $endMonthNum = $eEndMonth;
        if ($pEndMonth > $eEndMonth) {
            $endMonthNum = $pEndMonth;
        }
        $monthPosition = $startMonth - 1;
        $year = $startYear;
        $holderArray = [];

        return $this->fillStageArray($prepBarData, $startMonth, $pEndMonth + 1);

        return [$startMonth, $pEndMonth, $endMonthNum];


        for ($i = 0; $i < sizeof($prepBarData); $i++) {
            $tempArray['p'] = $prepBarData[$i]->value;
            $tempArray['e'] = $prepBarData[$i]->value;
            $tempArray['m'] = $prepBarData[$i]->date;

            array_push($chartData, $tempArray);


        }

        return $chartData;

    }


    public function fillStageArray($stageArray, $startMonth, $endMonth)
    {
        $data = [$startMonth, $endMonth];
        //$data = [];
        $LoopMonth = $startMonth;
        $monthInArray = 0;
        $stageHasLoopedAroundMonths = 0;
        $StageCurrentMonthTemp = 0;
        $loop = 0;
        for ($i = $startMonth; $i < $endMonth; $i++) {

            if ($loop < sizeof($stageArray) - 1) {

                if ($stageHasLoopedAroundMonths === 0) {
                    $StageCurrentMonth = $stageArray[$loop]->monthNumber;
                } else {
                    $StageCurrentMonth = $stageArray[$loop]->monthNumber;
                }

                if ($StageCurrentMonth === $LoopMonth) {
                    $monthInArray = 1;
                }

                if ($monthInArray === 1) {
                    $tempArray['monthName'] = $stageArray[$loop]->monthName;
                    $tempArray['monthNumber'] = $stageArray[$loop]->monthNumber;
                    $tempArray['year'] = $stageArray[$loop]->year;
                    $tempArray['date'] = $stageArray[$loop]->date;
                    $tempArray['value'] = (int)$stageArray[$loop]->value;
                    array_push($data, $tempArray);
                    $loop++;
                } else {
                    $x = 1;
                    while ($monthInArray === 0 && $x <= 20) {
                        if ($LoopMonth > 12) {
                            $LoopMonth = 1;
                        }
                        $tempArray['monthName'] = $this->monthsFull[$loop - 1];
                        $tempArray['monthNumber'] = 'month name';
                        $tempArray['year'] = 'month name';
                        $tempArray['date'] = 'month name';
                        $tempArray['value'] = 0;
                        array_push($data, $tempArray);
                        $LoopMonth++;

                        if ($StageCurrentMonth === $LoopMonth) {
                            $tempArray['monthName'] = $stageArray[$loop]->monthName;
                            $tempArray['monthNumber'] = $stageArray[$loop]->monthNumber;
                            $tempArray['year'] = $stageArray[$loop]->year;
                            $tempArray['date'] = $stageArray[$loop]->date;
                            $tempArray['value'] = (int)$stageArray[$loop]->value;
                            array_push($data, $tempArray);
                            $monthInArray = 1;
                            break;
                        }
                        $x++;
                        $loop++;
                    }
                    $loop++;
                }
                $startMonth++;
                if ($LoopMonth > 12) {
                    $LoopMonth = 1;
                    $stageHasLoopedAroundMonths = 1;

                }

                $monthInArray = 0;
                $LoopMonth++;

            } else {

                $loop++;
            }

        }

        return $data;

    }


    /*
     *  Saves the users configuration for the Systems overview Report Table
     *  saves the stage and category for each column in the report
     */
    public function saveSystemOverviewReportSettings()
    {

        //$userConfig  = $request->configArray;

        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        if (isset($userFilter)) {

            $categoryAndStageIds = [
                [1, 2],
                [1, 7],
                [1, 17],
                [2, 7],
                [2, 17],
                [1, 54],
                [1, 60]
            ];

            $configJson = collect($categoryAndStageIds)->toJson();

            $userFilter->system_report_config = '';

            $userFilter->system_report_config = $configJson;

            $userFilter->save();
        }
        //$userFilterExists = UserFilter::where('user_id', $user->id)->exists();

        return $userFilter->system_report_config;


    }


}
<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 25/9/16
 * Time: 12:47 PM
 */

namespace app\Rowsys\Tracker\Classes\StepPhases;

use App\Models\Task;
use App\Models\TaskStepPhase;
use App\Models\TaskTypeStepPhase;

class UpdateTaskStepPhase
{

    protected $stepPhase;
    protected $task;
    protected $taskTypeStepPhase;
    protected $percentage;

    /*
     * Update the task step phase OR add one if it doesn't already exist for the task
     *
     *
     */

    public function updateOrAddStepPhase(Task $task, TaskTypeStepPhase $taskTypeStepPhase, $taskPercentage)
    {

        $this->task = $task;
        $this->percentage = $taskPercentage;
        $this->taskTypeStepPhase = $taskTypeStepPhase;
        $stepPhaseExists = false;

        $this->stepPhase = TaskStepPhase::where('task_id', (int)$this->task->id)
            ->where('task_type_step_id', (int)$this->taskTypeStepPhase->id)
            ->where('step_number', (int)$this->taskTypeStepPhase->step_number)
            ->first();

        if ($stepPhaseExists === true) {
            $this->stepPhase = TaskStepPhase::where('task_id', $this->task->id)
                ->where('task_type_step_id', $this->taskTypeStepPhase->id)
                ->where('step_number', $this->taskTypeStepPhase->step_number)
                ->first();
        } else {
            //Add new Step Phase for the task
            $this->createStepPhase();
        }
        $this->updatePhasePercentage();

        if ($this->stepPhase->phase_count > 0) {

             $countOfReviewersThatAreOn = $this->countTheNumberOfReviewersThatAreOn();

            $this->stepPhase->break_down_count = 0;

            if ($this->percentage === 0) {
                $this->turnOffAllReviewers();
            }
            if ($this->percentage > 0 && $this->percentage < 100) {

                //Rest all Reviewers - then update depending on the phase count & %
                $this->turnOffAllReviewers();

                switch ($this->taskTypeStepPhase->phase_count) {
                    case 1:
                        if ($this->percentage < 100) {
                            //Do Nothing - all reviewers must be OFF
                        }
                        break;

                    case 2:
                        if ($this->percentage === 50 ) {
                            $this->turnOnCountOfReviewers(1);
                        }
                        break;

                    case 3:
                        if ($this->percentage > 31 && $this->percentage < 36) {
                            $this->turnOnCountOfReviewers(1);
                        }
                        if ($this->percentage > 65 && $this->percentage < 67) {
                            $this->turnOnCountOfReviewers(2);
                        }
                        break;

                    case 4:
                        if ($this->percentage === 25) {
                            $this->turnOnCountOfReviewers(1);
                        }
                        if ($this->percentage === 50) {
                            $this->turnOnCountOfReviewers(2);
                        }
                        if ($this->percentage === 75) {
                            $this->turnOnCountOfReviewers(3);
                        }
                        break;

                    case 5:
                        if ($this->percentage === 20) {
                            $this->turnOnCountOfReviewers(1);
                        }
                        if ($this->percentage === 40) {
                            $this->turnOnCountOfReviewers(2);
                        }
                        if ($this->percentage === 60) {
                            $this->turnOnCountOfReviewers(3);
                        }
                        if ($this->percentage === 80) {
                            $this->turnOnCountOfReviewers(4);
                        }
                        break;

                    default:
                }
            }

            if ($this->percentage >= 100) {
                $this->turnOnAllReviewers();
            }

        }else{
            $this->turnOffAllReviewers();
        }

        $this->saveStepPhase();
    }



    function countTheNumberOfReviewersThatAreOn()
    {

        $count = 0;

        return $count;


    }

    // Turns ON the number of reviewers - from start to number inclusive
    function turnOnCountOfReviewers($reviewers)
    {

        switch ($reviewers) {
            case 5:
                $this->stepPhase->p5_status = 1;

            case 4:
                $this->stepPhase->p4_status = 1;

            case 3:
                $this->stepPhase->p3_status = 1;

            case 2:
                $this->stepPhase->p2_status= 1;

            case 1:
                $this->stepPhase->p1_status = 1;

                break;
        }



    }

    // Turns OFF the reviewer at the certain step OR the next reviewer that is not ON
    function turnOffReviewer($reviewers)
    {

    }

    //Turn OFF all the Reviewers i.e reset to zero
    function turnOffAllReviewers()
    {

        $this->stepPhase->p1_status = 0;
        $this->stepPhase->p2_status = 0;
        $this->stepPhase->p3_status = 0;
        $this->stepPhase->p4_status = 0;
        $this->stepPhase->p5_status = 0;
        $this->stepPhase->p6_status = 0;
        $this->stepPhase->p7_status = 0;
        $this->stepPhase->p8_status = 0;
        $this->stepPhase->p9_status = 0;
        $this->stepPhase->p10_status = 0;

    }

    //Turn ON all the Reviewers i.e the first 5 Reviewers
    function turnOnAllReviewers()
    {
        $this->stepPhase->p1_status = 1;
        $this->stepPhase->p2_status = 1;
        $this->stepPhase->p3_status = 1;
        $this->stepPhase->p4_status = 1;
        $this->stepPhase->p5_status = 1;
        $this->stepPhase->p6_status = 0;
        $this->stepPhase->p7_status = 0;
        $this->stepPhase->p8_status = 0;
        $this->stepPhase->p9_status = 0;
        $this->stepPhase->p10_status = 0;
    }

    public function createStepPhase()
    {

        $taskStepPhase = new TaskStepPhase();

        $taskStepPhase->task_id = $this->task->id;

        $taskStepPhase->task_type_step_id = $this->taskTypeStepPhase->id;

        $taskStepPhase->step_number = $this->taskTypeStepPhase->step_number;

        $taskStepPhase->phase_count = $this->taskTypeStepPhase->phase_count;

        $taskStepPhase->break_down_count = $this->taskTypeStepPhase->break_down_count;

        $taskStepPhase->p1_owner_id = $this->taskTypeStepPhase->p1_owner_id;
        $taskStepPhase->p2_owner_id = $this->taskTypeStepPhase->p2_owner_id;
        $taskStepPhase->p3_owner_id = $this->taskTypeStepPhase->p3_owner_id;
        $taskStepPhase->p4_owner_id = $this->taskTypeStepPhase->p4_owner_id;
        $taskStepPhase->p5_owner_id = $this->taskTypeStepPhase->p5_owner_id;
        $taskStepPhase->p6_owner_id = $this->taskTypeStepPhase->p6_owner_id;
        $taskStepPhase->p7_owner_id = $this->taskTypeStepPhase->p7_owner_id;
        $taskStepPhase->p8_owner_id = $this->taskTypeStepPhase->p8_owner_id;
        $taskStepPhase->p9_owner_id = $this->taskTypeStepPhase->p9_owner_id;
        $taskStepPhase->p10_owner_id = $this->taskTypeStepPhase->p10_owner_id;

        //Status - get the step status e.g gen, review
        $taskStepPhase->status = $this->percentage;

        $taskStepPhase->current_owner_id = 1;

        //Save
        $taskStepPhase->save();

        $this->stepPhase = $taskStepPhase;


    }

    //update the status value to the new percentage
    public function updatePhasePercentage()
    {
        $this->stepPhase->status = $this->percentage;

    }

    //save the Step Phase
    public function saveStepPhase()
    {
        $this->stepPhase->save();
    }

}
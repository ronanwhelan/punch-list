<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 25/9/16
 * Time: 11:47 AM
 */

namespace app\Rowsys\Tracker\Classes\StepPhases;


use App\Models\Task;


class UpdateTaskStepPercentage {

    protected $task;
    protected $stepCount;
    protected $stepNumber;


    //If the breakdown count changes, the task percentage may need to be changed to fit the new breakdown
    //e.g Lets say the current  breakdown count is 3 and the percent for the task is at 33% - if the user changes
    //the breakdown count to 4 then 33% does not work - it needs to be changed to 25% or 50% to fit the new breakdown of 4
    public function updateTaskStepPercentage(Task $task,$stepCount,$stepNumber)
    {

        $this->task = $task;
        $this->stepCount = $stepCount;
        $this->stepNumber = $stepNumber;

        $stepPercentage = $this->getStepPercentageValue();

        if($stepPercentage === 0 || $stepPercentage === 100 ) {

        //do nothing

        }else{

            //recalculate the task percentage - to ensure it fits the breakdown count
            $stepPercentage = $this->recalculateTaskStepPercentage($stepPercentage);

            //update the task
            $this->updateAndSaveTaskStepPercentage($stepPercentage);

        }


        return $stepPercentage;

    }


    // Get the Value of the Tasks Step Percentage based on the Step Number
    public function getStepPercentageValue(){
        $stepPercentage = 0;

        switch ($this->stepNumber) {
            case 1:
                $stepPercentage = $this->task->gen_perc;
                break;

            case 2:
                $stepPercentage = $this->task->rev_perc;
                break;

            case 3:
                $stepPercentage = $this->task->re_issu_perc;
                break;

            case 4:
                $stepPercentage = $this->task->s_off_perc;
                break;

        }

        return   $stepPercentage;
    }


    //Re-calculate the Task Percentage based on the new Breakdown Count
    public function recalculateTaskStepPercentage($currentTaskPercentage){

        $newPercentage = 0;

        switch ($this->stepCount) {

            case 2:
                $newPercentage = $this->breakDownTwo($currentTaskPercentage);
                break;
            case 3:
                $newPercentage = $this->breakDownThree($currentTaskPercentage);
                break;
            case 4:
                $newPercentage = $this->breakDownFour($currentTaskPercentage);
                break;
            case 5:
                $newPercentage = $this->breakDownFive($currentTaskPercentage);
                break;
            case 10:
                $newPercentage = $this->breakDownTen($currentTaskPercentage);
                break;

        }

        return $newPercentage;

    }
    //get the Percentage if the break down is 2
    public function breakDownOne($percentage){

        switch ($percentage) {
            case ($percentage > 0 && $percentage < 100):
                $percentage = 0;
                break;
        }
        return $percentage;
    }

    //get the Percentage if the break down is 2
    public function breakDownTwo($percentage){

        switch ($percentage) {
            case ($percentage > 0 && $percentage < 100):
                $percentage = 50;
                break;
        }
        return $percentage;
    }

    //get the Percentage if the break down is 3
    public function breakDownThree($percentage){

        switch ($percentage) {
            case ($percentage > 0 && $percentage < 66):
                $percentage = 33;
                break;
            case ($percentage > 66 && $percentage < 100):
                $percentage = 66;
                break;
        }
        return $percentage;
    }

    //get the Percentage if the break down is 4
    public function breakDownFour($percentage){

        switch ($percentage) {
            case ($percentage > 0 && $percentage < 25):
                $percentage = 25;
                break;
            case ($percentage > 25 && $percentage < 50):
                $percentage = 50;
                break;
            case ($percentage > 50 && $percentage < 100):
                $percentage = 75;
                break;

        }
        return $percentage;
    }

    //get the Percentage if the break down is 5
    public function breakDownFive($percentage){

        switch ($percentage) {
            case ($percentage > 0 && $percentage < 20):
                $percentage = 20;
                break;
            case ($percentage > 20 && $percentage < 40):
                $percentage = 40;
                break;
            case ($percentage > 40 && $percentage < 60):
                $percentage = 60;
                break;
            case ($percentage > 60 && $percentage < 100):
                $percentage = 80;
                break;
        }
        return $percentage;
    }

    //get the Percentage if the break down is 5
    public function breakDownTen($percentage){

        switch ($percentage) {
            case ($percentage > 0 && $percentage < 10):
                $percentage = 10;
                break;
            case ($percentage > 10 && $percentage < 20):
                $percentage = 10;
                break;
            case ($percentage > 20 && $percentage < 30):
                $percentage = 10;
                break;
            case ($percentage > 30 && $percentage < 40):
                $percentage = 10;
                break;
            case ($percentage > 40 && $percentage < 50):
                $percentage = 50;
                break;
            case ($percentage > 60 && $percentage < 70):
                $percentage = 70;
                break;
            case ($percentage > 70 && $percentage < 80):
                $percentage = 80;
                break;
            case ($percentage > 80 && $percentage < 100):
                $percentage = 90;
                break;
        }
        return $percentage;
    }


    public function updateAndSaveTaskStepPercentage($percentage){

        switch ($this->stepNumber) {
            case 1:
                $this->task->gen_perc = $percentage;
                break;
            case 2:
                $this->task->rev_perc = $percentage;
                break;
            case 3:
                $this->task->re_issu_perc = $percentage;
                break;
            case 4:
                $this->task->s_off_perc = $percentage;
                break;
        }

        $this->task->save();
    }

    /**
     * @return mixed
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

}
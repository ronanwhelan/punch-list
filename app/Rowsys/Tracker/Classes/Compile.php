<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 1/3/16
 * Time: 1:30 PM
 */

namespace app\Rowsys\Tracker\Classes;

use App\Jobs\CompileAllTasks;
use App\Jobs\CompileTasks;
use App\Jobs\CompileTasksAfterModelDetailsUpdated;
use App\Jobs\CompileTaskTags;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskTypeStepPhase;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use App\Models\TaskRule;
use App\Jobs\ApplyUpdatedRuleToTasks;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Compile
{

    use DispatchesJobs;

    protected $message;

    /**
     * ------------------------------------------------
     *    Compile All the Project Tasks
     *  -----------------------------------------------
     *
     *  Applies the rule to all the tasks for the project.
     *  Used when the project has new baseline dates
     */
    public function compileAllTasks()
    {
        $now = Carbon::now()->subHour(26);

        DB::table('tasks')->update(['compiled_at' => '2000-01-01 00:01:00', 'compiled' => 0]);
       // Task::where('complete','!=', 3)->update(['compiled_at' => '2000-01-01 00:01:00', 'compiled' => 0]);

        Task::
        //where('complete', '=', 0)->where('complete', '=', 1)
            orderBy('id', 'ASC')
            ->chunk(50, function ($tasks) {
                $startId = $tasks->first()->id;
                $endId = $tasks->last()->id;
                $compileTasks = new CompileTasks($startId, $endId);
                $this->dispatch($compileTasks);
            });
    }

    /**
     * ------------------------------------------------
     *    Compile tasks after Rule Update
     *  -----------------------------------------------
     *
     *  Applies the rule to all the tasks for the project.
     *  Used when the project has new baseline dates
     * @param TaskRule $rule
     */
    public function compileAfterRuleUpdated(TaskRule $rule)
    {
        $now = Carbon::now()->subHour(26);

        Task::where('complete', 0)->update(['compiled_at' => '2000-01-01 00:01:00', 'compiled' => 0]);

        Task::where('complete', '=', 0)
            ->orderBy('id', 'ASC')
            // ->where('compiled_at', '<', $now)
            ->chunk(100, function ($tasks) {
                $startId = $tasks->first()->id;
                $endId = $tasks->last()->id;

                $compileTasks = new CompileTasks($startId, $endId);
                $this->dispatch($compileTasks);
            });
    }
    /**
     * ------------------------------------------------
     *    Compile All the Project Tasks
     *  -----------------------------------------------
     *
     *  Applies the rule to all the tasks for the project.
     *  Used when the project has new baseline dates
     */
    public function compileAllTasksOld()
    {

        //TODO project id
        $project_id = 1;
        $date = Carbon::now();
        //\Storage::disk('local')->append('JobsLog.txt', $date . ' | COMPILE ALL |  All tasks compile Started | ' . Auth::user()->id);
        TaskRule::where('project_id', '=', $project_id)->chunk(1, function ($rules) {
            foreach ($rules as $rule) {
                $compileJobForRule = new CompileAllTasks($rule, Auth::user()->id);
                $this->dispatch($compileJobForRule);
            }
        });
        //\Storage::disk('local')->append('JobsLog.txt', $date . ' | COMPILE ALL |  All tasks compiled Finished  | ' . Auth::user()->id);
    }


    /**
     * ------------------------------------------------
     *    Compile Selected Tasks after a Model is updated
     *  -----------------------------------------------
     *
     *  Compiles the tasks after an update is made to
     *  models that effect the naming e.g System Details, Task Typ, Sub Type
     */
    public function compileAffectedTasksAfterModelIsUpdate($columnName, $value)
    {
        // compile tasks that are associated to system X, or have task type X, or have sub type x
        $compileEffectedTasks = new CompileTasksAfterModelDetailsUpdated($columnName, $value);
        $this->dispatch($compileEffectedTasks);
        $count = Task::where($columnName, $value)->count();

        return $count;

    }

    /**
     * ------------------------------------------------
     *    Compile Task Tags
     *  -----------------------------------------------
     *
     *  Updates the task Tag with its auto generated tag
     */
    public function compileTaskTags()
    {
        //TODO project id
        $project_id = 1;
        $date = Carbon::now();
        //\Storage::disk('local')->append('JobsLog.txt', $date . ' | COMPILE TAGS |  tasks tag compile Started | ' . Auth::user()->id);
        $stages = Stage::get();
        foreach ($stages as $stage) {
            $taskTypesIds = Task::where('stage_id', $stage->id)
                ->where('project_id', $project_id)
                ->groupBy('task_type_id')
                ->lists('task_type_id');
            foreach ($taskTypesIds as $taskTypesId) {
                $compileJob = new CompileTaskTags($stage->id, $taskTypesId, Auth::user()->id);
                $this->dispatch($compileJob);
            }
        }
        //\Storage::disk('local')->append('JobsLog.txt', $date . ' | COMPILE TAGS |  tasks tag compiled Finished  | ' . Auth::user()->id);
    }


    /**
     * ------------------------------------------------
     *   Run Compile Batch
     *  -----------------------------------------------
     *
     */
    public function runCompileBatch()
    {


    }

    /**
     * ------------------------------------------------
     *    Re baseline the Project
     *  -----------------------------------------------
     *
     */
    public function reBaseLine()
    {
        //$query = "UPDATE tasks SET base_date = prim_date";
        $query = "UPDATE tasks SET base_date = last_td";
        \DB::getPdo()->exec($query);

    }

    /**
     * ------------------------------------------------
     *  Compile Tasks After Task Type Updated
     *  i.e when task type details are updated all related tasks
     *  must be update.
     *  -----------------------------------------------
     *
     */
    public function compileAfterTaskTypeUpdated($id)
    {
        $count = 33;

        //$count = Task::where('task_type_id', $id)->update(['group_id' => (int)$request->update_parent]);
        return $count;
    }


    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message . ' Compile Tasks Called';
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

}
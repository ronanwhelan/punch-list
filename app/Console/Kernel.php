<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Rowsys\Tracker\Classes\Compile;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        Commands\BackupDataBase::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $tasksCompiler = new Compile();
            $tasksCompiler->compileAllTasks();
            //echo 'compile tasks called' .PHP_EOL;
        })->dailyAt('03:00');//->daily();//->everyMinute();//->dailyAt('13:00');

         //$schedule->command('inspire')->everyMinute();
        //$schedule->command('backupDatabase')->everyMinute();



    }
}

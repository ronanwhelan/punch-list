<?php

namespace App\Jobs\Punchlist;

use App\Jobs\Job;
use App\Models\PunchList\Group;
use App\Models\PunchList\Item;
use App\Models\PunchList\SystemScheduleDates;
use App\Models\PunchList\Type;
use App\Models\ScheduleDate;
use App\Models\System;
use App\Rowsys\PunchList\Compile;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CompileItems extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $compile = new Compile();
        $message = $compile->compileItems();

    }
}

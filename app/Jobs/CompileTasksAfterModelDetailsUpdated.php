<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Task;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CompileTasksAfterModelDetailsUpdated extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $columnName;
    protected $value;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($columnName, $value)
    {
        $this->columnName = $columnName;
        $this->value = $value;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $applyRule = new ApplyRuleToTasks();
        $tasks = Task::where($this->columnName, $this->value)->get();
        foreach ($tasks as $task) {
            $applyRule->compileThisTask($task);

        }
    }
}

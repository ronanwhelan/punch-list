<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\TaskRule;
use Carbon\Carbon;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;

class CompileAllTasks extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $rule;
    protected $userId;


    /**
     * @param TaskRule $rule
     * @param $userId
     */
    public function __construct(TaskRule $rule, $userId)
    {
        //
        $this->rule = $rule;
        $this->userId = $userId;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $applyRule = new ApplyRuleToTasks();
        $applyRule->applyThisRuleToAllRelatedTasks($this->rule);
        $date = Carbon::now();
        //\Storage::disk('local')->append('JobsLog.txt',   $date . ' | COMPILE SUB | ' . $this->rule->taskType->name.' ('. $this->rule->stage->name .' STAGE) | '.$this->userId );
    }
}

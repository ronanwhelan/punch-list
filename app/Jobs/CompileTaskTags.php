<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskType;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CompileTaskTags extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $stage_id;
    protected $task_type_id;
    protected $userId;


    /**
     * @param $stage_id
     * @param $task_type_id
     * @param $userId
     */
    public function __construct($stage_id,$task_type_id, $userId)
    {
        //
        $this->stage_id = $stage_id;
        $this->task_type_id = $task_type_id;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stageName = Stage::find($this->stage_id)->short_name;
        $taskTypeName = TaskType::find($this->task_type_id)->short_name;
        $lastTaskTypeTag = Task::where('task_type_id', '=', $this->task_type_id)
            ->where('stage_id', '=', $this->stage_id)
            ->orderBy('id', 'desc')
            ->first()->tag;
        $lastTaskTypeNumber = $lastTaskTypeTag;
        $number = 1;

        Task::where('stage_id', '=', $this->stage_id)
            ->where('task_type_id', '=', $this->task_type_id)
            ->with('area', 'group', 'system')
            ->orderBy('number', '=', 'ASC')
            ->chunk(500, function ($tasks) use ($stageName,$taskTypeName,$number){
            foreach ($tasks as $task) {
                $task->tag = $stageName.'.'.$taskTypeName.'.'.sprintf('%03d',$number);
                $task->description =
                    $task->stage->description.' '.
                    $task->TaskType->name.' '.
                    $task->area->description .' ' .
                    $task->system->tag . ' '.$task->system->description . ' '.
                    $task->group->description;
                $task->save();
                $number ++;
            }
        });

        $date = Carbon::now();
        \Storage::disk('local')->append('JobsLog.txt',   $date . ' | COMPILE TAG | ' . $taskTypeName.' ('. $stageName .' STAGE) | '.$this->userId );

    }
}

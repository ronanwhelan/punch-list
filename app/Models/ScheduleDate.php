<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleDate extends Model {

	//
    //protected $table = 'schedule_dates';
    protected $table = 'schedule_activities';

    protected $connection = 'mysqlTwo';

    public function getDates()
    {
        return [
            'created_at',
            'updated_at',
            'start_date',
            'finish_date'
        ];

    }

    public function task()
    {
        return $this->hasMany(Task::class);
    }

}

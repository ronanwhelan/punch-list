<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskColor extends Model {

    //
    protected $table = 'task_colors';

    public function task()
    {
        return $this->hasMany('Task');
    }

}
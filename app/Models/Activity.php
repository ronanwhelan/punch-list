<?php

namespace App\Models;

use App\Models\PunchList\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Activity extends Model {

	//
    protected $table = 'schedule_activities';

    protected $guarded = [];

    protected $connection = 'mysqlTwo';

    protected $dates = ['start_date','finish_date'];


}

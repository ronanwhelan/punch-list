<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model {

    //
    protected $table = 'help_forums';

    //******  Task Rule  *********
    public function user()
    {
        return $this->hasOne(User::class);
    }

}
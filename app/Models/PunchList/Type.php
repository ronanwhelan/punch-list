<?php

namespace App\Models\PunchList;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{

    protected $table = 'punch_list_types';

    public function items()
    {
        return $this->hasMany(Item::class);
    }
}

<?php

namespace App\Models\PunchList;

use App\Models\Stage;
use App\Models\System;
use App\Models\TaskType;
use Illuminate\Database\Eloquent\Model;

class SystemScheduleDates extends Model
{
    protected $table = 'punch_list_system_schedule_links';

    protected $guarded = [''];

    /**
     * @return array
     *
     * This tells Laravel to return the dates as Carbon Instances
     */
    public function getDates()
    {
        return [
            'created_at',
            'updated_at',
            'm0_date',
            'm1_date',
            'm2_date',
            'm3_date',
            'm4_date',
            'm5_date',
            'm6_date',
            'm7_date',
            'm8_date',
            'm9_date',

        ];

    }

    public function system()
    {
        return $this->belongsTo(System::class);
    }

    //Milestone 01
    public function m1TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m1Stage()
    {
        return $this->belongsTo(Stage::class);
    }
    //Milestone 02
    public function m2TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m2Stage()
    {
        return $this->belongsTo(Stage::class);
    }
    //Milestone 03
    public function m3TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m3Stage()
    {
        return $this->belongsTo(Stage::class);
    }
    //Milestone 04
    public function m4TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m4Stage()
    {
        return $this->belongsTo(Stage::class);

    }
    //Milestone 05
    public function m5TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m5Stage()
    {
        return $this->belongsTo(Stage::class);

    }
    //Milestone 06
    public function m6TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m6Stage()
    {
        return $this->belongsTo(Stage::class);

    }
    //Milestone 07
    public function m7TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m7Stage()
    {
        return $this->belongsTo(Stage::class);

    }
    //Milestone 08
    public function m8TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m8Stage()
    {
        return $this->belongsTo(Stage::class);

    }
    //Milestone 09
    public function m9TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m9Stage()
    {
        return $this->belongsTo(Stage::class);

    }
    //Milestone 10
    public function m10TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m10Stage()
    {
        return $this->belongsTo(Stage::class);

    }
    //Milestone 11
    public function m11TaskType()
    {
        return $this->belongsTo(TaskType::class);
    }
    public function m11Stage()
    {
        return $this->belongsTo(Stage::class);

    }
}

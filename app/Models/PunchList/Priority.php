<?php

namespace App\Models\PunchList;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $table = 'punch_list_priorities';

    public function items()
    {
        return $this->hasMany(Item::class);
    }

}

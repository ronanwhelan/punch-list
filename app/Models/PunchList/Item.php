<?php

namespace App\Models\PunchList;

use App\Models\Area;
use App\Models\Company;
use App\Models\Component;
use App\Models\Project\SystemMilestone;
use App\Models\Role;
use App\Models\System;
use App\Models\User;
use App\Models\UserFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Item extends Model
{

    protected $table = 'punch_list_items';

    /**
     * @return array
     *
     * This tells Laravel to return the dates as Carbon Instances
     */
    public function getDates()
    {
        return [
            'created_at',
            'updated_at',
            'raised_date',
            'closed_date',
            'due_date',
            'owner_updated_date',
            'res_updated_date',
            'res_target_date',
            'assigned_date',
            'complete_date',
            'last_updated_at',
            'due_date_updated_on_date'
        ];
    }

    protected $colourDefault = "white";
    protected $colourDueInTwoWeeks = "orange";
    protected $colourPastDue = "LightSalmon";
    protected $colourError = "red";
    protected $colourNa = "lightgrey";
    protected $colourComplete = "lightgreen";
    protected $colourOpen = 'lightblue';

    //--------------------------
    // ---  RELATED USERS ------
    //--------------------------

    //******  Raised By  *********
    public function raisedByUser()
    {
        return $this->belongsTo(User::class);
    }

    public function dueDateUpdatedBy()
    {
        return $this->belongsTo(User::class);
    }
    public function raisedByRole()
    {
        return $this->belongsTo(Role::class);
    }
    public function closedBy()
    {
        return $this->belongsTo(User::class);
    }

    //******  Last Update By User  *********
    public function lastUpdatedBy()
    {
        return $this->belongsTo(User::class);
    }

    //******  Owner Last Update By User  *********
    public function ownerUpdatedBy()
    {
        return $this->belongsTo(User::class);
    }
    //******  Responsible Last Update By User  *********
    public function resUpdatedBy()
    {
        return $this->belongsTo(User::class);
    }

    //******  assigned Owner Group  *********
    public function resCompany()
    {
        return $this->belongsTo(Company::class);
    }

    //******  assigned Owner Group  *********
    public function assignedCompany()
    {
        return $this->belongsTo(Company::class);
    }

    //******  assigned Owner User  *********
    public function assignedOwnerUser()
    {
        return $this->belongsTo(User::class);
    }

    //******  assigned Doer Group  *********
    public function assignedDoerGroup()
    {
        return $this->belongsTo(User::class);
    }

    //******  assigned Doer User  *********
    public function assignedDoerUser()
    {
        return $this->belongsTo(User::class);
    }

    /* -----------------------------------
     *              ROLES
      -------------------------------------*/

    //******  Responsible Role  *********
    public function resRole()
    {
        return $this->belongsTo(Role::class);
    }
    //******  Responsible Role  *********
    public function resUser()
    {
        return $this->belongsTo(User::class);
    }


    //--------------------------------------
    // ---  RELATED AREA AND SYSTEMS  ------
    //--------------------------------------
    //******   AREA  *********
    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    //******  SYSTEM  *********
    public function system()
    {
        return $this->belongsTo(System::class);
    }

    //******  SYSTEM  *********
    public function component()
    {
        return $this->belongsTo(Component::class);
    }
    //--------------------------------------
    // ---  RELATED GROUP AND TYPES  ------
    //--------------------------------------
    //******   GROUP   *********
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    //******  TYPE  *********
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    //******  Genre / Class  *********
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    //******  Priority  *********
    public function priority()
    {
        return $this->belongsTo(Priority::class);/*$category = 'A';if ($this->category_id === 2) {$category = 'B';}return $category;*/
    }

    /**
     * --------------------------------------
     *    Get the Item  Status
     * -------------------------------------
     */
    public function getItemStatus()
    {
        $label = ' <label class="label label-info">OPEN</label>';
        $backGroundColour = $this->colourDefault;
        $rowBackGroundColour = $this->colourDefault;
        $closedBy = '';
        $closedOn = '';
        $status = (int)$this->complete;
        if ((int)$this->stage === 5) {
            $status = 2;
        }
        switch ($status) {
            case 0://Open
                $label = ' <label class="label label-info">Open</label>';
                $backGroundColour = $this->colourDefault;
                break;
            case 1://Closed
                $label = ' <label class="label label-success">Closed</label>';
                $backGroundColour = $this->colourComplete;
                $rowBackGroundColour = $this->colourComplete;
                $closedBy = 'by ' . $this->closedBy->name . ' ' . $this->closedBy->surname . '<br> on  ';
                $closedOn = $this->closed_date->format('d-m-Y');
                break;
            case 2://Rejected
                $label = ' <label class="label label-danger">Rejected</label>';
                $backGroundColour = $this->colourError;
                break;
        }
        $data = [
            'label' => $label,
            'backGroundColour' => $backGroundColour,
            'rowBackGroundColour' => $rowBackGroundColour,
            'closedBy' => $closedBy,
            'closedOn' => $closedOn,
        ];

        return $data;
    }

    public function getScheduleLinkStep(){
        $milestone = "No Link";

        if($this->linked_milestone > 0){
            $milestone = SystemMilestone::find($this->linked_milestone)->name;
        }


        return $milestone;

    }

    /**
     * --------------------------------------
     *    Location
     * -------------------------------------
     */
    public function getLocation()
    {
        $system = $this->system->tag;
        $building = $this->building;
        $floor = $this->floor;
        $room = $this->room;
        $addOn = $this->other_location;
        $data = $system . '<br>' . $building.'<br>'. $floor.'<br>'. $room.'<br>'. $addOn;
        return $data;
    }

    /**
     * --------------------------------------
     *    Get the Due Date Colour and Text
     * -------------------------------------
     */
    public function getDueDateColourAndText()
    {
        $today = Carbon::now();
        $twoWeekFromToday = Carbon::now()->addWeeks(2);

        $backGroundColour = $this->colourDefault;
        $dueText = ' <label class="label label-info">OPEN</label>';

        if ($this->due_date < $twoWeekFromToday) {
            $backGroundColour = $this->colourDueInTwoWeeks;
            $dueText = ' <label class="label label-warning">Due Within<br> 2 Weeks</label>';

        }
        if ($this->due_date < $today) {
            $backGroundColour = $this->colourPastDue;
            $dueText = ' <label class="label label-warning">Past Due</label>';
        }

        if ($this->complete === 1) {
            $backGroundColour = $this->colourComplete;
        }
        $data = [
            'dueText' => $dueText,
            'backGroundColour' => $backGroundColour,
        ];

        return $data;
    }

    /**
     * --------------------------------------
     *    Get the Responsible Party Status
     * -------------------------------------
     */
    public function getResStatus()
    {
        $label = ' <label class="label label-info">OPEN</label>';
        $backGroundColour = $this->colourDefault;
        $stage = (int)$this->res_status;

        switch ($stage) {
            case 1://Open
                $label = ' <label class="label label-info">Open</label>';
                $backGroundColour = $this->colourDefault;
                break;
            case 2://In Progress
                $label = ' <label class="label label-info">In Progress</label>';
                $backGroundColour = $this->colourDefault;
                break;
            case 3://Waiting For Others
                $label = ' <label class="label label-warning">Waiting For Others</label>';
                $backGroundColour = $this->colourDueInTwoWeeks;
                break;
            case 4://Complete
                $label = ' <label class="label label-success">Complete</label>';
                $backGroundColour = $this->colourComplete;
                break;
            case 5://Rejected
                $label = ' <label class="label label-danger">Rejected</label>';
                $backGroundColour = $this->colourError;
                break;
        }
        if($this->complete === 1){
            $backGroundColour = $this->colourComplete;
        }

        $data = [
            'label' => $label,
            'backGroundColour' => $backGroundColour
        ];

        return $data;
    }

    /**
     * --------------------------------------
     *    Get the Assigned Company Details
     * -------------------------------------
     */
    public function getAssignedDetails()
    {
        $assignedRole = '<div class="font-xs">Not Assigned</div>';
        $action = Action::where('item_id',$this->id)->first();
        $label = '';
        $backGroundColour = $this->colourDefault;
        $comment = '';
        $assignedDate = '';

        if(count($action) > 0){
            //$role = $this->assigned_user_id;
            $detail = $action->assignedRole->name.'<br>' .$action->assignedUser->name.' '.$action->assignedUser->surname.'<br>' ;
            $assignedRole = '<div class="">'.$detail.'</div>';

            $comment = '<div class="font-xs">'.$action->comment.'</div>';
            $status = (int)$action->status;

            $assignedDate = '<div class="font-xs">'.$this->res_target_date->format('d-m-Y').'</div>';

            switch ($status) {
                case 1://Open
                    $label = ' <label class="label label-info">Open</label>';
                    $backGroundColour = $this->colourDefault;
                    break;
                case 2://In Progress
                    $label = ' <label class="label label-info">In Progress</label>';
                    $backGroundColour = $this->colourDefault;
                    break;
                case 3://Waiting For Others
                    $label = ' <label class="label label-warning">Waiting For Others</label>';
                    $backGroundColour = $this->colourDueInTwoWeeks;
                    break;
                case 4://Complete
                    $label = ' <label class="label label-success">Complete</label>';
                    $backGroundColour = $this->colourComplete;
                    break;
                case 5://Rejected
                    $label = ' <label class="label label-danger">Rejected</label>';
                    $backGroundColour = $this->colourError;
                    break;
            }

        }

        if($this->complete === 1){
            $backGroundColour = $this->colourComplete;
        }

        $data = [
            'role' => $assignedRole,
            'label' => $label,
            'comment' => $comment,
            'backGroundColour' => $backGroundColour,
            'assignedDate' => $assignedDate,
        ];

        return $data;

    }

    public function isThereAnAttachment(){
        $attachment = Attachment::where('item_id',$this->id)->get();
        if(count($attachment) > 0){
            return 1;
        }else{
            return 0;
        }
    }


    //******  Check If task is my watch list *********
    public function isOnMyWatchList()
    {
        $isOnList = 0;
        $user = Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        if (count($userFilter) > 0) {
            $currentListString = $userFilter->item_flag_list;
            $currentListArray = explode(',', $currentListString);
            if (in_array($this->id, $currentListArray)) {
                $isOnList = 1;
            }
        }

        return $isOnList;
    }

    public function activatedStatus(){

        $status = (int)$this->activated_status;
        $label = '';

        switch ($status) {
            case 0://Proposed
                $label = ' <label class=" font-xs label label-primary ">Proposed</label>';
                break;
            case 1://Accepted
                $label = '<label class=" font-xs label label-success ">Accepted</label>';
                break;
            case 2://Rejected
                $label = ' <label class="font-xs label label-danger">Rejected</label>';
                break;
            case 3://Complete
                $label = ' <label class="font-xs label label-success">Complete</label>';
                break;
            case 4://Rejected
                $label = ' <label class=" font-xs label label-danger">Rejected</label>';
                break;
        }

        $data = [
            'label' => $label,
        ];

        return $data;


    }

}

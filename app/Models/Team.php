<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Team extends Model {

	//
    protected $table = 'teams';

    protected $connection = 'mysqlTwo';

    /**
     * The users that belong to the team.
     * @return
     */
    public function users()
    {
        $teams =  collect(DB::connection('mysql2')->table('team_user')
            ->where('team_id', $this->id)
            ->select('user_id')->get());

        return User::whereIn('id', $teams->values()->pluck('user_id')->toArray())->get();
        //return $this->belongsToMany(User::class,'team_user');
    }

}

<?php

namespace App\Models;

use App\Models\PunchList\Item;
use Illuminate\Database\Eloquent\Model;

class System extends Model {
    //
    protected $table = 'systems';

    //update

    protected $connection = 'mysqlTwo';

    protected $fillable = ['tag','description','area_id','project_id'];

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }


}

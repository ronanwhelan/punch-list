<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {

	//
    protected $table = 'groups';

    protected $fillable = ['name','short_name','description','project_id'];

    //******  Task Rule  *********
    public function taskRule()
    {
        return $this->hasOne(TaskRule::class);
    }

}

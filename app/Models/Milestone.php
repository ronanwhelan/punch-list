<?php

namespace App\Models;

use App\Models\PunchList\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Milestone extends Model {

	//
    protected $table = 'schedule_milestones';

    protected $connection = 'mysqlTwo';

}

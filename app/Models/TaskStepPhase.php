<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskStepPhase extends Model
{
    //
    protected $table = 'task_step_phases';

    //rotected $fillable = ['tag','description','area_id','project_id'];


    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function taskTypeStepPhase()
    {
        return $this->belongsTo(TaskTypeStepPhase::class);
    }

    // The phase owners - member of the group table for now
    public function p1Owner()
    {
        return $this->belongsTo(Group::class);
    }
    public function p2Owner()
    {
        return $this->belongsTo(Group::class);
    }
    public function p3Owner()
    {
        return $this->belongsTo(Group::class);
    }
    public function p4Owner()
    {
        return $this->belongsTo(Group::class);
    }
    public function p5Owner()
    {
        return $this->belongsTo(Group::class);
    }
    public function p6Owner()
    {
        return $this->belongsTo(Group::class);
    }
    public function p7Owner()
    {
        return $this->belongsTo(Group::class);
    }
    public function p8Owner()
    {
        return $this->belongsTo(Group::class);
    }
    public function p9Owner()
    {
        return $this->belongsTo(Group::class);
    }
    public function p10Owner()
    {
        return $this->belongsTo(Group::class);
    }

}

<?php

namespace App\Models;

use App\Models\PunchList\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class MainAppTask extends Model {

	//
    protected $table = 'tracker_tasks';

    protected $connection = 'mysqlTwo';

}

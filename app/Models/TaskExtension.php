<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskExtension extends Model
{
    //
    protected $table = 'task_extensions';

    /**
     * @return array
     *
     * This tells Laravel to return the dates as Carbon Instances
     */
    public function getDates()
    {
        return [
            'created_at',
            'updated_at',
        ];

    }

    protected $fillable = ['task_id'];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function whoUpdatedLast()
    {
        return $this->belongsTo(User::class);
    }
}

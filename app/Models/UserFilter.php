<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UserFilter extends Authenticatable
{


    /**
     * The attributes that are guarded from mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    // -------------------
    // Table Relationships
    // -------------------

    // A user has many Tasks
    public function user()
    {
        return $this->hasOne(User::class);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';

    protected $fillable = array('name','description');

    // ******  Users  *********
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskScheduleLink extends Model {

	//
    protected $table = 'task_schedule_links';

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    public function schedules()
    {
        return $this->hasMany(ScheduleDate::class);
    }

}

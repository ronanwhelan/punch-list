<?php

namespace App\Models;

use Carbon\Carbon;
use  Illuminate\Database\Eloquent\Model;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use Illuminate\Support\Facades\Auth;


/**
 * Class Task
 * @package App\Models
 */
class Task extends Model
{

    protected $table = 'tasks';

    protected $colourDefault = "white";
    protected $colourDueInTwoWeeks = "orange";
    protected $colourPastDue = "LightSalmon";
    protected $colourError = "red";
    protected $colourNa = "lightgrey";
    protected $colourComplete = "lightgreen";

    /**
     * @return array
     *
     * This tells Laravel to return the dates as Carbon Instances
     */
    public function getDates()
    {
        return [
            'created_at',
            'updated_at',
            'base_date',
            'prim_date',
            'next_td',
            'last_td',
            'gen_td',
            'gen_complete_date',
            'rev_td',
            'rev_complete_date',
            're_issu_td',
            're_issu_complete_date',
            's_off_td',
            's_off_complete_date',
            'revised_projected_date',
            'complete_date',
            'schedule_start_date',
            'schedule_finish_date',
            'compiled_at'
        ];

    }

    //protected $fillable = ['gen_td','rev_td','re_issu_td','s_off_td'];

    protected $taskVariables = ['gen_td', 'rev_td', 're_issu_td', 's_off_td'];

    //******   TASK VARIABLES  *********
    public function getTaskVariablesArray()
    {
        return $this->$taskVariables;
    }

    // Table Relationships
    //******   OWNER *********
    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    //******   AREA  *********
    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    //******  SYSTEM  *********
    public function system()
    {
        return $this->belongsTo(System::class);
    }

    //******   GROUP   *********
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    //******   TASK TYPE  *********
    public function taskType()
    {
        return $this->belongsTo(TaskType::class);
    }

    //******   TASK SUB TYPE  *********
    public function taskSubType()
    {
        return $this->belongsTo(TaskSubType::class);
    }

    //******  STAGE   *********
    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    //******   TestSpec  *********
    public function testSpec()
    {
        return $this->belongsTo(TaskSubType::class);
    }

    //******   Schedule  Number  *********
    public function scheduleNumber()
    {
        return $this->belongsTo('App\Models\ScheduleDate', 'schedule_number', 'number');
    }

    // *****  EXTENSION  **********
    public function extension()
    {
        return $this->hasOne(TaskExtension::class);
    }

    // *****  EXTENSION  **********
    public function stepPhase()
    {
        return $this->hasMany(TaskStepPhase::class);
    }

    // *****  Task Step Phase - Step 01 **********
    public function step_1_owner()
    {
        return $this->belongsTo(Group::class);
    }

    // *****  Task Step Phase - Step 02 **********
    public function step_2_owner()
    {
        return $this->belongsTo(Group::class);
    }

    // *****  Task Step Phase - Step 03 **********
    public function step_3_owner()
    {
        return $this->belongsTo(Group::class);
    }

    // *****  Task Step Phase - Step 04 **********
    public function step_4_owner()
    {
        return $this->belongsTo(Group::class);
    }

    //******  Assigned Role *********
    public function assignedRole()
    {
        return $this->belongsTo(Role::class);
    }
    //******  Assigned Team *********
    public function groupOwner()
    {
        return $this->belongsTo(Area::class);
    }
    //******  Assigned User *********
    public function assignedUser()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * ---------------------------------
     *    Apply Task Rule
     * ---------------------------------
     */
    public function applyTaskRule()
    {
        $applyRule = new ApplyRuleToTasks();
        $applyRule->compileThisTask($this);
        return $applyRule->getMessage();
    }


   public function getAssignedUserDetails(){

       $name = "Not Assigned";
       $user = User::find($this->assigned_user_id);

       if($this->assigned_user_id > 0 && isset($user)) {
           $name=    $this->assignedUser->name . ' ' .$this->assignedUser->surname ;

       }
       if($this->assigned_user_id > 0 && !isset($user)) {
           $name =   'User Does Not Exists';

       }
       return $name;
   }
    /**
     * ---------------------------------
     *    Get the Task Percentage Complete Value
     * ---------------------------------
     */
    public function getTaskCompletePercentage()
    {
        $total = 0;
        $count = 0;
        if ($this->gen_applicable) {
            $total = $this->gen_perc;
            $count++;
        }
        if ($this->rev_applicable) {
            $total = $total + $this->rev_perc;
            $count++;
        }
        if ($this->re_issu_applicable) {
            $total = $total + $this->re_issu_perc;
            $count++;
        }
        if ($this->s_off_applicable) {
            $total = $total + $this->s_off_perc;
            $count++;
        }

        if ($count > 0) {
            $percent = 0;
        } else {
            $percent = $total / $count;
        }

        return round($percent) . '%';
    }

    /**
     * ---------------------------------
     *    Get the Task used  Hours Percentage
     * ---------------------------------
     */
    public function getTaskHoursPercentage()
    {

        if ($this->target_val > 0) {

            $percent = ($this->earned_val / $this->target_val) * 100;
        } else {
            $percent = 0;
        }

        return round($percent) . '%';
    }

    /**
     * ---------------------------------
     *    Get the Task Next Step
     * ---------------------------------
     */
    public function getTaskNextStep()
    {
        $nextStep = 'Step Unknown';

        if ($this->gen_applicable && $this->gen_perc !== 100) {
            $nextStep = 'Generation';
        } elseif ($this->rev_applicable && $this->rev_perc !== 100) {
            $nextStep = 'Review';
        } elseif ($this->re_issu_applicable && $this->re_issu_perc !== 100) {
            $nextStep = 'Re-Issue';
        } elseif ($this->s_off_applicable && $this->s_off_perc !== 100) {
            $nextStep = 'Sign Off';
        }

        return $nextStep . '';
    }

    /**
     * ----------------------------------------
     *    Get the Task Step BackGround Colours
     *    Used for displaying the task status
     * ---------------------------------------
     */
    public function getTaskNextStepBackGroundColour()
    {
        $today = Carbon::now();
        $twoWeekFromToday = Carbon::now()->addWeeks(2);

        $nextTdColour = $this->colourDefault;
        $genColour = $this->colourDefault;
        $revColour = $this->colourDefault;
        $reIssuColour = $this->colourDefault;
        $sOffColour = $this->colourDefault;

        //Gen
        if ($this->gen_applicable) {
            if ($this->gen_td < $twoWeekFromToday) {
                $genColour = $this->colourDueInTwoWeeks;
            }
            if ($this->gen_td < $today) {
                $genColour = $this->colourPastDue;
            }
            if ($this->gen_perc === 100) {
                $genColour = $this->colourComplete;
            }
        } else {
            $genColour = $this->colourNa;
        }
        //Review
        if ($this->rev_applicable) {
            if ($this->rev_td < $twoWeekFromToday) {
                $revColour = $this->colourDueInTwoWeeks;
            }
            if ($this->rev_td < $today) {
                $revColour = $this->colourPastDue;
            }
            if ($this->rev_perc === 100) {
                $revColour = $this->colourComplete;
            }
        } else {
            $revColour = $this->colourNa;
        }
        //Re Issue
        if ($this->re_issu_applicable) {
            if ($this->re_issu_td < $twoWeekFromToday) {
                $reIssuColour = $this->colourDueInTwoWeeks;
            }
            if ($this->re_issu_td < $today) {
                $reIssuColour = $this->colourPastDue;
            }
            if ($this->re_issu_perc === 100) {
                $reIssuColour = $this->colourComplete;
            }
        } else {
            $reIssuColour = $this->colourNa;
        }
        //Sign Off
        if ($this->s_off_applicable) {
            if ($this->s_off_td < $twoWeekFromToday) {
                $sOffColour = $this->colourDueInTwoWeeks;
            }
            if ($this->s_off_td < $today) {
                $sOffColour = $this->colourPastDue;
            }
            if ($this->s_off_perc === 100) {
                $sOffColour = $this->colourComplete;
            }
        } else {
            $sOffColour = $this->colourNa;
        }

        //Next Target Date Colour
        if ($this->gen_applicable && $this->gen_perc !== 100) {
            $nextTdColour = $genColour;
        } elseif ($this->rev_applicable && $this->rev_perc !== 100) {
            $nextTdColour = $revColour;
        } elseif ($this->re_issu_applicable && $this->re_issu_perc !== 100) {
            $nextTdColour = $reIssuColour;
        } elseif ($this->s_off_applicable && $this->s_off_perc !== 100) {
            $nextTdColour = $sOffColour;
        }
        if ($this->complete === 1) {
            $nextTdColour = $this->colourComplete;
        }

        //Row colour e.g Error or Complete or White
        $rowColour = $this->colourError;
        if ($this->compile_error === 0) {

            if ($this->complete === 1) {
                $rowColour = $this->colourComplete;
            } else {
                $rowColour = $this->colourDefault;
            }
        }


        return [
            'nextTdColour' => $nextTdColour,
            'genColour' => $genColour,
            'revColour' => $revColour,
            'reIssuColour' => $reIssuColour,
            'sOffColour' => $sOffColour,
            'rowColour' => $rowColour,

        ];

    }

    //******  Check If task is my watch list *********
    public function isOnMyWatchList()
    {
        $isOnList = 0;
        $user = Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        if (count($userFilter) > 0) {
            $currentListString = $userFilter->task_flag_list;
            $currentListArray = explode(',', $currentListString);
            if (in_array($this->id, $currentListArray)) {
                $isOnList = 1;
            }
        }

        return $isOnList;
    }
    //******  Check If task is my watch list *********
    public function lastUpdatedBy()
    {
        $lastUpdatedBy = 'Has not been updated yet';
        $extension = TaskExtension::where('task_id', $this->id)->first();
        if(isset($extension)){
            $lastUpdatedBy =  $extension->whoUpdatedLast['full_name'].' -  '.$extension->updated_at->diffForHumans();
        }
        return $lastUpdatedBy;
    }


    //******  TASK COLOURS   *********
    public function genColor()
    {
        return $this->belongsTo(TaskColor::class);
    }

    public function revColor()
    {
        return $this->belongsTo(TaskColor::class);
    }

    public function reIssuColor()
    {
        return $this->belongsTo(TaskColor::class);
    }

    public function sOffColor()
    {
        return $this->belongsTo(TaskColor::class);
    }

}

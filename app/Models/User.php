<?php

namespace App\Models;

use App\Models\PunchList\Item;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'role',
        'punchlist_role',
        'access_to_tracker',
        'access_to_punchlist',
        'punchlist_filter',
        'confirmed',
        'company_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    // A user belongs to a company
    public function company(){
        return $this->belongsTo(Company::class);
    }

    /**
     * The users that belong to the Team.
     */
    public function teams()
    {
       // $teams =  DB::connection('mysql2')->table('team_user')->where('user_id', $userId)->get();
      //  return Team::whereIn('id', $teams);
       return $this->belongsToMany(Team::class,'team_user','user_id','team_id');
    }



    /*
    |--------------------------------------------------------------------------
    | USER ROLES
    |--------------------------------------------------------------------------
    */

    /**
     * Get the roles a user has
     */
    public function roles()
    {

       return $this->belongsToMany(Team::class,'team_user','team_id','user_id');
      // dd($this->belongsToMany(Role::class,'team_user','user_id','team_id'));
      //return $this->belongsToMany(Role::class);
       // return $this->belongsToMany('App\Models\Role', 'team_user','team_id','user_id');
       // return $this->belongsToMany(Role::class, 'team_user','user_id','team_id');
/*       return $this->belongsToMany(Role::class)->withTimestamps();
        //return $this->belongsToMany(Role::class)->withTimestamps();//with timestamps is need if the pivot table has timestamps
        $teams =  collect(DB::connection('mysqlTwo')->table('team_user')
            ->where('user_id', $this->id)
            ->select('team_id')->get());
        return Team::whereIn('id', $teams->values()->pluck('user_id')->toArray());*/

    }

    //Check if a user has a role
    public function hasRole($name){
        foreach($this->roles as $role){
            if ($role->name === $name)return true;
        }
        return false;
    }

    //Attach a role to the user - by role id
    public function assignRole($role){
        return $this->roles()->attach($role);
    }

    //Remove/Revoke  a role from the user - by role id
    public function removeRole($role){

        return $this->roles()->detach($role);
    }
    /**
     * Detach all roles from a user
     *
     * @return object
     */
    public function detachAllRoles()
    {
        DB::table('role_user')->where('user_id', $this->id)->delete();

        return $this;
    }

    //------------------------
    //---- TRACKER -------
    //------------------------
    // A user has many Tasks
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    //------------------------
    //---- PUNCH LIST -------
    //------------------------
    // A user has many Items
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    // A user has many Tasks
    public function filter()
    {
        return $this->blongsTo(UserFilter::class);
    }

    // Check if a User is Confirmed
    public function isConfirmed()
    {
        if ($this->confirmed === 1) {
            return true;
        } else {
            return false;
        }
    }

    // Check if a User has Read Only Access
    public function hasReadOnlyAccess()
    {
        if ($this->role < 4 && $this->isConfirmed()) {
            return true;
        } else {
            return false;
        }
    }

    // Check if a User is a Manager
    public function isManager()
    {
        if ($this->role > 3 && $this->isConfirmed()) {
            return true;
        } else {
            return false;
        }
    }

    // Check if a User is Confirmed
    public function isAdministrator()
    {
        if ($this->role > 5 && $this->isConfirmed()) {
            return true;
        } else {
            return false;
        }

    }

    // Check if a User has access to the tracker
    public function hasTrackerAccess()
    {
        if ($this->access_to_tracker === 1 && $this->isConfirmed()) {
            return true;
        } else {
            return false;
        }
    }

    // Check if a User has access to the punch list
    public function hasPunchListAccess()
    {
        if ($this->access_to_punchlist === 1 && $this->isConfirmed()) {
            return true;
        } else {
            return false;
        }
    }




}

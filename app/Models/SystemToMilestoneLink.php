<?php

namespace App\Models;

use App\Models\PunchList\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class SystemToMilestoneLink extends Model {

	//
    protected $table = 'schedule_system_links';

    protected $connection = 'mysqlTwo';


    /**
     * The related Milestone
     */
    public function milestone()
    {
        return $this->belongsTo(Milestone::class);
    }


}

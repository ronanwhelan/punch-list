<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobLog extends Model {

    /**
     * @return array
     *
     * This tells Laravel to return the dates as Carbon Instances
     */
    public function getDates()
    {
        return ['created_at', 'updated_at','started_at' ];

    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskType extends Model {

	//
    protected $table = 'task_types';

    protected $fillable = ['name','short_name','description','group_id','project_id'];

    //******  Task Rule  *********
    public function taskRule()
    {
        return $this->hasOne(TaskRule::class);
    }
    //******  Task Group  *********
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    //******  Task Group  *********
    public function testSpec()
    {
        return $this->belongsTo(TaskTestSpec::class);
    }

    //******  Number of Rules *********
    public function numberOfRules(){

       $associatedRuleDetails = '';

        $rules = TaskRule::where('task_type_id',$this->id)->get();

        if(isset($rules)){

            if(sizeof($rules) === 2){
                $associatedRuleDetails = 'Prep & Exec';
            }else if (sizeof($rules) === 1){

                if($rules[0]->stage_id === 1){
                    $associatedRuleDetails = 'Prep Only';
                }
                if($rules[0]->stage_id === 2){
                    $associatedRuleDetails = 'Exec Only';
                }

            }


        }else{
            $associatedRuleDetails = '<strong>No Rules!</strong>';
        }


        return $associatedRuleDetails;

    }

}

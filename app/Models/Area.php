<?php

namespace App\Models;

use App\Models\PunchList\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Area extends Model {

	//
    protected $table = 'areas';

    protected $fillable = ['name','description','project_id'];

    protected $connection = 'mysqlTwo';


    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function system()
    {
        return $this->hasOne(System::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

}

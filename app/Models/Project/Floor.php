<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $table = 'floors';

    protected $connection = 'mysqlTwo';

    //******  Building *********
    public function building()
    {
        return $this->belongsTo(Building::class);
    }


}

<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $table = 'buildings';

    protected $connection = 'mysqlTwo';
}

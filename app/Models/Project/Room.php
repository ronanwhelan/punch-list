<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';

    protected $connection = 'mysqlTwo';

    //******  Floor *********
    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 26/6/16
 * Time: 1:24 PM
 */

namespace App\Models\Roster;


use App\Models\Area;
use Illuminate\Database\Eloquent\Model;

class AreaHour extends Model{

    protected $table = 'roster_area_hours';

    protected $fillable = ['area_id','hours','month_num','month','year'];


    public function area()
    {
        return $this->belongsTo(Area::class);
    }


}
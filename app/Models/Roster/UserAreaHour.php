<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 26/6/16
 * Time: 1:24 PM
 */

namespace App\Models\Roster;


use App\Models\Area;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserAreaHour extends Model{

    protected $table = 'roster_user_area_hours';

    protected $fillable = ['user_id','area_id','hours','month_num','month','year'];


    // A user has many Tasks
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

}
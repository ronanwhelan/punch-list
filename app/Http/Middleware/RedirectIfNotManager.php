<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() === null || !$request->user()->isManager()) {
            return redirect('welcome')->with('message', 'You must be a Manager to Access that part of the Application');
        }
        return $next($request);

    }
}

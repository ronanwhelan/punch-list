<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotConfirmed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() === null || !$request->user()->isConfirmed()) {
            return redirect('welcome')->with('message', 'Your account has still not been confirmed. Please contact your administrator');
        }
        return $next($request);

    }
}

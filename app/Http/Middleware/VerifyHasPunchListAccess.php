<?php

namespace App\Http\Middleware;

use Closure;

class VerifyHasPunchListAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() === null || !$request->user()->hasPunchListAccess() ){
            return redirect()->guest('login');
            //return redirect('welcome')->with('message', 'You must be an Administrator to Access that part of the Application');
        }
        return $next($request);

    }
}

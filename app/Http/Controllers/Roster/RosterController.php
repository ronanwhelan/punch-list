<?php

namespace App\Http\Controllers\Roster;

use App\Models\Area;
use App\Models\Roster\UserAreaHour;
use App\Models\User;

use App\Rowsys\Admin\Files\CsvFileImporter;
use App\Rowsys\Admin\Files\ImportRosterAreaDataFromFile;
use App\Rowsys\Admin\Files\ImportRosterUserDataFromFile;

use App\Rowsys\Tracker\Classes\Compile;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class RosterController extends Controller
{

    protected $months = array('January', 'February', 'March', 'April', 'May',
        'June', 'July ', 'August', 'September', 'October', 'November', 'December'
    );


    /* =================================
    |
    |           USER ROSTER
    |
    |================================ */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rowsys.roster.user.read.index', [
            //'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        $areas = Area::get();

        $userAreaHoursData = UserAreaHour::get();

        return view('rowsys.roster.user.create.index', [
            'users' => $users,
            'areas' => $areas,
            'months' => $this->months,
            'userAreaHoursData' => $userAreaHoursData,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user' => 'required',
            'area' => 'required',
            'month' => 'required',
            'hours' => 'required|integer|min:1',
        ]);

        $today = Carbon::today();
        $year = $request->year;
        $month = (int)$request->month;
        $d=cal_days_in_month(CAL_GREGORIAN,$month,$year);
        $day = $d;
        $tz = 'Asia/Singapore';
        $monthDate = Carbon::createFromDate($year, $month, $day, $tz);

        $userAreaHours = new UserAreaHour();
        $userAreaHours->user_id = $request->user;
        $userAreaHours->area_id = $request->area;

        $userAreaHours->month_date = $monthDate;
        $userAreaHours->year = $year;

        $userAreaHours->month_num = $month;
        $userAreaHours->month = $this->months[$month-1];
        $userAreaHours->hours = $request->hours;

        $userAreaHours->save();

        $message = 'User Area hours added';
        return Redirect::back()->with('message', $message)->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('rowsys.roster.user.update.form', [
            //'items' => $items,
        ]);
        return "controller form for user roster";
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return " update the user roster";
    }

    //updates the hours with X-editable
    public function updateHours(Request $request,$id){
        //return $request->hours;
        $hours = (int)$request->value;
        if (is_numeric($request->value) && $hours > 0) {
            $userAreaHours = UserAreaHour::find($id);
            $userAreaHours->hours = $hours;
            $userAreaHours->save();
            return \Response::json([
                'status'=> 'success',
                'msg' => 'Updated'
            ], 200);
        }
        else {
            return \Response::json([
                'hours' => $request->name,
                'value' => $request->value,
                'status'=> 'error',
                'msg' => 'Value must be number'
            ], 200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        UserAreaHour::destroy($id);
        $data = ['message'=>'Deleted '.$id];
        return $data;
    }


    /* =================================
    |
    |           AREA ROSTER
    |
    |================================ */

    public function test(Request $request,$id){
        $countries = [['id' =>'gb','text'=>'Great Britain'],['id' =>'us','text'=>'United States']];
        return $countries;
        $pk = $request->pk;
        return \Response::json([
            'status'=> 'error',
            'msg' => 'No Dates For Selected System.'
        ], 200);
        dd($request);
        return " im here";
    }
    public function test1(){
        $countries = [['id' =>'gb','text'=>'Great Britain'],['id' =>'us','text'=>'United States']];
        return $countries;
    }



    /* =================================
    |
    |          IMPORT
    |
    |================================ */
    public function showImportAreaData()
    {
        return view('rowsys.roster.import.area.index', [
        ]);
    }

    public function ImportAreaData(Request $request)
    {
        $message = '';
        $returnedMessages = '';
        $errorData = [];
        $file = $request->file('file');

        $this->validate($request, [
            //'file' => 'required|mimes:csv,xlsx,xls',
            'file' => 'required',
        ]);

        $validated = true;
        $extension = $file->getClientOriginalExtension();

        if (!isset($file) || $extension !== "csv" || !file_exists($file) || !is_readable($file)) {
            $validated = false;
        }

        //Copy file to temp directory and normalize it
        $fileImporter = new CsvFileImporter();
        $file = $fileImporter->moveAndNormalize($file);
        $filePath = $file->getPathName();
        $importScheduleFromFile = new ImportRosterAreaDataFromFile($filePath);

        if ($validated) {
            try {
                //$importScheduleFromFile->clearScheduleTable();
                //$count  = $importScheduleFromFile->importToScheduleDatesTable();
                $returnedMessages = $returnedMessages . ' / ' . $importScheduleFromFile->deleteTempTable();
                $returnedMessages = $returnedMessages . ' / ' . $importScheduleFromFile->createTempTable();

                $count = $importScheduleFromFile->importBasicFileToTempTable();

                $errors = \DB::table('roster_area_hours_tmp')
                    ->where('area_id', null)
                    ->orWhere('area_id', 0)
                    ->count();

                //$errors = ScheduleDate::where('start_date',null)->orWhere('finish_date',null)->count();
                if ($errors === 0) {
                    $message = $count . ' Items Imported.';
                    $importScheduleFromFile->clearTable();
                    $importScheduleFromFile->copyTempTableToLiveTable();
                    //Delete the temp file - ie. the import file

                } else {
                    $countErrors =  \DB::table('roster_area_hours_tmp')->where('area_id', null)->orWhere('area_id', 0)->get();
                    if ($countErrors > 0) {
                        $message = $message . 'Opps! Import Failed!  There are ' . sizeof($countErrors) . ' entries with the incorrect area format in the file.Please correct the date format and try again';
                    } else {
                        $message = $message . 'Something is not quite right with the File. Ensure the file is setup accordingly ';
                    }

                    $errorData = \DB::table('roster_area_hours_tmp')->where('area_id', null)->orWhere('area_id', 0)
                        ->get();
                    $data = ['message' => $message, 'errorData' => $errorData];
                }

            } catch (\Exception $e) {
                $message = $message . ' Something is not right  :' . $e;
            } finally {

            }

        } else {
            $message = $message . ' :Validation Failed - Please check the file and try again. File extension must be csv - Type .' . $extension . ' provided';
        }

        $importScheduleFromFile->deleteTempTable();
        $importScheduleFromFile->deleteTempImportFile();

        return redirect('/roster/import/area')->with('message', $message);

       // return $message;
    }



    public function showImportUserData()
    {
        return view('rowsys.roster.import.user.index', [
        ]);
    }


    public function ImportUserData(Request $request)
    {
        $message = '';
        $returnedMessages = '';
        $errorData = [];
        $file = $request->file('file');

        $this->validate($request, [
            //'file' => 'required|mimes:csv,xlsx,xls',
            'file' => 'required',
        ]);

        $validated = true;
        $extension = $file->getClientOriginalExtension();

        if (!isset($file) || $extension !== "csv" || !file_exists($file) || !is_readable($file)) {
            $validated = false;
        }


        if ($validated) {
            try {
                //Copy file to temp directory and normalize it
                $fileImporter = new CsvFileImporter();
                $file = $fileImporter->moveAndNormalize($file);
                $filePath = $file->getPathName();


                $importScheduleFromFile = new ImportRosterUserDataFromFile($filePath);
                //$importScheduleFromFile->clearScheduleTable();
                //$count  = $importScheduleFromFile->importToScheduleDatesTable();
                $returnedMessages = $returnedMessages . ' / ' . $importScheduleFromFile->deleteTempTable();
                $returnedMessages = $returnedMessages . ' / ' . $importScheduleFromFile->createTempTable();

                return 'im here';
                //$importScheduleFromFile->clearTempScheduleTable();
                $count = $importScheduleFromFile->importBasicFileToTempScheduleDatesTable();

                $errors = \DB::table('schedule_dates_tmp')->where('start_date', null)->orWhere('finish_date', null)->count();

                //$errors = ScheduleDate::where('start_date',null)->orWhere('finish_date',null)->count();
                if ($errors === 0) {
                    $message = $count . ' Dates Imported.';
                    $importScheduleFromFile->clearScheduleTable();
                    $importScheduleFromFile->copyTempTableToScheduleTable();
                    //Tasks
                    $tasksCompiler = new Compile();
                    $tasksCompiler->compileAllTasks();
                    //Items
                    $compileJob = new CompileItems();
                    $this->dispatch($compileJob);

                    //Delete the temp file - ie. the import file
                    $importScheduleFromFile->deleteTempScheduleTable();
                    $importScheduleFromFile->deleteTempImportScheduleFile();

                } else {
                    $countErrors = ScheduleDate::where('start_date', null)->orWhere('finish_date', null)->count();
                    if ($countErrors > 0) {
                        $message = $message . 'Opps! Import Failed!  There are ' . $countErrors . ' entries with the incorrect date format in the file.Please correct the date format and try again';
                    } else {
                        $message = $message . 'Something is not quite right with the File. Ensure the file is setup accordingly ';
                    }

                    $errorData = \DB::table('schedule_dates_tmp')->where('start_date', null)->orWhere('finish_date', null)->count();
                    $data = ['message' => $message, 'errorData' => $errorData];
                }
                //$importScheduleFromFile->deleteTempScheduleTable();
                //$message = $message .' Delete File ='.$importScheduleFromFile->deleteTempImportScheduleFile();


            } catch (\Exception $e) {
                $message = $message . ' Something is not right  :' . $e;
            } finally {

            }

        } else {
            $message = $message . ' :Validation Failed - Please check the file and try again. File extension must be csv - Type .' . $extension . ' provided';
        }

        return $message;
       // return redirect('/schedule/import')->with('message', $message);
    }

}

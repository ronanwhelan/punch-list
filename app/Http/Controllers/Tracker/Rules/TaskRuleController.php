<?php

namespace App\Http\Controllers\Tracker\Rules;

use App\Jobs\ApplyUpdatedRuleToTasks;
use App\Jobs\JobCompileTasksAfterRuleUpdated;
use App\Models\Group;
use App\Models\ScheduleDate;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskType;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TaskRule;
use App\Rowsys\Tracker\Classes\UpdateRule;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use Illuminate\Support\Facades\Auth;

class TaskRuleController extends Controller
{
    /**
     * Display the Task Rules Index Page - table View
     *
     * @return Response
     */
    public function index()
    {
        $taskRules = TaskRule::with('taskType', 'stage', 'taskGroup')->get();

        $showMultiDate= 0;
        $multiDateConfigChoice = \Config::get('figaro.multi-date', 'false');
        if($multiDateConfigChoice ){
            $showMultiDate = 1;
        }


        return view('rowsys.tracker.tasks.rules.general.index', [
            'taskRules' => $taskRules,
            'showMultiDate' => $showMultiDate,
        ]);
    }

    public function returnTaskRuleDetail($id)
    {
        $taskRule = TaskRule::with('taskType', 'stage', 'taskGroup')->find($id);
        $taskCount = Task::where('task_type_id', '=', $taskRule->taskType->id)->where('stage_id', '=', $taskRule->stage->id)->count();
        $showMultiDate= 0;
        $multiDateConfigChoice = \Config::get('figaro.multi-date', 'false');
        if($multiDateConfigChoice && $taskRule->stage_id === 2  ){
            $showMultiDate = 1;
        }
        $multiDateText = 'OFF';
        if($taskRule->multi_date){
            $multiDateText = 'ON';
        }

        return view('rowsys.tracker.tasks.rules.general.rule_details', [
            'taskRule' => $taskRule,
            'taskCount' => $taskCount,
            'showMultiDate' => $showMultiDate,
            'multiDateText' => $multiDateText,
        ]);
    }

    public function scheduleDatesView()
    {
        $scheduleDates = ScheduleDate::orderBy('number','ASC')
            ->where('finish_date','!=','null')
            ->take(500)
            ->get();

        return view('rowsys.tracker.tasks.rules.scheduler_import.index', [
            'scheduleDates' => $scheduleDates,
        ]);


    }

    /**
     * ---------------------------------
     *         Update Task Rule
     *  ---------------------------------
     *
     *  Updates the Task Rule &
     *  Updates ALL the Tasks that are effected
     */
    public function update(Request $request, $id)
    {
        //Find the Task Rule to Be Update
        $taskRule = TaskRule::find($id);
        //----------------------------
        //   Update the Task Rule Values
        //----------------------------
        // Applicable
        $taskRule->gen_ex_applicable = (int)$request->gen_ex_applicable;
        $taskRule->rev_applicable = (int)$request->review_applicable;
        $taskRule->re_issu_applicable = (int)$request->re_issue_applicable;
        $taskRule->s_off_applicable = (int)$request->sign_off_applicable;

        // Buffer Days
        $taskRule->gen_ex_buffer_days = (int)$request->gen_ex_buffer_days;
        $taskRule->rev_buffer_days = (int)$request->review_buffer_days;
        $taskRule->re_issu_buffer_days = (int)$request->re_issue_buffer_days;
        $taskRule->s_off_buffer_days = (int)$request->s_off_buffer_days;

        // Hrs Percentage
        $taskRule->gen_ex_hrs_perc = (int)$request->gen_hrs;
        $taskRule->rev_hrs_perc = (int)$request->rev_hrs;
        $taskRule->re_issu_hrs_perc = (int)$request->re_issu_hrs;
        $taskRule->s_off_hrs_perc = (int)$request->s_off_hrs;

        //Multi Update
        if ($request->has('multi_date')){
            $taskRule->multi_date = (int)$request->multi_date;
        }

        //----------------------------
        //   Save the updated task Rule
        //----------------------------
        $taskRule->save();
        //-----------------------------
        //   Update ALL Effected Tasks
        //-----------------------------
        //$job = (new ApplyUpdatedRuleToTasks($taskRule))->delay(60 * 1);
        //$this->dispatch($job);
        Task::where('complete', 0)
            ->where('task_type_id', '=', $taskRule->task_type_id)
            ->where('stage_id', '=', $taskRule->stage_id)
            ->update(['compiled_at' => '2000-01-01 00:01:00', 'compiled' => 0]);

        Task::where('task_type_id', '=', $taskRule->task_type_id)
            ->where('stage_id', '=', $taskRule->stage_id)
            ->where('complete', '=', 0)
            ->orderBy('id','ASC')
            ->chunk(50, function ($tasks) use ($taskRule) {
                $startId = $tasks->first()->id;
                $endId = $tasks->last()->id;
                $compileTasks = new JobCompileTasksAfterRuleUpdated($startId, $endId,$taskRule->id);
                $this->dispatch($compileTasks);
            });
        //$updateAllTasksJob = new ApplyUpdatedRuleToTasks($taskRule, Auth::user()->id);
       // $this->dispatch($updateAllTasksJob);//->delay(60 * 1);
        //(new ApplyRuleToTasks())->applyThisRuleToAllRelatedTasks($taskRule);
        // $taskRule->applyRuleToAllTasks();

        //------------------------
        //  Return Confirmation Message
        //------------------------
        $message = "This should not take too long<br>Page will re-load";
        if ((int)$request->task_rule_count > 2000) {
            $message = " Allow a minute or so to update all effected Tasks.";
        };

        $data = array(
            'taskRule' => $taskRule,
            'message' => $message,
        );

        return $data;
    }


    /**
     * ---------------------------------
     *         Delete Task Rule
     *  ---------------------------------
     *
     */
    public function deleteRule($id)
    {
        TaskRule::destroy($id);
        $message = "Rule Deleted";
        return \Redirect::back()->with('rule-message', $message)->withInput();
    }

    public  function hello(){

        dd('im here in hello');

}

    /**
     * ---------------------------------
     *         Add All Task Rules
     *  ---------------------------------
     * A shortcut way to add all the task Rules
     * for each task type and each each stage
     */
    public function addAllTaskRules()
    {
        $taskTypes = TaskType::all();
        $stages = Stage::all();
        //$taskRules = TaskRule::destroy();
        $count = 0;
        $num = 0;
        foreach ($taskTypes as $taskType) {
            $applicableNum = $taskType->rule_type;
            //1 = GEN , 2 = EXEC, 3 = GEN & EXEC
            if ($applicableNum === 3) {
                foreach ($stages as $stage) {
                    $ruleExists = TaskRule::where('task_type_id',$taskType->id)
                        ->where('stage_id',$stage->id)
                        ->exists();
                    if($ruleExists !== true){
                        $this->addTaskRule($stage->id, $taskType);
                        $count++;
                    }
                }
            } else {
                if ($applicableNum === 2) {
                    $ruleExists = TaskRule::where('task_type_id',$taskType->id)
                        ->where('stage_id',2)
                        ->exists();
                    if($ruleExists !== true) {
                        $this->addTaskRule(2, $taskType);
                        $count++;
                    }
                } else {
                    if ($applicableNum === 1) {
                        $ruleExists = TaskRule::where('task_type_id',$taskType->id)
                            ->where('stage_id',1)
                            ->exists();
                        if($ruleExists !== true) {
                            $this->addTaskRule(1, $taskType);
                            $count++;
                        }
                    }
                }
            }
        }

        return $count . ' Task Rules added';
    }

    public function addRuleForStage(Request $request){

        $taskType = TaskType::find($request->taskTypeId);
        $choice = (int)$request->rule_choice;
        $prepStageRuleExists = TaskRule::where('task_type_id',$request->taskTypeId)->where('stage_id',1)->exists();
        $execStageRuleExists = TaskRule::where('task_type_id',$request->taskTypeId)->where('stage_id',2)->exists();

        switch ($choice) {
            case 1:
                //Prep Stage
                if($prepStageRuleExists !== true){
                    $this->addTaskRule(1, $taskType);
                }
                break;
            case 2:
                //Exec Stage
                if($execStageRuleExists !== true) {
                    $this->addTaskRule(2, $taskType);
                }
                break;
            case 3:
                if($prepStageRuleExists !== true){
                    $this->addTaskRule(1, $taskType);
                }
                if($execStageRuleExists !== true) {
                    $this->addTaskRule(2, $taskType);
                }
                break;
        }

        $message = "Rule(s) Added";
        return \Redirect::back()->with('rule-message', $message)->withInput();

    }

    private function addTaskRule($stage_id, TaskType $taskType)
    {
        $taskRule = new TaskRule();
        $taskRule->project_id = 1;
        $taskRule->task_group_id = $taskType->group_id;
        $taskRule->task_type_id = $taskType->id;
        $taskRule->stage_id = $stage_id;


        if($stage_id === 1){
            $taskRule->gen_ex_buffer_days = -14;
            $taskRule->gen_ex_applicable = 1;
            $taskRule->gen_ex_hrs_perc = 70;

            $taskRule->rev_buffer_days = 0;
            $taskRule->rev_applicable = 0;
            $taskRule->rev_hrs_perc = 0;

            $taskRule->re_issu_buffer_days = -7;
            $taskRule->re_issu_applicable = 1;
            $taskRule->re_issu_hrs_perc = 20;

            $taskRule->s_off_buffer_days = 0;
            $taskRule->s_off_applicable = 1;
            $taskRule->s_off_hrs_perc = 10;
        }


        if($stage_id === 2){
            $taskRule->gen_ex_buffer_days = 0;
            $taskRule->gen_ex_applicable = 1;
            $taskRule->gen_ex_hrs_perc = 70;

            $taskRule->rev_buffer_days = 0;
            $taskRule->rev_applicable = 0;
            $taskRule->rev_hrs_perc = 0;

            $taskRule->re_issu_buffer_days = 7;
            $taskRule->re_issu_applicable = 1;
            $taskRule->re_issu_hrs_perc = 20;

            $taskRule->s_off_buffer_days = 7;
            $taskRule->s_off_applicable = 1;
            $taskRule->s_off_hrs_perc = 10;
        }



        $taskRule->default_hrs = 43;
        $taskRule->save();
    }



}

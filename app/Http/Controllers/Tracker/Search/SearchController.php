<?php

namespace App\Http\Controllers\Tracker\Search;

use App\Models\Role;
use App\Models\ScheduleDate;
use App\Models\Task;
use App\Models\TaskSubType;
use App\Models\UserFilter;
use App\Rowsys\Tracker\Classes\Search;
use App\Rowsys\Tracker\Classes\TaskStats;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\System;
use App\Models\Group;
use App\Models\Stage;
use App\Models\TaskType;
use Illuminate\Database\Eloquent\Builder;

class SearchController extends Controller
{
    //
    public function getTaskSearchForm(Request $request)
    {
        //--------------------------------------
        // GET User Filters to For DropDowns - Saved last Search
        //--------------------------------------
        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $searchText = $userFilter->search_text;
        $selectedAreasArray = array_map('intval', explode(',', $userFilter->search_area_ids));
        $selectedSystemsArray = array_map('intval', explode(',', $userFilter->search_system_ids));
        $selectedGroupsArray = array_map('intval', explode(',', $userFilter->search_group_ids));
        $selectedTypesArray = array_map('intval', explode(',', $userFilter->search_type_ids));
        $selectedStagesArray = array_map('intval', explode(',', $userFilter->search_stage_ids));
        $selectedRolesArray = array_map('intval', explode(',', $userFilter->search_role_ids));
        $genStageSelected = false;
        $stages = Stage::all();
        if (str_contains(implode(",", $selectedStagesArray), strval($stages[0]->id)) || $userFilter->search_stage_ids === "") {
            $genStageSelected = true;
        }
        $exeStageSelected = false;
        if (str_contains(implode(",", $selectedStagesArray), strval($stages[1]->id)) || $userFilter->search_stage_ids === "") {
            $exeStageSelected = true;
        }

        //--------------------------------------
        // Search Query
        //--------------------------------------
        $search = new Search($request);
        $search->addSavedSearchToRequest();
        $search->buildAndReturnMainQuery();
        $search->addStepStatus();
        $query = $search->getQuery();
        $statsObj = new TaskStats();
        $stats = $statsObj->getSearchStats($query);

       // dd($stats);

        // $project_id = $request->session()->get('project_id');
        //todo clean this up - no need for the data here
        $project_id = 1;
        $areas = Area::where('project_id', '=', $project_id)->orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        if($selectedAreasArray[0] === 0){
            $systems = System::where('project_id', '=', $project_id)->orderBy('tag', 'ASC')->whereNotIn('tag', array('N/A'))->get();
        }else{
            $systems = System::where('project_id', '=', $project_id)->orderBy('tag', 'ASC')->whereIn('area_id',$selectedAreasArray)->whereNotIn('tag', array('N/A'))->get();
        }

        $groups = Group::where('project_id', '=', $project_id)->orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $taskTypes = TaskType::where('project_id', '=', $project_id)->orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $taskSubTypes = TaskSubType::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $roles = Role::get();

        return view('rowsys.tracker.tasks.search.task_search_form', [
            //return view('tasks.read.tables.index', [
            'areas' => $areas,
            'systems' => $systems,
            'groups' => $groups,
            'taskTypes' => $taskTypes,
            'taskSubTypes' => $taskSubTypes,
            'stages' => $stages,
            'stats' => $stats,
            'roles' => $areas,
            'searchText' =>$searchText,
            'selectedAreasArray' => $selectedAreasArray,
            'selectedSystemsArray' => $selectedSystemsArray,
            'selectedGroupsArray' => $selectedGroupsArray,
            'selectedTypesArray' => $selectedTypesArray,
            'selectedRolesArray' => $selectedRolesArray,
            'genStageSelected' => $genStageSelected,
            'exeStageSelected' => $exeStageSelected,

        ]);

    }

    //
    public function getSearchStats(Request $request)
    {
        $search = new Search($request);
        $search->buildAndReturnMainQuery();
        $search->addCompleteFilterQuery();
        $search->addTimeFilterQuery();
        $search->addSavedSearchToRequest();
        $search->addStepStatus();
        $query = $search->getQuery();
        $statsObj = new TaskStats();
        $searchStats = $statsObj->getSearchStats($query);
        $data['stats'] = $searchStats;

        return $data;
    }

    //
    public function getFilteredSystemsByArea(Request $request)
    {
        $systems = System::whereNotIn('tag', array('N/A'))->get();
        $data = $_GET['data'];
        if (isset($_GET['data']) && $data !== '') {
            $isArray = is_array($data);
            if ($isArray) {
                $systems = System::whereIn('area_id', $data)->orderBy('tag', 'ASC')->whereNotIn('tag', array('N/A'))->get();
            } else {
                $systems = System::where('area_id', $data)->orderBy('tag', 'ASC')->whereNotIn('tag', array('N/A'))->get();
            }
        }

        return $systems;
    }

    public function getFilteredGroupsByArea()
    {
        $groups = Group::get();
        $data = $_GET['data'];

        if ($data !== '') {
            $arrayIds = [];
            for ($i = 0; $i < sizeof($data); $i++) {
                $arrayIds[$i] = (int)$data[$i];
            }
            $groupIds = Task::whereIn('area_id', $arrayIds)
                ->groupBy('group_id')
                ->lists('group_id');
            $groups = Group::whereIn('id', $groupIds)->orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        }

        return $groups;
    }

    public function getFilteredTypesByGroup()
    {
        $types = TaskType::get();
        $data = $_GET['data'];
        $isArray = is_array($data);
        if ($isArray) {
            if (isset($_GET['data']) && $data !== '') {
                $types = TaskType::whereIn('group_id', $data)->orderBy('name', 'ASC')->get();
            }
        } else {
            $types = TaskType::where('group_id', $data)->orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        }
        if ($data === '') {
            $types = TaskType::whereNotIn('name', array('N/A'))->orderBy('name', 'ASC')->get();
        }

        return $types;
    }


    public function getFilteredSubTypesByType()
    {
        $data = $_GET['data'];
        if ($data !== '') {
            $testSpecs = TaskSubType::where('task_type_id', (int)$data)->orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        } else {
            $testSpecs = TaskSubType::whereNotIn('name', array('N/A'))->orderBy('name', 'ASC')->get();
        }

        return $testSpecs;
    }

    public function getNumberForAutoComplete($tag)
    {
        $searchNumber = '%' . $tag . '%';
        $searchText = $tag;
        $pieces = explode(" ", $searchText);

        $tasks = Task::take(100)->where('number', 'like', $searchNumber)->orWhere('description', 'like', $searchNumber)->orWhere('schedule_number', 'like', $searchNumber)->get();

/*        $tasks = Task::query();
        $tasks->where('number', 'LIKE', '%'.trim($pieces[0]).'%');
        for ($i = 1; $i < sizeof($pieces); $i++) {
            $tasks->where('number', 'LIKE', '%'.$pieces[$i].'%');
        }
        $tasks->orWhere('description', 'LIKE', '%' . trim($pieces[0]) . '%');
        for ($i = 1; $i < sizeof($pieces); $i++) {
            if (trim($pieces[$i]) !== '') {
                $tasks->where('description', 'LIKE', '%' . trim($pieces[$i]) . '%');
            }
        }
        //$tasks = $tasks->take(200)->toSql();
        $tasks = $tasks->take(200)->orderBy('number')->get();*/

        $results = array();
        foreach ($tasks as $task) {
            $detailText =  $task->description;
            $results[] = ['id' => $task->id, 'value' => $detailText];
        }

        return $results;
    }

    public function getScheduleNumberForAutoComplete($tag)
    {
        $searchText = $tag;
        $pieces = explode(" ", $searchText);

        $dates = ScheduleDate::query();
        $dates->where('number', 'LIKE', '%'.trim($pieces[0]).'%');
        for ($i = 1; $i < sizeof($pieces); $i++) {
            $dates->where('number', 'LIKE', '%'.$pieces[$i].'%');
        }
        $dates->orWhere('description', 'LIKE', '%' . trim($pieces[0]) . '%');
        for ($i = 1; $i < sizeof($pieces); $i++) {
            if (trim($pieces[$i]) !== '') {
                $dates->where('description', 'LIKE', '%' . trim($pieces[$i]) . '%');
            }

        }
        //$dates = $dates->take(200)->toSql();
        $dates = $dates->take(200)->orderBy('description')->get();
        //return $dates;

        $results = array();
        foreach ($dates as $date) {
            $results[] = ['id' => $date->id, 'value' => $date->number . ', ' . $date->description .' (Start '.$date->start_date->format('d-m-Y').') (Finish ' .$date->finish_date->format('d-m-Y').')'];
        }

        return $results;
    }


}

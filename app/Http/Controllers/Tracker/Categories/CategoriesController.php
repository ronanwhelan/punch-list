<?php

namespace App\Http\Controllers\Tracker\Categories;

use App\Jobs\CompileTasksAfterModelDetailsUpdated;
use App\Models\Group;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskRule;
use App\Models\TaskStepPhase;
use App\Models\TaskSubType;
use App\Models\TaskType;
use App\Models\TaskTypeStepPhase;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use App\Rowsys\Tracker\Classes\Compile;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
        $groups = Group::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $taskTypes = TaskType::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $taskTestSpecs = TaskSubType::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();

        // $tasks = Task::with('area', 'group', 'system', 'taskType', 'stage')->orderBy('number', 'ASC')->take(1000)->get();
        return view('rowsys.tracker.tasks.categories.create.index', [
            'groups' => $groups,
            'taskTypes' => $taskTypes,
            'taskTestSpecs' => $taskTestSpecs,
        ]);
    }

    /**
     * ---------------------------------
     *         GROUPS
     *  ---------------------------------
     */
    public function showGroupView()
    {
        $groups = Group::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();

        return view('rowsys.tracker.tasks.categories.groups.create.index', [
            'groups' => $groups,
        ]);
    }

    public function addGroup(Request $request)
    {
        $this->validate($request, [
            'groupName' => 'required|unique:groups,name',//
            'groupDescription' => 'required',
        ]);
        $exists = Group::where('name', $request->groupName)->exists();
        if (!$exists) {
            $group = Group::firstOrNew(['name' => $request->groupName, 'short_name' => $request->shortName, 'description' => $request->groupDescription, 'project_id' => 1]);
            $group->save();
            $message = ' Task Group Added';
        } else {
            $message = ' Task Group Already exists';
        }

        return \Redirect::back()->with('message', $message)->withInput();

    }

    // UPDATE
    public function getGroupDetails($group_id)
    {
        $group = Group::find($group_id);
        $taskCount = Task::where('group_id', $group_id)->count();
        $typeCount = TaskType::where('group_id', $group_id)->count();

        $stepPhaseCount = TaskTypeStepPhase::where('p1_owner_id',$group_id)
            ->orWhere('p2_owner_id',$group_id)
            ->orWhere('p3_owner_id',$group_id)
            ->orWhere('p4_owner_id',$group_id)
            ->orWhere('p5_owner_id',$group_id)
            ->orWhere('p6_owner_id',$group_id)
            ->orWhere('p7_owner_id',$group_id)
            ->orWhere('p8_owner_id',$group_id)
            ->orWhere('p9_owner_id',$group_id)
            ->count();

        return view('rowsys.tracker.tasks.categories.groups.update.details', [
            'group' => $group,
            'taskCount' => $taskCount,
            'typeCount' => $typeCount,
            'stepPhaseCount' => $stepPhaseCount,

        ]);
    }

    public function updateGroupDetails(Request $request, $id)
    {
        $this->validate($request, [
            'update_name' => 'required',
            'update_description' => 'required',
        ]);
        $group = Group::find($id);
        $group->name = $request->update_name;
        $group->short_name = $request->short_name;
        $group->description = $request->update_description;
        $group->save();
        $message = ' Group Updated';

        return $message;
    }

    public function deleteGroup($id)
    {
        Group::destroy($id);

        return " Owner Deleted";

    }


    /**
     * ---------------------------------
     *         TYPES
     *  ---------------------------------
     */
    public function showTypeView()
    {
        $taskTypes = TaskType::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $groups = Group::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();

        return view('rowsys.tracker.tasks.categories.types.create.index', [
            'taskTypes' => $taskTypes,
            'groups' => $groups,
        ]);
    }

    public function addType(Request $request)
    {
        $this->validate($request, [
            'typeName' => 'required|unique:task_types,name',//
            'typeShortName' => 'required|unique:task_types,short_name',
            'typeDescription' => 'required',
            'typeGroup' => 'required',
        ]);

        $type = TaskType::firstOrNew([
            'name' => $request->typeName,
            'short_name' => $request->typeShortName,
            'description' => $request->typeDescription,
            'group_id' => $request->typeGroup,
            'project_id' => 1
        ]);
        $type->save();

        $addRule = $request->addRule;
        $stage = (int)$request->typeStage;
        $message = ' Task Type Added';
        if ($addRule) {
            $stages = Stage::get();
            if ($stage === 3) {
                foreach ($stages as $stage) {
                    $this->addTaskRule($stage->id, $type);
                }
            } elseif ($stage === 1) {
                $this->addTaskRule($stages[0]->id, $type);
            } else {
                $this->addTaskRule($stages[1]->id, $type);
            }
            $message = ' Task Type and Rule Added';
        }

        return \Redirect::back()->with('message', $message)->withInput();

    }

    private function addTaskRule($stage_id, TaskType $taskType)
    {
        $taskRule = new TaskRule();
        $taskRule->project_id = 1;
        $taskRule->task_group_id = $taskType->group_id;
        $taskRule->task_type_id = $taskType->id;
        $taskRule->stage_id = $stage_id;
        $taskRule->gen_ex_buffer_days = -50;
        $taskRule->gen_ex_applicable = 1;
        $taskRule->gen_ex_hrs_perc = 50;
        $taskRule->rev_buffer_days = -30;
        $taskRule->rev_applicable = 1;
        $taskRule->rev_hrs_perc = 30;
        $taskRule->re_issu_buffer_days = -20;
        $taskRule->re_issu_applicable = 1;
        $taskRule->re_issu_hrs_perc = 20;
        $taskRule->s_off_buffer_days = 0;
        $taskRule->s_off_applicable = 1;
        $taskRule->s_off_hrs_perc = 0;
        $taskRule->default_hrs = 40;
        $taskRule->save();
    }

    // UPDATE
    public function getTypeDetails($type_id)
    {
        $type = TaskType::find($type_id);
        $groups = Group::orderBy('name', 'ASC')->get();
        $taskCount = Task::where('task_type_id', $type_id)->count();
        $subCount = TaskSubType::where('task_type_id', $type_id)->count();
        $ruleCount = TaskRule::where('task_type_id', $type_id)->count();
        $rules = TaskRule::where('task_type_id', $type_id)->get();
        $stages = Stage::get();
        return view('rowsys.tracker.tasks.categories.types.update.details', [
            'type' => $type,
            'groups' => $groups,
            'taskCount' => $taskCount,
            'subCount' => $subCount,
            'ruleCount' => $ruleCount,
            'rules' => $rules,
            'stages' => $stages,
        ]);
    }

    public function updateTypeDetails(Request $request, $id)
    {
        $this->validate($request, [
            //'system' => 'required',
            'update_name' => 'required',
            'update_short_name' => 'required',
            'update_description' => 'required',
        ]);
        $type = TaskType::find($id);
        $type->name = $request->update_name;
        $updatedTaskCount = 0;
        if ($type->short_name !== $request->update_short_name) {
            $type->short_name = $request->update_short_name;
        }
        $type->description = $request->update_description;

        if ($type->group_id !== $request->update_parent) {
            $type->group_id = $request->update_parent;

            $taskRules = TaskRule::where('task_type_id', $id)->count();
            if($taskRules > 0){
                TaskRule::where('task_type_id', $id)->update(['task_group_id' => (int)$request->update_parent]);
            }
            Task::where('task_type_id', $id)->update(['group_id' => (int)$request->update_parent]);

        }
        $type->save();
        //TODO update all related tasks number, description
        //$applyRule = new ApplyRuleToTasks();$tasks = Task::where('task_type_id', $id)->get();foreach ($tasks as $task) {$applyRule->compileThisTask($task);}$updated = 56;

        $compile = new CompileTasksAfterModelDetailsUpdated('task_type_id', $id);
        $this->dispatch($compile);


        $message = 'Category Updated';

        return $message;
    }

    public function deleteType($id)
    {

        $rulesIds = TaskRule::where('task_type_id', $id)->lists('id')->toArray();
        if (count($rulesIds) > 0) {

            TaskRule::destroy($rulesIds);
        }

        TaskType::destroy($id);

        $taskStepIdsToDelete = TaskTypeStepPhase::where('task_type_id', $id)->lists('id');
        TaskStepPhase::whereIn('task_type_step_id', $taskStepIdsToDelete)->delete();

        TaskTypeStepPhase::where('task_type_id', $id)->delete();

        return "Category and its Rules Deleted";

    }

    /**
     * ---------------------------------
     *         SUB CATEGORIES
     *  ---------------------------------
     */
    public function showSubTypeView()
    {
        $taskTypes = TaskType::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $taskSubTypes = TaskSubType::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();


        return view('rowsys.tracker.tasks.categories.sub_types.create.index', [
            'taskTypes' => $taskTypes,
            'taskSubTypes' => $taskSubTypes,
        ]);
    }

    public function addSubType(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:task_sub_types,name',//
            'description' => 'required',
            'type' => 'required',
        ]);
        $type = TaskSubType::firstOrNew([
            'name' => $request->name,
            'description' => $request->description,
            'task_type_id' => $request->type,
        ]);
        $type->save();
        $message = ' Sub Type Added';

        return \Redirect::back()->with('message', $message)->withInput();
    }

    // UPDATE
    public function getSubTypeDetails($sub_type_id)
    {
        $sub_type = TaskSubType::find($sub_type_id);
        $types = TaskType::orderBy('name', 'ASC')->get();
        $taskCount = Task::where("number", "LIKE", "%." . $sub_type->name . "%")->count();

        return view('rowsys.tracker.tasks.categories.sub_types.update.details', [
            'sub_type' => $sub_type,
            'types' => $types,
            'taskCount' => $taskCount,
        ]);
    }

    public function updateSubTypeDetails(Request $request, $id)
    {
        $this->validate($request, [
            'update_name' => 'required',
            'update_description' => 'required',
        ]);
        $message = 'Details updated.';
        $sub_type = TaskSubType::find($id);

        if ($sub_type->task_type_id !== $request->update_parent) {
            Task::where('task_sub_type_id', $id)->update(['task_type_id' => (int)$request->update_parent]);
            $sub_type->task_type_id = $request->update_parent;
        }

        $sub_type->name = $request->update_name;
        $sub_type->description = $request->update_description;

        $sub_type->save();

        $compile = new Compile();
        $updated = $compile->compileAffectedTasksAfterModelIsUpdate('task_sub_type_id', $id);
        $message = $message . ' and ' . $updated . ' Tasks Updated';

        return $message;
    }

    public function deleteSubType($id)
    {
        TaskSubType::destroy($id);
        return " Sub Category Deleted";

    }
}

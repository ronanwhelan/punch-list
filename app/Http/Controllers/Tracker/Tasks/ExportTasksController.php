<?php

namespace App\Http\Controllers\Tracker\Tasks;


use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use League\Csv\Writer;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class ExportTasksController extends Controller
{
    public function exportTasksToCsv(){

        $tasks = Task::select(\DB::raw('
                    number as task_number,
                    description as task_description,
                    add_on,
                    add_on_desc,
                    status,
                    complete,
                    target_val,
                    earned_val,
                    (select name from stages where id  = stage_id) as stage,
                    (select name from areas where id  = area_id) as area,
                    (select tag from systems where id  = system_id) as system,
                    (select name from groups where id  = group_id) as task_group,
                    (select name from task_types where id  = task_type_id) as category,
                    (select name from task_sub_types where id  = task_sub_type_id) as sub_category,
                    (select name from roles where id  = group_owner_id) as owner,
                    
                    schedule_number,
                    schedule_date_choice as date_choice,
                    schedule_lag_days as lag,
                    
                    next_td as next_target_date,
                    last_td as last_target_date,
                    
                    gen_perc as Step_1_Perc,
                    gen_td as Step_1_Target_Date,
                    gen_hrs as Step_1_Earned_Hrs,
                    gen_complete_date as Step_1_Complete_Date,
                    gen_applicable as Step_1_Applicable,
                   
                    rev_perc as Step_2_Perc,
                    rev_td as Step_2_Target_Date,
                    rev_hrs as Step_2_Earned_Hrs,
                    rev_complete_date as Step_2_Complete_Date,
                    rev_applicable as Step_2_Applicable,
                    
                    re_issu_perc as Step_3_Perc,
                    re_issu_td as Step_3_Target_Date,
                    re_issu_hrs as Step_3_Earned_Hrs,
                    re_issu_complete_date as Step_3_Complete_Date,
                    re_issu_applicable as Step_3_Applicable,
                    
                    s_off_perc as Step_4_Perc,
                    s_off_td as Step_4_Target_Date,
                    s_off_hrs as Step_4_Earned_Hrs,
                    s_off_complete_date as Step_4_Complete_Date,
                    s_off_applicable as Step_4_Applicable,
                    
                    base_date
                    
                    

                    '))
            //->where('status', '<>', 1)
            // ->groupBy('status')
            ->get();

        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());

       // $csv->insertOne(\Schema::getColumnListing('users'));

        $csv->insertOne([
            'number', 'description','add_on','add_on_desc',
            'status', 'complete', 'target_val', 'earned_val',
            'stage',
            'area','system',
            'task_group', 'category','sub_category',
            'owner','schedule Link','Date Choice','Lead/lag',
            'Next Target Date','Last Target Date',
            'Step 1 %','Step 1 Target Date','Step 1 Hours','Step 1 Complete Date','Step 1 Applicable',
            'Step 2 %','Step 2 Target Date','Step 2 Hours','Step 2 Complete Date','Step 2 Applicable',
            'Step 3 %','Step 3 Target Date','Step 3 Hours','Step 3 Complete Date','Step 3 Applicable',
            'Step 4 %','Step 4 Target Date','Step 4 Hours','Step 4 Complete Date','Step 4 Applicable',
            'Base Date'
        ]);

        foreach ($tasks as $task) {
            $csv->insertOne($task->toArray());
        }

        $fileName = 'tasks-'.Carbon::today()->toDateString().'.csv';

        $csv->output($fileName);


    }
}

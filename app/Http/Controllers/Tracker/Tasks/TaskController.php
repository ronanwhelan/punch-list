<?php

namespace App\Http\Controllers\Tracker\Tasks;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Task;
use App\Models\TaskEarnedValue;
use App\Models\TaskScheduleLink;
use App\Models\TaskStepPhase;
use App\Models\TaskSubType;
use App\Models\TaskTestSpec;
use App\Models\TaskTypeStepPhase;
use App\Models\User;
use App\Models\UserFilter;
use App\Rowsys\Tracker\Classes\Search;
use App\Rowsys\Tracker\Classes\TaskStats;
use App\Rowsys\Tracker\Classes\UpdateTaskEarnedValues;
use Illuminate\Database\Schema\Builder;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\System;
use App\Models\Group;
use App\Models\TaskType;
use App\Models\TaskRule;
use App\Models\Stage;
use App\Models\TaskColor;
use App\Models\ScheduleDate;
use App\Models\TaskExtension;
use App\Rowsys\Tracker\Classes\Compile;
use App\Rowsys\Tracker\Classes\ApplyRuleToTasks;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\Array_;
use session;
use DateTime;
use Mail;


/**
 * Class TaskController
 * @package App\Http\Controllers\Tracker\Tasks
 */
class TaskController extends Controller
{
    /**
     * Display the Task View Index Page - table View
     * change on master to test
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //--------------------------------------
        // Last Search
        //--------------------------------------
        $search = new Search($request);
        $search->saveLastSearch();
        $returnAllResults = $request->has('chk_return_all_results');
        //Search NOT active i.e Not coming from search modal
        //OFF - Search is OFF
        if (!$request->has('search_active')) {
            $search->buildAndReturnMainQuery();//if(!$request->has('flagged_tasks') && !$request->has('my_tasks') && !$request->has('my_flagged_tasks') && !$request->has('my_teams_tasks')){}
            $search->addCompleteFilterQuery();
            $search->checkForAnyAppLinks();//Check for any App links e.g like coming from the dashboard
            $query = $search->getQuery();
            //if ($this->request->has('my_flagged_tasks')) {
            if (!$request->has('flagged_tasks') && !$request->has('my_flagged_tasks')) {
                $query->where('complete', 0);
            }
            $query->orderBy('next_td', 'ASC');
            if ($returnAllResults && $request->chk_return_all_results === 'on') {
                $query->take(1500);
            } else {
                $query->with(
                    'area',
                    'group',
                    'system',
                    'taskType',
                    'stage',
                    'scheduleNumber',
                    'taskSubType',
                    'extension',
                    'extension.whoUpdatedLast',
                    'assignedUser',
                    'assignedRole',
                    'groupOwner'
                )->take(250);
            }
            $tasks = $query->get();

        } else {
            //Search Is ON
            $search->buildAndReturnMainQuery();
            $search->addCompleteFilterQuery();
            $search->addTimeFilterQuery();
            $search->addSavedSearchToRequest();
            $search->addOrderBy();
            $search->addStepStatus();
            $query = $search->getQuery();
            //return $search->getQueryToSql();
            $query->with('area', 'group', 'system', 'taskType', 'stage', 'scheduleNumber', 'taskSubType', 'extension', 'extension.whoUpdatedLast', 'assignedUser', 'assignedRole', 'groupOwner');
            if ($returnAllResults && $request->chk_return_all_results === 'on') {
                $query->take(1500);
            } else {
                $query->take(250);
            }
            $tasks = $query->get();

        }

        //Stats
        $phaseStepPhaseStats = [];
        $statsObj = new TaskStats();
        $stats = [];
        $statQuery = clone $query;//to get stats
        $stats = $statsObj->getSearchStats($statQuery);
        $phaseQuery = clone $statQuery;

        //$phaseStepPhaseStats = $statsObj->getStepPhaseStats($phaseQuery);

        //TaskIds for Step phase Stats
        //$phaseOwnerStatsQuery = clone $query;
        // $listOfTaskIds = $phaseOwnerStatsQuery->get()->lists('id');

        $view = 'rowsys.tracker.tasks.read.tables.index1';
        if ($request->goToTaskParametersView === '1') {
            $view = 'rowsys.tracker.tasks.parameters.index';
        }

        return view($view, [
            'tasks' => $tasks,
            'stats' => $stats,
            'phaseStepPhaseStats' => $phaseStepPhaseStats,
            //'listOfTaskIds' => $listOfTaskIds,
        ]);
    }

    /**
     * ---------------------------------
     *       Task Parameters
     *  ---------------------------------
     ** @return Response
     * Returns the view to update the Task Target Value and Lag
     */
    public function getParametersView(Request $request)
    {
        $search = new Search($request);
        $returnAllResults = $request->has('chk_return_all_results');
        //Search NOT active i.e Not coming from search modal
        if (!$request->has('search_active')) {
            $search->buildAndReturnMainQuery();
            $search->checkForAnyAppLinks();//Check for any App links e.g like coming from the dashboard
            $query = $search->getQuery();
            $statQuery = clone $query;//to get stats
            $query->orderBy('next_td', 'ASC')
                ->where('complete', 0)
                ->with('area', 'group', 'system', 'taskType', 'stage', 'scheduleNumber', 'taskSubType');
            if ($returnAllResults && $request->chk_return_all_results === 'on') {
                $query->take(2000);
            } else {
                $query->take(250);
            }

            $tasks = $query->get();


        } else {
            $search->buildAndReturnMainQuery();
            $search->addCompleteFilterQuery();
            $search->addTimeFilterQuery();
            $query = $search->getQuery();
            $statQuery = clone $query;
            $query->orderBy('next_td', 'ASC')
                ->where('complete', 0)
                ->with('area', 'group', 'system', 'taskType', 'stage', 'scheduleNumber', 'taskSubType');
            if ($returnAllResults && $request->chk_return_all_results === 'on') {
                $query->take(2000);
            } else {
                $query->take(250);
            }

            $tasks = $query->get();
        }

        $statsObj = new TaskStats();
        $stats = $statsObj->getSearchStats($statQuery);;

        return view('rowsys.tracker.tasks.parameters.index', [
            'tasks' => $tasks,
        ]);
    }


    /**
     * ---------------------------------
     *          Task Table
     *  ---------------------------------
     *
     *    Returns task table built by server
     */
    public function taskTable()
    {

        return "im here";
        return view('rowsys.tracker.tasks.read.tables.server_table1', [
            // 'tasks' => $tasks,
        ]);

    }


    /**
     * ---------------------------------
     *          Task List View
     *  ---------------------------------
     *
     *    Shows the Tasks in a List View
     */
    public function getTaskListView()
    {
        // $project_id = $request->session()->get('project_id');
        //todo get the project id
        $project_id = 1;
        $tasks = Task::with('area', 'group', 'system', 'taskType', 'stage')
            ->orderBy('number', 'ASC')
            ->take(1000)
            ->get();

        return view('rowsys.tracker.tasks.read.list.index', [
            'tasks' => $tasks,
        ]);
    }

    /**
     * ---------------------------------
     *          CARD VIEW
     *  ---------------------------------
     *
     *    Shows the Tasks in a List View
     */
    public function getTaskCardView(Request $request)
    {

        $search = new Search($request);
        //Search NOT active i.e Not coming from search modal
        if (!$request->has('search_active')) {
            $search->addUserFilterToRequest();
            $search->buildAndReturnMainQuery();
            $search->checkForAnyAppLinks();//Check for any App links e.g like coming from the dashboard
            $query = $search->getQuery();
            $statQuery = clone $query;//to get stats
            $tasks = $query->orderBy('next_td', 'ASC')
                ->take(250)
                ->with('area', 'group', 'system', 'taskType', 'stage')
                ->get();
        } else {
            $search->buildAndReturnMainQuery();
            $search->addCompleteFilterQuery();
            $search->addTimeFilterQuery();
            $query = $search->getQuery();
            $statQuery = clone $query;
            $tasks = $query->orderBy('next_td', 'ASC')
                ->take(250)
                ->with('area', 'group', 'system', 'taskType', 'stage')
                ->get();
        }

        $statsObj = new TaskStats();
        $stats = $statsObj->getSearchStats($statQuery);;

        return view('rowsys.tracker.tasks.read.cards.index', [
            'tasks' => $tasks,
            'stats' => $stats,
        ]);

    }

    /**
     * ---------------------------------
     *     Task List Built By Server
     *  ---------------------------------
     *
     *    Shows the Tasks in a List View
     */
    public function getTaskTableBuiltByServer()
    {
        // $project_id = $request->session()->get('project_id');
        //todo get the project id
        $project_id = 1;
        $tasks = Task::with('area', 'group', 'system', 'taskType', 'stage')
            ->orderBy('number', 'ASC')
            ->take(100)
            ->get();

        return view('rowsys.tracker.tasks.read.tables.index', [
            'tasks' => $tasks,
        ]);
    }

    /**
     * @param $startId
     * @return \Illuminate\Http\JsonResponse
     */
    public function tasksInJsonFormat($startId)
    {
        // = json_encode(Task::with('system','genColor')->get());

        //find the tasks where the id is greater tan the number provided and take 200 - accend by number
        //This can be used to send back chunks of a full search and populate the datatable for each chunk
        //rather than getting and processing all tasks at once  - each chunk is appended to the table.
        $tasks = Task::with('area', 'group', 'system', 'taskType', 'stage')
            ->where('id', '>', $startId)
            ->orderBy('next_td', 'ASC')
            ->take(250)
            ->get();

        //$tasks = Task::take(1000)->get();
        return \Response::json($tasks);
        //return Response::json($tasks]);
    }

    /**
     * ---------------------------------
     *          Show Add Task View
     *  ---------------------------------
     *
     *    Shows the Add Task View
     */
    public function showAddTaskView()
    {
        $project_id = Session::get('project_id');
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $systems = System::orderBy('tag', 'ASC')->whereNotIn('tag', array('N/A'))->get();
        $groups = Group::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $taskTypes = TaskType::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $stages = Stage::all();
        $taskSubTypes = TaskSubType::whereNotIn('name', array('N/A'))->get();
        $today = Carbon::now();
        $todaysDate = $today->toDateString();
        $scheduleDates = ScheduleDate::orderBy('number', 'ASC')->get();

        return view('rowsys.tracker.tasks.create.index', [
            'areas' => $areas,
            'systems' => $systems,
            'groups' => $groups,
            'taskTypes' => $taskTypes,
            'stages' => $stages,
            'taskSubTypes' => $taskSubTypes,
            'todaysDate' => $todaysDate,
            'scheduleDates' => $scheduleDates,
        ]);
    }

    /**
     * ---------------------------------
     *          Add New Task
     * ---------------------------------
     *
     * Add New Task  - through POST in javascript file
     */

    public function addTask(Request $request)
    {

        $this->validate($request, [
            'taskType' => 'required',
            'stage' => 'required',
            'assignedHours' => 'required|integer|min:1',
            'scheduleNumber' => 'required',
            'scheduleDateChoice' => 'required',
            'scheduleDateLag' => 'required|integer',
            'startDate' => 'required',
            'finishDate' => 'required',
            'taskNumber' => 'required',//',
            'taskDescription' => 'required',
            'responsible_team' => 'required|integer|min:1'
        ]);

        $message = '';

        // Every TaskType Must have a Task Rule
        $taskType = TaskType::find((int)$request->taskType);

        //Bit to ensure a task Type Rule Exist
        //If not report back to user
        $taskTypeRuleExists = false;
        $taskTypeRuleCount = TaskRule::where('task_type_id', $taskType->id)
            ->where('stage_id', $request->stage)
            ->count();

        if ($taskTypeRuleCount > 0) {
            $taskTypeRuleExists = true;
        }

        if ($taskTypeRuleExists) {
            // --- Project ----
            //$project_id = Session::get('project_id');
            $project_id = 1;

            // --- Area AND System----
            //id system count > 0 if so then there is at least one system selected
            //if count is 0 then there is no system.
            $systems = '';
            $hasSystems = false;
            $increment = 0;
            $loopSize = 1;
            $systemCount = sizeof(($request->system));
            if ($systemCount > 0) {
                $loopSize = $systemCount;
                $systems = $request->system;
                $hasSystems = true;
            }

            // --- Sub Type---
            $subType = 1;
            if ((int)$request->taskSubType !== 0) {
                $subType = (int)$request->taskSubType;
            }

            //------------------------
            //  create a new task for each System
            //------------------------
            for ($i = 0; $i < $loopSize; $i++) {
                $task = new Task;

                //Todo add project number
                $task->project_id = $project_id;

                //Values that come from the Form
                $task->number = $request->taskNumber;
                $task->description = $request->taskDescription;

                $task->add_on = $request->numberAddOn;
                $task->add_on_desc = $request->numberAddOnDescription;


                // Area and Systems
                if ($hasSystems) {
                    $areaId = System::find($systems[$i])->area_id;
                    $task->area_id = (int)$areaId;
                    $task->system_id = (int)$systems[$i];
                } else {
                    $task->area_id = 1;
                    $task->system_id = 1;
                }

                $task->group_id = (int)$taskType->group_id;
                $task->task_type_id = (int)$taskType->id;
                $task->task_sub_type_id = $subType;

                $task->stage_id = (int)$request->stage;

                $task->gen_applicable = 1;
                $task->gen_td = Carbon::now();
                $task->gen_perc = 0;
                $task->gen_hrs = 0;

                $task->rev_applicable = 1;
                $task->rev_td = Carbon::now();
                $task->rev_perc = 0;
                $task->rev_hrs = 0;

                $task->re_issu_applicable = 1;
                $task->re_issu_td = Carbon::now();
                $task->re_issu_perc = 0;
                $task->re_issu_hrs = 0;

                $task->s_off_applicable = 1;
                $task->s_off_td = Carbon::now();
                $task->s_off_perc = 0;
                $task->s_off_hrs = 0;

                //Assigned Hours
                $task->target_val = $request->assignedHours;
                $task->earned_val = 0;

                //Owner
                $task->group_owner_id = $request->responsible_team;

                //Document Number
                $task->document_number = $request->documentNumber;

                //Automation
                //$task->auto_id = 1;

                //---- Calculate the Target and Base Date -----
                $targetDateChoice = $request->startDate;
                if ((int)$request->scheduleDateChoice === 1) {
                    $targetDateChoice = $request->finishDate;
                }
                $task->prim_date = $targetDateChoice;
                $task->base_date = $targetDateChoice;

                //Schedule Link
                $schedule_number = ScheduleDate::find($request->scheduleNumber)->number;
                $task->schedule_number = $schedule_number;
                $task->schedule_date_choice = $request->scheduleDateChoice;
                $task->schedule_start_date = $request->startDate;
                $task->schedule_finish_date = $request->finishDate;
                $task->schedule_lag_days = (int)$request->scheduleDateLag;


                //Next Target Date
                $task->next_td = Carbon::now();//$task->next_td = new \DateTime('2016-05-09');

                //Last Target Date
                $task->last_td = Carbon::now();//$task->next_td = new \DateTime('2016-05-09');

                //Projected Data - Target Completion Dat
                $task->projected_date = Carbon::now();

                //Projected Data - Target Completion Date
                $task->revised_projected_date = Carbon::create(2016, 1, 1, 0, 0, 0);


                $task->base_dev_days = 0;

                //Value that are set to default values
                $task->complete = 0;
                $task->complete_date = Carbon::create(2000, 1, 1, 0, 0, 0);

                //Compile Error
                $task->compile_error = 1;

                $task->flag = 0;

                //------------------------
                //   save the new task
                //------------------------
                //todo is this needed? as the applying the rule saves the task?
                $task->save();


                //---------------------------
                //   Add Task Extension
                //---------------------------

                $taskExtension = new TaskExtension;

                $taskExtension->task_id = $task->id;
                $taskExtension->gen_complete_date = Carbon::create(2000, 1, 1, 0, 0, 0);
                $taskExtension->gen_completed_by_id = 0;
                $taskExtension->rev_complete_date = Carbon::create(2000, 1, 1, 0, 0, 0);
                $taskExtension->rev_completed_by_id = 0;
                $taskExtension->re_issu_complete_date = Carbon::create(2000, 1, 1, 0, 0, 0);
                $taskExtension->re_issu_completed_by_id = 0;
                $taskExtension->s_off_complete_date = Carbon::create(2000, 1, 1, 0, 0, 0);
                $taskExtension->s_off_completed_by_id = 0;
                $taskExtension->error_code = $task->compile_error;
                $taskExtension->doc_id = 0;
                $taskExtension->who_updated_last_id = 1;
                $taskExtension->who_completed_id = 0;
                $taskExtension->who_created_id = 0;
                $taskExtension->note = $request->note;
                $taskExtension->push();


                //---------------------------
                //   Task Step Phases
                //---------------------------
                for ($j = 1; $j < 5; $j++) {
                    $stepExists = TaskTypeStepPhase::where('task_type_id', $taskType->id)
                        ->where('stage_id', (int)$request->stage)
                        ->where('step_number', $j)
                        ->exists();
                    if ($stepExists) {
                        $taskTypeStepPhase = TaskTypeStepPhase::where('task_type_id', $taskType->id)
                            ->where('stage_id', (int)$request->stage)
                            ->where('step_number', $j)
                            ->first();

                        $step = 'step_' . $j . '_owner_id';
                        $task->$step = $taskTypeStepPhase->p1_owner_id;
                        $task->save();

                        //---------------------------
                        //   New Task Step Phases
                        //---------------------------
                        $taskStepPhase = new TaskStepPhase();
                        $taskStepPhase->task_id = $task->id;

                        $taskStepPhase->task_type_step_id = $taskTypeStepPhase->id;

                        $taskStepPhase->step_number = $taskTypeStepPhase->step_number;

                        $taskStepPhase->phase_count = $taskTypeStepPhase->phase_count;

                        $taskStepPhase->break_down_count = $taskTypeStepPhase->break_down_count;

                        $taskStepPhase->current_owner_id = $taskTypeStepPhase->p1_owner_id;

                        $taskStepPhase->p1_owner_id = $taskTypeStepPhase->p1_owner_id;
                        $taskStepPhase->p2_owner_id = $taskTypeStepPhase->p2_owner_id;
                        $taskStepPhase->p3_owner_id = $taskTypeStepPhase->p3_owner_id;
                        $taskStepPhase->p4_owner_id = $taskTypeStepPhase->p4_owner_id;
                        $taskStepPhase->p5_owner_id = $taskTypeStepPhase->p5_owner_id;
                        $taskStepPhase->p6_owner_id = $taskTypeStepPhase->p6_owner_id;
                        $taskStepPhase->p7_owner_id = $taskTypeStepPhase->p7_owner_id;
                        $taskStepPhase->p8_owner_id = $taskTypeStepPhase->p8_owner_id;
                        $taskStepPhase->p9_owner_id = $taskTypeStepPhase->p9_owner_id;
                        $taskStepPhase->p10_owner_id = $taskTypeStepPhase->p10_owner_id;

                        $taskStepPhase->save();
                    }
                }

                //---------------------------
                //  Add Task Schedule Link
                //---------------------------
                $scheduleTaskLink = new TaskScheduleLink();
                $scheduleTaskLink->schedule_id = $request->scheduleNumber;
                $scheduleTaskLink->task_id = $task->id;
                $scheduleTaskLink->date_choice = $request->scheduleDateChoice;
                $scheduleTaskLink->lag = $request->scheduleDateLag;

                //---- Calculate the Target Date -----
                $targetDateChoice = $request->startDate;
                if ((int)$request->scheduleDateChoice === 1) {
                    $targetDateChoice = $request->finishDate;
                }
                $targetDateWithLag = Carbon::parse($targetDateChoice);
                if ((int)$request->scheduleDateLag > 0) {
                    $days = (int)$request->scheduleDateLag;
                    $targetDateWithLag->addDays($days);
                }
                if ((int)$request->scheduleDateLag < 0) {
                    $days = -(int)$request->scheduleDateLag;
                    $targetDateWithLag->subDays($days);
                }

                $scheduleTaskLink->target_date = $targetDateWithLag;
                $scheduleTaskLink->base_date = $targetDateWithLag;

                $scheduleTaskLink->schedule_start_date = $request->startDate;
                $scheduleTaskLink->schedule_finish_date = $request->finishDate;
                //$scheduleTaskLink->push();

                //---------------------------
                //   Apply Rule to New Task
                //---------------------------
                $task->applyTaskRule();
                $increment++;
                $message = 'Task ' . $task->number . " added";
            }

        } else {
            $message = ' There was an error';
        }
        //------------------------
        //  Return Confirmation Message
        //------------------------
        $data = array(
            'message' => $message,
            'request' => $request,
            'taskTypeRuleExists' => $taskTypeRuleExists,
        );

        return $data;
    }


    /**
     * ---------------------------------
     *    Get Update Task Status Form
     *  ---------------------------------
     *
     * Returns the task details for a selected
     * task to show on a view
     *
     */
    public function getUpdateStatusForm($id)
    {
        $task = Task::with('stage')->find($id);
        $extension = TaskExtension::where('task_id', $id)->first();

        $stepPhase1 = TaskStepPhase::where('task_id', $id)->where('step_number', 1)->first();
        $stepPhase2 = TaskStepPhase::where('task_id', $id)->where('step_number', 2)->first();
        $stepPhase3 = TaskStepPhase::where('task_id', $id)->where('step_number', 3)->first();
        $stepPhase4 = TaskStepPhase::where('task_id', $id)->where('step_number', 4)->first();
        $roles = Role::get();
        $users = User::get();


        return view('rowsys.tracker.tasks.update.update_status_form', [
            'task' => $task,
            'stepPhase1' => $stepPhase1,
            'stepPhase2' => $stepPhase2,
            'stepPhase3' => $stepPhase3,
            'stepPhase4' => $stepPhase4,
            'extension' => $extension,
            'roles' => $roles,
            'users' => $users,

        ]);
    }


    /**
     * ---------------------------------
     *   Update Task Step Status
     *  ---------------------------------
     *
     *  Updates the Task Details - through POST in javascript file
     *  Also updates the Next Target Sate and Earned Value
     *
     */
    public function updateStatus(Request $request, $id)
    {
        $task = Task::find($id);
        $today = Carbon::now();
        $defaultDate = Carbon::createFromFormat('Y-m-d H', '2000-01-01 22');
        $taskRule = TaskRule::where('task_type_id', $task->task_type_id)->where('stage_id', $task->stage_id)->first();

        /* -------------------------------
         *  Step 1
         -------------------------------*/
        //Update Step 1 Status - GEN/EXE
        $step1CurrentPercent = $task->gen_perc;
        if ((int)$request->taskGenStatusUpdate === 100 && (int)$task->gen_perc !== 100) {
            $task->gen_complete_date = $today;
            $task->gen_completed_by_id = \Auth::user()->id;
        }
        if ((int)$request->taskGenStatusUpdate < 100) {
            $task->gen_complete_date = $defaultDate;
        }
        $task->gen_perc = (int)$request->taskGenStatusUpdate;

        //Step 1  - Earned Value
        $step1NewPercent = (int)$request->taskGenStatusUpdate;
        if ($step1CurrentPercent != $step1NewPercent) {
            $this->stepEarnedHrs($task, 1, $step1CurrentPercent, $step1NewPercent, $taskRule->gen_ex_hrs_perc);
        }

        //Step Phase
        if ((int)$request->step1_updated === 1 || true) {
            $stepPhase1 = TaskStepPhase::where('task_id', $id)->where('step_number', 1)->first();
            if (count($stepPhase1) > 0) {
                $stepPhase1->status = (int)$request->taskGenStatusUpdate;
                for ($i = 1; $i <= $stepPhase1->phase_count; $i++) {
                    $taskStepStatus = 'p' . $i . '_status';
                    $stepStatus = 'step1_phase' . $i . '_status';
                    $stepPhase1->$taskStepStatus = $request->$stepStatus;
                }
                $stepPhase1->save();
            }
        }

        /* -------------------------------
         *  Step 2
         -------------------------------*/
        //Update Step 2 Status - REVIEW
        $step2CurrentPercent = $task->rev_perc;
        if ((int)$request->taskRevStatusUpdate === 100 && (int)$task->rev_perc !== 100) {
            $task->rev_complete_date = $today;
            $task->rev_completed_by_id = \Auth::user()->id;
        }
        if ((int)$request->taskRevStatusUpdate < 100) {
            $task->rev_complete_date = $defaultDate;
        }
        $task->rev_perc = (int)$request->taskRevStatusUpdate;

        //Step 2  - Earned Value
        $step2NewPercent = (int)$request->taskRevStatusUpdate;
        if ($step2CurrentPercent != $step2NewPercent) {
            $this->stepEarnedHrs($task, 2, $step2CurrentPercent, $step2NewPercent, $taskRule->rev_hrs_perc);
        }

        //Step Phase
        if ((int)$request->step2_updated === 1 || true) {
            $stepPhase2 = TaskStepPhase::where('task_id', $id)->where('step_number', 2)->first();
            if (count($stepPhase2) > 0) {
                $stepPhase2->status = (int)$request->taskRevStatusUpdate;
                if ($stepPhase2->break_down_count === 0) {
                    for ($i = 1; $i <= $stepPhase2->phase_count; $i++) {
                        $taskStepStatus = 'p' . $i . '_status';
                        $stepStatus = 'step2_phase' . $i . '_status';
                        $stepPhase2->$taskStepStatus = $request->$stepStatus;
                    }
                }
                $stepPhase2->save();
            }
        }

        /* -------------------------------
        *  Step 3
        -------------------------------*/
        //Update Step 3 Status - RE-ISSUE
        $step3CurrentPercent = $task->re_issu_perc;
        if ((int)$request->taskReIssueStatusUpdate === 100 && (int)$task->re_issu_perc !== 100) {
            $task->re_issu_complete_date = $today;
            $task->re_issu_completed_by_id = \Auth::user()->id;
        }
        if ((int)$request->taskReIssueStatusUpdate < 100) {
            $task->re_issu_complete_date = $defaultDate;
        }

        $task->re_issu_perc = (int)$request->taskReIssueStatusUpdate;

        //Step 3  - Earned Value
        $step3NewPercent = (int)$request->taskReIssueStatusUpdate;
        if ($step3CurrentPercent != $step3NewPercent) {
            $this->stepEarnedHrs($task, 3, $step3CurrentPercent, $step3NewPercent, $taskRule->re_issu_hrs_perc);
        }
        //Step Phase
        if ((int)$request->step3_updated === 1 || true) {
            $stepPhase3 = TaskStepPhase::where('task_id', $id)->where('step_number', 3)->first();
            if (count($stepPhase3) > 0) {
                $stepPhase3->status = (int)$request->taskReIssueStatusUpdate;
                for ($i = 1; $i <= $stepPhase3->phase_count; $i++) {
                    $taskStepStatus = 'p' . $i . '_status';
                    $stepStatus = 'step3_phase' . $i . '_status';
                    $stepPhase3->$taskStepStatus = $request->$stepStatus;
                }
                $stepPhase3->save();
            }
        }

        /* -------------------------------
        *  Step 4
       -------------------------------*/
        //Update Step 4 Status - S-OFF
        $step4CurrentPercent = $task->s_off_perc;
        if ((int)$request->taskSignOffStatusUpdate === 100 && (int)$task->s_off_perc !== 100) {
            $task->s_off_complete_date = $today;
            $task->s_off_completed_by_id = \Auth::user()->id;
        }
        if ((int)$request->taskSignOffStatusUpdate < 100) {
            $task->s_off_complete_date = $defaultDate;
        }
        $task->s_off_perc = (int)$request->taskSignOffStatusUpdate;

        //Step 4  - Earned Value
        $step4NewPercent = (int)$request->taskSignOffStatusUpdate;
        if ($step4CurrentPercent != $step4NewPercent) {
            $this->stepEarnedHrs($task, 4, $step4CurrentPercent, $step4NewPercent, $taskRule->s_off_hrs_perc);
        }
        //Step Phase
        if ((int)$request->step4_updated === 1 || true) {
            $stepPhase4 = TaskStepPhase::where('task_id', $id)->where('step_number', 4)->first();
            if (count($stepPhase4) > 0) {
                $stepPhase4->status = (int)$request->taskSignOffStatusUpdate;
                for ($i = 1; $i <= $stepPhase4->phase_count; $i++) {
                    $taskStepStatus = 'p' . $i . '_status';
                    $stepStatus = 'step4_phase' . $i . '_status';
                    $stepPhase4->$taskStepStatus = $request->$stepStatus;
                }
                $stepPhase4->save();
            }
        }

        //$revised_provisioned_date = new Carbon($request->revised_provisioned_date);
        //$task->revised_projected_date = $request->revised_provisioned_date;

        //---------  ADD TO WATCH LIST -------------------
        //Project Flag - Add to Project Watch List - to be watched
        if ($request->flag === 'on') {
            $task->flag = 1;
        } else {
            $task->flag = 0;
        }
        //My Flag - Add to May Watch List - to be watched
        $user = Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        if (count($userFilter) === 0) {
            $userFilter = new UserFilter();
            $userFilter->user_id = $user->id;
        }
        $currentListString = $userFilter->task_flag_list;
        $currentListArray = explode(',', $currentListString);
        if ($request->my_flag === 'on') {
            if ($currentListString === "" || $currentListString === "''") {
                $userFilter->task_flag_list = $task->id;
            } else {
                if (!in_array($task->id, $currentListArray)) {
                    array_push($currentListArray, $task->id);
                    $userFilter->task_flag_list = implode(",", $currentListArray);
                }
            }
        } // Else - Remove from the array i.e unset
        else {
            if (in_array($task->id, $currentListArray)) {
                $key = array_search($task->id, $currentListArray);
                if ($key !== false) {
                    unset($currentListArray[$key]);
                }
                $userFilter->task_flag_list = implode(",", $currentListArray);
            }
        }
        $userFilter->save();

        //------------------------
        //   save the updated task
        //------------------------
        $task->save();

        //---------------------------
        //   Apply Rule to New Task
        //---------------------------
        $task->applyTaskRule();

        //$taskHistoryExi  = TaskExtension::where('task_id',$task->id)->exists();
        $taskExtension = TaskExtension::firstOrNew(['task_id' => $task->id]);
        $taskExtension->task_id = $task->id;
        $taskExtension->gen_complete_date = $task->gen_td;
        $taskExtension->gen_completed_by_id = $task->gen_completed_by_id;
        $taskExtension->rev_complete_date = $task->rev_td;
        $taskExtension->rev_completed_by_id = $task->rev_completed_by_id;
        $taskExtension->re_issu_complete_date = $task->re_issu_td;
        $taskExtension->re_issu_completed_by_id = $task->re_issu_completed_by_id;
        $taskExtension->s_off_complete_date = $task->s_off_td;
        $taskExtension->s_off_completed_by_id = $task->s_off_completed_by_id;
        $taskExtension->who_completed_id = 0;
        $taskExtension->error_code = $task->compile_error;
        $taskExtension->doc_id = 0;
        $taskExtension->who_updated_last_id = \Auth::user()->id;

        $taskExtension->who_created_id = 0;
        //$taskExtension->note = '';
        $taskExtension->save();

        //(new ApplyRuleToTasks())->compileThisTask($task);
        //------------------------
        //  Return Confirmation Message
        //------------------------
        $message = ' ' . $task->number . " Updated";
        $stepOwners = [$task->step_1_owner->name, $task->step_2_owner->name, $task->step_3_owner->name, $task->step_4_owner->name];
        $data = array(
            'message' => $message,
            'task' => $task,
            'stepOwners' => $stepOwners,
            'user_filter' => $currentListArray,
            'rev_perc' => (int)$request->taskRevStatusUpdate
        );

        return $data;
    }

    /**
     *  Create an entry for the Earned value for the task step
     *
     * @param $task
     * @param $stepNumber
     * @param $currentPerc
     * @param $newPercent
     * @param $stepRulePerc
     */
    public function stepEarnedHrs($task, $stepNumber, $currentPerc, $newPercent, $stepRulePerc)
    {

        $today = Carbon::now();

        $percentDifference = $newPercent - $currentPerc;

        $stepTargetHrs = ['gen_target_hrs', 'rev_target_hrs', 're_issu_target_hrs', 's_off_target_hrs'];

        $stepHrsString = $stepTargetHrs[$stepNumber - 1];

        $stepTargetHrs = $task->$stepHrsString;

        //so it is going up or going down?
        if ($percentDifference > 0) {
            //the earned value must be captured
            $earnedValue = round(($stepTargetHrs * $percentDifference) / 100, 1);

        } else {
            $earnedValue = round(($stepTargetHrs * -$percentDifference) / 100, 1);
            $earnedValue = $earnedValue * -1;
        }

        //what happens if the hour changed / or percentage changed after the step was started
        //ther maybe hours that are not caught - so if the task step goes to 100% from the browser,
        //ensure the full hours earned are captured.
        if ($newPercent > 99) {
            $earnedHoursTotal = $earnedValue + $task->gen_hrs;
            if ($stepTargetHrs != $earnedHoursTotal) {
                //then there is a problem - the missing earned hrs should be added to the step?
                //$earnedValue = $earnedValue + ($stepTargetHrs - ($task->gen_hrs - $earnedHoursTotal) );
            }
        }

        $taskEarnedValue = TaskEarnedValue::create([
            'task_id' => $task->id,
            'stage_id' => $task->stage_id,
            'type_id' => $task->task_type_id,
            'step_num' => $stepNumber,
            'group_owner_id' => $task->group_owner_id,
            'begin_perc' => $currentPerc,
            'end_perc' => $newPercent,
            'dif_perc' => $newPercent - $currentPerc,
            'task_target_hrs' => $task->target_val,
            'step_rule_perc' => $stepRulePerc,
            'earned_hrs' => $earnedValue,
            'step_target_hrs' => $stepTargetHrs,
            'created_by_id' => Auth::user()->id,
        ]);
    }

    /**
     * ---------------------------------
     *    Get Task Owner Section
     *  ---------------------------------
     *
     * Returns the task owner details for a selected
     * task to show and update
     *
     */
    public function getUpdateTaskOwner($id)
    {
        $task = Task::find($id);
        if (TaskExtension::where('task_id', $id)->exists()) {
            $extension = TaskExtension::where('task_id', $id)->first();
        } else {
            $extension = TaskExtension::firstOrCreate(['task_id' => $id]);
        }

        $roles = Role::get();
        //$users = User::where('')->get();

        $role = Role::find($task->group_owner_id);
        $users = $role->users;

        return view('rowsys.tracker.tasks.update.role_and_user', [
            'task' => $task,
            'roles' => $roles,
            'users' => $users,
            'extension' => $extension,
        ]);
    }

    /**
     * ---------------------------------
     *   Update Task Role And User
     *  ---------------------------------
     *
     * Returns the task owner details for a selected
     * task to show and update
     *
     */
    public function updateRoleAndUser(Request $request, $id)
    {
        $task = Task::find($id);
        //Assigned Role
        $task->assigned_role_id = 0;
        if ((int)$request->responsible_team > 0) {
            $task->group_owner_id = (int)$request->responsible_team;
            $task->assigned_role_id = (int)$request->responsible_team;
        }
        //Assigned User
        $task->assigned_user_id = 0;
        if ((int)$request->attention_to > 0) {
            $task->assigned_user_id = (int)$request->attention_to;
        }

        $task->add_on = $request->add_on;
        $task->add_on_desc = $request->add_on_desc;
        $task->document_number = $request->document_number;

        //Lags
        $task->lag_days = (int)$request->lag_days;
        $task->schedule_lag_days = (int)$request->schedule_lag_days;

        $task->save();

        $task->applyTaskRule();

        $extension = TaskExtension::where('task_id', $id)->first();
        $extension->note = $request->note;
        $extension->save();


        $message = "Task Details Updated";

        //------------------------
        //  Return Confirmation Message
        //------------------------
        $data = array(
            'message' => $message,
            'task' => $task,
        );

        return $data;

    }

    /**
     *  ---------------------
     *  Update task Parameters
     *  ---------------------
     *
     * @param Request $request
     * @return string
     */
    public function updateTaskParameters(Request $request)
    {

        //return $request->target_hrs . ' :'. $request->chk_target_hrs . ' :'. $request->check_lock_date . ' :'.
        //$request->schedule_link_date . ' :'. $request->schedule_link_lag . ' :'. $request->schedule_link_number;
        if ($request->id === '' || $request->id === null) {
            return 'No Tasks Selected';
        }
        $updateArray = array();

        //Details
        $groupTypeSubChoice = 0;
        //Sub Category
        if ($request->taskSubType !== '') {
            $groupTypeSubChoice = 3;
        } //Type
        elseif ($request->taskType !== '') {
            $groupTypeSubChoice = 2;
        } //Group
        elseif ($request->taskGroup !== '') {
            $groupTypeSubChoice = 1;
        }

        switch ($groupTypeSubChoice) {
            case 1:
                //$updateArray["group_id"] =(int)$request->taskGroup;
                //$updateArray["task_type_id"] = (int)$request->taskType;
                break;
            case 2:
                $updateArray["group_id"] = TaskType::find((int)$request->taskType)->group_id;
                $updateArray["task_type_id"] = (int)$request->taskType;
                break;
            case 3:
                $typeId = TaskSubType::find((int)$request->taskSubType)->task_type_id;
                $groupId = TaskType::find($typeId)->group_id;
                $updateArray["group_id"] = $groupId;
                $updateArray["task_type_id"] = $typeId;
                $updateArray["task_sub_type_id"] = (int)$request->taskSubType;
                break;
        }
        //Add on Text
        if ($request->numberAddOn !== '') {
            $updateArray["add_on"] = $request->numberAddOn;
        }
        //Add on Text
        if ($request->numberAddOnDescription !== '') {
            $updateArray["add_on_desc"] = $request->numberAddOnDescription;
        }

        //Target Hours
        if ($request->chk_target_hrs === 'on') {
            if ((int)$request->target_hrs > 0) {
                $updateArray["target_val"] = (int)$request->target_hrs;
            }
        }
        //Lag
        if ($request->chk_general_lag_days === 'on') {
            if ((int)$request->general_lag_days >= 0) {
                $updateArray["lag_days"] = (int)$request->general_lag_days;
            }
        }
        //Lock Date
        $updateArray["lock_date"] = 0;
        if ($request->check_lock_date === 'on') {
            $updateArray["lock_date"] = 1;
        }
        //Schedule Lag
        if ($request->chk_schedule_link_lag === 'on') {
            $updateArray["schedule_lag_days"] = (int)$request->schedule_link_lag;
        }
        //Schedule Date Selection e.g Start, finish
        if ($request->schedule_link_date !== '') {
            $updateArray["schedule_date_choice"] = $request->schedule_link_date;
        }
        //Schedule Link Number
        if ($request->update_schedule_link_chk === 'on') {
            $schedule_number = ScheduleDate::find($request->scheduleNumber)->number;
            $updateArray["schedule_number"] = $schedule_number;
        }

        //Team
        if ((int)$request->responsible_team !== 0) {
            $updateArray["group_owner_id"] = (int)$request->responsible_team;
            $updateArray["assigned_role_id"] = (int)$request->responsible_team;
        }
        //Role
        if ((int)$request->attention_to !== 0) {
            $updateArray["assigned_user_id"] = (int)$request->attention_to;
        }

        //System
        if ((int)$request->system !== 0) {
            $updateArray["system_id"] = (int)$request->system;
        }

        $arrayOfIds = [];
        foreach ($request->id as $id) {
            array_push($arrayOfIds, (int)$id);
        }
        Task::whereIn('id', $arrayOfIds)->update($updateArray);

        //todo CRON job
        //Apply the rule for each task
        $applyRuleToTask = new ApplyRuleToTasks();
        for ($i = 0; $i < sizeof($arrayOfIds); $i++) {
            $applyRuleToTask->applyTheRuleToThisTaskId((int)$request->id[$i]);
        }

        return sizeof($arrayOfIds) . ' tasks updated';
    }


    public function getBulkUpdateTaskForm()
    {

        $project_id = Session::get('project_id');
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $systems = System::orderBy('tag', 'ASC')->whereNotIn('tag', array('N/A'))->get();
        $groups = Group::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $taskTypes = TaskType::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $stages = Stage::all();
        $users = User::get();
        $taskSubTypes = TaskSubType::whereNotIn('name', array('N/A'))->get();

        return view('rowsys.tracker.tasks.parameters.forms.bulk_update_form', [
            'areas' => $areas,
            'systems' => $systems,
            'groups' => $groups,
            'taskTypes' => $taskTypes,
            'stages' => $stages,
            'taskSubTypes' => $taskSubTypes,
            'users' => $users,
        ]);
    }


    public function checkIfTaskNumberExists()
    {
        $systems = System::get();
        $data = $_GET['data'];
        $exist = 0;
        if (isset($_GET['data']) && $data !== '') {
            $exist = Task::where('number', $data['number'])->where('stage_id', $data['stage'])->count();
        }

        return $exist;
    }

    /**
     * ---------------------------------
     *     Get Task Search Ribbon
     *  ---------------------------------
     *
     * Returns the task Search Ribbon
     *
     */
    public function getTaskSearchRibbon()
    {
        // $project_id = $request->session()->get('project_id');
        //todo add project id
        $project_id = 1;
        $areas = Area::where('project_id', '=', $project_id)->get();
        $systems = System::where('project_id', '=', $project_id)->get();
        $groups = Group::where('project_id', '=', $project_id)->get();
        $taskTypes = TaskType::where('project_id', '=', $project_id)->get();
        $stages = Stage::all();

        return view('rowsys.tracker.tasks.search.ribbon', [
            'areas' => $areas,
            'systems' => $systems,
            'groups' => $groups,
            'taskTypes' => $taskTypes,
            'stages' => $stages,
        ]);

    }

    /**
     * ---------------------------------
     *     Compile All Tasks
     *  ---------------------------------
     *
     * Returns the task Update Status Popup
     *
     */
    public function compileTasks()
    {
        $compiler = new Compile();
        $compiler->compileAllTasks();
        return ['message' => 'compile started'];

    }

    /**
     * ---------------------------------
     *     Compile Task Tags
     *  ---------------------------------
     *
     * Returns the task Update Status Popup
     *
     */
    public function compileTaskTags()
    {
        $compiler = new Compile();
        $compiler->compileTaskTags();

        return $compiler->getMessage();
    }

    /**
     * ---------------------------------
     *     Update Task Status Popup
     *  ---------------------------------
     *
     * Returns the task Update Status Popup
     *
     */
    public function updateStatusPopup($id)
    {
        $task = Task::find($id);

        return \View::make('rowsys.tracker.tasks.update.update_task_status', array('task' => $task));
    }

    /**
     * ---------------------------------
     *         Get Task Details View
     *  ---------------------------------
     *
     * Returns the task details for a selected
     * task to show on a view
     */
    public function getTaskDetailsView($id)
    {
        $task = Task::with('area', 'group', 'system', 'taskType', 'stage', 'genColor', 'revColor', 'reIssuColor',
            'sOffColor')->find($id);

        return \View::make('rowsys.tracker.tasks.read.details.details', array('task' => $task));
    }


    /**
     * ---------------------------------
     *         Get Task Schedule Dates
     *  ---------------------------------
     *
     * Returns the task Scheduler Task Dates
     * If No Dates then Allow thew user to end the dates
     */
    public function getTaskSchedulerDates(Request $request)
    {
        $dates = ScheduleDate::find($request->scheduleId);

        return \View::make('rowsys.tracker.tasks.create.dates_partial', array('dates' => $dates));

    }

    /**
     * ---------------------------------
     *         Get Task Errors View
     *  ---------------------------------
     *
     * Returns a list of tasks with errors e.g no schedule number
     */
    public function getTaskErrorsView(Request $request)
    {
        $tasks = Task::where('compile_error', '>', '0')->get();

        return \View::make('rowsys.tracker.tasks.errors.index', array('tasks' => $tasks));
    }


    /**
     * ---------------------------------
     *         Mobile - Task Details
     *  ---------------------------------
     *
     * Returns the task Scheduler Task Dates
     * If No Dates then Allow thew user to end the dates
     */

    public function mobileTaskDetails(Request $request)
    {

        return \View::make('rowsys.mobile.tasks.task_details');

    }

    /**
     * ---------------------------------
     *         Get Schedule Dates List
     *  ---------------------------------
     *
     * Returns the task Scheduler Task Dates
     * If No Dates then Allow thew user to end the dates
     */

    public function getScheduleDateListJSON()
    {
        $scheduleDates = ScheduleDate::orderBy('number', 'ASC')->get();

        return $scheduleDates;

    }

    /**
     * ---------------------------------
     *         Get Tasks Gant View
     *  ---------------------------------
     *
     * Returns the window pop up for the gantt chart
     */

    public function getTasksGanttChart()
    {
        //this data is not used
        $tasks = Task::get()->take(1);

        return view('rowsys.tracker.tasks.read.gantt.index', [
            'tasks' => $tasks,
        ]);

    }


    public function destroy($id)
    {
        Task::destroy($id);
        TaskStepPhase::where('task_id', $id)->delete();

        return "task deleted";
    }

    /**
     * ---------------------------------
     *         DELETE - Bulk Delete Tasks
     *  ---------------------------------
     *
     * Returns the task Scheduler Task Dates
     * If No Dates then Allow thew user to end the dates
     */

    public function bulkDeleteTasks(Request $request)
    {
        //$scheduleDates = ScheduleDate::orderBy('number', 'ASC')->get();
        $arrayOfIds = [];
        foreach ($request->id as $id) {
            array_push($arrayOfIds, (int)$id);
        }
        Task::destroy($arrayOfIds);
        TaskStepPhase::whereIn('task_id', $arrayOfIds)->delete();

        return sizeof($arrayOfIds) . " tasks Deleted";

    }

}
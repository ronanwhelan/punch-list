<?php

namespace App\Http\Controllers\Tracker\Charts;

use App\Models\Area;
use App\Models\Group;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskType;
use App\Rowsys\Tracker\Classes\Charts;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ChartsController extends Controller
{
    //

    /* -------------------------------------
             AREAS MONTHLY - PROJECTED BAR AND LINE
    ----------------------------------------*/
    public function getAreaBarAndLineChartData($area_id)
    {

        $areaIds = Task::groupBy('group_owner_id')->lists('group_owner_id')->toArray();
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->whereIn('id', $areaIds)->get();
        //$areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        //Initial load - all areas
        $chartObj = new Charts();
        $projectedChartData = $chartObj->areaProjectedBarChartData($areaIds,'06-2016','12-2018');

        $selectedAreaName = 'All Teams';
       // return $projectedChartData;

        return view('rowsys.tracker.tasks.graphs.test.test2', [
            'areas' => $areas,
            'selectedAreaName' => $selectedAreaName,
            'projectedChartData' => $projectedChartData,
        ]);
    }


    /* -------------------------------------
         PROJECTED BAR AND LINE - get the data over ajx - areas monthly
    ----------------------------------------*/
    public function areaBarAndLineChartData(Request $request)
    {
        $teams = $request->teams;
        $fromDate = $request->from_date;
        $toDate = $request->to_date;
        $dateChoice = $request->chart_axis_selection;

        $teamsArray =[];
        if(!isset($teams)){
            $teamsArray = Task::groupBy('group_owner_id')->lists('group_owner_id')->toArray();
        }else{
            for($i =0; $i < sizeof($teams);$i++){
                array_push($teamsArray,(int)$teams[$i]);
            }
        }
        if(!isset($fromDate)){
            $fromDate = '';
        }
        if(!isset($toDate)){
            $toDate = '';
        }

        //$teams =
        $chartObj = new Charts();
        $projectedChartData = $chartObj->areaProjectedBarChartData($teamsArray,$fromDate,$toDate);
        $projectedChartData = json_encode($projectedChartData);
        return [
            'message' => 'here is a message for chart data',
            'chartData' => $projectedChartData,
        ];
    }

    /* ----------------------------------------------
              Progress Overview (HIts & Misses)
    -----------------------------------------------*/

    public function areaProgressOverview(Request $request)
    {

        //dd($request);
        $chartObj = new Charts();
        $chart1Data = $chartObj->areaProgressOverview($request);

     // return $chart1Data;

        $stages = Stage::orderBy('name', 'ASC')->get();
        $groups = Group::orderBy('name', 'ASC')->get();
        $types = TaskType::orderBy('name', 'ASC')->get();
        $areas = Area::orderBy('name', 'ASC')->get();

        if (sizeof($request->type) === 0) {
            $oldTypes = [];
        } else {
            $oldTypes = $request->type;
        }
        if (sizeof($request->group) === 0) {
            $oldGroups = [];
        } else {
            $oldGroups = $request->group;
        }

        if (sizeof($request->role) === 0) {
            $oldRoles = [];
        } else {
            $oldRoles = $request->role;
        }

        $oldData = ['stage' => (int)$request->stage, 'group' => $oldGroups, 'type' => $oldTypes, 'role' => $oldRoles, 'date' => $request->to_date];

        // return $chart1Data;

        return view('rowsys.tracker.tasks.graphs.hits_misses.index', [

            'chart1Data' => $chart1Data,
            'stages' => $stages,
            'groups' => $groups,
            'types' => $types,
            'areas' => $areas,
            'old' => $oldData,
        ]);

    }


    public function test1($area_id)
    {

        $areaIds = Task::groupBy('group_owner_id')->lists('group_owner_id')->toArray();
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->whereIn('id', $areaIds)->get();
        $chartObj = new Charts();
        $projectedChartData = $chartObj->areaProjectedBarChartData($area_id);

        return view('rowsys.tracker.tasks.graphs.test.test1', [
            'areas' => $areas,
            // 'selectedArea' => $area,
            'projectedChartData' => $projectedChartData,
        ]);

    }


    public function test3()
    {

        $areaIds = Task::groupBy('group_owner_id')->lists('group_owner_id')->toArray();
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->whereIn('id', $areaIds)->get();
        $chartObj = new Charts();

        //$projectedChartData = $chartObj->areaProjectedBarChartData($area_id);

        return view('rowsys.tracker.tasks.graphs.test.test3', [
            'areas' => $areas,
            // 'selectedArea' => $area,
            //'projectedChartData' => $projectedChartData,
        ]);

    }


    public function test4()
    {

        $areaIds = Task::groupBy('group_owner_id')->lists('group_owner_id')->toArray();
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->whereIn('id', $areaIds)->get();
        $chartObj = new Charts();

        //$projectedChartData = $chartObj->areaProjectedBarChartData($area_id);

        return view('rowsys.tracker.tasks.graphs.test.test4', [
            'areas' => $areas,
            // 'selectedArea' => $area,
            //'projectedChartData' => $projectedChartData,
        ]);

    }


    /* -------------------------------------
     *     USER SAVED CHARTS
     -------------------------------------*/
    public function userChart1Data()
    {
        $data = [];

        $dataSet1 = ['label' => 'Label1', 'backgroundColor' => "rgba(151,187,205,0.5)", 'data' => [10, 12, 18, 25, 12, 67, 100]];
        $dataSet2 = ['label' => 'Label2', 'backgroundColor' => "rgba(151,187,205,0.5)", 'data' => [15, 10, 30, 25, 46, 55, 90]];

        array_push($data, $dataSet1);
        array_push($data, $dataSet2);

        return $data;

    }


    /* -------------------------------------
     *     AREA HOURS BREAK DOWN BY MONTH - BAR
     -------------------------------------*/
    public function getAreaMonthlyBar($area_id)
    {


        $databaseName = env('DB_DATABASE');

        //SELECT month(prim_date) as month ,SUM(target_val) as value FROM tasks where stage_id = 1 and area_id = 2 GROUP BY month(prim_date);
        $prepData = \DB::select(\DB::raw("SELECT concat(year(prim_date),'-',monthname(prim_date)) as date, SUM(target_val) as value ,month(prim_date) as month FROM tasks where stage_id = :stage and group_owner_id = :area GROUP BY DATE_FORMAT(prim_date,'%Y-%m')"),
            array(
                'stage' => 1,
                'area' => $area_id,
            ));
        $exeData = \DB::select(\DB::raw("SELECT concat(year(prim_date),'-',monthname(prim_date)) as date,SUM(target_val) as value ,month(prim_date) as month FROM tasks where stage_id = :stage and group_owner_id = :area GROUP BY DATE_FORMAT(prim_date,'%Y-%m')"),
            array(
                'stage' => 2,
                'area' => $area_id,
            ));

        $prepSize = sizeof($prepData);
        $exeSize = sizeof($exeData);
        $largestDataArraySize = $prepSize;
        $largestDataArray = $prepData;
        if ($exeSize > $prepSize) {
            $largestDataArraySize = $exeSize;
            $largestDataArray = $exeData;
        }
        $data = [];
        if ($prepSize > 0) {
            for ($i = 0; $i < $largestDataArraySize; $i++) {

                $month = $this->months[$largestDataArray[$i]->month];
                $array['m'] = $largestDataArray[$i]->date;

                if (isset($prepData[$i]->value)) {
                    $array['p'] = $prepData[$i]->value;
                } else {
                    $array['p'] = 0;
                }
                if (isset($exeData[$i]->value)) {
                    $array['e'] = $exeData[$i]->value;
                } else {
                    $array['e'] = 0;
                }
                array_push($data, $array);

            }
        }
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $selectedArea = Area::find($area_id);

        return view('rowsys.tracker.tasks.stats.areas.monthly_hours_bar.index', [
            'data' => $data,
            'selectedArea' => $selectedArea,
            'areas' => $areas,
        ]);
    }


    public function combineAreaGraphDataArray($stage1Data, $stage2Data)
    {
        $dataOut = [];
        $sizeOfArray1 = sizeof($stage1Data);
        $sizeOfArray2 = sizeof($stage2Data);

        $largestDataArraySize = $sizeOfArray1;

        if ($sizeOfArray2 > $sizeOfArray1) {
            $largestDataArraySize = $sizeOfArray2;
        }

        return $dataOut;

    }


    /* -------------------------------------
       Category Tabulation Report Data
    ----------------------------------------*/
    //returns the task category tabulation report
    public function taskCategoryTabulationReport(Request $request)
    {

        $stages = Stage::orderBy('name', 'ASC')->get();
        $groups = Group::orderBy('name', 'ASC')->get();
        $types = TaskType::orderBy('name', 'ASC')->get();
        $areas = Area::orderBy('name', 'ASC')->get();

        if (sizeof($request->type) === 0) {
            $oldTypes = [];
        } else {
            $oldTypes = $request->type;
        }
        if (sizeof($request->group) === 0) {
            $oldGroups = [];
        } else {
            $oldGroups = $request->group;
        }


        if (sizeof($request->role) === 0) {
            $oldRoles = [];
        } else {
            $oldRoles = $request->role;
        }

        $oldData = ['stage' => (int)$request->stage, 'group' => $oldGroups, 'type' => $oldTypes, 'role' => $oldRoles, 'date' => $request->to_date];

        $chartObj = new Charts();
        $tableData = $chartObj->taskCategoryTabulationReportData($request);

        return view('rowsys.tracker.tasks.graphs.tabulation_report.index', [
            'tableData' => $tableData,
            'stages' => $stages,
            'groups' => $groups,
            'types' => $types,
            'areas' => $areas,
            'old' => $oldData,
        ]);
    }

    //returns the task category tabulation report
    public function taskCategoryTabulationReportData(Request $request)
    {
        $chartObj = new Charts();
        $projectedChartData = $chartObj->taskCategoryTabulationReportData($request);

        return $projectedChartData;

    }
}

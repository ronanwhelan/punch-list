<?php

namespace App\Http\Controllers\Tracker\Charts;

use App\Models\Area;
use App\Models\Group;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskType;
use App\Rowsys\Tracker\Classes\Charts;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AreasWeeklyChartController extends Controller
{

    /* -------------------------------------
         AREAS WEEKLY - PROJECTED BAR AND LINE
    ----------------------------------------*/
    public function getWeeklyAreaBarAndLineChartData()
    {
        $areaIds = Task::groupBy('group_owner_id')->lists('group_owner_id')->toArray();
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->whereIn('id', $areaIds)->get();
        $chartObj = new Charts();
        $projectedChartData = $chartObj->areasWeeklyProjectedBarChartData($areaIds,'06-2016','12-2018');

        $selectedAreaName = 'All Teams';

       //return $projectedChartData;

        return view('rowsys.tracker.tasks.graphs.test.test20', [
            'areas' => $areas,
            'selectedAreaName' => $selectedAreaName,
            'projectedChartData' => $projectedChartData,
        ]);
    }

    /* -------------------------------------
     PROJECTED BAR AND LINE - get the data over ajx - areas monthly
----------------------------------------*/
    public function areaWeeklyBarAndLineChartData(Request $request)
    {

        $teams = $request->teams;
        $fromDate = $request->from_date;
        $toDate = $request->to_date;
        $dateChoice = $request->chart_axis_selection;

        $teamsArray =[];
        if(!isset($teams)){
            $teamsArray = Task::groupBy('group_owner_id')->lists('group_owner_id')->toArray();
        }else{
            for($i =0; $i < sizeof($teams);$i++){
                array_push($teamsArray,(int)$teams[$i]);
            }
        }
        if(!isset($fromDate)){
            $fromDate = '';
        }
        if(!isset($toDate)){
            $toDate = '';
        }

        //$teams =
        $chartObj = new Charts();
        $projectedChartData = $chartObj->areasWeeklyProjectedBarChartData($teamsArray,$fromDate,$toDate);
        $projectedChartData = json_encode($projectedChartData);
        return [
            'message' => 'here is a message for chart data',
            'chartData' => $projectedChartData,
        ];
    }


}

<?php

namespace App\Http\Controllers\Tracker\Systems;

use App\Models\Area;
use App\Models\Component;
use App\Models\PunchList\Item;
use App\Models\Stage;
use App\Models\System;
use App\Models\Task;
use App\Models\TaskType;
use App\Models\UserFilter;
use App\Rowsys\Tracker\Classes\Charts;
use App\Rowsys\Tracker\Classes\Compile;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class SystemsController extends Controller
{
    public function index()
    {

        $systems = System::orderBy('tag','ASC')->with('area')->whereNotIn('tag', array('N/A'))->get();
        $areas = Area::orderBy('name','ASC')->get();

        // $tasks = Task::with('area', 'group', 'system', 'taskType', 'stage')->orderBy('number', 'ASC')->take(1000)->get();
        return view('rowsys.tracker.systems.create.index', [
            'systems' => $systems,
            'areas' => $areas,
        ]);
    }

    public function addSystem(Request $request)
    {
        $this->validate($request, [
            'systemTag' => 'required|unique:systems,tag',//
            'systemDescription' => 'required',
            'systemArea' => 'required',
        ]);

        $system = System::firstOrNew([
            'tag' => $request->systemTag,
            'description' => $request->systemDescription,
            'area_id' => $request->systemArea,
            'project_id' => 1,
        ]);
        $system->save();
        $message = ' System Added';
        return \Redirect::back()->with('message', $message)->withInput();

    }

    // UPDATE
    public function getSystemDetails($system_id)
    {
        $system = System::find($system_id);
        $areas = Area::orderBy('name','ASC')->get();
        $taskCount = Task::where('system_id',$system_id)->count();
        $itemCount = Item::where('system_id',$system_id)->count();
        $totalCount = $taskCount + $itemCount;

        return view('rowsys.tracker.systems.update.details', [
            'system' => $system,
            'areas' => $areas,
            'taskCount' => $taskCount,
            'itemCount' => $itemCount,
            'totalCount' => $totalCount,
        ]);
    }

    public function updateSystemDetails(Request $request, $id)
    {
        $this->validate($request, [
            //'system' => 'required',
            'update_name' => 'required',
            'update_description' => 'required',
        ]);
        $system = System::find($id);
        $system->tag = $request->update_name;
        $system->description = $request->update_description;

        if($system->area_id !== $request->update_parent){
            Task::where('system_id', $id)->update(['area_id' => (int)$request->update_parent]);
            $system->area_id = $request->update_parent;
        }

        $system->save();
        $compile = new Compile();
        $compile->compileAffectedTasksAfterModelIsUpdate('system_id',$id);

        $message = ' System Updated';

        return $message;
    }


    public function deleteSystem($id){

        System::destroy($id);

        return " System Deleted";
    }

    public function ganttChart(){
        $systems = System::get();

        return view('rowsys.tracker.systems.gantt.index', [
            'systems' => $systems,
        ]);
    }

    /* ---------------------------------------------------------------
     * System and  Category Overview Report
     * ---------------------------------------------------------------
     */
    public function statusTable(Request $request){

        /* ---------------------------------------------------------------
        *   Search Details - to filter system list
        * ---------------------------------------------------------------
        */
        $searchAreas = $request->areas;
        $searchAreasCount = 0;
        if(isset($request->areas)){
            $searchAreasCount = sizeof($searchAreas);
        }

        $searchSystemsCount = 0;
        if(isset($request->systems)){
            $searchSystemsCount = sizeof($request->systems);
        }
        $searchSystems = $request->systems;


        $showAll = false;
        if($request->chk_show_all === 'on'){
            $showAll = true;
        }


        $systemIds = [];

        //Nothing selected
        if($searchAreasCount === 0 && $searchSystemsCount === 0){
            $systems = System::whereNotIn('tag', array('N/A'))
               // ->limit('100')
                ->get();
        }
        //Only Area selected
        else if($searchAreasCount > 0 &&  $searchSystemsCount === 0){
            $systems = System::whereIn('area_id',$searchAreas)->get();
        }
        //Only System Selected
        else if($searchAreasCount === 0 &&  $searchSystemsCount > 0){
            $systems = System::whereIn('id',$searchSystems)->get();

        }else{
            $systems = System::whereIn('id',$searchSystems)->get();
        }

        /* ---------------------------------------------------------------
         * Get the Systems Details for the Table
         * ---------------------------------------------------------------
         */

        //User Configuration settings
        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();

        ///dd($userFilter);
        //$categoryAndStageIds = [[1,2], [1,7], [1,17], [2,7], [2,17], [1,54], [1,60]];
        $categoryAndStageIds = json_decode($userFilter->system_report_config);

        //---------------------------------
        //Populate the Categories Details for the Column Headings
        $columnHeaders = [];
        $tableData = [];
         if($categoryAndStageIds !== '' && $categoryAndStageIds !== null ) {
             foreach ($categoryAndStageIds as $category) {
                 $taskType = TaskType::find($category[1]);
                 $stage = Stage::find($category[0]);
                 //array_push($categories,$taskType->name.' ('.$taskType->short_name.')');
                 if(isset($taskType) && isset($stage) ){
                     array_push($columnHeaders, [$taskType->short_name, $taskType->name, $stage->short_name]);
                 }
             }
             global $categoryTaskCountTotal;
             //---------------------------------
             //  Table Data
             //---------------------------------
             $tableData = [];
             $today = Carbon::now();

             foreach ($systems as $system) {
                 $categoryTaskCountTotal = 0;
                 $systemData = [];
                 array_push($systemData, $system->tag);
                 array_push($systemData, $system->description);

                 $systemTaskTotalCount = $taskTotal = Task::where('system_id', $system->id)->count();
                 $systemTaskDoneCount = $taskTotal = Task::where('system_id', $system->id)->where('complete', 1)->count();
                 $systemPercent = 0;
                 if ($systemTaskTotalCount > 0) {
                     $systemPercent = round(($systemTaskDoneCount / $systemTaskTotalCount) * 100, 0);
                     $progressPercent = $systemPercent;
                     $systemPercent = $systemPercent . '%';

                 }
                 $systemPercent = '<span class="">'.$systemPercent.' (' . $systemTaskDoneCount . ' ' . ' / ' . $systemTaskTotalCount . ')</span>';
                 array_push($systemData, $systemPercent);
                 array_push($systemData, $progressPercent);

                 $categoryTaskCountTotal = 0;
                 foreach ($categoryAndStageIds as $category) {

                     $taskTotal = Task::where('system_id', $system->id)
                         ->where('stage_id', $category[0])
                         ->where('task_type_id', $category[1])
                         ->count();
                     $categoryTaskCountTotal = $categoryTaskCountTotal +$taskTotal;

                     $taskSumPercent = Task::where('system_id', $system->id)
                         ->where('stage_id', $category[0])
                         ->where('task_type_id', $category[1])
                         ->sum('status');


                     $taskDoneTotal = Task::where('system_id', $system->id)
                         ->where('stage_id', $category[0])
                         ->where('task_type_id', $category[1])
                         ->where('complete', 1)
                         ->count();

                     $taskOverDueTotal = Task::where('system_id', $system->id)
                         ->where('stage_id', $category[0])
                         ->where('task_type_id', $category[1])
                         ->where('complete', 0)
                         ->where('next_td', '<', $today)
                         ->count();

                     $percent = 0;

                     //unknown/fault = 0, not started = 1, started = 2, done = 3 , overdue = 4
                     $statusNum = 0;
                     $statusText = 'unknown/fault';
                     $backColour = 'red';

                     $statusIconUnknown = '<button type="button" class="btn btn-danger btn-block" data-container="body" data-toggle="popover" data-placement="bottom" data-content=""><i class="fa fa-question-circle "></i></button>';
                     $statusIconNotStarted = '<button type="button" class="btn btn-default btn-block" data-container="body" data-toggle="popover" data-placement="bottom" data-content=""><i class="fa fa-hourglass-o"></i></button>';
                     $statusIconStarted = '<button type="button" class="btn btn-primary btn-block" data-container="body" data-toggle="popover" data-placement="bottom" data-content=""><i class="fa fa-hourglass-start"></i></button>';
                     $statusIconComplete = '<button type="button" class="btn btn-success btn-block" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Complete"><i class="fa fa-check-square-o"></i></button>';
                     $statusIconPastDue = '<button type="button" class="btn btn-danger btn-block" data-container="body" data-toggle="popover" data-placement="bottom" data-content=""><i class="fa fa-exclamation-triangle "></i></button>';

                     $statusIconUnknown = '<i class="fa fa-question-circle  " style="margin: 2px;padding: 5px"></i>';
                     $statusIconNotStarted = '<i class="fa fa-square-o " style="margin: 2px;padding: 5px"></i>';
                     $statusIconStarted = '<i class="fa fa-square-o " style="background-color: lightgreen;margin: 2px;padding: 5px"></i>';
                     $statusIconComplete = '<i class="fa fa-check-square-o" style="margin: 2px;padding: 5px"></i>';
                     $statusIconPastDue = '<i class="fa fa-exclamation " style="margin: 2px;padding: 5px"></i>';

                     $statsToolTip = 'Total = ' . $taskTotal . '  Done = ' . $taskDoneTotal . ' Overdue = ' . $taskOverDueTotal;
                     $statusIcon = $statusIconUnknown;


                     if ($taskTotal > 0) {
                         $percent = round(($taskDoneTotal / $taskTotal) * 100, 0);
                         $percent = round(($taskSumPercent / $taskTotal), 0);

                         //$percent =ceil(($taskDoneTotal/$taskTotal) * 100);

                         if ($percent < 1) {
                             $statusNum = 1;
                             $statusText = 'Not Started';
                             $backColour = 'white';
                             $statusIcon = $statusIconNotStarted;
                         }
                         if ($percent > 1 && $percent < 100) {
                             $statusNum = 2;
                             $statusText = 'Started';
                             $backColour = 'white';
                             $statusIcon = $statusIconStarted;
                         }
                         if ($percent > 99) {
                             $statusNum = 3;
                             $statusIcon = $statusIconComplete;
                             $x = 'Total = ' . $taskTotal . '  Done = ' . $taskDoneTotal;
                             $statusText = 'Done';
                             $backColour = 'lightgreen';
                             // $statusIcon = '<button type="button" class="btn btn-success btn-block" data-container="body" data-toggle="popover" data-placement="bottom" data-content="' .$x.'"><i class="fa fa-check-square-o"></i></button>';
                         }
                         $percent = $percent . '%';
                     }
                     if ($taskTotal === 0) {
                         $percent = 'no task(s)';
                         $statusText = '---';
                         $backColour = 'lightgrey';
                     }
                     if ($taskOverDueTotal > 0) {
                         $statusNum = 4;
                         $statusText = 'Over Due';
                         $backColour = 'red';
                         $statusIcon = $statusIconPastDue;

                     }
                     array_push($systemData, [$backColour, $statusNum, $statusText, $statusIcon, $statsToolTip, $taskTotal, $taskDoneTotal, $percent]);
                 }

                 if($request->chk_select_all === 'on'){
                     return $request->chk_select_all;
                 }

                 if($showAll){
                     array_push($tableData, $systemData);
                 }else{
                     if($categoryTaskCountTotal > 0){
                         array_push($tableData, $systemData);
                     }
                 }
             }

         }
        //return $request->chk_select_all;
        //return $tableData;

        /* ---------------------------------------------------------------
        *   Return the Search info - like old data,  Areas, systems
        * ---------------------------------------------------------------
        */
        $areas = Area::whereNotIn('name', array('N/A'))->orderBy('name','ASC')->get();
        if (sizeof($request->areas) === 0) {
            $oldAreas = [];
        } else {
            $oldAreas = $request->areas;
        }

        $systems = System::whereNotIn('tag', array('N/A'))->orderBy('tag','ASC')->get();
        if (sizeof($request->systems) === 0) {
            $oldSystems = [];
        } else {
            $oldSystems = $request->systems;
        }
        $oldData = ['stage' => (int)$request->stage, 'systems' => $oldSystems,'areas' => $oldAreas];


        $stages = Stage::orderBy('name','ASC')->get();
        $types = TaskType::orderBy('name','ASC')->whereNotIn('name', array('N/A'))->get();




        return view('rowsys.tracker.tasks.graphs.systems_status.read.table.index', [
            'systems' => $systems,
            'stages' =>$stages,
            'areas' =>$areas,
            'types' =>$types,
            'tableData' => $tableData,
            'categories' => $categoryAndStageIds,
            'columnHeaders' => $columnHeaders,
            'old' => $oldData,
        ]);
    }


    /* ---------------------------------------------------------------
     * Update the Systems Category Report Configuration settings
     * ---------------------------------------------------------------
     */
    public function updateSystemsOverviewConfiguration(Request $request){

        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $data = [];

        if(isset($userFilter)){
            $data = json_decode($userFilter->system_report_config);

            if($data === '' || $data === null){
                $data = [];
                array_push($data,[(int)$request->data['stage'],(int)$request->data['type']]);

            }else{

                array_push($data,[(int)$request->data['stage'],(int)$request->data['type']]);
            }

            $configJson = collect($data)->toJson();

            $userFilter->system_report_config = '';

            $userFilter->system_report_config = $configJson;

            $userFilter->save();
        }


        return $userFilter->system_report_config;




 /*       $chartsObj = new Charts();
        $userFilter = $chartsObj->saveSystemOverviewReportSettings();
        return $userFilter;*/

    }

    /* ---------------------------------------------------------------
    * Clear the Systems Category Report Configuration settings
    * ---------------------------------------------------------------
    */
    public function clearSystemsOverviewConfiguration(){

        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();

        $userFilter->system_report_config = '';
        $userFilter->save();

        return "updated";

    }
    /* ---------------------------------------------------------------
    * Returns the configuration table
    * ---------------------------------------------------------------
    */
    public function overviewReportConfigTable(){

        $types = TaskType::get();
        $stages = Stage::get();
        $tableData = [];
        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $configJson = [];
        if(isset($userFilter)){
            if($userFilter->system_report_config !== ''){
                $configJson = json_decode($userFilter->system_report_config);
                foreach($configJson as $setting){
                    $type = TaskType::find($setting[1]);
                    $stage = Stage::find($setting[0]);
                    if(isset($type)){
                        array_push($tableData,[$stage->short_name,$type->name.' ('.$type->short_name.')']);
                    }
                }
            }
        }

        return view('rowsys.tracker.tasks.graphs.systems_status.report_settings.table', [
            'tableData' => $tableData,
            'types' => $types,
            'stages' => $stages,

        ]);

    }

    public function componentList($id){

        return Component::where('system_id',$id)->get();
    }
}

<?php

namespace App\Http\Controllers\Tracker\Areas;

use App\Models\Area;
use App\Models\Role;
use App\Models\System;
use App\Models\Task;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AreasController extends Controller
{
    public function index()
    {
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();

        return view('rowsys.tracker.areas.create.index', [
            'areas' => $areas,
        ]);
    }
    

    public function addArea(Request $request)
    {
        $this->validate($request, [
            'areaName' => 'required|unique:areas,name',//
            'areaDescription' => 'required',
        ]);

        $area = Area::firstOrNew([
            'name' => $request->areaName,
            'description' => $request->areaDescription,
            'project_id' => 1,
        ]);
        $area->save();

        //Add Role - to ensure the tables are aligned
        $role = new Role();
        $role->id = $area->id;
        $role->name = $area->name;
        $role->save();

        $message = ' Area Added';
        return \Redirect::back()->with('message', $message)->withInput();

    }

    // UPDATE
    public function getAreaDetails($area_id)
    {
        $area = Area::find($area_id);
        $taskCount = Task::where('area_id',$area_id)->count();
        $systemCount = System::where('area_id',$area_id)->count();
        return view('rowsys.tracker.areas.update.details', [
            'area' => $area,
            'taskCount' => $taskCount,
            'systemCount' => $systemCount,

        ]);
    }

    public function updateAreaDetails(Request $request, $id)
    {
        $this->validate($request, [
            //'system' => 'required',
            'update_name' => 'required',
            'update_short_name' => 'required',
            'update_description' => 'required',
        ]);
        $area = Area::find($id);
        $area->name = $request->update_name;
        $area->short_name = $request->update_short_name;
        $area->description = $request->update_description;
        $area->save();
        $message = ' Area Updated';

        return $message;
    }

    public function deleteArea($id){

        Area::destroy($id);

        return " Area Deleted";
    }
}

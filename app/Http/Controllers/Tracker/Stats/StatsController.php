<?php

namespace App\Http\Controllers\Tracker\Stats;


use App\Models\Area;
use App\Models\Group;
use App\Models\Project;
use App\Models\Stage;
use App\Models\System;
use App\Models\Task;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Rowsys\Tracker\Classes\TaskStats;

class StatsController extends Controller
{

    protected $months = array(
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'June',
        'July ',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
        'Jan',
        'Feb',
        'March',
        'April',
        'May',
        'June',
        'July ',
        'August',
        'September',
        'October',
        'November',
        'December',
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'June',
        'July ',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
        'Jan',
        'Feb',
        'March',
        'April',
        'May',
        'June',
        'July ',
        'August',
        'September',
        'October',
        'November',
        'December',
    );

    /**
     * Display a listing of the resource.
     * @class View
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stats = new TaskStats();

        $allStats = $stats->getStats();
        $genTaskStats = $stats->getGenTaskStats();
        $groupTaskStatsInNextTwoWeeks = $stats->getTaskGroupStatsForNextTwoWeeks();

        $taskBreakDownByGroup = $stats->getTaskBreakDownByGroup();

        $taskBreakDownByGroupTaskType = $stats->getTaskBreakDownByGroupTaskType();
        //return $taskBreakDownByGroupTaskType[0][1]['Automation'][0];

        $groupTaskTypeBreakDown = $stats->getGroupTaskTypeBreakDown(2);
        //return $groupTaskTypeBreakDown;
        $groupBreakDown = $stats->getGroupBreakDown();

        return view('rowsys.tracker.tasks.stats.index', [
            'allStats' => $allStats,
            'allStatsForJs' => json_encode($allStats),
            'groupTaskStatsInNextTwoWeeks' => $groupTaskStatsInNextTwoWeeks,
            // 'taskBreakDownByGroup' => $taskBreakDownByGroup,
            'taskBreakDownByGroup' => json_encode($taskBreakDownByGroup),
            'groupBreakDown' => json_encode($groupBreakDown),
            // 'groupTaskStatsInNextTwoWeeks' => json_encode($groupTaskStatsInNextTwoWeeks),
            //'genTaskStats' => $genTaskStats,
        ]);
    }

    public function groupBreakDown()
    {
        $stats = new TaskStats();
        $groupBreakDown = $stats->getGroupBreakDown();
        $groupTaskTypeBreakDown = $stats->getGroupTaskTypeBreakDown(2);
        $systemTaskTypeBreakDown = $stats->getSystemTaskTypeBreakDown(10);
        $groupBreakDownHrs = $stats->getGroupBreakDownHrs();
        //return $systemTaskTypeBreakDown;
        //dd($systemTaskTypeBreakDown[0]->taskType->name);
        return view('rowsys.tracker.tasks.stats.group_breakdown', [
            'groupBreakDown' => $groupBreakDown,
            'groupTaskTypeBreakDown' => $groupTaskTypeBreakDown,
            'systemTaskTypeBreakDown' => $systemTaskTypeBreakDown,
            'groupBreakDownHrs' => $groupBreakDownHrs,
        ]);
    }


    //-----------------------------------------------
    //         PROJECT
    //-----------------------------------------------
    public function projectBarGraph()
    {
        $stats = new TaskStats();
        $generalStats = $stats->getStats();
        $projectStats = $stats->getAllAreasBarHrsGraphData();
        $groupBreakDown = $stats->getGroupBreakDown();
        $groupTaskTypeBreakDown = $stats->getGroupTaskTypeBreakDown(2);
        $systemTaskTypeBreakDown = $stats->getSystemTaskTypeBreakDown(10);
        $groupBreakDownHrs = $stats->getGroupBreakDownHrs();
        $areaBreakDown = $stats->getAreaBreakDown();
        $groupList = Group::get();
        $systemList = System::get();
        $prepStageMonthlyHrsData = $this->getMonthlyProjectedHrs(1);
        $execStageMonthlyHrsData = $this->getMonthlyProjectedHrs(2);
        //return $groupBreakDownHrs;
        $data = $stats->getProjectGenData(1);

        $project = Project::find(1);
        $project['stats'] = $data;

        return view('rowsys.tracker.tasks.stats.project.index', [
            'project' => $project,
            'generalStats' => $generalStats,
            'groupList' => $groupList,
            'systemList' => $systemList,
            'projectStats' => json_encode($projectStats),
            'groupBreakDown' => $groupBreakDown,
            'groupTaskTypeBreakDown' => $groupTaskTypeBreakDown,
            'systemTaskTypeBreakDown' => $systemTaskTypeBreakDown,
            'groupBreakDownHrs' => $groupBreakDownHrs,
            'areaBreakDown' => $areaBreakDown,
            'prepStageMonthlyHrsData' => $prepStageMonthlyHrsData,
            'execStageMonthlyHrsData' => $execStageMonthlyHrsData,


        ]);
    }

    public function getProjectBarHrsGraphData($id)
    {
        $stats = new TaskStats();
        $group = new Group();
        $graphData = $stats->getProjectBarHrsGraphData($group, 'group', $id);

        return $graphData;
    }


    // Get the projected target Hrs value for the line graph
    public function getMonthlyProjectedHrs($stageId)
    {
        $data = [];

        //SELECT month(prim_date) as month ,SUM(target_val) as value FROM tasks where stage_id = 1  GROUP BY month(prim_date);
        $monthData = \DB::select(\DB::raw("SELECT month(prim_date) as month ,SUM(target_val) as value FROM tasks where stage_id = :stage  GROUP BY month(prim_date)"),
            array(
                'stage' => $stageId,
            ));

        $lastMonthsValue = 0;

        for ($i = 0; $i < sizeof($monthData); $i++) {
            $month = $this->months[$i];
            $array['month'] = $month;

            if (isset($monthData[$i]->value)) {
                $array['value'] = $monthData[$i]->value + $lastMonthsValue;

            } else {
                $array['value'] = 0;
            }
            array_push($data, $array);
            $lastMonthsValue = $lastMonthsValue + $monthData[$i]->value;
        }

        return $data;
    }

    //-----------------------------------------------
    //          AREAS Bar Charts
    //-----------------------------------------------
    public function getAreaBarShell()
    {

        $stats = new TaskStats();
        if (isset($_GET['area'])) {
            $areaId_GET = $_GET['area'];
            $areas = Area::where('id', $areaId_GET)->get();
        } else {
            $areaIds = Task::groupBy('area_id')->lists('area_id');
            $areas = Area::find($areaIds->toArray());;
        }
        $stages = Stage::get();

        foreach ($areas as $area) {
            $data = $stats->getAreaStageGenData($area->id, $stages[0]->id);
            $dataExe = $stats->getAreaStageGenData($area->id, $stages[1]->id);
            $area['stats_gen'] = $data;
            $area['stats_exe'] = $dataExe;
        }

       // return $areas;

        //return $areas[0]['stats_gen'];
        // return $areas;
        return view('rowsys.tracker.tasks.stats.areas.index', [
            'areas' => $areas,
        ]);
    }

    public function getAreaBarPartial($id)
    {
        $stats = new TaskStats();
        $area = Area::find($id);
        $systemTaskTypeBreakDown = $stats->getSystemTaskTypeBreakDown(10);

        return view('rowsys.tracker.tasks.stats.areas.area_bar', [
            'area' => $area,
            'systemTaskTypeBreakDown' => $systemTaskTypeBreakDown,
        ]);
    }

    public function getAreaBarHrsGraphData($id)
    {
        $stats = new TaskStats();
        $area = new Area();
        $graphData = $stats->getAreaBarHrsGraphData($id);

        //$graphData = $stats->getModelBarHrsGraphData($area,'area',$id);
        return $graphData;
    }

    /* -------------------------------------
     *     AREA HOURS BREAK DOWN BY MONTH - BAR
     -------------------------------------*/
    public function getAreaMonthlyBar($area_id)
    {
        $databaseName = env('DB_DATABASE');

        //SELECT month(prim_date) as month ,SUM(target_val) as value FROM tasks where stage_id = 1 and area_id = 2 GROUP BY month(prim_date);
        $prepData = \DB::select(\DB::raw("SELECT concat(year(prim_date),'-',monthname(prim_date)) as date, SUM(target_val) as value ,month(prim_date) as month FROM tasks where stage_id = :stage and area_id = :area GROUP BY DATE_FORMAT(prim_date,'%Y-%m')"),
            array(
                'stage' => 1,
                'area' => $area_id,
            ));
        $exeData = \DB::select(\DB::raw("SELECT concat(year(prim_date),'-',monthname(prim_date)) as date,SUM(target_val) as value ,month(prim_date) as month FROM tasks where stage_id = :stage and area_id = :area GROUP BY DATE_FORMAT(prim_date,'%Y-%m')"),
            array(
                'stage' => 2,
                'area' => $area_id,
            ));

        $prepSize = sizeof($prepData);
        $exeSize = sizeof($exeData);
        $largestDataArraySize = $prepSize;
        $largestDataArray = $prepData;
        if ($exeSize > $prepSize) {
            $largestDataArraySize = $exeSize;
            $largestDataArray = $exeData;
        }
        $data = [];
        if ($prepSize > 0) {
            for ($i = 0; $i < $largestDataArraySize; $i++) {

                $month = $this->months[$largestDataArray[$i]->month];
                $array['m'] = $largestDataArray[$i]->date;

                if (isset($prepData[$i]->value)) {
                    $array['p'] = $prepData[$i]->value;
                } else {
                    $array['p'] = 0;
                }
                if (isset($exeData[$i]->value)) {
                    $array['e'] = $exeData[$i]->value;
                } else {
                    $array['e'] = 0;
                }
                array_push($data, $array);

            }
        }
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $selectedArea = Area::find($area_id);

        return view('rowsys.tracker.tasks.stats.areas.monthly_hours_bar.index', [
            'data' => $data,
            'selectedArea' => $selectedArea,
            'areas' => $areas,
        ]);
    }

    public function combineAreaGraphDataArray($stage1Data,$stage2Data){
        $dataOut = [];
        $sizeOfArray1 = sizeof($stage1Data);
        $sizeOfArray2 = sizeof($stage2Data);

        $largestDataArraySize = $sizeOfArray1;

        if ($sizeOfArray2 > $sizeOfArray1) {
            $largestDataArraySize = $sizeOfArray2;
        }




        return $dataOut;

    }

    /* -------------------------------------------
     *     AREA HOURS BREAK DOWN BY MONTH - BAR
     --------------------------------------------*/
    public function getAreaMonthlyLine($area_id)
    {
        $databaseName = env('DB_DATABASE');

        //SELECT month(prim_date) as month ,SUM(target_val) as value FROM tasks where stage_id = 1 and area_id = 2 GROUP BY month(prim_date);
        $prepData = \DB::select(\DB::raw("SELECT concat(year(last_td),'-',monthname(last_td)) as date, SUM(target_val) as value ,month(prim_date) as month FROM tasks where stage_id = :stage and group_owner_id = :area GROUP BY DATE_FORMAT(prim_date,'%Y-%m')"),
            array(
                'stage' => 1,
                'area' => $area_id,
            ));
        $exeData = \DB::select(\DB::raw("SELECT concat(year(last_td),'-',monthname(last_td)) as date,SUM(target_val) as value ,month(prim_date) as month FROM tasks where stage_id = :stage and group_owner_id = :area GROUP BY DATE_FORMAT(prim_date,'%Y-%m')"),
            array(
                'stage' => 2,
                'area' => $area_id,
            ));

        $prepSize = sizeof($prepData);
        $exeSize = sizeof($exeData);
        $largestDataArraySize = $prepSize;
        $largestDataArray = $prepData;
        if ($exeSize > $prepSize) {
            $largestDataArraySize = $exeSize;
            $largestDataArray = $exeData;
        }
        $data = [];
        $prepTotal = 0;
        $execTotal = 0;
        if ($prepSize > 0) {
            for ($i = 0; $i < $largestDataArraySize; $i++) {

                $month = $this->months[$largestDataArray[$i]->month];
                $array['m'] = $largestDataArray[$i]->date;

                if (isset($prepData[$i]->value)) {
                    $prepTotal = $prepTotal + $prepData[$i]->value;
                    $array['p'] = $prepTotal;
                } else {
                    $array['p'] = $prepTotal;
                }
                if (isset($exeData[$i]->value)) {
                    $execTotal = $execTotal + $exeData[$i]->value;
                    $array['e'] = $execTotal;
                } else {
                    $array['e'] = $execTotal;
                }

                array_push($data, $array);
            }
        }
        $areas = Area::orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        $selectedArea = Area::find($area_id);

        return view('rowsys.tracker.tasks.stats.areas.monthly_hours_bar.index', [
            'data' => $data,
            'selectedArea' => $selectedArea,
            'areas' => $areas,
        ]);
    }

    //-----------------------------------------------
    //         Groups Bar Charts
    //-----------------------------------------------
    public function getGroupBarShell()
    {
        $stats = new TaskStats();
        $firstGroupId = Group::first()->id;
        $area_id = 0;
        $area = null;
        if (isset($_GET['area'])) {
            $area_id = $_GET['area'];
            $groupArray = Task::where('area_id', $area_id)->groupBy('group_id')->lists('group_id');
            $groups = Group::find($groupArray->toArray());
            $area = Area::find($area_id);
            $area_id = $area->id;
        } else {
            $groups = Group::orderBy('description', 'ASC')->get();
        }
        $stages = Stage::get();
        foreach ($groups as $group) {
            $data = $stats->getAreaGroupStageGenData($area->id, $group->id, $stages[0]->id);
            $dataExe = $stats->getAreaGroupStageGenData($area->id, $group->id, $stages[1]->id);
            $group['stats_gen'] = $data;
            $group['stats_exe'] = $dataExe;
        }

        //return $groups;
        return view('rowsys.tracker.tasks.stats.groups.index', [
            'groups' => $groups,
            'area' => $area,
        ]);
    }


    public function getGroupBarHrsGraphData($group_id, $area_id)
    {
        $stats = new TaskStats();
        $group = new Group();
        $graphData = $stats->getGroupBarHrsGraphData($group_id, $area_id);

        return $graphData;
    }


    //-----------------------------------------------
    //         Systems Bar Charts
    //-----------------------------------------------
    public function getSystemBarShell()
    {
        $stats = new TaskStats();
        $firstGroupId = System::first()->id;
        $group_id = 0;
        $group = null;
        if (isset($_GET['group'])) {
            $group_id = $_GET['group'];
            $systemArray = Task::where('group_id', $group_id)->groupBy('system_id')->lists('system_id');
            $systems = System::find($systemArray->toArray())->take(15);
            $group = Group::find($group_id);
            $group_id = $group->id;
        } else {
            $systems = System::orderBy('description', 'ASC')->take(15);
        }
        foreach ($systems as $system) {
            //$data = $stats->getModelGenData($group,'group',$group->id);
            $data = $stats->getSystemGenData($system, 'system', $system->id, $group_id);
            $system['stats'] = $data;
        }
        $groupSystemStats = $stats->getSystemPageStatsForGroup($group_id);

        //return $groupSystemStats['data'];
        // return $systems;
        return view('rowsys.tracker.tasks.stats.systems.index', [
            'systems' => $systems,
            'group' => $group,
            'groupSystemStats' => $groupSystemStats,
        ]);
    }

    public function getSystemBarHrsGraphData($id)
    {
        $stats = new TaskStats();
        $system = new System();
        $graphData = $stats->getSystemBarHrsGraphData($system, 'group', $id, 0);

        return $graphData;
    }


}

<?php

namespace App\Http\Controllers\DesignDocs\Search;

use App\Models\Area;
use App\Models\System;
use App\Models\UserFilter;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function searchForm(Request $request){

        //--------------------------------------
        // GET User Filters to For DropDowns - Saved last Search
        //--------------------------------------
        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $searchText = $userFilter->search_text;
        $selectedAreasArray = array_map('intval', explode(',', $userFilter->search_area_ids));
        $selectedSystemsArray = array_map('intval', explode(',', $userFilter->search_system_ids));
        $systems = System::get();
        $areas = Area::get();


        return view('rowsys.design_docs.search.form', [
            'systems' => $systems,
            'areas' => $areas,
            'searchText' => $searchText,
            'selectedAreasArray' => $selectedAreasArray,
            'selectedSystemsArray' => $selectedSystemsArray,
        ]);


    }


}

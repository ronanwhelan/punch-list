<?php

namespace App\Http\Controllers\DesignDocs\Docs;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DocsController extends Controller
{

    /* ----------------------------------------------------
    *                  DASHBOARD
     ----------------------------------------------------*/
    public function dashboard(){

        return view('rowsys.design_docs.dashboard.index', [
            //'items' => $items,
        ]);
    }


    /* ----------------------------------------------------
    *                    READ
    ----------------------------------------------------*/
    public function documentTable(){

        return view('rowsys.design_docs.documents.read.table.index', [
            //'items' => $items,
        ]);
    }


    /* ----------------------------------------------------
     *                  CREATE
      ----------------------------------------------------*/
    public function createView(){

        return view('rowsys.design_docs.documents.create.index', [
            //'items' => $items,
        ]);
    }




    /* ----------------------------------------------------
     *                  UPDATE
      ----------------------------------------------------*/

    public function documentDetails($id){
        return view('rowsys.design_docs.documents.update.form', [
            //'items' => $items,
        ]);

    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Area;
use App\Models\Forum;
use App\Models\Group;
use App\Models\Role;
use App\Models\Stage;
use App\Models\Task;
use App\Models\TaskType;
use App\Models\UserFilter;
use App\Rowsys\Tracker\Classes\Search;
use Illuminate\Http\Request;
use App\Rowsys\Tracker\Classes\TaskStats;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Session\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    //App Admin Dashboard

    /**
     * @param Request $request
     * @Class User
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adminDashboard(Request $request)
    {

        $areasList = Area::orderBy('name', 'ASC')->get();
        $stagesList = Stage::orderBy('name', 'ASC')->get();
        $groupsList = Group::orderBy('name', 'ASC')->get();
        $typesList = TaskType::orderBy('name', 'ASC')->get();

        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $selectedAreasArray = array_map('intval', explode(',', $userFilter->filter_area_ids));
        $selectedGroupsArray = array_map('intval', explode(',', $userFilter->filter_group_ids));
        $selectedTypesArray = array_map('intval', explode(',', $userFilter->filter_type_ids));
        $selectedStagesArray = array_map('intval', explode(',', $userFilter->filter_stage_ids));
        $selectedRolesArray = array_map('intval', explode(',', $userFilter->filter_role_ids));

        $filterOn = 0;
        if($userFilter->filter_area_ids !== '' || $userFilter->filter_group_ids !== ''
            || $userFilter->filter_type_ids !== '' || $userFilter->filter_stage_ids !== ''
            || $userFilter->filter_role_ids !== ''){
            $filterOn = 1;
        }

        $searchObj = new Search($request);
        $searchObj->buildAndReturnMainQuery();
        $searchObj->addUserFilter();
        $query = $searchObj->getQuery();

        $statsObj = new TaskStats();
        $stepPhaseQuery = clone $query;
        $userStats = $statsObj->getUserDashBoardStatistics($query);
        $stepStats = $statsObj->getTaskStepStats($stepPhaseQuery);
        $activePercentage = 0;
        if($userStats['stats']['totalNotDone'] > 0){
            $activePercentage = ceil(($stepStats['activeCount']/$userStats['stats']['totalNotDone'])*100);
            if($activePercentage < 1){
                $activePercentage = '<1';
            }
        }

        $roleList = Role::whereNotIn('name', array('N/A'))->orderBy('name', 'ASC')->get();

        return view('rowsys._app.dashboards.user', [
            //return view('tasks.read.tables.index', [
            'stats' => $userStats['stats'],
            'groups' => $userStats['groups'],
            'pastDueByGroup' => $userStats['stats']['pastDueByGroup'],
            //'selectedAreasArray' => implode(",",$selectedAreasArray),
            'selectedAreasArray' => $selectedAreasArray,
            'selectedGroupsArray' => $selectedGroupsArray,
            'selectedTypesArray' => $selectedTypesArray,
            'selectedStagesArray' => $selectedStagesArray,
            'selectedRolesArray' => $selectedRolesArray,
            'areaList' => $areasList,
            'groupList' => $groupsList,
            'stageList' => $stagesList,
            'typesList' => $typesList,
            'roleList' => $roleList,
            'user' => $user,
            'stepStats' => $stepStats,
            'activePercentage' => $activePercentage,
            'filterOn' => $filterOn,
        ]);

    }

    public function stepStatsForDashBoard(Request $request){

        $searchObj = new Search($request);
        $searchObj->addUserFilter();
        $searchObj->buildAndReturnMainQuery();
        $query = $searchObj->getQuery();

        $statsObj = new TaskStats();
        $stepPhaseQuery = clone $query;
        $stepStats = $statsObj->getStepPhaseStats($stepPhaseQuery);

        return view('rowsys.tracker.tasks.stats.step_status.step_owners', [
            'stepStats' => $stepStats,
        ]);

    }


    public function updateUserFilters(Request $request)
    {
        $user = \Auth::user();
        $userFilterExists = UserFilter::where('user_id', $user->id)->exists();
        if (!$userFilterExists) {
            $userFilter = new UserFilter();
            $userFilter->user_id = $user->id;
            $userFilter->save();
        }
        if ($userFilterExists) {
            $userFilter = UserFilter::where('user_id', $user->id)->first();
            //Area
            $user->area = '';
            $userFilter->filter_area_ids = '';
            if ($request->area_filter !== null && $request->area_filter !== "") {
                $user->area = implode(',', $request->area_filter);
                $userFilter->filter_area_ids = implode(',', $request->area_filter);
            }
            //Group
            $user->group = '';
            $userFilter->filter_group_ids = '';
            if ($request->group_filter !== null && $request->group_filter !== "") {
                $user->group = implode(',', $request->group_filter);
                $userFilter->filter_group_ids = implode(',', $request->group_filter);
            }
            //Type
            $user->type = '';
            $userFilter->filter_type_ids = '';
            if ($request->type_filter !== null && $request->type_filter !== "") {
                $user->type = implode(',', $request->type_filter);
                $userFilter->filter_type_ids = implode(',', $request->type_filter);
            }
            //Stage
            $user->stage = '';
            $userFilter->filter_stage_ids = '';
            if ($request->stage_filter !== null && $request->stage_filter !== "") {
                $user->stage = implode(',', $request->stage_filter);
                $userFilter->filter_stage_ids = implode(',', $request->stage_filter);
            }

            //Role/Teams
            $userFilter->filter_role_ids = '';
            if ($request->role_filter !== null && $request->role_filter !== "") {
                $userFilter->filter_role_ids = implode(',', $request->role_filter);
            }


            $userFilter->save();
        }

        $user->save();

        return \Redirect::back()->with('message', 'Filters Updated');
    }

    public function addForum(Request $request)
    {

        $user = \Auth::user()->name;
        $forum = new Forum();
        $forum->topic = $request->heading;
        $forum->description = $request->description;
        $forum->area = $request->area;
        $forum->type = $request->optionsType;
        $forum->device = $request->optionsDevice;
        $forum->platform = $request->brand;
        $forum->status = 0;
        $forum->user = $user;

        $forum->save();

        $message = 'Thank you for your feed back. Topic added to forum';

        return \Redirect::back()->with('message', $message)->withInput();
        //return redirect('form')->withInput($request->except('password'));
        //return 'im here';

    }

}

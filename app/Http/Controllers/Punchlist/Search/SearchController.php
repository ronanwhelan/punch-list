<?php

namespace App\Http\Controllers\Punchlist\Search;

use App\Models\Company;
use App\Models\Project\Floor;
use App\Models\Project\Room;
use App\Models\PunchList\Action;
use App\Models\PunchList\Category;
use App\Models\PunchList\Item;
use App\Models\PunchList\Priority;
use App\Models\Role;
use App\Models\User;
use App\Models\UserFilter;
use App\Rowsys\PunchList\Stats;
use App\Rowsys\PunchList\Search;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\System;
use App\Models\PunchList\Group;
use App\Models\PunchList\Type;
use Illuminate\Database\Eloquent\Builder;

class SearchController extends Controller
{
    //
    public function getSearchForm(Request $request)
    {
        //--------------------------------------
        // GET User Filters to For DropDowns - Saved last Search
        //--------------------------------------
        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        if (isset($userFilter) && false) {
            $searchText = $userFilter->search_text;
            $selectedAreasArray = array_map('intval', explode(',', $userFilter->item_search_area_ids));
            $selectedSystemsArray = array_map('intval', explode(',', $userFilter->item_search_system_ids));
            $selectedGroupsArray = array_map('intval', explode(',', $userFilter->item_search_group_ids));
            $selectedTypesArray = array_map('intval', explode(',', $userFilter->item_search_type_ids));
        } else {
            $searchText = '';
            $selectedAreasArray = [];
            $selectedSystemsArray = [];
            $selectedGroupsArray = [];
            $selectedTypesArray = [];
        }


        //--------------------------------------
        // Search Query
        //--------------------------------------
        $search = new Search($request);
        $search->addUserFilterToRequest();
        $search->buildAndReturnMainQuery();
        $search->addActivationStatus();
        // $search->associatedItems();
        //$search->orPublic();
        //$search->addPrivacy();
        $query = $search->getQuery();

        $statsObj = new Stats();
        $stats = $statsObj->getSearchStats($query);

        // $project_id = $request->session()->get('project_id');
        //todo clean this up - no need for the data here
        $project_id = 1;
        $areas = Area::where('project_id', '=', $project_id)->orderBy('name', 'ASC')->whereNotIn('name', array('N/A'))->get();
        // $systems = System::where('project_id', '=', $project_id)->orderBy('tag', 'ASC')->whereNotIn('tag', array('N/A'))->get();
        $systemIds = Item::groupBy('system_id')->lists('system_id')->toArray();
        $systems = System::whereIn('id', $systemIds)->get();
        $groups = Group::where('project_id', '=', $project_id)->orderBy('name', 'ASC')->get();
        $types = Type::where('project_id', '=', $project_id)->orderBy('name', 'ASC')->get();
        $categories = Category::orderBy('name', 'ASC')->get();
        $priorities = Priority::orderBy('name', 'ASC')->get();

        //Roles
        $ownerRoleIds = Item::groupBy('raised_by_role_id')->lists('raised_by_role_id');
        $ownerRoles = Role::whereIn('id', $ownerRoleIds)->get();

        $resRole_ids = Item::groupBy('res_role_id')->lists('res_role_id');
        $resRoles = Role::whereIn('id', $resRole_ids)->get();

        $assignedRoleIds = Action::groupBy('assigned_role_id')->lists('assigned_role_id');
        $assignedRoles = Role::whereIn('id', $assignedRoleIds)->get();

        /*
         *      //Company - old
                $userCompanyId = \Auth::user()->company_id;
                $ownerCompanyIds = Item::where('raised_by_company_id',$userCompanyId)
                    ->orWhere('res_company_id',$userCompanyId)
                    ->groupBy('raised_by_company_id')
                    ->lists('raised_by_company_id');
                $ownerCompanies = Company::whereIn('id', $ownerCompanyIds)->get();
                $resCompany_ids = Item::groupBy('res_company_id')
                    ->lists('res_company_id');
                $resCompanies = Company::whereIn('id', $resCompany_ids)->get();
                $assignedCompanyIds = Action::groupBy('assigned_company_id')->lists('assigned_company_id');
                $assignedCompanies = Company::whereIn('id', $assignedCompanyIds)->get();*/


        return view('rowsys.punchlist.search.form.form', [
            //return view('tasks.read.tables.index', [
            'areas' => $areas,
            'systems' => $systems,
            'groups' => $groups,
            'types' => $types,
            'categories' => $categories,
            'priorities' => $priorities,

            'ownerRoles' => $ownerRoles,
            'resRoles' => $resRoles,
            'assignedRoles' => $assignedRoles,

            //'resCompanies' => $resCompanies,
            // 'ownerCompanies' => $ownerCompanies,
            //'assignedCompanies' => $assignedCompanies,

            'stats' => $stats,
            'searchText' => $searchText,
            'selectedAreasArray' => $selectedAreasArray,
            'selectedSystemsArray' => $selectedSystemsArray,
            'selectedGroupsArray' => $selectedGroupsArray,
            'selectedTypesArray' => $selectedTypesArray,

        ]);

    }

    //
    public function getSearchStats(Request $request)
    {
        $search = new Search($request);
        $search->buildAndReturnMainQuery();
        $search->addCompleteFilterQuery();
        $search->addTimeFilterQuery();
        //$search->addForget();
        $search->addRole();
        $search->addPriority();
        $search->addActivationStatus();
        //$search->addCompany();
        $search->addOrderBy();
        $query = $search->getQuery();
        $statsObj = new Stats();
        $searchStats = $statsObj->getSearchStats($query);

        $search_has_owner = $request->has('search_raised_by');


        $data['stats'] = $searchStats;
        $data['request'] = 'ff' . $search_has_owner;


        return $data;
    }

    //
    public function getFilteredSystemsByArea(Request $request)
    {
        $systems = System::get();
        $data = $_GET['data'];
        if (isset($_GET['data']) && $data !== '') {
            $isArray = is_array($data);
            if ($isArray) {
                $systems = System::whereIn('area_id', $data)->get();
            } else {
                $systems = System::where('area_id', $data)->get();
            }
        }

        return $systems;
    }

    public function getFilteredGroupsByArea()
    {
        $groups = Group::get();
        $data = $_GET['data'];

        if ($data !== '') {
            $arrayIds = [];
            for ($i = 0; $i < sizeof($data); $i++) {
                $arrayIds[$i] = (int)$data[$i];
            }
            $groupIds = Task::whereIn('area_id', $arrayIds)
                ->groupBy('group_id')
                ->lists('group_id');
            $groups = Group::whereIn('id', $groupIds)->get();
        }

        return $groups;
    }

    public function getFilteredTypesByGroup()
    {
        $types = Type::get();
        $data = $_GET['data'];

        $types = Type::where('group_id', $data)->get();

        if ($data === '') {
            $types = Type::get();
        }

        return $types;
    }


    public function getFilteredTestSpecsByType()
    {
        $data = $_GET['data'];
        if ($data !== '') {
            $testSpecs = TaskTestSpec::where('task_type_id', (int)$data)->get();
        } else {
            $testSpecs = TaskTestSpec::get();
        }

        return $testSpecs;
    }

    public function getNumberForAutoComplete($tag)
    {
        $searchNumber = '%' . $tag . '%';
        $tasks = Item::take(100)
            ->where('number', 'like', $searchNumber)
            ->orWhere('description', 'like', $searchNumber)
            ->get();

        $results = array();
        foreach ($tasks as $task) {
            $results[] = ['id' => $task->id, 'value' => $task->description];
        }

        return $results;
    }


    //--------------------------------------
    // GET User Filters for dropdown - by company
    //--------------------------------------
    public function getFilteredUsersByCompany()
    {
        $data = $_GET['data'];
        $users = User::where('company_id', $data)->get();
        if ($data === '') {
            $users = User::get();
        }

        return $users;
    }


    //--------------------------------------
    // GET Filtered Rooms By Floor
    //--------------------------------------
    public function getFilteredRoomsByFloor()
    {
        $data = $_GET['data'];

        if ($data === '') {
            $rooms = Room::get();
        } else {
            $floorId = Floor::where('name', $data)->first()->id;
            $rooms = Room::where('floor_id', $floorId)->get();
        }

        return $rooms;
    }


    //--------------------------------------
    // Find Similar Items already  raised
    //--------------------------------------
    public function getListOfSimilarItems()
    {
        $systemId = $_GET['systemId'];
        $roomText = '%' . $_GET['roomText'] . '%';
        $typeId = $_GET['typeId'];
        $items = Item::where('system_id', $systemId)
            ->where('type_id', $typeId)
            // ->where('room', 'like', $roomText)
            //->where('complete', 0)
            ->get();

        //$items = Item::get();

        return $items;
    }

}

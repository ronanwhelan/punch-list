<?php

namespace App\Http\Controllers\Punchlist\Sync;

use App\Models\Company;
use App\Models\Project\Floor;
use App\Models\Project\Room;
use App\Models\PunchList\Action;
use App\Models\PunchList\Category;
use App\Models\PunchList\Item;
use App\Models\PunchList\Priority;
use App\Models\Role;
use App\Models\User;
use App\Models\UserFilter;
use App\Rowsys\PunchList\LinkedDates;
use App\Rowsys\PunchList\Stats;
use App\Rowsys\PunchList\Search;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\System;
use App\Models\PunchList\Group;
use App\Models\PunchList\Type;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class SyncDataController extends Controller
{


    public function index(){

       //return 'im in Index';

        /*
         *  DROP TABLE IF EXISTS punchlist.team_user;
            DROP TABLE IF EXISTS punchlist.teams;
            CREATE TABLE punchlist.teams LIKE default_4.teams;
            INSERT INTO punchlist.teams SELECT * FROM default_4.teams GROUP BY ID;
            CREATE TABLE punchlist.team_user LIKE default_4.team_user;
            INSERT INTO punchlist.team_user SELECT * FROM default_4.team_user;
         */
        $mainTableName = env('DB_DATABASE_TWO', 'figaro');
        $punchListTableName = 'punchlist';
        DB::statement("DROP TABLE IF EXISTS $punchListTableName.team_user");
        DB::statement("DROP TABLE IF EXISTS $punchListTableName.teams");
        DB::statement("CREATE TABLE $punchListTableName.teams LIKE $mainTableName.teams");
        DB::statement("INSERT INTO $punchListTableName.teams SELECT * FROM $mainTableName.teams GROUP BY ID");
        DB::statement("CREATE TABLE $punchListTableName.team_user LIKE $mainTableName.team_user");
        DB::statement("INSERT INTO $punchListTableName.team_user SELECT * FROM $mainTableName.team_user");

        /*
            DROP TABLE IF EXISTS punchlist.role_user;
            DROP TABLE IF EXISTS punchlist.roles;
            CREATE TABLE punchlist.roles LIKE default_4.teams;
            INSERT INTO punchlist.roles SELECT * FROM default_4.teams;
         */

        DB::statement("DROP TABLE IF EXISTS $punchListTableName.role_user");
        DB::statement("DROP TABLE IF EXISTS $punchListTableName.roles");
        DB::statement("CREATE TABLE $punchListTableName.roles LIKE $mainTableName.teams");
        DB::statement("INSERT INTO $punchListTableName.roles SELECT * FROM  $mainTableName.teams");
        /*
            CREATE TABLE punchlist.role_user LIKE punchlist.team_user;
            INSERT INTO punchlist.role_user SELECT * FROM punchlist.team_user;
            ALTER TABLE punchlist.role_user CHANGE team_id role_id INT(10) NOT NULL;
            ALTER TABLE punchlist.role_user ADD COLUMN `created_at` DATETIME NOT NULL;
            ALTER TABLE punchlist.role_user ADD COLUMN `updated_at` DATETIME NOT NULL;
            UPDATE punchlist.role_user SET created_at = NOW();
            UPDATE punchlist.role_user SET updated_at = NOW();
         */
        DB::statement("CREATE TABLE $punchListTableName.role_user LIKE $punchListTableName.team_user");
        DB::statement("INSERT INTO $punchListTableName.role_user SELECT * FROM $punchListTableName.team_user");
        DB::statement("ALTER TABLE $punchListTableName.role_user CHANGE team_id role_id INT(10) NOT NULL");
        DB::statement("ALTER TABLE $punchListTableName.role_user ADD COLUMN `created_at` DATETIME NOT NULL");
        DB::statement("ALTER TABLE $punchListTableName.role_user ADD COLUMN `updated_at` DATETIME NOT NULL");
        DB::statement("UPDATE $punchListTableName.role_user SET created_at = NOW()");
        DB::statement("UPDATE $punchListTableName.role_user SET updated_at = NOW()");

        return back();

        return 'im done';

    }
    public function syncDataFromMainApp(){

    }


    /**
     *  Sync the Linked Activity to the Milestone for each system
     *
     * @param $activity
     * @return string
     */
    public function syncScheduleLinks($activity){

         LinkedDates::syncLinks($activity);

        return back();

    }

    /**
     *  Sync the Linked Activity to the Milestone for each system
     *
     * @return string
     */
    public function toolsView(){

        return view('rowsys.punchlist.sync.index', [

        ]);

    }




}

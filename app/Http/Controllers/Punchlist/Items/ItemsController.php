<?php

namespace App\Http\Controllers\Punchlist\Items;

use App\Jobs\Punchlist\CompileItems;
use App\Models\Activity;
use App\Models\Area;
use App\Models\Component;
use App\Models\MainAppTask;
use App\Models\Milestone;
use App\Models\Project\Building;
use App\Models\Project\Floor;
use App\Models\Project\Room;
use App\Models\Project\SystemMilestone;
use App\Models\PunchList\Category;
use App\Models\PunchList\Priority;
use App\Models\PunchList\SystemScheduleDates;
use App\Models\Role;
use App\Models\ScheduleDate;
use App\Models\System;
use App\Models\SystemToMilestoneLink;
use App\Models\Task;
use App\Models\Team;
use App\Models\User as AppUser;
use App\Models\Company;
use App\Models\PunchList\Action;
use App\Models\PunchList\Attachment;
use App\Models\PunchList\Group;
use App\Models\PunchList\Item;
use App\Models\PunchList\Type;
use App\Models\UserFilter;
use App\Rowsys\PunchList\Compile;
use App\Rowsys\PunchList\NewItemDates;
use App\Rowsys\PunchList\Search;
use App\Rowsys\PunchList\Stats;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Rowsys\PunchList\Attachment as fileAttachment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ItemsController extends Controller
{

    /**
     * Display the Items View Table - table View
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        //--------------------------------------
        // Last Search
        //--------------------------------------
        $search = new Search($request);
        //$search->saveLastSearch();
        //Search NOT active i.e Not coming from search modal
        if (!$request->has('search_active')) {
            $search->addUserFilterToRequest();
            $search->buildAndReturnMainQuery();
            $search->checkForAnyAppLinks();
            //$search->associatedItems();
            $search->addForget();
            //$search->orPublic();
            $search->addCompleteFilterQuery();
            //$search->addPrivacy();
            $query = $search->getQuery();
            $statQuery = clone $query;//to get stats
            $items = $query->orderBy('due_date', 'ASC')
                ->where('complete', 0)
                //->where('imported', 1)
                ->take(200)
                //->with('area', 'group', 'system', 'taskType', 'stage')
                ->get();
        } else {
            $search->buildAndReturnMainQuery();
            $search->addCompleteFilterQuery();
            $search->addTimeFilterQuery();
            $search->addCompleteFilterQuery();
            $search->addForget();
            $search->addRole();
            $search->addActivationStatus();
            //$search->addCompany();
            $search->addOrderBy();
            $query = $search->getQuery();
            // return $search->getQueryToSql();
            $statQuery = clone $query;
            $items = $query->with('area', 'group', 'system')
                //->take(200)
                ->get();
        }

        $view = 'rowsys.punchlist.items.read.tables.index1';

        return view($view, [
            'items' => $items,
            //'stats' => $stats,
        ]);
    }

    /**
     * Display the Items View Table - table View
     *
     * @return Response
     */
    public function dashboard(Request $request)
    {
        $user = \Auth::user();
        $companyId = $user->company_id;
        $companyName = Company::find($companyId)->name;
        $items = Item::where('res_company_id', $companyId)->get();
        $openCount = Item::where('res_company_id', $companyId)->count();
        $today = Carbon::now();
        $overDueCount = Item::where('res_company_id', $companyId)->where('due_date', '<', $today)->count();
        $twoWeeksFromToday = $today->addWeeks(2);
        $dueWithinTwoWeeksCount = Item::where('res_company_id', $companyId)->where('due_date', '<', $twoWeeksFromToday)->count();
        $view = 'rowsys.punchlist.dashboard.index';

        $searchObj = new Search($request);
        $searchObj->addUserFilterToRequest();
        $searchObj->buildAndReturnMainQuery();
        //$searchObj->addCompany();
        $query = $searchObj->getQuery();

        $statsObj = new Stats();
        $dashBoardStats = $statsObj->dashBoardStatistics();
        $userRoles = $user->roles->lists('name');


        return view($view, [
            'items' => $items,
            'companyName' => $companyName,
            'openCount' => $openCount,
            'overDueCount' => $overDueCount,
            'dueWithinTwoWeeksCount' => $dueWithinTwoWeeksCount,
            'dashBoardStats' => $dashBoardStats,
            'userRoles' => $userRoles,
        ]);
    }


    public function itemsTable()
    {

        $items = Item::get();

        return view('rowsys.punchlist.items.read.tables.index', [
            'items' => $items,
        ]);
    }


    public function getCreateIndexMobile()
    { //Dashboard

        $areas = Area::get();
        $systems = System::where('id', '>', 1)->get();
        $groups = Group::get();
        $types = Type::get();
        $teams = Team::get();
        $company_ids = User::groupBy('company_id')->lists('company_id');;
        $companies = Company::whereIn('id', $company_ids)->get();
        $roles = Role::get();
        $users = User::get();

        $priorities = Priority::get();
        $categories = Category::get();
        $buildings = Building::get();
        $floors = Floor::get();
        $rooms = Room::get();
        $components = Component::get()->take(0);

        return view('rowsys.punchlist.items.create.index_mobile', [
            'areas' => $areas,
            'systems' => $systems,
            'groups' => $groups,
            'types' => $types,
            'companies' => $companies,
            'roles' => $roles,
            'users' => $users,
            'priorities' => $priorities,
            'categories' => $categories,
            'buildings' => $buildings,
            'floors' => $floors,
            'rooms' => $rooms,
            'teams' => $teams,
            'components' => $components,
        ]);
    }

    public function createIndex()
    {

        $areas = Area::get();
        $systems = System::get();
        $groups = Group::get();
        $types = Type::get();

        return view('rowsys.punchlist.items.create.index', [
            //'tasks' => $tasks,
        ]);

    }

    public function validateFormData(Request $request)
    {
        $this->validate($request, [
            'class' => 'required',
            'system' => 'required|filled',
            'description' => 'required|max:255',
            'type' => 'required',
            'category' => 'required',
            'due_date' => 'required|date',
            'lag_days' => 'required|digits_between:0,2|integer|numeric|min:0',
            'responsible_role' => 'required',
            'attention_to' => 'required',
        ]);

        return 'pass';
    }

    public function create(Request $request)
    { //Dashboard

        //return $request->description;
        $this->validate($request, [
            'description' => 'required|max:255',
        ]);

        //---------------------------------
        //         ITEM
        //---------------------------------
        $item = new Item();

        //Genre
        $category = Category::find((int)$request->class);

        //Number
        $count = Item::where('category_id', $category->id)->count();
        $system = System::find((int)$request->system);
        $type = Type::find((int)$request->type);
        $group = Group::find($type->group_id);


        $item->number = $system->name . '.' . $category->short_name . '-' . ($count + 1);
        $item->increment_number = ($count + 1);
        $item->description = $group->name . ' ' . $type->name . ' on ' . $system->description . ' (' . $request->system_extra_info . ')';

        //Details
        $item->finding = $request->description;//finding can not be used as the validation will sya finding not description
        $item->action = $request->action;
        $item->note = $request->note;

        //Category
        $item->category_id = $category->id;//The Item Genre or Class, what type it is

        //Logged In User
        $authUser = \Auth::user();
        //Owner
        $item->raised_by_user_id = $authUser->id;
        $item->raised_by_company_id = $authUser->company_id;

        if (count($authUser->roles) > 0) {
            $item->raised_by_role_id = (int)$authUser->roles[0]->id;
        } else {
            $item->raised_by_role_id = 1;
        }
        //$item->raised_by_role_id = $request->responsible_role;

        $item->raised_date = Carbon::now();
        $item->closed_date = Carbon::createFromDate(2000, 01, 01);
        $item->closed_by_id = $authUser->id;//placeholder for when the item gets closed will be updated
        $item->own_status = 1;
        $item->owner_updated_by_id = $authUser->id;
        $item->owner_updated_date = Carbon::now();

        //Responsible Party
        $item->res_company_id = $authUser->company_id;
        $item->res_user_id = $request->attention_to;
        $item->res_role_id = $request->responsible_role;

        $item->res_cause = '';
        $item->res_action = '';//$request->action;//'';
        $item->res_note = '';
        $item->res_status = 1;
        $item->res_complete_by_id = 0;
        $item->res_complete_date = Carbon::createFromDate(2000, 01, 01);

        $item->res_updated_by_id = $authUser->id;
        $item->res_updated_date = Carbon::now();

        //Assigned
        $item->assigned_company_id = 0;

        //Location
        $item->area_id = System::find($request->system)->area_id;
        $item->system_id = $request->system;

        $component = null;
        if (isset($request->component) && $request->component != "") {
            $component = $request->component;
        }
        $item->component_id = $component;

        $item->equipment_tag = $request->system_extra_info;
        $item->system_extra_info = $request->system_extra_info;
        $item->system_level = '';

        $item->building = $request->building;
        $floors = $request->floors;
        for ($i = 0; $i < sizeof($floors); $i++) {
            $item->floor = $item->floor . '  ' . $floors[$i];
        }

        $rooms = $request->rooms;
        for ($i = 0; $i < sizeof($rooms); $i++) {
            $item->room = $item->room . '  ' . $rooms[$i];
        }
        $item->other_location = $request->other_location;//'';// $request->other_location;

        //Referenced Dos - e.g Drawing Number
        $item->drawing_reference = '';//$request->drawing_reference;
        $item->extra_info = '';//$request->extra_info;

        //Group & Type
        $item->group_id = $group->id;//Type::find($request->type)->group_id;
        $item->type_id = $type->id;










        //Priority, Lag & Target Date 1 = A, B = 2 , C = 3
        $priority = (int)$request->category;
        $selectedMilestone = 0;
        $dueDate = Carbon::now();
        $columnLink = '';
        $columnChoice = '';
        $columnLag = '';
        $lagDays = $request->lag_days;
        switch ($priority) {
            case 1:
                $selectedMilestone = $request->category_a_choice;
                $columnLink = 'm' . $request->category_a_choice . '_schedule_link_number';
                $columnChoice = 'm' . $request->category_a_choice . '_schedule_choice';
                $columnLag = 'm' . $request->category_a_choice . '_lag';
                break;
            case 2:
                $selectedMilestone = $request->category_b_choice;
                $columnLink = 'm' . $request->category_b_choice . '_schedule_link_number';
                $columnChoice = 'm' . $request->category_b_choice . '_schedule_choice';
                $columnLag = 'm' . $request->category_b_choice . '_lag';
                $columnLink = '';
                break;
            case 3:
                if ((int)$request->category_b_choice > 0) {
                    $selectedMilestone = $request->category_b_choice;
                    $columnLink = 'm' . $request->category_b_choice . '_schedule_link_number';
                    $columnChoice = 'm' . $request->category_b_choice . '_schedule_choice';
                    $columnLag = 'm' . $request->category_b_choice . '_lag';
                } else {
                    $selectedMilestone = 0;
                    $dueDate = $request->due_date;
                    $lagDays = 0;
                }
                break;
            default:
        }

        $phaseRecordedIn = '';
        //Get the Schedule date
        // we have the system id, and the milestone step to look at
        if ($columnLink !== '') {

            $systemScheduleMilestoneDates = SystemScheduleDates::where('system_id', $request->system)->first();
            $milestoneLink = $systemScheduleMilestoneDates->$columnLink;
            $milestoneDate = ScheduleDate::where('name', $milestoneLink)->first();
            if (strpos($columnChoice, 'start') == true) {
                $linkDate = $milestoneDate->start_date;
            } else {
                $linkDate = $milestoneDate->finish_date;
            }

            $linkDate = Carbon::createFromFormat('Y-m-d H:m:s', $linkDate);

            // $linkDate = Carbon::createFromDate('Y:m:d H:m:s',$linkDate);
            $linkDate = $linkDate->subDays(0);

            $item->linked_milestone = $selectedMilestone;

            $phaseRecoredIn = $request->phase_recorded_in;
        }

        $item->priority_id = $priority;

        //due dates
       // $linkDate = Carbon::now()->addWeeks(10);
        if ($request->cat_c_due_date === '') {

            if($priority === 2){
                $dueDate = Carbon::createFromFormat('d-m-Y', $request->due_date);
            }else{
                $dueDate = $linkDate;
            }
        }
        $item->lag_days = $lagDays;
        $item->due_date = $dueDate;
        $item->original_due_date = $dueDate;
        $item->res_target_date = $dueDate;










        $item->phase_recorded_in = $phaseRecordedIn;

        //Complete
        $item->complete = 0;
        $item->percent_complete = 0;
        $item->completion_note = '';

        //Privacy (1 = public)
        $item->privacy = 1;
        //Project
        $item->project_id = 1;

        //Project
        $item->urgent = 0;
        if ($request->urgent === 'on') {
            $item->urgent = 1;
        }

        $item->last_updated_by_id = \Auth::user()->id;
        $item->last_updated_at = Carbon::now();

        //Save Item
        $item->save();
        //---------------------------------
        //         ATTACHMENT
        //---------------------------------
        //File
        $requestHasFile = $request->hasFile('attachment_file1');
        if ($requestHasFile) {
            $file = $request->file('attachment_file1');
            $this->addAttachment($file, $request->attachment_tag, $item->id, $item->number);

        }

        /*        $files = Input::file('images');
                // Making counting of uploaded images
                $file_count = count($files);
                // start count how many uploaded
                $uploadcount = 0;
                foreach($files as $file) {
                    $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                    $validator = Validator::make(array('file'=> $file), $rules);
                    if($validator->passes()){
                        $destinationPath = 'uploads';
                        $filename = $file->getClientOriginalName();
                        $upload_success = $file->move($destinationPath, $filename);
                        $uploadcount ++;
                    }
                }
                if($uploadcount == $file_count){
                    Session::flash('success', 'Upload successfully');
                    return Redirect::to('upload');
                }
                else {
                    return Redirect::to('upload')->withInput()->withErrors($validator);
                }*/


        /*        $user = Item::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'confirmed' => $confirmed,
            'role' => $request['role'],
        ]);*/


        return redirect('items/create/mobile')->with('system', $request->system);

    }

    /*
    |--------------------------------------------------------------------------
    | Add Attachment for Item
    |--------------------------------------------------------------------------
    */
    public function addAttachment($file, $tag, $item_id, $item_number)
    {
        $attachmentObj = new fileAttachment();
        $fileName = $item_number . '-' . time();
        //check if the upload is a photo
        $fileData = $attachmentObj->uploadPhotoAttachment($file, $fileName, $item_number);
        //Update DB with Attachment Details
        $attachment = new Attachment();
        $attachment->item_id = $item_id;
        $attachment->path = $fileData[0];
        $attachment->file_type = $fileData[1];
        $attachment->folder = $fileData[2];
        $attachment->tag = $tag;
        $attachment->uploaded_by_id = \Auth::user()->id;
        $attachment->save();
    }

    /*
    |--------------------------------------------------------------------------
    | Add File over AJAX - to allow a file to upload within a form
    | First the FIle is uploaded, on success the form is submitted.
    |--------------------------------------------------------------------------
    */
    public function addIsolatedAttachment(Request $request, $id)
    {

        $this->validate($request, [
            'attachment_file' => 'required',
        ]);
        $item = Item::find($id);
        //File
        $requestHasFile = $request->hasFile('attachment_file');
        if ($requestHasFile) {
            $file = $request->file('attachment_file');
            $this->addAttachment($file, $request->attachment_tag, $item->id, $item->number);
        }
        $message = 'File Uploaded';

        return \Redirect::back()->withInput()->with(array('file_uploaded' => $message));

    }


    /*
    |--------------------------------------------------------------------------
    | Get the Items details Info
    |--------------------------------------------------------------------------
    */
    public function getItemDetails($id)
    {
        $areas = Area::get();
        $systems = Area::get();
        $item = Item::find($id);
        $users = User::get();
        //$company_ids = User::groupBy('company_id')->lists('company_id');;
        //$companies = Company::whereIn('id',$company_ids)->get();
        $companies = Company::get();
        $roles = Role::get();
        //figaro-testing/project-1/punchlist/PLI-4/img
        //figaro-testing/project-1/punchlist/$item->name
        $directory = '/project-1/punchlist/' . trim($item->number) . '/img';
        $imgList = Storage::allFiles($directory);

        //https://s3-ap-southeast-1.amazonaws.com/abbvie-asia-bucket/uploads/2016-04-26-photo.JPG
        $awsLink = 'https://s3-ap-southeast-1.amazonaws.com/figaro-testing/';
        $statusArray = [1 => 'Open', 2 => 'In progress', 3 => 'Waiting for others', 4 => 'Complete', 5 => 'Rejected'];
        $ownStatusArray = [1 => 'Open', 2 => 'Closed', 3 => 'Forget',];
        $action = Action::where('item_id', $id)->first();


        //Update Rights
        // is the user part of the owner,res, or assign role

        $userRoles = Auth::user()->roles->lists('id')->toArray();
        $accessLevel = 0;
        //if raised_by_role_id is in array OR the rasied id is the same as the Auth user Level = 3
        if (in_array($item->assigned_role_id, $userRoles)) {
            $accessLevel = 1;//echo "Access level 1";
        }
        if (in_array($item->res_role_id, $userRoles)) {
            $accessLevel = 2;
        }
        if (in_array($item->raised_by_role_id, $userRoles) || $item->raised_by_user_id === Auth::user()->id || Auth::user()->punchlist_super_user === 1) {
            $accessLevel = 3;
        }

        $activationStatusArray = [0 => 'Proposed', 1 => 'Accepted', 2 => 'Rejected',];
        $superUser = 0;
        if (Auth::user()->punchlist_super_user === 1) {
            $superUser = 1;
        }


        //dd($userRoles) ;
        /* Old code for company access
       $userCompanyId = \Auth::user()->company->id;
       if ($item->assigned_company_id === $userCompanyId) {$accessLevel = 1;}
       if ($item->res_company_id === $userCompanyId) {$accessLevel = 2;}
       if ($item->raised_by_company_id === $userCompanyId) {$accessLevel = 3;}*/

        return view('rowsys.punchlist.items.read.details.details', [
            'item' => $item,
            'companies' => $companies,
            'roles' => $roles,
            'users' => $users,
            'fileList' => $imgList,
            'awsLink' => $awsLink,
            'statusArray' => $statusArray,
            'ownStatusArray' => $ownStatusArray,
            'action' => $action,
            'accessLevel' => $accessLevel,
            'superUser' => $superUser,
            'activationStatusArray' => $activationStatusArray,

        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Update the Item Owner Details
    |--------------------------------------------------------------------------
    */
    public function updateItemOwnerDetails(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'required',
            'responsible_role' => 'required',
            'attention_to' => 'required',
        ]);
        $item = Item::find($id);
        $item->own_status = $request->own_status;
        if ((int)$request->own_status === 2) {
            $item->complete = 1;
            $item->closed_date = Carbon::now();
            $item->closed_by_id = \Auth::user()->id;
        } else {
            $item->complete = 0;
        }
        $item->forget = 0;
        if ((int)$request->own_status === 3) {
            $item->forget = 1;
        }
        $item->finding = $request->description;
        $item->privacy = 1;//1 = public
        if ($request->privacy === 'on') {
            $item->privacy = 1;
        }
        $item->urgent = 0;
        if ($request->urgent === 'on') {
            $item->urgent = 1;
        }

        $item->res_company_id = User::find((int)$request->attention_to)->company_id;
        $item->res_role_id = (int)$request->responsible_role;
        $item->res_user_id = (int)$request->attention_to;
        //Last updated by and when
        $item->owner_updated_by_id = \Auth::user()->id;
        $item->owner_updated_date = Carbon::now();

        //$item->res_user_id = (int)$request->res_company_user;

        //---------------------------------
        //         Watch List / Flag it
        //---------------------------------
        //My Flag - Add to May Watch List - to be watched
        $user = Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        if (count($userFilter) === 0) {
            $userFilter = new UserFilter();
            $userFilter->user_id = $user->id;
        }
        $currentListString = $userFilter->item_flag_list;
        $currentListArray = explode(',', $currentListString);
        if ($request->my_flag === 'on') {
            if ($currentListString === "" || $currentListString === "''") {
                $userFilter->item_flag_list = $item->id;
            } else {
                if (!in_array($item->id, $currentListArray)) {
                    array_push($currentListArray, $item->id);
                    $userFilter->item_flag_list = implode(",", $currentListArray);
                }
            }
        } // Else - Remove from the array i.e unset
        else {
            if (in_array($item->id, $currentListArray)) {
                $key = array_search($item->id, $currentListArray);
                if ($key !== false) {
                    unset($currentListArray[$key]);
                }
                $userFilter->item_flag_list = implode(",", $currentListArray);
            }
        }
        $userFilter->save();

        //---------------------------------
        // Item Due Date Update
        //---------------------------------
        if (isset($request->updated_target_date) && $request->updated_target_date !== ''
            && $request->chk_update_due_date !== '') {
            //Who/When
            $item->due_date_updated_by_id = \Auth::user()->id;
            $item->due_date_updated_on_date = Carbon::now();
            //Update Note
            $item->due_date_updated_note = $request->update_date_note;

            $item->due_date_updated = 1;

            //Update the Due Date for the Item
            $item->due_date = $request->updated_target_date;

        }

        //---------------------------------
        // Activation Status
        //---------------------------------
        if (isset($request->activated_status)) {
            $item->activated_status = (int)$request->activated_status;

        }

        //Save the Item
        $item->save();

        $compile = new Compile();
        $message = $compile->compileItem($item->id);


        return 'Item Details Updated - ' . $message;
    }

    /*
    |--------------------------------------------------------------------------
    | Update the Item Responsible Details
    |--------------------------------------------------------------------------
    */
    public function updateItemResponsibleDetails(Request $request, $id)
    {
        $this->validate($request, [
            'res_target_date' => 'required|date',
            'assigned_to' => 'required',
            'assigned_role' => 'required',
        ]);

        $item = Item::find($id);

        $item->res_note = $request->res_note;
        $item->res_cause = $request->res_cause;
        $item->res_action = $request->res_action;

        $dateString = $request->res_target_date . '00';
        $due_date = Carbon::createFromFormat('Y-m-d H', $dateString);
        $enteredDate = $request->res_target_date;

        $item->res_target_date = $due_date;

        if ($due_date > $item->due_date || (int)$request->assign_to === 0) {
            $item->res_target_date = $item->due_date;
            $item->assigned_company_id = 0;
        }

        //Last updated by and when
        $user = Auth::user();
        $item->res_updated_by_id = $user->id;
        $item->res_updated_date = Carbon::now();

        $item->last_updated_by_id = $user->id;
        $item->last_updated_at = Carbon::now();

        $item->res_status = (int)$request->res_status;
        if ((int)$request->res_status === 4) {
            $item->res_complete_date = Carbon::now();
            $item->res_complete_by_id = $user->id;
        }

        //---------------------------------
        //         ATTACHMENT
        //---------------------------------
        //File
        $requestHasFile = $request->hasFile('attachment_file');
        if ($requestHasFile) {
            //$file = $request->file('attachment_file');
            //$this->addAttachment($file,$request->attachment_tag,$item->id,$item->number);

        }

        //Assignee
        if ((int)$request->assigned_role > 0) {
            //Update the Item
            $item->assigned_company_id = $user->company_id;
            $item->assigned_role_id = (int)$request->assigned_role;
            $item->assigned_user_id = (int)$request->assigned_to;

            //Add an Assignee Action
            $assignee = Action::firstOrCreate(['item_id' => $item->id]);
            $assignee->assigned_company_id = $user->company_id;
            $assignee->assigned_role_id = (int)$request->assigned_role;
            if ((int)$request->assigned_to === 0) {
                $assignee->assigned_user_id = 1;
            } else {
                $assignee->assigned_user_id = (int)$request->assigned_to;
            }

            //History
            $assignee->assigned_by_user_id = $user->id;
            $assignee->last_updated_by_id = $user->id;
            $assignee->last_updated_at = Carbon::now();
            $assignee->save();
        }

        if ((int)$request->assigned_role === 0) {
            $removeAssignee = Action::where('item_id', $item->id)->first();
            if (count($removeAssignee) > 0) {
                Action::destroy($removeAssignee->id);
                $item->assigned_company_id = 0;
            }
        }

        $item->save();

        return '' . $request->my_flag;
    }


    /*
    |--------------------------------------------------------------------------
    | Update the Item Assignee Details
    |--------------------------------------------------------------------------
    */
    public function updateItemAssigneeDetails(Request $request, $id)
    {
        $action = Action::find($id);
        $action->comment = $request->assignee_description;
        $action->status = (int)$request->assignee_status;
        //Update the Associated Item
        $item = Item::where('id', $action->item_id)->first();
        if (count($item) > 0) {
            $item->assigned_status = (int)$request->assignee_status;
            $item->save();
        }
        $action->last_updated_by_id = \Auth::user()->id;
        $action->last_updated_at = Carbon::now();
        if ((int)$request->assignee_status === 4) {
            $action->complete = 1;
            $action->complete_date = Carbon::now();
            $action->complete_by = \Auth::user()->id;
        }
        $action->save();

        return 'Item Updated (Assignee)';
    }

    /*
    |--------------------------------------------------------------------------
    | Get the System Schedule Dates for when adding the Item e.g Cat A and B dates
    | returns all the dates for the system for cat A and B to populate drop downs
    |--------------------------------------------------------------------------
    */
    public function getSystemScheduleDates($system_id)
    {
        $data =  NewItemDates::forSystem($system_id);
        if( ! is_null( $data)){
            return $data;
        }else{
            return \Response::json([
                'message' => 'No Dates For Selected System'
            ], 422);
        }
    }


    /*
    |--------------------------------------------------------------------------
    | Show the Attachment view
    |--------------------------------------------------------------------------
    */
    public function getAttachmentsIndex($id)
    {
        $item = Item::find($id);
        $attachmentList = Attachment::where('item_id', $id)->get();
        // $imageList = Attachment::where('item_id', $id)->whereIn('file_type', $mimeTypes = ['bmp', 'gif', 'jpeg', 'jpg', 'tif', 'tiff', 'ico'])->get();
        $imageList = Attachment::where('item_id', $id)->get();
        $pdfList = Attachment::where('item_id', $id)->whereIn('file_type', $mimeTypes = ['pdf'])->get();
        $directory = 'project-1/punchlist';
        $fileList = Storage::allFiles($directory);
        //https://s3-ap-southeast-1.amazonaws.com/abbvie-asia-bucket/uploads/2016-04-26-photo.JPG
        $awsLink = 'https://s3-ap-southeast-1.amazonaws.com/' . env('AWS_BUCKET', 'figaro-testing') . '/';


        return view('rowsys.punchlist.items.attachments.index', [
            'item' => $item,
            'fileList' => $fileList,
            'awsLink' => $awsLink,
            'attachmentList' => $attachmentList,
            'imageList' => $imageList,
            'pdfList' => $pdfList,
        ]);
    }

    /*
|--------------------------------------------------------------------------
| Get the System Schedule Dates for when adding the Item e.g Cat A and B dates
| returns all the dates for the system for cat A and B to populate drop downs
|--------------------------------------------------------------------------
*/
    public function OLDgetSystemScheduleDates($system_id)
    {

        //table = punch_list_system_schedule_links
        $dates = SystemScheduleDates::where('system_id', $system_id)->first();
        $milestones = SystemMilestone::lists('name')->toArray();
        $datesDescription = ['Walk Down', 'MC', 'COM', 'Loop Check & Cal', 'IQ - End', 'OV-Start', 'OQ-End', 'PQ-Start', 'PQ-End', 'Post OQ Cal'];
        $currentPhase = ['Construction', 'Walk Down', 'MC', 'COM', 'Loop Check & Cal', 'IQ - End', 'OV-Start', 'OQ-End', 'PQ-Start', 'PQ-End', 'Post OQ Cal'];
        $datesArray = [];
        $system_current_phase = '';
        if (isset($dates)) {

            // Status 0=N/A , 1 = not done, 2= done

            // Selections
            //Cat 1 setup
            $catA_Date = $dates->m1_date->format('d-m-Y');
            $catA_step = '';
            $catA_step_number = 0;
            $columnStatus = 'm1_status';
            $columnDate = 'm1_date';
            $catA_step_txt = 'default';

            //Cat B
            $today = Carbon::now();
            $a_step_found = false;

            for ($i = 1; $i < sizeof($milestones); $i++) {//$i+=2 increment by 2
                $columnDate = 'm' . $i . '_date';
                $columnStatus = 'm' . $i . '_status';
                if ($dates->$columnDate !== null && $dates->$columnDate > $today) {
                    //Cat A
                    if ($dates->$columnStatus !== 0 && $dates->$columnStatus !== 1 && $a_step_found !== true) {
                        $catA_Date = $dates->$columnDate->format('d-m-Y');
                        $catA_step_txt = $milestones[$i];
                        $system_current_phase = $milestones[$i - 1];
                        $catA_step_number = $i;
                        $a_step_found = true;
                    }
                    //Cat B
                    $step = $milestones[$i];
                    array_push($datesArray, [$step, $dates->$columnDate->format('d-m-Y'), $i]);
                }
            }

            $data = [
                'datesArray' => $datesArray,
                'system_current_phase' => $system_current_phase,
                'cat_a_date' => $catA_Date,
                'cat_a_step' => $catA_step_txt,
                'cat_a_step_number' => $catA_step_number,
                'datesDescription' => $datesDescription,
                'mc_date' => $dates->m1_date->format('d-m-Y'),
                'com_date' => $dates->m2_date->format('d-m-Y'),
                'oq_date' => $dates->m3_date->format('d-m-Y'),
                'pq_date' => $dates->m4_date->format('d-m-Y')

            ];

            return $data;
        } else {
            return \Response::json([
                'message' => 'No Dates For Selected System.'
            ], 422);
        }
    }

    public function destroy($id)
    {

        Item::destroy($id);

        return " Item Deleted";
    }

    //Compile the Items
    public function compileItems()
    {

        //$compile = new Compile();
        //$message = $compile->compileItem(1);
        // return $message;
        $compileJob = new CompileItems();
        $this->dispatch($compileJob);


        return "compile started. Ensure the Queue is Listening  ";
    }


    //return the Items from the Dashboard Links - User
    public function userItems(Request $request)
    {

        $user = \Auth::getUser();

        $userRoles = $user->roles->lists('id')->toArray();
        $items = Item::get();

        switch ((int)$request->selection) {

            //Raised
            case 1:
                $items = Item::where('raised_by_user_id', $user->id)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Responsible
            case 2:
                $items = Item::where('res_user_id', $user->id)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Assigned
            case 3:
                $items = Item::where('assigned_user_id', $user->id)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Waiting Closeout
            case 4:
                $items = Item::where('raised_by_user_id', $user->id)->where('complete', 0)->where('forget', 0)->where('res_status', '=', 4)->get();
                break;

            //Urgent
            case 5:
                $items = Item::where('forget', 0)->where('complete', 0)->where('urgent', 1)
                    ->where(function ($query) use ($user) {
                        $query->where('raised_by_role_id', $user->id)
                            ->orWhere('res_role_id', $user->id)
                            ->orWhere('assigned_role_id', $user->id);
                    })
                    ->get();
                break;

            //Steve 1
            case 6:
                $items = Item::where('raised_by_user_id', $user->id)->where('res_status', 4)->where('forget', 0)->where('complete', 0)->get();
                break;
            //Steve 2
            case 7:
                $items = Item::where('res_user_id', $user->id)->where('res_status', 4)->where('forget', 0)->where('complete', 0)->get();
                break;
            //Steve 3
            case 8:
                $items = Item::where('res_user_id', $user->id)->where('assigned_status', 4)->where('forget', 0)->where('complete', 0)->get();
                break;
            //Steve 4
            case 9:
                $items = Item::where('assigned_role_id', $user->id)->where('assigned_status', 4)->where('res_status', '!=', 4)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Default
            default:
                $items = Item::where('raised_by_user_id', $user->id)->where('forget', 0)->where('complete', 0)->get();

        }

        $view = 'rowsys.punchlist.items.read.tables.index1';

        return view($view, ['items' => $items,]);
    }

    //return the Items from the Dashboard Links - User
    public function roleItems(Request $request)
    {

        $user = \Auth::getUser();
        $userRoles = $user->roles->lists('id')->toArray();

        switch ((int)$request->selection) {

            //Raised
            case 1:
                $items = Item::whereIn('raised_by_role_id', $userRoles)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Responsible
            case 2:
                $items = Item::whereIn('res_role_id', $userRoles)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Assigned
            case 3:
                $items = Item::whereIn('assigned_role_id', $userRoles)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Waiting Closeout
            case 4:
                $items = Item::whereIn('raised_by_role_id', $userRoles)->where('complete', 0)->where('forget', 0)->where('res_status', '=', 4)->get();
                break;

            //Urgent
            case 5:
                $items = Item::where('forget', 0)->where('complete', 0)->where('urgent', 1)
                    ->where(function ($query) use ($userRoles) {
                        $query->whereIn('raised_by_role_id', $userRoles)
                            ->orWhereIn('res_role_id', $userRoles)
                            ->orWhereIn('assigned_role_id', $userRoles);
                    })
                    ->get();
                break;
            //Steve 1
            case 6:
                $items = Item::whereIn('raised_by_user_id', $userRoles)->where('res_status', 4)->where('forget', 0)->where('complete', 0)->get();
                break;
            //Steve 2
            case 7:
                $items = Item::whereIn('res_user_id', $userRoles)->where('res_status', 4)->where('forget', 0)->where('complete', 0)->get();
                break;
            //Steve 3
            case 8:
                $items = Item::whereIn('res_user_id', $userRoles)->where('assigned_status', 4)->where('forget', 0)->where('complete', 0)->get();
                break;
            //Steve 4
            case 9:
                $items = Item::whereIn('assigned_role_id', $userRoles)->where('assigned_status', 4)->where('res_status', '!=', 4)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Default
            default:
                $items = Item::whereIn('raised_by_role_id', $userRoles)->where('forget', 0)->where('complete', 0)->get();

        }

        $view = 'rowsys.punchlist.items.read.tables.index1';

        return view($view, ['items' => $items,]);
    }

    //Returns the Items from the Quick Links on the Dashboard
    public function dashBoardQuickLinks(Request $request)
    {

        $user = \Auth::getUser();
        $userRoles = $user->roles->lists('id')->toArray();

        switch ((int)$request->choice) {

            //Raised Today
            case 1:
                $today = Carbon::today()->toDateString();
                $items = Item::whereDate('raised_date', '=', $today)->where('forget', 0)->where('complete', 0)->get();
                //dd($items);
                break;

            //Raised last 7 days
            case 2:
                $today = Carbon::now();
                $sevenDaysAgo = Carbon::now()->subDays(7);
                $items = Item::where('raised_date', '<', $today)->where('raised_date', '>', $sevenDaysAgo)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Due Soon
            case 3:
                $twoWeeksAway = Carbon::now()->addWeeks(1);
                $items = Item::where('res_target_date', '<', $twoWeeksAway)->where('forget', 0)->where('complete', 0)->get();
                break;

            //Ready to Close
            case 4:
                $items = Item::where('res_status', '=', 4)->where('complete', 0)->where('forget', 0)->get();
                break;

            //Urgent
            case 5:
                $items = Item::where('forget', 0)->where('complete', 0)->where('urgent', 1)->get();
                break;

            //Default
            default:
                $items = Item::limit('100')->where('complete', 0)->get();

        }

        $view = 'rowsys.punchlist.items.read.tables.index1';

        return view($view, ['items' => $items,]);
    }


}

<?php

namespace App\Http\Controllers\Schedule;

use App\Jobs\CompileTasksAfterScheduleDateChange;
use App\Models\Project\SystemMilestone;
use App\Models\PunchList\SystemScheduleDates;
use App\Models\ScheduleDate;
use App\Models\Stage;
use App\Models\System;
use App\Models\Task;
use App\Models\TaskType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Mockery\Exception;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{

    /* ----------------------------------------------
     * READ
     *----------------------------------------------*/

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datesWithErrors = [];
        try {

            if (Schema::hasTable('schedule_dates_tmp')) {
                $datesWithErrors = \DB::table('schedule_dates_tmp')->where('start_date', null)->orWhere('finish_date', null)->get();
            }
            // return $datesWithErrors;
        } catch (Exception $e) {

        }

        return view('rowsys.schedule.import.index', [
            'datesWithErrors' => $datesWithErrors,
        ]);

    }

    public function downloadImportFile()
    {

        return "download file";

        return response()->download($pathToFile);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gantt()
    {
        $datesWithErrors = ScheduleDate::where('start_date', null)->orWhere('finish_date', null)->get();

        return view('rowsys.schedule.import.index', [
            'datesWithErrors' => $datesWithErrors,
        ]);

    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function table(Request $request)
    {

        $searchText = '';
        $searchOn = 0;

        if ($request->has('searchText') || $request->has('chk_imported_data')) {
            $searchText = $request->searchText;
            $searchOn = 1;
        }

        if ($searchOn === 0) {
            $scheduleDates = ScheduleDate::orderBy('number', 'ASC')
                ->where('finish_date', '!=', 'null')
                ->where('imported_data', 0)
                //->where('start_date', '>', '2016-05')
                ->take(250)
                ->get();
        } else {
            $pieces = explode(" ", $searchText);
            $scheduleDates = ScheduleDate::query();
            $scheduleDates->where('number', 'LIKE', '%' . trim($pieces[0]) . '%');
            for ($i = 1; $i < sizeof($pieces); $i++) {
                $scheduleDates->where('number', 'LIKE', '%' . $pieces[$i] . '%');
            }
            $scheduleDates->orWhere('description', 'LIKE', '%' . trim($pieces[0]) . '%');
            for ($i = 1; $i < sizeof($pieces); $i++) {
                if (trim($pieces[$i]) !== '') {
                    $scheduleDates->where('description', 'LIKE', '%' . trim($pieces[$i]) . '%');
                }
            }

            if ($request->chk_imported_data === 'on') {
                $scheduleDates->where('imported_data', 1);
            } else {
                $scheduleDates->where('imported_data', 0);
            }
            $scheduleDates = $scheduleDates->take(200)->orderBy('number', 'ASC')->get();

            /*
            select number,description,(`finish_date` - `start_date`) * 0.000000277778 as dif from `schedule_dates` where  (`finish_date` - `start_date` ) > 0
            and  (`finish_date` - `start_date` ) > 0
            */

        }

        return view('rowsys.schedule.read.table.index', [
            'scheduleDates' => $scheduleDates,
        ]);

    }


    /**
     * @param Request $request
     * @return array
     */
    public function addActivity(Request $request)
    {

        $this->validate($request, [
            'new_number' => 'required|unique:schedule_dates,number',
            'new_description' => 'required',
            'new_start_date' => 'required|date',
            'new_finish_date' => 'required|date|after:start_date'
        ]);

        $activity = new ScheduleDate();
        $activity->number = $request->new_number;
        $activity->description = $request->new_description;
        $activity->start_date = Carbon::createFromFormat('Y-m-d', $request->new_start_date);
        $activity->finish_date = Carbon::createFromFormat('Y-m-d', $request->new_finish_date);
        $activity->imported_data = 0;
        $activity->save();
        $message = 'Activity added';
        $data = ['message' => $message];

        return $data;

    }
    /* ----------------------------------------------
     * UPDATE
     *----------------------------------------------*/
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateForm($id)
    {
        $scheduleActivity = ScheduleDate::find($id);
        $taskCount = Task::where('schedule_number', $scheduleActivity->number)->count();

        return view('rowsys.project.schedule.update.form', [
            'scheduleActivity' => $scheduleActivity,
            'taskCount' => $taskCount,
        ]);

    }

    public function updateActivity(Request $request, $id)
    {

        $this->validate($request, [
            // 'update_number' => 'required',
            //'update_description' => 'required',
            'start_date' => 'required|date',
            'finish_date' => 'required|date'
        ]);

        //$activityId = (int)$request->update_activity_id;
        $activity = ScheduleDate::find($id);
        $activity->number = $request->update_number;
        $activity->description = $request->update_description;
        $activity->start_date = $request->start_date;
        $activity->finish_date = $request->finish_date;
        $activity->save();

        $message = 'Activity updated - effected tasks will be updated';

        $compileTasks = new CompileTasksAfterScheduleDateChange($activity->number);
        $this->dispatch($compileTasks);

        return $message;

    }

    /* ----------------------------------------------
     * DESTROY
     *----------------------------------------------*/
    /**
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        ScheduleDate::destroy($id);

        $message = 'Activity deleted';
        $data = ['message' => $message];

        return $data;
    }


    /**
     * @param Request $request
     * @return string
     */
    public function taskDataForGanttChart(Request $request)
    {

        $system = (int)$request->system;

        $tasks = Task::where('task_type_id', '>', 0)
            //->where('stage_id',1)
            //->whereIn('system_id', [86, 98, 44, 45, 23])
            // ->where('area_id',4)
            ->where('system_id', $system)
            ->orderBy('stage_id')
            ->orderBy('task_type_id')
            ->with('taskType', 'stage', 'system')
            ->get();


        $json = json_encode($tasks);

        return $json;


    }

    /* ----------------------------------------------
    *   SYSTEM LINK
    *----------------------------------------------*/
    public function systemLinkTable()
    {

        $systemLinkDates = SystemScheduleDates::with('system',
            'm1Stage', 'm1TaskType',
            'm2Stage', 'm2TaskType',
            'm3Stage', 'm3TaskType',
            'm4Stage', 'm4TaskType',
            'm5Stage', 'm5TaskType',
            'm6Stage', 'm6TaskType',
            'm7Stage', 'm7TaskType',
            'm8Stage', 'm8TaskType',
            'm9Stage', 'm9TaskType',
            'm10Stage', 'm10TaskType',
            'm11Stage', 'm11TaskType'
        )->get();

        $mileStones = SystemMilestone::get();

        // return $systemLinkDates;
        $systems = System::get();
        $mileStones = SystemMilestone::get();

        return view('rowsys.schedule.system_link.read.table.index', [
            'systemLinkDates' => $systemLinkDates,
            'systems' => $systems,
            'mileStones' => $mileStones,
        ]);

    }


    public function updateSystemLinkView()
    {

        $systems = System::get();
        $mileStones = SystemMilestone::get();
        $taskTypes = TaskType::get();
        $stages = Stage::get();

        return view('rowsys.schedule.system_link.update.index', [
            'systems' => $systems,
            'mileStones' => $mileStones,
            'taskTypes' => $taskTypes,
            'stages' => $stages,
        ]);
    }

    public function milestoneEditForm(Request $request)
    {

        $systemId = (int)$_GET['data']['systemId'];
        $milestoneId = (int)$_GET['data']['milestone'];
        $taskTypes = TaskType::get();
        $stages = Stage::get();
        $scheduleLink = SystemScheduleDates::where('system_id', $systemId)->with('system',
            'm1Stage', 'm1TaskType',
            'm2Stage', 'm2TaskType',
            'm3Stage', 'm3TaskType',
            'm4Stage', 'm4TaskType',
            'm5Stage', 'm5TaskType',
            'm6Stage', 'm6TaskType',
            'm7Stage', 'm7TaskType',
            'm8Stage', 'm8TaskType',
            'm9Stage', 'm9TaskType',
            'm10Stage', 'm10TaskType',
            'm11Stage', 'm11TaskType')->first();
        $selectedMilestone = 'm' . $milestoneId . '_';
        //Lag
        $holder = $selectedMilestone . 'lag';
        $lag = $scheduleLink->$holder;
        //Choice
        $holder = $selectedMilestone . 'schedule_choice';
        $choice = $scheduleLink->$holder;
        //Schedule Number
        $holder = $selectedMilestone . 'schedule_link_number';
        $scheduleNumber = $scheduleLink->$holder;
        //Schedule Date
        $scheduleLinkDescription = 'No Link';
        $scheduleLinkId = 0;
        $infoMessage = 'No Schedule Link';
        $scheduleDate = ScheduleDate::where('name', $scheduleNumber)->first();
        if (count($scheduleDate) > 0) {
            $scheduleLinkId = $scheduleDate->id;
            $scheduleLinkDescription = $scheduleDate->description;
            $infoMessage = 'Milestone is Linked To Schedule';
        }
        //Stage
        $holder = $selectedMilestone . 'stage_id';
        $selectedStage = $scheduleLink->$holder;
        //Task Type
        $holder = $selectedMilestone . 'task_type_id';
        $selectedType = $scheduleLink->$holder;

        $dateChoice = [1 => 'start', 2 => 'finish'];


        return view('rowsys.schedule.system_link.update.form', [
            'taskTypes' => $taskTypes,
            'stages' => $stages,
            'systemId' => $systemId,
            'milestoneId' => $milestoneId,
            'lag' => $lag,
            'choice' => $choice,
            'scheduleNumber' => $scheduleNumber,
            'selectedStage' => (int)$selectedStage,
            'selectedType' => $selectedType,
            'scheduleLinkDescription' => $scheduleLinkDescription,
            'scheduleDate' => $scheduleDate,
            'infoMessage' => $infoMessage,
            'dateChoice' => $dateChoice,
            'scheduleLink' => $scheduleLink,
            'scheduleLinkId' => $scheduleLinkId,

        ]);

    }

    public function updateMilestone(Request $request)
    {

        $scheduleLink = $request->schedule_link;
        if ($scheduleLink !== '') {
            $this->validate($request, [
                //'schedule_link' => 'exists:schedule_dates,number',
                'choice' => 'required',
                'lag' => 'required|integer|min:0',
                'stage' => 'required',
                'type' => 'required|integer|min:0',
            ]);
        }

        $scheduleLinkDate = SystemScheduleDates::find((int)$request->schedule_link_date_id);
        $selectedMilestone = 'm' . $request->milestone_number . '_';
        //Lag
        $holder = $selectedMilestone . 'lag';
        $scheduleLinkDate->$holder = (int)$request->lag;
        //Choice
        $holder = $selectedMilestone . 'schedule_choice';
        $scheduleLinkDate->$holder = $request->choice;

        //Schedule Number
        $holder = $selectedMilestone . 'schedule_link_number';
        $scheduleLinkDate->$holder = '';
        if ($request->schedule_link !== '') {
            $scheduleDate = ScheduleDate::find($request->scheduleId);
            if (count($scheduleDate) > 0) {
                $scheduleLinkDate->$holder = $scheduleDate->number;
            } else {
                $scheduleLinkDate->$holder = '';
            }
        }
        //Stage
        $holder = $selectedMilestone . 'stage_id';
        $scheduleLinkDate->$holder = (int)$request->stage;
        //Task Type
        $holder = $selectedMilestone . 'task_type_id';
        $scheduleLinkDate->$holder = (int)$request->type;

        $scheduleLinkDate->save();

        return "Milestone Updated";
    }




    /* ----------------------------------------------
    *   SEARCH
    *----------------------------------------------*/
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function searchResults(Request $request)
    {
        $searchText = $request->search_text;
        $chk_imported_data = $request->chk_imported_data;

        return redirect()->action('Schedule\ScheduleController@table', ['searchText' => $searchText, 'chk_imported_data' => $chk_imported_data]);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ganttChart()
    {
        $systems = System::get();

        return view('rowsys.project.schedule.read.chart.index1', [
            'systems' => $systems,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ganttChart1()
    {
        $systems = System::get();

        return view('rowsys.project.schedule.read.chart.gantt1', [
            'systems' => $systems,
        ]);
    }


    public function scheduleGanttChart()
    {

        return view('rowsys.schedule.read.chart.gantt.index', [
            //'tasks' => $tasks,
        ]);
    }

    //Show the Activity Summary Index
    public function activitySummaryIndex()
    {
        $scheduleGroupCount = Task::groupBy('schedule_number')->get();
        return view('rowsys.schedule.activity_summary.read.table.index', [
            'scheduleGroupCount' => sizeof($scheduleGroupCount),
        ]);
    }

    //returns the %,start & end date for each Schedule Activity
    public function activityPercentData(Request $request)
    {
        $scheduleGroup = Task::groupBy('schedule_number');
        $choice = 1;
        $displayChoice = "Results for both Prep and Exec";
        switch ($request->get('report-choice-radio-button')) {
            case "all":
                $choice = 1;
                break;
            case "prep":
                $choice = 2;
                $displayChoice = "Results for Prep Only";
                $scheduleGroup->where('stage_id',1);
                break;
            case "exec":
                $choice = 3;
                $displayChoice = "Results for Exec Only";
                $scheduleGroup->where('stage_id',2);
                break;
            default:
                $choice = 1;
        }

        $data = [];
        $scheduleGroup = $scheduleGroup
            //->limit(5)
            ->lists('schedule_number');
        $scheduleDates = ScheduleDate::whereIn('number',$scheduleGroup)->get();

        //--- loop through the schedule dates ---
        foreach ($scheduleDates as $scheduleDate) {

            //1. get the stage_id,system_id,task_type_id,p1 & p2 values.
            $stage_id = 1;
            $task_type_id = 1;
            $system_id = 1;
            $p1 = 85;
            $p2 = 75;

            $scheduleNumber = $scheduleDate->number;

            $HoursPercent = 0;

            //2. Get the Number of Assigned Hours
            $assignedHours = Task::where('schedule_number',$scheduleNumber);
            if($choice === 2){
                $assignedHours->where('stage_id',1);
            }
            if($choice === 3){
                $assignedHours->where('stage_id',2);
            }
            $assignedHours= $assignedHours->sum('target_val');

            //3. Get the Number of Earned Hours
            $earnedHours = Task::where('schedule_number',$scheduleNumber);
            if($choice === 2){
                $earnedHours->where('stage_id',1);
            }
            if($choice === 3){
                $earnedHours->where('stage_id',2);
            }
            $earnedHours = $earnedHours->sum('earned_val');


            //4. Calculate the % , Start and End Dates.
            if($assignedHours > 0){
                $HoursPercent  = round(($earnedHours/$assignedHours) * 100,1);
            }

            if ($HoursPercent > 85) {
                $startDate = $scheduleDate->start_date->toDateString();
                $startText = 'Started';
                $finishDate = $scheduleDate->finish_date->toDateString();
                $finishText = 'Finished';
                $SchedulePercent = 100;
            }
            elseif ($HoursPercent < 75){
                $SchedulePercent = 0;
                $startDate = '';
                $startText = 'NOT Started';
                $finishDate = '';
                $finishText = 'NOT Finished';
            }else{
                $startDate = $scheduleDate->start_date->toDateString();
                $startText = 'Started';
                $finishDate = '';
                $finishText = 'NOT Finished';
                //P1 = 85 P2 = 75
                $SchedulePercent = round((($p1 - $p2)/$p2) * 100,0);
            }

            $scheduleData = [$scheduleNumber,$scheduleDate->description, $SchedulePercent, $startText,$startDate, $finishText,$finishDate,$HoursPercent.'%',$assignedHours,$earnedHours];

            array_push($data, $scheduleData);
        }

        return view('rowsys.schedule.activity_summary.read.table.table', [
            'activitySummaries' => $data,
            'displayChoice' => $displayChoice
        ]);
    }

    //returns the exceptions for the Activity Summary
    // i.e the details of the tasks associated with the Schedule Link
    // that do not fit the search  configuration
    public function activityExceptions()
    {
        $data = [];

        //get list of Schedule dates used in task
        $scheduleGroup = Task::groupBy('schedule_number')
            ->limit('10')
            ->lists('schedule_number');
        $scheduleDates = ScheduleDate::whereIn('number',$scheduleGroup)->get();

        //--- loop through the schedule dates ---
        foreach ($scheduleDates as $scheduleDate) {

            //1. get the stage_id,system_id,task_type_id,p1 & p2 values.
            $stage_id = 1;
            $task_type_id = 1;
            $system_id = 1;
            $p1 = 85;
            $p2 = 75;

            $scheduleNumber = $scheduleDate->number;

            //2. Get the Number of task that in total assigned to the activity
            $totalTasksCount = Task::where('schedule_number',$scheduleNumber)
                //->where('stage_id',$stage_id)
                //->where('task_type_id',$task_type_id)
                //->where('system_id',$system_id)
                ->count();

            //3. Get the Number of tasks that fit the configuration
            $totalConfigTaskCount = Task::where('schedule_number',$scheduleNumber)
                //->where('stage_id',$stage_id)
               // ->where('task_type_id',$task_type_id)
                //->where('system_id',$system_id)
                ->count();

            //4. Calculate the difference - if zero move on else record the details for the user
            if(($totalTasksCount - $totalConfigTaskCount) > 0 || true){

                $taskNumbersArray = Task::where('schedule_number',$scheduleNumber)
                   // ->where('stage_id','!=',$stage_id)
                    //->where('task_type_id','!=',$task_type_id)
                    //->where('system_id','!=',$system_id)
                    ->lists('number');

                $exceptionsData = [
                    $scheduleNumber,
                    $scheduleDate->description,
                    $totalTasksCount,
                    sizeof($taskNumbersArray),
                    $taskNumbersArray,
                ];

                array_push($data, $exceptionsData);
            }

        }



        return view('rowsys.schedule.activity_summary.read.exceptions.table', [
            'activitySummaries' => $data,
        ]);
    }
}

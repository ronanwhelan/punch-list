<?php

namespace App\Http\Controllers\Admin;

use App\Models\Area;
use App\Models\Role;
use App\Models\Task;
use App\Models\Team;
use App\Models\User;
use App\Models\Company;
use App\Models\UserFilter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class UsersController extends Controller
{

    public function __construct()
    {
        //$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        $companies = Company::get();
        $roles = Role::get();

        return view('rowsys.admin.users.index', [
            'users' => $users,
            'companies' => $companies,
            'roles' => $roles,
        ]);
    }

    public function getUserDetails($user_id)
    {
        $user = User::find($user_id);
        $companies = Company::get();
        $roles = Role::get();
        $userRoles = $user->roles->lists('id')->toArray();

        return view('rowsys.admin.users.user_details', [
            'user' => $user,
            'companies' => $companies,
            'roles' => $roles,
            'userRoles' => $userRoles,
        ]);
    }

    public function getUserSettingsDetails($user_id)
    {
        //check if this user is the user for these details
        $user = User::find($user_id);

        return view('rowsys.admin.users.profile.user_setting_details', [
            'user' => $user,
        ]);

    }

    public function updateUserMyDetails(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255',
        ]);

        $user = User::find($request['user_id']);
        $user->name = $request['name'];
        $user->surname = $request['surname'];
        $user->email = $request['email'];
        $user->save();

        $message = $request['name'] . ' details Updated';

        $data = [$message, $user];

        return $data;

    }

    public function updateUserMyPassword(Request $request)
    {

        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        if (\Hash::check($request->current_password, \Auth::user()->password)) {
            $user = \Auth::user();
            $user->password = bcrypt($request['password']);
            $user->password_changed = 1;
            $user->password_changed_date = Carbon::now();
            $user->save();
            \Auth::logout();

            return \Response::json(['message' => 'Password Updated.'], 200);

        } else {
            return \Response::json([
                'message' => 'Password Incorrect.'
            ], 422);
        }
    }


    public function updatePasswordOnInitialLogin(Request $request)
    {

        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        if (\Hash::check($request->current_password, \Auth::user()->password)) {
            $user = \Auth::user();
            $user->password = bcrypt($request['password']);
            $user->password_changed = 1;
            $user->password_changed_date = Carbon::now();
            $user->save();
            \Auth::logout();
            \Session::flash('message', "Special message goes here");

            //\Session::flash('message', "Thanks, Your password is updated. Just Login again.");
            return redirect('login')->with('message', 'Thanks, Your password is updated. Just Login again.');
            // return redirect('/welcome');
            //redirect()->route('/welcome');

        } else {
            \Session::flash('message', "Special message goes here");

            return \Redirect::back()->with('message', 'Password Incorrect.Try again');
            // return \Response::json(['message' => 'Password Incorrect.'], 422);
        }
    }

    public function getResetPasswordOnInitialLoginPage()
    {
        $user = \Auth::user();

        return view('rowsys.admin.users.password.change_password', [
            'user' => $user,
            //'companies' => $companies,
        ]);
    }


    /**
     * @param Request $request
     * @return string|static
     * @throws \Illuminate\Foundation\Validation\ValidationException
     */
    protected function createUser(Request $request)
    {
        $validateArray = [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'company' => 'required|min:1',
            'roles' => 'required|min:1',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ];
        $this->validate($request, $validateArray);

        //Allow Access
        $confirmed = 0;
        if ($request['confirmed'] === 'on') {
            $confirmed = 1;
        }

        $tracker_access = 0;
        $role = 0;
        if ($request->has('access_to_tracker')) {
            //Tracker Access and Role
            $tracker_access = 0;
            if ($request['access_to_tracker'] === 'on') {
                $tracker_access = 1;
            }
            $role = (int)$request['role'];
            if ($request['role'] > 8) {
                $role = 1;
            }
        }

        //Punch List Access and Role
        $punchlist_access = 0;
        $punchlist_role = 0;
        if ($request->has('access_to_punchlist')) {
            if ($request['access_to_punchlist'] === 'on') {
                $punchlist_access = 1;
            }
            $punchlist_role = (int)$request['punchlist_role'];
            if ($request['role'] > 8) {
                $punchlist_role = 1;
            }
        }

        $user = User::create([
            'name' => $request['name'],
            'surname' => $request['surname'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'company_id' => (int)$request->company,
            'confirmed' => $confirmed,
            'role' => $role,
            'access_to_tracker' => $tracker_access,
            'access_to_punchlist' => $punchlist_access,
            '$punchlist_role' => $punchlist_role,

        ]);

        $roles = $request->roles;
        $numOfRoles = count($roles);
        for ($i = 0; $i < $numOfRoles; $i++) {
            $user->assignRole($roles[$i]);
        }

        UserFilter::create([
            'user_id' => $user->id,
        ]);
        $message = $request['name'] . ' is now set up on figaro';

        return \Redirect::back()->with('message', $message)->withInput();
    }

    /**
     * @param Request $request
     * @return string|static
     * @throws \Illuminate\Foundation\Validation\ValidationException
     */
    protected function updateUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255',
            'roles' => 'required|min:1',
        ]);

        $confirmed = 0;
        if ($request['confirmed'] === 'on') {
            $confirmed = 1;
        }

        //Tracker Access and Role
        $tracker_access = 0;
        $role = 0;
        if ($request->has('access_to_tracker')) {
            //Tracker Access and Role
            $tracker_access = 0;
            if ($request['access_to_tracker'] === 'on') {
                $tracker_access = 1;
            }
            $role = (int)$request['role'];
            if ($request['role'] > 9) {
                //$role = 1;
            }
        }

        //Punch List Access and Role
        $punchlist_access = 0;
        $punchlist_role = 0;
        if ($request->has('access_to_punchlist')) {
            if ($request['access_to_punchlist'] === 'on') {
                $punchlist_access = 1;
            }
            $punchlist_role = (int)$request['punchlist_role'];
            if ($request['role'] > 9) {
                $punchlist_role = 1;
            }
        }

        $user = User::find($request['user_id']);
        $user->name = $request['name'];
        $user->surname = $request['surname'];
        $user->email = $request['email'];
        $user->company_id = $request['company'];
        $user->access_to_tracker = $tracker_access;
        $user->access_to_punchlist = $punchlist_access;
        $user->confirmed = $confirmed;

        $user->role = $role;
        $user->punchlist_role = $punchlist_role;

        //password
        if ($request['chk_reset_password'] === 'on') {
            if ($request['reset_password'] !== '') {
                $user->password = bcrypt($request['reset_password']);
                $user->password_changed = 0;
            }
        }

        $roles = $request->roles;
        $user->detachAllRoles();
        $numOfRoles = count($roles);
        for ($i = 0; $i < $numOfRoles; $i++) {
            $user->assignRole($roles[$i]);
        }

        $user->save();

        return $request['name'] . ' details Updated';
    }

    public function destroy($id){

        User::destroy($id);

        $message = 'User Deleted';

        //Remove user from any assigned

        return \Redirect::back()->with('message', $message)->withInput();

    }

    /**
     * --------------------------------------
     *        COMPANY
     * --------------------------------------
     */
    public function getCompanyDetails($id)
    {
        $company = Company::find($id);

        return view('rowsys.admin.company.details', [
            'company' => $company,
        ]);
    }

    protected function createCompany(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|filled|min:2|unique:company_list',
        ]);

        $company = new Company();
        $company->name = $request->name;
        $company->description = '';
        $company->short_name = '';
        $company->customer = 0;
        $company->save();

        // $company_message = 'Coma'
        return 'company added';
        //$message = $request['name'] . ' is now set up on figaro';
        //return \Redirect::back()->with('message', $message)->withInput();

    }

    protected function updateCompany(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $company = Company::find($request['company_id']);
        $company->name = $request['name'];
        $company->save();

        return $request['name'] . " details updated";

    }

    //Get the Users Role
    protected function userRoles($id)
    {
        $user = User::find($id);

        return $user->roles;
    }

    //Get the list of Users  for the selected Role
    protected function userListByRole($id)
    {
        $role = Role::find($id);

        $usersId = collect(DB::connection('mysqlTwo')->table('team_user')
            ->where('team_id',$id)
            ->select('user_id')->get())->pluck('user_id');

        $users = User::whereIn('id',$usersId->toArray())->get();
        return $users;

      //  return $role->users;
    }

    //Get the list of Users  for the selected Role
    protected function userListByTeam($id)
    {
        $team = Team::find($id);

        return $team->users();
    }

    //Clear project watchlist
    protected function clearProjectWatchlist()
    {
        Task::where('flag', 1)->update(['flag' => 0]);
        return redirect('dashboard/admin');
    }
    //Clear my watchlist
    protected function clearMyWatchlist()
    {
        $user = \Auth::user();
        $userFilter = UserFilter::where('user_id', $user->id)->first();
        $userFilter->task_flag_list = '';
        $userFilter->save();
        return redirect('dashboard/admin');
    }

    //Roles - make the Area table and ROles table line up

    protected function alignRoleAndAreaTables(){

        $areas = Area::get();

        //INSERT INTO roles (id, name ) SELECT id, name FROM areas ON DUPLICATE KEY UPDATE id = VALUES(id) ;

        foreach($areas as $area){



        }
        return " im here";
    }

}

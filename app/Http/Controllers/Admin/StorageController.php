<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Validator;



class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $directory = 'uploads';
        $fileList = Storage::allFiles($directory);
        //https://s3-ap-southeast-1.amazonaws.com/abbvie-asia-bucket/uploads/2016-04-26-photo.JPG
        $awsLink = 'https://s3-ap-southeast-1.amazonaws.com/figaro-testing/';

        //dd($fileList);
        return view('rowsys.admin.storage.storage', [
            'fileList' => $fileList,
            'awsLink' => $awsLink,
        ]);
        //return view('storage')->with('message','');
    }


    public function uploadFile(Request $request)
    {
        $this->validate($request, [
            //'filename' => 'required|min:1',
            'file' => 'required',
            //'file' => 'required|mimes:jpeg,bmp,png',
            'location' => 'required',
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
        }else{

            $message = 'Please select a file to upload';
            return Redirect::back()->with('message',$message)->withInput();

        }

        $date = Carbon::now();
        $fileExtension = $file->getClientOriginalExtension();
        $fileName = $file->getClientOriginalName();

        $location = $request->location;

        //Local Storage
        if($location === '2'){
            Storage::disk('local')->put('uploads/'.$date->toDateString().'-'.$fileName,
                file_get_contents($request->file('file')->getRealPath())
            );
        //S3 Bucket
        }else{
            Storage::put('uploads/'.$date->toDateString().'-'.$fileName,
                file_get_contents($request->file('file')->getRealPath())
            );
        }

        $message = 'File Uploaded';
        return Redirect::back()->with('message',$message)->withInput();

    }





    public function retrieveFile(Request $request)
    {
        //$contents = Storage::disk('local')->get('testImage.jpg');

        //Storage::put('file.jpg', $contents);



        //$contents = Storage::disk('local')->get($file);

        //$contents = Storage::get('logFile.txt');
        //Storage::put('file.jpg', $contents);


        //Storage::put('file.jpg', $file->__toString());


        //Storage::disk('local')->get('file.jpg')
        //Storage::disk('local')->append('logFile.txt', 'Appended Text56');

        // Storage::put('file.jpg', $file);

        //return Redirect::back()->with('message','File Uploaded');


        //Storage::disk('s3')->put('xx.jpg', $file->__toString());

        //Storage::put('file.jpg', 'yo!');

    }



}

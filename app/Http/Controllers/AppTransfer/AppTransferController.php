<?php

namespace App\Http\Controllers\AppTransfer;

use App\Models\Company;
use App\Models\Project\Floor;
use App\Models\Project\Room;
use App\Models\PunchList\Action;
use App\Models\PunchList\Category;
use App\Models\PunchList\Item;
use App\Models\PunchList\Priority;
use App\Models\Role;
use App\Models\User;
use App\Models\UserFilter;
use App\Rowsys\PunchList\Stats;
use App\Rowsys\PunchList\Search;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\System;
use App\Models\PunchList\Group;
use App\Models\PunchList\Type;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class AppTransferController extends Controller
{

    public function fromMainApp(Request $request,$id,$token)
    {

        if( $token === 'LHLJHLHJLKHYUTRTYRJFYGO&OUHLKG' ){

            Auth::loginUsingId($id);

            return  Redirect::to('/items/dashboard');

        }else{

            return 'You are not allowed here';
        }
    }


    public function toMainApp()
    {

        try{
            $userId = Auth::user()->id;

            $token = 'LHLJHLHJLKHYUTRTYRJFYGO&OUHLKG674567';

            $url = env('BASE_URL', 'http://punchlist.dev.');

            return \Redirect::to("$url/transfer/from/punchlist/" . $userId . "/" . $token);

        }

        catch(\Exception $e){

            return \Redirect::to("/");
        }


    }
}

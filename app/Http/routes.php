<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|--------------------------------------------------------------------------
*/
/*
|--------------------------------------------------------------------------
| HOME
|--------------------------------------------------------------------------
|
*/
Route::get('/', 'HomeController@index');
//\DB::listen(function ($query) {var_dump($query->sql); var_dump($query->bindings); var_dump($query->time);});
/*
|--------------------------------------------------------------------------
| GENERAL Routes
|--------------------------------------------------------------------------
|
*/
Route::group(['middleware' => ['web']], function () {
    Route::auth();
    Route::get('/welcome', 'HomeController@index');
    Session::put('project_id', 1);
});

/*
|--------------------------------------------------------------------------
| PROJECT  Routes
|--------------------------------------------------------------------------
|
*/

Route::group(['middleware' => ['web', 'auth']], function () {
    //Systems
    Route::get('/system', 'Tracker\Systems\SystemsController@index');
    Route::post('/system/new', 'Tracker\Systems\SystemsController@addSystem');
    Route::get('get-system-details/{id}', 'Tracker\Systems\SystemsController@getSystemDetails');
    Route::post('update-system-details/{id}', 'Tracker\Systems\SystemsController@updateSystemDetails');
    Route::get('delete-system/{id}', 'Tracker\Systems\SystemsController@deleteSystem');
    //Areas
    Route::get('/area', 'Tracker\Areas\AreasController@index');
    Route::post('/area/new', 'Tracker\Areas\AreasController@addArea');
    Route::get('get-area-details/{id}', 'Tracker\Areas\AreasController@getAreaDetails');
    Route::post('update-area-details/{id}', 'Tracker\Areas\AreasController@updateAreaDetails');
    Route::get('/delete-area/{id}', 'Tracker\Areas\AreasController@deleteArea');

    Route::get('list/components/system/{id}', 'Tracker\Systems\SystemsController@componentList');


});
/*
|--------------------------------------------------------------------------
| USER DETAILS -Common Routes across the APP
|--------------------------------------------------------------------------
|used by the user for updating their account
*/
Route::group(['middleware' => ['web', 'auth', 'confirmed']], function () {
    //User Account Settings
    Route::get('/users/get-user-settings-details/{user_id}', 'Admin\UsersController@getUserSettingsDetails');
    Route::post('/user/update/my-details', 'Admin\UsersController@updateUserMyDetails');
    Route::post('/user/update/my-password', 'Admin\UsersController@updateUserMyPassword');

    //Reset Password on initial login or password is reset
    Route::get('reset-password-main', 'Admin\UsersController@getResetPasswordOnInitialLoginPage');
    Route::post('/user/update/default-password', 'Admin\UsersController@updatePasswordOnInitialLogin');

    //User Roles
    Route::get('/user/roles/list/{id}', 'Admin\UsersController@userRoles');

    //Get the list of Users for the selected Role
    Route::get('/roles/user-list/{id}', 'Admin\UsersController@userListByRole');
    //Get the list of Users for the selected Role
    Route::get('/teams/user-list/{id}', 'Admin\UsersController@userListByTeam');
    //Clear watch Lists
    Route::get('/watchlist/my/clear', 'Admin\UsersController@clearMyWatchlist');
    Route::get('/watchlist/project/clear', 'Admin\UsersController@clearProjectWatchlist');

});
/*
|--------------------------------------------------------------------------
| ADMINISTRATION Routes
|--------------------------------------------------------------------------
|The admin part of the Application - only accessible if User is an
|an administrator (i.e level 9 or above)
*/
Route::group(['middleware' => ['web', 'auth', 'admin']], function () {
    // USERS
    Route::get('/admin/users', 'Admin\UsersController@index');
    Route::get('/admin/users/get-user-details/{user_id}', 'Admin\UsersController@getUserDetails');
    Route::post('/admin/add-user', 'Admin\UsersController@createUser');
    Route::post('/admin/update-user', 'Admin\UsersController@updateUser');
    Route::get('/user/register', function () {
        return view('auth.register');
    });
    Route::post('auth/register', 'Auth\AuthController@postRegister');
    Route::get('/user/Delete/{id}', 'Admin\UsersController@destroy');
    //COMPANY
    Route::post('/admin/add-company', 'Admin\UsersController@createCompany');
    Route::get('/admin/company/details/{user_id}', 'Admin\UsersController@getCompanyDetails');
    Route::post('/admin/update-company', 'Admin\UsersController@updateCompany');
    //ADMIN
    Route::get('/tracker/admin', function () {
        return view('rowsys.tracker.admin.index');
    });
    Route::get('/tracker/admin', 'Tracker\Tasks\CompileTasksController@index');
});
/*
|--------------------------------------------------------------------------
| TASK TRACKER  Routes
|--------------------------------------------------------------------------
| User must have access to the Tracker
*/
Route::group(['middleware' => ['web', 'auth', 'hasTrackerAccess']], function () {
    /*
    |--------------------------------------------------------------------------
    | DASHBOARD
    |--------------------------------------------------------------------------
    */
    Route::get('/dashboard/admin', 'HomeController@adminDashboard');
    Route::get('/dashboard/step-stats', 'HomeController@stepStatsForDashBoard');

    //User Filters
    Route::get('/user/filters/update', 'HomeController@updateUserFilters');
    /*
    |--------------------------------------------------------------------------
    | SEARCH & DROP DOWN FILTERS
    |--------------------------------------------------------------------------
    */
    //Search
    Route::get('get-task-search-ribbon', 'Tracker\Tasks\TaskController@getTaskSearchRibbon');

    Route::get('/get-task-search-form', 'Tracker\Search\SearchController@getTaskSearchForm');//Index
    Route::get('/get-task-search-stats', 'Tracker\Search\SearchController@getSearchStats');//Index

    Route::get('/get-filtered-groups', 'Tracker\Search\SearchController@getFilteredGroupsByArea');//Index
    Route::get('/get-filtered-types', 'Tracker\Search\SearchController@getFilteredTypesByGroup');//Index
    Route::get('/get-filtered-sub-types', 'Tracker\Search\SearchController@getFilteredSubTypesByType');//Index

    //Auto complete
    Route::get('/get-task-number-autocomplete-list/{tag}', 'Tracker\Search\SearchController@getNumberForAutoComplete');//Index
    Route::get('/get-schedule-number-autocomplete-list/{tag}', 'Tracker\Search\SearchController@getScheduleNumberForAutoComplete');

    /*
    |--------------------------------------------------------------------------
    | TASKS
    |--------------------------------------------------------------------------
    */
    // -- Read ---
    Route::get('tasks/table', 'Tracker\Tasks\TaskController@index');
    Route::get('tasks/list', 'Tracker\Tasks\TaskController@getTaskListView');
    Route::get('tasks/cards', 'Tracker\Tasks\TaskController@getTaskCardView');
    Route::get('tasks/server', 'Tracker\Tasks\TaskController@getTaskTableBuiltByServer');
    Route::get('/tasks/gantt', 'Tracker\Tasks\TaskController@getTasksGanttChart');
    // -- Read Partials---
    Route::get('get-task-details-view/{id}', 'Tracker\Tasks\TaskController@getTaskDetailsView');
    Route::get('get-update-task-status-form/{id}', 'Tracker\Tasks\TaskController@getUpdateStatusForm');
    Route::get('update-task-status-popup/{id}', 'Tracker\Tasks\TaskController@updateStatusPopup');
    Route::get('/task/role-details/{id}', 'Tracker\Tasks\TaskController@getUpdateTaskOwner');

    // -- Create --
    Route::get('task/create', 'Tracker\Tasks\TaskController@showAddTaskView');//Shows the View to add a task
    Route::get('add-task-test', 'Tracker\Tasks\TaskController@addTaskTest');//Shows the View to add a task
    Route::get('get-task-scheduler-dates', 'Tracker\Tasks\TaskController@getTaskSchedulerDates');//Shows the View to add a task
    Route::get('post-add-task', 'Tracker\Tasks\TaskController@addTask'); //Adds a task through AJAX
    Route::get('/check-if-task-number-exists', 'Tracker\Tasks\TaskController@checkIfTaskNumberExists');
    Route::get('/tasks/get-table', 'Tracker\Tasks\TaskController@taskTable');
    // -- Update  --
    Route::post('/post-update-task/{id}', 'Tracker\Tasks\TaskController@updateStatus');
    Route::post('/task/update-role/{id}', 'Tracker\Tasks\TaskController@updateRoleAndUser');
    Route::get('/get-bulk-task-update-form', 'Tracker\Tasks\TaskController@getBulkUpdateTaskForm');
    // -- Delete --
    Route::get('/task/destroy/{id}', 'Tracker\Tasks\TaskController@destroy');
    Route::post('bulk-delete-tasks', 'Tracker\Tasks\TaskController@bulkDeleteTasks');
    //post-delete-task
    // -- F --
    Route::get('/compile-all-tasks', 'Tracker\Tasks\TaskController@compileTasks');
    Route::get('/compile-task-tags', 'Tracker\Tasks\TaskController@compileTaskTags');
    // -- Errors --
    Route::get('/tasks/errors', 'Tracker\Tasks\TaskController@getTaskErrorsView');

    // Schedule Dates in JSON format - for updating the task  --
    Route::get('/get-schedule-dates-json', 'Tracker\Tasks\TaskController@getScheduleDateListJSON');
    //  -- REST Resource -  Viewing/Showing Tasks  --//Route::resource('tasks', 'Tracker\Tasks\TaskController');
    //Export Tasks to File
    Route::get('/tasks/export/file', 'Admin\FilesController@exportTasksToFile');
    /*-------------------------------------------------
    |                   EXPORT
     --------------------------------------------------*/
    Route::get('/export/csv/tasks', 'Tracker\Tasks\ExportTasksController@exportTasksToCsv');

    /*-------------------------------------------------
    |                   EARNED HOURS
     --------------------------------------------------*/
    Route::get('/task/earned/hrs/generate', 'Tracker\Tasks\TaskEarnedValueController@insertCurrentTasksEarned');

    /*
    |--------------------------------------------------------------------------
    | RULES AND PARAMETERS
    |--------------------------------------------------------------------------
    */
    //Add Rule
    Route::post('add-rule-for-stage', 'Tracker\Rules\TaskRuleController@addRuleForStage');

    Route::get('tasks/parameters', 'Tracker\Tasks\TaskController@getParametersView');
    Route::post('tasks/updates/parameters', 'Tracker\Tasks\TaskController@updateTaskParameters');

    //-- REST Resource -  Viewing/Showing Tasks  --
    Route::resource('taskRule', 'Tracker\Rules\TaskRuleController');

    //-- Add Task Rule --
    Route::get('/quick-add-task-rules', 'Tracker\Rules\TaskRuleController@addAllTaskRules');
    //-- View  a Task Rule  --
    Route::get('/tasks/rules/gen', 'Tracker\Rules\TaskRuleController@index');
    //-- View Schedule Dates View  --
    Route::get('/tasks/rules/scheduler-import', 'Tracker\Rules\TaskRuleController@scheduleDatesView');

    //-- Update a Task Rule  --
    Route::post('post-update-task-rule/{id}', 'Tracker\Rules\TaskRuleController@update');
    Route::get('task-target-value', function () {
        return view('tasks.rules.task.index');
    });
    //-- get task details - returns a View  --
    Route::get('get-task-rule-details/{id}', 'Tracker\Rules\TaskRuleController@returnTaskRuleDetail');

    // -- get task details - returns a View  --
    Route::get('update-task-rule/{id}', 'Tracker\Rules\TaskRuleController@updateRule');

    //Delete --
    Route::get('delete-rule/{id}', 'Tracker\Rules\TaskRuleController@deleteRule');
    /*
    |--------------------------------------------------------------------------
    | STEP PHASES - Review and approval steps
    |--------------------------------------------------------------------------
    */
    //Read
    Route::get('/tasks/step/phases', 'Tracker\StepPhases\StepPhaseController@index');
    Route::get('/tasks/step/phases/form/{id}', 'Tracker\StepPhases\StepPhaseController@updateForm');

    //Create
    Route::post('/tasks/step/phases/add', 'Tracker\StepPhases\StepPhaseController@create');

    //Update
    Route::post('/tasks/step/phases/update/{id}', 'Tracker\StepPhases\StepPhaseController@update');

    //Stats
    Route::post('/tasks/step/phases/owner-stats', 'Tracker\StepPhases\StepPhaseController@ownerStats');

    //Compile the Tasks & Step Phases
    Route::get('/tasks/step/phases/compile', 'Tracker\StepPhases\StepPhaseController@compileTaskStepPhases');
    Route::get('/tasks/task-step/phases/compile', 'Tracker\StepPhases\StepPhaseController@updateTaskTypeStepPhase');

    //
    /*
    |--------------------------------------------------------------------------
    | MODELS
    |--------------------------------------------------------------------------
    */
    //Groups
    Route::get('/task/group', 'Tracker\Categories\CategoriesController@showGroupView');
    Route::post('/task/group/new', 'Tracker\Categories\CategoriesController@addGroup');
    Route::get('get-group-details/{id}', 'Tracker\Categories\CategoriesController@getGroupDetails');
    Route::post('update-group-details/{id}', 'Tracker\Categories\CategoriesController@updateGroupDetails');
    Route::get('delete-task-group/{id}', 'Tracker\Categories\CategoriesController@deleteGroup');
    //Types
    Route::get('/task/type', 'Tracker\Categories\CategoriesController@showTypeView');
    Route::post('/task/type/new', 'Tracker\Categories\CategoriesController@addType');
    Route::get('get-type-details/{id}', 'Tracker\Categories\CategoriesController@getTypeDetails');
    Route::post('update-type-details/{id}', 'Tracker\Categories\CategoriesController@updateTypeDetails');
    Route::get('delete-task-type/{id}', 'Tracker\Categories\CategoriesController@deleteType');
    //Type Sub Types
    Route::get('/task/sub-type', 'Tracker\Categories\CategoriesController@showSubTypeView');
    Route::post('/task/sub-type/new', 'Tracker\Categories\CategoriesController@addSubType');
    Route::get('get-sub-type-details/{id}', 'Tracker\Categories\CategoriesController@getSubTypeDetails');
    Route::post('update-sub-type-details/{id}', 'Tracker\Categories\CategoriesController@updateSubTypeDetails');
    Route::get('delete-task-sub-type/{id}', 'Tracker\Categories\CategoriesController@deleteSubType');
    //NOT USED  --Categories ---
    Route::get('/category/new', 'Tracker\Categories\CategoriesController@index');
    Route::post('/category/new/group', 'Tracker\Categories\CategoriesController@addGroup');
    /*
    |--------------------------------------------------------------------------
    | GRAPHS (REPORTS)
    |--------------------------------------------------------------------------
    */
    //-- Project Stats and Graphs
    Route::get('/tasks/stats/project/bar', 'Tracker\Stats\StatsController@projectBarGraph');//Index
    Route::get('/tasks/stats/project-bar-data/{id}', 'Tracker\Stats\StatsController@getProjectBarHrsGraphData');//Graph Data
    //-- Area Stats and Graphs
    Route::get('/tasks/stats/areas/bar-graph', 'Tracker\Stats\StatsController@getAreaBarShell');//Index
    Route::get('/tasks/stats/areas/monthly-bar/{area_id}', 'Tracker\Stats\StatsController@getAreaMonthlyBar');//Index
    Route::get('/testing-line-data/{stage}', 'Tracker\Stats\StatsController@getMonthlyProjectedHrs');//Index

    //Route::get('/tasks/stats/area-bar-partial/{id}', 'Tracker\Stats\StatsController@getAreaBarPartial');//Graph Section
    Route::get('/tasks/stats/area-bar-data/{id}', 'Tracker\Stats\StatsController@getAreaBarHrsGraphData');//Graph Data
    //-- Groups Stats and Graphs
    Route::get('/tasks/stats/groups/bar-graph', 'Tracker\Stats\StatsController@getGroupBarShell');//Index
    Route::get('/tasks/stats/group-bar-data/{id}/{areaid}', 'Tracker\Stats\StatsController@getGroupBarHrsGraphData');//Graph Data
    //-- System Stats and Graphs
    Route::get('/tasks/stats/systems/bar-graph', 'Tracker\Stats\StatsController@getSystemBarShell');//Index
    Route::get('/tasks/stats/system-bar-data/{id}', 'Tracker\Stats\StatsController@getSystemBarHrsGraphData');//Graph Data

    //==== STATS =====
    Route::get('/tasks/stats', 'Tracker\Stats\StatsController@index');
    Route::get('/tasks/group-break-down', 'Tracker\Stats\StatsController@groupBreakDown');

    //--System Charts
    Route::get('/tasks/charts/system-gantt', 'Tracker\Systems\SystemsController@ganttChart');
    Route::get('/tasks/charts/systems/status', 'Tracker\Systems\SystemsController@statusTable');
    Route::get('/tasks/charts/systems/status/update-config', 'Tracker\Systems\SystemsController@updateSystemsOverviewConfiguration');
    Route::get('/tasks/charts/systems/status/config-table', 'Tracker\Systems\SystemsController@overviewReportConfigTable');
    Route::get('/tasks/charts/systems/status/clear-config-data', 'Tracker\Systems\SystemsController@clearSystemsOverviewConfiguration');

    //Category Tabulation Chart
    Route::get('/tasks/charts/category-tabulation', 'Tracker\Charts\ChartsController@taskCategoryTabulationReport');
    Route::get('/tasks/charts/category-tabulation-data', 'Tracker\Charts\ChartsController@taskCategoryTabulationReportData');

    //Testing Charts
    Route::get('/tasks/charts/test1/{id}', 'Tracker\Charts\ChartsController@getAreaBarAndLineChartData');
    Route::get('/tasks/s-charts/data/all', 'Tracker\Charts\ChartsController@areaBarAndLineChartData');

    Route::get('/tasks/charts/progress-overview', 'Tracker\Charts\ChartsController@areaProgressOverview');

    Route::get('/tasks/charts/test1', 'Tracker\Charts\ChartsController@test1');
    Route::get('/tasks/charts/test3', 'Tracker\Charts\ChartsController@test3');

    Route::get('/tasks/charts/test4', 'Tracker\Charts\ChartsController@test4');
    Route::get('/tasks/charts/user-chart-1-data', 'Tracker\Charts\ChartsController@userChart1Data');

    //Areas Weekly for Alexion
    Route::get('/tasks/charts/area/weekly', 'Tracker\Charts\AreasWeeklyChartController@getWeeklyAreaBarAndLineChartData');
    Route::get('/tasks/charts/area/weekly/search', 'Tracker\Charts\AreasWeeklyChartController@areaWeeklyBarAndLineChartData');


});
/*
|--------------------------------------------------------------------------
| PUNCH LIST  Routes
|--------------------------------------------------------------------------
| User must have access to the Punch List
*/
Route::group(['middleware' => ['web', 'auth']], function () {
    /*
    |--------------------------------------------------------------------------
    | DASHBOARD
    |--------------------------------------------------------------------------
    */
    Route::get('/items/dashboard', 'Punchlist\Items\ItemsController@dashboard');
    Route::get('/items', 'Punchlist\Items\ItemsController@index');
    Route::get('/items/dashboard/user', 'Punchlist\Items\ItemsController@userItems');
    Route::get('/items/dashboard/role', 'Punchlist\Items\ItemsController@roleItems');
    Route::get('/items/dashboard/links', 'Punchlist\Items\ItemsController@dashBoardQuickLinks');
    /*
    |--------------------------------------------------------------------------
    | ITEMS
    |--------------------------------------------------------------------------
    */
    //Create --
    Route::get('items/create', 'Punchlist\Items\ItemsController@createIndex');//Shows the View to add a task
    Route::get('/items/create/mobile', 'Punchlist\Items\ItemsController@getCreateIndexMobile');//Shows the View to add a task
    Route::get('item/create/validate', 'Punchlist\Items\ItemsController@validateFormData');//Shows the View to add a task
    Route::post('item/create', 'Punchlist\Items\ItemsController@create');//Shows the View to add a task
    //Read ---
    //Route::get('/items/table', 'Punchlist\Items\ItemsController@itemsTable');
    Route::get('/items/details/{id}', 'Punchlist\Items\ItemsController@getItemDetails');
    //Update
    Route::post('/items/update/owner-details/{id}', 'Punchlist\Items\ItemsController@updateItemOwnerDetails');
    Route::post('/items/update/responsible-details/{id}', 'Punchlist\Items\ItemsController@updateItemResponsibleDetails');
    Route::post('/items/update/assignee-details/{id}', 'Punchlist\Items\ItemsController@updateItemAssigneeDetails');
    //Delete
    Route::get('/items/delete/{id}', 'Punchlist\Items\ItemsController@destroy');

    /*
    |--------------------------------------------------------------------------
    | SEARCH & DROP DOWN FILTERS
    |--------------------------------------------------------------------------
    */
    //Search and Filters
    Route::get('/get-item-search-form', 'Punchlist\Search\SearchController@getSearchForm');
    Route::get('/get-item-search-stats', 'Punchlist\Search\SearchController@getSearchStats');
    Route::get('/items/find-similar', 'Punchlist\Search\SearchController@getListOfSimilarItems');
    //filtered lists
    Route::get('/get-filtered-item-types', 'Punchlist\Search\SearchController@getFilteredTypesByGroup');
    //Autocmplete
    Route::get('/get-item-number-autocomplete-list/{tag}', 'Punchlist\Search\SearchController@getNumberForAutoComplete');//Index

    /*
    |--------------------------------------------------------------------------
    | COMPILE
    |--------------------------------------------------------------------------
    */
    Route::get('/items/compile', 'Punchlist\Items\ItemsController@compileItems');

    /*
     |--------------------------------------------------------------------------
     | Attachments
     |--------------------------------------------------------------------------
     */
    //View
    Route::get('item/attachments/{item_id}', 'Punchlist\Items\ItemsController@getAttachmentsIndex');//Shows the View to add a task

    //Upload Single Isoloated file - like from the Upload the Item details Modal
    Route::post('/item/upload-file/{item_id}', 'Punchlist\Items\ItemsController@addIsolatedAttachment');//Shows the View to add a task

    /*
    |--------------------------------------------------------------------------
    | SYNC DATA
    |--------------------------------------------------------------------------
    */
    Route::resource('sync/punchlist', 'Punchlist\Sync\SyncDataController');//Shows the View to add a task
    Route::get('schedule-links/sync/{activity}', 'Punchlist\Sync\SyncDataController@syncScheduleLinks');//Shows the View to add a task
    Route::get('tools/sync', 'Punchlist\Sync\SyncDataController@toolsView');//Shows the View to add a task

});
/*
|--------------------------------------------------------------------------
| DESIGN DOCUMENTS Routes
|--------------------------------------------------------------------------
| User must have access to Design Docs
*/
Route::group(['middleware' => ['web', 'auth']], function () {

    /* ---------------  Documents  -------------*/
    //Dashboard
    Route::get('/design-documents/dashboard', 'DesignDocs\Docs\DocsController@dashboard');
    //Read
    Route::get('/design-documents/table', 'DesignDocs\Docs\DocsController@documentTable');
    //Create
    Route::get('/design-documents/Create/View', 'DesignDocs\Docs\DocsController@createView');
    //Update
    Route::get('get-design-doc-details/{id}', 'DesignDocs\Docs\DocsController@documentDetails');

    /* ---------------  Types  -------------*/
    //Resource
    Route::resource('/design-doc/types', 'DesignDocs\Types\TypesController');

    /* ---------------  Search  -------------*/
    //Search
    Route::get('/get-design-documents-search-form', 'DesignDocs\Search\SearchController@searchForm');

});
/*
|--------------------------------------------------------------------------
| ROSTER
|--------------------------------------------------------------------------
| User must have access to the Roster
*/
Route::group(['middleware' => ['web', 'auth']], function () {

    /* --------------- User  Roster  -------------*/
    //Resource
    Route::get('/roster/countries', 'Roster\RosterController@test1');
    Route::resource('/roster', 'Roster\RosterController');
    Route::get('/roster/user-hours/delete/{id}', 'Roster\RosterController@destroy');
    Route::post('/roster/update-hours/{id}', 'Roster\RosterController@updateHours');

    /* --------------- Area  Roster  -------------*/
    Route::resource('/roster/area', 'Roster\AreaRosterController');
    Route::post('/roster/area/update-hours/{id}', 'Roster\AreaRosterController@updateHours');
    Route::get('/roster/area-hours/delete/{id}', 'Roster\AreaRosterController@destroy');

    Route::get('/area-roster', 'Roster\AreaRosterController@showAreaRoster');

    //import Area
    Route::get('/roster/import/area', 'Roster\RosterController@showImportAreaData');
    Route::post('/roster/import/area', 'Roster\RosterController@ImportAreaData');
    //import User Data
    Route::get('/roster/import/user', 'Roster\RosterController@showImportUserData');
    Route::post('/roster/import/user', 'Roster\RosterController@ImportUserData');

});

/*
|--------------------------------------------------------------------------
| SCHEDULE Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['web', 'auth']], function () {

    //Read
    Route::get('/schedule/import', 'Schedule\ScheduleController@index');
    Route::get('/schedule/table', ['as' => '/schedule/table', 'uses' => 'Schedule\ScheduleController@table']);

    //Gantt Chart
    Route::get('schedule/chart/gantt', 'Schedule\ScheduleController@scheduleGanttChart');

    //SCHEDULE
    Route::get('/import/schedule', 'Admin\FilesController@getImportScheduleView');
    Route::post('/import/schedule/dates', 'Admin\FilesController@importScheduleDatesFromFileToScheduleTable');
    Route::get('/schedule/download-import-files', 'Admin\FilesController@downloadImportFile');

    Route::post('/schedule-date/add', 'Schedule\ScheduleController@addActivity');
    Route::post('/schedule-date/search', 'Schedule\ScheduleController@searchResults');
    Route::post('/schedule/update/{id}', 'Schedule\ScheduleController@updateActivity');
    Route::get('/schedule/delete/{id}', 'Schedule\ScheduleController@destroy');
    Route::get('schedule/update/form/{id}', 'Schedule\ScheduleController@updateForm');
    Route::get('schedule/chart', 'Schedule\ScheduleController@ganttChart');

    Route::get('/schedule/chart/task-data', 'Schedule\ScheduleController@taskDataForGanttChart');

    //System Milestones Link to Schedule
    Route::get('/schedule/system-link', 'Schedule\ScheduleController@systemLinkTable');
    Route::get('/schedule/system-link/edit', 'Schedule\ScheduleController@updateSystemLinkView');
    Route::get('/schedule/system-link/edit-form', 'Schedule\ScheduleController@milestoneEditForm');
    Route::post('/schedule/milestone/update', 'Schedule\ScheduleController@updateMilestone');

    //Export Data for Scheduler e.g Activity %, start date, finish date etc
    Route::get('/schedule/activity-summary/home', 'Schedule\ScheduleController@activitySummaryIndex');
    Route::get('/schedule/activity-summary/all-data', 'Schedule\ScheduleController@activityPercentData');
    Route::get('/schedule/activity-exceptions', 'Schedule\ScheduleController@activityExceptions');

});

/*
|--------------------------------------------------------------------------
| Global  Routes internal to the App e.g Filtering Systems by Areas, Types by Groups etc
|--------------------------------------------------------------------------
|
*/
Route::group(['middleware' => ['web', 'auth']], function () {


    //Schedule Dates for Systems when adding an Item
    Route::get('/item/system/dates/{system_id}', 'Punchlist\Items\ItemsController@getSystemScheduleDates');

    Route::get('/get-filtered-systems', 'Tracker\Search\SearchController@getFilteredSystemsByArea');//Index
    //Filtered User By Company
    Route::get('/get-filtered-user-by-company', 'Punchlist\Search\SearchController@getFilteredUsersByCompany');
    //Rooms
    Route::get('/get-filtered-rooms-by-floor', 'Punchlist\Search\SearchController@getFilteredRoomsByFloor');

});


/*
|--------------------------------------------------------------------------
| API  Routes
|--------------------------------------------------------------------------
|
*/
Route::group(['middleware' => 'web'], function () {

    // -- API Return Tasks  --
    Route::get('tasks-json/{startId}', 'Tracker\Tasks\TaskController@tasksInJsonFormat');
    Route::get('mob-task-details', 'Tracker\Tasks\TaskController@mobileTaskDetails');
});
/*
|--------------------------------------------------------------------------
| ROWSYS Admin Routes
|--------------------------------------------------------------------------
|The admin part of the Application - used by Rowsys
*/
Route::group(['middleware' => ['web', 'auth', 'admin']], function () {

    //Queues
    Route::get('queue', 'Admin\QueueController@index');
    Route::get('/compile-logs', 'Admin\QueueController@CompileLogFiles');

    //Storage
    Route::get('storage', 'Admin\StorageController@index');
    Route::post('file/upload', 'Admin\StorageController@uploadFile');

    //Console
    Route::get('console', 'Admin\ConsoleController@index');

    //Mail
    Route::get('mail', 'Admin\MailController@index');
    Route::get('mail-reminder/{id}', 'Admin\MailController@sendEmailReminder');
    Route::get('mail/gen-all-users', 'Admin\MailController@sendMailToAllUsers');

    //Tables
    Route::get('datatable/smart', 'Admin\TablesController@smartDataTables');
    Route::get('datatable/site', 'Admin\TablesController@siteDataTables');
    Route::get('/datatable/repsonsive', 'Admin\TablesController@responsiveDataTable');

    //Files
    Route::get('import', 'Admin\FilesController@index');
    Route::post('import/file', 'Admin\FilesController@importFile');
    Route::post('import-file', 'Admin\FilesController@importTasksFromFileToImportTable');
    Route::get('import/all-from-import-table', 'Admin\FilesController@addTasksToMainTableFromTempTable');
    Route::get('/import/tasks/report', 'Admin\FilesController@importTasksReport');
    //Library
    Route::get('/lib-components', function () {
        return view('rowsys._app.library.components');
    });

    /*
    |--------------------------------------------------------------------------
    | Testing
    |--------------------------------------------------------------------------
    */
    Route::get('test', function () {
        return view('rowsys._app.testing.test1');


        $img = Storage::disk('s3')->get('/uploads/PLI-95-2016-04-26.JPG');
        return \Intervention\Image\Facades\Image::make($img)->response();
        //dd($path);

        //return $img->getRealPath();
        //return view('rowsys._app.testing.test1')->with('img',$img->getRealPath());
    });
    Route::get('test1', function () {
        return view('rowsys._app.testing.testing_tables');
    });

});

/*
|--------------------------------------------------------------------------
| MISC
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => 'web', 'auth', 'hasTrackerAccess'], function () {

    /*
    |--------------------------------------------------------------------------
    | Help Forum
    |--------------------------------------------------------------------------
    */
    Route::get('/forum', function () {
        $suggestions = \App\Models\Forum::where('type', '=', 'suggestion')
            ->where('topic', '!=', '')
            ->where('status', '=', 0)
            ->get();
        $bugs = \App\Models\Forum::where('type', '=', 'bug')
            ->where('topic', '!=', '')
            //->orWhere('description','!=','')
            ->where('status', '=', 0)
            ->get();
        $completeList = \App\Models\Forum::where('status', '=', 1)->where('topic', '!=', '')->orderBy('created_at', 'DESC')->get();

        return view('rowsys.help.forum.index', ['suggestions' => $suggestions, 'bugs' => $bugs, 'completeList' => $completeList]);
    });
    Route::get('/forum/add', 'HomeController@addForum');
});


/*
|--------------------------------------------------------------------------
| UNKNOWN - Are these still being used?
|--------------------------------------------------------------------------
|
*/
Route::group(['middleware' => 'web', 'auth', 'hasTrackerAccess'], function () {
    //WHERE IN TASKS

    Route::get('/graph-examples', function () {
        return view('rowsys.tracker.tasks.graphs.examples.index');
    });
    Route::get('/area-graphs', function () {
        return view('rowsys.tracker.tasks.graphs.bar.area');
    });
    Route::get('/line-graphs', function () {
        return view('rowsys.tracker.tasks.graphs.line.line');
    });
    Route::get('/projects-hrs', function () {
        return view('rowsys.tracker.tasks.graphs.hours.hours');
    });
});


//App Transfer - from amin app

Route::group(['middleware' => 'web', 'auth'], function () {
//Transfer From Main App
    Route::get('/transfer/from/mainapp/{id}/{token}', 'AppTransfer\AppTransferController@fromMainApp');
    Route::get('/transfer/to/mainapp', 'AppTransfer\AppTransferController@toMainApp');
});





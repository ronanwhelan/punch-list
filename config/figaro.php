<?php

return [

    'company-name' => env('COMPANY_NAME', '*company name*'),
    'project-name' => env('PROJECT_NAME', '*project name*'),


    'multi-date' => env('MULTIDATE', 'false'),

    'over-due-tasks' => env('OVERDUE_TASKS', 'false'),

];

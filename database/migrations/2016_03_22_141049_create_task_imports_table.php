<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_imports', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('number');
            $table->string('description');
            $table->string('area');
            $table->string('system');
            $table->string('sys_desc');
            $table->string('task_group');
            $table->string('task_type');
            $table->string('task_name');
            $table->string('stage');
            $table->string('test_spec')->default('');
            $table->string('date_string');
            $table->dateTime('base_Date');
            $table->integer('count');
            $table->string('schedule_task_id');
            $table->string('schedule_task_desc');
            $table->dateTime('start_Date');
            $table->dateTime('finish_Date');
            $table->string('date_choice');
            $table->integer('date_lag');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('task_imports');

    }
}

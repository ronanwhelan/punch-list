<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePunchListActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punch_list_actions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('item_id')->unsigned();// Link to the Punch List Item

            $table->Integer('assigned_by_user_id')->unsigned();// Link to a user

            $table->Integer('assigned_company_id')->unsigned();// Link to a user
            $table->Integer('assigned_role_id')->unsigned();// Link to a user
            $table->Integer('assigned_user_id')->unsigned();// Link to a user

            $table->Integer('last_updated_by_id')->unsigned();// Link to a user
            $table->dateTime('last_updated_at')->default(\Carbon\Carbon::createFromDate(2016,01,01));//Complete Date

            $table->Integer('status');

            $table->string('action_taken');
            $table->string('requirement');
            $table->string('comment');
            $table->string('cause');

            //$table->tinyInteger('impact_to_schedule')->default(0);//1=yes, 0 =no

            //Complete
            $table->tinyInteger('complete')->default(0);
            $table->Integer('complete_by')->unsigned();// Link to a user
            $table->dateTime('complete_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));//Complete Date
            $table->string('close_out_comment');

            //Permits
            $table->tinyInteger('impact_to_schedule')->default(0);

            //Special Requirements e.g work at height, tag off, live work,
            $table->tinyInteger('work_at_height')->default(0);//maybe have a stting with all rewuirments that can be searched?


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('punch_list_actions');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_milestones', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('project_id')->unsigned();// Link to the Projects table
            $table->string('name');
            $table->string('description');
            $table->timestamps();
            //$table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('system_milestones');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}

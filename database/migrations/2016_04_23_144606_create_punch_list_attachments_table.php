<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePunchListAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punch_list_attachments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('item_id')->unsigned();// Link to the Punch List Item
            $table->string('file_name',100);
            $table->string('path');
            $table->string('folder');
            $table->string('file_type')->default('');
            $table->string('tag',100)->default('');
            $table->Integer('uploaded_by_id')->unsigned();// Link to an User
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('punch_list_attachments');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

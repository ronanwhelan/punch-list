<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roster_area_hours', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('area_id')->unsigned();// Link to the Area table
            $table->Integer('hours')->unsigned();// Link to the Users table
            $table->Integer('month_num')->unsigned();// Link to the Users table
            $table->string('month')->default('');
            $table->dateTime('month_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));
            $table->string('year')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('roster_area_hours');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}

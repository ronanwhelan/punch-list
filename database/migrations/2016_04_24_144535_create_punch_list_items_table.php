<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePunchListItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punch_list_items', function(Blueprint $table)
        {
            $table->increments('id');

            //Details
            $table->String('number',50)->index();//the Item Number
            $table->Integer('increment_number');//the assigned increment number
            $table->String('description')->default('');//The item Description
            $table->String('finding');//The item Description
            $table->String('stage');//The item Description
            $table->String('action');//The item Action to take
            $table->String('note');//The item Action to take

            //Class
            $table->Integer('category_id')->unsigned();// e.g type, kind

            //Owner
            $table->Integer('raised_by_company_id')->unsigned();// Link to a user
            $table->Integer('raised_by_user_id')->unsigned();// Link to a user
            $table->Integer('raised_by_role_id')->unsigned();// Link to a users roles
            $table->Integer('owner_updated_by_id')->unsigned();// Link to a users roles
            $table->dateTime('owner_updated_date')->default(\Carbon\Carbon::now());
            $table->dateTime('raised_date')->default(\Carbon\Carbon::now());
            $table->Integer('closed_by_id')->unsigned();// Link to a user
            $table->dateTime('closed_date')->default(\Carbon\Carbon::now());

            //Status
            $table->Integer('own_status')->unsigned();//item stage 1=open, 2=closed 3=forget

            //Priority
            $table->Integer('priority_id')->unsigned();// Link to Item Priority  e.g A or B or High ,Med, Low

            //Responsible Party
            $table->Integer('res_company_id')->unsigned();// Link to a user - The group that owns the item
            $table->Integer('res_user_id')->unsigned();// Link to a user - The user that owns the item
            $table->Integer('res_role_id')->unsigned();// Link to the roles
            $table->String('res_cause');//The cause of the item
            $table->String('res_action');//The item Action to take
            $table->String('res_note');//The item Action to take
            $table->dateTime('res_target_date')->default(\Carbon\Carbon::createFromDate(2000,01,01));//A date Entered by the User by passing the Schedule Date
            $table->Integer('res_status')->unsigned();//item stage 1=raised, 2=assigned, 3=in-progress, 4=complete
            $table->Integer('res_updated_by_id')->unsigned();// Who updated the details e.g status
            $table->dateTime('res_updated_date')->default(\Carbon\Carbon::now());
            $table->dateTime('res_complete_date')->default(\Carbon\Carbon::now());
            $table->Integer('res_complete_by_id')->unsigned();// Link to a user
            $table->Integer('assigned_company_id')->unsigned()->default(0);// Link to a company
            //$table->Integer('assigned_doer_group_id')->unsigned();// Link to a user -The group that carries out the work
            //$table->Integer('assigned_doer_user_id')->unsigned();// Link to a user- The user assinged to carry out the work
            //$table->dateTime('assigned_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));//The date the Assignee mut have the job complete by

            //System
            $table->Integer('area_id')->unsigned();// Link to an Area
            $table->Integer('system_id')->unsigned();// Link to the System
            $table->Integer('component_id')->unsigned()->nullable();// Link to the Component
            //Location
            $table->String('equipment_tag',50)->default('');//equipment tag
            $table->String('system_level',50)->default('');//the System Floor Level
            $table->String('system_extra_info',100)->default('');//any thing extra to add.
            $table->String('building',50)->default('');//the Building location
            $table->String('floor')->default('');//the Building Floor location
            $table->String('room')->default('');//the Building  Room location
            $table->String('other_location',100)->default('');//Allows the user to put in a free location

            //Phase the item was recorded in
            $table->String('phase_recorded_in',50)->default('');//the Phase the System was in when the punch was taken

            //Referenced Dos - e.g Drawing Number
            $table->String('drawing_reference',100)->default('');//Documents that may be referenced by the Item
            $table->String('extra_info',100)->default('');//any thing extra to add.

            //Group & Type
            $table->Integer('group_id')->unsigned(); // Link to an Item Types Group
            $table->Integer('type_id')->unsigned(); // Link to a Item type

            //linked Milestone
            $table->Integer('linked_milestone')->unsigned()->default(0);// The Linked Milestone
           // $table->dateTime('category_selected_date')->nullable();//If Cat C is selected the entered date

            //Due Date and Lag
            $table->tinyInteger('lag_days')->default(0);// the number of days to lag the from the target date(scheduled date)
            $table->dateTime('due_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));//The Scheduled Date
            $table->dateTime('original_due_date')->default(\Carbon\Carbon::createFromDate(2016,01,01));//The original due date

            //Status and Completion
            $table->tinyInteger('complete')->default(0);
            $table->tinyInteger('percent_complete')->default(0);
            $table->String('completion_note')->default('');//A note to add about the Completion
            $table->tinyInteger('forget')->default(0);//1 = forget

            $table->Integer('last_updated_by_id')->unsigned();// Link to a user
            $table->dateTime('last_updated_at')->default(\Carbon\Carbon::createFromDate(2016,01,01));

            //Project
            $table->Integer('project_id')->unsigned();// Link to the Projects table

            //Privacy
            $table->Integer('privacy')->unsigned()->default(1);// 0 = private, 1 = public viewing

            //Error - Compile error
            $table->Integer('error')->unsigned()->default(0);// 0 = no error , 1 = Schedule link Error

            //Urgent
            $table->Integer('urgent')->unsigned()->default(0);// 1 = urgent
            //Important
            $table->Integer('important')->unsigned()->default(0);// 1 = important

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('punch_list_items');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpForumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_forums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('topic',200);
            $table->longText('description');
            $table->string('area',50);
            $table->string('type',50);//e.g nice to have, important
            $table->string('device',50);
            $table->string('platform',80);
            $table->integer('status');
            $table->string('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('help_forums');
    }
}

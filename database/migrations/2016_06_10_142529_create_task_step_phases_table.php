<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskStepPhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_step_phases', function(Blueprint $table)
        {
            $table->increments('id');

            $table->Integer('task_id')->unsigned()->default(1);// Link to the task in teh task table
            $table->Integer('task_type_step_id')->unsigned()->default(1);// Link to the step phase in the task_type_step_phases

            $table->integer('step_number')->default(1);//the associated step number for the task e.g Gen = 1, Rev = 2 etc
            $table->integer('phase_count')->default(1);//how many phases are applicable e.g 2 = the is first two phases only

            $table->integer('status')->default(0);//the percentage status of the step - to populate the task table
            $table->integer('current_owner_id')->default(1);//the current owner for the step i.e who is responsible now

            $table->integer('break_down_count')->default(0);//if the phase is just broken down to a number of phases - this is how many phases e.g 4 25% x 4

            $table->Integer('p1_owner_id')->unsigned()->default(1);//Phase 1-  Link to the owners e.g Automation, Commissioning
            $table->integer('p1_status')->default(0);//the step percentage or complete or not?

            $table->Integer('p2_owner_id')->unsigned()->default(1);//Phase 2 -  Link to the owners e.g Automation, Commissioning
            $table->integer('p2_status')->default(0);//the step percentage or complete or not?

            $table->Integer('p3_owner_id')->unsigned()->default(1);//Phase 3 -  Link to the owners e.g Automation, Commissioning
            $table->integer('p3_status')->default(0);//the step percentage or complete or not?

            $table->Integer('p4_owner_id')->unsigned()->default(1);//Phase 4 -  Link to the owners e.g Automation, Commissioning
            $table->integer('p4_status')->default(0);//the step percentage or complete or not?

            $table->Integer('p5_owner_id')->unsigned()->default(1);//Phase 5 -  Link to the owners e.g Automation, Commissioning
            $table->integer('p5_status')->default(0);//the step percentage or complete or not?

            $table->Integer('p6_owner_id')->unsigned()->default(1);//Phase 6 -  Link to the owners e.g Automation, Commissioning
            $table->integer('p6_status')->default(0);//the step percentage or complete or not?

            $table->Integer('p7_owner_id')->unsigned()->default(1);//Phase 7 -  Link to the owners e.g Automation, Commissioning
            $table->integer('p7_status')->default(0);//the step percentage or complete or not?

            $table->Integer('p8_owner_id')->unsigned()->default(1);//Phase 8 -  Link to the owners e.g Automation, Commissioning
            $table->integer('p8_status')->default(0);//the step percentage or complete or not?

            $table->Integer('p9_owner_id')->unsigned()->default(1);//Phase 9 -  Link to the owners e.g Automation, Commissioning
            $table->integer('p9_status')->default(0);//the step percentage or complete or not?

            $table->Integer('p10_owner_id')->unsigned()->default(1);//Phase 10 -  Link to the owners e.g Automation, Commissioning
            $table->integer('p10_status')->default(0);//the step percentage or complete or not?


            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('task_step_phases');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePunchListGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punch_list_groups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('project_id')->unsigned();// Link to the Projects table
            $table->string('name');
            $table->string('description');
            $table->timestamps();
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('punch_list_groups');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

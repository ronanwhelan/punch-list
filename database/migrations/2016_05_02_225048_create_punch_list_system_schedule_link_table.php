<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePunchListSystemScheduleLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punch_list_system_schedule_links', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('system_id')->unsigned();// Link to the Systems

            //when looking for the dates for the milestones for the punchlist
            //to get the status of the step, look for the systems status of task type
            //for the stage. if the task type is N/A move to next milestone
            // If the step is complete then move to the next milestone
            //If the step is applicable and not complete, get the Schedule date
            //for the schedule link, its date choice e.g start or finish and add the lag or lead

            $table->string('m1_schedule_link_number');//The link to the schedule
            $table->integer('m1_lag')->default(0);//= or - days from schedule link date
            $table->string('m1_schedule_choice')->default('finish');//start or finish
            $table->integer('m1_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m1_task_type_id')->default(1);//link to task type

            $table->string('m2_schedule_link_number');//The link to the schedule
            $table->integer('m2_lag')->default(0);//= or - days from schedule link date
            $table->string('m2_schedule_choice')->default('finish');//start or finish
            $table->integer('m2_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m2_task_type_id')->default(1);//link to task type

            $table->string('m3_schedule_link_number');//The link to the schedule
            $table->integer('m3_lag')->default(0);//= or - days from schedule link date
            $table->string('m3_schedule_choice')->default('finish');//start or finish
            $table->integer('m3_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m3_task_type_id')->default(1);//link to task type

            $table->string('m4_schedule_link_number');//The link to the schedule
            $table->integer('m4_lag')->default(0);//= or - days from schedule link date
            $table->string('m4_schedule_choice')->default('finish');//start or finish
            $table->integer('m4_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m4_task_type_id')->default(1);//link to task type

            $table->string('m5_schedule_link_number');//The link to the schedule
            $table->integer('m5_lag')->default(0);//= or - days from schedule link date
            $table->string('m5_schedule_choice')->default('finish');//start or finish
            $table->integer('m5_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m5_task_type_id')->default(1);//link to task type

            $table->string('m6_schedule_link_number');//The link to the schedule
            $table->integer('m6_lag')->default(0);//= or - days from schedule link date
            $table->string('m6_schedule_choice')->default('finish');//start or finish
            $table->integer('m6_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m6_task_type_id')->default(1);//link to task type

            $table->string('m7_schedule_link_number');//The link to the schedule
            $table->integer('m7_lag')->default(0);//= or - days from schedule link date
            $table->string('m7_schedule_choice')->default('finish');//start or finish
            $table->integer('m7_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m7_task_type_id')->default(1);//link to task type

            $table->string('m8_schedule_link_number');//The link to the schedule
            $table->integer('m8_lag')->default(0);//= or - days from schedule link date
            $table->string('m8_schedule_choice')->default('finish');//start or finish
            $table->integer('m8_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m8_task_type_id')->default(1);//link to task type

            $table->string('m9_schedule_link_number');//The link to the schedule
            $table->integer('m9_lag')->default(0);//= or - days from schedule link date
            $table->string('m9_schedule_choice')->default('finish');//start or finish
            $table->integer('m9_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m9_task_type_id')->default(1);//link to task type

            $table->string('m10_schedule_link_number');//The link to the schedule
            $table->integer('m10_lag')->default(0);//= or - days from schedule link date
            $table->string('m10_schedule_choice')->default('finish');//start or finish
            $table->integer('m10_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m10_task_type_id')->default(1);//link to task type

            $table->string('m11_schedule_link_number');//The link to the schedule
            $table->integer('m11_lag')->default(0);//= or - days from schedule link date
            $table->string('m11_schedule_choice')->default('finish');//start or finish
            $table->integer('m11_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m11_task_type_id')->default(1);//link to task type

            $table->string('m12_schedule_link_number');//The link to the schedule
            $table->integer('m12_lag')->default(0);//= or - days from schedule link date
            $table->string('m12_schedule_choice')->default('finish');//start or finish
            $table->integer('m12_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m12_task_type_id')->default(1);//link to task type

            $table->string('m13_schedule_link_number');//The link to the schedule
            $table->integer('m13_lag')->default(0);//= or - days from schedule link date
            $table->string('m13_schedule_choice')->default('finish');//start or finish
            $table->integer('m13_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m13_task_type_id')->default(1);//link to task type

            $table->string('m14_schedule_link_number');//The link to the schedule
            $table->integer('m14_lag')->default(0);//= or - days from schedule link date
            $table->string('m14_schedule_choice')->default('finish');//start or finish
            $table->integer('m14_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m14_task_type_id')->default(1);//link to task type

            $table->string('m15_schedule_link_number');//The link to the schedule
            $table->integer('m15_lag')->default(0);//= or - days from schedule link date
            $table->string('m15_schedule_choice')->default('finish');//start or finish
            $table->integer('m15_stage_id')->default(1);//link to stage e.g Prep or Exec
            $table->integer('m15_task_type_id')->default(1);//link to task type

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('punch_list_system_schedule_links');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

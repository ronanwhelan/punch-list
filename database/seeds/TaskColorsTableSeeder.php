<?php

use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 13/1/16
 * Time: 3:43 PM
 */

class TaskColorsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('task_colors')->delete();

        $data = array(
            array(
                'value' => 'N/A',
                'description' => 'Not Applicable',
                'bkg_color' => 'white',
                'text_color' => 'black',

                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'value' => '0',
                'description' => '0% Complete',
                'bkg_color' => 'lightgray',
                'text_color' => 'lightgray',

                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'value' => '50',
                'description' => '50% Complete',
                'bkg_color' => 'lightgray',
                'text_color' => 'lightgray',

                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'value' => '100',
                'description' => '100% Complete',
                'bkg_color' => 'lightgreen',
                'text_color' => 'white',

                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),

        );

        DB::table('task_colors')->insert($data);
    }

}
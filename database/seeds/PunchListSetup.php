<?php
use Illuminate\Database\Seeder;
class PunchListSetup extends Seeder {

    public function run()
    {
        //Categories
        DB::table('punch_list_priorities')->delete();
        $data = array(
            array(
                'name'      => 'A',
                'short_name'      => 'Cat-A',
                'description'   => 'Must be done by next milestone (not selectable)',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'name'      => 'B',
                'short_name'      => 'Cat-B',
                'description'   => 'Choose the milestone date',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'name'      => 'C',
                'short_name'      => 'Cat-C',
                'description'   => 'Wish list. Choose a milestone or enter a date.',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('punch_list_priorities')->insert( $data );


        //Genres
        DB::table('punch_list_categories')->delete();
        $data = array(
            array(
                'name'    => 'Quality Observation Report',
                'short_name' => 'QOR',
                'description'=> '',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'name'      => 'Outstanding Work List',
                'short_name'      => 'OWL',
                'description'   => '',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );

        DB::table('punch_list_categories')->insert($data);


        //Groups
        DB::table('punch_list_groups')->delete();
        $data = array(
            array(
                'project_id' => 1,
                'name' => 'Electrical',
                'description'=> '',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'Construction',
                'description'=> '',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );

        DB::table('punch_list_groups')->insert($data);

        //Groups
        DB::table('punch_list_types')->delete();
        $data = array(
            array(
                'project_id' => 1,
                'name' => 'Cable Tying',
                'short_name' => 'CBL-TIE',
                'description'=> '',
                'group_id'=> 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'Piping',
                'short_name' => 'PIPE',
                'description'=> '',
                'group_id'=> 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );

        DB::table('punch_list_types')->insert($data);

    }


}

<?php

use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 9/05/2014
 * Time: 1:45 PM
 */

class ProjectsTableSeeder extends Seeder  {

    public function run()
    {
        DB::table('projects')->delete();


        $projects = array(
            array(

                'name' => 'Singapore',
                'description' => 'Tuas Singapore',
                'status' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(

                'name' => 'Project 0001',
                'description' => 'Project Description 0001',
                'status' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );


        DB::table('projects')->insert($projects);

    }

} 
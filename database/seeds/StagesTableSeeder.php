<?php

use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 13/1/16
 * Time: 3:43 PM
 */

class StagesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('stages')->delete();

        $data = array(
            array(
                'name' => 'Preparation',
                'short_name' => 'PREP',
                'description' => 'Preparation Stage',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),

            array(

                'name' => 'Execution',
                'short_name' => 'EXEC',
                'description' => 'Execution Stage',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );


        DB::table('stages')->insert($data);


    }

}
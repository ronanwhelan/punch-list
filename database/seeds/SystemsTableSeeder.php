<?php

use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 13/1/16
 * Time: 3:43 PM
 */

class SystemsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('systems')->delete();

        $data = array(
            array(
                'project_id' => 1,
                'tag' => 'N/A',
                'description' => 'Not Applicable',
                'area_id' => 1,
                'l1_id' => 1,
                'l2_id' => 1,
                'l3_id' => 1,
                'l4_id' => 1,
                'tag_divider' => '-',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'tag' => 'Dummy System',
                'description' => 'dummy system description',
                'area_id' => 1,
                'l1_id' => 1,
                'l2_id' => 1,
                'l3_id' => 1,
                'l4_id' => 1,
                'tag_divider' => '-',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );


        DB::table('systems')->insert($data);


    }

}
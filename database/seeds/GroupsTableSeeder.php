<?php

use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 13/1/16
 * Time: 3:43 PM
 */

class GroupsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('groups')->delete();

        $data = array(
            array(
                'project_id' => 1,
                'name' => 'PM',
                'description' => 'Project Management',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),

            array(
                'project_id' => 1,
                'name' => 'AUTO',
                'description' => 'Automation',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'CONS',
                'description' => 'Construction',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'COMM',
                'description' => 'Commissioning',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'DSGN',
                'description' => 'Design',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'project_id' => 1,
                'name' => 'QUAL',
                'description' => 'Qualification',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),


        );


        DB::table('groups')->insert($data);


    }

}
<!DOCTYPE html>
<html>
<body>
<form class="form-horizontal animated fadeInUp" role="form" method="POST" action="{{ url('/login') }}">
    {!! csrf_field() !!}
    <div class="login-form">
        <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}">
        <input type="password" class="form-control" name="password" id="password" value="Figaro33">
        <button type="submit" class="btn btn-lg btn-primary">Login <i class="fa fa-angle-right"></i></button>
    </div>
</form>

</body>
</html>
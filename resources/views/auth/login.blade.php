@extends('layouts.app')

@section('content')
<!-- Main Content -->
<section class="main-content">
  <div class="col-lg-8 col-lg-offset-2">
    <img src="img/branding/figaro-logo-white.png" class="figaro-logo img-responsive" alt="Figaro">

    <form class="form-horizontal animated fadeInUp" role="form" method="POST" action="{{ url('/login') }}">
      {!! csrf_field() !!}
      <div class="login-form">

          @if (\Session::has('message'))
              <div class="alert alert-info">{{ \Session::get('message') }}</div>
          @endif

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <label class="control-label">Email:</label>
          <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">

          @if ($errors->has('email'))
          <span class="help-block">
            {{ $errors->first('email') }}
          </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <label class="control-label">Password:</label>
          <input type="password" class="form-control" name="password" id="password">
          @if ($errors->has('password'))
          <span class="help-block">
            {{ $errors->first('password') }}
          </span>
          @endif
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-lg btn-primary">Login <i class="fa fa-angle-right"></i></button>
        </div>

        <div class="form-group">
          <label>
              <input type="checkbox" name="remember">
              <p>Remember Me</p>
          </label>

          <div class="col-md-6 col-md-offset-4 hide">
            <a class="btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
          </div>
        </div>

      </div>
    </form>

  </div>
</section><!-- end section -->
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Common Head Data  -->
    @include('rowsys._app.includes.structure.head.head')

    <!-- Common Title  -->
    @include('rowsys._app.includes.structure.title.title')

    <!-- Head Scripts  -->
    @include('rowsys._app.includes.styles.styles')

    <!-- PAGE RELATED - CSS  -->
    @yield('page_related_css')

    <!-- Head Scripts  -->
    @include('rowsys._app.includes.scripts.head.head_scripts')

    <!-- PAGE RELATED - HEAD JS  -->
    @yield('head_js')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
</head>
<body id="app-layout">
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                Figaro
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav hide">
                <li><a href="{{ url('/home') }}">Home</a></li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())

                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

@yield('content')


<!-- GLOBAL SCRIPTS -->
@include('rowsys._app.includes.scripts.global.global_scripts')

<!-- PUT LOCAL SCRIPTS HERE -->
@yield('local_scripts')
<script>
$('email').blur(function(){
    $('#password').focus();
});
$(document).ready(function () {


});
</script>

</body>
</html>

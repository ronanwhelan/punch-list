@extends('layouts.app')

@section('content')
    <section class="main-content figaro intro">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-align-center animated fadeInUp">
                    <img src="img/branding/figaro-logo-white.png" class="figaro-logo img-responsive" alt="Figaro">

                    <h3>Welcome to <abbr title="attribute">{{Config::get('figaro.company-name', '')}}  </abbr>Figaro.</h3>

                    @if (Auth::check())

                        @if(Auth::user()->password_changed === 0)
                            <h4 class="txt-color-white">{{ Auth::user()->name }}, please Update your password</h4>
                            <p class="txt-color-white">Password is required to be reset on initial login.</p>
                            <p class="txt-color-white font-xs hide">Once your password is changed you will not see this message again ,
                                unless it is rest by an Administrator</p>

                            <a class="btn btn-default" href="/reset-password-main"><i class=" fa fa-key" aria-hidden="true"></i> Change Password</a>
                        @else

                        <h4> Hi {{ Auth::user()->name }}, where would you like to go?</h4>


                        @if (Auth::user()->access_to_tracker === 1 )
                            <a href="/dashboard/admin" class="btn btn-primary"><i class="fa fa-book"></i>&nbsp;&nbsp;Task Tracker</a>
                            <br>
                        @endif
                        @if (Auth::user()->access_to_punchlist === 1 )
                            <a href="/items/dashboard" class="  btn btn-info"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Punchlist</a>
                            <br>
                        @endif

                            @if (Auth::user()->access_to_roster === 1 )
                                <a href="/roster/create" class="  btn btn-primary"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Roster</a>
                                <br>

                        <a href="#" class="hide  btn btn-success disabled"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Timesheet</a>
                            @endif

                            @if (Auth::user()->access_to_schedule === 1 )
                                <a href="/schedule/table" class=" btn btn-info"><i class="fa fa-align-left "></i>&nbsp;&nbsp;Schedule</a>
                                <br>
                            @endif


                        @if (Auth::user()->access_to_project === 1 )
                            <a href="/project/dashboard" class="btn btn-primary hide"><i class="fa fa-cubes"></i>&nbsp;&nbsp;Projects</a>

                        @endif

                        @if( Auth::user()->role > 6)
                            <a href="/admin/users" class=" btn btn-warning"><i class="fa fa-users"></i>&nbsp;&nbsp;Administration</a>
                        @else
                            <a href="/admin/users" class="disabled btn btn-warning"><i class="fa fa-key"></i>&nbsp;&nbsp;Administration</a>
                        @endif

                        @endif
                    @else
                        <a href="login" type="submit" class="btn btn-primary">Login <i class="fa fa-angle-right"></i></a>
                    @endif

                    @if (\Session::has('message'))
                        <div class="alert alert-info alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Oops! </strong> Seems to be a problem with your login.
                            <br>

                            <p class="text-danger">{{ \Session::get('message') }}</p>
                        </div>
                    @endif

                </div>

            </div>
        </div>

        <div id="" class="app-footer">
            <ul class="list-inline pad-10 text-align-center">
                <li> <a class="" href="http://pes-international.com/contact.htm"> <i class="fa fa-copyright"></i> 2016 PES International</a></li>
            </ul>
        </div>
    </section>
@endsection
@include('rowsys.admin.users.profile.settings_modal')
<!--  FOOTER -->

<script>

</script>
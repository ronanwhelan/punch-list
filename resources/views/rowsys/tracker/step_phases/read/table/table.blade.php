<div class="">
    <div class="row hide">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div id="buttons" class="font-xs"></div>
                    <div id="toggle_buttons" class="btn-group pull-right">
                        <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="0">Col 1</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="1">Col 2</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="2">Col 3</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">Col 4</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="4">Col 5</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Col 6</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="6">Col 7</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="7">Col 8</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="8">Col 9</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="9">Col 10</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">Task Review and Approval Steps</div>
        <div class="panel-body">
            <div class="row" id="table-section" style="opacity: .08">
                <div class="col-lg-12 font-xs">

                    <table id="data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr class="font-xs">
                            <th data-class="expand" class="font-xs">Stage</th>
                            <th data-hide="phone,tablet" class="font-xs">Category</th>
                            <th data-hide="phone,tablet" class="font-xs">Task Step Number</th>
                            <th data-hide="phone,tablet" class="font-xs">Increment Steps</th>
                            <th data-hide="phone,tablet" class="font-xs">Phase 1</th>
                            <th data-hide="phone,tablet" class="font-xs">Phase 2</th>
                            <th data-hide="phone,tablet" class="font-xs">Phase 3</th>
                            <th data-hide="phone,tablet" class="font-xs">Phase 4</th>
                            <th data-hide="phone,tablet" class="font-xs">Phase 5</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Phase 6</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Phase 7</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Phase 8</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Phase 9</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Phase 10</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- Insert For each to populate table--}}
                        @foreach($taskTypeStepsPhases as $taskTypeStepsPhase)
                            <tr class="font-xs" onclick="showUpdateModelModal('/tasks/step/phases/form/{{$taskTypeStepsPhase->id}}')">
                                <td>{{$taskTypeStepsPhase->stage->name}}</td>
                                <td>
                                   {{-- {{$taskTypeStepsPhase->id}}--}}
                                    {{$taskTypeStepsPhase->taskType->short_name}} - {{$taskTypeStepsPhase->taskType->name}}

                                    @if($taskTypeStepsPhase->break_down_count === 0
                                    && $taskTypeStepsPhase->p1_owner_id === 1
                                    && $taskTypeStepsPhase->p2_owner_id === 1
                                    && $taskTypeStepsPhase->p3_owner_id === 1
                                    && $taskTypeStepsPhase->p4_owner_id === 1
                                    && $taskTypeStepsPhase->p5_owner_id === 1
                                    && $taskTypeStepsPhase->p6_owner_id === 1
                                    && $taskTypeStepsPhase->p7_owner_id === 1
                                    && $taskTypeStepsPhase->p8_owner_id === 1
                                    && $taskTypeStepsPhase->p9_owner_id === 1
                                    && $taskTypeStepsPhase->p10_owner_id === 1)
                                        <p class="alert-danger"> <i class="fa fa-exclamation-circle"></i> Not configured correctly
                                        <br>Increment Steps is zero and all phases set to N/A</p>
                                        @endif

                                </td>
                                @if($taskTypeStepsPhase->step_number === 1)
                                <td>Prep/Exec (step 1)</td>
                                @elseif($taskTypeStepsPhase->step_number === 2)
                                <td>Review (step 2)</td>
                                @elseif($taskTypeStepsPhase->step_number === 3)
                                <td>Update (step 3)</td>
                                @elseif($taskTypeStepsPhase->step_number === 4)
                                <td>Sign Off (step 4)</td>
                                @endif
                                <td>{{$taskTypeStepsPhase->break_down_count}}</td>
                                <td>{{$taskTypeStepsPhase->p1Owner->name}}</td>
                                <td>{{$taskTypeStepsPhase->p2Owner->name}}</td>
                                <td>{{$taskTypeStepsPhase->p3Owner->name}}</td>
                                <td>{{$taskTypeStepsPhase->p4Owner->name}}</td>
                                <td>{{$taskTypeStepsPhase->p5Owner->name}}</td>
                                <td>{{$taskTypeStepsPhase->p6Owner->name}}</td>
                                <td>{{$taskTypeStepsPhase->p7Owner->name}}</td>
                                <td>{{$taskTypeStepsPhase->p8Owner->name}}</td>
                                <td>{{$taskTypeStepsPhase->p9Owner->name}}</td>
                                <td>{{$taskTypeStepsPhase->p10Owner->name}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="row">
    <div class="col-lg-12"><h1>Task Step Approval Phases</h1></div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title">Add Step Approval Breakdown</h3></div>
            <div class="panel-body">
                @if (Session::has('message') && old('areaFormActive') === '1')
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="fa fa-check"> </i>{{ Session::get('message') }}
                    </div>
                @endif
                <form id="add-step-phase-form" action="/area/new" method="post">
                    {{ csrf_field() }}
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Task Step</label>
                                    <select class="form-control selectpicker" name="step"
                                            data-style="btn-primary" title="Step">
                                        <optgroup label="Steps">
                                            <option value="1">Prep/Execution</option>
                                            <option value="2">Review</option>
                                            <option value="3">Update</option>
                                            <option value="4">Approval</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Stage</label>
                                    <select class="form-control selectpicker" name="stage"
                                            data-style="btn-primary" title="Stage">
                                        <optgroup label="Stages">
                                            @foreach ($stages as $stage)
                                                <option value="{{$stage->id}}">{{$stage->name}}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Category</label>
                                    <select class="form-control selectpicker" name="category"
                                            data-style="btn-primary" title="Category" data-live-search="true">
                                        <optgroup label="Categories">
                                            @foreach ($taskTypes as $type)
                                                <option value="{{$type->id}}">{{$type->name}} ({{$type->short_name}})</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">

                        <br>

                        <p class="lead"> Select an owner for each Phase of the step</p>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phase 1</label>
                                        <select class=" form-control selectpicker" name="phase_1"
                                                id="group" data-style="btn-primary">
                                            <option value="1"></option>
                                            @foreach ($groups as $group)
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phase 2</label>
                                        <select class=" form-control selectpicker" name="phase_2"
                                                id="group" data-style="btn-primary">
                                            <option value="1"></option>
                                            @foreach ($groups as $group)
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phase 3</label>
                                        <select class=" form-control selectpicker" name="phase_3"
                                                id="group" data-style="btn-primary">
                                            <option value="1"></option>
                                            @foreach ($groups as $group)
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phase 4</label>
                                        <select class=" form-control selectpicker" name="phase_4"
                                                id="group" data-style="btn-primary">
                                            <option value="1"></option>
                                            @foreach ($groups as $group)
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phase 5</label>
                                        <select class=" form-control selectpicker" name="phase_5"
                                                id="group" data-style="btn-primary">
                                            <option value="1"></option>
                                            @foreach ($groups as $group)
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="col-md-12">
                        <h2>OR</h2>

                        <p>Choose how many Increments the step is broken down into e.g 4 means every phase is worth 25%

                        <p class="text-warning" style="font-size: 60%">Note: Default is 2 increments (50%,100%). So there is no need to add a phase step if only 2 are required</p>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Increment Steps</label>
                                    <select class=" form-control selectpicker" name="phase_break_down_count"
                                            id="group" data-style="btn-primary">
                                        <option value="0"></option>
                                        <option value="4">4 Phases (increment by 25%)</option>
                                        <option value="5">5 Phases (increment  by 20%)</option>
                                        <option value="10">10 Phases (increment by 10%)</option>
                                        <option value="10" class="hide">20 Phases (increment by 5%)</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has() && old('areaFormActive') === '1' )
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="alert alert-info fadeIn hide " id="add-validation-errors"></div>
                    </div>

                    <div class="col-md-2">
                        <br>
                        <br>

                        <div class="form-group">
                            <!-- SERVER AND VALIDATION ERRORS -->

                            <input type="hidden" name="areaFormActive" value="1">
                            <button type="button" onclick="addStepPhase()" class="btn btn-success btn-block">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

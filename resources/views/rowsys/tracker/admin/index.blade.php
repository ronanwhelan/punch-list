@extends ('rowsys._app.layouts.app_admin')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')


            <!-- ==========================CONTENT STARTS HERE ========================== -->
            <h2>Administration Tools</h2>

            <div class="row animated fadeInUp">

                <div class="col-lg-4 col-md-6">
                    <div class="card-box">
                        <h3 class="header-title">Compile Tasks</h3>
                        <div class="widget-box">
                            <div class="widget-detail">
                                <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i>
                                    Applies the rule to each task e.g updates the target date to the current date in the schedule </div>
                                <button type="button" class="btn btn-success btn-block" id="btn-add-everything"
                                        onclick="addEverything()">Compile All Tasks <i
                                            id="import-spinner"
                                            class="fa  "></i>
                                </button>
                                @if (Session::has('message'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fa fa-check"> </i>{{ Session::get('message') }}
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div><!-- end .card-box -->
                </div><!-- end col -->


                <div class="col-lg-4 col-md-6">
                    <div class="card-box">
                        <h3 class="header-title">Compile Tasks Tags</h3>
                        <div class="widget-box">
                            <div class="widget-detail">
                                <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Updates all Task Tags and adds an incremental number to each task for easier search and reference.</div>
                                <a class="btn btn-info btn-block" href="javascript:void(0);">Compile Task Tags</a>
                            </div>
                        </div>
                    </div><!-- end .card-box -->
                </div><!-- end col -->

                <div class="col-lg-4 col-md-6">
                    <div class="card-box">
                        <h3 class="header-title">Re-Baseline</h3>
                        <div class="widget-box">
                            <div class="widget-detail">
                                <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Resets the target date for each task to current date in the Schedule</div>
                                <a class="btn btn-warning btn-block" href="javascript:void(0);">Re-Baseline</a>
                            </div>
                        </div>
                    </div><!-- end .card-box -->
                </div><!-- end col -->

                <div class="col-lg-4 col-md-6">
                    <div class="card-box">
                        <h3 class="header-title">Compile Task Step Phases</h3>
                        <div class="widget-box">
                            <div class="widget-detail">
                                <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Compiles all the Task Step Phases.
                                    Updates the Task % if needed and updates OR adds a step phase </div>
                                <a class="btn btn-warning btn-block" href="/tasks/step/phases/compile">Compile </a>
                            </div>
                        </div>
                    </div><!-- end .card-box -->
                </div><!-- end col -->
            </div><!-- end .row -->


            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-lg-12">

                        <h3>Last Compile Report</h3>

                        <div class="panel panel-primary">
                            <div class="panel-heading">Last Compile Report</div>
                            <div class="panel-body"><p>An overview of the last full task compilation</p>
                            <h3>Last Compile Finished <strong>
                                    @if(isset($report[5]))
                                        {{$report[5]}}@endif

                                </strong> </h3>
                            </div>
                            @if(sizeof($report) > 0)

                        <!-- Table -->
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Start Time</th>
                                    <th>Finish Time</th>
                                    <th>Duration</th>
                                    <th>Compiled Count</th>
                                    <th>Not Auto Compiled</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <th>{{$report[0]}}</th>
                                    <td>{{$report[1]}}</td>
                                    <td>{{$report[2]}} mins</td>
                                    <td>{{$report[3]}}</td>
                                    <td>{{$report[4]}}</td>
                                </tr>

                                </tbody>

                            </table>

                            <div class="col-md-12">
                                <br>
                                <p class=""><small><em>Note: not auto compiled means that the tasks were not part of the last scheduled compile
                                            e.g tasks that may have been added after an auto compile was run</em></small></p>
                            </div>



                                @else
                                    <div class="col-12-md" style="padding: 20px"><h3>No tasks are compiled</h3>
                                        <p> please compile all tasks using 'Compile all Tasks' button above </p>
                                        <p> If after a few minutes no tasks are compiled please contact Figaro team </p>
                                    </div>

                                @endif
                        </div>
                    </div>
                </div>
            </div>


            <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script>
        function addEverything() {
            //alert('im here');
            console.log('compile called');
            var compileAllButton = $('#btn-add-everything');
            compileAllButton.prop("disabled", true);
            $('#import-spinner').prop("disabled", true).addClass('fa-spinner fa-spin');
            //alert('compiled started');
            $.ajax({
                type: "GET",
                url: "/compile-all-tasks", success: function (result) {
                    console.log(result);
                    //alert('compile all tasks');
                    compileAllButton.text('Compiled started - this make take a few minutes');
                    //$('#import-all-alert').html('<h2></h2>').text(result);
                    //$('#import-all-alert').removeClass('hide').fadeIn(1500);
                    //$('#import-spinner').removeClass('fa-spinner fa-spin');
                }
            });
        }
        $(document).ready(function () {
            //
        });
    </script>

@stop

@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')


    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><h1>Area</h1></div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-info">
                    <div class="panel-heading"><h3 class="panel-title">Add a Area</h3></div>
                    <div class="panel-body">
                        @if (Session::has('message') && old('areaFormActive') === '1')
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <i class="fa fa-check"> </i>{{ Session::get('message') }}
                            </div>
                        @endif
                        <form action="/area/new" method="post">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" id="" name="areaName" value="{{old('areaName')}}" placeholder="Name">
                            </div>

                            <div class="form-group">
                                <label for="">Description</label>
                                <input type="text" class="form-control" id="" name="areaDescription" value="{{old('areaDescription')}}" placeholder="Description">
                            </div>

                            @if ($errors->has() && old('areaFormActive') === '1' )
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif

                            <input type="hidden" name="areaFormActive" value="1">
                            <button type="submit" class="btn btn-success ">Add Area</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Area List</div>
                    <div class="panel-body">
                        <!-- Table -->
                        <table width="100%" role="grid" id="table-list"
                               class="table table-striped table-hover table-bordered dataTable no-footer" style="opacity: .5">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Short Name</th>
                                <th>Description</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($areas as $area)
                                <tr onclick="showUpdateModelModal('/get-area-details/{{$area->id}}')">
                                    <th scope="row">{{$area->name}}</th>
                                    <th scope="row">{{$area->short_name}}</th>
                                    <td>{{$area->description}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('rowsys.tracker.includes.update_modal.update_modal')
    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')
    <script src="/js/rowsys/tracker/tasks/general.js"></script>

    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


    <script>
        var otable = $('#table-list');
        $(document).ready(function () {
            var responsiveHelper_mission_list_column = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";
            /* COLUMN FILTER  */
            otable.DataTable({
                //"bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                //"bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 50,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                //responsive: true,
                // paging: false,
                //"bStateSave": true // saves sort state using localStorage
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_mission_list_column) {
                        responsiveHelper_mission_list_column = new ResponsiveDatatablesHelper($('#table-list'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_mission_list_column.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_mission_list_column.respond();
                }

            });

            // Apply the filter
            $("#table-list thead th input[type=text]").on('keyup change', function () {
                otable.column($(this).parent().index() + ':visible').search(this.value).draw();
            });


            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = otable.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });
            $('#table-list').fadeTo(2000, '1');
            $('#loading-box-section').remove();
        });

    </script>

@stop

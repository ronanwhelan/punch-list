<form id="update-model-form" action="/update-system-details" method="POST">
    {{ csrf_field() }}

    <input type="hidden" id="task_id" name="update_task_id" value="{{$system->id}}">

    <div class="form-group">
        <label for="">Tag</label>
        <input type="text" class="form-control" id="name" name="update_name" value="{{$system->tag}}">
    </div>
    <div class="form-group">
        <label for="">Description</label>
        <textarea class="form-control" name="update_description" id="update_description" rows="4" cols="6">{{$system->description}}</textarea>
    </div>
    <div class="form-group">
        <label for="">Area</label>
        <select class="form-control selectpicker"
                id="update_parent" name="update_parent"
                data-style="btn-primary" title="Area">
            <optgroup label="Areas">
                @foreach ($areas as $area)
                    @if($area->id === $system->area_id)
                        <option selected value="{{$area->id}}">{{$area->name}}</option>
                    @else
                        <option value="{{$area->id}}">{{$area->name}}</option>
                    @endif
                @endforeach
            </optgroup>
        </select>
    </div>



    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="updateCategoryModel('update-model-form', '/update-system-details','{{$system->id}}')" class="btn btn-success">Update</button>

        @if($totalCount === 0 && Auth::user()->role > 7)
            <button type="button" onclick="showModelDeleteSection({{$totalCount}})" class="btn btn-danger pull-left">Delete</button>
        @endif

    </div>
    <div id="are-you-sure-delete-section" class="hide">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times; </span></button>
            <p class="">Are you Sure <i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;
                <button type="button" onclick="deleteSystem({{$system->id}})" class="btn btn-danger"> Yes</button>
            </p>
        </div>
    </div>
</form>

<div class="alert alert-warning" role="alert">

    {{$taskCount}} Tasks are associated to this system

    @if($itemCount > 0)
        {{$itemCount}} Punch list Items are associated to this system
    @endif

</div>
<p class="txt-color-orange font-xs"> Note: if you wish to delete this System, first delete all associated Tasks
    @if($itemCount > 0)
       and punch list items
    @endif
</p>
<script>
    $(document).ready(function () {

    });

    function deleteSystem(id) {
        $.ajax({
            type: 'GET',
            url: '/delete-system/' + id, success: function (result) {
                $('#update-modal').modal('hide');

                $.notify({
                    title: '<strong>Success!</strong><br>',
                    message: result + ""
                }, {
                    animate: {
                        enter: 'animated fadeInLeft',
                        exit: 'animated fadeOutRight'
                    },
                    type: 'success',
                    //offset: {x: 100, y: 100},
                    //placement: {from: "bottom"},
                    showProgressbar: false,
                    delay: 1500
                });

                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        });
    }

</script>



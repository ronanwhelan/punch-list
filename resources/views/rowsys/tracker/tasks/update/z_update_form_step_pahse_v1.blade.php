
<input type="hidden" id="task-number"  value="{{$task->number}}">
<input type="hidden" id="task-description"  value="{{$task->description}}">
<h3> {{$task->stage->description}} Steps</h3>
<div class="pull-right">
    <button type="button" onclick="reloadStatusForm({{$task->id}})" class="btn btn-link">
        <i class="fa fa-refresh"></i>
    </button>
</div>
<form class="form" role="form" id="update-task-form" method="POST">
    <input type="hidden" id="edit-task-id" name="edit-task-id" value="{{$task->id}}">

    {{ csrf_field() }}
    <div class="row pad-20">
        <div class="col-lg-12">
            <div class="panel panel-default" id="genExStatusPanel">
                <div class="">
                    <input type="hidden" class="" id="gen-percent" name="taskGenStatusUpdate"
                           value="{{$task->gen_perc}}">
                    <input type="hidden" class="" id="gen-applicable" value="{{$task->gen_applicable}}">
                    <div>
                        @if($task->stage_id === 1)
                            <h4>Preparation <span id="gen-percent-text">({{$task->gen_perc}}%)</span></h4>
                        @else
                            <h4> Execution<span id="gen-percent-text">({{$task->gen_perc}}%)</span></h4>
                        @endif
                    </div>
                </div>
                <div class="panel-body">
                    <input type="hidden" id="step1-updated" name="step1_updated"  value="0">
                    @if($task->gen_applicable == 1)
                        <div class="row">
                            <div class="" onclick="FillTaskStatusStep('gen')">
                                <div class="progress">
                                    <div id="gen-progress-bar" class="progress-bar progress-bar-success"
                                         role="progressbar"
                                         aria-valuenow="{{$task->gen_perc}}"
                                         aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width:{{$task->gen_perc}}%;min-width: 2em;height: 30px;">
                                        {{$task->gen_perc}}%
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(count($stepPhase1) > 0)
                            <input type="hidden" id="gen-has-phases" value="1">
                            <input type="hidden" id="step1-has-phases" value="1">
                            <input type="hidden" id="step1-phase-count" value="{{$stepPhase1->phase_count}}">
                            <input type="hidden" id="step1-status" value="{{$task->gen_perc}}" name="step_1_status">
                            <div class="row">
                                <ol class="breadcrumb">
                                    @for($i=1;$i<($stepPhase1->phase_count+1);$i++)
                                        <?php $phase = 'p' . $i . 'Owner';$status = 'p' . $i . '_status'; ?>
                                        @if($stepPhase1->$status === 1)
                                            <li id="step1-phase{{$i}}-owner" class="text-success">{{$stepPhase1->$phase->name}} <i class=" text-success fa fa-check"></i>  </li>
                                        @else
                                            <li id="step1-phase{{$i}}-owner" class="text-primary" >{{$stepPhase1->$phase->name}} <i class="text-primary fa fa-times"></i></li>
                                        @endif
                                        <input type="hidden" id="step1-phase{{$i}}-status" name="step1_phase{{$i}}_status" value="{{$stepPhase1->$status}}">
                                    @endfor
                                </ol>
                            </div>
                        @endif
                    @else
                        <p id="gen-percent-text" class="font-md">N/A</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row pad-20">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="">
                    <input type="hidden" class="" id="rev-percent" name="taskRevStatusUpdate"
                           value="{{$task->rev_perc}}">
                    <input type="hidden" class="" id="rev-applicable" value="{{$task->rev_applicable}}">
                    <div>
                        <h4>Review <span id="rev-percent-text">({{$task->rev_perc}}%)</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <input type="hidden" id="step2-updated" name="step2_updated"  value="0">
                    @if($task->rev_applicable == 1)
                        <div class="" onclick="FillTaskStatusStep('rev')">
                            <div class="row">
                                <div class="progress">
                                    <div id="rev-progress-bar" class="progress-bar progress-bar-success"
                                         role="progressbar"
                                         aria-valuenow="{{$task->rev_perc}}"
                                         aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width:{{$task->rev_perc}}%;min-width: 2em;height: 30px;">
                                        {{$task->rev_perc}}%
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(count($stepPhase2) > 0)
                            <input type="hidden" id="rev-has-phases" value="1">
                            <input type="hidden" id="step2-has-phases" value="1">
                            <input type="hidden" id="step2-phase-count" value="{{$stepPhase2->phase_count}}">
                            <input type="hidden" id="step2-status" value="{{$task->rev_perc}}" name="step_2_status">
                            <div class="row">
                                <ol class="breadcrumb">
                                    @for($i=1;$i<($stepPhase2->phase_count+1);$i++)
                                        <?php $phase = 'p' . $i . 'Owner';$status = 'p' . $i . '_status'; ?>
                                        @if($stepPhase2->$status === 1)
                                            <li id="step2-phase{{$i}}-owner" class="text-success">{{$stepPhase2->$phase->name}} <i class=" text-success fa fa-check"></i>  </li>
                                        @else
                                            <li id="step2-phase{{$i}}-owner" class="text-primary" >{{$stepPhase2->$phase->name}} <i class="text-primary fa fa-times"></i></li>
                                        @endif
                                        <input type="hidden" id="step2-phase{{$i}}-status" name="step2_phase{{$i}}_status" value="{{$stepPhase2->$status}}">
                                    @endfor
                                </ol>
                            </div>
                        @endif
                    @else
                        <p id="rev-percent-text" class="font-md">N/A</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row pad-20">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="">
                    <input type="hidden" class="" id="re-issu-percent" name="taskReIssueStatusUpdate"
                           value="{{$task->re_issu_perc}}">
                    <input type="hidden" class="" id="re-issu-applicable" value="{{$task->re_issu_applicable}}">

                    <div>
                        <h4>Re Issue <span id="re-issu-percent-text">({{$task->re_issu_perc}}%)</h4>
                    </div>
                </div>
                <div class="panel-body">
                    @if($task->re_issu_applicable == 1)
                        <div class="" onclick="FillTaskStatusStep('re-issu')">
                            <div class="row">
                                <div class="progress">
                                    <div id="re-issu-progress-bar" class="progress-bar progress-bar-success"
                                         role="progressbar"
                                         aria-valuenow="{{$task->re_issu_perc}}"
                                         aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width:{{$task->re_issu_perc}}%;min-width: 2em;height: 30px;">
                                        {{$task->re_issu_perc}}%
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(count($stepPhase3) > 0)
                            <input type="hidden" id="re-issu-has-phases" value="1">
                            <input type="hidden" id="step3-has-phases" value="1">
                            <input type="hidden" id="step3-phase-count" value="{{$stepPhase3->phase_count}}">
                            <input type="hidden" id="step3-status" value="{{$task->re_issu_perc}}" name="step_3_status">
                            <div class="row">
                                <ol class="breadcrumb">
                                    @for($i=1;$i<($stepPhase3->phase_count+1);$i++)
                                        <?php $phase = 'p' . $i . 'Owner';$status = 'p' . $i . '_status'; ?>
                                        @if($stepPhase3->$status === 1)
                                            <li id="step3-phase{{$i}}-owner" class="text-success">{{$stepPhase3->$phase->name}} <i class=" text-success fa fa-check"></i>  </li>
                                        @else
                                            <li id="step3-phase{{$i}}-owner" class="text-primary" >{{$stepPhase3->$phase->name}} <i class="text-primary fa fa-times"></i></li>
                                        @endif
                                        <input type="hidden" id="step3-phase{{$i}}-status" name="step3_phase{{$i}}_status" value="{{$stepPhase3->$status}}">
                                    @endfor
                                </ol>
                            </div>
                        @endif
                    @else
                        <p id="re-issu-percent-text" class="font-md">N/A</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row pad-20">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="">
                    <input type="hidden" class="" id="s-off-percent" name="taskSignOffStatusUpdate"
                           value="{{$task->s_off_perc}}">
                    <input type="hidden" class="" id="s-off-applicable" value="{{$task->s_off_applicable}}">

                    <div>
                        <h4>Sign Off <span id="s-off-percent-text">({{$task->s_off_perc}}%)</span>
                    </div>
                </div>
                <div class="panel-body">
                    @if($task->s_off_applicable == 1)
                        <div class="" onclick="FillTaskStatusStep('s-off')">
                            <div class="row">
                                <div class="progress">
                                    <div id="s-off-progress-bar" class="progress-bar progress-bar-success"
                                         role="progressbar"
                                         aria-valuenow="{{$task->s_off_perc}}"
                                         aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width:{{$task->s_off_perc}}%;min-width: 2em;height: 30px;">
                                        {{$task->s_off_perc}}%
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(count($stepPhase4) > 0)
                            <input type="hidden" id="s-off-has-phases" value="1">
                            <input type="hidden" id="step4-has-phases" value="1">
                            <input type="hidden" id="step4-phase-count" value="{{$stepPhase4->phase_count}}">
                            <input type="hidden" id="step4-status" value="{{$task->s_off_perc}}" name="step_4_status">
                            <div class="row">
                                <ol class="breadcrumb">
                                    @for($i=1;$i<($stepPhase4->phase_count+1);$i++)
                                        <?php $phase = 'p' . $i . 'Owner';$status = 'p' . $i . '_status'; ?>
                                        @if($stepPhase4->$status === 1)
                                            <li id="step4-phase{{$i}}-owner" class="text-success">{{$stepPhase4->$phase->name}} <i class=" text-success fa fa-check"></i>  </li>
                                        @else
                                            <li id="step4-phase{{$i}}-owner" class="text-primary" >{{$stepPhase4->$phase->name}} <i class="text-primary fa fa-times"></i></li>
                                        @endif
                                        <input type="hidden" id="step4-phase{{$i}}-status" name="step4_phase{{$i}}_status" value="{{$stepPhase4->$status}}">
                                    @endfor
                                </ol>
                            </div>
                        @endif
                    @else

                        <p id="re-issu-percent-text" class="font-md">N/A</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row pad-20">
        <div class="col-lg-12">
            <button type="button" class="btn btn-link-primary btn-block btn-lg" onclick="updateTask()">Save <i class="fa fa-save"></i></button>
        </div>
    </div>


    <div class="row pad-20">
        <div class="col-lg-12">
            <div id="statusRules" class="font-xs">
                <ul>
                    <li>Previous Step must be 100% or N/A before the next step can be changed</li>
                    <li>A step cannot be changed if at 100%</li>
                </ul>
            </div>
        </div>
    </div>

    <legend></legend>

    @if( Auth::user()->role > 5)

        <div class="row pad-20">
            <div class="col-md-12 ">
                <div id="overRide" class="font-xs">
                    <p> Over Ride to change the status of a step that is at 100% </p>
                    <button type="button" class="btn btn-warning" id="over-ride-button" onclick="overRideStatus()">Over Ride
                        Off
                    </button>
                </div>
            </div>
        </div>
    @endif

    @if( Auth::user()->role > 7)
        <hr>
        <div class="row pad-20">
            <div class="col-md-12 ">
                <div id="" class="font-xs">
                    <button type="button" class="btn btn-danger " id="delete-task-button" onclick="showModelDeleteSection(0)">Delete Task
                    </button>
                </div>
            </div>

        </div>
        <div id="are-you-sure-delete-section" class="hide">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times; </span></button>
                <p class="">Are you Sure <i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;
                    <button type="button" onclick="deleteTask({{$task->id}})" class="btn btn-danger"> Yes</button>
                </p>
            </div>
        </div>
    @endif

</form>

<script>
    $(document).ready(function () {

    });

    function deleteTask(id){
        $.ajax({
            type: 'GET',
            url: '/task/destroy/' + id, success: function (result) {
                $('#single-edit-modal').modal('hide');

                $.notify({
                    title: '<strong>Success!</strong><br>',
                    message: result + ""
                }, {
                    animate: {
                        enter: 'animated fadeInLeft',
                        exit: 'animated fadeOutRight'
                    },
                    type: 'success',
                    //offset: {x: 100, y: 100},
                    //placement: {from: "bottom"},
                    showProgressbar: false,
                    delay: 1500
                });

                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        });
    }

</script>


<div class="panel panel-default">
    <div class="panel-body">

        <form class="form" role="form" id="task-update-role-and-user-form" method="POST">
            <input type="hidden" id="edit-task-role-id" name="edit-task-role-id" value="{{$task->id}}">

            {{ csrf_field() }}

            <div class="form-group">
                <label for="add-on">Add On Text <small>(part of task number)</small></label>
                <input type="text" class="form-control" id="add-on"  name="add_on" value="{{$task->add_on}}">
            </div>
            <div class="form-group">
                <label for="add-on">Add On Description <small>(part of task description)</small></label>
                <input type="text" class="form-control" id="add-on-desc"  name="add_on_desc" value="{{$task->add_on_desc}}">
            </div>

            <div class="form-group">
                <label for="add-on">Linked Document Number</label>
                <input type="text" class="form-control" id="doc-number"  name="document_number" value="{{$task->document_number}}">
            </div>

            <div class="form-group">
                <label for="add-on">Note</label>
                <input type="text" class="form-control" id="note"  name="note" value="{{$extension->note}}">
            </div>

            <hr>
{{--
            <div class="form-group">
                <label for="add-on">Schedule Lead/Lag (+/- days)</label>
                <input type="number" class="form-control" id="schedule_lag_days"  name="schedule_lag_days" value="{{$task->schedule_lag_days}}">
            </div>
            <div class="form-group">
                <label for="add-on">Added Lead/Lag (+/- days)</label>
                <input type="number" class="form-control" id="lag_days"  name="lag_days" value="{{$task->lag_days}}">
            </div>
            <hr>--}}


            <h5><i class="fa fa-users"></i> Assigned Team:</h5>

            <div class="form-group">
                <select class=" form-control selectpicker"  @if(Auth::user()->role < 6) disabled @endif
                onchange="getUsersByRole(this.value,'attention_to')" data-style="btn-primary"
                        name="responsible_team" id="responsible_role" title="Responsible Party">
                    <option value=""></option>
                    @if(count($roles) > 0)
                        @foreach($roles as $role)
                            @if($role->id === $task->group_owner_id)
                                <option value="{{$role->id}}" selected>{{$role->name}}</option>
                            @else
                                <option value="{{$role->id}}">{{$role->name}}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="form-group ">
                <h5><i class="fa fa-user"></i> Individual:</h5>
                <select class=" form-control selectpicker" data-live-search="true" onchange="" data-style="btn-primary"
                        name="attention_to" id="attention_to" title="Responsible Individual">
                    <option value=""></option>
                    @if(count($users) > 0)
                        @foreach($users as $user)
                            @if($user->id === $task->assigned_user_id)
                                <option selected value="{{$user->id}}">{{$user->name}}  {{$user->surname}}</option>
                            @else
                                <option value="{{$user->id}}">{{$user->name}}  {{$user->surname}}</option>
                            @endif

                        @endforeach
                    @endif
                </select>
            </div>

            <br>

            <div class="form-group ">
                <button type="button" onclick="updateTaskRoleAndIndividual()" class="btn btn-block btn-success">Update</button>
            </div>
            <p style="font-size: 50%">Note: Save the data  before switching the
                tabs as the data on this tab will not be updated.</p>

        </form>

    </div>
</div>
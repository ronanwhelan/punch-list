<div class="row">
    <div class="col-lg-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Details  <i class="fa fa-caret-down"></i>
                        </a>
                    </h4>
                </div>


                <div id="collapseOne" class="panel-collapse " role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <div class="form-group">

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                    <select class=" form-control selectpicker" name="taskGroup"
                                            id="group" data-style="btn-primary" title="Owner"
                                            onchange="getAddTaskFilteredTypesByGroup()">
                                        <option value=""></option>
                                        @foreach ($groups as $group)
                                            <option value="{{$group->id}}">{{$group->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pad-20">
                                    <select class="form-control selectpicker" data-live-search="true" name="taskType" title="Category"
                                            id="type" data-style="btn-primary"
                                            onchange="getAddTaskFilteredSubTypeByType()">
                                        <option value=""></option>
                                        @foreach ($taskTypes as $taskType)
                                            <option value="{{$taskType->id}}">{{$taskType->short_name}}, {{$taskType->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 pad-20">
                                    <select class="form-control selectpicker" data-live-search="true" name="taskSubType" title="Sub Category"
                                            id="sub-type" data-style="btn-primary">
                                        <option value=""></option>
                                        @foreach ($taskSubTypes as $taskSubType)
                                            <option value="{{$taskSubType->id}}">{{$taskSubType->name}}, {{$taskSubType->description}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>
                        <button type="button" onclick="showHelpSection(1)" class="btn btn-info btn-xs"><i class="fa fa-info-circle"></i></button>

                        <div id="help-section-1" class="hide">
                            <p class="txt-color-orange font-xs">Note #1: The Owner can not be updated. It is used to the filter the Category.
                                The Owner can be updated by updating the Category Owner in the Build Section.
                            </p>

                            <p class="txt-color-orange font-xs">Note #2: If the Category is updated and the Sub Category is left empty, all
                                Tasks that are associated to a Sub category will be removed. If you wish to update a Category and Sub Category simply choose
                                the Sub Category, and Figaro will do the rest.
                            </p>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pad-20">
                                    <label>Add On Text</label>
                                    <input type="text" class="form-control" name="numberAddOn" id="number-add-on"/>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pad-20">
                                    <label>Add On Text Description</label>
                                    <input type="text" class="form-control" name="numberAddOnDescription" id="number-add-on-description"/>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5><i class="fa fa-users"></i> Responsible Team:</h5>
                                    <select class=" form-control selectpicker"  @if(Auth::user()->role < 5) disabled @endif
                                    onchange="getUsersByRole(this.value,'attention_to')" data-style="btn-primary"
                                            name="responsible_team" id="responsible_role" title="Responsible Party">
                                        <option value="0"></option>
                                        @if(count($areas) > 0)
                                            @foreach($areas as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <h5><i class="fa fa-user"></i> Individual:</h5>
                                    <select class=" form-control selectpicker" data-live-search="true" onchange="" data-style="btn-primary"
                                            name="attention_to" id="attention_to" title="Responsible Individual">
                                        <option value="0"></option>
                                        @if(count($users) > 0)
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}  {{$user->surname}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        @if( Auth::user()->role > 7)
                            <div class="row">
                                <hr>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <h5><i class="fa fa-cubes"></i> Systems:</h5>
                                        <select class=" form-control selectpicker" data-live-search="true" onchange="" data-style="btn-primary"
                                                name="system" id="system" title="Systems">
                                            <option value="0"></option>
                                                @foreach($systems as $system)
                                                    <option value="{{$system->id}}">{{$system->tag}} , {{$system->description}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


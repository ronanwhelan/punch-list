<!-- Modal -->
<div class="modal fade" id="update-parameter-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Task Parameters</h4>
            </div>
            <div class="modal-body">
                <div id="bulk-update-form-section">
                    <div class="text-align-center">
                        <i class="fa fa-spinner  fa-spin fa-5x"></i>Loading....<br><br></div>
                    <!-- populated by AJAX -->
                </div>


                <div class="panel panel-default">



                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                               aria-controls="collapseTwo">
                                Schedule Link and Hrs    <i class="fa fa-caret-down"></i>
                            </a>
                        </h4>
                    </div>

                    <div id="schedule-link-section" hidden="true">
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <div class="col-md-12">

                                    <div class="alert alert-info" role="alert">
                                        <i class="fa fa-info-circle"></i> Select checkbox to update parameter
                                    </div>
                                    {{--  Scheule Link --}}
                                    <h5>Schedule Link</h5>


                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input name="update_schedule_link_chk" id="update-schedule-link-chk" type="checkbox">Update Schedule Link
                                            </label>
                                        </div>
                                        <div id="schedulelink-section">
                                            <input type="text" class="form-control" id="schedule-number-input" name="scheduleNumberInput" value=""
                                                   placeholder="Start typing for suggestions">
                                            <br>
                                            <input type="hidden" value="" name="scheduleNumber" id="schedule-number">

                                            <div><i class="hide fa fa-spinner fa-spin"></i></div>
                                            <div class="form-group">
                                                <label for=""> Date Select</label>
                                                <select class=" form-control selectpicker" id="schedule_link_date" name="schedule_link_date" data-style="btn-primary">
                                                    <option value=""></option>
                                                    <option value="Start">Use Start date</option>
                                                    <option value="Finish">Use Finish Date</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for=""> Lag days (+/-)</label>

                                                <div class="input-group"><span class="input-group-addon"><input name="chk_schedule_link_lag" type="checkbox"
                                                                                                                aria-label="..."></span>
                                                    <input type="number" class="form-control" id="schedule_link_lag" name="schedule_link_lag" placeholder="e.g -30 , 45">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="  col-md-12">
                                    {{--  Hours --}}
                                    <h5>Hours</h5>
                                    <label for="">Target Hours</label>

                                    <div class="input-group"><span class="input-group-addon"><input name="chk_target_hrs" type="checkbox" aria-label="..."></span>
                                        <input type="number" class="form-control" id="target_hrs" name="target_hrs" value="0">
                                    </div>
                                    <br>

{{--                                    <div class="form-group">
                                        <label for="">Added Days (+ days)</label>

                                        <div class="input-group"><span class="input-group-addon"><input name="chk_general_lag_days" type="checkbox" aria-label="..."></span>
                                            <input type="number" class="form-control" id="general_lag_days" name="general_lag_days" value="0">
                                        </div>
                                        <br>

                                        <div class="checkbox">
                                            <label>
                                                <input name="check_lock_date" type="checkbox">Lock Date
                                            </label>
                                        </div>
                                    </div>--}}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Row start -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" id="update-selected-tasks-button" onclick="updateSelectedTasks('update-selected-tasks-form', '/tasks/updates/parameters')" class="btn btn-success">Update
                                    Selected Tasks
                                </button>

                            </div>
                        </div>

                    </div>
                </div>

                @if(Auth::user()->role > 5)
                    <!-- Row start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="button" onclick="showDeleteTaskSection()" class="btn btn-danger">Delete
                                Selected Tasks
                            </button>
                            <div id="are-you-sure-delete-task-section" class="hide">
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times; </span></button>
                                    <p class="">Are you Sure <i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;
                                        <button type="button" onclick="deleteTasks()" class="btn btn-danger"> Yes</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>

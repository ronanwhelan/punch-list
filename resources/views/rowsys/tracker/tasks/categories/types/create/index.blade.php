@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')


    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><h1>Task Category</h1></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                   aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-plus"></i> New Category
                                </a>
                            </h4>
                        </div>

                        <div id="collapseOne" class="panel-collapse collapse in " role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                @if (Session::has('rule-message'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fa fa-check">&nbsp; </i>{{ Session::get('rule-message') }}
                                    </div>
                                @endif


                                @if (Session::has('message') && old('typeFormActive') === '1')
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fa fa-check"> </i>{{ Session::get('message') }}
                                    </div>
                                @endif
                                <form action="/task/type/new" method="post">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="">Name</label>
                                        <input type="text" class="form-control" id="" name="typeName" value="{{old('typeName')}}" placeholder="e.g User Requirement Specification">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Short Name
                                            <small>(used to build task tag)</small>
                                        </label>
                                        <input type="text" class="form-control" id="" name="typeShortName" value="{{old('typeShortName')}}" placeholder="e.g URS,TSIQ">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <input type="text" class="form-control" id="" name="typeDescription" value="{{old('typeDescription')}}" placeholder="Description">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Owner</label>
                                        <select class=" form-control selectpicker" data-style="btn-primary" data-live-search="true" name="typeGroup">
                                            <option></option>
                                            @foreach ($groups as $group)
                                                <option value="{{$group->id}}">{{$group->name}}
                                                    , {{$group->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <hr>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="addRule" checked> Add Rule
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Choose the Stage</label>
                                        <select class=" form-control selectpicker" data-style="btn-primary" name="typeStage">
                                            <option value="3" selected>Both Stages</option>
                                            <option value="1">Preparation Stage only</option>
                                            <option value="2">Execution Stage only</option>


                                        </select>
                                    </div>
                                    <hr>
                                    @if ($errors->has() && old('typeFormActive') === '1' )
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                {{ $error }}<br>
                                            @endforeach
                                        </div>
                                    @endif

                                    <input type="hidden" name="typeFormActive" value="1">
                                    <button type="submit" class="btn btn-success ">Add Category</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Category List</div>
                            <div class="panel-body">
                                <!-- Table -->
                                <table width="100%" role="grid" id="table-list"
                                       class="table table-striped table-hover table-bordered dataTable no-footer font-xs" style="opacity: .5">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Short Name</th>
                                        <th>Owner</th>
                                        <th>Description</th>
                                        <th>Rules</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($taskTypes as $type)
                                        <tr onclick="showUpdateModelModal('/get-type-details/{{$type->id}}')">
                                            <th scope="row">{{$type->name}}</th>
                                            <td>{{$type->short_name}}</td>
                                            <td>{{$type->group->name}}</td>
                                            <td>{{$type->description}}</td>
                                            <td>{!!$type->numberOfRules()!!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('rowsys.tracker.includes.update_modal.update_modal')

            <!-- ==========================CONTENT ENDS HERE ========================== -->

            @stop


            @section ('local_scripts')
                <script src="/js/rowsys/tracker/tasks/general.js"></script>

                <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
                <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
                <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
                <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
                <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


                <script>
                    var otable = $('#table-list');
                    $(document).ready(function () {

                        $(".selectpicker").selectpicker();
                        //var hasMessage = '<?php echo Session::has('message') ; ?>';
                        //var hasErrors = '<?php echo Session::has('message') ; ?>';
                        //console.log(hasMessage);


                        var responsiveHelper_mission_list_column = undefined;
                        var breakpointDefinition = {
                            computer: 2000,
                            tablet: 1024,
                            phone: 480
                        };

                        var screenHeight = $(document).height();
                        screenHeight = (screenHeight - 600) + "px";
                        /* COLUMN FILTER  */
                        otable.DataTable({
                            //"bFilter": true,
                            //"bInfo": true,
                            //"bLengthChange": true,
                            //"bAutoWidth": true,
                            fixedHeader: true,
                            "pageLength": 50,
                            //"bPaginate": true,
                            //"aaSorting": [[ 3, "ASC" ]],
                            "aaSorting": [],
                            "scrollY": "800px",
                            //"scrollX": "500px",
                            //"paging": false,
                            //responsive: true,
                            // paging: false,
                            "bStateSave": true, // saves sort state using localStorage
                            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                            "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                            "autoWidth": true,
                            "preDrawCallback": function () {
                                // Initialize the responsive datatables helper once.
                                if (!responsiveHelper_mission_list_column) {
                                    responsiveHelper_mission_list_column = new ResponsiveDatatablesHelper($('#table-list'), breakpointDefinition);
                                }
                            },
                            "rowCallback": function (nRow) {
                                responsiveHelper_mission_list_column.createExpandIcon(nRow);
                            },
                            "drawCallback": function (oSettings) {
                                responsiveHelper_mission_list_column.respond();
                            }

                        });

                        // Apply the filter
                        $("#table-list thead th input[type=text]").on('keyup change', function () {
                            otable.column($(this).parent().index() + ':visible').search(this.value).draw();
                        });


                        $('a.toggle-vis').on('click', function (e) {
                            e.preventDefault();
                            // Get the column API object
                            var column = otable.column($(this).attr('data-column'));
                            // Toggle the visibility
                            column.visible(!column.visible());
                        });
                        $('#table-list').fadeTo(2000, '1');
                        $('#loading-box-section').remove();
                    });




                </script>

@stop

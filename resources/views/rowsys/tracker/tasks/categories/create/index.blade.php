@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><h1>Task Categories</h1></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">Add New Task Group</h3></div>
                    <div class="panel-body">
                        <form action="/category/new/group" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" id="" name="groupName" value="{{old('groupName')}}" placeholder="e.g AUTO,COMM">
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <input type="text" class="form-control" id="" name="groupDescription" value="{{old('groupDescription')}}" placeholder="Description">
                            </div>
                            @if ($errors->has() && old('groupFormActive') === '1' )
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            <input type="hidden" name="groupFormActive" value="1">

                            <button type="submit" class="btn btn-success ">Add Group</button>

                            @if (Session::has('message') && old('groupFormActive') === '1' )
                                <div class="alert alert-info alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-info"> </i>{{ Session::get('message') }}
                                </div>
                            @endif
                        </form>
                    </div>
                </div>


                <div class="panel panel-success">
                    <div class="panel-heading"><h3 class="panel-title">Add a Task Type</h3></div>
                    <div class="panel-body">
                        <form action="/category/new/type" method="post">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" id="" name="typeName" value="{{old('typeName')}}" placeholder="e.g User Requirement Specification">
                            </div>
                            <div class="form-group">
                                <label for="">Short Name
                                    <small>(used to build task tag)</small>
                                </label>
                                <input type="text" class="form-control" id="" name="typeShortName" value="{{old('typeShortName')}}" placeholder="e.g URS,TSIQ">
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <input type="text" class="form-control" id="" name="typeDescription" value="{{old('typeDescription')}}" placeholder="Description">
                            </div>

                            <div class="form-group">
                                <label for="">Task Group</label>
                                <select class=" form-control selectpicker" data-live-search="true" name="typeGroup">
                                    <option></option>
                                    @foreach ($groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}
                                            , {{$group->description}}</option>
                                    @endforeach
                                </select>
                            </div>

                            @if ($errors->has() && old('typeFormActive') === '1' )
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif

                            <input type="hidden" name="typeFormActive" value="1">
                            <button type="submit" class="btn btn-success ">Add Type</button>

                            @if (Session::has('message') && old('typeFormActive') === '1')
                                <div class="alert alert-info alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-info"> </i>{{ Session::get('message') }}
                                </div>
                            @endif
                        </form>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading"><h3 class="panel-title">Add Test Specification</h3></div>
                    <div class="panel-body">
                        <form action="/category/new/test-spec" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" id="" name="testName" value="{{old('testName')}}" placeholder="e.g TS456">
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <input type="text" class="form-control" id="" name="testDescription" value="{{old('testDescription')}}" placeholder="Description">
                            </div>
                            <div class="form-group">
                                <label for="">Task Type</label>
                                <select class=" form-control selectpicker" data-live-search="true" name="testType">
                                    <option></option>
                                    @foreach ($taskTypes as $type)
                                        <option value="{{$type->id}}"> {{$type->name}}  ({{$type->short_name}})</option>
                                    @endforeach
                                </select>
                            </div>

                            @if ($errors->has() && old('testFormActive') === '1' )
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif

                            <input type="hidden" name="testFormActive" value="1">
                            <button type="submit" class="btn btn-success ">Add Test Spec</button>

                            @if (Session::has('message') && old('testFormActive') === '1')
                                <div class="alert alert-info alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-info"> </i>{{ Session::get('message') }}
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script>
        $(document).ready(function () {
            //
        });
    </script>

@stop

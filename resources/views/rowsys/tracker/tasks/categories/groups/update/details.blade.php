<form id="update-model-form" action="/update-type-details" method="POST">
    {{ csrf_field() }}

        <input type="hidden" id="task_id" name="update_task_id" value="{{$group->id}}">

        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" id="name" name="update_name" value="{{$group->name}}">
        </div>

    <div class="form-group">
        <label for="">Short Name</label>
        <input type="text" class="form-control" id="short-name" name="short_name" value="{{$group->short_name}}">
    </div>


        <div class="form-group">
            <label for="">Description</label>
            <textarea class="form-control" name="update_description" id="update_description" rows="4" cols="6">{{$group->description}}</textarea>
        </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="updateCategoryModel('update-model-form', '/update-group-details','{{$group->id}}')" class="btn btn-success">Update</button>

        @if($taskCount === 0 && $typeCount === 0 && $stepPhaseCount === 0 && Auth::user()->role > 7)
            <button type="button" onclick="showModelDeleteSection({{$taskCount}})" class="btn btn-danger pull-left">Delete</button>
        @endif
    </div>
    <div id="are-you-sure-delete-section" class="hide">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times; </span></button>
            <p class="">Are you Sure <i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;
                <button type="button" onclick="deleteGroup({{$group->id}})" class="btn btn-danger"> Yes</button>
            </p>
        </div>
    </div>
</form>

<div class="alert alert-warning" role="alert">{{$taskCount}} Tasks, {{$typeCount}} Categories and {{$stepPhaseCount}} Review/Approval steps are associated to this Owner.

</div>

<p class="txt-color-orange font-xs"> Note: if you wish to delete this Owner, first delete or update all associated Tasks, Categories and Review/Approval steps</p>
<script>
    $(document).ready(function () {

    });

    function deleteGroup(id){
        $.ajax({
            type: 'GET',
            url: '/delete-task-group/' + id, success: function (result) {
                $('#update-modal').modal('hide');

                $.notify({
                    title: '<strong>Success!</strong><br>',
                    message: result + ""
                }, {
                    animate: {
                        enter: 'animated fadeInLeft',
                        exit: 'animated fadeOutRight'
                    },
                    type: 'success',
                    //offset: {x: 100, y: 100},
                    //placement: {from: "bottom"},
                    showProgressbar: false,
                    delay: 1500
                });

                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        });
    }

</script>

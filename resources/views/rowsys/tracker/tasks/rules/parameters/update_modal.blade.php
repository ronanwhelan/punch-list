<!-- Modal -->
<div class="modal fade" id="update-parameter-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Task Parameters</h4>
            </div>
            <div class="modal-body">
                <!-- Row start -->
                <div class="row">

                    <div class="col-sm-12">

                        <h3>General</h3>

                        <div class="form-group">
                            <label for="">Target Hours</label>
                            <input type="text" class="form-control" id="" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="">Added Lag (days)</label>
                            <input type="text" class="form-control" id="" placeholder="">
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox">Lock Date
                            </label>
                        </div>
                        <h3>Schedule Link</h3>

                        <div class="form-group">
                            <label for=""> Number</label>
                            <input type="text" class="form-control" id="" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for=""> Date</label>
                            <input type="text" class="form-control" id="" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for=""> Lag</label>
                            <input type="text" class="form-control" id="" placeholder="">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success">Update Task</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
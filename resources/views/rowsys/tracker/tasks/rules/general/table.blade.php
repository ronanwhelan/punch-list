<div class="hide">

    <div class="row">
        <div class="col-lg-8">
            <div class="row">
                <div class="col-md-2">

                    <h4 class="text-align-center"><strong>Target Date</strong></h4>
                </div>

                <div class="col-md-2">
                    <p class="text-align-center"><strong>Generation</strong></p>
                    <input class="form-control input-sm">
                </div>
                <div class="col-md-2">
                    <p class="text-align-center"><strong>Review</strong></p>
                    <input class="form-control input-sm">
                </div>

                <div class="col-md-2">
                    <p class="text-align-center"><strong>Issued</strong></p>
                    <input class="form-control input-sm">
                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-8">
            <div class="row">
                <div class="col-md-2">

                    <h4 class="text-align-center"><strong>Value</strong></h4>
                </div>

                <div class="col-md-2">
                    <p class="text-align-center"><strong>Generation</strong></p>
                    <input class="form-control input-sm">
                </div>
                <div class="col-md-2">
                    <p class="text-align-center"><strong>Review</strong></p>
                    <input class="form-control input-sm">
                </div>

                <div class="col-md-2">
                    <p class="text-align-center"><strong>Issued</strong></p>
                    <input class="form-control input-sm">
                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-8">
            <div class="row">
                <div class="col-md-2">

                    <h4 class="text-align-center"><strong>Status</strong></h4>
                </div>

                <div class="col-md-2">
                    <p class="text-align-center"><strong>Generation</strong></p>
                    <input class="form-control input-sm">
                </div>
                <div class="col-md-2">
                    <p class="text-align-center"><strong>Review</strong></p>
                    <input class="form-control input-sm">
                </div>

                <div class="col-md-2">
                    <p class="text-align-center"><strong>Issued</strong></p>
                    <input class="form-control input-sm">
                </div>
            </div>
        </div>
    </div>

</div>
<div class=" custom-scroll  " style="height:auto; overflow-y: scroll;">
    <div class="row">

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget " id="wid-id-1" data-widget-editbutton="false"
                 data-widget-colorbutton="false" data-widget-setstyle="false" data-widget-togglebutton="false"
                 data-widget-deletebutton="false">

                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                    <h2>Task - General Rules </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="">
                            <table id="datatable_fixed_column" class="table table-striped table-hover table-bordered " width="100%">
                                <thead>
                                {{--                            <tr>
                                                                <th class="hasinput" style="width:4%"><input type="text" class="form-control" placeholder=""/></th>

                                                                <th class="hasinput" style="width:4%"><input type="text" class="form-control" placeholder=""/></th>
                                                                <th class="hasinput" style="width:10%"><input type="text" class="form-control" placeholder=""/></th>


                                                                <th class="hasinput" style="width:6%"><input type="text" class="form-control" placeholder=""/></th>
                                                                <th class="hasinput" style="width:6%"><input type="text" class="form-control" placeholder=""/></th>
                                                                <th class="hasinput" style="width:6%"><input type="text" class="form-control" placeholder=""/></th>
                                                                <th class="hasinput" style="width:6%"><input type="text" class="form-control" placeholder=""/></th>

                                                                <th class="hasinput" style="width:6%"><input type="text" class="form-control" placeholder=""/></th>
                                                                <th class="hasinput" style="width:6%"><input type="text" class="form-control" placeholder=""/></th>
                                                                <th class="hasinput" style="width:6%"><input type="text" class="form-control" placeholder=" "/></th>
                                                                <th class="hasinput" style="width:6%"><input type="text" class="form-control" placeholder=" "/></th>


                                                                <th class="hasinput" style="width:5%"><input type="text" class="form-control" placeholder=""/></th>
                                                                <th class="hasinput" style="width:5%"><input type="text" class="form-control" placeholder=""/></th>
                                                                <th class="hasinput" style="width:5%"><input type="text" class="form-control" placeholder=" "/></th>
                                                                <th class="hasinput" style="width:5%"><input type="text" class="form-control" placeholder=""/></th>

                                                            </tr>--}}

                                <tr>
                                    <th data-hide="" class="font-xs">Stage</th>
                                    <th data-hide="phone" class="font-xs">Group</th>
                                    <th data-class="expand" class="font-xs">Task Type</th>


                                    <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Status</span><br>Gen/Ex</th>
                                    <th data-hide="phone" class="font-xs">Status<br>Review</th>
                                    <th data-hide="phone" class="font-xs">Status<br>Update</th>
                                    <th data-hide="phone" class="font-xs">Status<br>SignOff</th>

                                    <th data-hide="phone,tablet" class="font-xs">TD<br>Gen/Ex</th>
                                    <th data-hide="phone,tablet" class="font-xs">TD<br>Review</th>
                                    <th data-hide="phone,tablet" class="font-xs">TD<br>Update</th>
                                    <th data-hide="phone,tablet" class="font-xs">TD<br>SignOff</th>


                                    <th data-hide="phone,tablet" class="font-xs">Hrs<br>Gen/Ex</th>
                                    <th data-hide="phone,tablet" class="font-xs">Hrs<br>Review</th>
                                    <th data-hide="phone,tablet" class="font-xs">Hrs<br>Update</th>
                                    <th data-hide="phone,tablet" class="font-xs">Hrs<br>SignOff</th>

                                    @if($showMultiDate)
                                        <th data-class="phone,tablet" class="font-xs">Multi Date</th>
                                    @endif

                                </tr>
                                </thead>

                                <tbody class="font-xs">
                                @foreach($taskRules as $taskRule)
                                    <tr onclick="showUpdateTaskRuleModal({{$taskRule->id}},'({{$taskRule->stage->name}}) {{$taskRule->taskType->name}}')">
                                        <td id="{{$taskRule->id}}-stage">{{$taskRule->stage->name}}</td>
                                        <td id="{{$taskRule->id}}-group">{{$taskRule->taskGroup->name}}</td>
                                        <td id="{{$taskRule->id}}-type">{{$taskRule->taskType->short_name}} - {{$taskRule->taskType->name}} </td>


                                        @if($taskRule->gen_ex_applicable === 0)
                                            <td id="{{$taskRule->id}}-gen-status" style="background-color: white">N/A</td>
                                        @else
                                            <td id="{{$taskRule->id}}-gen-status" style="background-color: lightgray">Yes</td>
                                        @endif

                                        @if($taskRule->rev_applicable === 0)
                                            <td id="{{$taskRule->id}}-rev-status" style="background-color: white">N/A</td>
                                        @else
                                            <td id="{{$taskRule->id}}-rev-status" style="background-color: lightgray">Yes</td>
                                        @endif

                                        @if($taskRule->re_issu_applicable === 0)
                                            <td id="{{$taskRule->id}}-re-issu-status" style="background-color: white">N/A</td>
                                        @else
                                            <td id="{{$taskRule->id}}-re-issu-status" style="background-color: lightgray">Yes</td>
                                        @endif

                                        @if($taskRule->s_off_applicable === 0)
                                            <td id="{{$taskRule->id}}-s-off-status" style="background-color: white">N/A</td>
                                        @else
                                            <td id="{{$taskRule->id}}-s-off-status" style="background-color: lightgray">Yes</td>
                                        @endif

                                        <td id="{{$taskRule->id}}-gen-days" style="background-color: lightsteelblue"> {{$taskRule->gen_ex_buffer_days}} days</td>
                                        <td id="{{$taskRule->id}}-rev-days" style="background-color: lightsteelblue"> {{$taskRule->rev_buffer_days}} days</td>
                                        <td id="{{$taskRule->id}}-re-issu-days" style="background-color: lightsteelblue"> {{$taskRule->re_issu_buffer_days}} days</td>
                                        <td id="{{$taskRule->id}}-s-off-days" style="background-color: lightsteelblue"> {{$taskRule->s_off_buffer_days}} days</td>

                                        <td id="{{$taskRule->id}}-gen-hrs" style="background-color: lightyellow">{{$taskRule->gen_ex_hrs_perc}}%</td>
                                        <td id="{{$taskRule->id}}-rev-hrs" style="background-color: lightyellow">{{$taskRule->rev_hrs_perc}}%</td>
                                        <td id="{{$taskRule->id}}-re-issu-hrs" style="background-color: lightyellow">{{$taskRule->re_issu_hrs_perc}}%</td>
                                        <td id="{{$taskRule->id}}-s-off-hrs" style="background-color: lightyellow">{{$taskRule->s_off_hrs_perc}}%</td>

                                        @if($showMultiDate)
                                            @if($taskRule->multi_date === 1)
                                                <td id="{{$taskRule->id}}-multi-date" style="background-color: lightgreen">On</td>
                                            @else
                                                @if($taskRule->stage_id === 2)
                                                    <td id="{{$taskRule->id}}-multi-date" style="background-color: lightgray">Off</td>
                                                @else
                                                    <td id="{{$taskRule->id}}-multi-date" style="background-color: #f5f5f5">N/A</td>
                                                @endif
                                            @endif
                                        @endif

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
    </div>
</div>

@if (isset($dates))
    <fieldset>
        <div class="row text-align-center">
            <p class=" text-align-center">Schedule Dates for </p>
            <h3 class=" text-align-center">{{$dates->number}} <br> {{$dates->description}}</h3>
        </div>
        <br>
<input type="hidden" name="dateAvailable" value="1">
        <div class="row">
            <div class=" col-xs-offset-1 col-xs-10  col-sm-offset-0 col-sm-6   col-md-offset-2  col-md-4  col-lg-offset-2 col-lg-4 col-xl-offset-2 col-xl-4">
                <div class="well well-sm  text-align-center">
                    <div>Start Date</div>
                    <h3 class="text-align-center" id="start-date-display" name="startDateDisplay">{{$dates->start_date->format('d-m-Y')}}</h3>
                    <input type="hidden" value="{{$dates->start_date}}" name="startDate">
                </div>
            </div>
            <div class=" col-xs-offset-1 col-xs-10  col-sm-offset-0 col-sm-6   col-md-offset-0  col-md-4  col-lg-offset-0 col-lg-4 col-xl-offset-0 col-xl-4">
                <div class="well well-sm  text-align-center">
                    <div>Finish Date</div>
                    <h3 class="text-align-center"  id=finish-date-display" >{{$dates->finish_date->format('d-m-Y')}}</h3>
                    <input type="hidden" value="{{$dates->finish_date}}" name="finishDate">
                </div>
            </div>
        </div>
    </fieldset>
@else

    <div class="" id="enter-target-dates-section">
        <fieldset>

            <div class="row text-align-center">
                <h3>No Dates Available For this Task</h3>

                <h3>Enter Dates</h3>
            </div>
            <input type="hidden" name="dateAvailable" value="0">
            <div class="row">
                <div class=" col-xs-offset-1 col-xs-10  col-sm-offset-0 col-sm-6   col-md-offset-2  col-md-4  col-lg-offset-2 col-lg-4 col-xl-offset-2 col-xl-4">
                    <div class="well well-sm  text-align-center">
                        <h4>Generation Date</h4>

                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control" id="genDate" name ="genDate" type="date" placeholder="From">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-xs-offset-1 col-xs-10  col-sm-offset-0 col-sm-6   col-md-offset-0  col-md-4  col-lg-offset-0 col-lg-4 col-xl-offset-0 col-xl-4">
                    <div class="well well-sm  text-align-center">
                        <h4>Execution Date</h4>

                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control" id="exDate" name="exDate" type="date" placeholder="From">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
@endif

<script>
    // Date Range Picker
    $("#genDate").datepicker({
        defaultDate: "+1w",
        dateFormat:"yy-mm-dd",
        changeMonth: true,
        numberOfMonths: 3,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onClose: function (selectedDate) {
            $("#to").datepicker("option", "maxDate", selectedDate);
        }
    }).datepicker("setDate", new Date());


    $("#exDate").datepicker({
        defaultDate: "+1w",
        dateFormat:"yy-mm-dd",
        changeMonth: true,
        numberOfMonths: 3,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onClose: function (selectedDate) {
            $("#from").datepicker("option", "minDate", selectedDate);
        }
    }).datepicker("setDate", new Date());
</script>
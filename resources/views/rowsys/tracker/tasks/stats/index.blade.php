@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link rel="stylesheet" type="text/css" href="/css/vendor/jsPlot/jquery.jqplot.css" />
@stop

@section ('head_js')

@stop

@section('content')


    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-lg-12">
            <h3>Contextual alternatives</h3>

            <div class="panel panel-success">
                <div class="panel-heading"><h3 class="panel-title">General Overview</h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4">

                            <h3>Total : {{$allStats[0]['general'][0]['total']}}</h3>
                            <h4>Completed : {{$allStats[0]['general'][0]['completed']}}</h4>
                            <h4>Left To Do : {{$allStats[0]['general'][0]['leftToDo']}}</h4>
                            <h4>Percent Done : {{$allStats[0]['general'][0]['percDone']}} %</h4>
                            <h4>Due in the Next Two Weeks : {{$allStats[0]['general'][0]['dueWithinTwoWeeks']}}</h4>
                            <h4>Added in the Last Two Weeks
                                : {{$allStats[0]['general'][0]['addedInTheLastTwoWeeks']}}</h4>
                            <h4>Completed in the Last Two Weeks
                                : {{$allStats[0]['general'][0]['completedInTheLastTwoWeeks']}}</h4>

                        </div>

                        <div class="col-lg-4">
                            <h4>Due in Two Weeks</h4>
                            <h3>Totoal = {{$groupTaskStatsInNextTwoWeeks[1]}}</h3>
                            @foreach($groupTaskStatsInNextTwoWeeks[0] as $key => $value)

                                @foreach($value as $key => $valuex)
                                    {{$key}} {{$valuex}}<br>
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>


            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">Group Stats - due in the Next 2 weeks</h3></div>
                <div class="panel-body">

                </div>
            </div>


            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Task General</h3></div>
                <div class="panel-body">Panel content
                    <div class="row">
                        <div class="col-lg-8">
                            <div id="piechart" style="width:500px; height: 500px;"></div>
                            <br>

                            <p>Total : 23456</p>
                        </div>

                        <div class="col-lg-4">
                            <h3>dsdf</h3>
                        </div>

                    </div>


                </div>
            </div>


            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                <div class="panel-body">
<h3>Sds</h3>
                    <div class="col-lg-4">
                    <div id="chart1" style="height: auto; width: auto; position: relative;" class="jqplot-target"></div>
                        </div>

                </div>
            </div>

            <div class="panel panel-warning">
                <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                <div class="panel-body">

                </div>
            </div>

            <div class="panel panel-danger">
                <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                <div class="panel-body">Panel content</div>
            </div>

            <h3>With tables</h3>

            <div class="panel panel-primary">
                <div class="panel-heading">Panel heading</div>
                <div class="panel-body"><p>Panel content</p></div>

                <!-- Table -->
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>

                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/plugin/jsPlot/jquery.jqplot.js"></script>
    <script type="text/javascript" src="/js/plugin/jsPlot/plugins/jqplot.pieRenderer.js"></script>
    <script type="text/javascript" src="/js/plugin/jsPlot/plugins/jqplot.donutRenderer.js"></script>


    <script>
        var allStats = '<?php echo $allStatsForJs; ?>';


        $(document).ready(function () {



            var datas = '<?php echo $taskBreakDownByGroup; ?>';
            datas = JSON.parse(datas);

            var arr = jQuery.makeArray( datas[0] );
            console.log(jQuery.makeArray( arr[0] ));

            // var data = [['Heavy Industry', 12],['Retail', 9], ['Light Industry', 14], ['Out of home', 16],['Commuting', 7], ['Orientation', 9]];
            var data = '<?php echo  $groupBreakDown; ?>';
            data = JSON.parse(data);
            console.log(data);


            var plot4 = $.jqplot('chart1', [data], {
                seriesColors: [ "#4bb2c5", "#c5b47f", "#EAA228", "#579575" ],
                legend: { show:true, location: 'e' },
                seriesDefaults: {
                    // make this a donut chart.
                    renderer:$.jqplot.DonutRenderer,
                    rendererOptions:{
                        // Donut's can be cut into slices like pies.
                        sliceMargin: 3,
                        // Pies and donuts can start at any arbitrary angle.
                        startAngle: -90,
                        showDataLabels: true,
                        // By default, data labels show the percentage of the donut/pie.
                        // You can show the data 'value' or data 'label' instead.
                        dataLabels: 'value',
                        // "totalLabel=true" uses the centre of the donut for the total amount
                        totalLabel: true
                    }


                }
            });


        });
    </script>





    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script>
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Tasks', 'Per Group'],
                ['Automation', 11],
                ['Design', 2],
                ['Construction', 2],
                ['Commissioning', 2],
                ['Qualification', 7]
            ]);

            var options = {
                title: 'Tasks by Group'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }


    </script>


@stop

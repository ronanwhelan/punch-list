@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')

    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                            <div class="panel-body">
                                <table class="highchart"
                                       data-graph-legend-disabled="0"
                                       data-graph-container-before="1"
                                       data-graph-type="column"
                                       style="display:none"
                                       data-graph-datalabels-enabled="1"
                                       data-graph-color-1="#999"
                                       data-graph-legend-layout="horizontal">

                                    <caption>Group Task Count</caption>
                                    <thead>

                                    <tr>
                                        <th>Group</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for( $i = 0;$i < sizeof($groupBreakDown);$i++)
                                        <tr>
                                            <td>{{$groupBreakDown[$i][0]}}</td>
                                            <td data-graph-name=" {{$groupBreakDown[$i][1]}}"> {{$groupBreakDown[$i][1]}}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                            <div class="panel-body">
                                <table class="highchart"
                                       data-graph-legend-disabled="0"
                                       data-graph-container-before="1"
                                       data-graph-type="pie"
                                       style="display:none"
                                       data-graph-datalabels-enabled="1"
                                       data-graph-color-1="#999"
                                       data-graph-legend-layout="horizontal">

                                    <caption>Group Task Count</caption>
                                    <thead>

                                    <tr>
                                        <th>Group</th>
                                        <th>Group</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for( $i = 0;$i < sizeof($groupBreakDown);$i++)
                                        <tr>
                                            <td>{{$groupBreakDown[$i][0]}}</td>
                                            <td data-graph-name=" {{$groupBreakDown[$i][1]}}"> {{$groupBreakDown[$i][1]}}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>






                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h3 class="panel-title">Group Task Type Count</h3></div>
                            <div class="panel-body">
                                <select class="selectpicker" data-live-search="true">
                                    <option data-tokens="automation">Automation</option>
                                </select>
                                <table class="highchart"
                                       data-graph-legend-disabled="0"
                                       data-graph-container-before="1"
                                       data-graph-type="column"
                                       style="display:none"
                                       data-graph-datalabels-enabled="1"
                                       data-graph-color-1="#999"
                                       data-graph-legend-layout="horizontal">
                                    <caption>Automation</caption>
                                    <thead>
                                    <tr>
                                        <th>Automation</th>
                                        <th>Automation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for( $i = 0;$i < sizeof($groupTaskTypeBreakDown);$i++)
                                        <tr>
                                            <td>{{$groupTaskTypeBreakDown[$i][0]}}</td>
                                            <td data-graph-name=" {{$groupTaskTypeBreakDown[$i][1]}}"> {{$groupTaskTypeBreakDown[$i][1]}}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h3 class="panel-title">System Task Type Count</h3></div>
                            <div class="panel-body">
                                <select class="selectpicker" data-live-search="true">
                                    <option data-tokens="automation">Systems</option>
                                </select>
                                <table class="highchart"
                                       data-graph-legend-disabled="0"
                                       data-graph-container-before="1"
                                       data-graph-type="line"
                                       style="display:none"
                                       data-graph-datalabels-enabled="1"
                                       data-graph-color-1="#999"
                                       data-graph-legend-layout="horizontal">
                                    <caption> System 1</caption>
                                    <thead>
                                    <tr>
                                        <th>Automation</th>
                                        <th>Automation</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @for( $i = 0;$i < sizeof($systemTaskTypeBreakDown);$i++)
                                        <tr>
                                            <td>{{$systemTaskTypeBreakDown[$i]['taskType']->name}}</td>
                                            <td data-graph-name=" {{$systemTaskTypeBreakDown[$i]['count']}}"> {{$systemTaskTypeBreakDown[$i]['count']}}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>








                <div class="panel panel-success">
                    <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                    <div class="panel-body">
                        <table class="highchart"
                               data-graph-legend-disabled="0"
                               data-graph-container-before="1"
                               data-graph-type="line"
                               style="display:none"
                               data-graph-datalabels-enabled="1"
                               data-graph-color-1="#999"
                               data-graph-legend-layout="horizontal">

                            <caption>Group Task Count</caption>
                            <thead>

                            <tr>
                                <th>Group</th>
                                <th>Group</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for( $i = 0;$i < sizeof($groupBreakDown);$i++)
                                <tr>
                                    <td>{{$groupBreakDown[$i][0]}}</td>
                                    <td data-graph-name=" {{$groupBreakDown[$i][1]}}"> {{$groupBreakDown[$i][1]}}</td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                    <div class="panel-body">

                        <table class="highchart"
                               data-graph-legend-disabled="0"
                               data-graph-container-before="1"
                               data-graph-type="line"
                               style="display:none"
                               data-graph-datalabels-enabled="1"
                               data-graph-color-1="#999"
                               data-graph-legend-layout="horizontal">

                            <caption>Assigned Hours against Earned Value by Group</caption>
                            <thead>

                            <tr>
                                <th>Group</th>
                                <th>Assigned</th>
                                <th>Earned</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for( $i = 0;$i < sizeof($groupBreakDownHrs);$i++)
                                <tr>
                                    <td>{{$groupBreakDownHrs[$i][0]}}</td>
                                    <td data-graph-name=" {{$groupBreakDownHrs[$i][1]}}"> {{$groupBreakDownHrs[$i][1]}}</td>
                                    <td data-graph-name=" {{$groupBreakDownHrs[$i][1]}}"> {{$groupBreakDownHrs[$i][2]}}</td>

                                </tr>
                            @endfor
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="panel panel-warning">
                    <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                    <div class="panel-body">Panel content</div>
                </div>

                <div class="panel panel-danger">
                    <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                    <div class="panel-body">Panel content</div>
                </div>

                <h3>With tables</h3>

                <div class="panel panel-primary">
                    <div class="panel-heading">Panel heading</div>
                    <div class="panel-body"><p>Panel content</p></div>

                    <!-- Table -->
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>

                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>


        <div class="row">
            <div id="myTabs" class="col-lg-12">
                <h3>Example of a tab system</h3>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                    <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                    <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                </ul>

                <!-- Tab panes -->
                <div id ="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <h3>Tab 1</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                        <h3>Tab 2</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages">
                        <h3>Tab 3</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                    </div>
                </div>
            </div>
        </div>


        <!-- row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12 well">
                    <div class="col-sm-12">
                        <div class="highchart-container"></div>
                    </div>
                    <div class="col-sm-12">


                    </div>

                </div>


            </div>
            <!-- end row -->
        </div>
        <!-- end row -->
    </section>
    <!-- end widget grid -->


@stop


@section ('local_scripts')
    <script type="text/javascript">

        $(document).ready(function () {
            /*
             data = JSON.parse(data);
             console.log(data);*/
        })
    </script>

    <script src="/js/plugin/highChartCore/highcharts-custom.min.js"></script>
    <script src="/js/plugin/highchartTable/jquery.highchartTable.min.js"></script>

@stop

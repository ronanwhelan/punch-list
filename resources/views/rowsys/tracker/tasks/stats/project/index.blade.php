@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')

    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <h2>Project Overview</h2>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="panel panel-info" style="height: 450px">
                <div class="panel-heading"><h3 class="panel-title">Project Preparation Status Overview</h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div id="myTabs" class="col-lg-12">
                            <h3>Preparation Stage</h3>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="#tab-1-{{$project->id}}"
                                                      aria-controls="tab-1-{{$project->id}}" role="tab"
                                                      data-toggle="tab">Graph</a>
                                </li>
                                @if($project->stats['prep_past_due_count'] > 0)
                                    <li><a href="#tab-2-{{$project->id}}" aria-controls="tab-2-{{$project->id}}" role="tab" data-toggle="tab">Past Due <span
                                                    class="badge">{{$project->stats['prep_past_due_count']}}</span></a></li>
                                @endif
                                <li><a href="#tab-3-{{$project->id}}" aria-controls="tab-3-{{$project->id}}" role="tab" data-toggle="tab">Task Info</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="tab-1-{{$project->id}}">

                                    <div class="col-md-12">
                                        <div class="text-align-center loading-section" id="loading-spinner-section-{{$project->id}}">
                                            <br>
                                            <i class="fa fa-spinner fa-spin fa-4x"> </i>
                                            <br>

                                            <p> Loading Graph......</p><br><br><br>
                                        </div>
                                    </div>
                                    <br>

                                    <div id="chart-section-prep" onclick="viewAreaGroup()" class="" style="">
                                        <canvas id="percentage-chart-canvas-prep" width="1002" height="250" class="percentage-chart"></canvas>
                                    </div>

                                    @if($project->stats['prep_past_due_count'] > 0)

                                        <div class="col-md-12">
                                            <h4 class="txt-color-red  ">Past
                                                Due: {{$project->stats['prep_past_due_count']}}</h4>
                                        </div>
                                    @endif
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="tab-2-{{$project->id}}">

                                    @if(!empty( $project->stats['prep_past_due_group'] ))
                                        <!-- Table -->
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th onclick="gotoTaskTable()">Group</th>
                                                <th>Count</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($project->stats['prep_past_due_group'] as $key => $value)
                                                <tr>
                                                    <th><a onclick="gotoTaskTable()">{{$key}}</a></th>
                                                    <td>{{$value}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    @else
                                        <h4>No Late Items</h4>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab-3-{{$project->id}}">
                                    <ul class="list-group">
                                        <li class="list-group-item">Task Count<span
                                                    class="badge">{{$project->stats['prep_total']}}</span></li>
                                        <li class="list-group-item">Completed<span
                                                    class="badge">{{$project->stats['prep_completed']}}</span></li>
                                        <li class="list-group-item">Left To Do<span
                                                    class="badge">{{$project->stats['prep_left_to_do']}}</span></li>
                                        <li class="list-group-item">Due in Two Weeks<span
                                                    class="badge">{{$project->stats['prep_due_in_two_weeks']}}</span></li>
                                    </ul>

                                    <button type="button" onclick="gotoTaskTable()" class="hide btn btn-info">Task Table</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="panel panel-info" style="height: 450px">
                <div class="panel-heading"><h3 class="panel-title">Project Execution Status Overview </h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div id="myTabs" class="col-lg-12">
                            <h3>Execution Stage</h3>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="#tab-1-{{$project->id}}2"
                                                      aria-controls="tab-1-{{$project->id}}2" role="tab"
                                                      data-toggle="tab">Graph</a>
                                </li>

                                @if($project->stats['exe_past_due_count'] > 0)
                                    <li><a href="#tab-2-{{$project->id}}2" aria-controls="tab-2-{{$project->id}}" role="tab" data-toggle="tab">Past Due <span
                                                    class="badge">{{$project->stats['exe_past_due_count']}}</span></a></li>
                                @endif


                                <li><a href="#tab-3-{{$project->id}}2" aria-controls="tab-3-{{$project->id}}2" role="tab" data-toggle="tab">Task Info</a></li>
                            </ul>


                            <!-- Tab panes -->
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="tab-1-{{$project->id}}2">

                                    <div class="col-md-12">
                                        <div class="text-align-center loading-section" id="loading-spinner-section-2">
                                            <br>
                                            <i class="fa fa-spinner fa-spin fa-4x"> </i>
                                            <br>

                                            <p> Loading Graph......</p><br><br><br>
                                        </div>

                                        <br>

                                        <div id="chart-section-exe" onclick="viewAreaGroup()" class="" style="">
                                            <canvas id="percentage-chart-canvas-exe" width="1002" height="250" class="percentage-chart"></canvas>
                                        </div>


                                    </div>
                                    @if($project->stats['exe_past_due_count'] > 0)

                                        <div class="col-md-12">
                                            <h4 class="txt-color-red  ">Past
                                                Due: {{$project->stats['exe_past_due_count']}}</h4>
                                        </div>
                                    @endif

                                </div>


                                <div role="tabpanel" class="tab-pane fade" id="tab-2-{{$project->id}}2">

                                    @if(!empty( $project->stats['exe_past_due_group'] ))
                                        <!-- Table -->
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th onclick="gotoTaskTable()">Group</th>
                                                <th>Count</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($project->stats['exe_past_due_group'] as $key => $value)
                                                <tr>
                                                    <th><a onclick="gotoTaskTable()">{{$key}}</a></th>
                                                    <td>{{$value}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    @else
                                        <h4>No Late Items</h4>
                                    @endif


                                </div>


                                <div role="tabpanel" class="tab-pane fade" id="tab-3-{{$project->id}}2">
                                    <ul class="list-group">
                                        <li class="list-group-item">Task Count<span
                                                    class="badge">{{$project->stats['exe_total']}}</span></li>
                                        <li class="list-group-item">Completed<span
                                                    class="badge">{{$project->stats['exe_completed']}}</span></li>
                                        <li class="list-group-item">Left To Do<span
                                                    class="badge">{{$project->stats['exe_left_to_do']}}</span></li>
                                        <li class="list-group-item">Due in Two Weeks<span
                                                    class="badge">{{$project->stats['exe_due_in_two_weeks']}}</span></li>
                                    </ul>

                                    <button type="button" onclick="gotoTaskTable()" class="hide btn btn-info">Task Table</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row hide">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Preparation Chart</h3></div>
                <div class="panel-body">
                    <table class="highchart"
                           data-graph-legend-disabled="0"
                           data-graph-container-before="1"
                           data-graph-type="line"
                           style="display:none"
                           data-graph-datalabels-enabled="1"
                           data-graph-color-1="#999"
                           data-graph-line-width="6"
                           data-graph-legend-layout="horizontal"
                           data-graph-xaxis-labels-font-size="50%">

                        <caption>Preparation Chart</caption>
                        <thead>
                        <tr>
                            <th>Month</th>
                            <th>Prep Actual</th>
                            <th>Prep Target</th>
                            <th>Prep Project</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($prepStageMonthlyHrsData as $data)
                            <tr>
                                <td>{{$data['month']}}</td>
                                <td>0</td>
                                <td>0</td>
                                <td>{{$data['value']}}</td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Execution Chart</h3></div>
                <div class="panel-body">
                    <table class="highchart"
                           data-graph-legend-disabled="0"
                           data-graph-container-before="1"
                           data-graph-type="line"
                           style="display:none"
                           data-graph-datalabels-enabled="1"
                           data-graph-color-1="#999"
                           data-graph-line-width="6"
                           data-graph-legend-layout="horizontal"
                           data-graph-xaxis-labels-font-size="50%">

                        <caption>Execution Chart</caption>
                        <thead>
                        <tr>
                            <th>Month</th>
                            <th>Prep Actual</th>
                            <th>Prep Target</th>
                            <th>Prep Project</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($execStageMonthlyHrsData as $data)
                            <tr>
                                <td>{{$data['month']}}</td>
                                <td>0</td>
                                <td>0</td>
                                <td>{{$data['value']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>


    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading"><h3 class="panel-title">Area Break Down</h3></div>
                                <div class="panel-body">
                                    <table class="highchart"
                                           data-graph-legend-disabled="0"
                                           data-graph-container-before="1"
                                           data-graph-type="column"
                                           style="display:none"
                                           data-graph-datalabels-enabled="1"
                                           data-graph-color-1="#999"
                                           data-graph-legend-layout="horizontal"
                                           data-graph-xaxis-labels-font-size="50%">

                                        <caption>Area Break Down</caption>
                                        <thead>

                                        <tr>
                                            <th>Group</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @for( $i = 0;$i < sizeof($areaBreakDown);$i++)
                                            <tr>
                                                <td>{{$areaBreakDown[$i][0]}}</td>
                                                <td data-graph-name=" {{$areaBreakDown[$i][1]}}"> {{$areaBreakDown[$i][1]}}</td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-success">
                                <div class="panel-heading"><h3 class="panel-title">Group Break Down</h3></div>
                                <div class="panel-body">
                                    <table class="highchart"
                                           data-graph-legend-disabled="0"
                                           data-graph-container-before="1"
                                           data-graph-type="column"
                                           style="display:none"
                                           data-graph-datalabels-enabled="1"
                                           data-graph-color-1="#999"
                                           data-graph-legend-layout="horizontal"
                                           data-graph-xaxis-labels-font-size="50%">

                                        <caption>Group Task Count</caption>
                                        <thead>

                                        <tr>
                                            <th>Group</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @for( $i = 0;$i < sizeof($groupBreakDown);$i++)
                                            <tr>
                                                <td>{{$groupBreakDown[$i][0]}}</td>
                                                <td data-graph-name=" {{$groupBreakDown[$i][1]}}"> {{$groupBreakDown[$i][1]}}</td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="panel panel-info">
                            <div class="panel-heading"><h3 class="panel-title">Hours</h3></div>
                            <div class="panel-body">

                                <table class="highchart"
                                       data-graph-container-before="1"
                                       data-graph-type="column"
                                       style="display:none"
                                       data-graph-legend-disabled="0"
                                       data-graph-yaxis-1-stacklabels-enabled="1"
                                       data-graph-color-1="#456"
                                       data-graph-legend-layout="horizontal">


                                    <caption> Hours Break Down by Group</caption>
                                    <thead>
                                    <tr>
                                        <th>Automation</th>
                                        <th data-graph-stack-group="1">Target</th>
                                        <th data-graph-stack-group="1">Earned</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for( $i = 0;$i < sizeof($groupBreakDownHrs);$i++)
                                        <tr>
                                            <td>{{$groupBreakDownHrs[$i][0]}}</td>
                                            <td data-graph-name=" {{$groupBreakDownHrs[$i][1]}}"> {{$groupBreakDownHrs[$i][1]}}</td>
                                            <td data-graph-name=" {{$groupBreakDownHrs[$i][2]}}"> {{$groupBreakDownHrs[$i][2]}}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                    <div class="row hide">
                        <div class="col-lg-6">
                            <div class="panel panel-info">
                                <div class="panel-heading"><h3 class="panel-title">Groups</h3></div>
                                <div class="panel-body">
                                    <select class="selectpicker" data-live-search="true">
                                        @foreach($groupList as $group)
                                            <option data-tokens="{{$group->id}}">{{$group->description}}</option>
                                        @endforeach
                                    </select>
                                    <table class="highchart"
                                           data-graph-legend-disabled="0"
                                           data-graph-container-before="1"
                                           data-graph-type="column"
                                           style="display:none"
                                           data-graph-datalabels-enabled="1"
                                           data-graph-color-1="#999"
                                           data-graph-legend-layout="horizontal">
                                        <caption>Automation</caption>
                                        <thead>
                                        <tr>
                                            <th>Automation</th>
                                            <th>Automation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @for( $i = 0;$i < sizeof($groupTaskTypeBreakDown);$i++)
                                            <tr>
                                                <td>{{$groupTaskTypeBreakDown[$i][0]}}</td>
                                                <td data-graph-name=" {{$groupTaskTypeBreakDown[$i][1]}}"> {{$groupTaskTypeBreakDown[$i][1]}}</td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-warning">
                                <div class="panel-heading"><h3 class="panel-title">Systems</h3></div>
                                <div class="panel-body">
                                    <select class="selectpicker" data-live-search="true">
                                        @foreach($systemList as $system)
                                            <option data-tokens="{{$system->id}}">{{$system->tag}}</option>
                                        @endforeach
                                    </select>
                                    <table class="highchart"
                                           data-graph-legend-disabled="0"
                                           data-graph-container-before="1"
                                           data-graph-type="line"
                                           style="display:none"
                                           data-graph-datalabels-enabled="1"
                                           data-graph-color-1="#999"
                                           data-graph-legend-layout="horizontal">
                                        <caption> System 1</caption>
                                        <thead>
                                        <tr>
                                            <th>Automation</th>
                                            <th>Automation</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @for( $i = 0;$i < sizeof($systemTaskTypeBreakDown);$i++)
                                            <tr>
                                                <td>{{$systemTaskTypeBreakDown[$i]['taskType']->name}}</td>
                                                <td data-graph-name=" {{$systemTaskTypeBreakDown[$i]['count']}}"> {{$systemTaskTypeBreakDown[$i]['count']}}</td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row"></div>

    <div class="col-lg-6 hide">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
            <div class="panel-body">
                <table class="highchart"
                       data-graph-legend-disabled="0"
                       data-graph-container-before="1"
                       data-graph-type="pie"
                       style="display:none"
                       data-graph-datalabels-enabled="1"
                       data-graph-color-1="#999"
                       data-graph-legend-layout="horizontal">

                    <caption>Group Task Count</caption>
                    <thead>

                    <tr>
                        <th>Group</th>
                        <th>Group</th>
                    </tr>
                    </thead>
                    <tbody>
                    @for( $i = 0;$i < sizeof($groupBreakDown);$i++)
                        <tr>
                            <td>{{$groupBreakDown[$i][0]}}</td>
                            <td data-graph-name=" {{$groupBreakDown[$i][1]}}"> {{$groupBreakDown[$i][1]}}</td>
                        </tr>
                    @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>




    <!-- end widget grid -->


@stop


@section ('local_scripts')

    <!-- Load HIGHCHARTS -->
    <script src="/js/plugin/highChartCore/highcharts-custom.min.js"></script>
    <script src="/js/plugin/highchartTable/jquery.highchartTable.min.js"></script>

    <!-- Load GOOGLE CHARTS-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <!-- Load APP scritps -->
    <script src="/js/rowsys/tracker/tasks/graphs.js"></script>


    <script type="text/javascript">

        /*
         *  --------------------------------------------------
         * Load GOOGLE Charts
         * --------------------------------------------------
         */
        //Call to the Google Charts Library
        google.charts.load('current', {packages: ['corechart', 'bar']});

        /*
         *  --------------------------------------------------
         * Document Ready
         * --------------------------------------------------
         */
        $(document).ready(function () {
            //pleaseWait('loading graph data');
            drawProjectBarCharts(1);
        });

        $(document).ajaxStop(function () {
            waitDone();
            //$( "#loading" ).hide();
        });
        /*
         *  --------------------------------------------------
         * GoTo Task Table
         * --------------------------------------------------
         */
        function gotoTaskTable() {
            var url = "/tasks/table";
            window.location = url;
        }
        /*
         *  --------------------------------------------------
         * Draw the Bar Chart
         * --------------------------------------------------
         */
        function DrawChart(target, actual, id, title) {
            chartId = id;
            chartTitle = title;
            calculateTheGraphData(1, 1);
            prepareAndDrawBarPercentChart(graphData, chartId);
            calculateTheGraphData(1, 1);
            prepareAndDrawBarPercentChart(graphData, 'chart-2');
        }
        /*
         *  --------------------------------------------------
         * Draw the Bar Chart WIth Target Percentage
         * --------------------------------------------------
         */
        var chart;
        function prepareAndDrawBarPercentChart(graphdata, chartId) {
            google.charts.setOnLoadCallback(drawStacked);
            function drawStacked() {
                var data = google.visualization.arrayToDataTable(graphdata);
                chart = new google.visualization.BarChart(document.getElementById(chartId));
                chart.draw(data, options);
            }
        }
        /*
         *  --------------------------------------------------
         * Select the Group by Selecting the Area Bar Chart
         * --------------------------------------------------
         */
        function viewAreaGroup() {
            var url = "/tasks/stats/areas/bar-graph";
            window.location = url;
        }
    </script>

@stop


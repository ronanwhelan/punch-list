<div class="row ">
    <div class="col-md-8">
        <dl>
            <dt>Number and Description</dt>
            <dd>{{$task->number}}</dd>
            <dd>{{$task->description}}</dd>
            @if($task->extension->note !== '')
            <dd class="font-xs">Note: {{$task->extension->note}}</dd>
                @endif
        </dl>
    </div>
    <div class="col-md-2">
        <dl>
            <dt>Stage</dt>
            <dd>{{$task->stage->description}}</dd>
        </dl>
    </div>
    <div class="col-md-2">
        <dl>
            <dt>Document</dt>
            <dd>{{$task->document_number}}</dd>
        </dl>
    </div>
</div>

<div class="row ">
    <div class="col-md-8">
        <dl>
            <dt>Add On Details</dt>
            <dd>{{$task->add_on}}</dd>
            <dd>{{$task->add_on_desc}}</dd>
        </dl>
    </div>
    </div>

<div class="row ">
    <div class="col-md-4">
        <dl>
            <dt>Owner</dt>
            <dd> {{$task->group->name}}</dd>
        </dl>
    </div>
    <div class="col-md-4">
        <dl>
            <dt>Category</dt>
            <dd> {{$task->taskType->name}}</dd>
        </dl>
    </div>
    <div class="col-md-4">
        <dl>
            <dt>Sub Category</dt>
            <dd> {{$task->taskSubType->name}}</dd>
        </dl>
    </div>
</div>
<hr>
<div class="row ">
    <div class="col-md-8">
        <dl>
            <dt>System</dt>
            <dd>  {{$task->system->tag}}</dd>
            <dd>  {{$task->system->description}}</dd>
        </dl>
    </div>
    <div class="col-md-4">
        <dl>
            <dt>Area</dt>
            <dd>  {{$task->area->name}}</dd>
        </dl>
    </div>

</div>
<hr>
<div class="row ">
    <div class="col-md-8">
        <dl>
            <dt>Schedule Link</dt>
            @if(isset($task->scheduleNumber->number))
            <dd>{{$task->scheduleNumber->number}}</dd>
            <dd>{{$task->scheduleNumber->description}}</dd>
            <dd>{{$task->schedule_date_choice}} date</dd>
            <dd>Schedule Import Lead/Lag: {{$task->schedule_lag_days}} days</dd>
                @else
                <dd>No Schedule Link </dd>
                @endif

            <dd>Parameter Lead/Lag: {{$task->lag_days}} days</dd>
        </dl>
    </div>
    <div class="col-md-4">
        <dl>
            <dt>Dates</dt>
            @if(isset($task->scheduleNumber->number))
            <dd> Start: {{$task->scheduleNumber->start_date}}</dd>
            <dd> Finish: {{$task->scheduleNumber->finish_date}}</dd>
            @endif
        </dl>
    </div>
</div>
<hr>
<div class="row ">
    <div class="col-md-8">
        <dl>
            <dt>Next Target Date</dt>
            <dd>{{$task->next_td->format('d-m-Y')}}</dd>
            <dd>{{$task->next_td->diffForHumans()}}</dd>

        </dl>
    </div>
    <div class="col-md-4">
        <dl>

        </dl>
    </div>
</div>
<hr>
@if ($task->complete == 0)
    <h5 id="">
        <small>Status:</small>
        In Progress
    </h5>
@else
    <h5 id="">
        <small>Status:</small>
        Complete
    </h5>
@endif
<hr>
@if(!isset($task->scheduleNumber->number))
<h3 id="" class="text-danger">
    Error
    <i class="fa fa-exclamation-circle"></i>
    No Schedule Link
</h3>
@endif
<hr>
<div class="row">
    <div class="col-md-12 text-align-center">

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="well well-sm well-light">
                    <p class="txt-color-blue">Gen/Ex <span class="semi-bold"></span></p>
                    @if($task->gen_applicable === 1)
                    {{$task->gen_perc}} % <br>
                    {{$task->gen_td->toDateString()}}
                        @else
                        <h5 class="text-align-center">No Applicable</h5>
                    @endif

                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="well well-sm well-light">
                    <p class="txt-color-blue">Review <span class="semi-bold"></span></p>
                    @if($task->rev_applicable === 1)
                    {{$task->rev_perc}} %  <br>
                    {{$task->rev_td->toDateString()}}
                    @else
                        <h5 class="text-align-center">No Applicable</h5>
                    @endif

                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="well well-sm well-light">
                    <p class="txt-color-blue">Re-Issue <span class="semi-bold"></span></p>
                    @if($task->re_issu_applicable === 1)
                    {{$task->re_issu_perc}} %  <br>
                    {{$task->re_issu_td->toDateString()}}
                    @else
                        <h5 class="text-align-center">No Applicable</h5>
                    @endif


                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="well well-sm well-light">
                    <p class="txt-color-blue">Sign Off <span class="semi-bold"></span></p>
                    @if($task->s_off_applicable === 1)
                    {{$task->s_off_perc}} %  <br>
                    {{$task->s_off_td->toDateString()}}
                    @else
                        <h5 class="text-align-center">No Applicable</h5>
                    @endif
                </div>
            </div>

    </div>
</div>
<div class=" hide row font-xs">
    <div class="col-sm-12 text-align-center">

        <hr class="data-line">
        <h3> Other Info</h3>

        <p id="">
            <small>Task Created:</small> {{$task->created_at}}</p>
        <p> Last updated by Ronan Whelan : {{$task->updated_at}}</p>

    </div>


</div>




<div id="accordion" class="" role="tablist" aria-multiselectable="true">
    <div id="search-stats" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
        <div class="row">
            <div class="col-md-6">
                <button class="  btn btn-primary outline" role="button" data-toggle="collapse" data-parent="#accordion" href="#search-stats" aria-expanded="true"
                        aria-controls="search-stats">
                    Hide Stats <i class="fa fa-angle-up"></i>
                </button>
                <div class="card-box">
                    @if(isset($stats))
                        <table class="table table-responsive table-bordered">
                            <tbody>
                            <tr>
                                <td scope="row"><span class="label label-success font-sm" id="search-stat-total">{{$stats['total']}}</span> Tasks</td>
                            </tr>
                            <tr>
                                <td scope="row"><span class="label label-info font-sm" id="search-stat-complete-percent">{{$stats['completePercent']}}</span> % Complete</td>
                            </tr>
                            <tr>
                                <td scope="row"><span class="label label-info font-sm" id="search-stat-complete">{{$stats['complete']}}</span> Complete</td>
                            </tr>
                            <tr>
                                <td scope="row"><span class="label label-danger font-sm" id="search-stat-past-due">{{$stats['pastDue']}}</span> Past Due Date</td>
                            </tr>
                            <tr>
                                <td scope="row"><span class="label label-danger font-sm" id="search-stat-non-recoverable">{{$stats['nonRecoverable']}}</span> Non Recoverable</td>
                            </tr>
                            <tr>
                                <td scope="row"><span class="label label-primary font-sm" id="search-stat-seven-days">{{$stats['dueInSevenDays']}}</span> Due within 7 days</td>
                            </tr>
                            <tr>
                                <td scope="row"><span class="label label-primary font-sm" id="search-stat-fourteen-days">{{$stats['dueInFourteenDays']}}</span> Due within 14 Days
                                </td>
                            </tr>
                            <tr>
                                <td scope="row"><span class="label label-primary font-sm" id="search-stat-one-month">{{$stats['dueInOneMonth']}}</span> Due within 1 Month:
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>


            @if(count($phaseStepPhaseStats) > 0)
                <div class="col-md-6 ">
                    <br><br>

                    <div class="card-box">
                        <div class="row">
                            <table class="table table-responsive table-bordered">
                                <tbody>
                                <tr>
                                    <td scope="row"><span class="label label-success font-sm" id="">{{$phaseStepPhaseStats['activeCount']}}</span> Active Tasks</td>
                                </tr>
                                <tr>
                                    <td scope="row"><span class="label label-danger font-sm" id="">{{$phaseStepPhaseStats['notActiveCount']}}</span> Tasks Not Active</td>
                                </tr>
                                <tr>
                                    <td scope="row"><span class="label label-info font-sm" id="">{{$phaseStepPhaseStats['inReviewCount']}}</span> Tasks in Review</td>
                                </tr>
                                <tr>
                                    <td scope="row"><span class="label label-info font-sm" id="">{{$phaseStepPhaseStats['inReIssueCount']}}</span> Tasks in Re-Issue</td>
                                </tr>
                                <tr>
                                    <td scope="row"><span class="label label-info font-sm" id="">{{$phaseStepPhaseStats['inSignOff']}}</span> Tasks for Sign Off</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        @if(isset($phaseStepPhaseStats['ownerPhaseData']))
                            @if(sizeof($phaseStepPhaseStats['ownerPhaseData']) > 0)
                                <p> Review and Approval Stats</p>
                                <table class="table table-responsive ">
                                    <thead class="text-primary" style="background-color: ghostwhite">
                                    <tr>
                                        <th>Owner</th>
                                        <th>Review</th>
                                        <th>Approval</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=0;$i < sizeof($phaseStepPhaseStats['ownerPhaseData']);$i++)

                                        <tr>
                                            <td><span class="">{{$phaseStepPhaseStats['ownerPhaseData'][$i][0]}}</span></td>
                                            <td><span class=" font-sm">{{$phaseStepPhaseStats['ownerPhaseData'][$i][1]}}</span></td>
                                            <td>{{$phaseStepPhaseStats['ownerPhaseData'][$i][2]}}</td>
                                        </tr>
                                    @endfor

                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-info" role="alert"><p> No Review and Approval Stats for this Search</p></div>

                            @endif
                        @endif
                        <div id="step-phase-stats-owners-section" class="hide">
                            <button class="btn btn-primary" onclick="getOwnerStepPhaseStats()"> Get Owner Stats</button>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <!-- end #accordion -->

    </div>
    <!-- end .col -->
</div><!-- end .row -->
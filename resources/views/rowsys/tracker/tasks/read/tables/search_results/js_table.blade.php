<!-- row -->
<div class="row">
    <!-- NEW WIDGET START -->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
        <div class="jarviswidget " id="wid-id-1" data-widget-editbutton="false"
             data-widget-colorbutton="false" data-widget-setstyle="false" data-widget-togglebutton="false"
             data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
            <header>
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                <h2><strong>Mission </strong> <i>List</i></h2>

                <div class="widget-toolbar">
                    <a href="javascript:void(0);" class="btn btn-primary hide"><i class="fa fa-refresh"></i></a>
                    <a href="javascript:void(0);" class="btn btn-primary hide">Stats <i class="fa fa-bar-chart-o"></i></a>

                    <div class="btn-group">
                        <button class="btn dropdown-toggle btn-xs btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="2">Area</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">System</a>
                            </li>

                            <li>
                                <a class="toggle-vis" data-column="4">Group</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Task Type</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="13">Revised Date</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="14">Base Date</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="15">Base Dev Days</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
            <!-- widget div-->
            <div>
                <div class="widget-body no-padding">
                    <div class=" custom-scroll  ">
                        <div id="mission_list_wrapper" class="dataTables_wrapper form-inline no-footer">
                            <table width="100%" role="grid" id="mission_list" class="table table-striped table-hover table-bordered dataTable no-footer has-columns-hidden">
                                <thead class="">
                                <tr class="">
                                    <th class="hasinput" style="width:5%"><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style="width:2%"><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style="width:1%"><input type="text" class="form-control " placeholder=""/></th>

                                    <th class="hasinput" style="width:2%"><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style="width:1%"><input type="text" class="form-control " placeholder=""/></th>

                                    <th class="hasinput" style="width:2%"><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style="width:1%"><input type="text" class="form-control " placeholder=""/></th>

                                    <th class="hasinput" style="width:2%"><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style="width:2%"><input type="text" class="form-control " placeholder=" "/></th>

                                    <th class="hasinput" style="width:2%"><input type="text" class="form-control " placeholder=" "/></th>
                                    <th class="hasinput" style="width:2%"><input type="text" class="form-control " placeholder=""/></th>

                                    <th class="hasinput" style="width:1%"><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style="width:1%"><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style="width:1%"><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style="width:1%"><input type="text" class="form-control " placeholder=""/></th>
                                    <th class="hasinput" style="width:1%"><input type="text" class="form-control " placeholder=""/></th>
                                </tr>
                                <tr class="font-xs">
                                    <th data-class="expand" class="font-xs">Number</th>
                                    <th data-hide="phone" class="font-xs">Next<br>Target<br>Date</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
                                    <th data-hide="phone,tablet" class="font-xs">Task Type</th>
                                    <th data-hide="phone,tablet" class="font-xs">Stage</th>

                                    <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Prep/Ex<br>Status &  Date</span></th>

                                    <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Review<br>Status &  Date</span></th>

                                    <th data-hide="phone" class="font-xs"><span class="txt-color-orange">ReIssue<br>Status &  Date</span></th>

                                    <th data-hide="phone" class="font-xs"><span class="txt-color-blue">SignOff<br>Status &  Date</span></th>

                                    <th data-hide="phone,tablet" class="font-xs">Target<br>Val (hrs)</th>
                                    <th data-hide="phone,tablet" class="font-xs">Earned<br>Val (hrs)</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Revised<br>Date</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Date</th>
                                    <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Dev Days</th>
                                </tr>
                                </thead>
                                <div class="text-align-center txt-color-blue" id="loading-box-section">
                                    <br><br>
                                    <span><i class="fa fa-spinner  fa-spin fa-5x"></i><br><br>Updating Table....</span>

                                </div>
                                <tbody id="mission-list-table-body" class="">
                                <!-- table is populated by AJAX and js in js/tasks/general -->


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


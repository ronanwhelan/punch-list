<div class="">
    <div class="row hide">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div id="buttons" class="font-xs"></div>
                    <div id="toggle_buttons" class="btn-group pull-right">
                        <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="1">Next Target Date</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="2">Area</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">System</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="4">Category Owner</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Category</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="18">Sub Categories</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="6">Stage</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="11">Target Hrs</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="12">Earned Hrs</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="13">Revised Date</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="14">Base Date</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="15">Base Dev Days</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="16">Schedule Link</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="17">Document Number</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="19">Assigned Team</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="20">Assigned Person</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Tasks</div>
        <div class="panel-body">
            <div class="row" id="table-section" style="opacity: .08">
                <div class="col-lg-12 font-xs">
                    <table id="data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr class="font-xs">
                            <th data-class="expand" class="font-xs">Number</th>
                            <th data-hide="phone" class="font-xs">Next<br>Target<br>Date</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Owner</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Category</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Stage</th>

                            <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Prep/Exec<br>Status &  Date</span></th>

                            <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Review<br>Status &  Date</span></th>

                            <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Update<br>Status &  Date</span></th>

                            <th data-hide="phone" class="font-xs"><span class="txt-color-blue">SignOff<br>Status &  Date</span></th>

                            <th data-hide="phone,tablet,computer" class="font-xs">Target<br>Val (hrs)</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Earned<br>Val (hrs)</th>

                            <th data-hide="phone,tablet,computer" class="font-xs">Revised<br>Date</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Date</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Dev Days</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Schedule<br>Link</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Document<br>Number</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Sub<br>Category</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Assigned<br>Team</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Assigned<br>Person</th>
                        </tr>
                        </thead>
                        {{--                <tfoot>
                                        <tr class="font-xs">
                                            <th data-class="expand" class="font-xs">Number</th>
                                            <th data-class="phone" class="font-xs">Details</th>
                                            <th data-hide="phone" class="font-xs">System/Location</th>
                                            <th data-hide="phone" class="font-xs"><br>Due Date</th>
                                            <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Responsible</span></th>
                                            <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-orange">Assigned</span></th>
                                            <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Status<br></span></th>

                                            <th data-hide="phone,tablet,computer" class="font-xs">Category</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">Priority</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">Location</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs">Type</th>
                                            <th data-hide="phone,tablet,computer" class="font-xs"><span class="txt-color-orange">Raised By<br> Company</span></th>

                                        </tr>
                                        </tfoot>--}}
                        <tbody>
                        @foreach( $tasks as $task)

                            <tr id="main-row-{{$task->id}}" onclick="showEditTaskModal({{$task->id}})" class=" task-table-row"
                                style="background-color: {{$task->getTaskNextStepBackGroundColour()['rowColour']}}">

                                <td id="row-{{$task->id}}" class="hover-over-item-number">

                                    <strong>{{$task->number}}
                                        @if($task->flag === 1)
                                            &nbsp;&nbsp;&nbsp;<span class=""><i class="fa fa-flag-o"></i></span>
                                        @endif


                                        {{--           @if($task->isOnMyWatchList() === 1)
                                                       &nbsp;&nbsp;&nbsp;<span class=""><i class="fa fa-thumbs-o-up"></i></span>
                                                   @endif--}}

                                    </strong>

                                    <br>

                                    <span class=" text-primary"><i class="fa fa-star"></i>&nbsp;{{$task->description}}

                                        @if($task->add_on !== '')
                                            <br>
                                            &nbsp;{{$task->add_on}} {{$task->add_on_desc}}
                                        @endif
                                        @if($task->document_number !== '')
                                            {{$task->document_number}}
                                        @endif


                                        @if($task->compile_error > 0)
                                            <div style="color: white">
                                                <br>ERROR Number {{$task->compile_error}} <i class="fa fa-exclamation-circle"></i><br>
                                                @if($task->compile_error > 0 && $task->compile_error < 2 )
                                                    No Schedule Link
                                                @endif
                                                @if($task->compile_error > 1 && $task->compile_error < 3)
                                                    No Rule for this Stage and Task Type
                                                @endif
                                                @if($task->compile_error > 2)
                                                    Unknown Error Code - Please contact PES Admin
                                                @endif
                                            </div>
                                        @endif
                                    </span>


                                    <br><span class=" text-warning"><i class="fa fa-key"></i>&nbsp; {{$task->stage->name}}</span>


                                    @if( isset($task->extension) &&
                                     ! is_null($task->extension->who_updated_last_id) &&
                                      $task->extension->who_updated_last_id !== "" &&
                                       $task->extension->who_updated_last_id !== 0
                                       )
                                        <br><i class="fa fa-user"></i>  last updated by:
                                        {{$task->extension->whoUpdatedLast['full_name']}} {{$task->extension->updated_at->diffForHumans()}}
                                    @endif
                                    {{--<br><span class=" font-xs"><i class="fa fa-user"></i>&nbsp; Last updated by: {{$task->lastUpdatedBy()}}</span>--}}

                                    @if(isset($task->extension->note) && $task->extension->note !== '' )
                                        <br><br>
                                        <span class="font-sm text-primary"><i class="fa fa-sticky-note-o"></i>&nbsp;{{$task->extension->note}}</span>
                                    @endif

                                </td>


                                <td id="{{$task->id}}-next-td" class="">
                                    <br>
                                {{$task->next_td->toDateString()  }}

                                <td id="{{$task->id}}-area" class="font-xs">{{$task->area->name}}</td>

                                <td id="{{$task->id}}-system" class="font-xs"><strong>{{$task->system->tag}}</strong> <br>({{$task->system->description}})</td>

                                <td id="{{$task->id}}-group" class="font-xs">{{$task->group->name}}</td>

                                <td id="{{$task->id}}-type" class="font-xs">{{$task->taskType->name}} ({{$task->taskType->short_name}})</td>

                                <td id="{{$task->id}}-stage" class="font-xs">{{$task->stage->name}}</td>


                                <td id="{{$task->id}}-gen-perc" style="background-color: {{$task->getTaskNextStepBackGroundColour()['genColour']}}">
                                    @if($task->gen_applicable === 1)
                                        {{$task->gen_td->toFormattedDateString() }}<br> {{ceil($task->gen_perc)}} %
                                        @if($task->gen_perc === 100)
                                            <br><span class="" style="font-size: 50%">(completed {{$task->gen_complete_date->toFormattedDateString() }})</span>
                                        @endif
                                    @else
                                        N/A
                                    @endif
                                </td>


                                <td id="{{$task->id}}-rev-perc" style="background-color: {{$task->getTaskNextStepBackGroundColour()['revColour']}}">
                                    @if($task->rev_applicable === 1)
                                        {{$task->rev_td->toFormattedDateString() }} <br>{{ceil($task->rev_perc)}} %
                                        @if($task->rev_perc === 100)
                                            <br><span class="" style="font-size: 50%">(completed {{$task->rev_complete_date->toFormattedDateString() }})</span>
                                        @endif
                                    @else
                                        N/A
                                    @endif
                                </td>


                                <td id="{{$task->id}}-re-issu-perc" style="background-color: {{$task->getTaskNextStepBackGroundColour()['reIssuColour']}}">
                                    @if($task->re_issu_applicable === 1)
                                        {{$task->re_issu_td->toFormattedDateString() }} <br>{{ceil($task->re_issu_perc)}} %
                                        @if($task->re_issu_perc === 100)
                                            <br><span class="" style="font-size: 50%">(completed {{$task->re_issu_complete_date->toFormattedDateString() }}
                                                )</span>
                                        @endif
                                    @else
                                        N/A
                                    @endif
                                </td>


                                <td id="{{$task->id}}-s-off-perc" style="background-color: {{$task->getTaskNextStepBackGroundColour()['sOffColour']}}">
                                    @if($task->s_off_applicable === 1)
                                        {{$task->s_off_td->toFormattedDateString() }} <br>{{ceil($task->s_off_perc)}} %
                                        @if($task->s_off_perc === 100)
                                            <br><span class="" style="font-size: 50%">(completed {{$task->s_off_complete_date->toFormattedDateString() }}
                                                )</span>
                                        @endif
                                    @else
                                        N/A
                                    @endif
                                </td>

                                <td id="{{$task->id}}-target-val">{{$task->target_val}}</td>
                                <td id="{{$task->id}}-earned-val">{{$task->earned_val}}</td>

                                <td id="{{$task->id}}-rev-proj-td">{{$task->revised_projected_date->format('d-m-Y')}}</td>
                                <td id="{{$task->id}}-base-td">{{$task->base_date->format('d-m-Y')}}</td>
                                <td id="{{$task->id}}-base-dev-days">{{$task->base_dev_days}}</td>

                                <td id="{{$task->id}}-schedule-link">
                                    @if(isset($task->scheduleNumber->number))
                                        {{$task->scheduleNumber->number}}
                                        <div class="font-xs font-xs">{{$task->scheduleNumber->description}}</div>
                                    @else
                                        <div style="color: white">
                                            ERROR <i class="fa fa-exclamation-circle"></i><br>
                                            {{$task->schedule_number}}
                                        </div>
                                    @endif
                                </td>
                                <td id="{{$task->id}}-doc-num">{{$task->document_number}}</td>
                                <td id="{{$task->id}}-doc-num">{{$task->taskSubType->name}}</td>

                                <td id="{{$task->id}}-doc-num">
                                    @if(isset($task->groupOwner->name))
                                        {{$task->groupOwner->name}}
                                    @else
                                        Unknown!
                                    @endif

                                </td>

                                <td id="{{$task->id}}-doc-num">
                                    @if( ! is_null($task->assigned_user_id ) && $task->assigned_user_id !== "" && $task->assigned_user_id !== 0 )
                                        {{$task->assignedUser['name']}}  {{$task->assignedUser['surname']}}
                                    @else
                                        NOT ASSIGNED
                                    @endif
                                    {{--  {{$task->getAssignedUserDetails()}}--}}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>















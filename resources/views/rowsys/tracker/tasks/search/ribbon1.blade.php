<div class="row">
    <div class="col-lg-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Search Result Stats</a>
                    </h4>
                </div>

                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">

                        <div id="search-options" class="" hidden="">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <select class="form-control selectpicker " data-style="btn-primary" multiple title="Area">>
                                                @foreach ($areas as $area)
                                                    <option value="{{$area->id}}">{{$area->name}}, {{$area->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-live-search="true"
                                                    data-style="btn-primary" multiple title="System">
                                                @foreach ($systems as $system)
                                                    <option value="{{$system->id}}">{{$system->tag}}, {{$system->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-style="btn-info" multiple title="Task Group">
                                                @foreach ($groups as $group)
                                                    <option value="{{$group->id}}">{{$group->name}}, {{$group->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-style="btn-info" multiple title="Task Type">
                                                @foreach ($taskTypes as $taskType)
                                                    <option value="{{$taskType->id}}">{{$taskType->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="inlineCheckbox1" checked value="option1"> Preparation
                                            </label>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="inlineCheckbox2" checked value="option2"> Execution
                                            </label>

                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 ">
                                            <div class="form-group">
                                                <label class="checkbox-inline"><input type="checkbox" checked value="">Complete</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <select class="selectpicker form-control show-tick" multiple data-max-options="1" multiple title="Choose Time">
                                        <option value="1" selected >Anytime</option>
                                        <option value="2">Due in 7 Days</option>
                                        <option value="3">Due in 2 weeks</option>
                                        <option value="4">Due in a month</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <button class="btn btn-success " onclick="">Get Tasks</button>
                            <br>
                        </div>


                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-responsive">
                                    <tbody>
                                        <tr>
                                            <td scope="row">
                                                <span class="label label-success font-sm">13011</span>Tasks returned from the Search
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row">
                                                <span class="label label-info font-sm">3%</span>Complete
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row">
                                                <span class="label label-danger font-sm">151</span>Tasks that are Past Due:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row">
                                                <span class="label label-primary font-sm">167</span>Tasks Due within 7 days:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"> <span class="label label-primary font-sm">178</span>Tasks Due within 14 Day:
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                                    <button class="btn btn-info btn-xs" onclick="taskTableShowSearchOptions()">Show Search Options</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

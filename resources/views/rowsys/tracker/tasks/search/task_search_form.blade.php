<input type="hidden" name="search_active" value="search_active">
<div class="row">
    <div class="col-lg-12">
        <div class="form-group ">
            <input type="text" class="form-control input-lg" id="search-task-number" placeholder="search task description" name="search_task_number">
            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker"
                    id="search-area" name="search_areas[]"
                    data-style="btn-primary" multiple title="Area"
                    onchange="getFilteredSystemsByArea('search')">
                <optgroup label="Areas">
                    @foreach ($areas as $area)
                        @if(in_array($area->id,$selectedAreasArray))
                            <option selected value="{{$area->id}}">{{$area->name}}</option>
                        @else
                            <option value="{{$area->id}}">{{$area->name}}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker " id="search-system" name="search_systems[]" data-live-search="true" data-style="btn-primary" multiple title="System">
                <optgroup label="Systems">
                    @foreach ($systems as $system)
                        @if(in_array($system->id,$selectedSystemsArray))
                            <option selected class="" value="{{$system->id}}">{{$system->tag}}, {{$system->description}}</option>
                        @else
                            <option class="" value="{{$system->id}}">{{$system->tag}}, {{$system->description}}</option>
                        @endif

                    @endforeach
                </optgroup>
            </select>
            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker" id="search-groups" name="search_groups[]"
                    data-style="btn-primary" multiple title="Owner"
                    onchange="getFilteredTypesByGroup()">
                <optgroup label="Owner">
                    @foreach ($groups as $group)
                        @if(in_array($group->id,$selectedGroupsArray))
                            <option selected value="{{$group->id}}">{{$group->name}}</option>
                        @else
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker" id="search-types" name="search_types[]"
                    onchange="getFilteredSubTypesByType()" data-live-search="true"
                    data-style="btn-primary" multiple title="Category">
                <optgroup label="Categories">
                    @foreach ($taskTypes as $type)
                        @if(in_array($type->id,$selectedTypesArray))
                            <option selected value="{{$type->id}}">{{$type->name}} ({{$type->short_name}})</option>
                        @else
                            <option value="{{$type->id}}">{{$type->name}} ({{$type->short_name}})</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>
@if(count($taskSubTypes) > 0)
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <select class="form-control selectpicker" id="search-test-specifications" name="search_task_sub_type[]" data-style="btn-primary" multiple title="Sub Categories">
                    <optgroup label="Test Specs">
                        @foreach ($taskSubTypes as $taskSubType)
                            <option value="{{$taskSubType->id}}">{{$taskSubType->name}} , {{$taskSubType->description}}</option>
                        @endforeach
                    </optgroup>
                </select>
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker" id="search-roles" name="search_roles[]" data-style="btn-primary" multiple title="Responsible Team">
                <optgroup label="Teams">

                    @foreach ($roles as $role)
                        @if(in_array($role->id,$selectedRolesArray))
                            <option selected value="{{$role->id}}">{{$role->name}}</option>
                        @else
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-lg-12"><h4>Stages and Status:</h4></div>
        <div class="col-lg-3">
            <label class="checkbox-inline">
                @if($genStageSelected)
                    <input type="checkbox" id="search-gen" name="search_gen" checked value="{{$stages[0]->id}}">Preparation
                @else
                    <input type="checkbox" id="search-gen" name="search_gen" value="{{$stages[0]->id}}">Preparation
                @endif

            </label>
        </div>
        <div class="col-lg-3">
            <label class="checkbox-inline">
                @if($exeStageSelected)
                    <input type="checkbox" id="search-exe" name="search_exe" checked value="{{$stages[0]->id}}">Execution
                @else
                    <input type="checkbox" id="search-exe" name="search_exe" value="{{$stages[0]->id}}">Execution
                @endif

            </label>
        </div>
        <div class="col-lg-6">
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="all"> All
            </label>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="done"> Done
            </label>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio3" checked value="not-done"> Not Done
            </label>
        </div>
    </div>
</div>


<div class="row ">
    <div class="col-lg-12"><p>Step Status:

        <p></div>
    <div class="col-lg-12">
        <div class="form-group">
            <select class="selectpicker form-control show-tick" id="search-status" name="search_status" data-style="btn-primary" data-max-options="1" title="Choose Status">
                <option selected value="1">All</option>
                <option value="2">In Review</option>
                <option value="3">In Sign-off</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12"><p>Choose a time:</p></div>
    <div class="col-lg-12">
        <div class="form-group">
            <select class="selectpicker form-control show-tick" id="search-time" name="search_time" data-style="btn-primary" data-max-options="1" title="Choose Time">
                <option value="1" selected>Anytime</option>
                <option value="2">Due within 7 Days</option>
                <option value="3">Due within 2 weeks</option>
                <option value="4">Due within 1 Month</option>
                <option value="5">Past Due</option>
                <option value="6">Date Range</option>
            </select>
        </div>
    </div>
</div>
<div id="search-date-range-section" hidden>
    <div class="row">
        <div class="col-lg-12"><h4>Date Range:</h4></div>
        <div class="col-lg-12">
            <div class="input-group input-daterange">
                <input type="date" id="search-from-date" name="search_from_date" class=" datepicker"
                       data-date-format="yyyy-mm-dd"
                       value="{{\Carbon\Carbon::now()->toDateString()}}">

                TO
                <span class=" hide input-group-addon">to</span>

                <input type="date" id="search-to-date" name="search_to_date" class=" datepicker"
                       data-date-format="yyyy-mm-dd"
                       value="{{\Carbon\Carbon::now()->addMonths(6)->toDateString()}}">
            </div>
        </div>
    </div>
    <br>
</div>


<div class="row">
    <div class="col-lg-12"><p>Sort By:</p></div>
    <div class="col-lg-12">
        <div class="form-group">
            <select class="selectpicker form-control show-tick" id="search-order-by" data-style="btn-primary" name="search_order_by" data-max-options="1" title="order By">
                <option value="1" selected>Next Target Date</option>
                <option value="2">Task Number</option>
                <option value="3">Area Name</option>
                <option value="4">System Tag</option>
                <option value="5">Task Group</option>
                <option value="6">Task Type</option>
                <option value="7">Stage</option>
            </select>
        </div>
    </div>
</div>


<div class="row pad-20">
    <div class="col-lg-12"><h4>Search Results:</h4></div>
    <div class="col-lg-12">
        <table class="table table-responsive table-bordered">
            <tbody>
            <tr>
                <td scope="row"><span class="label label-success font-sm" id="search-stat-total">{{$stats['total']}}</span> Tasks</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-info font-sm" id="search-stat-complete-percent">{{$stats['completePercent']}}</span>% Complete</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-info font-sm" id="search-stat-complete">{{$stats['complete']}}</span> Complete</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-danger font-sm" id="search-stat-past-due">{{$stats['pastDue']}}</span> Past Due Date</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-danger font-sm" id="search-stat-non-recoverable">{{$stats['nonRecoverable']}}</span> Non Recoverable</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-primary font-sm" id="search-stat-seven-days">{{$stats['dueInSevenDays']}}</span> Due within 7 days</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-primary font-sm" id="search-stat-fourteen-days">{{$stats['dueInFourteenDays']}}</span> Due within 14 Days
                </td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-primary font-sm" id="search-stat-one-month">{{$stats['dueInOneMonth']}}</span> Due within 1 Month:
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>


    $("#search-task-number").blur(function () {
        getSearchStats();
    });

    //.autocomplete("option", "appendTo", "#copy_dialog");
    $('#search-task-number').keyup(function () {
        getSearchStats();
        /*        vartagObj = $('#search-task-number');
         var tag = vartagObj.val();
         var source = "/get-task-number-autocomplete-list/" + tag;
         vartagObj.autocomplete({
         source: source,
         minLength: 1,
         //autoOpen: true,
         //width: 500,
         //modal: false,
         //zIndex: 10000000,
         //appendTo: "search-task-number",
         //title: 'Duplicate',
         select: function (event, ui) {
         //vartagObj.val(ui.item.value);
         console.log(ui.item.id);
         }
         });*/
    });


    $(document).ready(function () {
        checkIfOnMobile();

        $('.datepicker').datepicker({
            keyboardNavigation: true,
            calendarWeeks: true,
            autoclose: true
        });

        $(document).ready(function () {
            if (screen.width <= 480) {
                $('.selectpicker').selectpicker('mobile');
                $('.datepicker').datepicker('mobile');
                //$('#screenSize').val('2');
            }
        });
        getSearchStats();
    });

    //$(selectpickerButton).trigger('click');

    $("#search-time").change(function () {
        if ($('#search-time :selected').val() > 5) {
            $('#search-date-range-section').fadeIn(1500);
            console.log('more than 5');
        } else {
            $('#search-date-range-section').fadeOut(1500);
            console.log('less than 5');
        }
        $("#search-time").trigger('click');

    });


    /*===============================================
     /         TASKS- AUTO COMPLETE
     =================================================*/
    /*$("#search-task-number").keyup(function () {
     getSearchStats();
     *//*var numberTextField = $('#search-task-number');
     var tag = numberTextField.val();
     $(function () {
     function log(message) {
     console.log('function called');
     $("<div>").text(message).prependTo("#log");
     $("#log").scrollTop(0);
     }


     console.log('autocomplete called.');
     $("#search-task-number").autocomplete({
     source: "/get-task-number-autocomplete-list/" + tag,
     minLength: 3,
     select: function (event, ui) {
     log(ui.item ?
     "Selected: " + ui.item.value + " aka " + ui.item.id :
     "Nothing selected, input was " + this.value);

     }

     });






     });
     //When the text field looses focuus ,let the modal get focus
     numberTextField.focusout(function () {
     $('#user-filter-modal').focus();
     }).blur(function () {
     blur++;
     $('#user-filter-modal').focus();
     });*//*
     });*/
</script>
<div class="">
    <div class="row hide ">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body ">
                    <div id="buttons" class="font-xs"></div>
                    <div id="toggle_buttons" class="btn-group pull-right">
                        <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="1">STATUS</a>
                            </li>
                            @for($i = 0;$i < sizeof($columnHeaders);$i++)
                                <li>
                                    <a class="toggle-vis" data-column="{{$i+2}}"> {{$columnHeaders[$i][2]}} :{{$columnHeaders[$i][0]}}</a>
                                </li>
                            @endfor
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row" id="table-section" style="opacity: .08">
                <div class="col-lg-12 ">

                    <table id="data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr class="">
                            <th data-class="expand" class="font-xs">System</th>
                            <th data-hide="" class="font-xs">Status %<br>tasks totals</th>
                            @foreach($columnHeaders as $header)
                                <th data-hide="phone" class="font-xs text-align-center">
                                    {{$header[0]}}<br> {{$header[2]}}{{--<p class="" data-toggle="tooltip" data-placement="left" title="{{$header[1]}}">  {{$header[0]}}</p>--}}
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        {{-- Insert For each to populate table--}}
                        @foreach($tableData as $data)
                            <tr class="text-align-center">
                                <td class="font-lg text-left "><strong>{{$data[0]}}</strong><br>{{$data[1]}} </td>
                                <td class="text-left ">
                                    <br>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success progress-bar-striped"
                                             role="progressbar"
                                             aria-valuenow="10"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$data[3]}}%">
                                            <span class="">{{$data[3]}}%</span>
                                        </div>
                                    </div>
                                    {!!$data[2]!!}
                                </td>
                                @for($i = 4;$i < (sizeof($categories)+4);$i++)
                                    <td style="background-color: {{$data[$i][0]}} ">
                                          <span style="font-size: 80%"> {{$data[$i][7]}} <br>
                                              {{$data[$i][2]}}<br></span>

                                        <p class="" data-toggle="tooltip" data-placement="bottom" title="{{$data[$i][4]}}"> {!!$data[$i][3]!!}</p>
                                    </td>
                                    {{--<td><p class="" data-toggle="tooltip" data-placement="bottom" title="{{$data[$i][3]}} {{$data[$i][1]}} ">{!!$data[$i][2]!!}</p>{{$data[$i][1]}}</td>--}}
                                @endfor
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




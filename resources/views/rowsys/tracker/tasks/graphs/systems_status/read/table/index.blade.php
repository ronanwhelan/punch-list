@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/vendor/datatables/datatables.responsive.css"/>
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><p class="panel-title">Systems Overview Table by Category </p></div>
                <div class="panel-body">
                    <form action="/tasks/charts/systems/status" method="get">
                        {{ csrf_field() }}
                        <div class="col-md-2">
                            {{--<label class=" control-label">Stage:</label>--}}
                            <div class="form-group">
                                <select class="form-control selectpicker" multiple
                                        id="area" name="areas[]"
                                        data-style="btn-primary" onchange="getFilteredSystemsByArea()" title="Areas" data-live-search="true">
                                    <optgroup label="Areas">
                                        @foreach ($areas as $area)
                                            @if(in_array($area->id,$old['areas']))
                                                <option value="{{$area->id}}" selected>{{$area->name}}</option>
                                            @else
                                                <option value="{{$area->id}}">{{$area->name}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-2">
                            {{--<label class=" control-label">Stage:</label>--}}
                            <div class="form-group">
                                <select class="form-control selectpicker" multiple
                                        id="system" name="systems[]"
                                        data-style="btn-primary" title="Systems" data-live-search="true">
                                    <optgroup label="Systems">
                                        @foreach ($systems as $system)
                                            @if(in_array($system->id,$old['systems']))

                                                <option value="{{$system->id}}" selected>{{$system->tag}} : {{$system->description}}</option>
                                            @else
                                                <option value="{{$system->id}}">{{$system->tag}}  : {{$system->description}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class=" col-md-1">
                            <div class="form-group">
                                <div class="checkbox input-md">
                                    <label><input type="checkbox" name="chk_show_all" value="on">Show All</label>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-2">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block"><i class="fa fa-table "></i><span class=" hidden-md "> Show Table</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-primary">

        <div class="panel-body">
            <div class="row">
                <div id="myTabs" class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#system-table" aria-controls="system-table" role="tab" data-toggle="tab">Table</a></li>
                        <li><a href="#report-config" aria-controls="report-config" role="tab" data-toggle="tab">Config</a></li>
                        <li class="hide"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div id="myTabContent" class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="system-table">

                            @include('rowsys.tracker.tasks.graphs.systems_status.read.table.table')
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="report-config">
                            <form class="form " role="form" id="update-report-config-form" method="GET">
                                {{ csrf_field() }}
                                <br>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-md-2">
                                            <label class=" control-label">Stage:</label>

                                            <div class="form-group">
                                                <select class="form-control selectpicker"
                                                        id="stage" name="stage"
                                                        data-style="btn-info" title="Stage">
                                                    <optgroup label="Stages">
                                                        @foreach ($stages as $stage)
                                                            <option value="{{$stage->id}}">{{$stage->name}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <label class=" control-label">Category:</label>

                                            <div class="form-group">
                                                <select class="form-control selectpicker"
                                                        id="type" name="type" data-live-search="true"
                                                        data-style="btn-info" title="Category">
                                                    <optgroup label="Categories">
                                                        @foreach ($types as $type)
                                                            <option value="{{$type->id}}">{{$type->name}} : ({{$type->short_name}})</option>
                                                        @endforeach
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>

                                        <div class=" col-md-2">
                                            <label class=" control-label"></label>

                                            <div class="form-group">
                                                <button type="button" id="add-category-button" name="add-category-button" onclick="addConfigurationCategoryAndReturnTable()"
                                                        class="btn btn-success btn-block"><i class="fa fa-plus "></i><span class=" hidden-md "> Add</span></button>
                                            </div>
                                        </div>

                                        <div class=" col-md-1">
                                            <label class=" control-label"></label>

                                            <div class="form-group">
                                                <button type="button" id="clear-category-button" name="clear-category-button" onclick="clearConfigTable()" class="btn btn-warning">
                                                    <i class="fa fa-eraser "></i><span class=" hidden-md "> Clear</span></button>
                                            </div>
                                        </div>

                                        <div class=" col-md-1 hide">
                                            <br>
                                            <label class="checkbox-inline"><input name="chk_select_all" type="checkbox" value="">Select All</label>
                                        </div>

                                        <div class=" col-md-1 hide">
                                            <label class=" control-label"></label>
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary btn-block" onclick="reloadSystemReportTable()"><i class="fa fa-refresh "></i> Reload
                                                </button>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="alert alert-info fade in" id="alert-no-stage-and-category-section" hidden>
                                    <button data-dismiss="alert" class="close">×</button>
                                    <i class="fa-fw fa fa-info"></i>
                                    <strong>Info</strong> Please select a stage and category
                                </div>
                            </form>


                            <div id="config-table-section"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="messages">
                            <button type="button" onclick="reportConfigTable()" class="btn btn-primary">Get Table</button>
                            <br>

                            <div class="panel panel-default">
                                <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                                <div class="panel-body">Panel content</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ==========================CONTENT ENDS HERE ========================== -->
@stop

@section ('local_scripts')

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script>

        var table = $('#data_table');

        $(document).ready(function () {
            var screenHeight = $(document).height();


            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();

            screenHeight = (screenHeight - 600) + "px";

            var responsiveHelper;

            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            table = $('#data_table').DataTable({
                dom: 'Bfrtip',
                buttons: ['copy', 'excel'],
                //"sDom":"flrtip",
                "bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                "bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 100,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                responsive: true,
                // paging: false,
                "bStateSave": true, // saves sort state using localStorage
                autoWidth: false,
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(table, breakpointDefinition);
                    }
                },
                rowCallback: function (nRow) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                drawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }

            });
            // Apply the filter
            $("#example thead th input[type=text]").on('keyup change', function () {
                table.column($(this).parent().index() + ':visible').search(this.value).draw();
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: [//'excel'//'copyHtml5',//'excelHtml5',//'csvHtml5',//'pdfHtml5'
                ]
            }).container().appendTo($('#buttons'));
            table.buttons().container().appendTo($('.col-sm-6:eq(0)', table.table().container()));

            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = table.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });

            $('#table-section').fadeTo(1500, '1');
            $('#loading-box-section').remove();

            //--------------------------------------------------------
            //      TOGGLE BUTTONS
            //--------------------------------------------------------
            //Add Toggle Buttons to top of table in the #_filter section
            var toggleButtons = $('#toggle_buttons');
            var tableFilterSection = $('#data_table_filter');

            var dtButtons = $('.dt-buttons');
            dtButtons.addClass('hidden-xs hidden-sm hidden-md ');
            //tableFilterSection.append('<div class="">Task List</div>');
            tableFilterSection.append('&nbsp;&nbsp;').append(toggleButtons);


            //--------------------------------------------------------
            //      EXPORT BUTTONS
            //--------------------------------------------------------
            tableFilterSection.append('&nbsp;&nbsp;').append(
                    '<div class="btn-group hidden-xs hidden-sm"> <button type="button" ' +
                    'class="btn btn-primary dropdown-toggle" ' +
                    'data-toggle="dropdown" aria-haspopup="true" ' +
                    'aria-expanded="false"> Export <span class="caret"></span> </button> <ul class="dropdown-menu"> ' +
                    '<li id="export-buttons" style="padding: 10px;"><a href="#"></a></li> ' +
                        //'<li><a href="#">Excel</a></li> ' +
                        //'<li><a href="#">Something else here</a>' +
                    '</li> </ul> </div>').append('&nbsp;&nbsp;');

            var exportButton1 = $('#export-button-1');
            var exportButtons = $('#export-buttons');
            var copyButton = $('.buttons-copy');
            var excelButton = $('.buttons-excel');
            exportButtons.append('&nbsp;&nbsp;').append(dtButtons);
            //exportButton2.append(excelButton);


            var tablePaginateSection = $('#data_table_paginate');
            //tablePaginateSection.css("background-color", "navy");


            //get the cofiguration table
            reportConfigTable();

        });


        //--------------------------------------------------------
        //      Get the Configuration Table
        //--------------------------------------------------------
        function reportConfigTable() {

            //var systemId = $('#system').val();
            //var milestone = $('#milestone').val();
            // console.log('get milestone form' + systemId + milestone );
            //var data = {systemId:systemId,milestone:milestone};
            pleaseWait('please wait');
            $.ajax({
                type: 'GET',
                //data: {data: data},
                url: "/tasks/charts/systems/status/config-table", success: function (result) {
                    console.log(result);
                    $('#config-table-section').html(result);
                    waitDone();
                }
            });

        }


        //--------------------------------------------------------
        //     ADD Configuration Category for the report
        //--------------------------------------------------------
        function addConfigurationCategoryAndReturnTable() {

            //addSpinnerToButton('add-category-button');

            var stage = $('#stage').val();
            var type = $('#type').val();
            console.log('' + stage + type);
            var data = {stage: stage, type: type};
            if (stage !== '' && type != '') {
                pleaseWait('updating table');
                $('#alert-no-stage-and-category-section').fadeOut(300);
                $('#add-category-button').append(' <i class="fa fa-spinner fa-spin"></i>').addClass('disable');
                $.ajax({
                    type: 'GET',
                    data: {data: data},
                    url: "/tasks/charts/systems/status/update-config", success: function (result) {
                        console.log(result);
                        //$('#config-table-section').html(result);
                        reportConfigTable();
                        $('#add-category-button').html('<i class="fa fa-plus"></i> Add').removeClass('disable');
                        waitDone();
                    }
                });
            } else {
                $('#alert-no-stage-and-category-section').fadeIn(200);

            }


        }

        //--------------------------------------------------------
        //     Reload the System Report Table
        //--------------------------------------------------------
        function reloadSystemReportTable() {
            location.reload();
        }

        //--------------------------------------------------------
        //     Clear the Configuration Table
        //--------------------------------------------------------
        function clearConfigTable() {
            $('#clear-category-button').append('<i class="fa fa-spinner fa-spin"></i>').addClass('disable');
            pleaseWait('updating table');
            $.ajax({
                type: 'GET',
                url: "/tasks/charts/systems/status/clear-config-data", success: function (result) {
                    console.log(result);
                    reportConfigTable();
                    $('#clear-category-button').html('<i class="fa fa-eraser"> Clear</i>').removeClass('disable');
                    waitDone();
                }
            });
        }

        //--------------------------------------------------------
        //     Add a spinner to a button - Ajax call
        //--------------------------------------------------------
        function addSpinnerToButton(id) {
            id = "#" + id;
            $(id).html('<i class="fa fa-spinner fa-spin"></i>').addClass('disable');

        }

        //--------------------------------------------------------
        //     remove a spinner to a button - Ajax call
        //--------------------------------------------------------
        function removeSpinnerToButton(id) {


        }


    </script>

@stop

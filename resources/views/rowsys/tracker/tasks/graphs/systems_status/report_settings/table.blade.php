<hr>
<h2> Configuration Table</h2>
<p>This is the configuration for the System Category Report table.</p>
<div class="panel panel-primary">
    <div class="panel-body">
        <!-- Table -->
        <table class="table table-bordered">
            <thead>
            <tr class="strong bg-info">
                <th>Col</th>
                <th>Stage</th>
                <th>Category</th>
            </tr>
            </thead>

            <tbody>
            <?php $i = 1; ?>
            @foreach($tableData as $type)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$type[0]}}</td>
                    <td>{{$type[1]}}</td>
                </tr>
                <?php $i = $i + 1; ?>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

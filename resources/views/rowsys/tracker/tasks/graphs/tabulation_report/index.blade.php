@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/vendor/datatables/datatables.responsive.css"/>
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                <div class="panel-heading"><h3 class="panel-title">Tabulation Report - Chart Selection </h3></div>
                <div class="panel-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <i class="fa fa-check"> </i>{{ Session::get('message') }}
                        </div>
                    @endif

                    <form action="/tasks/charts/category-tabulation" method="get">
                        {{ csrf_field() }}

                        <div class="col-md-2">
                            <label class=" control-label">Stage:</label>
                            <div class="form-group">
                                <select class="form-control selectpicker"
                                        id="stage" name="stage"
                                        data-style="btn-primary" title="Stage">
                                    <optgroup label="Stages">
                                        <option value="0">Both</option>
                                        @foreach ($stages as $stage)
                                            @if ($old['stage'] === $stage->id)
                                                <option value="{{$stage->id}}" selected>{{$stage->name}}</option>
                                            @else
                                                <option value="{{$stage->id}}">{{$stage->name}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-2">
                            <div class="form-group">
                                <label class=" control-label font-xs">Category Owner:</label>
                                <select class="form-control selectpicker" multiple
                                        id="group" name="group[]"
                                        data-style="btn-primary" title="Category Owner">
                                    <optgroup label="Category Owners">
                                        <option value="0">All</option>
                                        @foreach ($groups as $group)
                                            @if (in_array($group->id,$old['group']))
                                                <option value="{{$group->id}}" selected>{{$group->name}}</option>
                                            @else
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label class=" control-label">Category:</label>

                            <div class="form-group">

                                <select class="form-control selectpicker"
                                        id="type" name="type[]" multiple data-live-search="true"
                                        data-style="btn-primary" title="Category">
                                    <optgroup label="Categories">
                                        <option value="0"></option>
                                        @foreach ($types as $type)
                                            @if (in_array($type->id,$old['type']))
                                                <option value="{{$type->id}}" selected>{{$type->name}} ({{$type->short_name}})</option>
                                            @else
                                                <option value="{{$type->id}}">{{$type->name}} ({{$type->short_name}})</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class=" control-label">Team:</label>

                            <div class="form-group">
                                <select class="form-control selectpicker"
                                        id="role" name="role[]" multiple
                                        data-style="btn-primary" title="Teams">
                                    <optgroup label="Teams">
                                        @foreach ($areas as $area)
                                            @if (in_array($area->id,$old['role']))
                                                <option value="{{$area->id}}" selected>{{$area->name}}</option>
                                            @else
                                                <option value="{{$area->id}}">{{$area->name}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>

                        <div class=" col-md-2 ">
                            <div class="form-group">
                                <label class=" control-label">Before Date:</label>
                                <input type="hidden" id="old_search_date" value="{{$old['date']}}">

                                <div class="input-group date">
                                    <input type="text"  id="to_date" onchange="" name="to_date" class=" datepicker"
                                           data-type="date"  data-date-format="yyyy-mm-dd" value="">
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-2">
                            <label class=" control-label"></label>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block"><i class="fa fa-table ">

                                    </i><span class=" hidden-md "> Table</span></button>
                            </div>
                        </div>

                        @if ($errors->has() && sizeof($errors) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}<br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-info">
        @include('rowsys.tracker.tasks.graphs.tabulation_report.table')
    </div>




    <!-- end row -->

    <!-- ==========================CONTENT ENDS HERE ========================== -->


@stop


@section ('local_scripts')

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

    <script>
        var table = $('#data_table');

        //Setup the date for display
        var displayDate = new Date();
        var oldDate = $('#old_search_date').val();
        if(oldDate !== ''){
            displayDate = new Date(oldDate)  ;
        }

        $(document).ready(function () {

            $('.datepicker').datepicker('setDate', displayDate);

            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";

            var responsiveHelper;

            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            table = $('#data_table').DataTable({
                dom: 'Bfrtip',
                buttons: ['copy', 'excel'],
                //"sDom":"flrtip",
                "bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                "bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 100,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                responsive: true,
                // paging: false,
                "bStateSave": true, // saves sort state using localStorage
                autoWidth : false,
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(table, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelper.respond();
                }

            });
            // Apply the filter
            $("#example thead th input[type=text]").on('keyup change', function () {
                table.column($(this).parent().index() + ':visible').search(this.value).draw();
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: [//'excel'//'copyHtml5',//'excelHtml5',//'csvHtml5',//'pdfHtml5'
                ]
            }).container().appendTo($('#buttons'));
            table.buttons().container().appendTo($('.col-sm-6:eq(0)', table.table().container()));

            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = table.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });

            $('#table-section').fadeTo(1500, '1');
            $('#loading-box-section').remove();

            //--------------------------------------------------------
            //      TOGGLE BUTTONS
            //--------------------------------------------------------
            //Add Toggle Buttons to top of table in the #_filter section
            var toggleButtons = $('#toggle_buttons');
            var tableFilterSection = $('#data_table_filter');

            var dtButtons = $('.dt-buttons');
            dtButtons.addClass('hidden-xs hidden-sm hidden-md ');
            //tableFilterSection.append('<div class="">Task List</div>');
            //tableFilterSection.append('&nbsp;&nbsp;').append(toggleButtons);



            //--------------------------------------------------------
            //      EXPORT BUTTONS
            //--------------------------------------------------------
            tableFilterSection.append('&nbsp;&nbsp;').append(
                    '<div class="btn-group hidden-xs hidden-sm"> <button type="button" ' +
                    'class="btn btn-primary dropdown-toggle" ' +
                    'data-toggle="dropdown" aria-haspopup="true" ' +
                    'aria-expanded="false"> Export <span class="caret"></span> </button> <ul class="dropdown-menu"> ' +
                    '<li id="export-buttons" style="padding: 10px;"><a href="#"></a></li> ' +
                        //'<li><a href="#">Excel</a></li> ' +
                        //'<li><a href="#">Something else here</a>' +
                    '</li> </ul> </div>').append('&nbsp;&nbsp;');

            //New Window Button
            tableFilterSection.append('&nbsp;&nbsp;').append('<button class="btn btn-primary hidden-sm hidden-xs" ' +
            'onclick="newReportWindow()"> <i class="fa fa-plus-square-o"></i> New Window</button>');

            var exportButton1 = $('#export-button-1');
            var exportButtons = $('#export-buttons');
            var copyButton = $('.buttons-copy');
            var excelButton = $('.buttons-excel');
            exportButtons.append('&nbsp;&nbsp;').append(dtButtons);
            //exportButton2.append(excelButton);

            var tablePaginateSection = $('#data_table_paginate');
            //tablePaginateSection.css("background-color", "navy");


        });

        function newReportWindow() {

            params = 'width=' + (screen.width - 200);
            params += ', height=' + (screen.height -300);
            params += ', top=0, left=0';
            params += ', fullscreen=yes';

            window.open('/tasks/charts/category-tabulation', 'newwindow', params);



        }

    </script>

@stop
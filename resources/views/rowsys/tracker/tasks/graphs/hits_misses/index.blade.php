@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Chart Selection </h3></div>
                <div class="panel-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <i class="fa fa-check"> </i>{{ Session::get('message') }}
                        </div>
                    @endif

                    <form action="/tasks/charts/progress-overview" method="get">
                        {{ csrf_field() }}

                        <div class="col-md-2">
                            <label class=" control-label">Stage:</label>

                            <div class="form-group">
                                <select class="form-control selectpicker"
                                        id="stage" name="stage"
                                        data-style="btn-primary" title="Stage">
                                    <optgroup label="Stages">
                                        <option value="0">Both</option>
                                        @foreach ($stages as $stage)
                                            @if ($old['stage'] === $stage->id)
                                                <option value="{{$stage->id}}" selected>{{$stage->name}}</option>
                                            @else
                                                <option value="{{$stage->id}}">{{$stage->name}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-2">
                            <div class="form-group">
                                <label class=" control-label">Category Owner:</label>
                                <select class="form-control selectpicker" multiple
                                        id="group" name="group[]"
                                        data-style="btn-primary" title="Category Owner">
                                    <optgroup label="Category Owners">
                                        <option value="0">All</option>
                                        @foreach ($groups as $group)
                                            @if (in_array($group->id,$old['group']))
                                                <option value="{{$group->id}}" selected>{{$group->name}}</option>
                                            @else
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label class=" control-label">Category:</label>

                            <div class="form-group">

                                <select class="form-control selectpicker"
                                        id="type" name="type[]" multiple
                                        data-style="btn-primary" title="Category">
                                    <optgroup label="Categories">
                                        <option value="0"></option>
                                        @foreach ($types as $type)
                                            @if (in_array($type->id,$old['type']))
                                                <option value="{{$type->id}}" selected>{{$type->name}}</option>
                                            @else
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class=" control-label">Team:</label>

                            <div class="form-group">
                                <select class="form-control selectpicker"
                                        id="role" name="role[]" multiple
                                        data-style="btn-primary" title="Teams">
                                    <optgroup label="Teams">
                                        @foreach ($areas as $area)
                                            @if (in_array($area->id,$old['role']))
                                                <option value="{{$area->id}}" selected>{{$area->name}}</option>
                                            @else
                                                <option value="{{$area->id}}">{{$area->name}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class=" col-md-2">
                            <div class="form-group">
                                <label class=" control-label">Before Date:</label>
                                <input type="hidden" id="old_search_date" value="{{$old['date']}}">
                                <div class="input-group date">
                                    <input type="text"  id="to_date" onchange="" name="to_date" class=" datepicker"
                                           data-type="date"  data-date-format="yyyy-mm-dd" value="">
                                </div>
                            </div>
                        </div>

                        <div class=" col-md-2">
                            <label class=" control-label"></label>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block"><i class="fa fa-bar-chart "></i><span class=" hidden-md "> Chart</span></button>
                            </div>
                        </div>
                        @if ($errors->has() && sizeof($errors) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}<br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading"><h3 class="panel-title">Progress Report </h3></div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-3">
                    <div class=" card-box">
                        <h2 class="text-align-center">{{$chart1Data[5]['totalNotDone']}}
                            <small> Open</small>
                        </h2>
                        <p class="text-align-center text-primary"> {{$chart1Data[5]['total']}}
                            <small> total</small>
                        </p>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-box">
                        <h2 class="text-align-center">{{$chart1Data[5]['totalPerc']}} %
                            <small> complete</small>
                        </h2>
                        <p class="text-align-center text-primary">{{$chart1Data[5]['totalDone']}} complete</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-box">
                        <h2 class="text-align-center">  {{$chart1Data[5]['totalNotDone']}}
                            <small> Left To Do</small>
                        </h2>
                        <p class="text-align-center text-primary">{{$chart1Data[5]['started']}} in progress</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-box">
                        <h2 class="text-align-center">{{$chart1Data[5]['pastDue']}}
                            <small> Past Due</small>
                        </h2>
                        <p class="text-align-center text-primary">{{$chart1Data[5]['nonRecoverable']}} Non Recoverable</p>
                    </div>
                </div>
            </div>


            <div class="row">
                <div id="myTabs" class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#chart1" aria-controls="profile" role="tab" data-toggle="tab">Table & Pie Chart</a></li>
                        <li><a href="#chart2" aria-controls="home" role="tab" data-toggle="tab">Weekly Bar Chart</a></li>
                        <li class="hide"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div id="myTabContent" class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="chart1">
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <table class="table table-bordered table-responsive">
                                            <thead>
                                            <tr style="background-color: lightcyan">
                                                <th>Step</th>
                                                <th>Count</th>
                                                <th>% Break Down</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Complete</td>
                                                <td><span class=" " id=""> {{$chart1Data[5]['totalDone']}}  </span></td>
                                                <td><span class="label  font-sm" id="" style="background-color: #00CC00">{{$chart1Data[5]['totalPerc']}} %</span></td>
                                            </tr>
                                            <tr>
                                                <td>Not Started</td>
                                                <td><span class=" " id=""> {{$chart1Data[5]['notStarted']}}  </span></td>
                                                <td><span class="label  font-sm" id="" style="background-color: #A0A0A0">{{$chart1Data[5]['notStartedPerc']}} %</span></td>
                                            </tr>
                                            <tr>
                                                <td>Prep / Exec</td>
                                                <td><span class=" " id="">{{$chart1Data[5]['inStep1']}}  </span></td>
                                                <td><span class="label font-sm" id="" style="background-color: #FFD700;color: #000000">{{$chart1Data[5]['step1Perc']}} %</span></td>

                                            </tr>
                                            <tr>
                                                <td>Review</td>
                                                <td><span class=" " id="">{{$chart1Data[5]['inStep2']}}  </span></td>
                                                <td><span class="label font-sm" id="" style="background-color: #4169E1">{{$chart1Data[5]['step2Perc']}} %</span></td>

                                            </tr>
                                            <tr>
                                                <td>Update</td>
                                                <td><span class=" " id="">{{$chart1Data[5]['inStep3']}}  </span></td>
                                                <td><span class="label  font-sm" id="" style="background-color: #FFFF99;color: #000000;">{{$chart1Data[5]['step3Perc']}} %</span></td>

                                            </tr>
                                            <tr>
                                                <td>Sign Off</td>
                                                <td><span class=" " id="">{{$chart1Data[5]['inStep4']}}  </span></td>
                                                <td><span class="label  font-sm" id="" style="background-color: #F4A460">{{$chart1Data[5]['step4Perc']}} %</span></td>
                                            </tr>
                                            </tbody>

                                        </table>
                                    </div>

                                    <div class="col-md-6 ">
                                        <div class="panel panel-info">
                                            <div class="panel-body">
                                                <div style="height: 100%">
                                                    <canvas id="chart-2"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($chart1Data[5]['noHoursAssignedComplete'] > 0 || $chart1Data[5]['noHoursAssignedNotComplete'] > 0)
                                        <div class="col-md-6 hide">
                                            <div class="alert alert-warning" role="alert">
                                                <h2><i class="fa  fa-exclamation-triangle"></i> Seems there are tasks with no hours</h2>
                                            </div>


                                            <table class="table table-bordered table-responsive">
                                                <tbody>
                                                <tr>
                                                    <td>Assigned hours and Complete</td>
                                                    <td><span class=" " id=""> {{$chart1Data[5]['completeWithHours']}}  </span></td>
                                                </tr>
                                                <tr>
                                                    <td>Assigned hours and NOT Complete</td>
                                                    <td><span class=" " id=""> {{$chart1Data[5]['notCompleteWithHours']}}  </span></td>
                                                </tr>
                                                <tr>
                                                    <td>No Assigned Hours and and Complete</td>
                                                    <td><span class=" " id=""> {{$chart1Data[5]['noHoursAssignedComplete']}}  </span></td>
                                                </tr>
                                                <tr>
                                                    <td>No Assigned Hours and NOT Complete</td>
                                                    <td><span class=" " id=""> {{$chart1Data[5]['noHoursAssignedNotComplete']}}  </span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade " id="chart2">
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div style="height: 100%">
                                            <canvas id="chart-1"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div role="tabpanel" class="tab-pane fade" id="messages">
                            <h3>Tab 3</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row hide">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">ChaRT</h3></div>
                <button type="button" onclick="rebuildChart1()" class=" hide btn btn-primary">Rebuild</button>
                <div class="panel-body">

                </div>
            </div>
        </div>
        <div class="col-md-6 hide">
            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">ChaRT</h3></div>
                <div class="panel-body">
                    <div style="height: 200%">
                        <canvas id="chart-2"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide">
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">ChaRT</h3></div>
                <div class="panel-body">
                    <div style="width: 100%">
                        <canvas id="chart-3"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!-- end row -->

    <!-- ==========================CONTENT ENDS HERE ========================== -->


@stop


@section ('local_scripts')
    <!-- Morris Chart Dependencies -->
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/js/plugin/chartjs/Chart.bundle.min.js"></script>

    <script>
        var serverChart1Data = <?php echo json_encode($chart1Data );?>;
        console.log(serverChart1Data);

        var chart1 = document.getElementById("chart-1").getContext("2d");
        var chart2 = document.getElementById("chart-2").getContext("2d");
        var chart3 = document.getElementById("chart-3").getContext("2d");


        //Setup the date for display
        var displayDate = new Date();
        var oldDate = $('#old_search_date').val();
        if(oldDate !== ''){
            displayDate = new Date(oldDate)  ;
        }

        function rebuildChart1() {
            hitsMissesChart.update();
        }
        $(document).ready(function () {

            $('.datepicker').datepicker('setDate', displayDate);
            /* ------------------------------------------
             CHART 1 CHART
             ---------------------------------------------*/

            window.myBar = new Chart(chart1, {
                type: 'bar',
                data: chart1Data,
                options: {
                    title: {
                        display: true,
                        text: "Progress Overview"
                    },
                    legend: {
                        position: 'top'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            //stacked: true,
                        }],
                        yAxes: [{
                            //stacked: true
                        }]
                    }
                }
            });


            /* ------------------------------------------
             CHART 2 CHART
             ---------------------------------------------*/
            var chart2Config = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            {{$chart1Data[5]['totalPerc']}},
                            {{$chart1Data[5]['notStartedPerc']}},
                            {{$chart1Data[5]['step1Perc']}},
                            {{$chart1Data[5]['step2Perc']}},
                            {{$chart1Data[5]['step3Perc']}},
                            {{$chart1Data[5]['step4Perc']}}

                        ],
                        backgroundColor: [
                            "#00CC00",
                            "#A0A0A0",
                            "#FFD700",
                            "#4169E1",
                            "#FFFF99",
                            "#F4A460",

                        ],
                    }],
                    labels: [
                        "Complete",
                        "Not Started",
                        "Prep / Exec",
                        "Review",
                        "Re-Issue",
                        "Sign Off"
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true
                    },
                    tooltips: {
                        enabled: true,
                    }
                }
            };
            window.myBar = new Chart(chart2, chart2Config);
            /* ------------------------------------------
             CHART 3 CHART
             ---------------------------------------------*/

            /*            window.myBar = new Chart(chart3, {
             type: 'bar',
             data: chart3Data,
             options: {
             title: {
             display: true,
             text: "Complete %"
             },
             legend: {
             position: 'bottom'
             },
             responsive: true,
             scales: {
             xAxes: [{
             //stacked: true,
             }],
             yAxes: [{
             ticks: {
             min: 0,
             max: 100,
             stepSize: 10
             }
             //stacked: true
             }]
             }
             }
             });*/

        });


        /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         CHART  DATA
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
        /* ------------------------------------------
         CHART 1 DATA
         ---------------------------------------------*/
        var chart1Data = {
            labels: serverChart1Data[0],
            datasets: [{
                type: 'bar',
                label: 'Target ',
                backgroundColor: "rgba(30,144,255,0.9)",
                data: serverChart1Data[1],
            }, {
                type: 'bar',
                label: 'Complete',
                backgroundColor: "rgba(60,179,113,0.9)",
                data: serverChart1Data[2],
                fill: false,
                pointStyle: 'square',
                // borderDash: [5, 5],
            }, /* {
             type: 'line',
             label: 'Roster',
             backgroundColor: "rgba(0,0,0,1.0)",
             data: serverChart1Data[3],
             borderColor: 'black',
             borderWidth: 1,
             pointStyle: 'square',
             borderDash: [5, 5],
             borderDashOffset: 0.5,
             fill: false,
             }, {
             type: 'line',
             label: 'Total Spend',
             backgroundColor: "rgba(0,128,0,1.0)",
             data: serverChart1Data[4],
             borderColor: 'orange',
             borderWidth: 1,
             fill: false,
             }, {
             type: 'line',
             label: 'Total Earned',
             backgroundColor: "rgba(151,187,205,1.0)",
             data: [0, 10, 35, 50],
             borderColor: 'blue',
             borderWidth: 1,
             fill: false,
             }*/
            ]
        };

        /* ------------------------------------------
         CHART 2 DATA
         ---------------------------------------------*/

        /* ------------------------------------------
         CHART 3 DATA
         ---------------------------------------------*/
        var chart3Data = {
            labels: serverChart1Data[0],
            datasets: [{
                type: 'bar',
                label: 'Prep',
                backgroundColor: "rgba(30,144,255,0.7)",
                data: serverChart1Data[5],
            }, {
                type: 'bar',
                label: 'Exec',
                backgroundColor: "rgba(60,179,113,0.7)",
                data: serverChart1Data[6],
                fill: false,
                pointStyle: 'square',
                //borderDash: 0.5
            }, /* {
             type: 'line',
             label: 'Roster',
             backgroundColor: "rgba(0,0,0,1.0)",
             data: serverChart1Data[3],
             borderColor: 'black',
             borderWidth: 1,
             pointStyle: 'square',
             borderDash: [5, 5],
             borderDashOffset: 0.5,
             fill: false,
             }, {
             type: 'line',
             label: 'Total Spend',
             backgroundColor: "rgba(0,128,0,1.0)",
             data: serverChart1Data[4],
             borderColor: 'orange',
             borderWidth: 1,
             fill: false,
             },{
             type: 'line',
             label: 'Total Earned',
             backgroundColor: "rgba(151,187,205,1.0)",
             data: [0, 10, 35, 50],
             borderColor: 'blue',
             borderWidth: 1,
             fill: false,
             }*/
            ]
        };


    </script>




@stop
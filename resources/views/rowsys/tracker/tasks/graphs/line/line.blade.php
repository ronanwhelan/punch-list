@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')

            <!-- ==========================CONTENT STARTS HERE ========================== -->
            <!-- widget grid -->
            <section id="widget-grid" class="">
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12 well">
                            <div class="col-sm-12">
                                <div class="highchart-container"></div>
                            </div>
                            <div class="col-sm-2">
                                <table class="highchart table table-hover table-bordered hide"
                                       data-graph-container=".. .. .highchart-container"
                                       data-graph-type="line"
                                        data-graph-line-width="6">

                                    <caption>Preparation Chart</caption>
                                    <thead>
                                    <tr>
                                        <th>Month</th>
                                        <th>Prep Actual</th>
                                        <th>Prep Target</th>
                                        <th>Prep Project</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>January</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>February</td>
                                        <td>12</td>
                                        <td>15</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>March</td>
                                        <td>24</td>
                                        <td>27</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>April</td>
                                        <td>35</td>
                                        <td>41</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>May</td>
                                        <td>67</td>
                                        <td>62</td>
                                        <td>67</td>
                                    </tr>
                                    <tr>
                                        <td>June</td>
                                        <td></td>
                                        <td>47</td>
                                        <td>75</td>
                                    </tr>
                                    <tr>
                                        <td>July</td>
                                        <td></td>
                                        <td>87</td>
                                        <td>83</td>
                                    </tr>
                                    <tr>
                                        <td>Aug</td>
                                        <td></td>
                                        <td>98</td>
                                        <td>91</td>
                                    </tr>
                                    <tr>
                                        <td>Sep</td>
                                        <td></td>
                                        <td>100</td>
                                        <td>99</td>
                                    </tr>
                                    <tr>
                                        <td>Oct</td>
                                        <td></td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    <tr>
                                        <td>Nov</td>
                                        <td></td>
                                        <td>100</td>
                                        <td>100</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="col-sm-12 well">
                            <div class="col-sm-6">
                                <table class="highchart table table-hover table-bordered" data-graph-container=".. .. .highchart-container2" data-graph-type="column">
                                    <caption>Column example</caption>
                                    <thead>
                                    <tr>
                                        <th>Month</th>
                                        <th class="">Sales</th>
                                        <th class="">Benefits</th>
                                        <th class="">Expenses</th>
                                        <th class="">Prediction</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>January</td>
                                        <td class="">8000</td>
                                        <td class="">2000</td>
                                        <td class="">1000</td>
                                        <td class="">9000</td>
                                    </tr>
                                    <tr>
                                        <td>February</td>
                                        <td class="">12000</td>
                                        <td class="">3000</td>
                                        <td class="">1300</td>
                                        <td class="">10000</td>
                                    </tr>
                                    <tr>
                                        <td>March</td>
                                        <td class="">18000</td>
                                        <td class="">4000</td>
                                        <td class="">1240</td>
                                        <td class="">11000</td>
                                    </tr>
                                    <tr>
                                        <td>April</td>
                                        <td class="">2000</td>
                                        <td class="">-1000</td>
                                        <td class="">-150</td>
                                        <td class="">13000</td>
                                    </tr>
                                    <tr>
                                        <td>May</td>
                                        <td class="">500</td>
                                        <td class="">-2500</td>
                                        <td class="">1000</td>
                                        <td class="">14000</td>
                                    </tr>
                                    <tr>
                                        <td>June</td>
                                        <td class="">600</td>
                                        <td class="">-500</td>
                                        <td class="">-500</td>
                                        <td class="">15000</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-6">
                                <div class="highchart-container2"></div>
                            </div>
                        </div>



                        <div class="col-sm-12 well">
                            <div class="col-sm-6">
                                <table class="highchart table table-hover table-bordered" data-graph-container=".. .. .highchart-container3" data-graph-type="column">
                                    <caption>Column + area example</caption>
                                    <thead>
                                    <tr>
                                        <th>Month</th>
                                        <th>Sales</th>
                                        <th data-graph-type="area">Benefits</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>January</td>
                                        <td>8000</td>
                                        <td>2000</td>
                                    </tr>
                                    <tr>
                                        <td>February</td>
                                        <td>12000</td>
                                        <td>3000</td>
                                    </tr>
                                    <tr>
                                        <td>March</td>
                                        <td>18000</td>
                                        <td>4000</td>
                                    </tr>
                                    <tr>
                                        <td>April</td>
                                        <td>2000</td>
                                        <td>-1000</td>
                                    </tr>
                                    <tr>
                                        <td>May</td>
                                        <td>500</td>
                                        <td>-2500</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-6 ">
                                <div class="highchart-container3"></div>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->
                </div>
                <!-- end row -->
            </section>
            <!-- end widget grid -->


@stop


@section ('local_scripts')
    <script type="text/javascript">

        $(document).ready(function() {
            $('table.highchart').highchartTable();
        })
    </script>

    <script src="/js/plugin/highChartCore/highcharts-custom.min.js"></script>
    <script src="/js/plugin/highchartTable/jquery.highchartTable.min.js"></script>

@stop

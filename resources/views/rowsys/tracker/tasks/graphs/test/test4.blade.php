@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="panel panel-info hide">
        <div class="panel-body">
            <div class="col-md-12">
                <label class="control-label">Responsible Team</label>
                <select class="form-control selectpicker" onchange="getAreaBarAndLineGraphData(this.value)" name="area" id="area" data-style="btn-primary">
                    <option value="0"></option>
                    @foreach($areas as $area)
                        <option value="{{$area->id}}"> {{$area->name}}</option>
                    @endforeach
                </select>
            </div>
            <br>

            <div class="col-md-12"></div>
        </div>
    </div>

    <title>Combo Bar-Line Chart</title>

    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title">My Chart 1</h3></div>
            <div class="panel-body">

                <div class="row">
                    <div id="myTabs" class="col-lg-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Graph</a></li>
                            <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Setup</a></li>
                            <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Table</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home">

                                <div style="width: 100%">
                                    <canvas id="chart-1"></canvas>
                                </div>
                                <br>
                                <button onclick="updateChart1()" class="btn btn-primary btn-sm">Update</button>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="profile">
                                <div class="panel">
                                    <div class="panel-body">
                                        <form>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Title</label>
                                                    <input type="text" class="form-control" id="chart-1-title" name="chart_title" placeholder="title">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Type</label>
                                                    <input type="text" class="form-control" id="chart-1-type" name="chart_type" placeholder="title">
                                                </div>
                                            </div>
                                            <hr>

                                            <div class="col-md-12">
                                                <h3>Data Sets</h3>

                                                <div class="form-group">
                                                    <label for="">Type</label>
                                                    <input type="text" class="form-control" id="chart-1-type" name="chart_type" value="line">
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"> Stacked
                                                    </label>
                                                </div>

                                                <button type="submit" class="btn btn-default">Submit</button>

                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="messages">
                                <h3>Tab 3</h3>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus
                                    consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id,
                                    rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Panel title</h3></div>
            <div class="panel-body">
                <div style="width: 100%">
                    <canvas id="chart-2"></canvas>
                </div>
                <br>
                <button onclick="updateChart2()" class="btn btn-primary btn-sm">Update</button>
            </div>
        </div>
    </div>

    <!-- end row -->

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')
    <!-- Morris Chart Dependencies -->
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/js/plugin/chartjs/Chart.bundle.js"></script>


    <script>
        /* ------------------------------------------
         RANDOM DATA
         ---------------------------------------------*/
        var randomScalingFactor = function () {
            return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
        };
        var randomColorFactor = function () {
            return Math.round(Math.random() * 255);
        };
        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        var randomColor = function (opacity) {
            return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
        };
        function randomizeData(chartData) {
            if (chartData === 'chart1') {
                $.each(chart1Data.datasets, function (i, dataset) {
                    dataset.backgroundColor = 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',.7)';
                    dataset.data = [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()];

                });
            } else {
                $.each(chart2Data.datasets, function (i, dataset) {
                    dataset.backgroundColor = 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',.7)';
                    dataset.data = [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()];

                });
            }

        }
        $('#randomizeData').click(function () {
            $.each(chart1Data.datasets, function (i, dataset) {
                dataset.backgroundColor = 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',.7)';
                dataset.data = [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()];

            });
            //myLineChart.update();
        });

    </script>

    <script>

        /* ------------------------------------------
         CHART 1
         ---------------------------------------------*/
        //Setup.
        var chart1Labels = ["January", "February", "March", "April", "May", "June", "July"];
        var chart1Type = 'line';
        var chart1Title = 'Chart 1 Title';
        var chart1StackedChoice = false;
        var chart1Div = document.getElementById("chart-1").getContext("2d");
        var chart1DataSets = [{
            label: 'Dataset 1',
            backgroundColor: "rgba(220,220,220,0.5)",
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        }, {
            label: 'Dataset 2',
            backgroundColor: "rgba(151,187,205,0.5)",
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        }, {
            label: 'Dataset 3',
            backgroundColor: "rgba(151,187,205,0.5)",
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        }];

        var chart1Data = {
            labels: chart1Labels,
            datasets: chart1DataSets
        };
        var chart1Config = {
            type: chart1Type,
            data: chart1Data,
            options: {
                title: {
                    display: true,
                    text: chart1Title
                },
                tooltips: {
                    mode: 'label'
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: chart1StackedChoice,
                    }],
                    yAxes: [{
                        stacked: chart1StackedChoice
                    }]
                }
            }
        };
        var chart1 = new Chart(chart1Div, chart1Config);
        function getChart1NewData() {
            console.log('getChart1NewData');
            $.ajax({
                type: "GET",
                url: "/tasks/charts/user-chart-1-data", success: function (data) {
                    //console.log(data[0]['label']);
                    chart1Data.datasets = [];
                    $.each(data, function (i, data) {
                        console.log(data.label);
                        var background = randomColor(0.5);
                        var newDataset = {
                            label: data.label,
                            borderColor: background,
                            backgroundColor: background,
                            pointBorderColor: background,
                            pointBackgroundColor: background,
                            pointBorderWidth: 1,
                            fill: false,
                            data: data.data,
                        };
                        chart1Data.datasets.push(newDataset);
                        console.log(chart1Data.datasets);

                    });
                    chart1.update();
                }
            });
        }
        function updateChart1() {
            getChart1NewData();

        }


        /* ------------------------------------------
         CHART 2
         ---------------------------------------------*/
        //Setup.
        var chart2Labels = ["January", "February", "March", "April", "May", "June", "July"];
        var chart2Type = 'bar';
        var chart2Title = 'Chart 1 Title';
        var chart2StackedChoice = false;
        var chart2Div = document.getElementById("chart-2").getContext("2d");
        var chart2Data = {
            labels: chart2Labels,
            datasets: [{
                label: 'Dataset 1',
                backgroundColor: "rgba(220,220,220,0.5)",
                data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
            }, {
                label: 'Dataset 2',
                backgroundColor: "rgba(151,187,205,0.5)",
                data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
            }, {
                label: 'Dataset 3',
                backgroundColor: "rgba(151,187,205,0.5)",
                data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
            }]

        };
        var chart2Config = {
            type: chart2Type,
            data: chart2Data,
            options: {
                title: {
                    display: true,
                    text: chart2Title
                },
                tooltips: {
                    mode: 'label'
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: chart2StackedChoice,
                    }],
                    yAxes: [{
                        stacked: chart2StackedChoice
                    }]
                }
            }
        };
        var chart2 = new Chart(chart2Div, chart2Config);
        function getChart2NewData() {
            console.log('getChart1NewData');
            $.ajax({
                type: "GET",
                url: "/tasks/charts/my-chart-4-data", success: function (data) {
                    console.log(data);
                }
            });
        }
        function updateChart2() {
            randomizeData('chart2');
            chart2.update();
        }
    </script>



@stop

@if ($errors->has() && old('systemFormActive') === '1' )
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}<br>
        @endforeach
    </div>
@endif
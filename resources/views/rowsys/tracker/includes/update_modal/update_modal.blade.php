<!-- Modal -->
<div class="modal fade" id="update-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">CLOSE&times;</span></button>
                <h5 class="modal-title" id="myModalLabel">Update Form</h5>
            </div>
            <div class="modal-body">

                @include('rowsys.tracker.includes.errors.errors')
                <div id="model-details">
                    {{-- Model details are loaded by AJAX--}}
                    @include('rowsys._app.includes.loading.spinner.spinner_simple')
                </div>
                @include('rowsys.tracker.includes.messages.forms.session_message')


                <div class=" hide alert alert-danger" id="validation-errors">
                </div>
            </div>
        </div>
    </div>
</div>
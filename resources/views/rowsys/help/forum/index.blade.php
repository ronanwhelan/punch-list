@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')
<div class="container">
    <div class="row animated fadeIn">
        <div class="col-lg-12">
            <h2 class="pull-left">Feedback Forum</h2>
            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">Add a topic <i class="fa fa-comment-o"></i></button>
        </div>
    </div>  
    
     <div class="row animated fadeIn">
        <div class="col-lg-12">
            <p class="lead"><strong class="text-success">Join the forum</strong> Use this Forum to discuss any ideas, critics, feature requests and all the ideas regarding our web application. Please check the current posts before posting to prevent duplicate posts.</p>

            <p>If you need to contact the team directly email us at <a href="mailto:#">rwhelan@pes-international.com</a></p>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <strong>Great!</strong>  {{ Session::get('message') }}
                </div>
            @endif
                <div class="card-box pad-20">
                    <table class="table forum table-striped table-responsive">
                        <thead>
                        <tr>
                            <th class="cell-stat"></th>
                            <th>
                                <h3>Bug <i class="fa fa-bug text-danger"></i></h3>
                            </th>
                            <th class="cell-stat text-center hidden-xs">Status</th>
                            <th class="cell-stat-2x hidden-xs ">Added</th>
                        </tr>
                        </thead>
                        <tbody>
                         {{--  Home Contoller controls this page for now--}}
                        @foreach($bugs as $bug)
                            <tr>
                                <td class="text-center"></td>
                                <td>
                                    <h4><a href="#">{{$bug->topic}}</a><br>
                                        <small>{{$bug->description}}</small>
                                    </h4>
                                    <h6><small>Area:</small> {{$bug->area}}</h6>
                                    <h6><small>Device:</small> {{$bug->device}}</h6>
                                    <h6><small>Platform:</small> {{$bug->platform}}</h6>
                                </td>
                                <td class="text-center hidden-xs"><a href="#"><span class="label label-danger">Open</span></a></td>

                                <td class="hidden-xs">by <a href="#">{{$bug->user}}</a><br>
                                    <small><i class="fa fa-clock-o"></i> {{$bug->created_at}}</small>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <th class="cell-stat"></th>
                            <th>
                                <h3 class="pad-20">Suggestions <i class="fa fa-lightbulb-o text-success"></i></h3>
                            </th>
                            <td colspan="4"></td>
                        </tr>

                        @foreach($suggestions as $suggestion)
                        <tr>
                            <td class="text-center"></td>
                            <td>
                                <h4><a href="#">{{$suggestion->topic}}</a><br>
                                    <small>{{$suggestion->description}}</small>
                                </h4>
                                <h6><small>Area:</small> {{$suggestion->area}}</h6>
                                <h6><small>Device:</small> {{$suggestion->device}}</h6>
                                <h6><small>Platform:</small> {{$suggestion->platform}}</h6>
                            </td>
                            <td class="text-center hidden-xs"><a href="#"><span class="label label-warning">Open</span></a></td>
                            <td class="hidden-xs">by <a href="#">{{$suggestion->user}}</a><br>
                                <small><i class="fa fa-clock-o"></i> {{$suggestion->created_at}}</small>
                            </td>
                        </tr>
                            @endforeach
                        <tr>
                            <th class="cell-stat"></th>
                            <th>
                                <h3 class="pad-20">Closed/Implemented <i class="fa fa-check text-success"></i></h3>
                            </th>
                            <td colspan="4"></td>
                        </tr>
                        @foreach($completeList as $complete)
                            <tr class="text-success">
                                <td class="text-center"></td>
                                <td>
                                    <h4><a href="#">{{$complete->topic}}</a><br>
                                        <small>{{$complete->description}}</small>
                                    </h4>
                                    <h6><small>Area:</small> {{$complete->area}}</h6>
                                    <h6><small>Device:</small> {{$complete->device}}</h6>
                                    <h6><small>Platform:</small> {{$complete->platform}}</h6>
                                </td>
                                <td class="text-center hidden-xs"><a href="#"><span class="label label-success">Closed <i class="fa fa-check text-success"></i></span></a></td>
                                <td class="hidden-xs">by <a href="#">{{$complete->user}}</a><br>
                                    <small><i class="fa fa-clock-o"></i> {{$complete->created_at}}</small>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Topic</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="/forum/add" method="get">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Heading</label>
                            <div class="col-sm-10">
                                <input type="text" name="heading" class="form-control" id="" placeholder="Give the topic a heading">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" rows="5" id="comment" placeholder="Describe the topic,what you were doing, what you would like etc.."></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Area</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="area" id="" placeholder="e.g task table, dashboard, graph etc..">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Type</label>
                            <div class="col-sm-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsType" id="type2" value="suggestion" checked>
                                        Suggestion/Idea
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsType" id="type1" value="bug" >
                                        Bug
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Device</label>

                            <div class="col-sm-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsDevice" id="device1" value="pc" checked>
                                        PC
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsDevice" id="devic2" value="tablet">
                                        Tablet
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsDevice" id="device3" value="phone">
                                        Phone
                                    </label>
                                </div>
                                <br>
                                <input type="text" name="brand" class="form-control" id="" placeholder="Brand and Platform">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-6">
                                <div class="checkbox" name="status">
                                    <label>
                                        <input type="checkbox" value="">
                                        Closed
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info pull-right">Add Topic</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop

@section ('local_scripts')

@stop

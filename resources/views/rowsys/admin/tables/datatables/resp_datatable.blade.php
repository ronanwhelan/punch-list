<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DataTables Example</title>



    <!-- CSS -->
    <link rel="stylesheet" href="/css/reset.css">
    <!-- CSS reset -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">

    <link rel="stylesheet" href="/css/style.css">
    <!-- Resource style -->
    <link rel="stylesheet" href="/css/vendor/datatables/datatables.responsive.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/plug-ins/e9421181788/integration/bootstrap/3/dataTables.bootstrap.css"/>
    <script src="/js/modernizr.js"></script>
    <!-- Modernizr -->
    <style>
        .title {
            font-size: larger;
            font-weight: bold;
        }

        .table th.centered-cell, .table td.centered-cell {
            text-align: center;
        }
    </style>
</head>
<body>

<header class="main-header">
    <a href="/welcome" class="logo">RowSys.co</a>


    <div class="search is-hidden">
        <form action="#0">
            <input type="search" placeholder="Search...">
        </form>
    </div>
    <!-- search -->

    <a href="#0" class="nav-trigger">Menu<span></span></a>

    <nav class="nav">
        <ul class="top-nav">
            <li class="has-children account">
                <a href="#0">
                    <img src="/img/avatar.png" alt="avatar">
                    @if (Auth::check())
                        {{ Auth::user()->name }}
                    @endif
                </a>
                <ul>
                    <li><a href="#0">My Account</a></li>
                    <li><a href="#0">Edit Account</a></li>
                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</header>
<!-- .main-header -->

<nav class="side-nav">
    <ul>
        <li class="active">
            <a href="/dashboard/Admin">Dashboard</a>
        </li>

        <li class="has-children notifications">
            <a href="#0">Tools</a>
            <ul>
                <li><a href="/queue">Queues</a></li>
                <li><a href="/console">Console</a></li>
                <li><a href="/storage">Storage</a></li>
                <li><a href="/mail">Send e-mail</a></li>
                <li><a href="/Import">Import From File</a></li>
                <li><a href="/storage">Export To File</a></li>

                <li>
                    <a href="#0">Database</a>
                    <ul>
                        <li><a href="storage">Backup</a></li>
                    </ul>

                </li>
            </ul>
        </li>

        <li class="has-children notifications">
            <a href="#0">Server</a>
            <ul>
                <li><a href="console">Choose Server</a></li>
                <li><a href="queue">Stats</a></li>
                <li><a href="console">Logs</a></li>
            </ul>
        </li>
        <li class="has-children notifications">
            <a href="#0">Tables</a>
            <ul>
                <li><a href="/datatable/smart">Smart Data Table</a></li>
                <li><a href="/datatable/site">Site Table</a></li>
                <li><a href="/datatable/repsonsive">Responsive</a></li>
            </ul>
        </li>
        <li class="has-children notifications">
            <a href="#0">Graphs</a>
            <ul>
                <li><a href="queue">Area</a></li>
                <li><a href="queue">Line</a></li>

            </ul>
        </li>
    </ul>
</nav>

<section class="main-content">
    <div class="content-wrapper">
        <br><br>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            <br>

            <div class="form-inline">
                <table id="example"
                       class="table table-striped table-hover table-bordered dataTable no-footer has-columns-hidden"
                       style="font-size: 11px;" cellspacing="0" width="100%">
                    <thead>
                    <tr class="font-xs">
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=" "/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=" "/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                        <th class=""><input type="text" class="" placeholder=""/></th>
                    </tr>
                    <tr>
                        <th data-class="expand" style="width: 13%" class="font-xs">Number</th>
                        <th data-hide="phone,tablet,computer">Area</th>
                        <th data-hide="phone,tablet,computer">System</th>
                        <th data-hide="phone,tablet,computer">System Description</th>
                        <th data-hide="phone,tablet,computer">Group</th>
                        <th data-hide="phone,tablet">Task Type</th>
                        <th data-hide="phone,tablet">Stage</th>
                        <th data-hide="phone,tablet">Next<br>Target<br>Date</th>
                        <th data-hide="" class="font-xs"><span class="txt-color-orange">Gen/Ex<br>Status %</span></th>
                        <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-orange">Gen/Ex<br>Target<br>Date</span>
                        </th>
                        <th data-hide="" class="font-xs"><span class="txt-color-blue">Review<br>Status %</span></th>
                        <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-blue">Review<br>Target<br>Date</span>
                        </th>
                        <th data-hide="" class="font-xs"><span class="txt-color-orange">ReIssue<br>Status %</span></th>
                        <th data-hide="phone,tablet" class="font-xs"><span
                                    class="txt-color-orange">ReIssue<br>Target<br>Date</span></th>
                        <th data-hide="" class="font-xs"><span class="txt-color-blue">SignOff<br>Status %</span></th>
                        <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-blue">SignOff<br>Target<br>Date</span>
                        </th>
                        <th data-hide="phone,tablet" class="font-xs">Target<br>Val (hrs)</th>
                        <th data-hide="phone,tablet" class="font-xs">Earned<br>Val (hrs)</th>
                        <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Date</th>
                        <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Dev Days</th>


                    </tr>
                    </thead>

                    <tfoot>

                    </tfoot>

                    <tbody>
                    <tr>
                        <td>Tiger Nixon</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                        <td>$320,800</td>

                    </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>

</section>
<!-- .main-content -->
<script src="https://code.jquery.com/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
<script src="/js/vendor/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="/js/vendor/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="/js/vendor/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="/js/vendor/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="/js/vendor/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script>
    'use strict';

    $(document).ready(function () {
        var responsiveHelper = undefined;
        var breakpointDefinition = {
            computer: 2000,
            tablet: 1024,
            phone: 480
        };
        // var tableElement = $('#example');

        var tableElement = $('#example').DataTable({
            "paging": true,
            //"bStateSave": true // saves sort state using localStorage

            "autoWidth": true,
            preDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper($('#example'), breakpointDefinition);
                }
            },
            rowCallback: function (nRow) {
                responsiveHelper.createExpandIcon(nRow);
            },
            drawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });
        // Apply the filter
        $("#example thead th input[type=text]").on('keyup change', function () {
            tableElement
                    .column($(this).parent().index() + ':visible')
                    .search(this.value)
                    .draw();
        });
    });
</script>
</body>
</html>
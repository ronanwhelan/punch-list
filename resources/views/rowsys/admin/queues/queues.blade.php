@extends ('rowsys._app.layouts.app_admin')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')    

    <div class="row animated fadeIn">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Queues</h3>
                </div>
                <div class="panel-body">
                    <h3> There are '{{$numOfJobs}}' Jobs in the Queue</h3>

                    <br>
                    <div class="alert alert-info" role="alert"> Dont forget to Start the <strong>Queue listener!</strong>
                        The
                        command must be run on the server (or directly on homestead)
                    </div>
                    <br>
                    <a class="btn btn-success" href="/compile-logs" role="button">Compile Logs &nbsp;&nbsp;  <i class="fa fa-refresh"></i></a>
                    <br><br>

                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <strong>Info!</strong>  {{ Session::get('message') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Logs</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div id="myTabs" class="col-lg-12">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="#compile-all" aria-controls="compile-all" role="tab" data-toggle="tab">Full Compile</a></li>
                                <li><a href="#compile-sub" aria-controls="compile-sub" role="tab" data-toggle="tab">Sub Compile</a></li>
                                <li><a href="#rules-update" aria-controls="rules-update" role="tab" data-toggle="tab">Rules Updated</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div id ="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="compile-all">
                                    <br>
                                    <h4>list of the Full Task Compiles that happened in the last month</h4>
                                    <div class=" custom-scroll  " style="height:200px; overflow-y: scroll;">
                                        <table class="table table-responsive" style="text-align: left;font-size: 12px">
                                            <thead>
                                            <tr>
                                                <th>Description</th>
                                                <th>User</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($jobsCompileFull as $log)
                                                <tr>
                                                    <td>{{$log->description}}</td>
                                                    <td> {{$log->user->name}}</td>
                                                    <td>{{$log->started_at}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="compile-sub">
                                    <br>
                                    <h4>A list of the Task Sub Compiles that happened in the last week</h4>
                                    <div class=" custom-scroll  " style="height:200px; overflow-y: scroll;">
                                        <table class="table table-responsive" style="text-align: left;font-size: 12px">
                                            <thead>
                                            <tr>
                                                <th>Description</th>
                                                <th>User</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($jobsCompileSub as $log)
                                                <tr>
                                                    <td>{{$log->description}}</td>
                                                    <td> {{$log->user->name}}</td>
                                                    <td>{{$log->started_at}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="rules-update">
                                    <br>
                                    <h4>list of the Task Rules Updated in the last month</h4>
                                    <div class=" custom-scroll  " style="height:200px; overflow-y: scroll;">
                                        <table class="table table-responsive" style="text-align: left;font-size: 12px">
                                            <thead>
                                            <tr>
                                                <th>Description</th>
                                                <th>User</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($jobsRulesUpdated as $log)
                                                <tr>
                                                    <td>{{$log->description}}</td>
                                                    <td> {{$log->user->name}}</td>
                                                    <td>{{$log->started_at}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>

    <div class="row animated fadeIn">
        <div id="myTabs" class="col-lg-12">
            <h3>Example of a tab system</h3>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Job In The Q</a></li>
                <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
            </ul>

            <!-- Tab panes -->
            <div id ="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="home">
                    @if(!$jobs->isEmpty())
                        <h2>{{$numOfJobs}} Jobs in the Queue</h2>

                        <div class="col-xs-12">
                            <div class=" custom-scroll  " style="height:400px; overflow-y: scroll;">
                                <table class="table table-responsive" style="text-align: left;font-size: 12px">
                                <thead>
                                <tr>
                                    <th>Queue</th>
                                    <th>Payload</th>
                                    <th>Attempts</th>
                                    <th>Reserved</th>
                                    <th>Available At</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $jobs as $job)
                                    <tr class="">
                                        <td>{{$job->queue}}</td>
                                        <td>{{$job->payload}}</td>
                                        <td>{{$job->attempts}}</td>
                                        <td>{{$job->attempts}}</td>
                                        <td>{{$job->available_at->toDateTimeString()}}</td>
                                        <td>{{$job->created_at->toDateTimeString()}}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                                <br>
                                <h2 class="text-uppercase text-success"> No Jobs in the Queue</h2>
                            @endif
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="profile">
                            <h3>Tab 2</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="messages">
                            <h3>Tab 3</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                        </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section ('local_scripts')

@stop
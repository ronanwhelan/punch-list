@extends ('rowsys._app.layouts.app_admin')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')
    <br>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="well ">
                    <h4 class="text-primary">Upload A File to Local Server Drive Or Remote Storage (AWS S3)</h4>
                    <br>

                    <form action="file/upload" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        <label for="filename">Choose Location</label>
                        <select class="form-control" name="location">
                            <option value="1">AWS S3</option>
                            <option value="2">Local</option>
                        </select>
                        <br>

                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" id="file" name="file" required>

                            <p class="help-block">Max size 20MB.</p>
                            @if (Session::has('message'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <strong>Info!</strong>  {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>
                        <br>

                        <div class="form-group ">
                            <label for="filename">New File Name (Optional)</label>
                            <input type="text" class="form-control" id="filename" name="filename"
                                   placeholder="File Name"
                                   required>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-success">Upload <i class="fa fa-upload"></i></button>
                        <br> <br>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> Something went wrong!.<br><br>

                                <ul class="list-group">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>

                        @endif
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">List of Files</div>
                    <div class="panel-body">
                        @foreach($fileList as $file)
                            <ul>
                                <li> <a href="{{$awsLink}}{{$file}}">{{$file}}</a></li>
                                <li><img src="{{$awsLink}}{{$file}}" class="img-responsive" alt="Responsive image"></li>
                            </ul>


                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    <img src="img/aws-logo.jpg" alt="..." class="img-responsive img-rounded hide" >

@stop

@section ('local_scripts')


@stop
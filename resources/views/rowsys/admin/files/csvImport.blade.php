@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop


@section('content')

<br>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-6">
            <div class="well ">
                <h4 class="text-primary">Import Tasks Into Database</h4>

                <form action="import-file" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}


                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <input type="file" id="file" name="file" required>

                        <p class="help-block">(only xls, xlsx and csv files can be imported).</p>
                        @if (Session::has('message'))
                            <div class="alert alert-info alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Info!</strong>  {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-success">Import <i class="fa fa-upload"></i></button>
                    <br> <br>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Something went wrong!.<br><br>

                            <ul class="list-group">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>

                    @endif
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">List of Files</div>
                <div class="panel-body">
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section ('local_scripts')


@stop
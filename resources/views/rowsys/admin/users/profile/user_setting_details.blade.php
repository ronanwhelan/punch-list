<div class="row animated fadeIn">
    <div class="col-lg-12">
        <a title="Logout" href="/logout" class="pull-right">Sign Out <i class="fa fa-sign-out" aria-hidden="true"></i></a>
        <h5> Settings for {{$user->name}}'s account</h5>

    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#user-details" aria-expanded="true" aria-controls="user-details">
                            Update Details
                        </a>
                    </h4>
                </div>
                <div id="user-details" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body ">
                        <form class="form-horizontal" role="form" id="update-my-details-form" method="POST" action="{{ url('/user/update/my-details') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="user_id" value="{{$user->id}}">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Name:</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{$user->name }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Surname:</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="surname" value="{{$user->surname }}">

                                    @if ($errors->has('surname'))
                                        <span class="help-block">{{ $errors->first('surname') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Email:</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{$user->email }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Password:</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">

                                    <p class="font-xs font-xs txt-color-orange">password is required to update details</p>

                                    @if ($errors->has('password'))
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="button" onclick="updateMyDetails('update-my-details-form', '/user/update/my-details',{{$user->id}})" class="btn btn-primary"><i class="fa fa-btn fa-user"></i> Update</button>

                                    <div id="my-details-updated-message" class=" alert alert-success alert-dismissible" role="alert" hidden="true">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fa fa-check">&nbsp; </i> Details Updated
                                    </div>

                                </div>
                            </div>

                            <!-- SERVER AND VALIDATION ERRORS -->
                            <div class="alert alert-info fadeIn hide  " id="user-modal-update-errors"></div>
                        </form>
                    </div>
                </div>
            </div>



            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#user-passsword" aria-expanded="false" aria-controls="user-passsword">
                            Change Password
                        </a>
                    </h4>
                </div>

                <div id="user-passsword" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">

                        <form class="form-horizontal" id="update-my-password-form" role="form" method="POST" action="{{ url('/user/update/password') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-md-4 control-label">Current:</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="current_password" >

                                    @if ($errors->has('current_password'))
                                        <span class="help-block">{{ $errors->first('current_password') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">New:</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password" >

                                    @if ($errors->has('password'))
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Confirm Password:</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        {{ $errors->first('password_confirmation') }}
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="button" onclick="updateMyDetails('update-my-password-form', '/user/update/my-password',{{$user->id}})" class="btn btn-primary"><i class="fa fa-btn fa-user"></i> Update</button>

                                    <div id="my-password-updated-message" class=" alert alert-success alert-dismissible" role="alert" hidden="true">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fa fa-check">&nbsp; </i> Password Updated
                                    </div>

                                </div>
                            </div>

                            <!-- SERVER AND VALIDATION ERRORS -->
                            <div class="alert alert-info fadeIn hide  " id="user-modal-update-password-errors"></div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

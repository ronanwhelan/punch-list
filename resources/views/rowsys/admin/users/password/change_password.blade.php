@extends('layouts.app')

@section('content')

    <!-- Main Content -->
    <section class="main-content">
        <div class="col-lg-8 col-lg-offset-2">
            <img src="img/branding/figaro-logo-white.png" class="figaro-logo img-responsive" alt="Figaro">

            <form class="form-horizontal"  role="form" method="POST" action="{{ url('/user/update/default-password') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="col-md-4 control-label">Current Password:</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="current_password" >

                        @if ($errors->has('current_password'))
                            <span class="help-block">{{ $errors->first('current_password') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">New:</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password" >

                        @if ($errors->has('password'))
                            <span class="help-block">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Confirm:</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation">

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        {{ $errors->first('password_confirmation') }}
                                    </span>
                        @endif
                    </div>
                </div>
                @if (\Session::has('message'))
                    <div class="col-md-6 col-md-offset-4">
                    <div class="alert alert-info text-center" role="alert">
                        <p class="text-danger">{{ \Session::get('message') }}</p>
                    </div>
                        </div>
                @endif

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-btn fa-user"></i> Update</button>

                        <div id="my-password-updated-message" class=" alert alert-success alert-dismissible" role="alert" hidden="true">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <i class="fa fa-check">&nbsp; </i> Password Updated
                        </div>

                    </div>
                </div>

                <!-- SERVER AND VALIDATION ERRORS -->
                <div class="col-md-6 col-md-offset-4">
                <div class="alert alert-info fadeIn hide  " id="user-modal-update-password-errors"></div>
                </div>

                <div id="password-guidelines-section">
                    <div class="col-md-6 col-md-offset-4">
                    <div class="well" style="background-color: #139ff7; color: #f5f5f5">
                        <h4>Password Guidelines</h4>
                        <ul>
                            <li> <small class="">Is at least eight characters long.</small></li>
                            <li> <small>Does not contain your user name, real name, or company name.</small></li>
                            <li> <small> Does not contain a complete word.</small></li>
                            <li> <small> Is significantly different from previous passwords.</small></li>

                        </ul>
                    </div>

                </div>
            </form>


        </div>
    </section><!-- end section -->

@endsection
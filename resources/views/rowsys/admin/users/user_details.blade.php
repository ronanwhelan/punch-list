<hr>
<input type="hidden" name="user_id" value="{{$user->id}}">
<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Surname</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="surname" name="surname" value="{{$user->surname}}">
    </div>
</div>


<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

    <div class="col-sm-10">
        <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}">
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Company</label>

    <div class="col-md-6">
        <select class=" form-control selectpicker" data-style="btn-primary" name="company" data-live-search="true">
            <option value=""></option>
            @foreach($companies as $company)
                @if($user->company_id === $company->id)
                    <option value="{{$company->id}}" selected>{{$company->name}}</option>
                @else
                    <option value="{{$company->id}}">{{$company->name}}</option>
                @endif
            @endforeach

        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Role/Team</label>
    <div class="col-md-6">

        <select class=" form-control selectpicker" data-style="btn-primary" name="roles[]" multiple>
            @foreach($roles as $role)
                @if(in_array($role->id,$userRoles))
                    <option value="{{$role->id}}" selected>{{$role->name}}</option>
                @else
                    <option value="{{$role->id}}">{{$role->name}}</option>
                @endif
            @endforeach
        </select>
        @if ($errors->has('roles'))
            <span class="help-block">{{ $errors->first('roles') }}</span>
        @endif
    </div>
</div>

@if(Auth::user()->access_to_tracker === 1)
    <hr>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
                @if($user->access_to_tracker === 1)
                    <label><input name="access_to_tracker" type="checkbox" checked> Tracker Access</label>
                @else
                    <label><input name="access_to_tracker" type="checkbox"> Tracker Access</label>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Tracker Access Level</label>

        <div class="col-sm-10">
            <select class=" form-control selectpicker" data-style="btn-primary" data-live-search="true" name="role">
                @for($i = 1;$i < 10;$i++)
                    @if($user->role === $i)
                        <option selected value="{{$i}}">Level {{$i}}</option>
                    @else
                        <option value="{{$i}}">Level {{$i}} </option>
                    @endif
                @endfor
            </select>
        </div>
    </div>
@endif


@if(Auth::user()->access_to_punchlist === 1)
    <hr>
    <div class="form-group">

        <div class="col-sm-offset-2 col-sm-10">

            <div class="checkbox">

                @if($user->access_to_punchlist === 1)
                    <label><input name="access_to_punchlist" type="checkbox" checked> Punchlist Access</label>
                @else
                    <label><input name="access_to_punchlist" type="checkbox"> Punchlist Access</label>
                @endif
            </div>
        </div>
        <label for="inputEmail3" class="col-sm-2 control-label">Punch List Access</label>
    </div>
    <div class="form-group hide">
        <label for="inputEmail3" class="col-sm-2 control-label">Punchlist Access Level</label>

        <div class="col-sm-10 hide">
            <select class=" form-control selectpicker" data-style="btn-primary" data-live-search="true" name="punchlist_role">
                @for($i = 1;$i < 10;$i++)
                    @if($user->punchlist_role === $i)
                        <option selected value="{{$i}}">Level {{$i}}</option>
                    @else
                        <option value="{{$i}}">Level {{$i}} </option>
                    @endif
                @endfor
            </select>
        </div>
    </div>
    <div class="form-group hide">
        <label for="inputEmail3" class="col-sm-2 control-label">Filter</label>

        <div class="col-sm-10 hide">
            <select class=" form-control selectpicker" data-style="btn-primary" name="punchlist_filter">
                <option value=""> View All</option>
                <option value="1"> Responsible Company Only</option>
                <option value="2"> Assigned Company Only</option>
            </select>
        </div>
    </div>
@endif
<hr>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
            <label>
                @if($user->confirmed === 1)
                    <input name="confirmed" type="checkbox" checked> Allow Access To Figaro
                @else
                    <input name="confirmed" type="checkbox"> Allow Access To Figaro
                @endif
            </label>
        </div>
    </div>
</div>
<hr>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Reset Password</label>

    <div class="col-sm-10">
        <div class="input-group"><span class="input-group-addon"><input name="chk_reset_password" type="checkbox" aria-label="..."></span>
            <input type="password" class="form-control" id="reset_password" name="reset_password" value="">
        </div>
        <p class="txt-color-orange font-xs">Reset users password. Checkbox must be selected to change the users password.
        </p>
    </div>
</div>

<hr>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <!-- SERVER AND VALIDATION ERRORS -->
        <div class="alert alert-info fadeIn hide  " id="update-user-validation-errors"></div>
        <button type="button" onclick="updateUserDetails('update-user-details-form', '/admin/update-user','{{$user->id}}')" class="btn btn-success"><i class="fa fa-user"></i>&nbsp;Update
        </button>
        <br><br>
        <p><small>Note: If you wish to remove a user it is better to restrict their access to
            Figaro as the user details is used for history purpose e.g Christine completed this task 2 months ago<small> </p>
        <a class=" pull-right btn btn-danger hide" onclick="deleteUser({{$user->id}})"><i class="fa fa-user-times"></i>&nbsp;Delete</a>
    </div>
</div>
<hr>
<input type="hidden" name="company_id" value="{{$company->id}}">
<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="name" name="name" value="{{$company->name}}">
    </div>
</div>



<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="button" onclick="updateCompanyDetails('update-company-details-form', '/admin/update-company','{{$company->id}}')" class="btn btn-success">Update</button>
        <button type="button" class="pull-right btn btn-danger"><i class="fa fa-times"></i> Delete</button>
    </div>
</div>
<!-- SERVER AND VALIDATION ERRORS -->
<div class="alert alert-info fadeIn hide  " id="add-company-update-validation-errors"></div>
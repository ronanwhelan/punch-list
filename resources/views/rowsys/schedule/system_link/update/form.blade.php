<form id="update-milestone-form">
    <input type="hidden" name="schedule_link_date_id" value="{{$scheduleLink->id}}">
    <input type="hidden" name="system_id" value="{{$systemId}}">
    <input type="hidden" name="milestone_number" value="{{$milestoneId}}">


    <div class="">

            <div class="form-group">
                <div class="alert alert-info" role="alert">{{$infoMessage}}</div>
                <label for="">Schedule Link</label>
                <input type="text" class="form-control" id="schedule-number-input" name="schedule_link" value="{{$scheduleNumber}}">
                <input type="hidden" value="{{$scheduleLinkId}}" name="scheduleId" id="schedule-number">
                <br>

                <div class="alert alert-success" role="alert"><p class="font-sm text-primary">{{$scheduleLinkDescription}}</p></div>


            </div>

            <div class="form-group">
                <label for="">Schedule Date Choice  ({{$choice}})</label>
                <select class="form-control selectpicker"
                        id="choice" name="choice"
                        data-style="btn-primary">
                    @if($choice === 'Start' ||$choice === 'start' )
                        <option selected value="start">Start Date</option>
                    @else
                        <option value="start">Start Date</option>
                    @endif
                    @if($choice === 'Finish' || $choice === 'finish')
                        <option selected value="finish">Finish Date</option>
                    @else
                        <option value="finish">Finish Date</option>
                    @endif
                </select>
            </div>

            <div class="form-group">
                <label for="">Lead (- days)</label>
                <input type="number" class="form-control" id="lag" name="lag" value="{{$lag}}">
            </div>

            <div class="form-group">
                <label for="">Stage</label>
                <select class="form-control selectpicker"
                        id="stage" name="stage"
                        data-style="btn-primary">
                    @foreach ($stages as $stage)
                        @if($selectedStage === $stage->id)
                            <option selected value="{{$stage->id}}">{{$stage->name}}</option>
                        @else
                            <option value="{{$stage->id}}">{{$stage->name}}</option>
                        @endif
                    @endforeach

                </select>
            </div>
            <div class="form-group">
                <label for="">Task Type</label>
                <p>Should this be a task from the tracker??</p>
                <select class="form-control selectpicker"
                        id="type" name="type" data-live-search="true"
                        data-style="btn-primary" title="Task Types">
                    <optgroup label="Task Types">
                        @foreach ($taskTypes as $taskType)
                            @if($selectedType === $taskType->id)
                                <option selected value="{{$taskType->id}}">{{$taskType->short_name}}, {{$taskType->name}}</option>
                            @else
                                <option value="{{$taskType->id}}">{{$taskType->short_name}}, {{$taskType->name}}</option>
                            @endif
                        @endforeach
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="alert alert-info fadeIn hide" id="validation-errors"></div>
        <button type="button" onclick="updateMilestoneDetails('update-milestone-form', '/schedule/milestone/update',{{$systemId}})" class="btn btn-success btn-block">Submit
        </button>

</form>

<script>
    $(document).ready(function () {
        $('.selectpicker').selectpicker();
    });

    $('#schedule-number-input').keyup(function () {
        console.log('get schedule number');
        var inputTextObj = $('#schedule-number-input');
        var tag = inputTextObj.val();
        var source = "/get-schedule-number-autocomplete-list/" + tag;
        inputTextObj.autocomplete({
            source: source,
            minLength: 1,
            autoOpen: true,
            width: 500,
            modal: false,
            zIndex: 10000,
            appendTo: "search-task-number",
            title: 'Duplicate',
            select: function (event, ui) {
                inputTextObj.val(ui.item.value);
                $('#schedule-number').val(ui.item.id);
            }
        });
    });
</script>

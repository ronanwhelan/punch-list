@extends ('rowsys.schedule.includes.layout.layout')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')

    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">Edit System Milestone Schedule Link</h3></div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="">Select System </label>
                        <select class="form-control selectpicker"
                                id="system" name="system" data-live-search="true"
                                data-style="btn-primary" title="System">
                            <optgroup label="System">
                                @foreach ($systems as $system)
                                    <option value="{{$system->id}}">{{$system->name}}, {{$system->description}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Select Milestone</label>
                        <select class="form-control selectpicker"
                                id="milestone" name="milestone"
                                data-style="btn-primary" title="Select Milestone"
                                onchange="getMilestoneForm()">
                            <optgroup label="Milestones">
                                @foreach ($mileStones as $mileStone)
                                    <option value="{{$mileStone->id}}">{{$mileStone->name}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>

                    <div id="update-milestone-form-section"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">Panel title</h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div id="myTabs" class="col-lg-12">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                                <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                                <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    <h3>Tab 1</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus
                                        consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet
                                        id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <h3>Tab 2</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus
                                        consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet
                                        id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages">
                                    <h3>Tab 3</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus
                                        consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet
                                        id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script>
        $(document).ready(function () {
            //
        });

        function getMilestoneForm(){
            var systemId = $('#system').val();
            var milestone = $('#milestone').val();
           // console.log('get milestone form' + systemId + milestone );
            var data = {systemId:systemId,milestone:milestone};
            $.ajax({
                type: 'GET',
                data: {data: data},
                url: "/schedule/system-link/edit-form", success: function (result) {
                    console.log(result);
                    $('#update-milestone-form-section').html(result);
                }
            });
        }

        /*
         * ----------------------------------------------------
         *           Update the Item Details
         * ----------------------------------------------------
         *   Same function used to update the Owner,
         *  Responsible and Assignee Details
         */
        function updateMilestoneDetails(formName, urlText, id) {

            var formId = '#' + formName;
            //a token is needed to prevent cross site
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var form = $(formId);
            var method = form.find('input[name="_method"]').val() || 'POST';
            var datastring = $(form).serialize();
            var validationSection = $('#validation-errors');//update-item-owner-details-form-validation-errors
            pleaseWait('Updating Item Details');
            $.ajax({
                type: 'POST',
                url: urlText,
                data: datastring,
                success: function (result) {
                    console.log(result);
                    $.notify({
                        title: '<strong>Success!</strong><br>',
                        message: result + ""
                    }, {
                        animate: {
                            enter: 'animated fadeInLeft',
                            exit: 'animated fadeOutRight'
                        },
                        type: 'success',
                        //offset: {x: 100, y: 100},
                        //placement: {from: "bottom"},
                        showProgressbar: false,
                        delay: 1500
                    });
                    setTimeout(function () {
                        //location.reload();
                        // getItemDetails(id);
                    }, 800);
                    validationSection.addClass('hide');

                },
                beforeSend: function () {
                    // pleaseWait();
                },
                complete: function () {
                    waitDone();
                },
                // Handles the Errors if returned back from the Server -
                // Including any Validation Errors
                error: function (xhr, status, error) {
                    waitDone();
                    validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong>');
                    var errorText = '';
                    var validationErrorText = ' Validation Errors';
                    var list = $("<ul class=''></ul>");
                    var contactAdminText = ' - Contact the Figaro Administrator';

                    // If there is a Validation Error then show the user the validation error Text
                    if (error === 'Unprocessable Entity' || xhr.status === 422) {
                        //validationSection.append(validationErrorText);
                        jQuery.each(xhr.responseJSON, function (i, val) {
                            validationErrorText = '<li>' + val + '</li>';
                            list.append(validationErrorText);
                            console.log(validationErrorText);
                        });
                    } else {
                        errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                        errorText = errorText + contactAdminText;
                    }
                    validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
                }
            });

        }
    </script>

@stop
<div class="">
    <div class="row hide">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div id="buttons" class="font-xs"></div>
                    <div id="toggle_buttons" class="btn-group pull-right">
                        <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="0">Col 1</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="1">Col 2</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="2">Col 3</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">Col 4</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="4">Col 5</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Col 6</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="6">Col 7</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="7">Col 8</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="8">Col 9</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="9">Col 10</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">

        <div class="panel-body">
            <div class="row" id="table-section" style="opacity: .08">
                <div class="col-lg-12 font-xs">

                    <table id="data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr class="font-xs">
                            <th data-class="expand" class="font-xs">System</th>
                            @foreach($mileStones as $milestone)
                                <th data-hide="phone" class="font-xs">{{$milestone->name}}</th>
                                @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        {{-- Insert For each to populate table--}}

                        @foreach($systemLinkDates as $link)
                            <tr class="font-xs" onclick="showUpdateModelModal('/get-design-doc-details/1')">
                                <td>{{$link->system->name}} <br></td>

                                <td>@if($link->m1_schedule_link_number !== '')link: {{$link->m1_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m1_lag}}<br> Choice:  {{$link->m1_schedule_choice}}<br> Stage: {{$link->m1Stage->name}}<br> Task Type: {{$link->m1TaskType->name}}@else No Link @endif</td>
                                <td>@if($link->m2_schedule_link_number !== '')link: {{$link->m2_schedule_link_number}}&nbsp;&nbsp; <br> Lag: {{$link->m2_lag}}<br> Choice:  {{$link->m2_schedule_choice}}<br> Stage: {{$link->m2Stage->name}}<br> Task Type: {{$link->m2TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m3_schedule_link_number !== '')link: {{$link->m3_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m3_lag}}<br> Choice:  {{$link->m3_schedule_choice}}<br> Stage: {{$link->m3Stage->name}}<br> Task Type: {{$link->m3TaskType->name}}@else No Link @endif</td>
                                <td>@if($link->m4_schedule_link_number !== '')link: {{$link->m4_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m4_lag}}<br> Choice:  {{$link->m4_schedule_choice}}<br> Stage: {{$link->m4Stage->name}}<br> Task Type: {{$link->m4TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m5_schedule_link_number !== '')link: {{$link->m5_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m5_lag}}<br> Choice:  {{$link->m5_schedule_choice}}<br> Stage: {{$link->m5Stage->name}}<br> Task Type: {{$link->m5TaskType->name}}@else No Link @endif</td>
                                <td>@if($link->m6_schedule_link_number !== '')link: {{$link->m6_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m6_lag}}<br> Choice:  {{$link->m6_schedule_choice}}<br> Stage: {{$link->m6Stage->name}}<br> Task Type: {{$link->m6TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m7_schedule_link_number !== '')link: {{$link->m7_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m7_lag}}<br> Choice:  {{$link->m7_schedule_choice}}<br> Stage: {{$link->m7Stage->name}}<br> Task Type: {{$link->m7TaskType->name}}@else No Link @endif </td>
                                <td>@if($link->m8_schedule_link_number !== '')link: {{$link->m8_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m8_lag}}<br> Choice:  {{$link->m8_schedule_choice}}<br> Stage: {{$link->m8Stage->name}}<br> Task Type: {{$link->m8TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m9_schedule_link_number !== '')link: {{$link->m9_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m9_lag}}<br> Choice:  {{$link->m9_schedule_choice}}<br> Stage: {{$link->m9Stage->name}}<br> Task Type: {{$link->m9TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m10_schedule_link_number !== '')link: {{$link->m10_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m10_lag}}<br> Choice:  {{$link->m11_schedule_choice}}<br> Stage: {{$link->m10Stage->name}}<br> Task Type: {{$link->m10TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m11_schedule_link_number !== '')link: {{$link->m11_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m11_lag}}<br> Choice:  {{$link->m12_schedule_choice}}<br> Stage: {{$link->m11Stage->name}}<br> Task Type: {{$link->m11TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m12_schedule_link_number !== '')link: {{$link->m12_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m12_lag}}<br> Choice:  {{$link->m13_schedule_choice}}<br> Stage: {{$link->m12Stage->name}}<br> Task Type: {{$link->m12TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m13_schedule_link_number !== '')link: {{$link->m13_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m13_lag}}<br> Choice:  {{$link->m14_schedule_choice}}<br> Stage: {{$link->m13Stage->name}}<br> Task Type: {{$link->m13TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m14_schedule_link_number !== '')link: {{$link->m14_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m14_lag}}<br> Choice:  {{$link->m15_schedule_choice}}<br> Stage: {{$link->m14Stage->name}}<br> Task Type: {{$link->m14TaskType->name}} @else No Link @endif</td>
                                <td>@if($link->m15_schedule_link_number !== '')link: {{$link->m15_schedule_link_number}}&nbsp;&nbsp;<br> Lag: {{$link->m15_lag}}<br> Choice:  {{$link->m16_schedule_choice}}<br> Stage: {{$link->m15Stage->name}}<br> Task Type: {{$link->m15TaskType->name}} @else No Link @endif</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




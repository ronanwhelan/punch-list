@extends ('rowsys.schedule.includes.layout.layout')

@section ('page_related_css')
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/vendor/datatables/datatables.responsive.css"/>
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title">System Milestone Schedule Links</h3></div>
            <div class="panel-body">
                <div id="myTabs" class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Table</a></li>
                        <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Edit</a></li>
                        <li class="hide"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div id="myTabContent" class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                            @include('rowsys.schedule.system_link.read.table.table')
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="profile">
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <div class="panel panel-info">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="">Select System</label>
                                                    <select class="form-control selectpicker"
                                                            id="system" name="system" data-live-search="true"
                                                            data-style="btn-primary" title="System">
                                                        <optgroup label="System">
                                                            @foreach ($systems as $system)
                                                                <option value="{{$system->id}}">{{$system->name}}, {{$system->description}}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Select Milestone</label>
                                                    <select class="form-control selectpicker"
                                                            id="milestone" name="milestone"
                                                            data-style="btn-primary" title="Select Milestone"
                                                            onchange="getMilestoneForm()">
                                                        <optgroup label="Milestones">
                                                            @foreach ($mileStones as $mileStone)
                                                                <option value="{{$mileStone->id}}">{{$mileStone->name}}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    </select>
                                                </div>

                                                <div id="update-milestone-form-section"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="messages">
                            <h3>Tab 3</h3>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>


    <!-- ==========================CONTENT ENDS HERE ========================== -->
@stop

@section ('local_scripts')

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script>

        var table = $('#data_table');

        $(document).ready(function () {
            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";

            var responsiveHelper;

            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            table = $('#data_table').DataTable({
                dom: 'Bfrtip',
                buttons: ['copy', 'excel'],
                //"sDom":"flrtip",
                "bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                "bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 100,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                responsive: true,
                // paging: false,
                "bStateSave": true, // saves sort state using localStorage
                autoWidth: false,
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(table, breakpointDefinition);
                    }
                },
                rowCallback: function (nRow) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                drawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }

            });
            // Apply the filter
            $("#example thead th input[type=text]").on('keyup change', function () {
                table.column($(this).parent().index() + ':visible').search(this.value).draw();
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: [//'excel'//'copyHtml5',//'excelHtml5',//'csvHtml5',//'pdfHtml5'
                ]
            }).container().appendTo($('#buttons'));
            table.buttons().container().appendTo($('.col-sm-6:eq(0)', table.table().container()));

            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = table.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });

            $('#table-section').fadeTo(1500, '1');
            $('#loading-box-section').remove();

            //--------------------------------------------------------
            //      TOGGLE BUTTONS
            //--------------------------------------------------------
            //Add Toggle Buttons to top of table in the #_filter section
            var toggleButtons = $('#toggle_buttons');
            var tableFilterSection = $('#data_table_filter');

            var dtButtons = $('.dt-buttons');
            dtButtons.addClass('hidden-xs hidden-sm hidden-md ');
            //tableFilterSection.append('<div class="">Task List</div>');
            tableFilterSection.append('&nbsp;&nbsp;').append(toggleButtons);


            /*            tableFilterSection.append('&nbsp;&nbsp;').append('<button class="btn btn-primary hidden-sm hidden-xs" ' +
             'role="button" data-toggle="collapse" data-parent="#accordion" ' +
             'href="#search-stats" aria-expanded="true" aria-controls="search-stats">Stats ' +
             '<i class="fa fa-bar-chart-o"></i> </button>').append('&nbsp;&nbsp;');*/

            //--------------------------------------------------------
            //      EXPORT BUTTONS
            //--------------------------------------------------------
            tableFilterSection.append('&nbsp;&nbsp;').append(
                    '<div class="btn-group hidden-xs hidden-sm"> <button type="button" ' +
                    'class="btn btn-primary dropdown-toggle" ' +
                    'data-toggle="dropdown" aria-haspopup="true" ' +
                    'aria-expanded="false"> Export <span class="caret"></span> </button> <ul class="dropdown-menu"> ' +
                    '<li id="export-buttons" style="padding: 10px;"><a href="#"></a></li> ' +
                        //'<li><a href="#">Excel</a></li> ' +
                        //'<li><a href="#">Something else here</a>' +
                    '</li> </ul> </div>').append('&nbsp;&nbsp;');

            var exportButton1 = $('#export-button-1');
            var exportButtons = $('#export-buttons');
            var copyButton = $('.buttons-copy');
            var excelButton = $('.buttons-excel');
            exportButtons.append('&nbsp;&nbsp;').append(dtButtons);
            //exportButton2.append(excelButton);


            var tablePaginateSection = $('#data_table_paginate');
            //tablePaginateSection.css("background-color", "navy");


        });

        function getMilestoneForm(){
            var systemId = $('#system').val();
            var milestone = $('#milestone').val();
            // console.log('get milestone form' + systemId + milestone );
            var data = {systemId:systemId,milestone:milestone};
            $.ajax({
                type: 'GET',
                data: {data: data},
                url: "/schedule/system-link/edit-form", success: function (result) {
                    console.log(result);
                    $('#update-milestone-form-section').html(result);
                }
            });
        }

        /*
         * ----------------------------------------------------
         *           Update the Item Details
         * ----------------------------------------------------
         *   Same function used to update the Owner,
         *  Responsible and Assignee Details
         */
        function updateMilestoneDetails(formName, urlText, id) {

            var formId = '#' + formName;
            //a token is needed to prevent cross site
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var form = $(formId);
            var method = form.find('input[name="_method"]').val() || 'POST';
            var datastring = $(form).serialize();
            var validationSection = $('#validation-errors');//update-item-owner-details-form-validation-errors
            pleaseWait('Updating Item Details');
            $.ajax({
                type: 'POST',
                url: urlText,
                data: datastring,
                success: function (result) {
                    console.log(result);
                    $.notify({
                        title: '<strong>Success!</strong><br>',
                        message: result + ""
                    }, {
                        animate: {
                            enter: 'animated fadeInLeft',
                            exit: 'animated fadeOutRight'
                        },
                        type: 'success',
                        //offset: {x: 100, y: 100},
                        //placement: {from: "bottom"},
                        showProgressbar: false,
                        delay: 1500
                    });
                    setTimeout(function () {
                        //location.reload();
                        // getItemDetails(id);
                    }, 800);
                    validationSection.addClass('hide');

                },
                beforeSend: function () {
                    // pleaseWait();
                },
                complete: function () {
                    waitDone();
                },
                // Handles the Errors if returned back from the Server -
                // Including any Validation Errors
                error: function (xhr, status, error) {
                    waitDone();
                    validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong>');
                    var errorText = '';
                    var validationErrorText = ' Validation Errors';
                    var list = $("<ul class=''></ul>");
                    var contactAdminText = ' - Contact the Figaro Administrator';

                    // If there is a Validation Error then show the user the validation error Text
                    if (error === 'Unprocessable Entity' || xhr.status === 422) {
                        //validationSection.append(validationErrorText);
                        jQuery.each(xhr.responseJSON, function (i, val) {
                            validationErrorText = '<li>' + val + '</li>';
                            list.append(validationErrorText);
                            console.log(validationErrorText);
                        });
                    } else {
                        errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                        errorText = errorText + contactAdminText;
                    }
                    validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
                }
            });

        }


    </script>

@stop

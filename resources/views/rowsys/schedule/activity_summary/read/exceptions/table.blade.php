<div class="">
    <div class="row hide">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div id="buttons" class="font-xs"></div>
                    <div id="exceptions_toggle_buttons" class="btn-group pull-right">
                        <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="0">Col 1</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="1">Col 2</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="2">Col 3</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">Col 4</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="4">Col 5</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Col 6</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="6">Col 7</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="7">Col 8</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="8">Col 9</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="9">Col 10</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="exceptions_table_row">
        <div class="col-lg-12 font-xs">
            <table id="exceptions_data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr class="font-xs">
                    <th data-class="expand" class="font-xs">Activity</th>
                    <th data-hide="" class="font-xs">Description</th>
                    <th data-hide="phone" class="font-xs">Task Count</th>
                    <th data-hide="phone" class="font-xs">Exceptions</th>
                    <th data-hide="phone" class="font-xs">Exception Tasks List</th>
                </tr>
                </thead>
                <tbody>
                {{-- Insert For each to populate table--}}
                @foreach($activitySummaries as $activitySummary)
                    <tr>
                        <td>{{$activitySummary[0]}}</td>
                        <td>{{$activitySummary[1]}}</td>
                        <td>{{$activitySummary[2]}}</td>
                        <td>{{$activitySummary[3]}}</td>
                        <td>
                            <ul>
                                @foreach($activitySummary[4] as $task)
                                    <li>  {{$task}}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
    var exceptionsTable = $('#exceptions_data_table');

    $(document).ready(function () {
        var ex_screenHeight = $(document).height();
        ex_screenHeight = (ex_screenHeight - 600) + "px";

        var ex_responsiveHelper;

        var ex_breakpointDefinition = {
            computer: 2000,
            tablet: 1024,
            phone: 480
        };

        exceptionsTable = $('#exceptions_data_table').DataTable({
            //dom: 'Bfrtip',
            //buttons: ['copy', 'excel'],
            //"sDom":"flrtip",
            "bFilter": true,
            //"bInfo": true,
            //"bLengthChange": true,
            "bAutoWidth": true,
            fixedHeader: true,
            "pageLength": 100,
            "bPaginate": true,
            //"aaSorting": [[ 3, "ASC" ]],
            "aaSorting": [],
            "scrollY": "900px",
            //"scrollX": "500px",
            //"paging": false,
            responsive: true,
            // paging: false,
            "bStateSave": true, // saves sort state using localStorage
            autoWidth: false,
            preDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!ex_responsiveHelper) {
                    ex_responsiveHelper = new ResponsiveDatatablesHelper(exceptionsTable, ex_breakpointDefinition);
                }
            },
            rowCallback: function (nRow) {
                ex_responsiveHelper.createExpandIcon(nRow);
            },
            drawCallback: function (oSettings) {
                ex_responsiveHelper.respond();
            }
        });


    });
</script>




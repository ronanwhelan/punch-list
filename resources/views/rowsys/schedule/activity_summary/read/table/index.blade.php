@extends ('rowsys.schedule.includes.layout.layout')

@section ('page_related_css')
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/vendor/datatables/datatables.responsive.css"/>
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    @include('rowsys.schedule.activity_summary.update.modal')
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">Schedule Activity Summary</h3></div>
        <div class="panel-body">
            <div class="row">
                <div id="myTabs" class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Summary</a></li>
                        <li class="hide"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Activity</a></li>
                        <li class="hide"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Exceptions</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div id="myTabContent" class="tab-content">
                        <br>
                        <div class="panel-body">
                            <div id="activity-summary-table">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    <form id="scheduler-activity-call-form">
                                        <div>
                                            <label class="radio-inline">
                                                <input type="radio" checked name="report-choice-radio-button" id="view-all" value="all"> All
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="report-choice-radio-button" id="prep-only" value="prep"> Prep Only
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="report-choice-radio-button" id="exec-only" value="exec"> Execution Only
                                            </label>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-primary btn-lg" id="btn-generate-summary" onclick="ActivitySummaryTable()"> Generate Activity Summary</button>
                                        <br><br>
                                        <div class="alert alert-info" role="alert"><p><i class="fa fa-exclamation-circle"></i> This may take some time to build and return the data - please be patient.</p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="profile">
                        <h3>Tab 2</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque
                            eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui.
                            Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>

                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages">
                        <div id="activity-exceptions-section">
                            <button class="btn btn-info btn-lg" id="btn-generate-exception" onclick="ActivityExceptionsTable()"> Generate Exceptions Summary</button>
                            <p>This returns an overview for each Schedule activity that does not fit the configuration settings for the activity summary report </p>
                            <div class="alert alert-info" id="exceptions-alert" role="alert"><p><i class="fa fa-exclamation-circle"></i> This may take some time to build and return the data - please
                                    be patient.</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


    <!-- ==========================CONTENT ENDS HERE ========================== -->
@stop

@section ('local_scripts')

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script>

        $(document).ready(function () {

            //ActivitySummaryTable();

        });


        /** ===============================================================
         *  Fetch the Activity Summary Data & Populate the Table
         * ===============================================================
         */
        function ActivitySummaryTable() {
            console.log('function : ActivitySummaryTable ');
            var form = ('#scheduler-activity-call-form');
            //var url = form.attr('action');
            //var method = form.find('input[name="_method"]').val() || 'POST';
            var datastring = $(form).serialize();
            //data = {'number':number,'stage':stage};
            var generateButton = $('#btn-generate-summary');
            generateButton.append('&nbsp;<i class="fa fa-spinner fa-spin"></i>').addClass('disabled');
            $('.alert').show();
            $.ajax({
                type: 'GET',
                data: datastring,
                url: "/schedule/activity-summary/all-data", success: function (result) {
                    // JSON.parse(result);
                    $('#activity-summary-table').html(result);
                    console.log('activity data  = ' + result);
                    generateButton.remove('&nbsp;<i class="fa fa-spinner fa-spin"></i>').removeClass('disabled');
                }
            });
        }

        /** ===============================================================
         *  Fetch the Activity Summary Exception Data and Table
         * ===============================================================
         */
        function ActivityExceptionsTable() {
            console.log('function : ActivitySummaryTable ');
            //data = {'number':number,'stage':stage};
            $('#btn-generate-exception').append('&nbsp;<i class="fa fa-spinner fa-spin"></i>').addClass('disabled');
            $('#exceptions-alert').show();
            $.ajax({
                type: 'GET',
                //data: {data: data},
                url: "/schedule/activity-exceptions", success: function (result) {
                    // JSON.parse(result);
                    $('#activity-exceptions-section').html(result);
                    console.log('exceptions data  = ' + result);
                }
            });
        }


    </script>

@stop

<div class="">
    <h3>{{$displayChoice}}</h3>
    <div class="row hide">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div id="buttons" class="font-xs"></div>
                    <div id="toggle_buttons" class="btn-group pull-right">
                        <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="0">Col 1</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="1">Col 2</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="2">Col 3</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">Col 4</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="4">Col 5</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Col 6</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="6">Col 7</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="7">Col 8</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="8">Col 9</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="9">Col 10</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="table-section" style="opacity: .08">
        <div class="col-lg-12 font-xs">
            <table id="data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr class="font-xs">
                    <th data-class="expand" class="font-xs">Activity</th>
                    <th data-hide="" class="font-xs">Description</th>
                    <th data-hide="phone" class="font-xs">%</th>
                    <th data-hide="phone" class="font-xs">Start Status</th>
                    <th data-hide="phone" class="font-xs">Start Date</th>
                    <th data-hide="phone" class="font-xs">Finish Status</th>
                    <th data-hide="phone" class="font-xs">Finish Date</th>
                    <th data-hide="phone" class="font-xs">Hrs %</th>
                    <th data-hide="phone" class="font-xs">Hrs Assigned</th>
                    <th data-hide="phone" class="font-xs">Hrs Earned</th>
                </tr>
                </thead>
                <tbody>
                {{-- Insert For each to populate table--}}
                @foreach($activitySummaries as $activitySummary)
                    <tr onclick="showUpdateModelModal('/get-design-doc-details/1')">
                        <td>{{$activitySummary[0]}}</td>
                        <td>{{$activitySummary[1]}}</td>
                        <td>{{$activitySummary[2]}}</td>
                        <td>{{$activitySummary[3]}}</td>
                        <td>{{$activitySummary[4]}}</td>
                        <td>{{$activitySummary[5]}}</td>
                        <td>{{$activitySummary[6]}}</td>
                        <td>{{$activitySummary[7]}}</td>
                        <td>{{$activitySummary[8]}}</td>
                        <td>{{$activitySummary[9]}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
    var table = $('#data_table');

    $(document).ready(function () {

        var screenHeight = $(document).height();
        screenHeight = (screenHeight - 600) + "px";

        var responsiveHelper;

        var breakpointDefinition = {
            computer: 2000,
            tablet: 1024,
            phone: 480
        };

        table = $('#data_table').DataTable({
            dom: 'Bfrtip',
            buttons: ['copy', 'excel'],
            //"sDom":"flrtip",
            "bFilter": true,
            //"bInfo": true,
            //"bLengthChange": true,
            "bAutoWidth": true,
            fixedHeader: true,
            "pageLength": 100,
            //"bPaginate": true,
            //"aaSorting": [[ 3, "ASC" ]],
            "aaSorting": [],
            "scrollY": "900px",
            //"scrollX": "500px",
            //"paging": false,
            responsive: true,
            // paging: false,
            "bStateSave": true, // saves sort state using localStorage
            autoWidth: false,
            preDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(table, breakpointDefinition);
                }
            },
            rowCallback: function (nRow) {
                responsiveHelper.createExpandIcon(nRow);
            },
            drawCallback: function (oSettings) {
                responsiveHelper.respond();
            }

        });
        // Apply the filter
        $("#example thead th input[type=text]").on('keyup change', function () {
            table.column($(this).parent().index() + ':visible').search(this.value).draw();
        });

        var buttons = new $.fn.dataTable.Buttons(table, {
            buttons: [//'excel'//'copyHtml5',//'excelHtml5',//'csvHtml5',//'pdfHtml5'
            ]
        }).container().appendTo($('#buttons'));
        table.buttons().container().appendTo($('.col-sm-6:eq(0)', table.table().container()));

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();
            // Get the column API object
            var column = table.column($(this).attr('data-column'));
            // Toggle the visibility
            column.visible(!column.visible());
        });

        $('#table-section').fadeTo(1500, '1');
        $('#loading-box-section').remove();

        //--------------------------------------------------------
        //      TOGGLE BUTTONS
        //--------------------------------------------------------
        //Add Toggle Buttons to top of table in the #_filter section
        var toggleButtons = $('#toggle_buttons');
        var tableFilterSection = $('#data_table_filter');

        var dtButtons = $('.dt-buttons');
        dtButtons.addClass('hidden-xs hidden-sm hidden-md ');
        //tableFilterSection.append('<div class="">Task List</div>');
        tableFilterSection.append('&nbsp;&nbsp;').append(toggleButtons);
        /*            tableFilterSection.append('&nbsp;&nbsp;').append('<button class="btn btn-primary hidden-sm hidden-xs" ' +
         'role="button" data-toggle="collapse" data-parent="#accordion" ' +
         'href="#search-stats" aria-expanded="true" aria-controls="search-stats">Stats ' +
         '<i class="fa fa-bar-chart-o"></i> </button>').append('&nbsp;&nbsp;');*/

        //--------------------------------------------------------
        //      EXPORT BUTTONS
        //--------------------------------------------------------
        tableFilterSection.append('&nbsp;&nbsp;').append(
                '<div class="btn-group hidden-xs hidden-sm"> <button type="button" ' +
                'class="btn btn-primary dropdown-toggle" ' +
                'data-toggle="dropdown" aria-haspopup="true" ' +
                'aria-expanded="false"> Export <span class="caret"></span> </button> <ul class="dropdown-menu"> ' +
                '<li id="export-buttons" style="padding: 10px;"><a href="#"></a></li> ' +
                    //'<li><a href="#">Excel</a></li> ' +
                    //'<li><a href="#">Something else here</a>' +
                '</li> </ul> </div>').append('&nbsp;&nbsp;');

        var exportButton1 = $('#export-button-1');
        var exportButtons = $('#export-buttons');
        var copyButton = $('.buttons-copy');
        var excelButton = $('.buttons-excel');
        exportButtons.append('&nbsp;&nbsp;').append(dtButtons);
        //exportButton2.append(excelButton);


        var tablePaginateSection = $('#data_table_paginate');
        //tablePaginateSection.css("background-color", "navy");

    });
</script>




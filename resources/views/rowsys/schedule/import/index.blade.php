@extends ('rowsys.schedule.includes.layout.layout')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Import Schedule Dates</h3></div>
                <div class="panel-body">

                    <h2>Import the Schedule milestone data  </h2>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>Example of the import file structure and data</p>
                            <table width="100%" role="grid" id=""
                                   class="table table-striped table-hover table-bordered dataTable no-footer" style="">
                                <thead>
                                <tr class="text-primary" style="background-color: lightcyan">
                                    <th>Number (text)</th>
                                    <th>Description (text)</th>
                                    <th>Start Date (date)</th>
                                    <th>Finish Date (date)</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">TASK-0011AABB</th>
                                    <td>This the description of the Milestone</td>
                                    <td>2016-05-09 08:00:00</td>
                                    <td>2017-05-09 16:00:00</td>
                                </tr>
                                </tbody>
                            </table>
                            <p> (there is also an image below of a typical import file)</p>
                            <div class="" role="alert">
                                <ul>
                                    <li> <i class="fa fa-info-circle"></i> Import guide</li>
                                    <li> <small>The import file must be of type <mark>.csv</mark> If you have an excel file, simply 'save as' type .csv</small></li>
                                    <li>  <small>Start date OR Finish date cell can be empty - but not both e.g if start date is missing the end date will be stored for the start date. </small></li>
                                    <li>  <small>Time (e.g. hours) is not required for the start or finish date.</small></li>
                                    <li>  <small> Format the Date cells as  <mark>yyyy-mm-dd hh:mm:ss</mark></small></li>
                                    <li>  <small>The first line in the csv file is ignored i.e the header row</small></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <br>
                    <h4 class="text-primary">Choose a file to be uploaded</h4>
                    <form action="/import/schedule/dates" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" id="file" name="file" required>

                            <p class="help-block">(only csv files can be imported).</p>
                            @if (Session::has('message'))
                                <div class="alert alert-info alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <strong>Info!</strong>  {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>

                        <br>

                        <div class="row">
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-success btn-block">Import <i class="fa fa-upload"></i></button>
                                <br>
                            </div>
                            </div>

                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> Something went wrong!.<br><br>

                                    <ul class="list-group">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>

                            @endif

                    </form>

                </div>
            </div>
        </div>
    </div>

    <br><br>

    @if(sizeof($datesWithErrors) > 0)
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3>Schedule Dates with Errors</h3></div>
                    <div class="panel-body"></div>

                    <!-- Table -->
                    <table width="100%" role="grid" id="table-list"
                           class="table table-striped table-hover table-bordered dataTable no-footer" style="opacity: .5">
                        <thead>
                        <tr class="">
                            <th>Activity</th>
                            <th>Description</th>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Row</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($datesWithErrors as $errors)
                            <tr>
                                <th scope="row">{{$errors->number}}</th>
                                <td>{{$errors->description}}</td>
                                <td>{{$errors->string_start_date}}</td>
                                <td>{{$errors->string_finish_date}}</td>
                                <td>{{$errors->id + 1}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @endif


    <ul>
        <li><h2>Import File example</h2></li>
        <li><img src="/img/schedule-import-file-example-pic.png" alt="..." class="img-rounded img-responsive"></li>
    </ul>
                <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script src="/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


    <script>
        var otable = $('#table-list');
        $(document).ready(function () {
            var responsiveHelper_mission_list_column = undefined;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";
            /* COLUMN FILTER  */
            otable.DataTable({
                //"bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                //"bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 50,
                //"bPaginate": true,
                "aaSorting": [[0, "ASC"]],
                //"aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                //responsive: true,
                // paging: false,
                //"bStateSave": true // saves sort state using localStorage
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_mission_list_column) {
                        responsiveHelper_mission_list_column = new ResponsiveDatatablesHelper($('#table-list'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_mission_list_column.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_mission_list_column.respond();
                }

            });

            // Apply the filter
            $("#table-list thead th input[type=text]").on('keyup change', function () {
                otable.column($(this).parent().index() + ':visible').search(this.value).draw();
            });


            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = otable.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });
            $('#table-list').fadeTo(2000, '1');
            $('#loading-box-section').remove();
        });


    </script>

@stop

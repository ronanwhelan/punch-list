@extends ('rowsys._app.layouts.attachment')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')

    <div class=" alert alert-success" role="alert">Task Gantt Chart

        <button type="button" class="btn btn-primary btn-sm" id="refresh-button" onclick=" drawTaskGanttChart('chart_div')"><i class="fa fa-refresh"></i></button>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Task Gantt Chart</h3>

        </div>
        <div class="panel-body" id="chart-section" style="overflow: scroll">

            <div id="chart_div">

                <h1 class="text-align-center"> Building Graph..
                    <br><i class="fa fa-spinner fa-5x fa-spin"></i></h1><div class="font-lg"></div>


            </div>

        </div>
    </div>



@stop

@section ('local_scripts')

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

        google.charts.load('current', {'packages': ['gantt']});
        google.charts.setOnLoadCallback(drawChart);

        var options = {};
        var screenWidth = 0;
        var screenHeight = 0;
        var chartHeight = 0;
        var serverChartData = [];
        var data = [];
        var activityCount = 0;
        var chart = '';
        function drawTaskGanttChart(chartDiv) {
            chart = new google.visualization.Gantt(document.getElementById(chartDiv));
            chart.clearChart();
            serverDataParsed = JSON.parse(window.sessionStorage.getItem('chartData'));
            //build the array with the data from the server
            for (var i = 0; i < serverDataParsed.length; i++) {
                if (i === 200) {
                    break;
                }
                serverChartData.push(
                        [
                            serverDataParsed[i].id.toString(),
                            serverDataParsed[i].number.toString(),
                            '',
                            new Date(serverDataParsed[i].start_date.split(" ")[0]),
                            new Date(serverDataParsed[i].finish_date.split(" ")[0]),
                            null,
                            0,
                            null
                        ]
                );
                activityCount++;
            }
            console.log(serverChartData);

            //activityCount = activityCount + serverChartData.length;
            screenWidth = $(document).width() - 50;
            screenHeight = $(document).height() + 200;
            console.log(activityCount);
            chartHeight = (activityCount * 35) + 300;//each activity row is about 35 pixels high
            $('#chart-section').height(chartHeight + 150).width(screenWidth + 50);
            options = {
                height: chartHeight,
                width: screenWidth,
                gantt: {
                    criticalPathEnabled: false,
                    innerGridHorizLine: {
                        //stroke: '#ffe0b2',
                        strokeWidth: 1
                    },
                   //defaultStartDateMillis: new Date(2015, 3, 28),
                    percentEnabled: true,
                    barHeight: 15,
                    labelMaxWidth: 400,
                    percentStyle:{
                        fill:'green'
                    },

                    labelStyle: {
                        //fontName: Roboto2,
                        fontSize: 15,
                        color: '#757575'
                    }
                    //innerGridTrack: {fill: '#fff3e0'},
                    //innerGridDarkTrack: {fill: '#ffcc80'}
                }
            };
            drawChart();


            chart.draw(data, options);

        }
        function drawChart() {
            data = new google.visualization.DataTable();
            data.addColumn('string', 'Task ID');
            data.addColumn('string', 'Task Name');
            data.addColumn('string', 'Resource');
            data.addColumn('date', 'Start Date');
            data.addColumn('date', 'End Date');
            data.addColumn('number', 'Duration');
            data.addColumn('number', 'Percent Complete');
            data.addColumn('string', 'Dependencies');
            data.addRows(serverChartData);
        }


        //Functions to set the date
        function toMilliseconds(minutes) {
            return minutes * 60 * 1000;
        }
        function daysToMilliseconds(days) {
            return days * 24 * 60 * 60 * 1000;
        }
        function hoursToDays(hours) {
            var days = hours / 8;
            if (days < 0) {
                return daysToMilliseconds(1);
            } else {
                return daysToMilliseconds(days);
            }
        }

    </script>

    <script type="text/javascript">

        $(window).load(function () {
            var chartData = window.sessionStorage.getItem('chartData');
            console.log('winsow loaded');

            setTimeout(function () {
                drawTaskGanttChart('chart_div');
            }, 1500);


        });
    </script>

@stop

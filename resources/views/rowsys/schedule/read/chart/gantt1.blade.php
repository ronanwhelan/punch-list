@extends ('rowsys._app.layouts.app_admin')

@section ('page_related_css')
    <link rel="stylesheet" href="/css/vendor/jquery-gantt/style.css"/>
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://taitems.github.com/UX-Lab/core/css/prettify.css"/>


    <style type="text/css">
        body {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 13px;
            padding: 0 0 50px 0;
        }

        .contain {
            width: 800px;
            margin: 0 auto;
        }

        h1 {
            margin: 40px 0 20px 0;
        }

        h2 {
            font-size: 1.5em;
            padding-bottom: 3px;
            border-bottom: 1px solid #DDD;
            margin-top: 50px;
            margin-bottom: 25px;
        }

        table th:first-child {
            width: 150px;
        }
    </style>
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
<div class="container">
    <h1>
        jQuery.Gantt
        <small>&mdash; Test Suite 01</small>
    </h1>

    <p>
        <strong>Expected behaviour:</strong> Gantt bar should run from "now" until 2 hours from now. It fails when all the bars are docked left at the hour view.
    </p>

    <p>
        <strong>Manual validation:</strong>
    <ul>
        <li>Passing</li>
    </ul>
    </p>

    <div class="gantt"></div>
</div>
    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop

@section ('local_scripts')
    <script src="/js/plugin/jquery-gantt/jquery.min.js"></script>
    <script src="/js/plugin/jquery-gantt/jquery.fn.gantt.js"></script>
    <script src="/js/plugin/jquery-gantt/moment.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="http://taitems.github.com/UX-Lab/core/js/prettify.js"></script>
    <script>


        var allStats = '<?php echo $data; ?>';
        var data2 = <?php echo json_encode($data );?>;
        var obj = JSON.parse(data2);
        console.log(obj[0]);

        var newData = [];
        var startDate = '';
        var finishDate = '';

        for(var i = 0;i<obj.length;i++){

            startDate = obj[i].start_date.split(" ")[0];

            console.log(startDate);
            newData.push(
                    [
                        obj[i].id.toString(),
                        obj[i].number.toString() + ' ' + obj[i].description.toString(),
                        '',
                        new Date(obj[i].start_date.split(" ")[0]),
                        new Date(obj[i].finish_date.split(" ")[0]),
                        //new Date("2016-04-08"),
                        // new Date(2016, 5, 20),
                        null,
                        45,
                        null
                    ]
            )

        }

        console.log(newData);




        $(function () {

            "use strict";

            var today = moment();
            var andTwoHours = moment().add("hours", 2);

            var today_friendly = "/Date(" + today.valueOf() + ")/";
            var next_friendly = "/Date(" + andTwoHours.valueOf() + ")/";

            var d =  {name: "Testing", desc: " ", values: [{from: today_friendly, to: next_friendly, label: "Test", customClass: "ganttRed"}]};

            var data = [
                { "name": " Step A ","desc": "&rarr; Step B"  ,"values": [{"id": "b0", "from": "/Date(1320182000000)/", "to": "/Date(1320301600000)/", "desc": "Id: 0<br/>Name:   Step A", "label": " Step A", "customClass": "ganttRed", "dep": "b1"}]},
                { "name": " Step B ","desc": "&rarr; step C"  ,"values": [{"id": "b1", "from": "/Date(1320601600000)/", "to": "/Date(1320870400000)/", "desc": "Id: 1<br/>Name:   Step B", "label": " Step B", "customClass": "ganttOrange", "dep": "b2"}]},
                { "name": " Step C ","desc": "&rarr; step D"  ,"values": [{"id": "b2", "from": "/Date(1321192000000)/", "to": "/Date(1321500400000)/", "desc": "Id: 2<br/>Name:   Step C", "label": " Step C", "customClass": "ganttGreen", "dep": "b3"}]},
                { "name": " Step D ","desc": "&rarr; step E"  ,"values": [{"id": "b3", "from": "/Date(1320302400000)/", "to": "/Date(1320551600000)/", "desc": "Id: 3<br/>Name:   Step D", "label": " Step D", "dep": "b4"}]},
                { "name": " Step E ","desc": "&mdash;"        ,"values": [{"id": "b4", "from": "/Date(1320802400000)/", "to": "/Date(1321994800000)/", "desc": "Id: 4<br/>Name:   Step E", "label": " Step E", "customClass": "ganttRed"}]},
                { "name": " Step F ","desc": "&rarr; step B"  ,"values": [{"id": "b5", "from": "/Date(1320192000000)/", "to": "/Date(1320401600000)/", "desc": "Id: 5<br/>Name:   Step F", "label": " Step F", "customClass": "ganttOrange", "dep": "b1"}]},
                { "name": " Step G ","desc": "&rarr; step C"  ,"values": [{"id": "b6", "from": "/Date(1320401600000)/", "to": "/Date(1320570400000)/", "desc": "Id: 6<br/>Name:   Step G", "label": " Step G", "customClass": "ganttGreen", "dep": "b8"}]},
                { "name": " Step H ","desc": "&rarr; Step J"  ,"values": [{"id": "b7", "from": "/Date(1321192000000)/", "to": "/Date(1321500400000)/", "desc": "Id: 7<br/>Name:   Step H", "label": " Step H", "dep": "b9"}]},
                { "name": " Step I ","desc": "&rarr; Step H"  ,"values": [{"id": "b8", "from": "/Date(1320302400000)/", "to": "/Date(1320551600000)/", "desc": "Id: 8<br/>Name:   Step I", "label": " Step I", "customClass": "ganttRed", "dep": "b7"}]},
                { "name": " Step J ","desc": "&mdash;"        ,"values": [{"id": "b9", "from": "/Date(1320802400000)/", "to": "/Date(1321994800000)/", "desc": "Id: 9<br/>Name:   Step J", "label": " Step J", "customClass": "ganttOrange"}]}
            ];



            $(".gantt").gantt({source: data, navigate: 'scroll', scale: 'days', maxScale: 'weeks', minScale: 'hours'});

            $(".gantt").popover({
                selector: ".bar",
                title: "I'm a popover",
                content: "And I'm the content of said popover.",
                trigger: "hover"
            });

        });

    </script>
@stop





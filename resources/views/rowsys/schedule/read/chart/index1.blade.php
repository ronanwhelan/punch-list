@extends ('rowsys._app.layouts.app_admin')

@section ('page_related_css')

@stop

@section ('head_js')

@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="">Systems</label>
                <select class=" form-control selectpicker" onchange="drawTaskGanttChart('chart_div',this.value)" data-style="btn-primary" data-live-search="true" name="system">
                    <option></option>
                    @foreach ($systems as $system)
                        <option value="{{$system->id}}">{{$system->tag}}
                            , {{$system->description}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div id="chart-section" style="height: 3000px;width: 1000px;overflow: scroll">
        <div id="chart_div"></div>
    </div>

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop

@section ('local_scripts')


    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        google.charts.load('current', {'packages': ['gantt']});
        google.charts.setOnLoadCallback(drawChart);
        var options = {};
        var screenWidth = 0;
        var screenHeight = 0;
        var chartHeight = 0;
        var serverChartData = [];
        var data = [];
        var activityCount = 0;
        function drawTaskGanttChart(chartDiv,system_id) {
            var serverDataParsed = '';
            var search = {system:system_id};
            $.ajax({
                dataType: "json",
                url: '/schedule/chart/task-data',
                data: search,
                success: function (result) {
                    serverDataParsed = result;
                    console.log(result);
                    //build the array with the data from the server
                    for (var i = 0; i < serverDataParsed.length; i++) {
                        if (i === 200) { break; }
                        step1Applicable = serverDataParsed[i].gen_applicable;
                        step2Applicable = serverDataParsed[i].gen_applicable;
                        step3Applicable = serverDataParsed[i].gen_applicable;
                        step4Applicable = serverDataParsed[i].gen_applicable;

                        if(step1Applicable === 1){
                            startDate = serverDataParsed[i].gen_td.split(" ")[0];
                        }
                        else if(step2Applicable === 1){
                            startDate = serverDataParsed[i].rev_td.split(" ")[0];
                        }
                        else if(step3Applicable === 1){
                            startDate = serverDataParsed[i].re_issu_td.split(" ")[0];
                        }
                        else if(step4Applicable === 1){
                            startDate = serverDataParsed[i].s_off_td.split(" ")[0];
                        }
                        serverChartData.push(
                                [
                                    serverDataParsed[i].id.toString(),
                                    serverDataParsed[i].number.toString(),
                                    //serverDataParsed[i].system.tag.toString() + ', ' + serverDataParsed[i].stage.name.toString() + ', ' + serverDataParsed[i].task_type.short_name.toString(),
                                    '',
                                    new Date(startDate),
                                    //null,
                                    new Date(serverDataParsed[i].last_td.split(" ")[0]),
                                    hoursToDays(serverDataParsed[i].target_val),
                                    serverDataParsed[i].status,
                                    null
                                ]
                        );
                        activityCount++;
                    }

                    activityCount = activityCount + serverDataParsed.length;
                    screenWidth = $(document).width() - 50;
                    screenHeight = $(document).height() + 200;
                    chartHeight = (activityCount * 35) + 50 ;//each activity row is about 35 pixels high
                    $('#chart-section').height(chartHeight+ 50).width(screenWidth);
                    options = {
                        height:chartHeight,
                        width:screenWidth,
                        gantt: {
                            criticalPathEnabled: false,
                            innerGridHorizLine: {
                                //stroke: '#ffe0b2',
                                strokeWidth: 1
                            },
                            'legend': 'left',
                            //defaultStartDateMillis: new Date(2015, 3, 28),
                            percentEnabled: true,
                            barHeight: 15,
                            labelMaxWidth: 400,
                            labelStyle: {
                                //fontName: Roboto2,
                                fontSize: 15,
                                color: '#757575'
                            }
                            //innerGridTrack: {fill: '#fff3e0'},
                            //innerGridDarkTrack: {fill: '#ffcc80'}
                        }
                    };
                    drawChart();
                    var chart = new google.visualization.Gantt(document.getElementById(chartDiv));
                    chart.clearChart();
                    chart.draw(data, options);
                }
            });

        }
        function drawChart() {
            data = new google.visualization.DataTable();
            data.addColumn('string', 'Task ID');
            data.addColumn('string', 'Task Name');
            data.addColumn('string', 'Resource');
            data.addColumn('date', 'Start Date');
            data.addColumn('date', 'End Date');
            data.addColumn('number', 'Duration');
            data.addColumn('number', 'Percent Complete');
            data.addColumn('string', 'Dependencies');
            data.addRows(serverChartData);
        }


        //Functions to set the date
        function toMilliseconds(minutes) {
            return minutes * 60 * 1000;
        }
        function daysToMilliseconds(days) {
            return days * 24 * 60 * 60 * 1000;
        }
        function hoursToDays(hours) {
            var days = hours / 8;
            if (days < 0) {
                return daysToMilliseconds(1);
            } else {
                return daysToMilliseconds(days);
            }
        }

    </script>

    <script></script>

@stop


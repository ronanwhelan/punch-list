@extends ('rowsys.schedule.includes.layout.layout')

@section ('page_related_css')
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdn.datatables.net/buttons/1.1.2/css/buttons.bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/vendor/datatables/datatables.responsive.css"/>
@stop

@section ('head_js')

@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div id="myTabs" class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Schedule <i class="fa fa-calendar-o"></i></h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div id="myTabs" class="col-lg-12">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Table <i class="fa fa-table"></i></a></li>
                                <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">DB Search <i class="fa fa-search"></i></a></li>
                                <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Add Activity <i class="fa fa-plus-circle"></i></a></li>

                            </ul>
                            <!-- Tab panes -->
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    <div class="">
                                        <div class="row hide">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div id="buttons" class="font-xs"></div>
                                                        <div id="toggle_buttons" class="btn-group pull-right">
                                                            <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                                                                Toggle column <i class="fa fa-caret-down"></i>
                                                            </button>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li>
                                                                    <a class="toggle-vis" data-column="0">Number</a>
                                                                </li>
                                                                <li>
                                                                    <a class="toggle-vis" data-column="1">Description</a>
                                                                </li>
                                                                <li>
                                                                    <a class="toggle-vis" data-column="2">Start Date</a>
                                                                </li>
                                                                <li>
                                                                    <a class="toggle-vis" data-column="3">Finish Date</a>
                                                                </li>
                                                                <li>
                                                                    <a class="toggle-vis" data-column="4">From Scheduler</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row" id="table-section" style="opacity: .08">
                                                    <div class="col-lg-12 font-xs">

                                                        <table id="data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                            <thead>
                                                            <tr class="font-xs">
                                                                <th data-class="expand" class="font-xs">Number</th>
                                                                <th data-hide="phone" class="font-xs">Description</th>
                                                                <th data-hide="phone" class="font-xs">Start date</th>
                                                                <th data-hide="phone" class="font-xs">Finish Date</th>
                                                                <th data-hide="phone,tablet,computer" class="font-xs">From Scheduler</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($scheduleDates as $scheduleDate)
                                                                <tr onclick="showUpdateModelModal('/schedule/update/form/{{$scheduleDate->id}}')">
                                                                    <th scope="row">{{$scheduleDate->number}}</th>
                                                                    <td>{{$scheduleDate->description}}</td>
                                                                    <td>{{$scheduleDate->start_date->format('d-m-Y')}}</td>
                                                                    <td>{{$scheduleDate->finish_date->format('d-m-Y')}}</td>
                                                                    @if($scheduleDate->imported_data === 1)
                                                                        <td>Yes</td>
                                                                    @else
                                                                        <td>No</td>
                                                                    @endif
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <div class="panel-body">
                                        <div class="col-lg-8">
                                            <form action="/schedule-date/search" method="post">
                                                {{ csrf_field() }}
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="chk_imported_data" type="checkbox"> Include Imported Data
                                                    </label>
                                                </div>
                                                <div class="input-group input-group-lg">
                                                    <input type="text" name="search_text" class="form-control"
                                                           placeholder="Search for...">
                                                    <span class="input-group-btn"><button class="btn btn-primary" type="submit">Search!</button></span>
                                                </div>




                                                <!-- /input-group -->
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="messages">
                                    <div class="panel">
                                        <div class="col-md-12">
                                            <form action="/schedule-date/add" method="post" id="add-activity-form">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    @if (count($errors) > 0)
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Number/ID</label>
                                                            <input type="text" class="form-control" name="new_number" placeholder="">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Description</label>
                                                            <input type="text" class="form-control" name="new_description" placeholder="">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Start Date</label>
                                                            <input type="date" id="search-from-date" name="new_start_date" class=" datepicker"
                                                                   data-date-format="yyyy-mm-dd"
                                                                   value="{{\Carbon\Carbon::now()->toDateString()}}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Finish Date</label>
                                                            <input type="date" id="search-from-date" name="new_finish_date" class=" datepicker"
                                                                   data-date-format="yyyy-mm-dd"
                                                                   value="{{\Carbon\Carbon::now()->toDateString()}}">
                                                        </div>

                                                        <!-- SERVER AND VALIDATION ERRORS -->
                                                        <div class="alert alert-info fadeIn hide " id="validation-errors"></div>
                                                        <br>
                                                        <div class="form-group">
                                                        <button type="button" onclick="addNewActivity()" class="btn btn-success btn-block ">Add</button>
                                                            </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('rowsys.project.schedule.update.modal')
    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop

@section ('local_scripts')

    <script>
        var ganttChartData = <?php echo json_encode($scheduleDates );?>;
    </script>

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

    <script>
        var table = $('#data_table');
        $(document).ready(function () {
            var screenHeight = $(document).height();
            screenHeight = (screenHeight - 600) + "px";
            var responsiveHelper;
            var breakpointDefinition = {
                computer: 2000,
                tablet: 1024,
                phone: 480
            };

            $('.datepicker').datepicker();

            table = $('#data_table').DataTable({
                dom: 'Bfrtip',
                buttons: ['copy', 'excel'],
                //"sDom":"flrtip",
                "bFilter": true,
                //"bInfo": true,
                //"bLengthChange": true,
                "bAutoWidth": true,
                fixedHeader: true,
                "pageLength": 100,
                //"bPaginate": true,
                //"aaSorting": [[ 3, "ASC" ]],
                "aaSorting": [],
                "scrollY": "900px",
                //"scrollX": "500px",
                //"paging": false,
                responsive: true,
                // paging: false,
                "bStateSave": true, // saves sort state using localStorage
                autoWidth: false,
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(table, breakpointDefinition);
                    }
                },
                rowCallback: function (nRow) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                drawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }

            });
            // Apply the filter
            $("#example thead th input[type=text]").on('keyup change', function () {
                table.column($(this).parent().index() + ':visible').search(this.value).draw();
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: [//'excel'//'copyHtml5',//'excelHtml5',//'csvHtml5',//'pdfHtml5'
                ]
            }).container().appendTo($('#buttons'));
            table.buttons().container().appendTo($('.col-sm-6:eq(0)', table.table().container()));

            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = table.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });

            $('#table-section').fadeTo(1500, '1');
            $('#loading-box-section').remove();

            //--------------------------------------------------------
            //      TOGGLE BUTTONS
            //--------------------------------------------------------
            //Add Toggle Buttons to top of table in the #_filter section
            var toggleButtons = $('#toggle_buttons');
            var tableFilterSection = $('#data_table_filter');

            var dtButtons = $('.dt-buttons');
            dtButtons.addClass('hidden-xs hidden-sm hidden-md ');
            //tableFilterSection.append('<div class="">Task List</div>');
            tableFilterSection.append('&nbsp;&nbsp;').append(toggleButtons);

            //--------------------------------------------------------
            //      EXPORT BUTTONS
            //--------------------------------------------------------
            tableFilterSection.append('&nbsp;&nbsp;').append(
                    '<div class="btn-group hidden-xs hidden-sm"> <button type="button" ' +
                    'class="btn btn-primary dropdown-toggle" ' +
                    'data-toggle="dropdown" aria-haspopup="true" ' +
                    'aria-expanded="false"> Export <span class="caret"></span> </button> <ul class="dropdown-menu"> ' +
                    '<li id="export-buttons" style="padding: 10px;"><a href="#"></a></li> ' +
                        //'<li><a href="#">Excel</a></li> ' +
                        //'<li><a href="#">Something else here</a>' +
                    '</li> </ul> </div>').append('&nbsp;&nbsp;');
            /* tableFilterSection.append('&nbsp;&nbsp;').append('<button class="btn btn-primary hidden-sm hidden-xs" ' +
             'role="button" data-toggle="collapse" data-parent="#search" ' +
             'href="#search-section" aria-expanded="true" aria-controls="search-section">Search ' +
             '<i class="fa fa-search"></i> </button>').append('&nbsp;&nbsp;');*/

            //Gantt Chart Button
            tableFilterSection.append('&nbsp;&nbsp;').append('<button class="btn btn-primary hidden-sm hidden-xs" ' +
            'onclick="drawScheduleGanttChart()"> <i class="fa fa-tasks"></i> Gantt</button>');

            var exportButton1 = $('#export-button-1');
            var exportButtons = $('#export-buttons');
            var copyButton = $('.buttons-copy');
            var excelButton = $('.buttons-excel');
            exportButtons.append('&nbsp;&nbsp;').append(dtButtons);
            //exportButton2.append(excelButton);

            var tablePaginateSection = $('#data_table_paginate');
            //tablePaginateSection.css("background-color", "navy");
        });


        /** ===============================================================
         *
         *  Add new Activity over AJAX
         *
         * ===============================================================
         */
        function addNewActivity() {
            console.log('Add new activity');
            //a token is needed to prevent cross site
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var form = $('#add-activity-form');
            var method = form.find('input[name="_method"]').val() || 'POST';
            var datastring = $(form).serialize();
            var validationSection = $('#validation-errors');

            $.ajax({
                type: 'POST',
                url: '/schedule-date/add',
                data: datastring,
                success: function (result) {

                    console.log(result);
                    $.notify({
                        title: '<strong>Success!</strong>',
                        message: result.message
                    },{
                        type: 'success',
                        offset: {
                            x: 50,
                            y: 100
                        },
                        showProgressbar: false,
                        delay:1000
                    });
                    validationSection.addClass('hide');
                   // location.reload();
                },
                beforeSend: function () {
                    //pleaseWait('Adding Tasks');
                    // Handle the beforeSend event

                },
                complete: function () {
                    // Handle the complete event
                    //waitDone();
                    //

                },
                // Handles the Errors if returned back from the Server -
                // Including any Validation Errors
                error: function (xhr, status, error) {

                    console.log(xhr);
                    console.log('-----');
                    console.log(status);
                    console.log('-----');
                    console.log(error);

                    validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Validation Errors</strong><br><br>');
                    var errorText = '';//'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                    var validationErrorText = '';
                    var list = $("<ul class=''></ul>");
                    var contactAdminText = ' - Contact the PES Administrator';

                    // If there is a Validation Error then show the user the validation error Text
                    if (error === 'Unprocessable Entity' || xhr.status === 422) {
                        validationSection.append(validationErrorText);
                        jQuery.each(xhr.responseJSON, function (i, val) {
                            validationErrorText = '<li>' + val + '</li>';
                            list.append(validationErrorText);
                        });
                    } else {
                        errorText = errorText + contactAdminText;
                    }
                    validationSection.removeClass('hide').append(list).append('<br>' + errorText);
                    //console.log(xhr);

                }
            });

        }


    </script>

@stop


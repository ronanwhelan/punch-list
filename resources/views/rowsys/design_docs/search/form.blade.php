<input type="hidden" name="search_active" value="search_active">
<div class="row">
    <div class="col-lg-12">
        <div class="form-group ">
            <input type="text" class="form-control input-lg" id="search-number" placeholder="search task description" name="search_number">
            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker"
                    id="search-area" name="search_areas[]"
                    data-style="btn-primary" multiple title="Area"
                    onchange="getFilteredSystemsByArea('search')">
                <optgroup label="Areas">
                    @foreach ($areas as $area)
                        @if(in_array($area->id,$selectedAreasArray))
                            <option selected value="{{$area->id}}">{{$area->name}}</option>
                        @else
                            <option value="{{$area->id}}">{{$area->name}}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker " id="search-system" name="search_systems[]" data-live-search="true" data-style="btn-primary" multiple title="System">
                <optgroup label="Systems">
                    @foreach ($systems as $system)
                        @if(in_array($system->id,$selectedSystemsArray))
                            <option selected class="" value="{{$system->id}}">{{$system->tag}}, {{$system->description}}</option>
                        @else
                            <option class="" value="{{$system->id}}">{{$system->tag}}, {{$system->description}}</option>
                        @endif

                    @endforeach
                </optgroup>
            </select>
            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
        </div>
    </div>
</div>



<div class="row">
    <div class="col-lg-12"><h4>Choose a time:</h4></div>
    <div class="col-lg-12">
        <div class="form-group">
            <select class="selectpicker form-control show-tick" id="search-time" name="search_time" data-style="btn-primary"  data-max-options="1" title="Choose Time">
                <option value="1" selected>Anytime</option>
                <option value="2">Due within 7 Days</option>
                <option value="3">Due within 2 weeks</option>
                <option value="4">Due within 1 Month</option>
                <option value="5">Past Due</option>
                <option value="6">Date Range</option>
            </select>
        </div>
    </div>
</div>
<div id="search-date-range-section" hidden>
    <div class="row">
        <div class="col-lg-12"><h4>Date Range:</h4></div>
        <div class="col-lg-12">
            <div class="input-group input-daterange">
                <input type="date" id="search-from-date" name="search_from_date" class=" datepicker"
                       data-date-format="yyyy-mm-dd"
                       value="{{\Carbon\Carbon::now()->toDateString()}}">

                TO
                <span class=" hide input-group-addon">to</span>

                <input type="date" id="search-to-date" name="search_to_date" class=" datepicker"
                       data-date-format="yyyy-mm-dd"
                       value="{{\Carbon\Carbon::now()->addMonths(6)->toDateString()}}">
            </div>
        </div>
    </div>
    <br>
</div>


<div class="row">
    <div class="col-lg-12"><h4>Sort By:</h4></div>
    <div class="col-lg-12">
        <div class="form-group">
            <select class="selectpicker form-control show-tick" id="search-order-by" data-style="btn-primary"  name="search_order_by" data-max-options="1" title="order By">
                <option value="1" selected>Next Target Date</option>
                <option value="2">Task Number</option>
                <option value="3">Area Name</option>
                <option value="4">System Tag</option>
                <option value="5">Task Group</option>
                <option value="6">Task Type</option>
                <option value="7">Stage</option>
            </select>
        </div>
    </div>
</div>


<div class="row pad-20">
    <div class="col-lg-12"><h4>Search Results:</h4></div>
    <div class="col-lg-12">
        <table class="table table-responsive table-bordered">
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<script>


    $("#search-task-number").blur(function(){
       // getSearchStats();
    });

//.autocomplete("option", "appendTo", "#copy_dialog");
    $('#search-task-number').keyup(function () {
        //getSearchStats();
/*        vartagObj = $('#search-task-number');
        var tag = vartagObj.val();
        var source = "/get-task-number-autocomplete-list/" + tag;
        vartagObj.autocomplete({
            source: source,
            minLength: 1,
            //autoOpen: true,
            //width: 500,
            //modal: false,
            //zIndex: 10000000,
            //appendTo: "search-task-number",
            //title: 'Duplicate',
            select: function (event, ui) {
                //vartagObj.val(ui.item.value);
                console.log(ui.item.id);
            }
        });*/
    });


    $(document).ready(function () {
        checkIfOnMobile();
        //getSearchStats();
        $('.datepicker').datepicker({
            keyboardNavigation: true,
            calendarWeeks: true,
            autoclose: true
        });

        $(document).ready(function () {
            if (screen.width <= 480) {
                $('.selectpicker').selectpicker('mobile');
                $('.datepicker').datepicker('mobile');
                //$('#screenSize').val('2');
            }
        });
    });

    //$(selectpickerButton).trigger('click');

    $("#search-time").change(function () {
        if ($('#search-time :selected').val() > 5) {
            $('#search-date-range-section').fadeIn(1500);
            console.log('more than 5');
        } else {
            $('#search-date-range-section').fadeOut(1500);
            console.log('less than 5');
        }
        $("#search-time").trigger('click');

    });





    /*===============================================
     /         TASKS- AUTO COMPLETE
     =================================================*/
    /*$("#search-task-number").keyup(function () {
        getSearchStats();
        *//*var numberTextField = $('#search-task-number');
        var tag = numberTextField.val();
        $(function () {
            function log(message) {
                console.log('function called');
                $("<div>").text(message).prependTo("#log");
                $("#log").scrollTop(0);
            }


            console.log('autocomplete called.');
            $("#search-task-number").autocomplete({
                source: "/get-task-number-autocomplete-list/" + tag,
                minLength: 3,
                select: function (event, ui) {
                    log(ui.item ?
                    "Selected: " + ui.item.value + " aka " + ui.item.id :
                    "Nothing selected, input was " + this.value);

                }

            });






        });
        //When the text field looses focuus ,let the modal get focus
        numberTextField.focusout(function () {
            $('#user-filter-modal').focus();
        }).blur(function () {
            blur++;
            $('#user-filter-modal').focus();
        });*//*
    });*/
</script>
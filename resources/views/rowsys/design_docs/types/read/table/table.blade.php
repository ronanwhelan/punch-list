<div class="">
    <div class="row hide">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div id="buttons" class="font-xs"></div>
                    <div id="toggle_buttons" class="btn-group pull-right">
                        <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="0">Col 1</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="1">Col 2</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="2">Col 3</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">Col 4</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="4">Col 5</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Col 6</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="6">Col 7</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="7">Col 8</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="8">Col 9</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="9">Col 10</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Table</div>
        <div class="panel-body">
            <div class="row" id="table-section" style="opacity: .08">
                <div class="col-lg-12 font-xs">

                    <table id="data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr class="font-xs">
                            <th data-class="expand" class="font-xs">Col 1</th>
                            <th data-hide="phone,tablet" class="font-xs">Col 2</th>
                            <th data-hide="phone,tablet" class="font-xs">Col 3</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Col 4</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Col 5</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Col 6</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Col 7</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Col 8</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Col 9</th>
                            <th data-hide="phone,tablet,computer" class="font-xs">Col 10</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- Insert For each to populate table--}}
                        @for($i=0;$i<10;$i++)
                            <tr onclick="showUpdateModelModal('/design-doc/types/1')">
                                <td>Col 1</td>
                                <td>Col 2</td>
                                <td>Col 3</td>
                                <td>Col 4</td>
                                <td>Col 5</td>
                                <td>Col 6</td>
                                <td>Col 7</td>
                                <td>Col 8</td>
                                <td>Col 9</td>
                                <td>Col 10</td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

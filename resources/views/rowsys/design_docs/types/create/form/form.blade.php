<div class="row">
    <div class="col-lg-12"><h3>Types of Design Docs</h3></div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Add New Design Document Type </h3></div>
            <div class="panel-body">
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="fa fa-check"> </i>{{ Session::get('message') }}
                    </div>
                @endif
                <form action="/design-doc/types" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control" id="" name="name" value="{{old('add_name')}}" placeholder="e.g Automation">
                    </div>
                    <div class="form-group">
                        <label for="">Short Name</label>
                        <input type="text" class="form-control" id="" name="short_name" value="{{old('add_short_name')}}" placeholder="e.g AUTO,COMM">
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <input type="text" class="form-control" id="" name="description" value="{{old('add_description')}}" placeholder="Description">
                    </div>
                    @if ($errors->has() )
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

                    <button type="submit" class="btn btn-success ">Add <i class="fa fa-plus-circle"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>

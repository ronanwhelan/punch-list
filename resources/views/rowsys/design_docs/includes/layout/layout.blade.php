<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <!-- Common Head Data  -->
    @include('rowsys._app.includes.structure.head.head')

    <!-- Common Title  -->
    @include('rowsys._app.includes.structure.title.title')

    <!-- Head Scripts  -->
    @include('rowsys._app.includes.styles.styles')
    @include('rowsys.design_docs.includes.styles.module')

    <!-- PAGE RELATED - CSS  -->
    @yield('page_related_css')

    <!-- Head Scripts  -->
    @include('rowsys._app.includes.scripts.head.head_scripts')

    <!-- PAGE RELATED - HEAD JS  -->
    @yield('head_js')

</head>

<body id="body" class="">

<!-- MAIN HEADER -->
@include('rowsys.design_docs.includes.header.header')

<!-- SIDE NAV-->
@include('rowsys.design_docs.includes.navbar.navbar')

<!-- CONTENT -->

<section class="main-content" id="main-content">
    <div class="content-wrapper">
        <!--Search Modal -->
        @include('rowsys.design_docs.search.modal')

        <!-- PUT CONTENT HERE -->
        @yield('content')

        <!-- RIGHT SIDE BAR -->
        @include('rowsys._app.includes.right_side_bar.right_side_bar')

        <!--  FOOTER -->
        @include('rowsys._app.includes.footer.footer')


    </div>
</section> <!-- .main-content -->
<!--  User Settings -->
@include('rowsys.admin.users.profile.settings_modal')
<div>
    <a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>
</div>

<!-- GLOBAL SCRIPTS -->
@include('rowsys._app.includes.scripts.global.global_scripts')

@include('rowsys.design_docs.includes.scripts.module')

<!-- PUT LOCAL SCRIPTS HERE -->
@yield('local_scripts')

</body>

</html>
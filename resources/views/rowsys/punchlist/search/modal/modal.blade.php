<!-- Modal -->
<form id="search-items-form" action="/items" method="get">
    <input type="hidden" id="go-to-task-parameters-view" name="goToTaskParametersView" value="0">
    <div class="modal fade bs-example-modal-lg" id="item-search-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Search Items &nbsp;   <i onclick="resetItemSearchModalDropDown()" class="fa fa-undo"></i></h4>
                </div>
                <div class="modal-body">
                    <div id="search-options-section"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="show-task-button" class="btn btn-success pull-right"> Search <i class="fa fa-table"></i></button>
                </div>
            </div>
        </div>
    </div>
</form>
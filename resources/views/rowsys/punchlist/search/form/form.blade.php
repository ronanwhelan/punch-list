<input type="hidden" name="search_active" value="search_active">
<div class="row">
    <div class="col-lg-12">
        <div class="form-group ">
            <input type="text" class="form-control input-lg" id="search-number" placeholder="start typing to search" name="search_number">

            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
        </div>
    </div>
</div>

<div class="row">
{{--    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker"
                    id="search-area" name="search_areas[]"
                    data-style="btn-primary" multiple title="Area"
                    onchange="getFilteredSystemsByArea('search')">
                <optgroup label="Areas">
                    @foreach ($areas as $area)
                        @if(in_array($area->id,$selectedAreasArray))
                            <option selected value="{{$area->id}}">{{$area->name}}</option>
                        @else
                            <option value="{{$area->id}}">{{$area->name}}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>--}}
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker " id="search-system" name="search_systems[]" data-style="btn-primary" data-live-search="true" multiple title="System">
                <optgroup label="Systems">
                    @foreach ($systems as $system)
                        @if(in_array($system->id,$selectedSystemsArray))
                            <option selected class="" value="{{$system->id}}">{{$system->name}}, {{$system->description}}</option>
                        @else
                            <option class="" value="{{$system->id}}">{{$system->name}}, {{$system->description}}</option>
                        @endif

                    @endforeach
                </optgroup>
            </select>
            <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker" id="search-group" name="search_groups[]"
                    data-style="btn-primary" multiple title="Group"
                    onchange="getFilteredItemTypesByGroup('search')">
                <optgroup label="Task Groups">
                    @foreach ($groups as $group)
                        @if(in_array($group->id,$selectedGroupsArray))
                            <option selected value="{{$group->id}}">{{$group->name}}</option>
                        @else
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <select class="form-control selectpicker" id="search-type" name="search_types[]" data-style="btn-primary" multiple title="Type">
                <optgroup label="Task Types">
                    @foreach ($types as $type)
                        @if(in_array($type->id,$selectedTypesArray))
                            <option selected value="{{$type->id}}">{{$type->name}}  {{$type->description}}</option>
                        @else
                            <option value="{{$type->id}}">{{$type->name}} {{$type->description}}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <select class="form-control selectpicker" id="search-raised-by" name="search_raised_by" data-style="btn-primary"  title="Raised By">
                <optgroup label="Raised By">
                    @foreach ($ownerRoles as $role)
                        <option value="{{$role->id}}">{{$role->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <select class="form-control selectpicker" id="search-responsible" name="search_responsible" data-style="btn-primary"  title="Responsible Party">
                <optgroup label="Responsible">
                    @foreach ($resRoles as $role)
                        <option value="{{$role->id}}">{{$role->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <select class="form-control selectpicker" id="search-assignee" name="search_assignee" data-style="btn-primary"  title="Assignee Party">
                <optgroup label="Assignee">
                    @foreach ($assignedRoles as $role)
                        <option value="{{$role->id}}">{{$role->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <select class="form-control selectpicker" id="search_category" name="search_category" data-style="btn-primary" title="Category">
                <optgroup label="Categories">
                    <option value=""></option>
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <select class="form-control selectpicker" id="search_priority" name="search_priority" data-style="btn-primary" title="Priority">
                <optgroup label="Categories">
                    <option value=""></option>
                    @foreach ($priorities as $priority)
                        <option value="{{$priority->id}}">{{$priority->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>


    <div class="col-md-4">
        <label class="radio-inline">
            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="all"> All
        </label>
        <label class="radio-inline">
            <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="done"> Closed
        </label>
        <label class="radio-inline">
            <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="not-done" checked> Open
        </label>

    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12"><h4>Activation Status:</h4></div>
    <div class="col-lg-12">
        <div class="form-group">
            <select class="selectpicker form-control show-tick" id="search_activated_status" name="search_activated_status" data-style="btn-primary" data-max-options="1" title="Choose Time">
                <option value="0">Proposed</option>
                <option value="1" selected>Accepted</option>
                <option value="2">Rejected</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12"><h4>Choose a time:</h4></div>
    <div class="col-lg-12">
        <div class="form-group">
            <select class="selectpicker form-control show-tick" id="search-time" name="search_time" data-style="btn-primary" data-max-options="1" title="Choose Time">
                <option value="1" selected>Anytime</option>
                <option value="2">Due within 7 Days</option>
                <option value="3">Due within 2 weeks</option>
                <option value="4">Due within 1 Month</option>
                <option value="5">Past Due</option>
                <option value="6">Date Range</option>
            </select>
        </div>
    </div>
</div>
<div id="search-date-range-section" hidden>
    <div class="row">
        <div class="col-lg-12"><h4>Date Range:</h4></div>
        <div class="col-lg-12">
            <div class="input-group input-daterange">
                <input type="date" id="search-from-date" name="search_from_date" class=" datepicker"
                       data-date-format="yyyy-mm-dd"
                       value="{{\Carbon\Carbon::now()->toDateString()}}">

                TO
                <span class=" hide input-group-addon">to</span>

                <input type="date" id="search-to-date" name="search_to_date" class=" datepicker"
                       data-date-format="yyyy-mm-dd"
                       value="{{\Carbon\Carbon::now()->addMonths(6)->toDateString()}}">
            </div>
        </div>
    </div>
    <br>
</div>


<div class="row">
    <div class="col-lg-12"><h4>Sort By:</h4></div>
    <div class="col-lg-12">
        <div class="form-group">
            <select class="selectpicker form-control show-tick" id="search-order-by" data-style="btn-primary" name="search_order_by" data-max-options="1" title="order By">
                <option value="1" selected>Due Soon</option>
                <option value="2">Item Number</option>
                <option value="3">Area Name</option>
                <option value="4">System Tag</option>
                <option value="5">Task Group</option>
                <option value="6">Task Type</option>
                <option value="7">Priority</option>
            </select>
        </div>
    </div>
</div>


<div class="row pad-20 ">
    <div class="col-lg-12"><h4>Search Results:</h4></div>
    <div class="col-lg-12">
        <table class="table table-responsive table-bordered">
            <tbody>

            <tr>
                <td scope="row"><span class="label label-success font-sm" id="search-stat-total">{{$stats['total']}}</span> Items</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-info font-sm" id="search-stat-complete-percent">{{$stats['completePercent']}}</span>% Complete</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-info font-sm" id="search-stat-complete">{{$stats['complete']}}</span> Complete</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-danger font-sm" id="search-stat-past-due">{{$stats['pastDue']}}</span> Past Due Date</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-primary font-sm" id="search-stat-seven-days">{{$stats['dueInSevenDays']}}</span> Due within 7 days</td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-primary font-sm" id="search-stat-fourteen-days">{{$stats['dueInFourteenDays']}}</span> Due within 14 Days
                </td>
            </tr>
            <tr>
                <td scope="row"><span class="label label-primary font-sm" id="search-stat-one-month">{{$stats['dueInOneMonth']}}</span> Due within 1 Month:
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        console.log('documentReady');
        checkIfOnMobile();
        //getItemSearchStats();
        $('.datepicker').datepicker({
            keyboardNavigation: true,
            calendarWeeks: true,
            autoclose: true
        });

        if (screen.width <= 480) {
            $('.selectpicker').selectpicker('mobile');
            $('.datepicker').datepicker('mobile');
            //$('#screenSize').val('2');
        }

    });


    $("#search-number").blur(function () {
        getItemSearchStats();
    });

    //.autocomplete("option", "appendTo", "#copy_dialog");
    $('#search-number').keyup(function () {
        getItemSearchStats();
        vartagObj = $('#search-number');
        var tag = vartagObj.val();
        var source = "/get-item-number-autocomplete-list/" + tag;
        vartagObj.autocomplete({
            source: source,
            minLength: 1,
            //autoOpen: true,
            //width: 500,
            //modal: false,
            zIndex: 10000000,
            //appendTo: "search-task-number",
            //title: 'Duplicate',
            select: function (event, ui) {
                vartagObj.val(ui.item.value);
                console.log(ui.item.id);
            }
        });
    });


    //$(selectpickerButton).trigger('click');

    $("#search-time").change(function () {
        if ($('#search-time :selected').val() > 5) {
            $('#search-date-range-section').fadeIn(1500);
            console.log('more than 5');
        } else {
            $('#search-date-range-section').fadeOut(1500);
            console.log('less than 5');
        }
        $("#search-time").trigger('click');

    });

    /*===============================================
     /         TASKS- AUTO COMPLETE
     =================================================*/
    $("#search-task-number").keyup(function () {
        getItemSearchStats();
        var numberTextField = $('#search-task-number');
        var tag = numberTextField.val();
        $(function () {
            function log(message) {
                console.log('function called');
                $("<div>").text(message).prependTo("#log");
                $("#log").scrollTop(0);
            }

            $("#search-task-number").autocomplete({
                source: "/get-task-number-autocomplete-list/" + tag,
                minLength: 3,
                select: function (event, ui) {
                    log(ui.item ?
                    "Selected: " + ui.item.value + " aka " + ui.item.id :
                    "Nothing selected, input was " + this.value);
                    console.log('autocomplete called.');
                }
            });
        });
        //When the text field looses focuus ,let the modal get focus
        numberTextField.focusout(function () {
            $('#user-filter-modal').focus();
        }).blur(function () {
            blur++;
            $('#user-filter-modal').focus();
        });
    });
</script>
@extends ('rowsys._app.layouts.attachment')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')

    <!-- ========================== CONTENT STARTS HERE ========================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">{{$item->number}}</h3></div>
                <div class="panel-body">
                    <h5>{{$item->description}}</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 ">
            <div class="gallery-filter">
                <a class="current" data-filter="*" href="#">All</a>
                <a class="" data-filter=".image" href="#">Images</a>
                <a class="" data-filter=".pdf" href="#">PDF</a>
            </div>
        </div>
    </div>

    <div class="row">


        <div class="gallery-container pad-20">
            @if(count($imageList) > 0)
                @foreach($imageList as $image)

                    <div class="col-sm-6 col-lg-3 col-md-4 gallery image">
                        <div class="gal-detail thumb">
                            link = {{$awsLink}}{{$image->folder}}{{$image->path}}<br>
                            <a href="{{$awsLink}}{{$image->folder}}{{$image->path}}" class="img-popup">
                                <img src="{{$awsLink}}{{$image->folder}}{{$image->path}}" class="img-responsive">
                            </a>

                            <h3 class="hide">{{$image->path}}</h3>
                            <p class="font-xs">Uploaded by {{$image->uploadedBy->name}}  {{$image->uploadedBy->surname}}</p>

                            <p>{{$image->tag}}.</p>
                        </div>
                    </div>

                @endforeach
            @endif


            @if(count($pdfList) > 0)
                @foreach($pdfList as $pdf)
                    <div class="col-sm-12 col-lg-12 col-md-12 gallery pdf">
                        <div class="gal-detail thumb">
                       {{--    <a href="{{$awsLink}}{{$pdf->folder}}{{$pdf->path}}" class="img-popup">
                                <img src="{{$awsLink}}{{$pdf->folder}}{{$pdf->path}}" class="img-responsive">
                            </a>
                            <br>--}}

                            <object data="{{$awsLink}}{{$pdf->folder}}{{$pdf->path}}" type="application/pdf" width="100%" height="600px">
                                <p>Alternative text - include a link <a href="{{$awsLink}}{{$pdf->folder}}{{$pdf->path}}">to the PDF!</a></p>
                            </object>



                            <h3 class="">{{$pdf->path}}</h3>
                            <p class="font-xs">Uploaded by {{$pdf->uploadedBy->name}} {{$pdf->uploadedBy->surname}}</p>

                            <p>{{$pdf->tag}}.</p>
                        </div>
                    </div>

                @endforeach
            @endif
        </div>
    </div>

    <!-- ========================== CONTENT ENDS HERE ========================== -->

@stop

@section ('local_scripts')

    <script src="/js/plugin/jquery.magnific-popup.min.js"></script>
    <script src="/js/plugin/isotope.pkgd.min.js"></script>
    <script>
        /* --------------------------------
         Gallery Pop
         -------------------------------- */
        $('.img-popup').magnificPopup({
            type: 'image'
        });</script>

    <script type="text/javascript">

        $(window).load(function () {
            var $container = $('.gallery-container');
            $container.isotope({
                filter: '*',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

            $('.gallery-filter a').click(function () {
                $('.gallery-filter .current').removeClass('current');
                $(this).addClass('current');

                var selector = $(this).attr('data-filter');
                $container.isotope({
                    filter: selector,
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                });
                return false;
            });


        });
    </script>

@stop

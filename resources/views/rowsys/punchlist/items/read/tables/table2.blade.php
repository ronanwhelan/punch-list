<div class="">
    <div class="dt-buttons hide">

        <a class="dt-button buttons-copy buttons-html5" tabindex="0" aria-controls="mission_list"><span>Copy</span></a>
        <a class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="mission_list"><span>CSV</span></a>
        <a class="dt-button buttons-excel buttons-flash" tabindex="0" aria-controls="mission_list">
            <span>Excel</span>

            <div style="position: absolute; left: 0px; top: 0px; width: 52px; height: 28px; z-index: 99;">
                <embed id="ZeroClipboard_TableToolsMovie_1"
                       src="//cdn.datatables.net/buttons/1.0.0/swf/flashExport.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="52"
                       height="28"
                       name="ZeroClipboard_TableToolsMovie_1"
                       align="middle" allowscriptaccess="always" allowfullscreen="false" type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer"
                       flashvars="id=1&amp;width=52&amp;height=28" wmode="transparent">
            </div>
        </a>

        <a class="dt-button buttons-pdf buttons-flash" tabindex="0" aria-controls="mission_list"><span>PDF</span>

            <div style="position: absolute; left: 0px; top: 0px; width: 47px; height: 28px; z-index: 99;">
                <embed id="ZeroClipboard_TableToolsMovie_2" src="//cdn.datatables.net/buttons/1.0.0/swf/flashExport.swf" loop="false" menu="false"
                       quality="best" bgcolor="#ffffff" width="47" height="28" name="ZeroClipboard_TableToolsMovie_2" align="middle" allowscriptaccess="always"
                       allowfullscreen="false" type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="id=2&amp;width=47&amp;height=28" wmode="transparent">
            </div>
        </a>

        <a class="dt-button buttons-print" tabindex="0" aria-controls="mission_list"><span>Print</span></a>

        <a class="dt-button buttons-excel buttons-flash" tabindex="0" aria-controls="mission_list"><span>Save current page</span>

            <div style="position: absolute; left: 0px; top: 0px; width: 115px; height: 28px; z-index: 99;">
                <embed id="ZeroClipboard_TableToolsMovie_3" src="//cdn.datatables.net/buttons/1.0.0/swf/flashExport.swf"
                       loop="false" menu="false" quality="best" bgcolor="#ffffff" width="115" height="28" name="ZeroClipboard_TableToolsMovie_3" align="middle"
                       allowscriptaccess="always" allowfullscreen="false"
                       type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"
                       flashvars="id=3&amp;width=115&amp;height=28"
                       wmode="transparent">
            </div>
        </a>

        <a class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="mission_list"><span>Copy all data</span></a>

        <a class="dt-button buttons-pdf buttons-flash" tabindex="0" aria-controls="mission_list"><span>PDF</span>

            <div style="position: absolute; left: 0px; top: 0px; width: 47px; height: 28px; z-index: 99;">
                <embed id="ZeroClipboard_TableToolsMovie_4" src="//cdn.datatables.net/buttons/1.0.0/swf/flashExport.swf"
                       loop="false" menu="false" quality="best" bgcolor="#ffffff" width="47" height="28" name="ZeroClipboard_TableToolsMovie_4"
                       align="middle" allowscriptaccess="always" allowfullscreen="false" type="application/x-shockwave-flash"
                       pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="id=4&amp;width=47&amp;height=28" wmode="transparent">
            </div>
        </a>
    </div>


    <div class="row hide">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div id="buttons" class="font-xs"></div>

                    <div id="toggle_buttons" class="btn-group pull-right">
                        <button class="btn dropdown-toggle btn-xs btn-info" data-toggle="dropdown">
                            Toggle column <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a class="toggle-vis" data-column="1">Details</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="2">Location</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="3">Due Date</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="4">Responsible</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="5">Assigned</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="6">Status</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="7">Category</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="8">Priority</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="9">Area</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="10">System</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="11">Site</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="12">Group</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="13">Type</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="14">Raised By Team</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="15">Raised By</a>
                            </li>
                            <li>
                                <a class="toggle-vis" data-column="16">Raised Date</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="panel-body">
        <div class="row" id="table-section" style="opacity: .08">
            <div class="col-lg-12 font-xs">

                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr class="">
                        <th data-class="expand" class="font-xs">Number</th>
                        <th data-class="phone" class="font-xs">Details</th>

                        <th data-hide="phone" class="font-xs">System/Location</th>
                        <th data-hide="phone" class="font-xs">Due Date</th>
                        <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Responsible</span></th>
                        <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Assigned</span></th>
                        <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Status<br></span></th>


                        <th data-hide="phone,tablet,computer" class="font-xs">Class</th>
                        <th data-hide="phone,tablet,computer" class="font-xs">Priority</th>
                        <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                        <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                        <th data-hide="phone,tablet,computer" class="font-xs">Site</th>
                        <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
                        <th data-hide="phone,tablet,computer" class="font-xs">Type</th>
                        <th data-hide="phone,tablet,computer" class="font-xs"><span class="txt-color-orange">Raised By<br> Team</span></th>
                        <th data-hide="phone,tablet,computer" class="font-xs"><span class="txt-color-orange">Raised By</span></th>
                        <th data-hide="phone,tablet,computer" class="font-xs"><span class="txt-color-orange">Raised Date</span></th>
                    </tr>
                    </thead>
                    {{--                <tfoot>
                                    <tr class="font-xs">
                                        <th data-class="expand" class="font-xs">Number</th>
                                        <th data-class="phone" class="font-xs">Details</th>
                                        <th data-hide="phone" class="font-xs">System/Location</th>
                                        <th data-hide="phone" class="font-xs"><br>Due Date</th>
                                        <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Responsible</span></th>
                                        <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-orange">Assigned</span></th>
                                        <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Status<br></span></th>

                                        <th data-hide="phone,tablet,computer" class="font-xs">Category</th>
                                        <th data-hide="phone,tablet,computer" class="font-xs">Priority</th>
                                        <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                                        <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                                        <th data-hide="phone,tablet,computer" class="font-xs">Location</th>
                                        <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
                                        <th data-hide="phone,tablet,computer" class="font-xs">Type</th>
                                        <th data-hide="phone,tablet,computer" class="font-xs"><span class="txt-color-orange">Raised By<br> Company</span></th>

                                    </tr>
                                    </tfoot>--}}
                    <tbody>
                    @foreach( $items as $item)

                        <tr style="background-color: {{$item->getItemStatus()['rowBackGroundColour']}}" id="" class=" task-table-row">

                            {{-- Number --}}
                            <td id="row-{{$item->id}}" onclick="showDetailsModal({{$item->id}})" style="width: 20%;">
                                <div class="hover-over-item-number">
                                    <h5 class="" onclick="getItemDetails({{$item->id}})">
                                        <small><i class="fa fa-tag"></i></small>
                                        &nbsp;{{$item->number}}
                                        @if($item->urgent === 1)
                                            &nbsp;&nbsp;&nbsp;  <i class="fa fa-exclamation-triangle text-danger"></i>
                                        @endif
                                        @if($item->isOnMyWatchList() === 1)
                                            &nbsp;&nbsp;  <i class="fa fa-thumbs-o-up text-warning"></i>
                                        @endif
                                        @if($item->error > 0)

                                            <div style="color: red">
                                                <i class="fa fa-exclamation-circle"></i> Compile ERROR <br>
                                                Check Link to Schedule - Ensure Link exists for the Milestone

                                            </div>
                                        @endif
                                    </h5>

                                    <p class="font-xs hidden-xs"><i class="fa fa-clock-o"></i>&nbsp;
                                        <small>Raised by</small>{{$item->raisedByUser->name}} {{$item->raisedByUser->surname}}
                                        <br> {{$item->raised_date->toFormattedDateString() }}
                                        <small class="font-xs">({{$item->raised_date->diffForHumans()}})</small>
                                    </p>

                                    @if($item->owner_updated_by_id > 0)
                                        <p class="font-xs text-muted hidden-xs">
                                            <small>Last updated by {{$item->ownerUpdatedBy->name}} {{$item->ownerUpdatedBy->surname}}
                                                <br>{{$item->owner_updated_date->diffForHumans()}}</small>
                                        </p>
                                    @endif
                                    @if($item->privacy === 0)<p class="text-danger"><i class="fa fa-hand-paper-o"></i> Private</p>@endif
                                </div>
                            </td>
                            {{-- Details--}}
                            <td id="row-{{$item->id}}" style="width: 30%;">
                                <p><i class="fa fa-pencil-square-o"></i> {{$item->finding}}</p>
                                @if($item->action !== '')
                                    <p><i class="fa  fa-wrench"></i> {{$item->action}}</p>
                                @endif

                                <p style="font-size: 60%">{{$item->description}}</p>

                                <p><i class="fa  fa-link"></i> {{$item->group->name}}, {{$item->type->name}}</p>

                                @if($item->isThereAnAttachment() === 1)
                                    <button class="btn btn-info btn-sm" onclick="showAttachmentsForItem({{$item->id}})"><i class="fa fa-paperclip"></i></button>
                                @endif

                            </td>
                            {{-- Location--}}
                            <td>
                                {{$item->system->tag}} <br>

                                <div class="font-xs">{{$item->system->description}} </div>
                                <br>
                                @if( isset($item->component))
                                    <div class="font-xs">Component: {{$item->component->tag}}</div>
                                    <br>
                                @endif

                                <p class="font-xs">
                                    @if($item->building !== "")
                                        Building:{{$item->building}} &nbsp;<br>
                                    @endif

                                    @if($item->floor !== "")
                                        Floor:{{$item->floor}} &nbsp;<br>
                                    @endif

                                    @if($item->room !== "")
                                        Room:{{$item->room}} &nbsp;<br>
                                    @endif

                                    {{$item->system_extra_info}}

                                </p>
                            </td>
                            {{-- Due Date--}}
                            <td class="font-xs" style="background-color: {{$item->getDueDateColourAndText()['backGroundColour']}}">
                                {{$item->due_date->toFormattedDateString() }}<br>
                                <small>({{$item->due_date->diffForHumans()}})</small>
                            </td>


                            {{-- Responsible--}}
                            <td style="background-color: {{$item->getResStatus()['backGroundColour']}}">
                                <span style="font-size: 75%">
                                    {{$item->resRole->name}}<br>
                                    {{$item->resUser->name}} {{$item->resUser->surname}}
                                    </span>
                                <br><br>

                                <div class="font-xs">{{$item->res_note}}</div>

                                {!!$item->getResStatus()['label']!!}

                                @if($item->res_updated_by_id > 0 && false)
                                    <p class="font-xs text-muted">
                                        <small>updated by<br> {{$item->resUpdatedBy->name}} {{$item->resUpdatedBy->surname}}
                                            <br>{{$item->res_updated_date->diffForHumans()}}</small>
                                    </p>
                                @endif
                            </td>
                            {{-- Assigned --}}
                            <td style="background-color: {{$item->getAssignedDetails()['backGroundColour']}}">
                                 <span style="font-size: 75%">
                                {!!$item->getAssignedDetails()['role']!!}
                                     {!!$item->getAssignedDetails()['assignedDate']!!}
                                    </span>
                                <br>
                                {!!$item->getAssignedDetails()['label']!!}
                            </td>
                            {{-- Status --}}
                            <td style="background-color: {{$item->getItemStatus()['backGroundColour']}}">
                                <br>
                                {!!$item->getItemStatus()['label']!!}<br><br>
                                {!!$item->activatedStatus()['label']!!}<br><br>

                                <div class="" style="font-size: 20%">
                                    {!!$item->getItemStatus()['closedBy']!!}
                                    {!!$item->getItemStatus()['closedOn']!!}
                                </div>
                            </td>

                            <td>{{$item->category->name}}</td>
                            <td>{{$item->priority->name}}</td>
                            <td>{{$item->area->name}}</td>
                            <td>{{$item->system->tag}}</td>
                            <td>{{$item->building}}, {{$item->floor}} , {{$item->room}}, {{$item->other_location}} </td>

                            <td>{{$item->group->name}}</td>
                            <td>{{$item->type->name}}</td>
                            <td>{{$item->raisedByRole->name}}</td>
                            <td>{{$item->raisedByUser->name}} {{$item->raisedByUser->surname}}</td>
                            <td>{{$item->raised_date->toDateString()}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

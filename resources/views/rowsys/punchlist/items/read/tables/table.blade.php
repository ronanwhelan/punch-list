<div class="row">
    <div class="col-sm-12">
        <article class="hide">
            <div aria-label="Justified button group" role="group" class="btn-group btn-group-justified">
                <a role="button" class="btn btn-success" style="font-size: 10px" href="#">High &nbsp;<i class="fa fa-check"></i></a>
                <a role="button" class="btn btn-info" style="font-size: 10px" href="#">Medium&nbsp;<i class="fa fa-times"></i></a>
                <a role="button" class="btn btn-info" style="font-size: 10px" href="#">Low&nbsp;<i class="fa fa-times"></i></a>
                <a role="button" class="btn btn-info" style="font-size: 10px" href="#">Filter 4&nbsp;<i class="fa fa-times"></i></a>
                <a role="button" class="btn btn-info" style="font-size: 10px" href="#">Filter 5&nbsp;<i class="fa fa-times"></i></a>
            </div>
        </article>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="panel panel-info">
            <div class="panel-heading"><h4 class="panel-title">Items List</h4></div>
            <div class="panel-body">

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Search</span>
                        <input id="search" type="text" class="form-control input-lg" aria-describedby="basic-addon3">
                    </div>
                </div>

                <a class="btn btn-default" href="#home" role="button">Link</a>

                <button class="btn btn-default" type="submit">Button</button>
                <input class="btn btn-default" type="button" value="Input">
                <input class="btn btn-default" type="submit" value="Submit">
                <!-- Nav tabs -->

                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Items </a></li>
                    <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Filters</a></li>
                    <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                </ul>

                <div class=" custom-scroll  " style="">

                            <!-- Tab panes -->
                            <div id ="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    <table class="table">
                                        <tbody class="">
                                        @foreach ($items as $item)
                                            <tr>
                                                <th scope="row">

                                                    <p class="" onclick="getItemDetails({{$item->id}})">{{$item->number}}</p>

                                                    <p class="font-xs"><i class="fa fa-clock-o"></i>&nbsp;
                                                        <small>Raised by</small>{{$item->raisedByUser->name}} {{$item->raisedByUser->surname}}
                                                        on  {{$item->raised_date->format('d-m-Y')}}
                                                        <small>({{$item->raised_date->diffForHumans()}})</small>
                                                    </p>


                                                    <a role="button" style=" width:100%;overflow:hidden;white-space: nowrap;"
                                                       data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$item->id}}" aria-expanded="true"
                                                       aria-controls="collapse-{{$item->id}}">
                                                        <i class="fa fa-caret-right"></i>&nbsp;{{$item->description}}</a>



                                                    <div id="collapse-{{$item->id}}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                            <p><i class="fa fa-pencil"></i>&nbsp; {{$item->description}}</p>

                                                            <p><i class="fa fa-map-pin"></i>&nbsp; Level 2, 1-4/A-G</p>

                                                            <p><i class="fa fa-user"></i>&nbsp; Lend Lease</p>

                                                            <p><i class="fa fa-tag"></i>&nbsp; HVAC</p>

                                                            <p><i class="fa fa-clock-o"></i>&nbsp;
                                                                <small>Raised by</small>
                                                                Joe Bloggs on 20-05-2016
                                                            </p>
                                                        </div>
                                                    </div>

                                                </th>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>




                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <h3>Tab 2</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages">
                                    <h3>Tab 3</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at tempor ipsum, a tempor elit. Sed ornare sapien mauris, a interdum risus consectetur non. Pellentesque eget condimentum velit. Integer dignissim erat ut purus rhoncus viverra. Sed ex nulla, tincidunt vel laoreet id, rhoncus at ex. Integer at lobortis tellus, non blandit dui. Praesent scelerisque finibus nulla, non luctus felis mattis tempus.</p>
                                </div>
                            </div>









                    <ul class="list-group hide">
                        @foreach ($items as $item)
                            <li class="list-group-item">

                                <div class="card-box font-xs">

                                    <h4 onclick="getItemDetails({{$item->id}})">{{$item->number}}</h4>

                                    <p onclick="getItemDetails({{$item->id}})" style=" width:100%;overflow:hidden;white-space: nowrap;" class="text-primary"><i
                                                class="fa fa-pencil"></i>&nbsp; {{$item->description}}</p>

                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                                                   aria-controls="collapseOne">
                                                    <i class="fa fa-caret-right"></i>
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="collapseOne" class="panel-collapse collapse in " role="tabpanel" >
                                            <div class="panel-body">

                                                <p><i class="fa fa-pencil"></i>&nbsp; {{$item->description}}</p>
                                                <p><i class="fa fa-map-pin"></i>&nbsp; Level 2, 1-4/A-G</p>

                                                <p><i class="fa fa-user"></i>&nbsp; Lend Lease</p>

                                                <p><i class="fa fa-tag"></i>&nbsp; HVAC</p>

                                                <p><i class="fa fa-clock-o"></i>&nbsp;
                                                    <small>Raised by</small>
                                                    Joe Bloggs on 20-05-2016
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="col-sm-8">
        <div class="panel panel-info">
            <div class="panel-heading">Item</div>
            <div class="panel-body">

                <div id="item-details-section">

                    <h2>No Item Selected</h2>
                </div>


            </div>
        </div>
    </div>

</div>

<!-- row -->
<div class="row hide id=" table-section" style="opacity: .08">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
    <div class="jarviswidget " id="wid-id-1" data-widget-editbutton="false"
         data-widget-colorbutton="false" data-widget-setstyle="false" data-widget-togglebutton="false"
         data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>

            <h2><strong>Punch </strong> <i>List</i></h2>

            <div class="widget-toolbar">
                <a href="javascript:void(0);" class="btn btn-primary hide"><i class="fa fa-refresh"></i></a>
                <a href="javascript:void(0);" class="btn btn-primary hide">Stats <i class="fa fa-bar-chart-o"></i></a>

                <div class="btn-group">
                    <button class="btn dropdown-toggle btn-xs btn-info" data-toggle="dropdown">
                        Toggle column <i class="fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a class="toggle-vis" data-column="1">Next Target Date</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="2">Area</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="3">System</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="4">Group</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="5">Task Type</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="6">Stage</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="11">Target Hrs</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="12">Earned Hrs</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="13">Revised Date</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="14">Base Date</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="15">Base Dev Days</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <!-- widget div-->
        <div>
            <div class="widget-body no-padding">
                <div class=" ">
                    <div id="mission_list_wrapper" class="dataTables_wrapper form-inline no-footer">
                        <table width="100%" role="grid" id="mission_list"
                               class="display nowrap table table-striped table-hover table-bordered dataTable no-footer has-columns-hidden" cellspacing="0" width="100%">
                            <thead class="">
                            <tr class="hide">
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=" "/></th>

                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=" "/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                            </tr>
                            <tr class="font-xs">
                                <th data-class="expand" class="font-xs">Number</th>
                                <th data-hide="phone,tablet,computer" class="font-xs">Next<br>Target<br>Date</th>
                                <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                                <th data-hide="phone,tablet,computer" class="font-xs">System</th>
                                <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
                                <th data-hide="phone,tablet,computer" class="font-xs">Task Type</th>
                                <th data-hide="phone,tablet,computer" class="font-xs">Stage</th>

                                <th data-hide="phone" class="font-xs"><span class="txt-color-orange">Prep/Ex<br>Status &  Date</span></th>

                                <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Review<br>Status &  Date</span></th>

                                <th data-hide="phone" class="font-xs"><span class="txt-color-orange">ReIssue<br>Status &  Date</span></th>

                                <th data-hide="phone" class="font-xs"><span class="txt-color-blue">SignOff<br>Status &  Date</span></th>

                                <th data-hide="phone,tablet,computer" class="font-xs">Target<br>Val (hrs)</th>
                                <th data-hide="phone,tablet,computer" class="font-xs">Earned<br>Val (hrs)</th>

                                <th data-hide="phone,tablet,computer" class="font-xs">Revised<br>Date</th>
                                <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Date</th>
                                <th data-hide="phone,tablet,computer" class="font-xs">Base<br>Dev Days</th>
                            </tr>
                            </thead>

                            <tbody id="mission-list-table-body" class="">
                            @foreach( $items as $item)

                                <tr id="" onclick="showEditTaskModal({{$item->id}})" class=" task-table-row">

                                    <td id="row-{{$item->id}}"><strong>{{$item->number}}</strong>
                                        <br>
                                            <span class=" text-primary">
                                                <i class="fa fa-star"></i>&nbsp;{{$item->description}}
                                            </span>
                                        <br>
                                               <span class=" text-warning">
                                                <i class="fa fa-key"></i>&nbsp; {{$item->number}}
                                            </span>


                                    </td>

                                    <td>{{$item->description}}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="text-align-center txt-color-blue" id="loading-box-section">
    <br><br>
    <span><i class="fa fa-spinner  fa-spin fa-5x"></i><br><br>Updating Table....</span>

</div>
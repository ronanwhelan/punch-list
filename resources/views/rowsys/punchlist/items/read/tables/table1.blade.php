
<div class="row" id="table-section" style="opacity: .08">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
    <div class="jarviswidget " id="wid-id-1" data-widget-editbutton="false"
         data-widget-colorbutton="false" data-widget-setstyle="false" data-widget-togglebutton="false"
         data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>

            <h2><strong>Items  </strong> <i></i></h2>

            <div class="widget-toolbar">
                <a href="javascript:void(0);" class="btn btn-primary hide"><i class="fa fa-refresh"></i></a>
                <a href="javascript:void(0);" class="btn btn-primary hide">Stats <i class="fa fa-bar-chart-o"></i></a>

                <div class="btn-group">
                    <button class="btn dropdown-toggle btn-xs btn-info" data-toggle="dropdown">
                        Toggle column <i class="fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a class="toggle-vis" data-column="1">Due Date</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="2">Area</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="3">System</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="4">Group</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="5">Type</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="6">Category</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="7">Responsible</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="8">Assigned</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="9">Raised By</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="9">Raised By Team</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="9">Raised Date</a>
                        </li>
                        <li>
                            <a class="toggle-vis" data-column="10">Status</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <!-- widget div-->
        <div>
            <div class="widget-body no-padding">
                <div class=" ">
                    <div id="mission_list_wrapper" class="dataTables_wrapper form-inline no-footer">
                        <table width="100%" role="grid" id="mission_list"
                               class="display nowrap table table-striped table-hover table-bordered dataTable no-footer has-columns-hidden" cellspacing="0" width="100%">
                            <thead class="">
                            <tr class="hide">
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>
                                <th class="hasinput" style=""><input type="text" class="form-control " placeholder=""/></th>

                            </tr>
                            <tr class="font-xs">
                                <th data-class="expand" class="font-xs">Details</th>
                                <th data-hide="phone" class="font-xs"><br>Due Date</th>

                                <th data-hide="phone,tablet,computer" class="font-xs">Area</th>
                                <th data-hide="phone" class="font-xs">System/Location</th>

                                <th data-hide="phone,tablet,computer" class="font-xs">Group</th>
                                <th data-hide="phone,tablet" class="font-xs">Type</th>

                                <th data-hide="phone,tablet,computer" class="font-xs">Category</th>

                                <th data-hide="phone" class=""><i class="fa fa-paperclip"></i></th>

                                <th data-hide="phone" class="font-xs"><span class="txt-color-blue">Responsible<br> & Status</span></th>
                                <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-orange">Assignee<br> & Status</span></th>

                                <th data-hide="phone,tablet,computer" class="font-xs"><span class="txt-color-orange">Raised By<br> Team</span></th>

                                <th data-hide="phone,tablet" class="font-xs"><span class="txt-color-orange">Status<br></span></th>

                            </tr>
                            </thead>

                            <tbody id="mission-list-table-body" class="">
                            @foreach( $items as $item)

                                <tr style="background-color: {{$item->getItemStatus()['rowBackGroundColour']}}" id=""  class=" task-table-row">

                                    <td id="row-{{$item->id}}" onclick="showDetailsModal({{$item->id}})">
                                        <h5 class="" onclick="getItemDetails({{$item->id}})"><small><i class="fa fa-tag"></i></small>&nbsp;{{$item->number}}</h5>
                                        <p>{{$item->description}}</p>
                                        <p class="font-xs"><i class="fa fa-clock-o"></i>&nbsp;
                                            <small>Raised by </small>{{$item->raisedByUser->name}} {{$item->raisedByUser->surname}} ({{$item->raisedByUser->company->name}})
                                            <br> on  {{$item->raised_date->format('d-m-Y')}}
                                            <small>({{$item->raised_date->diffForHumans()}})</small>
                                        </p>
                                        <p>@if($item->privacy === 0)<i class="fa fa-hand-paper-o"></i>@endif</p>
                                    </td>

                                    <td style="background-color: {{$item->getDueDateColourAndText()['backGroundColour']}}">{{$item->due_date->format('d-m-Y')}}<br><small>({{$item->due_date->diffForHumans()}})</small></td>
                                    <td>{{$item->area->name}}</td>
                                    <td>{{$item->system->tag}} <br><div class="font-xs">{{$item->system->description}} </div>
                                        <br>
                                        <p class="font-xs">{{$item->building}} &nbsp; {{$item->floor}} &nbsp;{{$item->room}} &nbsp;{{$item->extra_info}}</p>

                                    </td>

                                    <td>{{$item->group->name}}</td>
                                    <td>{{$item->type->name}}</td>

                                    <td>{{$item->priority->name}}</td>

                                    <td><button class="btn btn-default" onclick="showAttachmentsForItem({{$item->id}})"><i class="fa fa-paperclip"></i></button></td>


                                    <td style="background-color: {{$item->getResStatus()['backGroundColour']}}">{{$item->resCompany->name}}<br>
                                        <br>
                                        <div class="font-xs">{{$item->res_note}}</div>
                                        <br>
                                        {!!$item->getResStatus()['label']!!}

                                    </td>

                                    <td  style="background-color: {{$item->getAssignedDetails()['backGroundColour']}}">{!!$item->getAssignedDetails()['company']!!}
                                        {!!$item->getAssignedDetails()['assignedDate']!!}
                                        <br>
                                        {!!$item->getAssignedDetails()['label']!!}
                                    </td>


                                    <td ><strong>{{$item->raisedByUser->name}} {{$item->raisedByUser->surname}}</strong> from
                                     {{$item->raisedByUser->company->name}}</td>


                                    <td style="background-color: {{$item->getItemStatus()['backGroundColour']}}">{!!$item->getItemStatus()['label']!!}
                                        <br>
                                        <div class="font-xs">
                                        {!!$item->getItemStatus()['closedBy']!!}
                                        {!!$item->getItemStatus()['closedOn']!!}</div>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="text-align-center txt-color-blue" id="loading-box-section">
    <br><br>
    <span><i class="fa fa-spinner  fa-spin fa-5x"></i><br><br>Updating Table....</span>

</div>
<div class="row">
    <div id="myTabs" class="col-lg-12">
        {{-- NUMBER--}}
        <h3 id="header-item-number-for-show" class="hide"> {{$item->number}}   @if($item->urgent === 1)
                &nbsp;  <i class="fa fa-exclamation-triangle text-danger"></i>
            @endif</h3>

        {{-- STATUS --}}
        <div class="pull-right">{!!$item->getItemStatus()['label']!!}</div>
        {{-- CATEGORY--}}
        <label class="label label-default pull-right hidden-xs">{{$item->category->short_name}}</label>
        <label class="label label-warning pull-right hidden-xs">{{$item->priority->name}}</label>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#details-tab" aria-controls="details-tab" role="tab" data-toggle="tab"><i class="fa fa-pencil"></i><span class="hidden-xs"> Details</span>
                </a></li>
            <li><a href="#res-tab" aria-controls="res" role="tab" data-toggle="tab"><i class="fa fa-users"></i> <span class="hidden-xs">Responsible</span></a></li>
            <li><a href="#assign-tab" aria-controls="assign-tab" role="tab" data-toggle="tab"><i class="fa fa-user"></i> <span class="hidden-xs">Assigned</span></a></li>
            <li><a href="#attach-tab" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-paperclip"></i> <span class="hidden-xs">Attachments</span></a></li>
        </ul>

        <!-- Tab panes -->
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="details-tab">
                {{-- DETAILS  --}}
                <div class="panel panel-primary">

                    <div class="panel-body animated fadeIn ">
                        <form id="update-item-owner-details-form" action="/items/update/owner-details/{{$item->id}}" method="POST">

                            @if($superUser === 1 || true)
                                <div class="row ">
                                    <div class="">
                                        <div class="form-group col-sm-12">
                                            <label for="res-status">Activation Status:</label>
                                            <select class=" form-control selectpicker" data-style="btn-warning"
                                                    name="activated_status" id="activated_status">
                                                @foreach($activationStatusArray as $key => $value)
                                                    @if($key === $item->activated_status)
                                                        <option value="{{$key}}" selected>{{$value}} </option>
                                                    @else
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            @endif


                            {{-- DESCRIPTION --}}
                            <div class="">
                                <div class="form-group ">
                                    <small><label class="control-label text-muted"><i class="fa fa-pencil"></i>&nbsp; Description:</label></small>
                                    <textarea class="longInput font-lg form-control " name="description" cols="50" rows="3">{{$item->finding}}</textarea>
                                </div>

                                {{-- ACTION  --}}
                                <div class="form-group ">
                                    <small><label class="control-label text-muted"><i class="fa fa-cog"></i>&nbsp; Action Required:</label></small>
                                    <textarea class="longInput  font-lg form-control " name="owner_action_required" cols="50" rows="2">{{$item->action}}</textarea>
                                </div>

                                <p class="font-xs hidden-xs"><i class="fa fa-user"></i>&nbsp;
                                    <small>Raised by</small> {{$item->raisedByUser->name}} {{$item->raisedByUser->surname}} ({{$item->raisedByUser->company->name}})
                                    on  {{$item->raised_date->format('d-m-Y')}}
                                    <small>({{$item->raised_date->diffForHumans()}})</small>
                                </p>


                                {{-- MORE DETAILS details drop down --}}

                                <a class="" data-toggle="collapse" href="#collapse-owner-{{$item->id}}" aria-expanded="true"
                                   aria-controls="collapse-owner-{{$item->id}}">
                                    <i class="fa fa-caret-down pull-right"></i> <i class="fa fa-plus-circle"></i> More Details
                                </a>

                                <div id="collapse-owner-{{$item->id}}" class="panel-collapse collapse" role="tabpanel">
                                    <div class="form-group">
                                        <br>

                                        {{-- SYSTEM / LOCATION  --}}
                                        <button type="button" class=" hide btn btn-default btn-xs pull-right"><i class="fa fa-pencil"></i></button>

                                        <p>
                                            <small><label class=" control-label text-muted"><i class="fa fa-cubes"></i>&nbsp; System:</label></small>
                                            &nbsp;{{$item->system->tag}}, {{$item->system->description}}
                                            @if($item->system_extra_info !== '')
                                                &nbsp;&nbsp;({{$item->system_extra_info}})
                                            @endif
                                        </p>

                                        @if($item->due_date_updated === 0)
                                            <p>
                                                <small><label class=" control-label text-muted"><i class="fa fa-calendar-o"></i>&nbsp; Link To System Milestone Step : </label>
                                                </small>
                                                {{$item->getScheduleLinkStep()}} (num:{{$item->linked_milestone}})
                                            </p>
                                        @endif

                                        <p>
                                            <small><label class=" control-label text-muted"><i class="fa fa-map-pin"></i>&nbsp; Site Location:</label></small>
                                            <small>Building:</small>  {{$item->building}} &nbsp; &nbsp; &nbsp;
                                            <small> Floor(s):</small>{{$item->floor}} &nbsp;&nbsp;&nbsp;
                                            <small>Room(s):</small>{{$item->room}}
                                            @if($item->other_location !== '')
                                                &nbsp;&nbsp;({{$item->other_location}})
                                            @endif
                                        </p>
                                        {{-- TYPE --}}
                                        <p>
                                            <small><label class=" control-label text-muted"><i class="fa fa-link"></i>&nbsp; Type:</label></small>
                                            &nbsp;{{$item->group->name}},  {{$item->type->name}}
                                        </p>



                                        {{-- DUE DATE --}}
                                        <p>
                                            <small><label class=" control-label text-muted"><i class="fa fa-calendar"></i>&nbsp; Due Date:</label></small>
                                            {{$item->due_date->format('d-m-Y')}}
                                            <small>({{$item->due_date->diffForHumans()}})</small>
                                            @if($accessLevel > 2)
                                                <button type="button" class="btn btn-primary btn-xs" onclick="showUpdateDueDateSection()"><i class="fa fa-edit"></i> Edit</button>
                                            @endif
                                        </p>
                                        @if($item->due_date_updated === 1)
                                            <p class="text-primary"><i class="fa fa-info-circle"></i>
                                                <small>
                                                    Due Date Changed by :  {{$item->dueDateUpdatedBy->name}} {{$item->dueDateUpdatedBy->surname}}
                                                    {{$item->due_date_updated_on_date->diffForHumans()}}
                                                </small>
                                            </p>
                                        @endif

                                        {{-- UPDATE DUE DATE--}}
                                        <div id="change-due-date-section" hidden>
                                            <div class="form-group">
                                                <label class=" control-label">New Due Date:</label>
                                                <input type="hidden" id="old_search_date" value="">

                                                <div class="input-group date">
                                                    <input type="text" id="to_date" onchange="" name="updated_target_date" class=" datepicker"
                                                           data-type="date" data-date-format="yyyy-mm-dd" value="">
                                                </div>

                                            </div>
                                            <label class="checkbox-inline"><input type="checkbox" value="update" name="chk_update_due_date">Update Due Date</label>
                                            {{-- NOTE --}}
                                            <div class="form-group  ">
                                                <small><label class="control-label text-muted">&nbsp; Note:</label></small>
                                                <textarea class="longInput  form-control " name="update_date_note" cols="50" rows="2">{{$item->due_date_updated_note}}</textarea>
                                            </div>

                                            @if($item->due_date_updated === 1)
                                            <p class="font-xs text-muted">
                                                <small>
                                                    Changed by {{$item->dueDateUpdatedBy->name}} {{$item->dueDateUpdatedBy->surname}}
                                                    {{$item->due_date_updated_on_date}}
                                                </small>
                                            </p>
                                                @endif
                                        </div>

                                        @if($item->due_date_updated === 0)
                                            <p>
                                                <small><label class=" control-label text-muted"><i class="fa fa-star"></i>&nbsp; Lag: </label></small>
                                                {{$item->lag_days}} days
                                            </p>
                                        @endif








                                        {{-- RECORED PHASE --}}
                                        <p class="font-xs hide">
                                            <small> Recorded during the {{$item->phase_recorded_in}} phase</small>
                                        </p>

                                    </div>

                                    <button type="button" data-toggle="collapse" href="#collapse-date-{{$item->id}}" aria-expanded="true"
                                            aria-controls="collapse-date-{{$item->id}}" class="hide btn btn-default btn-xs pull-right"><i class="fa fa-pencil"></i></button>

                                    <div class="">

                                        <div id="collapse-date-{{$item->id}}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label class=" control-label">Lag (- days):</label>
                                                    <input type="number" class="form-control" name="lag_days" value="{{$item->lag_days}}">
                                                </div>

                                                <div class="form-group ">
                                                    <label class=" control-label">New Date:</label>

                                                    <div class="input-group date">
                                                        <input type="date" id="entered_target_date" name="entered_target_date" class=" datepicker"
                                                               data-date-format="yyyy-mm-dd"
                                                               value="{{$item->due_date->format('Y-m-d')}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="progress hide">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 60%;">
                                            60%
                                        </div>
                                    </div>

                                </div>
                            </div>


                            {{-- END MORE DETAILS --}}


                            <div class="row">
                                <div class="">
                                    <br>

                                    <div class="form-group  hide">
                                        <label for="res-status">Responsible:</label>
                                        <select class=" form-control selectpicker" data-style="btn-info"
                                                name="res_company" id="res_company">
                                            <option value="0"></option>
                                            @foreach($companies as $company)
                                                @if($item->res_company_id === $company->id)
                                                    <option value="{{$company->id}}" selected>{{$company->name}}</option>
                                                @else
                                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>




                                    <div class="form-group  col-sm-3">
                                        <label for="res-status">Responsible</label>
                                        <select class=" form-control selectpicker" data-style="btn-primary"
                                                name="responsible_role" id="responsible_role" onchange="getUsersByRole(this.value,'attention_to')">
                                            <option value="0"></option>
                                            @foreach($roles as $role)
                                                @if($item->res_role_id === $role->id)
                                                    <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                                @else
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>



                                    <div class="form-group col-sm-3">
                                        <label for="res-status">Individual:</label>
                                        <select class=" form-control selectpicker" data-style="btn-primary"
                                                name="attention_to" id="attention_to">
                                            <option value="0"></option>
                                            @foreach($users as $user)
                                                @if($item->res_user_id === $user->id)
                                                    <option value="{{$user->id}}" selected>{{$user->name}} {{$user->surname}}</option>
                                                @else
                                                    <option value="{{$user->id}}">{{$user->name}} {{$user->surname}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="form-group col-md-offset-1  col-md-2 ">
                                        <br>
                                        <label class="checkbox-inline ">
                                            @if($item->urgent === 1)
                                                <input type="checkbox" id="urgent" name="urgent" checked>Urgent <i class="fa fa-exclamation-triangle"></i>
                                            @else
                                                <input type="checkbox" id="urgent" name="urgent">Urgent <i class="fa fa-exclamation-triangle"></i>
                                            @endif
                                        </label>
                                    </div>
                                    <div class="form-group  col-md-2 ">
                                        <br>
                                        <label class="checkbox-inline ">
                                            @if($item->isOnMyWatchList() === 1)
                                                <input type="checkbox" id="my_flag" name="my_flag" checked> Watch List <i class="fa fa-thumbs-o-up"></i>
                                            @else
                                                <input type="checkbox" id="my_flag" name="my_flag">Watch List <i class="fa fa-thumbs-o-up"></i>
                                            @endif
                                        </label>
                                    </div>


                                    <div class="form-group  col-md-3 hide">
                                        <br>
                                        <label class="checkbox-inline ">
                                            @if($item->privacy === 0)
                                                <input type="checkbox" id="privacy" name="privacy" checked>For Private Viewing
                                            @else
                                                <input type="checkbox" id="privacy" name="privacy">For Private Viewing
                                            @endif
                                        </label>

                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="">
                                    <div class="form-group col-sm-12">
                                        <label for="res-status">Status:</label>
                                        <select class=" form-control selectpicker" data-style="btn-primary"
                                                name="own_status" id="own_status">
                                            @foreach($ownStatusArray as $key => $value)
                                                @if($key === $item->own_status)
                                                    <option value="{{$key}}" selected>{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <br>

                            <div class="row">


                                <!-- SERVER AND VALIDATION ERRORS -->
                                <div class="alert alert-info fadeIn hide" id="update-item-owner-details-form-validation-errors"></div>
                                @if($accessLevel > 2)
                                    <div class="col-md-12">
                                        <button type="button"
                                                onclick="updateItemDetails('update-item-owner-details-form','/items/update/owner-details/{{$item->id}}',{{$item->id}})"
                                                class="btn btn-success  btn-lg btn-block"><i class="fa fa-pencil"></i>&nbsp;Update
                                        </button>
                                        @else
                                            <button type="button" disabled
                                                    class="btn btn-success  btn-lg btn-block"><i class="fa fa-pencil"></i>&nbsp;Update
                                            </button>
                                        @endif
                                        @if($item->owner_updated_by_id > 0)
                                            <p class="font-xs text-muted">
                                                <small>Last updated by {{$item->ownerUpdatedBy->name}} {{$item->ownerUpdatedBy->surname}}
                                                    {{$item->owner_updated_date->diffForHumans()}}</small>
                                            </p>
                                        @endif
                                    </div>

                                    @if($accessLevel > 2)
                                        <div class="col-md-12 hide ">
                                            <div class="">
                                                <button type="button" onclick="showModelDeleteSection(0)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i>&nbsp;Delete&nbsp;
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-12 hide">
                                            <div id="are-you-sure-delete-section" class="hide">
                                                <div class="alert alert-danger alert-dismissible" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times; </span>
                                                    </button>
                                                    <p class="">Are you Sure <i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="deleteItem({{$item->id}})" class="btn btn-danger"> Yes</button>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                            @endif

                        </form>
                    </div>
                </div>
            </div>


            <div role="tabpanel" class="tab-pane fade" id="res-tab">
                {{-- RESPONSIBLE PARTY --}}
                <div class="panel panel-primary ">

                    <div class="panel-body animated fadeIn">

                        <form id="update-item-responsible-details-form" action="/items/update/responsible-details/{{$item->id}}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <h3><i class="fa fa-users"></i>&nbsp; {{$item->resRole->name}}</h3>

                            <p>{{$item->resUser->name}} {{$item->resUser->surname}}</p>

                            <a data-toggle="collapse" href="#collapse-responsible-{{$item->id}}" aria-expanded="true"
                               aria-controls="collapse-responsible-{{$item->id}}"
                               class="scroll-to-on-click hide">
                                <i class="fa fa-users"></i>&nbsp;
                                @if(isset($item->resCompany))
                                {{$item->resCompany->name}}
                                @else
                                    ERROR on resCompany
                                    @endif

                                &nbsp; <i class="pull-right fa fa-caret-down"></i>
                            </a>
                            <br>

                            <div id="collapse-responsible-{{$item->id}}" class="panel-collapse collapse in  " role="tabpanel">


                                <div class="form-group ">
                                    <label class="control-label"><i class="fa fa-comment"></i>&nbsp; Remark:</label>
                                    <textarea class="longInput  form-control " name="res_note" cols="50" rows="3">{{$item->res_note}}</textarea>
                                    <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                </div>


                                <div class="form-group hide ">
                                    <label class="control-label"><i class="fa fa-question-circle"></i>&nbsp; Cause:</label>
                                    <textarea class="longInput  form-control " name="res_cause" cols="50" rows="2">{{$item->res_cause}}</textarea>
                                    <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                </div>
                                <div class="form-group ">
                                    <label class="control-label"><i class="fa fa-cog"></i>&nbsp; Action:</label>
                                    <textarea class="longInput  form-control " name="res_action" cols="50" rows="1">{{$item->res_action}}</textarea>
                                    <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                </div>


                                <div class="form-group">
                                    <label for="res-status">Status:</label>
                                    <select class=" form-control selectpicker" data-style="btn-primary"
                                            name="res_status" id="res_status">
                                        @foreach($statusArray as $key => $value)
                                            @if($key === $item->res_status)
                                                <option value="{{$key}}" selected>{{$value}}</option>
                                            @else
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>


                                <div class="form-group hide ">
                                    <label class="control-label"><i class="fa fa-users"></i>&nbsp; Assign to:</label>
                                    <select class=" form-control selectpicker" onchange="" data-style="btn-primary" data-live-search="true"
                                            name="assign_to">
                                        <option value="0">Not Assigned</option>
                                        @if(count($action) === 0)

                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        @else
                                            @foreach($companies as $company)
                                                @if($company->id === $action->assigned_company_id)
                                                    <option value="{{$company->id}}" selected>{{$company->name}}</option>
                                                @else
                                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                    <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                </div>


                                <div class="form-group  ">
                                    <label for="res-status"><i class="fa fa-users"></i>&nbsp;Assigned Party:</label>
                                    <select class=" form-control selectpicker" data-style="btn-primary"
                                            name="assigned_role" id="assigned_role" onchange="getUsersByRole(this.value,'assigned_to')">
                                        <option value="0"></option>
                                        @foreach($roles as $role)
                                            @if($item->assigned_role_id === $role->id)
                                                <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                            @else
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group ">
                                    <label for="res-status"><i class="fa fa-user"></i>&nbsp;Individual:</label>
                                    <select class=" form-control selectpicker" data-style="btn-primary"
                                            name="assigned_to" id="assigned_to">
                                        <option value="0"></option>
                                        @foreach($users as $user)
                                            @if($item->assigned_user_id === $user->id)
                                                <option value="{{$user->id}}" selected>{{$user->name}} {{$user->surname}}</option>
                                            @else
                                                <option value="{{$user->id}}">{{$user->name}} {{$user->surname}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>


                                <label class="  control-label">Assign Target Date:</label>

                                <div class="input-group date ">
                                    <input type="date" id="entered_target_date" name="res_target_date" class="datepicker "
                                           data-date-format="yyyy-mm-dd"
                                           value="{{$item->res_target_date->format('Y-m-d')}}">
                                </div>

                                <br>
                                <!-- SERVER AND VALIDATION ERRORS -->
                                <div class="alert alert-info fadeIn hide" id="update-item-responsible-details-form-validation-errors"></div>

                                @if($accessLevel > 0)
                                    <button type="button"
                                            onclick="updateItemDetails('update-item-responsible-details-form','/items/update/responsible-details/{{$item->id}}',{{$item->id}})"
                                            class="btn btn-success btn-lg btn-block"><i class="fa fa-pencil"></i>&nbsp;Update
                                    </button>
                                @endif
                                @if($item->res_updated_by_id > 0)
                                    <p class="font-xs text-muted">
                                        <small>Last updated by {{$item->resUpdatedBy->name}} {{$item->resUpdatedBy->surname}}
                                            {{$item->res_updated_date->diffForHumans()}}</small>
                                    </p>
                                @endif


                                <hr>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="assign-tab">

                {{-- ASSIGNED PARTY --}}
                <div class="panel panel-primary ">

                    <div class="panel-body animated fadeIn">

                        @if(count($action) === 0)
                            <h3><i class="fa fa-users"></i>&nbsp; None</h3>
                        @else
                            <form id="update-item-assignee-details-form" action="/items/update/assignee-details/{{$item->id}}" method="POST">

                                <h3><i class="fa fa-users"></i>&nbsp;{{$action->assignedUser->name}} {{$action->assignedUser->surname}}</h3>

                                <p>{{$action->assignedRole->name}}</p>

                                <a data-toggle="collapse" href="#collapse-assignee-{{$item->id}}" aria-expanded="true"
                                   aria-controls="collapse-assignee-{{$item->id}}"
                                   class="hide">
                                    <i class="fa fa-users"></i>&nbsp; {{$action->assignedCompany->name}}
                                    &nbsp; <i class="pull-right fa fa-caret-down"></i>
                                </a>

                                <div id="collapse-assignee-{{$item->id}}" class="panel-collapse collapse in " role="tabpanel">

                                    <p class="font-xs text-info pad-10">Target Date: {{$item->res_target_date->format('d-m-Y')}}
                                        <small>({{$item->res_target_date->diffForHumans()}})</small>
                                    </p>

                                    <div class="form-group hide ">
                                        <label class="control-label"><i class="fa fa-pencil-square-o"></i>&nbsp; Action Taken:</label>
                                        <textarea class="longInput  form-control " name="assignee_action" cols="50" rows="3">{{$item->action}}</textarea>
                                        <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                    </div>


                                    <div class="form-group ">
                                        <label class="control-label"><i class="fa fa-comment"></i>&nbsp; Remark:</label>
                                        <textarea class="longInput  form-control " name="assignee_description" cols="50" rows="3">{{$action->comment}}</textarea>
                                        <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                                    </div>


                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="assignee-status">Status:</label>
                                            <select class=" form-control selectpicker" data-style="btn-primary"
                                                    name="assignee_status" id="assignee_status">
                                                @foreach($statusArray as $key => $value)
                                                    @if($key === $action->status)
                                                        <option value="{{$key}}" selected>{{$value}}</option>
                                                    @else
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endif
                                                @endforeach

                                            </select>
                                        </div>

                                    </div>
                                    <p class="font-xs text-muted">
                                        <small>Last updated by {{$action->lastUpdatedBy->name}} {{$action->lastUpdatedBy->surname}}

                                            {{$action->last_updated_at->diffForHumans()}}</small>
                                    </p>
                                    <hr>

                                    <!-- SERVER AND VALIDATION ERRORS -->
                                    <div class="alert alert-info fadeIn hide" id="update-item-assignee-details-form-validation-errors"></div>
                                    @if($accessLevel > 0)
                                        <button type="button"
                                                onclick="updateItemDetails('update-item-assignee-details-form','/items/update/assignee-details/{{$action->id}}',{{$action->id}})"
                                                class="btn btn-success btn-block btn-lg"><i class="fa fa-pencil"></i>&nbsp;Update
                                        </button>
                                    @endif


                                </div>
                            </form>
                            @endIf
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="attach-tab">
                {{-- ATTACHMENTS  --}}
                <div class="panel panel-info">
                    <br>

                    <div class="panel-heading hide">
                        <h3 class="panel-title collapse-action-button" role="button" data-toggle="collapse" data-parent="#accordion" href="#item-attachment-dropdown"
                            aria-expanded="true"
                            aria-controls="item-attachment-dropdown"><i class="fa fa-paperclip"></i> Attachments
                        </h3>
                    </div>

                    <div id="item-attachment-dropdown" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            @if($item->isThereAnAttachment() === 1)

                                <button class="btn btn-primary" onclick="showAttachmentsForItem({{$item->id}})">View Uploaded Attachments</button>
                            @else
                                <h3>No Attachments</h3>
                            @endif
                            <hr>
                            <form action="/item/upload-file/{{$item->id}}" id="upload-solo-attachment-form" method="post" enctype="multipart/form-data">

                                {{ csrf_field() }}

                                <label class="control-label"><i class="fa fa-files-o"></i>&nbsp; Upload New Attachment</label>

                                <p><input type="file" id="take-picture" name="attachment_file" accept=""></p>


                                <div class="form-group">
                                    <input type="text" class="form-control" name="attachment_tag" placeholder="attachment description">
                                </div>

                                <button type="button" id="upload-solo-attachment-button" onclick="updateSoloAttachment()" class="btn btn-success"><i class="fa fa-upload"></i>
                                    Upload
                                </button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- <div class=" custom-scroll  " style="height:1000px; overflow-y: scroll;"> --}}
<div class=" animated fadeInDown">


</div>

<script>
    function deleteItem(id) {
        $.ajax({
            type: 'GET',
            url: '/items/delete/' + id, success: function (result) {
                $('#item-details-modal').modal('hide');

                $.notify({
                    title: '<strong>Success!</strong><br>',
                    message: result + ""
                }, {
                    animate: {
                        enter: 'animated fadeInLeft',
                        exit: 'animated fadeOutRight'
                    },
                    type: 'success',
                    //offset: {x: 100, y: 100},
                    //placement: {from: "bottom"},
                    showProgressbar: false,
                    delay: 1500
                });


                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        });
    }

    function showUpdateDueDateSection() {
        $('#change-due-date-section').fadeIn(1000)

    }
</script>



@extends ('rowsys._app.layouts.punchlist')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')

    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <form class="form-horizontal" id="add-new-punchlist-item-form" role="form" method="POST" action="{{ url('/punchlist/add-item') }}">
        {{ csrf_field() }}
        <div class="col-sm-12">
            <div class="col-xs-4"> <!-- required for floating -->
                <!-- Nav tabs -->
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="panel-title">Settings</h4></div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="#details-v" data-toggle="tab" class="list-group-item "><i class="fa fa-pencil"></i>&nbsp;&nbsp;Details</a>
                            <a href="#location-v" data-toggle="tab" class="list-group-item"><i class="fa fa-map-pin"></i>&nbsp;&nbsp;Location</a>
                            <a href="#type-v" data-toggle="tab" class="list-group-item"><i class="fa fa-tags"></i>&nbsp;&nbsp;Type</a>
                            <a href="#date-v" data-toggle="tab" class=" list-group-item "><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Date</a>
                            <a href="#assigned-to-v" data-toggle="tab" class=" list-group-item "><i class="fa fa-user"></i>&nbsp;&nbsp;Assigned To</a>
                            <a href="#attachments-v" data-toggle="tab" class=" list-group-item "><i class="fa fa-file-image-o"></i>&nbsp;&nbsp;Attachments</a>
                            <a href="#close-out-v" data-toggle="tab" class=" list-group-item "><i class="fa fa-plus"></i>&nbsp;&nbsp;Close Out</a>
                            <br>
                            <button type="submit" class="btn btn-success btn-block"><i class="fa fa-btn fa-user"></i> Create</button>
                        </div>
                        <ul class="nav nav-tabs tabs-left sideways hide">
                            <li class="active"><a href="#home-v" data-toggle="tab">Create</a></li>
                            <li><a href="#profile-v" data-toggle="tab">Update</a></li>
                            <li><a href="#messages-v" data-toggle="tab">Roles</a></li>
                            <li class="hide"><a href="#settings-v" data-toggle="tab">Settings</a></li>
                        </ul>


                    </div>
                </div>
            </div>

            <div class="col-xs-8">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="details-v">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Item Details</h3></div>
                            <div class="panel-body animated fadeInUp">


                                <div class="form-group">
                                    <label class="col-md-2 control-label">Number:</label>

                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="name" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Description:</label>

                                    <div class="col-md-10">
                                        <textarea class="longInput form-control" name="name" cols="30" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Action:</label>

                                    <div class="col-md-10">
                                        <textarea class="longInput form-control" name="name" cols="30" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Priority:</label>
                                    <div class="col-sm-10">
                                        <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                name="systemArea">
                                            <option value="0"></option>
                                            <option value="1">High</option>
                                            <option value="2">Medium</option>
                                            <option value="3">Low</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Note:</label>

                                    <div class="col-md-10">
                                        <textarea class="longInput form-control" name="name" cols="30" rows="2"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Role</label>

                                    <div class="col-md-10">
                                        <select class=" form-control selectpicker" data-style="btn-primary" name="role">
                                            @for($i = 1;$i < 9;$i++)
                                                <option value="{{$i}}">Level {{$i}} </option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>

                                @if (Session::has('message'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fa fa-check">&nbsp; </i>{{ Session::get('message') }}
                                    </div>
                                @endif


                            </div>
                        </div>
                    </div>


                    <div class="tab-pane" id="location-v">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Location</h3></div>
                            <div class="panel-body animated fadeInUp">
                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Area:</label>

                                        <div class="col-sm-10">
                                            <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                    name="systemArea">
                                                <option>Area 1</option>
                                                <option>Area 2</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">System:</label>

                                        <div class="col-sm-10">
                                            <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                    name="systemArea">
                                                <option>System 1</option>
                                                <option>System 2</option>
                                            </select>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Building:</label>

                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="name" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Floor:</label>

                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="name" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Room:</label>

                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="name" value="">
                                        </div>
                                    </div>

                                    <hr>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Extra Info:</label>

                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="name" value="">
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="tab-pane" id="type-v">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Type</h3></div>
                            <div class="panel-body animated fadeInUp">
                                <div class="row pad-20">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Category:</label>

                                            <div class="col-sm-10">
                                                <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                        name="systemArea">
                                                    <option>Category 1</option>
                                                    <option>Category 2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Type:</label>

                                            <div class="col-sm-10">
                                                <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                        name="systemArea">
                                                    <option>Type 1</option>
                                                    <option>Type 2</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="tab-pane " id="date-v">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Date</h3></div>
                            <div class="panel-body animated fadeInUp">
                                <div class="row pad-20">

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Schedule Date:</label>
                                        <div class="col-sm-10">
                                            <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                    name="systemArea">
                                                <option></option>
                                                <option>MC</option>
                                                <option>Commissioning</option>
                                                <option>OQ</option>
                                                <option>PQ</option>
                                            </select>
                                        </div>
                                    </div>
                                    <p class="text-align-center text-primary">Schedule Date is associated to a System</p>
                                    <hr>
                                    <h2 class="text-align-center">OR</h2>




                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Due Date:</label>
                                        <div class="col-lg-10">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" class="form-control">

                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Lag (-days):</label>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control" name="name" value="0">
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="tab-pane " id="assigned-to-v">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Assigned To</h3></div>
                            <div class="panel-body animated fadeInUp">
                                <div class="row pad-20">

                                    <h2 class="text-align-center">Owner</h2>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">User Group:</label>

                                            <div class="col-sm-10">
                                                <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                        name="systemArea">
                                                    <option>Group 1</option>
                                                    <option>Group 2</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">User:</label>

                                            <div class="col-sm-10">
                                                <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                        name="systemArea">
                                                    <option>User 1</option>
                                                    <option>User 2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <br>
                                    <br>

                                    <hr>
                                    <h2 class="text-align-center">Assignee</h2>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">User Group:</label>
                                            <div class="col-sm-10">
                                                <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                        name="systemArea">
                                                    <option>Group 1</option>
                                                    <option>Group 2</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">User:</label>

                                            <div class="col-sm-10">
                                                <select class=" form-control selectpicker" onchange="getUserDetails(this.value)" data-style="btn-primary" data-live-search="true"
                                                        name="systemArea">
                                                    <option>User 1</option>
                                                    <option>User 2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="tab-pane " id="attachments-v">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Attachments</h3></div>
                            <div class="panel-body animated fadeInUp">
                                <div class="row pad-20">
                                    <div class="col-lg-12">
                                        <h2>Attachments</h2>

                                        <p>
                                            <input type="file" id="take-picture" accept="image/*">
                                        </p>

                                        <h2>Preview:</h2>
                                        <p>
                                            <img src="about:blank" alt="" id="show-picture">
                                        </p>

                                        <p id="error"></p>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane " id="close-out-v">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Close Out</h3></div>
                            <div class="panel-body animated fadeInUp">
                                <div class="row pad-20">
                                    <div class="col-lg-12">
                                        <h2>Close Out</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </form>

    @include('rowsys.punchlist.items.create.feedback_table.table')


    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script src="/js/rowsys/admin/users.js"></script>
    <script>
        $(document).ready(function () {
            $('.selectpicker').selectpicker('mobile');
            $('.datepicker').datepicker('mobile');
        });

        (function () {
            var takePicture = document.querySelector("#take-picture"),
                    showPicture = document.querySelector("#show-picture");

            if (takePicture && showPicture) {
                // Set events
                takePicture.onchange = function (event) {
                    // Get a reference to the taken picture or chosen file
                    var files = event.target.files,
                            file;
                    if (files && files.length > 0) {
                        file = files[0];
                        try {
                            // Create ObjectURL
                            var imgURL = window.URL.createObjectURL(file);

                            // Set img src to ObjectURL
                            showPicture.src = imgURL;

                            // Revoke ObjectURL
                            URL.revokeObjectURL(imgURL);
                        }
                        catch (e) {
                            try {
                                // Fallback if createObjectURL is not supported
                                var fileReader = new FileReader();
                                fileReader.onload = function (event) {
                                    showPicture.src = event.target.result;
                                };
                                fileReader.readAsDataURL(file);
                            }
                            catch (e) {
                                //
                                var error = document.querySelector("#error");
                                if (error) {
                                    error.innerHTML = "Neither createObjectURL or FileReader are supported";
                                }
                            }
                        }
                    }
                };
            }
        })();
    </script>

@stop
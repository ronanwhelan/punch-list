@extends ('rowsys._app.layouts.punchlist')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')
    <!-- ==========================CONTENT STARTS HERE ========================== -->
    <div class="animated fadeInUp">

        <div class="row">
            <div class="col-lg-offset-2 col-lg-8">
                <h4><i class="fa fa-pencil-square-o"></i> New Item</h4>

                <form action="/item/create" id="add-new-punchlist-item-form" method="POST" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5><i class="fa fa-cubes"></i> System</h5>

{{--                            <div class="form-group">
                                <label for="inputEmail3" class=" hide control-label">Area:</label>
                                <select class="form-control selectpicker" onchange="getFilteredSystemsByArea('create')" data-style="btn-primary" name="area" id="area"
                                        title="Areas">
                                    <optgroup label="Areas">
                                        <option value=""></option>
                                        @foreach($areas as $area)
                                            <option value="{{$area->id}}">{{$area->name}}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>--}}

                            <div class="form-group">
                                <label for="inputEmail3" class=" hide control-label">System:</label>
                                <select class="form-control selectpicker" onchange="getComponentsBySystem(this.value,'component');getSystemScheduleDates(this.value)" data-style="btn-primary" data-live-search="true"
                                        name="system" id="system" title="System">
                                    <optgroup label="Systems">
                                        <option value=""></option>
                                        @foreach($systems as $system)
                                            @if(Session::has('system'))
                                                @if((int)Session::get('system') === $system->id)
                                                    <option selected value="{{$system->id}}">{{$system->name}}, {{$system->description}}</option>
                                                @else
                                                    <option value="{{$system->id}}">{{$system->id}},{{$system->name}}, {{$system->description}}</option>
                                                @endif
                                            @else
                                                <option value="{{$system->id}}">{{$system->name}}, {{$system->description}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>

                                <div class="alert alert-warning alert-dismissible" role="alert" id="no-system-date-error" hidden="true">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Info!</strong> No Date for Selected System
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class=" hide control-label">Component:</label>
                                <select class="form-control selectpicker"  data-style="btn-primary" name="component" id="component" data-live-search="true"
                                        title="Component">
                                        <option value="">Please choose a system for a filter component list</option>
                                        @foreach($components as $component)
                                            <option value="{{$component->id}}">{{$component->tag}}</option>
                                        @endforeach
                                </select>
                            </div>

                            <p id="system-current-phase-text" class="font-xs text-info"></p>

                            <input type="hidden" class="form-control" id="phase_recorded_in" name="phase_recorded_in">

                            <div class="form-group  ">
                                <input type="text" class="form-control" name="system_extra_info" placeholder="Equipment or Device Tag ">
                            </div>

                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <h5><i class="fa fa-tasks"></i> Class</h5>
                                <select class="form-control selectpicker" onchange="" data-style="btn-primary" name="class" title="Class">
                                    <optgroup label="Class">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->short_name}}, {{$category->name}}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- .panel -->

                    <div class="panel panel-default">
                        <div class="panel-body ">
                            <h5><i class="fa fa-link"></i> Discipline</h5>

                            <div class="form-group">
                                <label for="inputEmail3" class="hide control-label">Owner:</label>
                                <select class=" form-control selectpicker" onchange="getFilteredItemTypesByGroup('create')" data-style="btn-primary"
                                        name="group" id="group" title="Group">
                                    <optgroup label="Group">
                                        @foreach($groups as $group)
                                            <option value="{{$group->id}}">{{$group->name}}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="hide control-label">Type:</label>
                                <select class=" form-control selectpicker" onchange="findSimilarItems()" data-style="btn-primary"
                                        name="type" id="type" title="Type">
                                    <optgroup label="Type">
                                        @foreach($types as $type)
                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>


                        </div>
                    </div>
                    <!-- .panel -->

                    <!-- .panel -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5 class="collapsed ">
                                <i class="fa fa-map-marker"></i> Location</h5>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select class=" form-control selectpicker" data-style="btn-primary"
                                                name="building" title="Buildings">
                                            @foreach($buildings as $building)
                                                <option value="{{$building->name}}">{{$building->name}} {{$building->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <select class=" form-control selectpicker" data-style="btn-primary"
                                        name="floors[]" onchange="getFilteredRoomsByFloor()" id="floors" title="Levels" multiple>
                                    @foreach($floors as $floor)
                                        <option value="{{$floor->name}}">{{$floor->name}} {{$floor->description}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row hide">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class=" control-label">Floor:</label>
                                        <input type="text" class="form-control" name="floor_old" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select class=" form-control selectpicker" data-style="btn-primary" id="rooms" onchange="findSimilarItems()"
                                                name="rooms[]" title="Rooms" multiple data-live-search="true">
                                            @foreach($rooms as $room)
                                                <option value="{{$room->name}}">{{$room->name}} {{$room->description}}</option>
                                            @endforeach
                                        </select>
                                        <input type="text" class=" hide form-control" name="room_old" value="">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="other_location" placeholder="other information e.g Ceiling">
                                    </div>
                                </div>
                            </div>

                            <div class="row hide">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class=" control-label">X Co-Ordinates:</label>
                                        <input type="text" class="form-control" name="c-cor" placeholder="e.g 14.5">
                                    </div>
                                </div>
                            </div>
                            <div class="row hide">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class=" control-label">Y Co-Ordinates: </label>
                                        <input type="text" class="form-control" name="y-cor" placeholder="e.g E.2">
                                    </div>
                                </div>
                            </div>

                            <p class=" hide font-xs text-primary">e.g B1, Level 1, Level 2, Room 1, Room 2, Ceiling</p>


                        </div>
                    </div>
                    <!-- .panel -->
                    <div class="panel panel-default">
                        <div class="panel-body animated fadeInUp">
                            <h5><i class="fa fa-camera"></i> Attachment
                                <small></small>
                            </h5>
                            <p><input type="file" id="take-picture" name="attachment_file1" accept="image/*"></p>

                            <div class="hide">
                                <p class="hide"><input type="file" multiple="true" id="take-picture" name="attachment_files[]" accept="image/*"></p>

                                <p><input type="file" id="take-picture" name="attachment_file2" accept="image/*"></p>

                                <p><input type="file" id="take-picture" name="attachment_file3" accept="image/*"></p>
                                <button type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i></button>
                            </div>

                            <div class="form-group">
                                <label class=" control-label">Note:</label>
                                <input type="text" class="form-control" name="attachment_tag" placeholder="add note to photo for reference">
                            </div>

                            <div id="attachment-preview-section" class="" hidden="true">
                                <h2>Preview:</h2>

                                <p><img src="about:blank" style="width: 100%" alt="" id="show-picture"></p>
                            </div>
                            <p id="error"></p>
                        </div>
                    </div>
                    <!-- .panel -->

                    <div class="panel panel-default">
                        <div class="panel-body ">
                            <h5><i class="fa fa-pencil-square"></i> Details</h5>

                            <div class="form-group ">
                                <label class=" control-label hide">Description:</label>
                                <textarea class="longInput form-control" name="description" placeholder="Describe the finding" cols="30" rows="3"></textarea>
                                <!-- <button type="button" class="btn btn-xs"> <i class="fa fa-info-circle"></i></button> -->
                            </div>
                            {{-- ACTION  --}}
                            <div class="form-group ">
                                <small><label class="control-label text-muted">Action Required:</label></small>
                                <textarea class="longInput  form-control " placeholder="What needs to be done" name="action" cols="50" rows="1"></textarea>
                            </div>
                            {{-- NOTE  --}}
                            <div class="form-group hide ">
                                <small><label class="control-label text-muted">Note:</label></small>
                                <textarea class="longInput  form-control " name="note" cols="50" rows="2"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- .panel -->


                    <div class="panel panel-default">
                        <div class="panel-body ">
                            <h5><i class="fa  fa-exclamation-triangle"></i> Priority</h5>

                            <div class="form-group ">
                                <label class=" control-label hide">Category:</label>
                                <select class=" form-control selectpicker" data-style="btn-primary" name="category" id="category"
                                        onchange="showCatBDatesAndUpdateDueDate(this.value)" title="Category">
                                    <optgroup label="Category">
                                        @foreach($priorities as $priority)
                                            @if($priority->id === 1)
                                                <option value="{{$priority->id}}" selected>{{$priority->name}}, {{$priority->description}}</option>
                                            @else
                                                <option value="{{$priority->id}}">{{$priority->name}}, {{$priority->description}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>

                                <input type="hidden" id="cat-a-date" value="">
                                <input type="hidden" id="cat-a-step" value="">
                                <input type="hidden" id="cat-a-step-number" name="category_a_choice" value="1">
                            </div>

                            <div hidden="true" id="category-b-options-section">
                                <div class="form-group ">
                                    <label for="inputEmail3" class="control-label hide">Category B Date Selection:</label>
                                    <select class=" form-control selectpicker" onchange="updateDueDate()" data-style="btn-primary"
                                            id="cat_b_schedule_choice" name="category_b_choice" title="Category B Date Selection"></select>

                                    <input type="hidden" id="cat-b-mc-date" value="">
                                    <input type="hidden" id="cat-b-com-date" value="">
                                    <input type="hidden" id="cat-b-oq-date" value="">
                                    <input type="hidden" id="cat-b-pq-date" value="">
                                </div>
                                <p class=" hide text-primary">Schedule Date is associated to a System</p>
                            </div>


                            <div id="category-c-options-section" hidden="true">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class=" control-label">Choose Date:</label>

                                            <div class="input-group date">
                                                <input type="date" id="cat_c_due_date" onchange="updateDueDate()" name="cat_c_due_date" class=" datepicker"
                                                       data-date-format="yyyy-mm-dd"
                                                       value="">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class=" control-label">Lead (- days):</label>
                                <input type="text" class="form-control" name="lag_days" value="0">
                            </div>

                            <hr>
                            <div class="col-md-8">
                                <h5><i class="fa fa-calendar-o"></i> Due Date</h5>

                                <div id="due-date-section" class="">
                                    <h4 id="due-date-for-show">No System Selected</h4>
                                    <input type="hidden" id="due-date" name="due_date" value="">
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input class="" type="checkbox" name="urgent"> <i class="fa  fa-exclamation-triangle"></i> Urgent
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .panel -->

                    <!-- .panel -->

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h5><i class="fa fa-users"></i> Responsible Party:</h5>
                            <div class="form-group">
                                <label for="inputEmail3" class=" hide control-label">Responsible Team:</label>
                                <select class=" form-control selectpicker" onchange="getUsersByRole(this.value,'attention_to')" data-style="btn-primary"
                                        name="responsible_role" id="responsible_role" title="Responsible Party">
                                    <option value=""></option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>

{{--
                            <div class="form-group ">
                                <label for="res-status">Responsible Team</label>
                                <select class=" form-control selectpicker" data-style="btn-primary"
                                        name="responsible_team" id="responsible_team" onchange="getUsersByTeam(this.value,'attention_to')">
                                    <option value="0"></option>
                                    @foreach($teams as $team)
                                            <option value="{{$team->id}}">{{$team->name}}</option>
                                    @endforeach
                                </select>
                            </div>--}}


                            <div class="form-group hide">
                                <label for="inputEmail3" class=" hide control-label">Responsible Company:</label>
                                <select class=" form-control selectpicker" onchange="getFilteredUserByCompany()" data-style="btn-primary"
                                        name="responsible_party" id="responsible_party" title="Responsible Company">
                                    <option value=""></option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group ">
                                <label for="inputEmail3" class=" control-label"> Attention:</label>
                                <select class=" form-control selectpicker" onchange="" data-style="btn-primary"
                                        name="attention_to" id="attention_to" title="Responsible Individual">
                                    <option value=""></option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}  {{$user->surname}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>
                    <!-- .panel -->

                    <!-- SERVER AND VALIDATION ERRORS -->
                    <div class="alert alert-danger fadeIn hide" id="validation-errors"></div>


                    <button type="button" id="add-new-item-button" onclick="ValidateAndSubmit('add-new-punchlist-item-form','/item/create/validate')"
                            class="btn btn-success btn-block btn-lg"><i class="fa fa-btn fa-plus"></i>&nbsp; Submit
                    </button>
                </form>

            </div>
            <!-- .col -->
        </div>
        <br>

        <div class="row" id="similar-info-section" hidden>
            <div class="col-lg-offset-2 col-lg-8">
                <div class="alert alert-info" role="alert"><p>
                        <a role="button" id="similar-items-text" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                           aria-controls="collapseOne">
                            <i class="fa fa-info-circle"></i>&nbsp;4 items already open in this location
                        </a>
                        &nbsp; <i class="pull-right fa fa-caret-down"></i>
                    </p>

                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <table class="table">
                                            <tbody id="similar-item-table-body"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .panel -->
            </div>
            <!-- .col -->
        </div>
        <!-- .row -->
    </div><!-- .container -->

    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')

    <script>
        $(document).ready(function () {
            checkIfOnMobile();
            updateDueDate();

            $('.datepicker').datepicker({
                keyboardNavigation: true,
                calendarWeeks: true,
                autoclose: true
            });


            if (screen.width <= 1300) {
                $('.datepicker').datepicker('destroy');
                //$('.selectpicker').selectpicker('mobile');
                // $('.datepicker').datepicker('destroy');
                //alert('on a mobile');
            }

            checkIfNewItem();
        });

        /** ===============================================================
         *
         *  Add Punch List Item
         *
         *  adds the item over AJAX
         *
         * ===============================================================
         */
        function addPunchListItem(formName, urlText) {

            //a token is needed to prevent cross site
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var formId = '#' + formName;
            var form = $(formId);
            var method = form.find('input[name="_method"]').val() || 'POST';
            var datastring = $(form).serialize();
            var formData = new FormData($(form));
            var validationSection = $('#validation-errors');
            $.ajax({
                type: 'GET',
                url: '/item/create/validate',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                // processData: false,  // tell jQuery not to process the data
                //contentType: false,   // tell jQuery not to set contentType
                success: function (result) {
                    console.log(result);
                    $.notify({
                        title: '<strong>Success!</strong><br>',
                        message: result + ""
                    }, {
                        animate: {
                            enter: 'animated fadeInLeft',
                            exit: 'animated fadeOutRight'
                        },
                        type: 'success',
                        //offset: {x: 100, y: 100},
                        //placement: {from: "bottom"},
                        showProgressbar: false,
                        delay: 1000
                    });
                    setTimeout(function () {
                        //window.location.reload(true);
                    }, 1500);
                    validationSection.addClass('hide');
                    //getUserDetails(userId);
                },
                beforeSend: function () {
                    // pleaseWait();
                },
                complete: function () {
                    //waitDone();
                },
                // Handles the Errors if returned back from the Server -
                // Including any Validation Errors
                error: function (xhr, status, error) {
                    waitDone();
                    validationSection.removeClass('hide').html('<br><i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
                    var errorText = '';
                    var validationErrorText = ' Missing Required Fields';
                    var list = $("<ul class=''></ul>");
                    var contactAdminText = ' - Contact the PES Administrator';

                    // If there is a Validation Error then show the user the validation error Text
                    if (error === 'Unprocessable Entity' || xhr.status === 422) {
                        //validationSection.append(validationErrorText);
                        jQuery.each(xhr.responseJSON, function (i, val) {
                            validationErrorText = '<li>' + val + '</li>';
                            list.append(validationErrorText);
                            console.log(validationErrorText);
                        });
                    } else {
                        errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                        errorText = errorText + contactAdminText;
                    }
                    validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
                }
            });
        }

        function ValidateAndSubmit(formName, urlText) {

            var submitButton = $('#add-new-item-button');

            var validatePassed = validateForm(formName, urlText);
            if (validatePassed === 'pass') {
                console.log('validation pass');
                submitButton.append('<i class="fa fa-spinner fa-spin"></i>').addClass('disabled');
                setTimeout(function(){
                    $("#" + formName).submit();
                }, 300);

                console.log('message = ' + validatePassed);

            } else if (validatePassed === 'fail') {
                console.log('validation Fail');
                $('html, body').scrollTop($(document).height() - $(window).height());

            } else {
                console.log('No Internet Connection');
                alert('you dont seem to be connected to the internet');
            }


            /*            var hasConnection = doesConnectionExist();
             if(hasConnection){
             }else{
             alert('you dont seem to be connected to the internet');
             }*/

        }


        function validateForm(formName, urlText) {
            var returnMessage = 'fail';
            //a token is needed to prevent cross site
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var formId = '#' + formName;
            var form = $(formId);
            var datastring = $(form).serialize();
            var validationSection = $('#validation-errors');
            $.ajax({
                type: 'GET',
                url: urlText,
                data: datastring,
                async: false,
                success: function (result) {
                    console.log(result);
                    if (result === 'pass') {
                        returnMessage = 'pass';
                    }
                    validationSection.addClass('hide');
                },
                beforeSend: function () {
                    // pleaseWait();
                },
                complete: function () {
                    //waitDone();
                },
                // Handles the Errors if returned back from the Server -
                // Including any Validation Errors
                error: function (xhr, status, error) {

                    console.log(xhr);
                    if (xhr.status === 422) {

                    } else {
                        returnMessage = '';
                    }

                    waitDone();
                    validationSection.removeClass('hide').html('<i class="fa fa-info-circle"></i><strong> Required Fields Missing</strong><br>');
                    var errorText = '';
                    var validationErrorText = ' Required Fields Missing';
                    var list = $('<ul class=""></ul>');
                    var contactAdminText = ' - Contact the PES Administrator';
                    // If there is a Validation Error then show the user the validation error Text
                    if (error === 'Unprocessable Entity' || xhr.status === 422) {
                        //validationSection.append(validationErrorText);
                        jQuery.each(xhr.responseJSON, function (i, val) {
                            validationErrorText = '<li class="">' + val + '</li>';
                            list.append(validationErrorText);
                            //console.log(validationErrorText);
                        });
                    } else {
                        errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                        errorText = errorText + contactAdminText;
                    }
                    validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
                }
            });
            return returnMessage;
        }


        function doesConnectionExist() {
            var xhr = new XMLHttpRequest();
            var file = "http://www.rowsys.co";
            var randomNum = Math.round(Math.random() * 10000);

            xhr.open('HEAD', file + "?rand=" + randomNum, false);

            try {
                xhr.send();

                if (xhr.status >= 200 && xhr.status < 304) {
                    return true;
                } else {
                    return false;
                }
            } catch (e) {
                return false;
            }
        }


        /*        (function () {
         var takePicture = document.querySelector("#take-picture"),
         showPicture = document.querySelector("#show-picture");

         if (takePicture && showPicture) {
         // Set events
         takePicture.onchange = function (event) {
         // Get a reference to the taken picture or chosen file
         var files = event.target.files,
         file;
         if (files && files.length > 0) {
         file = files[0];
         try {
         // Create ObjectURL
         var imgURL = window.URL.createObjectURL(file);

         // Set img src to ObjectURL
         showPicture.src = imgURL;

         // Revoke ObjectURL
         URL.revokeObjectURL(imgURL);

         $('#attachment-preview-section').fadeIn(1500);
         }
         catch (e) {
         try {
         // Fallback if createObjectURL is not supported
         var fileReader = new FileReader();
         fileReader.onload = function (event) {
         showPicture.src = event.target.result;
         };
         fileReader.readAsDataURL(file);
         }
         catch (e) {
         //
         var error = document.querySelector("#error");
         if (error) {
         error.innerHTML = "Neither createObjectURL or FileReader are supported";
         }
         }
         }
         }
         };
         }
         })();*/


    </script>

@stop

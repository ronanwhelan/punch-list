@extends ('rowsys._app.layouts.punchlist')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')


            <!-- ==========================CONTENT STARTS HERE ========================== -->
            <h2>Hello from Rowsys Default Index</h2>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a  class="btn btn-default" href="/sync/punchlist" role="button">Sync Data From Main App</a>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a class="btn btn-default" href="/schedule-links/sync/xx" role="button">Sync Schedule Links</a>
                    <p> /schedule-links/sync/{activity} </p>
                </div>
            </div>


            <!-- ==========================CONTENT ENDS HERE ========================== -->




@stop


@section ('local_scripts')

    <script>
        $(document).ready(function () {
            //
        });
    </script>

@stop

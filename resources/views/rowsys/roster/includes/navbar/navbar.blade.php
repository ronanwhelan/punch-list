<!-- Side Navigation -->
<nav class="nav-slide">
    <div class="ns-header">
        <a href="#0" class="nav-close"><i class="fa fa-times" aria-hidden="true"></i></a>
        <a href="/roster/create" class="nav-close-logo"><img src="/img/branding/figaro-logo.png" alt="Logo"></a>
    </div>

    <ul>
        <li class="dashboard hide ">
            <a href="/design-documents/dashboard" class="">Dashboard</a>
        </li>

        <li class="user">
            <a href="/roster/create" class="">User Time</a>
        </li>
{{--        <li class="user">
            <a href="/roster/import/user" class="">Import User Time</a>
        </li>--}}




        <li class="areas">
            <a href="/roster/area/create" class="">Area</a>
        </li>
        <li class="areas">
            <a href="/roster/import/area" class="">Import Area Roster</a>
        </li>



        <li class="user visible-xs">
            <a href="javascript:showUserSettingsModal({{ Auth::user()->id }});" class="">Account Settings</a>
        </li>
        <li class="sign-out visible-xs">
            <a href="/logout" class="">Logout</a>
        </li>

        <li class="main-menu">
            <a href="/welcome" class="">Main Menu</a>
        </li>

    </ul>
</nav> <!-- .nav-slide -->
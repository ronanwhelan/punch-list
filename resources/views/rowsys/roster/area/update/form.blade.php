<form id="update-model-form" action="/roster/1">
    <input type="hidden" name="_method" value="PUT">
    {{ csrf_field() }}

    <input type="hidden" id="task_id" name="update_task_id" value="">

    <div class="form-group">
        <label for="">Area</label>
        <input type="text" class="form-control" id="name" name="update_name" value="">
    </div>

    <div class="form-group">
        <label for="">Month</label>
        <input type="text" class="form-control" id="short-name" name="short_name" value="">
    </div>

    <div class="form-group">
        <label for="">Hours</label>
        <textarea class="form-control" name="update_description" id="update_description" rows="4" cols="6"></textarea>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="updateCategoryModel('update-model-form', '/update-group-details','1')" class="btn btn-success">Update</button>

        @if( Auth::user()->role > 7)
            <button type="button" onclick="showModelDeleteSection(1)" class="btn btn-danger pull-left">Delete</button>
        @endif
    </div>
    <div id="are-you-sure-delete-section" class="hide">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times; </span></button>
            <p class="">Are you Sure <i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;
                <button type="button" onclick="deleteGroup(1)" class="btn btn-danger"> Yes</button>
            </p>
        </div>
    </div>
</form>
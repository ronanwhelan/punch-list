<div class="row">
    <div id="myTabs" class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Table</a></li>
                    <li class="hide"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Graph Overview</a></li>
                    <li class="hide"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Individual</a></li>
                </ul>

                <!-- Tab panes -->
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <div class="col-md-12">


                            <table  id="data_table" class="table table-striped table-bordered">
                                <thead>
                                <tr class="font-xs">
                                    <th class="font-xs">Area</th>
                                    <th class="font-xs">Month</th>
                                    <th class="font-xs">Hours</th>
                                    <th class="font-xs">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($areaHoursData as $data)
                                    <tr>
                                        <td>{{$data->area->name}}</td>
                                        <td>{{$data->month}} {{$data->year}}</td>
                                        <td><a href="#" id="hours-{{$data->id}}" data-type="text" data-name='hours' data-pk="{{$data->id}}"
                                               data-url="/roster/area/update-hours/{{$data->id}}" data-title="Enter Hours">{{$data->hours}}</a></td>
                                        <td>
                                            <button class="btn btn-danger btn-sm" onclick="deleteAreaHoursConfirmation({{$data->id}})"><i class="fa fa-times"></i> Delete</button>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>


                        <div role="tabpanel" class="tab-pane fade" id="profile">
                            <h3>Tab 2</h3>

                        </div>


                        <div role="tabpanel" class="tab-pane fade" id="messages">
                            <br>
                            <div class="form-group">
                                <select class="selectpicker form-control show-tick" id="user" name="area"
                                        data-style="btn-primary" data-live-search="true"  data-max-options="1" title="Choose Area"
                                        onchange="drawUserHoursChart(this.value)"
                                        >
                                    <optgroup label="Users">
                                        @foreach ($areas as $area)
                                            @if ((int)old('area') === $area->id)
                                                <option value="{{$area->id}}" selected>{{$area->name}}  {{$area->description}}</option>
                                            @else
                                                <option value="{{$area->id}}">{{$area->name}}  {{$area->description}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>

                            <canvas id="myChart" width="400" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="row">
    <div id="myTabs" class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Table</a></li>
                    <li class="hide"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Graph Overview</a></li>
                    <li class="hide"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Individual</a></li>
                </ul>

                <!-- Tab panes -->
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <div class="col-md-12">
                            <br>
                            <div class="">
                                <div class="row hide">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div id="buttons" class="font-xs"></div>
                                                <div id="toggle_buttons" class="btn-group pull-right">
                                                    <button class="btn dropdown-toggle  btn-info" data-toggle="dropdown">
                                                        Toggle column <i class="fa fa-caret-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a class="toggle-vis" data-column="0">User</a>
                                                        </li>
                                                        <li>
                                                            <a class="toggle-vis" data-column="1">Area</a>
                                                        </li>
                                                        <li>
                                                            <a class="toggle-vis" data-column="2">Month</a>
                                                        </li>
                                                        <li>
                                                            <a class="toggle-vis" data-column="3">Hours</a>
                                                        </li>
                                                        <li>
                                                            <a class="toggle-vis" data-column="4">Delete</a>
                                                        </li>


                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="row" id="table-section" style="opacity: .08">
                                        <div class="col-lg-12 font-xs">
                                            <table id="data_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                <tr class="font-xs">
                                                    <th data-class="expand" class="font-xs">User</th>
                                                    <th data-hide="phone" class="font-xs">Area</th>
                                                    <th data-hide="phone" class="font-xs">Month</th>
                                                    <th data-hide="phone" class="font-xs">Hours</th>
                                                    <th data-hide="phone" class="font-xs">Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($userAreaHoursData as $data)
                                                    <tr>
                                                        <td>{{$data->user->name}} {{$data->user->surname}}</td>
                                                        <td>{{$data->area->name}}</td>
                                                        <td>{{$data->month}} {{$data->year}}</td>
                                                        <td><a href="#" id="hours-{{$data->id}}" data-type="text" data-name='hours' data-pk="{{$data->id}}"
                                                               data-url="/roster/update-hours/{{$data->id}}" data-title="Enter Hours">{{$data->hours}}</a></td>
                                                        <td>
                                                            <button class="btn btn-danger btn-sm" onclick="deleteUserAreaHoursConfirmation({{$data->id}})"><i class="fa fa-times"></i> Delete</button>
                                                        </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="profile">
                            <h3>Tab 2</h3>


                            <table class="highchart" data-graph-container-before="1" data-graph-type="column" style="display:none" data-graph-datalabels-enabled="1" data-graph-color-1="#999">
                                <thead>
                                <tr>
                                    <th>Month</th>
                                    <th>Area1</th>
                                    <th>Area2</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>January</td>
                                    <td>8000</td>
                                    <td>0</td>

                                </tr>
                                <tr>
                                    <td>February</td>
                                    <td >12000</td>
                                    <td>10003</td>
                                </tr>
                                <tr>
                                    <td>March</td>
                                    <td>18000</td>
                                    <td>0</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="messages">
                            <br>
                            <div class="form-group">
                                <select class="selectpicker form-control show-tick" id="user" name="user"
                                        data-style="btn-primary" data-live-search="true"  data-max-options="1" title="Choose User"
                                        onchange="drawUserHoursChart(this.value)"
                                        >
                                    <optgroup label="Users">
                                        @foreach ($users as $user)
                                            @if ((int)old('user') === $user->id)
                                                <option value="{{$user->id}}" selected>{{$user->name}}  {{$user->surname}}</option>
                                            @else
                                                <option value="{{$user->id}}">{{$user->name}}  {{$user->surname}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>

                            <canvas id="myChart" width="400" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


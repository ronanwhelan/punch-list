<form id="update-model-form" action="/schedule/update" method="POST">
    {{ csrf_field() }}

    <input type="hidden" id="activity_id" name="update_activity_id" value="{{$scheduleActivity->id}}">

    <div class="form-group">
        <label for="">Number / ID</label>
        <input type="text" class="form-control" id="name" name="update_number" value="{{$scheduleActivity->number}}">
    </div>
    <div class="form-group">
        <label for="">Description</label>
        <textarea class="form-control" name="update_description" id="update_description" rows="4" cols="6">{{$scheduleActivity->description}}</textarea>
    </div>


    <div class="form-group">
        <label for="exampleInputEmail1">Start Date</label>
        <input type="date" id="search-from-date" name="start_date" class=" datepicker"
               data-date-format="yyyy-mm-dd"
               value="{{$scheduleActivity->start_date->toDateString()}}">
    </div>

    <div class="form-group">
        <label for="exampleInputEmail1">Finish Date</label>
        <input type="date" id="search-from-date" name="finish_date" class=" datepicker"
               data-date-format="yyyy-mm-dd"
               value="{{$scheduleActivity->finish_date->toDateString()}}">
    </div>

    <div class="alert alert-info" role="alert">
    @if($scheduleActivity->imported_data === 1)
    <p>Activity imported (from Project Scheduler)</p>
    @else
        <p>Local activity - not from Scheduler</p>
        @endif
    </div>

    <div class="modal-footer">


        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="updateCategoryModel('update-model-form', '/schedule/update','{{$scheduleActivity->id}}')" class="btn btn-success">Update</button>

        @if($taskCount === 0 && Auth::user()->role > 7)
            <button type="button" onclick="showModelDeleteSection({{$taskCount}})" class="btn btn-danger pull-left">Delete</button>
        @endif

    </div>
    <div id="are-you-sure-delete-section" class="hide">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times; </span></button>
            <p class="">Are you Sure <i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;
                <button type="button" onclick="deleteActivity({{$scheduleActivity->id}})" class="btn btn-danger"> Yes</button>
            </p>
        </div>
    </div>
</form>

<div class="alert alert-warning" role="alert">
    {{$taskCount}} Tasks are associated to this Activity</div>
<p class="txt-color-orange font-xs"> Note: if you wish to delete this Activity, first delete all associated Tasks</p>
<script>
    $(document).ready(function () {

    });

    //=========  DELETE A MODEL ==============
    function showModelDeleteSection(taskCount) {
        if (taskCount === 0) {
            $('#are-you-sure-delete-section').removeClass('hide');
        } else {
            alert('Please delete All associated tasks first before Deleting')
        }
    }

    function deleteActivity(id){
        $.ajax({
            type: 'GET',
            url: '/schedule/delete/' + id, success: function (result) {
                $('#update-modal').modal('hide');

                $.notify({
                    title: '<strong>Success!</strong><br>',
                    message: result + ""
                }, {
                    animate: {
                        enter: 'animated fadeInLeft',
                        exit: 'animated fadeOutRight'
                    },
                    type: 'success',
                    //offset: {x: 100, y: 100},
                    //placement: {from: "bottom"},
                    showProgressbar: false,
                    delay: 1500
                });

                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        });
    }

</script>
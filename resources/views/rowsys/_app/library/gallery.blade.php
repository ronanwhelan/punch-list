



<h1>Gallery</h1>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 ">
        <div class="gallery-filter">
            <a class="current" data-filter="*" href="#">All</a>
            <a class="" data-filter=".image" href="#">Images</a>
            <a class="" data-filter=".pdf" href="#">PDF</a>
        </div>
    </div>
</div>

<div class="row">

    <div class="gallery-container pad-20">

        <div class="col-sm-6 col-lg-3 col-md-4 gallery image">
            <div class="gal-detail thumb">
                <a href="/img/gallery/700x400.png" class="img-popup">
                    <img src="/img/gallery/700x400.png" class="img-responsive">
                </a>

                <h3>Image 01</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 col-md-4 gallery pdf">
            <div class="gal-detail thumb">
                <a href="/img/gallery/700x400.png" class="img-popup">
                    <img src="/img/gallery/700x400.png" class="img-responsive">
                </a>

                <h3>Image 02</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 col-md-4 gallery image">
            <div class="gal-detail thumb">
                <a href="/img/gallery/700x400.png" class="img-popup">
                    <img src="/img/gallery/700x400.png" class="img-responsive">
                </a>

                <h3>Image 03</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 col-md-4 gallery pdf">
            <div class="gal-detail thumb">
                <a href="/img/gallery/700x400.png" class="img-popup">
                    <img src="/img/gallery/700x400.png" class="img-responsive">
                </a>

                <h3>Image 04</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 col-md-4 gallery image">
            <div class="gal-detail thumb">
                <a href="/img/gallery/700x400.png" class="img-popup">
                    <img src="/img/gallery/700x400.png" class="img-responsive">
                </a>

                <h3>Image 05</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 col-md-4 gallery pdf">
            <div class="gal-detail thumb">
                <a href="/img/gallery/700x400.png" class="img-popup">
                    <img src="/img/gallery/700x400.png" class="img-responsive">
                </a>

                <h3>Image 06</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
        </div>

    </div>
</div>


<script src="/js/plugin/jquery.magnific-popup.min.js"></script>
<script src="/js/plugin/isotope.pkgd.min.js"></script>
@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')

@stop

@section ('head_js')
@stop

@section('content')

<div class="row animated fadeInUp">
    <div class="col-lg-4 col-md-6">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title">My Tasks</h4>

            <div class="widget-box">
                <div class="widget-detail">
                    <h2 class="amount">341</h2>
                    <p class="text-muted">Progress</p>
                </div>
                <div class=" hide progress progress-bar-success-alt progress-sm">
                    <div style="width: 77%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="77" role="progressbar" class="progress-bar progress-bar-danger">
                        <span class="sr-only">77% Complete</span>
                    </div>
                </div>
            </div>
        </div><!-- end .card-box -->
    </div><!-- end col -->

    <div class="col-lg-4 col-md-6">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title">Overdue</h4>

            <div class="widget-box">
                <div class="widget-detail">
                    <h2 class="amount">10</h2>
                    <p class="text-muted">Progress</p>
                </div>
                <div class="progress progress-bar-danger-alt progress-sm">
                    <div style="width: 12%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="12" role="progressbar" class="progress-bar progress-bar-danger">
                        <span class="sr-only">12% Complete</span>
                    </div>
                </div>
            </div>
        </div><!-- end .card-box -->
    </div><!-- end col -->

    <div class="col-lg-4 col-md-6">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title">New Taks</h4>

            <div class="widget-box">
                <div class="widget-detail">
                    <h2 class="amount">3</h2>
                    <p class="text-muted">Progress</p>
                </div>
                <div class="progress progress-bar-warning-alt progress-sm">
                    <div style="width: 0%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-bar progress-bar-warning">
                        <span class="sr-only">0% Complete</span>
                    </div>
                </div>
            </div>
        </div><!-- end .card-box -->
    </div><!-- end col -->

</div><!-- end .row -->

<div class="row animated fadeInUp">
    <div class="col-lg-6">
        <div class="card-box">
        <div class="dropdown pull-right">
                <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title">Graph 1</h4>

            <div class="widget-chart">
                
            </div>
            
        </div> 
    </div><!-- end .col -->

    <div class="col-lg-6">
        <div class="card-box">
        <div class="dropdown pull-right">
                <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title">Graph 2</h4>

            <div class="widget-chart">
                
            </div>
            
        </div> 
    </div><!-- end .col -->

</div><!-- end .row -->

<div class="row animated fadeInUp">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle card-drop" href="#">
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Latest Tasks</h4>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Task Name</th>
                        <th>Start Date</th>
                        <th>Due Date</th>
                        <th>Status</th>
                        <th>Assign</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>26/04/2016</td>
                            <td><span class="label label-danger">Released</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>26/04/2016</td>
                            <td><span class="label label-success">Released</span></td>
                            <td>Adminto admin</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/05/2016</td>
                            <td>10/05/2016</td>
                            <td><span class="label label-info">Pending</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-info">Work in Progress</span>
                            </td>
                            <td>Adminto Joe Blogs</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-warning">Coming soon</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>

                        <tr>
                            <td>6</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-primary">Coming soon</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>

                        <tr>
                            <td>7</td>
                            <td>SG12.050.M7.DCS03.TS815.IQTEST041</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-primary">Coming soon</span></td>
                            <td>Adminto Joe Blogs</td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div><!-- end .card-box -->
    </div><!-- end .col -->
</div><!-- end .row -->

@stop

@section ('local_scripts')
    <!-- Load HIGHCHARTS -->
    <script src="/js/plugin/highChartCore/highcharts-custom.min.js"></script>
    <script src="/js/plugin/highchartTable/jquery.highchartTable.min.js"></script>
<script>
    $(document).ready(function() {
        $('table.highchart').highchartTable();
    });
</script>

@stop
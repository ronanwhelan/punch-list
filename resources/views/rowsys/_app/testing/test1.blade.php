@extends ('rowsys._app.layouts.app_master')

@section ('page_related_css')
@stop

@section ('head_js')
@stop

@section('content')


    <!-- ==========================CONTENT STARTS HERE ========================== -->

<h4>Autocomplete</h4>

    <div class="ui-widget">
        <label for="tags">Tags: </label>
        <input id="tags">
    </div>


<hr>


    <p>Overview Status Bar Chart</p>
    <button type="button" onclick="StopPulseTimer()" class=" hide btn btn-primary">Resize</button>
    <div class="row">
        <div class="col-lg-3">
            <div id="barChartPanel" class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Project Preparation</h3></div>
                <div class="panel-body">

                    <br>
                    <div id="chart-section" class="" style="">
                        <canvas id="percentage-chart-canvas" width="1002" height="250" class="percentage-chart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <!-- ==========================CONTENT ENDS HERE ========================== -->

@stop


@section ('local_scripts')
    <!-- Load APP scritps -->
    <script src="/js/rowsys/tracker/tasks/graphs.js"></script>

    <script>
        $(function() {
            var availableTags = [
                "ActionScript",
                "AppleScript",
                "Asp",
                "BASIC",
                "C",
                "C++",
                "Clojure",
                "COBOL",
                "ColdFusion",
                "Erlang",
                "Fortran",
                "Groovy",
                "Haskell",
                "Java",
                "JavaScript",
                "Lisp",
                "Perl",
                "PHP",
                "Python",
                "Ruby",
                "Scala",
                "Scheme"
            ];
            $( "#tags" ).autocomplete({
                source: availableTags
            });
        });
    </script>


    <script>
        //percentageChart.attr({width:screenWidth, height:screenWidth});



        $(document).ready(function () {
            var chartSectionId = 'chart-section';
            var chartId = 'percentage-chart-canvas';
            // var i = setInterval(drawPercentageChart, 200);
            var target = .44;
            var actual = .48;
            drawPercentageChart(chartId,chartSectionId, target, actual);
            var title = $("em").attr("title");


// to stop the counter

            /*
             var indexVariable = 0;
             while(indexVariable < 10){
             setInterval(function () {
             indexVariable++;
             //indexVariable = ++indexVariable % 360 + 1; // SET { 1-360 }
             console.log(indexVariable);
             }, 100);
             }*/
            //
            //drawPercentageChart(.1,.2,canvasWidth,canvasHeight);


            //drawPercentageChartBars(canvas,.4,.6);
            //var canvasWidth = mainCanvas.width;
            //var canvasHeight = mainCanvas.height;

            //$(window).height();   // returns height of browser viewport
            // $(document).height(); // returns height of HTML document
            // $(window).width();   // returns width of browser viewport
            //$(document).width();

            // var canvasWidth = screenWidth * .85;
            //var canvasHeight = screenWidth * .2;

            //$('#chart-section').slideDown( "slow" );
            //$("#chart-section").effect("bounce", { times:2 }, 600);
            //$('#chart-section').show().animate({ top: 305 }, {duration: 1000, easing: 'easeOutBounce'});


        });

        function stopPulse() {
            drawPercentageChart('percentage-chart-frame', .5, .6);
            console.log('function called');
            //clearInterval(i);
        }

        chartId = 'percentage-chart-bar';
        //drawPercentageChartBar(chartId,.5);

        function drawPercentageChartBar(chart_id, value) {
            var canvas = document.getElementById(chart_id);
            var canvasWidth = canvas.width;
            var canvasHeight = canvas.height;
            var rectValues = {
                'x': 0,
                'y': canvasHeight * .05,
                'width': (value * canvasWidth),
                'height': canvasHeight * .7,
                'fill': 'rgba(26, 68, 126, 0.3)'
            };

            if (canvas.getContext) {
                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, canvasWidth, canvasHeight);

                ctx.beginPath();
                ctx.rect(rectValues.x, rectValues.y, rectValues.width, rectValues.height);
                ctx.fillStyle = rectValues.fill;
                ctx.fill();

                ctx.closePath();

            }
        }


        // var canvasWidth = canvas.width;
        //var canvasHeight = canvas.height;
        /*

         var requestAnimationFrame = window.requestAnimationFrame ||
         window.mozRequestAnimationFrame ||
         window.webkitRequestAnimationFrame ||
         window.msRequestAnimationFrame;
         */



        //drawPercentageChart(.1,.2,canvasWidth,canvasHeight,.8);
        //draw(1);

        //requestAnimationFrame();
        //drawPercentageChart(.1,.2,canvasWidth,canvasHeight,.8)


        var myInterval;

        function StopPulseTimer() {
            clearInterval(myInterval);

        }


    </script>

    <script type="application/javascript">


        function drawPercentageChartBars(canvas, target, actual, width, height) {
            var ctx = canvas.getContext("2d");
            var targetRect = {
                'x': 0,
                'y': height * .05,
                'width': (target * width),
                'height': height * .7,
                'fill': 'rgba(26, 68, 126, 0.3)'
            };

            var actualText = (actual * 100) + '%';
            var targetText = (target * 100) + '%';
            var fillColour = "rgb(21, 198, 28)";
            if (actual < target) {
                fillColour = "rgb(255, 0, 0)";
            }

            ctx.beginPath();

            var percentageFont = (28 * (width * .001)) + 'px sans-serif';
            //Target %
            ctx.rect(targetRect.x, targetRect.y, targetRect.width, targetRect.height);
            ctx.fillStyle = targetRect.fill;
            ctx.fill();

            ctx.fillStyle = "rgba(26, 68, 126, 0.8)";
            ctx.font = percentageFont;
            ctx.fillText(targetText, (target * width) * .92, height * .11);

            //Actual %
            ctx.fillStyle = fillColour;
            ctx.fillRect(0, height * .15, actual * width, height * .5);

            ctx.fillStyle = "rgb(0, 0, 0)";
            ctx.font = percentageFont;
            ctx.fillText(actualText, (actual * (width * .92)), height * .4);

            ctx.closePath();


        }

        function drawPercentageChartOld(actual, target, width, height) {
            if (canvas.getContext) {
                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, width, height);

                var targetRect = {
                    'x': 0,
                    'y': height * .05,
                    'width': (target * width),
                    'height': height * .7,
                    'fill': 'rgba(26, 68, 126, 0.3)'
                };
                var actualRect = {
                    'x': 50,
                    'y': 50,
                    'width': 100,
                    'height': 100,
                    'fill': '#000000'
                };
                var actualText = (actual * 100) + '%';
                var targetText = (target * 100) + '%';
                var fillColour = "rgb(21, 198, 28)";
                if (actual < target) {
                    fillColour = "rgb(255, 0, 0)";
                }

                ctx.beginPath();
                // ====  PERCENTAGE LEGEND  =========
                var legendFont = (20 * (width * .001)) + 'px sans-serif';
                //Line
                ctx.fillStyle = "rgb(128, 128, 128)";//rgb(26, 68, 126) "rgba(0, 0, 200, 0.5)"
                ctx.fillRect(0, height * .8, width * .995, height * .01);

                //0%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("0%", 0, height * .95);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(0, height * .8, width * 0.005, height * 0.08);
                //25%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("25%", width * .24, height * .95);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(width * .25, height * .8, width * 0.005, height * 0.08);
                //50%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("50%", width * .49, height * .95);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(width * .5, height * .8, width * 0.005, height * 0.08);
                //75%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("75%", width * .74, height * .95);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(width * .75, height * .8, width * 0.005, height * 0.08);
                //75%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("100%", width * .946, height * .95);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(width * .995, height * .8, width * 0.005, height * 0.08);


                var percentageFont = (28 * (width * .001)) + 'px sans-serif';
                //Target %
                ctx.rect(targetRect.x, targetRect.y, targetRect.width, targetRect.height);
                ctx.fillStyle = targetRect.fill;
                ctx.fill();

                ctx.fillStyle = "rgba(26, 68, 126, 0.8)";
                ctx.font = percentageFont;
                ctx.fillText(targetText, (target * width) * .92, height * .11);

                //Actual %
                ctx.fillStyle = fillColour;
                ctx.fillRect(0, height * .15, actual * width, height * .5);

                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = percentageFont;
                ctx.fillText(actualText, (actual * (width * .92)), height * .4);

                ctx.closePath();

            }
        }


        var animate = function (prop, val, duration) {
            // The calculations required for the step function
            var start = new Date().getTime();
            var end = start + duration;
            var current = square[prop];
            var distance = val - current;

            var step = function () {
                // Get our current progres
                var timestamp = new Date().getTime();
                var progress = Math.min((duration - (end - timestamp)) / duration, 1);

                // Update the square's property
                square[prop] = current + (distance * progress);

                // If the animation hasn't finished, repeat the step.
                if (progress < 1) requestAnimationFrame(step);
            };

            // Start the animation
            return step();
        };


        function draw(percent) {
            if (canvas.getContext) {
                var ctx = canvas.getContext("2d");
                var sizePercent = percent;
                var percentageFont = (28 * sizePercent) + 'px sans-serif';

                //Actual %
                ctx.fillStyle = "rgb(21, 198, 28)";//rgb(0,255,0)
                ctx.fillRect(0, 30 * sizePercent, 600 * sizePercent, 100 * sizePercent);
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = percentageFont;//"25px serif";
                ctx.fillText("60%", 600 * sizePercent, 90 * sizePercent);

                //====  Text ====
                //Target %
                ctx.fillStyle = "rgba(26, 68, 126, 0.4)";//rgb(26, 68, 126) "rgba(0, 0, 200, 0.5)"
                ctx.fillRect(400 * sizePercent, 10, 15 * sizePercent, 130 * sizePercent);
                ctx.fillStyle = "rgba(26, 68, 126, 0.8)";
                ctx.font = percentageFont;
                ctx.fillText("40%", 420 * sizePercent, 20 * sizePercent);


                // ====  PERCENTAGE LEGEND  =========
                var legendFont = (20 * sizePercent) + 'px serif';
                //Line
                ctx.fillStyle = "rgb(128, 128, 128)";//rgb(26, 68, 126) "rgba(0, 0, 200, 0.5)"
                ctx.fillRect(0, 140 * sizePercent, 1000 * sizePercent, 2);
                //0%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("0%", 0, 165 * sizePercent);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(1, 140 * sizePercent, 5 * sizePercent, 10 * sizePercent);
                //25%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("25%", 240 * sizePercent, 165 * sizePercent);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(250 * sizePercent, 140 * sizePercent, 5 * sizePercent, 10 * sizePercent);
                //50%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("50%", 490 * sizePercent, 165 * sizePercent);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(500 * sizePercent, 140 * sizePercent, 5 * sizePercent, 10 * sizePercent);
                //75%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("75%", 740 * sizePercent, 165 * sizePercent);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(750 * sizePercent, 140 * sizePercent, 5 * sizePercent, 10 * sizePercent);
                //100%
                ctx.fillStyle = "rgb(0, 0, 0)";
                ctx.font = legendFont;
                ctx.fillText("100%", 970 * sizePercent, 165 * sizePercent);
                ctx.fillStyle = "rgba(0, 0, 0,0.5)";
                ctx.fillRect(1000 * sizePercent, 140 * sizePercent, 5 * sizePercent, 10 * sizePercent);

            }
        }
    </script>
@stop

<!-- Side Navigation -->
<nav class="nav-slide">
    <div class="ns-header">
        <a href="#0" class="nav-close"><i class="fa fa-times" aria-hidden="true"></i></a>
        <a href="/dashboard/admin" class="nav-close-logo"><img src="/img/branding/figaro-logo.png" alt="Logo"></a>
    </div>
    <ul>
         <li class="dashboard">
            <a href="/dashboard/admin" class="">Dashboard</a>
        </li>

        <li class="tables">
            <a role="button" data-toggle="collapse" href="#nav1">Tasks</a>
            <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav1">
                <ul class="sub-nav-dropdown">
                    <li><a href="/tasks/table">Table</a></li>
                    @if( Auth::user()->role > 4)
                    <li class=" hidden-xs"><a href="/tasks/parameters">Parameters</a></li>
                    @endif

                    <li class=""><a href="/tasks/errors">With Errors</a></li>
                    <li><a href="/tasks/cards" class="hide">Cards</a></li>
                    <li class="hide"><a href="/tasks/stats">Statistics</a></li>
                    <li class="hide"><a href="/tasks/group-break-down">Group Break Down</a></li>
                </ul>   
            </div>
        </li>

        <li class="reports">
            <a role="button" data-toggle="collapse" href="#nav2">Reports</a>
            <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav2">
                <ul class="sub-nav-dropdown">
                    <li class=""><a href="/tasks/charts/progress-overview">Progress Overview</a></li>
                    <li class=""><a href="/tasks/charts/category-tabulation">Tabulation Report</a></li>
                    <li><a href="/tasks/stats/project/bar">Project</a></li>
                    <li><a href="/tasks/stats/areas/bar-graph">Areas Bar</a></li>
                    <li><a href="/tasks/charts/test1/4">Areas Monthly</a></li>
                    <li><a href="/tasks/charts/area/weekly">Areas Weekly</a></li>
                    <li class=""><a href="/tasks/charts/system-gantt">System Gantt</a></li>
           {{--         <li class=""><a href="/tasks/charts/systems/status">System Overview</a></li>--}}
                    <li class="hide"><a href="/tasks/stats/groups/bar-graph">Groups</a></li>
                    <li class="hide"><a href="/area-graphs">Area</a></li>

                    <li class="hide"><a href="/line-graphs">Line</a></li>
                </ul>   
            </div>      
        </li>
        @if( Auth::user()->role > 5)
        <li class="rules ">
            <a role="button" data-toggle="collapse" href="#nav3">Rules</a>
            <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav3">
                <ul class="sub-nav-dropdown">
                    <li><a href="/tasks/rules/gen">Category Step Rules</a></li>
                  <li><a href="/tasks/step/phases" >Review and Approval </a></li>
                </ul>
            </div>      
        </li>
        @endif
        @if( Auth::user()->role > 4)
        <li class="build">
            <a role="button" data-toggle="collapse" href="#nav4">Models</a>
            <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav4">
                <ul class="sub-nav-dropdown">
                    <li><a href="/task/create">Task</a></li>
                    <li><a href="/task/type">Categories</a></li>
                    <li><a href="/task/sub-type">Sub Categories</a></li>
                    <li><a href="/task/group">Owners</a></li>
                    <li><a href="/system">Systems</a></li>
                    <li><a href="/area">Areas</a></li>
                    <li class="hide"><a href="#">New Rule</a></li>
                </ul>
            </div>
        </li>
        @endif


  {{--      <li class="forum">
            <a href="/forum" class="">Feedback Forum</a>
        </li>--}}

        <li class="user visible-xs">
            <a href="javascript:showUserSettingsModal({{ Auth::user()->id }});" class="">Account Settings</a>
        </li>
        <li class="sign-out visible-xs">
            <a href="/logout" class="">Logout</a>
        </li>

        <li class="main-menu">
            <a href="/welcome" class="">Main Menu</a>
        </li>

    </ul>
</nav> <!-- .nav-slide -->
<!-- Side Navigation -->
<nav class="nav-slide">
    <div class="ns-header">
        <a href="#0" class="nav-close"><i class="fa fa-times" aria-hidden="true"></i></a>
        <a href="/admin/users" class="nav-close-logo"><img src="/img/branding/figaro-logo.png" alt="Logo"></a>
    </div>

    <ul>

        <li class="users">
            <a href="/admin/users">Users</a>
        </li>


        @if( Auth::user()->role > 6)
            <li class="admin-tools ">
                <a role="button" data-toggle="collapse" href="#nav5">Tools</a>
                <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav5">
                    <ul class="sub-nav-dropdown">
                        <li><a href="/tracker/admin">Compile</a></li>
                    </ul>
                </div>
            </li>

            <li class="import hidden-sm hidden-xs hide">
                <a role="button" data-toggle="collapse" href="#nav8">Schedule</a>
                <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav8">
                    <ul class="sub-nav-dropdown">
                        <li><a href="/import/schedule">Import</a></li>
                        <li><a href="/schedule/table">Activities</a></li>

                    </ul>
                </div>
            </li>

            <li class="import hidden-sm hidden-xs">
                <a role="button" data-toggle="collapse" href="#nav6">Tasks</a>
                <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav6">
                    <ul class="sub-nav-dropdown">
                        <li><a href="/import">Import</a></li>
                        <li class="hide"><a href="/storage">Export To File</a></li>
                    </ul>
                </div>
            </li>
        @endif


        @if( Auth::user()->role > 8)
            <li class="server">
                <a role="button" data-toggle="collapse" href="#nav7">Server Admin</a>
                <div class="sub-nav collapse" data-toggle="collapse" onClick="event.stopPropagation();" id="nav7">
                    <ul class="sub-nav-dropdown">
                        <li><a href="/queue">Queues</a></li>
                        <li><a href="#">Console</a></li>
                        <li><a href="/storage">Storage</a></li>
                        <li><a href="/mail">Send e-mail</a></li>
                    </ul>
                </div>
            </li>
        @endif

        <li class="main-menu">
            <a href="/welcome" class="">Main Menu</a>
        </li>

        <li class="sign-out visible-xs">
            <a href="/logout" class="">Logout</a>
        </li>

    </ul>
</nav> <!-- .nav-slide -->
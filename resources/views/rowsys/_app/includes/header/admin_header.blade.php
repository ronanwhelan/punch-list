<header class="main-header">
    <div class="branding pull-left">
        <a href="/admin/users" class="logo"><img src="/img/branding/figaro-logo.png" alt="Logo"></a>
        <a href="#0" class="nav-slide-trigger"><span></span></a>
    </div>

    <button class=" hide btn btn-search-trigger" onclick="showSearchModal('tasks')"><i class="fa fa-search"></i><span class="hidden-xs"> Search</span></button>

    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav pull-right header-menu">
            <li id="user-header" class="dropdown">
                <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#" data-original-title="" title="">
                    <img width="25" class="avatar" alt="user avatar" src="/img/default_avatar.png">
                    <span class="username">{{ Auth::user()->name }}</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu animated fadeInDown">

                    <li>
                        <a href="javascript:showUserSettingsModal({{ Auth::user()->id }});"><i class="hide fa fa-cog"  aria-hidden="true"></i> Account Settings</a>
                    </li>
                    <li class="dropdown-footer clearfix">
                        <a title="Fullscreen" onclick="requestFullScreen()">
                            <i class="fa fa-expand" aria-hidden="true"></i>
                        </a>
                        <a title="Logout" href="/logout">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

</header> <!-- .main-header -->
<header class="main-header">
    <div class="branding pull-left">
        <a href="/items/create/mobile" class="logo"><img src="/img/branding/figaro-logo.png" alt="Logo"></a>
        <a href="#0" class="nav-slide-trigger"><span></span></a>
    </div>

    <button class="  btn btn-search-trigger" onclick="showItemSearchModal()"><i class="fa fa-search"></i><span class="hidden-xs"> Search</span></button>

    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav pull-right header-menu">
            <li id="user-header" class="dropdown">
                <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#" data-original-title="" title="">
                    <img width="25" class="avatar" alt="user avatar" src="/img/default_avatar.png">

                    <span class="username">
                        @if(Auth::user()->password_changed === 0)
                 <i class="fa fa-exclamation-triangle"></i>
                        @endif
                        {{ Auth::user()->name }}</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu animated fadeInDown">



                    <li>
                        <a href="javascript:showUserSettingsModal({{ Auth::user()->id }});"><i class="hide fa fa-cog"  aria-hidden="true"></i> Account Settings</a>
                    </li>
                    <li class="dropdown-footer clearfix">
                        <a title="Fullscreen" onclick="requestFullScreen()">
                            <i class="fa fa-expand" aria-hidden="true"></i>
                        </a>
                        <a title="Logout" href="/logout">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                        </a>
                    </li>

                    @if(Auth::user()->password_changed === 0)
                        <li>
                            <p class="txt-color-red font-xs">Please Update your password</p>
                        </li>

                    @endif
                </ul>
        </li>
            </ul>
    </div>

</header> <!-- .main-header -->
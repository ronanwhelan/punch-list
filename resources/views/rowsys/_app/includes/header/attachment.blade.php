<header class="main-header">
    <div class="branding pull-left">
        <a href="/items/create/mobile" class="logo"><img src="/img/branding/figaro-logo.png" alt="Logo"></a>

    </div>
  <button class="btn btn-primary pull-right btn-close" onclick="self.close();return false;">Close <i class="fa fa-close"></i></button>
</header> <!-- .main-header -->

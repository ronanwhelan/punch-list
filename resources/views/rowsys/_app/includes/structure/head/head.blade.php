<meta charset="UTF-8">
<title>FIGARO</title>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="white">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="Figaro">
<meta name="keywords" content="Figaro">
<meta name="author" content="Figaro">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- Fav & touch icons -->
<link rel="shortcut icon" href="/img/branding/favicon.png" />
<link rel="apple-touch-icon" href="/img/ico/apple-touch-icon.png" />
<link rel="apple-touch-icon" href="/img/ico/apple-touch-icon-60.png" />
<link rel="apple-touch-icon" href="/img/ico/apple-touch-icon-72.png" />
<link rel="apple-touch-icon" href="/img/ico/apple-touch-icon-76.png" />
<link rel="apple-touch-icon" href="/img/ico/apple-touch-icon-120.png" />
<link rel="apple-touch-icon" href="/img/ico/apple-touch-icon-152.png" />
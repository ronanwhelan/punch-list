<!-- Modal -->
<form id="search-task-form" action="/tasks/table" method="get">
    <input type="hidden" id="go-to-task-parameters-view" name="goToTaskParametersView" value="0">

    <div class="modal fade bs-example-modal-lg" id="task-search-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Search &nbsp; <i onclick="resetSearchModalDropDown()" class="fa fa-undo"></i></h4>
                </div>
                <div class="modal-body">
                    <div id="search-options-section"></div>

                    <div id="limit-search-results-section" class="hide">
                        <div class="col-md-12 ">
                            <div class="">
                                <div class="checkbox">
                                    <label>
                                        <input name="chk_return_all_results" type="checkbox"> Return All Results
                                    </label>
                                </div>
                                <p></p>
                                <p class=" text-warning" style="font-size: 25%"><i class="fa fa-exclamation-circle"></i> Note: returning all results may take longer to process and to build the result table.
                                    If not checked, the first 250 tasks will be returned. <br>Maximum returned is limited to 1500.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onclick="submitTaskSearchFromSearchModal()"  id="show-task-button" class="btn btn-success pull-right"> Go To Task Table <i class="fa fa-table"></i></button>


                        <button type="button" id="show-parameters-button" onclick="showTaskParametersView()" class=" hide hidden-xs hidden-sm   btn  btn-info"
                                data-toggle="tooltip" data-placement="top" title="Edit Task Parameters e.g Target Hrs,Schedule Link etc">
                            Go To Parameters Table <i class="fa fa-cog"></i></button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
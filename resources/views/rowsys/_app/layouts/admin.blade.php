<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <title>FIGARO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Figaro">
    <meta name="keywords" content="Figaro">
    <meta name="author" content="Figaro">

    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Lato:400,700,300' type='text/css'>

    <!-- CSS -->
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" href="/css/vendor/wait-me/waitMe.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <!-- Modernizr -->
    <script src="/js/modernizr.js"></script>

    <!-- Fav & touch icons -->
    <link rel="shortcut icon" href="/img/branding/favicon.png" />

</head>
<body>
<header class="main-header">
    <a href="/welcome" class="logo"><img src="/img/novartis-logo.png" alt="Logo"></a>

    <div class="search is-hidden">
        <form action="#0">
            <input type="search" placeholder="Search...">
        </form>
    </div> <!-- search -->

    <a href="#0" class="nav-trigger"><span></span></a>

    <nav class="nav">
        <ul class="top-nav">
            <li><a href="#" class="full-screen" onclick="requestFullScreen()"><i class="fa fa-arrows-alt"></i> Full Screen</a></li>
            <li class="has-children account">
                <a href="#0">
                    <img src="/img/avatar.png" alt="avatar">
                    @if (Auth::check())
                        {{ Auth::user()->name }}
                    @endif
                </a>
                <ul>
                    <li><a href="#0">My Account</a></li>
                    <li><a href="#0">Edit Account</a></li>
                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</header> <!-- .main-header -->

<!-- Side Navigation -->
<nav class="side-nav">
    <ul>
        <li class="active dashboard">
            <a href="/dashboard/admin" class="">Dashboard</a>
        </li>

        <li class="has-children tools">
            <a href="#0">Tools</a>
            <ul>
                <li><a href="/queue">Queues</a></li>
                <li><a href="/console">Console</a></li>
                <li><a href="/storage">Storage</a></li>
                <li><a href="/mail">Send e-mail</a></li>
                <li><a href="/import">Import From File</a></li>
                <li><a href="/storage">Export To File</a></li>
                <li><a href="#0">Database</a></li>
            </ul>
        </li>

        <li class="has-children server">
            <a href="#0">Server</a>
            <ul>
                <li><a href="console">Choose Server</a></li>
                <li><a href="queue">Stats</a></li>
                <li><a href="console">Logs</a></li>
            </ul>
        </li>
        <li class="has-children tables">
            <a href="#0">Tables</a>
            <ul>
                <li><a href="/datatable/smart">Smart Data Table</a></li>
                <li><a href="/datatable/site">Site Table</a></li>
                <li><a href="/datatable/repsonsive">Responsive</a></li>
            </ul>
        </li>
        <li class="has-children graphs">
            <a href="#0">Graphs</a>
            <ul>
                <li><a href="queue">Area</a></li>
                <li><a href="queue">Line</a></li>
            </ul>
        </li>
    </ul>
</nav>

<section class="main-content">
    <div class="content-wrapper">

        <!-- PUT CONTENT HERE -->
        @yield('content')

    </div>
</section> <!-- .main-content -->

<!-- Scripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="/js/jquery.menu-aim.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script> <!-- Resource jQuery -->

<script src="/js/rowsys/tracker/app/app.functions.js"></script>
<script src="/js/rowsys/tracker/tasks/general.js"></script>

<!-- WAIT ME - provides a spinner when loading Data -->
<script src="/js/vendor//wait-me/waitMe.min.js"></script>

<!-- PUT LOCAL SCRIPTS HERE -->
@yield('local_scripts')

</body>
</html>
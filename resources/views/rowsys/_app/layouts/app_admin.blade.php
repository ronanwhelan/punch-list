<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <!-- Common Head Data  -->
    @include('rowsys._app.includes.structure.head.head')

    <!-- Common Title  -->
    @include('rowsys._app.includes.structure.title.title')

    <!-- Head Scripts  -->
    @include('rowsys._app.includes.styles.styles')

    <!-- PAGE RELATED - CSS  -->
    @yield('page_related_css')

    <!-- Head Scripts  -->
    @include('rowsys._app.includes.scripts.head.head_scripts')

    <!-- PAGE RELATED - HEAD JS  -->
    @yield('head_js')

</head>

<body id="body" class="">

<!-- MAIN HEADER -->
@include('rowsys._app.includes.header.admin_header')

<!-- SIDE NAV-->
@include('rowsys._app.includes.nav_bar.admin_nav_bar')

<!-- CONTENT -->

<section class="main-content" id="main-content">
    <div class="content-wrapper">
        <!--Search Modal -->
        @include('rowsys._app.includes.search.task_search_modal')

        <!-- PUT CONTENT HERE -->
        @yield('content')

        <!-- RIGHT SIDE BAR -->
        @include('rowsys._app.includes.right_side_bar.right_side_bar')

        <!--  FOOTER -->
        @include('rowsys._app.includes.footer.footer')


    </div>
</section> <!-- .main-content -->
@include('rowsys.admin.users.profile.settings_modal')
<div>
    <a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>
</div>

<!-- GLOBAL SCRIPTS -->
@include('rowsys._app.includes.scripts.global.global_scripts')

<!-- PUT LOCAL SCRIPTS HERE -->
@yield('local_scripts')

</body>

</html>
/**
 * Created by ronanwhelan on 3/6/16.
 *
 * Design Docs Main JS Script
 */


/** -----------------------------------------------------------------
                        SEARCH SECTION
 ----------------------------------------------------------------- */

/** ===============================================================
 *  Show the Search Modal
 * ===============================================================
 */
function showSearchModal() {
    $.ajax({
        url: "/get-design-documents-search-form", success: function (result) {
            $("#search-options-section").html(result);
            $('#from-date').datepicker({format: "dd/mm/yyyy"});
            var modal = $('#search-modal');
            modal.modal('show');
            //Modal loosing focus after inputs updated - temp fix
            if (onMobile) {
                // Position modal absolute and bump it down to the scrollPosition
                modal.css({
                    position: 'absolute',
                    marginTop: $(window).scrollTop() + 'px',
                    bottom: 'auto'
                });
                // Position backdrop absolute and make it span the entire page
                //
                // Also dirty, but we need to tap into the backdrop after Boostrap
                // positions it but before transitions finish.
                //
                setTimeout(function () {
                    $('.modal-backdrop').css({
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: '100%',
                        height: Math.max(
                            document.body.scrollHeight, document.documentElement.scrollHeight,
                            document.body.offsetHeight, document.documentElement.offsetHeight,
                            document.body.clientHeight, document.documentElement.clientHeight
                        ) + 'px'
                    });
                }, 0);
            }


            $("#search-items-form").change(function () {
                getItemSearchStats();
            });
            $(".selectpicker").selectpicker();
            $(".select2").select2();
        }
    });
}
/**
 * Created by ronanwhelan on 28/4/16.
 */

/*
 * ----------------------------------------------------
 *          Show Details Modal
 * ----------------------------------------------------
 */
var itemModalIsOn = false;
function showDetailsModal(item_id) {
    $('#item-details-modal').modal('show');
    getItemDetails(item_id);
    if (onMobile) {
        // Position modal absolute and bump it down to the scrollPosition
        modal.css({
            position: 'absolute',
            marginTop: $(window).scrollTop() + 'px',
            bottom: 'auto'
        });
        // Position backdrop absolute and make it span the entire page
        //
        // Also dirty, but we need to tap into the backdrop after Boostrap
        // positions it but before transitions finish.
        //
        setTimeout(function () {
            $('.modal-backdrop').css({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: Math.max(
                    document.body.scrollHeight, document.documentElement.scrollHeight,
                    document.body.offsetHeight, document.documentElement.offsetHeight,
                    document.body.clientHeight, document.documentElement.clientHeight
                ) + 'px'
            });
        }, 0);
    }
    itemModalIsOn = true;
}
/*$('#item-details-modal').blur(function(){
 if (itemModalIsOn){
 // $('#item-details-modal').focus();
 }
 });*/
function closeItemViewModal() {
    itemModalIsOn = false;
    $('#item-details-modal').modal('hide');
}
/*
 * ----------------------------------------------------
 *          Get  the Item Details
 * ----------------------------------------------------
 */
function getItemDetails(id) {
    $("#item-details-section").html('<div class=" text-align-center" ><i class="  fa fa-spinner fa-5x fa-spin"></i> Loading...</div>');
    //pleaseWait('loading Item Details');
    $.ajax({
        type: "GET",
        url: "/items/details/" + id, success: function (result) {
            //console.log(result);
            $("#item-details-section").html(result);
            $('#item-heading-number').html($('#header-item-number-for-show').html());
            $(".selectpicker").selectpicker();
            var screenSize = screen.width;
            if (screenSize < 1300) {
                //$('.selectpicker').selectpicker('mobile');
                $('.datepicker').datepicker('destroy');
            } else {
                $(".datepicker").datepicker();
            }

            //pageSetUp();
            //waitDone();
            $('.scroll-to-on-click').on('click', function () {
                // $('html,body').animate({scrollTop: $(this).offset().top}, 'slow');
            });
            //$("html, body").animate({scrollTop: 0}, "slow");
        },
        error: function (xhr, status, error) {
            //waitDone();
            alert('oops - something went wrong - refresh to correct');

        }
    });
}
/*
 * ----------------------------------------------------
 *           Update the Item Details
 * ----------------------------------------------------
 *   Same function used to update the Owner,
 *  Responsible and Assignee Details
 */
function updateItemDetails(formName, urlText, id) {

    var formId = '#' + formName;
    //a token is needed to prevent cross site
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $(formId);
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var validationSection = $('#' + formName + '-validation-errors');//update-item-owner-details-form-validation-errors
    pleaseWait('Updating Item Details');
    $.ajax({
        type: 'POST',
        url: urlText,
        data: datastring,
        success: function (result) {
            console.log(result);
            $('#update-modal').modal('hide');
            $('#item-details-modal').modal('hide');
            $.notify({
                title: '<strong>Success!</strong><br>',
                message: result + ""
            }, {
                animate: {
                    enter: 'animated fadeInLeft',
                    exit: 'animated fadeOutRight'
                },
                type: 'success',
                //offset: {x: 100, y: 100},
                //placement: {from: "bottom"},
                showProgressbar: false,
                delay: 1500
            });
            setTimeout(function () {
                location.reload();
                // getItemDetails(id);
            }, 800);
            validationSection.addClass('hide');

        },
        beforeSend: function () {
            // pleaseWait();
        },
        complete: function () {
            waitDone();
        },
        // Handles the Errors if returned back from the Server -
        // Including any Validation Errors
        error: function (xhr, status, error) {
            waitDone();
            validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong>');
            var errorText = '';
            var validationErrorText = ' Validation Errors';
            var list = $("<ul class=''></ul>");
            var contactAdminText = ' - Contact the Figaro Administrator';

            // If there is a Validation Error then show the user the validation error Text
            if (error === 'Unprocessable Entity' || xhr.status === 422) {
                //validationSection.append(validationErrorText);
                jQuery.each(xhr.responseJSON, function (i, val) {
                    validationErrorText = '<li>' + val + '</li>';
                    list.append(validationErrorText);
                    console.log(validationErrorText);
                });
            } else {
                errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                errorText = errorText + contactAdminText;
            }
            validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
        }
    });

}


function updateSoloAttachment() {
    $('#upload-solo-attachment-button').attr('disabled', 'disabled').addClass('disable').append(' <i class="fa fa-spinner fa-spin"></i>');
    console.log('upload solo file');
    $('#upload-solo-attachment-form').submit();
}


function scrollDivToTop() {
    $('html,body').animate({scrollTop: $(this).offset().top}, 800);
}

/** -----------------------------------------------------------------
 * *************  SEARCH SECTION **********************
 *
 */
/** ===============================================================
 *  Show the Search Modal
 * ===============================================================
 */
function showItemSearchModal() {
    console.log('showItemSearchModal');
    $.ajax({
        url: "/get-item-search-form", success: function (result) {
            dropdownsResetting = true;
            $("#search-options-section").html(result);
            $('#from-date').datepicker({format: "dd/mm/yyyy"});
            var modal = $('#item-search-modal');
            modal.modal('show');
            //Modal loosing focus after inputs updated - temp fix
            if (onMobile) {
                // Position modal absolute and bump it down to the scrollPosition
                modal.css({
                    position: 'absolute',
                    marginTop: $(window).scrollTop() + 'px',
                    bottom: 'auto'
                });
                // Position backdrop absolute and make it span the entire page
                //
                // Also dirty, but we need to tap into the backdrop after Boostrap
                // positions it but before transitions finish.
                //
                setTimeout(function () {
                    $('.modal-backdrop').css({
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: '100%',
                        height: Math.max(
                            document.body.scrollHeight, document.documentElement.scrollHeight,
                            document.body.offsetHeight, document.documentElement.offsetHeight,
                            document.body.clientHeight, document.documentElement.clientHeight
                        ) + 'px'
                    });
                }, 0);
            }

            $("#search-items-form").change(function () {
                if (!dropdownsResetting) {
                    getItemSearchStats();
                }
            });
            $(".selectpicker").selectpicker();
            $(".select2").select2();

            dropdownsResetting = false;
            getItemSearchStats();
        }
    });
}
/** ===============================================================
 *  Get the Search Stats for the Search Section
 * ===============================================================
 */
function getItemSearchStats() {
    console.log('function: getItemSearchStats');
    $('#search-stat-total').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-complete').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-complete-percent').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-past-due').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-seven-days').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-fourteen-days').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-one-month').html('<i class="fa fa-spinner fa-spin"></i>');
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $('#search-items-form');
    var datastring = $(form).serialize();
    var stats = [];
    $.ajax({
        type: 'GET',
        data: datastring,
        url: "/get-item-search-stats", success: function (result) {
            stats = result.stats;
            $('#search-stat-total').text(stats.total);
            $('#search-stat-complete').text(stats.complete);
            $('#search-stat-complete-percent').text(stats.completePercent + ' %');
            $('#search-stat-past-due').text(stats.pastDue);
            $('#search-stat-seven-days').text(stats.dueInSevenDays);
            $('#search-stat-fourteen-days').text(stats.dueInFourteenDays);
            $('#search-stat-one-month').text(stats.dueInFourteenDays);
            console.log(stats);
        }
    });
    // setFocusOnSearchModal();
}
/** ===============================================================
 *  Find Similar Items to show user
 * ===============================================================
 */
function findSimilarItems() {
    var systemId = $('#system').val();
    var roomText = $("#rooms option:selected").text();
    var typeId = $("#type option:selected").val();
    var tableBody = $('#similar-item-table-body');
    var countText = $('#similar-items-text');
    var familiarInfoSection = $('#similar-info-section');
    tableBody.empty();
    $.ajax({
        type: "GET",
        data: {systemId: systemId, roomText: roomText, typeId: typeId},
        url: "/items/find-similar", success: function (result) {
            if (result.length > 0) {
                jQuery.each(result, function (index, item) {
                    tableBody.append('<tr>' +
                    '<td>' + item.description + '</td>' +
                    '<td>' + item.raised_date + '</td>' +
                    '' +
                    '</tr>');
                });
                countText.html('<i class="fa fa-info-circle"></i>&nbsp;' + result.length + ' similar item(s) already open in this location');
                familiarInfoSection.fadeIn(1500);
            } else {
                familiarInfoSection.fadeOut(1500);
            }

        }
    });
}


/** ===============================================================
 *  Date Functions
 * ===============================================================
 */
//Shows the Category B options
function categorySelectionOptions(selection) {

    var categoryBOptionsSection = $('#category-b-options-section');
    switch (selection) {
        case '1':
            categoryBOptionsSection.fadeOut(1000);
            break;
        case '2':
            categoryBOptionsSection.fadeIn(1500);
            break;
    }

    updateDueDateSection(selection);
    if (selection === '2') {
        $('#category-b-options-section').fadeIn(1500);

    } else {

    }
}
/** ===============================================================
 *
 *  gets the Dates for the System depending on the Category chosen
 *
 *
 * ===============================================================
 */
function getSystemScheduleDates(systemId) {
    //console.log(systemId);
    $('#due-date-for-show').html('<i class="fa fa-spinner fa-spin"></i>');
    if (systemId !== '') {
        findSimilarItems();
        //pleaseWait('getting System Dates');
        //$('#category').removeAttr('disabled');
        $.ajax({
            type: "GET",
            url: "/item/system/dates/" + systemId, success: function (result) {
                //console.log(result);
                var dates = result.datesArray;
                populateCatBDropDowns1(dates);

                //console.log(dates);
                $('#cat-a-date').val(result.cat_a_date);
                $('#cat-a-step-number').val(result.cat_a_step_number);
               // $('#system-current-phase-text').text('In ' + result.system_current_phase + ' phase');
                $('#phase_recorded_in').val(result.system_current_phase);
                $('#cat-a-step').val(result.cat_a_step);
                $('#cat-b-mc-date').val(result.mc_date);
                $('#cat-b-com-date').val(result.com_date);
                $('#cat-b-oq-date').val(result.oq_date);
                $('#cat-b-pq-date').val(result.pq_date);
                updateDueDate();
                waitDone();
                $('#no-system-date-error').fadeOut(100);
            },
            error: function (xhr, status, error) {
                // waitDone();
                console.log(xhr.responseJSON.message);
                alert(xhr.responseJSON.message);
                $('#no-system-date-error').fadeIn(1500);
            }
        });
    } else {
        // $('#category').attr('disabled',true);
    }
    updateDueDate();
}
/** ===============================================================
 *
 *  POPULATE THE CAT B DROP DOWN DATES
 *
 *  Populates the Cat B drop dates
 *
 * ===============================================================
 */
function populateCatBDropDowns(dates) {
    var dropDown = $('#cat_b_schedule_choice');
    dropDown.selectpicker('destroy');
    dropDown.empty();
    $(dropDown).append('<option value="1">MC Date : ' + dates.mc_date + '</option>');
    $(dropDown).append('<option value="2">COM Date : ' + dates.com_date + '</option>');
    $(dropDown).append('<option value="3">OQ Date : ' + dates.oq_date + '</option>');
    $(dropDown).append('<option value="4">PQ Date : ' + dates.pq_date + '</option>');
    dropDown.selectpicker();
    setSelectPickerFonts();
    updateDueDate();
}


function populateCatBDropDowns1(dates) {
    var dropDown = $('#cat_b_schedule_choice');
    dropDown.selectpicker('destroy');
    dropDown.empty();
    var position = '';
    var dateAndDesc = '';

    for (var i = 0; i < dates.length; i++) {
        //$(dropDown).append('<option>hello</option>');
        $(dropDown).append('<option value="' + dates[i][2] + '">' + dates[i][0] + ' : ' + dates[i][1] + '</option>');
        console.log(dates[i][0] + ' : ' + dates[i][1] + ' :' + dates[i][2]);
    }

    dropDown.selectpicker();
    setSelectPickerFonts();
    updateDueDate();
}
/** ===============================================================
 *
 *  Update Due Date
 *
 * ===============================================================
 */
function updateDueDate() {
    var system = $('#system').val();
    var chosenCategory = $('#category').val();
    var catBChoice = '';
    var catCChoice = '';
    if (chosenCategory === '') {
        chosenCategory = '1';
    }
    var DueDate = $('#due-date');
    var DueDateForShow = $('#due-date-for-show');

    if (chosenCategory === '1') {
        DueDateForShow.text($('#cat-a-date').val() + ' (' + $('#cat-a-step').val() + ')');
        DueDate.val($('#cat-a-date').val());

    } else if (chosenCategory === '2') {
        catBDateSelection();

    } else if (chosenCategory === '3') {
        var catCInsertedDate = $('#cat_c_due_date').val();
        if (catCInsertedDate !== '') {
            DueDateForShow.text(catCInsertedDate);
            DueDate.val(catCInsertedDate);
        } else {
            //get Cat B Choice
            catBDateSelection();
        }
    }
    if (system === '') {
        DueDateForShow.text('Please select a system');
        DueDate.val('');
    }

}

// CAT B - Date Selection
function catBDateSelection() {
    //console.log('cat be selected');
    var DueDate = $('#due-date');
    var DueDateForShow = $('#due-date-for-show');

    var catBChoice = $("#cat_b_schedule_choice option:selected").text();
    var dateText = catBChoice.split(": ");
    console.log(dateText[1]);
    DueDateForShow.text(catBChoice);
    DueDate.val(dateText[1]);

    /*    switch (catBChoice) {
     case '1':
     DueDateForShow.text($('#cat-b-mc-date').val() + ' ( MC Date)');
     DueDate.val($('#cat-b-mc-date').val());
     break;
     case '2':
     DueDateForShow.text($('#cat-b-com-date').val() + ' ( COM Date)');
     DueDate.val($('#cat-b-com-date').val());
     break;
     case '3':
     DueDateForShow.text($('#cat-b-oq-date').val() + ' ( OQ Date)');
     DueDate.val($('#cat-b-oq-date').val());
     break;
     case '4':
     DueDateForShow.text($('#cat-b-pq-date').val() + ' ( PQ Date)');
     DueDate.val($('#cat-b-pq-date').val());
     break;
     default:
     DueDateForShow.text('Please enter a date');
     DueDate.val('');
     }
     */


}

/** ===============================================================
 *
 *  Show CAT B Step Choices
 *
 * ===============================================================
 */
function showCatBDatesAndUpdateDueDate(selection) {

    if (selection === '2') {
        $('#category-b-options-section').fadeIn(1500);

    } else if (selection === '3') {
        $('#category-b-options-section').fadeIn(1500);
        $('#category-c-options-section').fadeIn(1500);

    } else {
        $('#category-b-options-section').fadeOut(2000);
        $('#category-c-options-section').fadeOut(2000);
    }

    updateDueDate();
}

/** ===============================================================
 *
 *  Check If Item Has been already added i.e the session has a system_id
 *  If an item was just added, then get the dates for the System
 *
 * ===============================================================
 */
function checkIfNewItem() {
    var systemDropDown = $('#system :selected').val();
    if (systemDropDown !== '') {
        getSystemScheduleDates(systemDropDown);
    }
}

/** ===============================================================
 *
 *  Show Attachments Window
 *
 * ===============================================================
 */
function showAttachmentsForItem(id) {
    //event.preventDefault();
    window.open('/item/attachments/' + id, 'newwindow', 'width=800, height=900');
    console.log('show attachments window');
    $('#item-details-modal').modal('hide');
}

$(function () {
    $(".showAttachments").on("click", function (e) {
        var w = window.open(this.href, this.target, "height=300,width=400");
        if (w) e.preventDefault(); // prevent link but only if not blocked
        console.log('show attachments window');
        $('#item-details-modal').modal('hide');
    });
});


function popUpAttachmentsWindow() {
    window.open("/item/attachments/1");
}


/** ===============================================================
 *  Get Filtered Down Items Types based on Groups Selected
 * ===============================================================
 */
function getFilteredItemTypesByGroup(choice) {
    if (!dropdownsResetting) {
        var group = 'group';
        var type = 'type';
        if (choice === 'search') {
            group = 'search-group';
            type = 'search-type';
        }
        console.log('filtering Item Types');
        if (!dropdownsResetting) {
            data = $('#' + group).val();
            var updateDropDown = $('#' + type);
            updateDropDown.selectpicker('destroy');
            updateDropDown.empty();
            $.ajax({
                type: 'GET',
                data: {data: data},
                url: "/get-filtered-item-types", success: function (result) {

                    //console.log(result);
                    $(updateDropDown).append('<option value=""></option>');
                    $.each(result, function (key, value) {
                        $(updateDropDown).append('<option value=' + value.id + '>' + value.name + '</option>');
                    });
                    updateDropDown.selectpicker();
                }
            });
        }
    }
}
/** ===============================================================
 *  Get Filtered Down Items Types based on Groups Selected
 * ===============================================================
 */
function resetItemTypesDropdown() {

    console.log('filtering Item Types');
    data = '';
    var updateDropDown = $('#search-type');
    updateDropDown.selectpicker('destroy');
    updateDropDown.empty();
    $.ajax({
        type: 'GET',
        data: {data: data},
        url: "/get-filtered-item-types", success: function (result) {

            //console.log(result);
            $(updateDropDown).append('<option value=""></option>');
            $.each(result, function (key, value) {
                $(updateDropDown).append('<option value=' + value.id + '>' + value.name + '</option>');
            });
            updateDropDown.selectpicker();
        }
    });
}




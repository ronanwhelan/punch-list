/**
 * Created by ronanwhelan on 10/5/16.
 */

/** ===============================================================
 *
 *   Disable an input text field from loading if the user hits the return key
 *
 * ===============================================================
 */
function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text")) {
        return false;
    }
}
document.onkeypress = stopRKey;


/** ===============================================================
 *
 *   Search Modal - Task Number on Keyup function to get list of Task Numbers over ajax
 *
 * ===============================================================
 */

function clearWorkspace() {
    $('#workspace').clear();
}

function pleaseWait(message) {
    var current_effect = 'bounce'; //
    run_waitMe(current_effect, message);
    console.log('wait me called');
}

function waitDone() {
    $('#main-content').waitMe('hide');
}

function run_waitMe(effect, message) {
    $('#main-content').waitMe({
        //http://www.jqueryscript.net/demo/jQuery-Plugin-For-Creating-Loading-Overlay-with-CSS3-Animations-waitMe/
        //none, rotateplane, stretch, orbit, roundBounce, win8,
        //win8_linear, ios, facebook, rotation, timer, pulse,
        //progressBar, bouncePulse or img
        effect: 'bouncePulse',

        //place text under the effect (string).
        text: 'Please wait ...' + message,

        //background for container (string).
        bg: 'rgba(255,255,255,0.7)',

        //color for background animation and text (string).
        color: '#000',

        //change width for elem animation (string).
        sizeW: '',

        //change height for elem animation (string).
        sizeH: '',

        // url to image
        source: 'smartadmin/img/logo/novartis_logo_3.jpg',

        // callback
        onClose: function () {
        }

    });
}

/** ===============================================================
 *  Put focus on the search modal. Used on mobile where search modal
 *  looses focus when some inputs got focus.
 * ===============================================================
 */
function setFocusOnSearchModal() {
    //$('#search-task-number').focus();
    $('#user-filter-modal').focus();

}

/** ===============================================================
 *   App Variables
 * ===============================================================
 */
var screenSize = screen.width;
var onMobile = false;
function checkIfOnMobile() {
    console.log('check if on mobile in app functions called. Screen Size is ' + screenSize);
    screenSize = screen.width;
    if (screenSize < 1300) {
        onMobile = true;
        //$('.selectpicker').selectpicker('mobile');
        $('.datepicker').datepicker('destroy');
    }
}
document.onkeypress = checkIfOnMobile;


/** ===============================================================
 *
 *  user details must be accessible from all places on the app i.e in the header.
 *  These are global functions to update the users detail like name, email and password
 *
 *
 *   User Settings Modal
 *
 * ===============================================================
 */
function showUserSettingsModal(user_id) {
    $.ajax({
        type: 'GET',
        url: '/users/get-user-settings-details/' + user_id, success: function (result) {
            $('#user-setting-details-section').html(result);
            $(".selectpicker").selectpicker();
        }
    });

    $('#user-settings-modal').modal('show');
}
function getUserDetails(id) {
    $.ajax({
        type: 'GET',
        url: '/admin/users/get-user-details/' + id, success: function (result) {
            $('#user-details-section').html(result);
            $(".selectpicker").selectpicker();
        }
    });
}

// get the Users Roles.
function getUserRoles(id) {
    if (!dropdownsResetting) {
        data = $('#responsible_role').val();
        var updateDropDown = $('#attention_to');
        updateDropDown.selectpicker('destroy');
        updateDropDown.empty();
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/user/roles/list/" + id, success: function (result) {
                //console.log(result);
                $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.id + '>' + value.name + ' ' + value.surname + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}

//----------------------------------
// Get the User for the selected Role
//----------------------------------
function getUsersByRole(userId,selectId) {
    if (!dropdownsResetting) {
        data = $('#responsible_role').val();
        var updateDropDown = $('#'+ selectId);
        updateDropDown.selectpicker('destroy');
        updateDropDown.empty();
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/roles/user-list/" + userId, success: function (result) {
                console.log(result);
               $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.id + '>'  + value.name + ' ' + value.surname + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}

//----------------------------------
// Get the User for the selected Role
//----------------------------------
function getUsersByTeam(userId,selectId) {
    if (!dropdownsResetting) {
        data = $('#responsible_team').val();
        var updateDropDown = $('#'+ selectId);
        updateDropDown.selectpicker('destroy');
        updateDropDown.empty();
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/teams/user-list/" + userId, success: function (result) {
                console.log(result);
                $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.id + '>'  + value.name + ' ' + value.surname + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}


//----------------------------------
// Get the User for the selected Role
//----------------------------------
function getComponentsBySystem(systemId,selectId) {

    console.log(systemId);
    if (!dropdownsResetting) {
        data = $('#responsible_role').val();
        var updateDropDown = $('#'+ selectId);
        updateDropDown.selectpicker('destroy');
        updateDropDown.empty();
        $.ajax({
            type: 'GET',
            //data: {data: data},
            url: "/list/components/system/" + systemId, success: function (result) {
                console.log(result);
                $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.id + '>'  + value.tag + ' ' + value.description + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}
// ===============================================================
//                     UPDATE MODELS
// ===============================================================

/** ===============================================================
 *
 *  Update a Model - from Update Modal Form
 *
 *  updates a Model over Ajax and return and process any errors
 *  used for all updates to Model - common Function
 *  The form is loaded to the update modal for each model
 *
 *  Note: Parameters are updated in the TaskController
 *
 * @param formName
 * @param urlText
 * @param itemId
 * ===============================================================
 */
function updateCategoryModel(formName, urlText, itemId) {
    var formId = '#' + formName;
    //a token is needed to prevent cross site
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $(formId);
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var validationSection = $('#validation-errors');
    $.ajax({
        type: 'POST',
        url: urlText + '/' + itemId,
        data: datastring,
        success: function (result) {
            console.log(result);
            $('#update-modal').modal('hide');
            $.notify({
                title: '<strong>Success!</strong><br>',
                message: result + ""
            }, {
                animate: {
                    enter: 'animated fadeInLeft',
                    exit: 'animated fadeOutRight'
                },
                type: 'success',
                //offset: {x: 100, y: 100},
                //placement: {from: "bottom"},
                showProgressbar: false,
                delay: 1000
            });
            setTimeout(function () {
                window.location.reload(true);
            }, 1500);
            validationSection.addClass('hide');
        },
        beforeSend: function () {
            // pleaseWait();
        },
        complete: function () {
            //waitDone();
        },
        // Handles the Errors if returned back from the Server -
        // Including any Validation Errors
        error: function (xhr, status, error) {
            waitDone();
            validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
            var errorText = '';
            var validationErrorText = ' Validation Errors';
            var list = $("<ul class=''></ul>");
            var contactAdminText = ' - Contact the PES Administrator';

            // If there is a Validation Error then show the user the validation error Text
            if (error === 'Unprocessable Entity' || xhr.status === 422) {
                //validationSection.append(validationErrorText);
                jQuery.each(xhr.responseJSON, function (i, val) {
                    validationErrorText = '<li>' + val + '</li>';
                    list.append(validationErrorText);
                    console.log(validationErrorText);
                });
            } else {
                errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                errorText = errorText + contactAdminText;
            }
            validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
        }
    });
}

//=========  DELETE A MODEL ==============
function showModelDeleteSection(taskCount) {
    if (taskCount === 0) {
        $('#are-you-sure-delete-section').removeClass('hide');
    } else {
        alert('Please delete All associated tasks first before Deleting')
    }
}
/**
 * Show the Update Model Modal
 * @param id
 */
function showUpdateModelModal(urlToModel)  {

    $('#update-modal').modal('show');
    console.log('show update model modal');
    var modelDetailsSection = $("#model-details");
    $.ajax({
        url: urlToModel, success: function (result) {
            modelDetailsSection.html(result);
            modelDetailsSection.fadeTo(1000, .9);
            $(".selectpicker").selectpicker();
            $(".datepicker").datepicker();
            setSelectPickerFonts();
        }
    });

    $('#loading-spinner-section-simple').fadeOut(1500);
}
/** ===============================================================
 *  Make ALL Drop Downs on the Search and Filter Modal
 *  of class type selectpicker
 * ===============================================================
 */
function setSelectPickerFonts() {
    $('#user-filter-modal').selectpicker('refresh');
}

/** ===============================================================
 *
 *  Update a Model - from Update Modal Form
 *
 *  updates a Model over Ajax and return and process any errors
 *  used for all updates to Model - common Function
 *  The form is loaded to the update modal for each model
 *
 *  Note: Parameters are updated in the TaskController
 *
 * @param formName
 * @param urlText
 * @param itemId
 * ===============================================================
 */
function updateMyDetails(formName, urlText, userId) {
    var formId = '#' + formName;
    //a token is needed to prevent cross site
    //$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $(formId);
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var successMessage = $('#my-details-updated-message');
    var validationSection = $('#user-modal-update-errors');
    if (formName === 'update-my-password-form') {
        validationSection = $('#user-modal-update-password-errors');
        successMessage = $('#my-password-updated-message');
    }
    $.ajax({
        type: 'POST',
        url: urlText,
        data: datastring,
        success: function (result) {
            console.log(result);
            successMessage.fadeIn(1500);
            validationSection.addClass('hide');

            $.notify({
                title: '<strong>Success!</strong><br>',
                message: data.message + ""
            }, {
                animate: {
                    enter: 'animated fadeInLeft',
                    exit: 'animated fadeOutRight'
                },
                type: 'success',
                //offset: {x: 100, y: 100},
                //placement: {from: "bottom"},
                showProgressbar: false,
                delay: 1000
            });
        },
        beforeSend: function () {
            // pleaseWait();
        },
        complete: function () {
            //waitDone();
        },
        // Handles the Errors if returned back from the Server -
        // Including any Validation Errors
        error: function (xhr, status, error) {
            waitDone();
            validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
            var errorText = '';
            var validationErrorText = ' Validation Errors';
            var list = $("<ul class=''></ul>");
            var contactAdminText = ' - Contact the PES Administrator';

            // If there is a Validation Error then show the user the validation error Text
            if (error === 'Unprocessable Entity' || xhr.status === 422) {
                //validationSection.append(validationErrorText);
                jQuery.each(xhr.responseJSON, function (i, val) {
                    validationErrorText = '<li>' + val + '</li>';
                    list.append(validationErrorText);
                    console.log(validationErrorText);
                });
            } else {
                errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                errorText = errorText + contactAdminText;
            }
            validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
        }
    });
}
// =========   SEARCH FILTERS  =========================
/** ===============================================================
 *  Reset ALl the Filter and Search Drop Downs
 * ===============================================================
 */
//this bit is used to prevent the dropdown updating
// while they are being reset
var dropdownsResetting = false;

// Tasks
function resetSearchModalDropDown() {
    dropdownsResetting = true;
    $('#search-gen').prop('checked', true);
    $('#search-exe').prop('checked', true);
    $('#search-task-number').val('');
    $('.selectpicker').selectpicker('deselectAll');
    $('#search-types').selectpicker('deselectAll');
    $('#search-system').selectpicker('deselectAll');
    $('#inlineRadio3').prop('checked',true);
    getFilteredTypesByGroup();
    resetSystemDropDownList();
    dropdownsResetting = false;
    getSearchStats();

}
// Items
function resetItemSearchModalDropDown() {
    dropdownsResetting = true;
    $('#search-number').val('');
    $('.selectpicker').selectpicker('deselectAll');
    $('#search-types').selectpicker('deselectAll');
    $('#search-system').selectpicker('deselectAll');
    // getFilteredSystemsByArea('');
    dropdownsResetting = false;
    resetItemTypesDropdown();
    resetSystemDropDownList();
    getItemSearchStats();
/*
    $( document ).ajaxComplete(function( event,request, settings ) {
        if(dropdownsResetting){
            getItemSearchStats();
            dropdownsResetting = false;
        }
    });*/
    //getFilteredTypesByGroup();
}
/** ===============================================================
 *  Get Filtered Down System list based on Areas Selected
 * ===============================================================
 */
function getFilteredSystemsByArea(choice) {
    console.log('getFilteredSystemsByArea');
    var area = 'area';
    var system = 'system';
    if (choice === 'search') {
        area = 'search-area';
        system = 'search-system';
    }

    if (!dropdownsResetting || choice === '') {
        var data = $('#' + area).val();
        var systemsDropDown = $('#' + system);
        systemsDropDown.selectpicker('destroy');
        if (choice === '') {
            data = '';
        }

        console.log('filtering systems' + 'area = ' + data);
        systemsDropDown.empty();
        $(systemsDropDown).append('<option><i class="fa fa-spinner fa-spin"></i></option>');
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/get-filtered-systems", success: function (result) {

                console.log(result);
                $.each(result, function (key, value) {
                    console.log('populating systems');
                    $(systemsDropDown).append('<option value=' + value.id + '>' + value.tag + ',  ' + value.description + '</option>');
                });
                systemsDropDown.selectpicker();
                setSelectPickerFonts();
            }
        });
    }
}
/** ===============================================================
 *  Resetting - Get Filtered Down System list based on Areas Selected
 * ===============================================================
 */
function resetSystemDropDownList() {
    var systemsDropDown = $('#search-system');
    systemsDropDown.selectpicker('destroy');
    var data = '';
    console.log('filtering systems' + 'area = ' + data);
    systemsDropDown.empty();
    $(systemsDropDown).append('<option><i class="fa fa-spinner fa-spin"></i></option>');
    $.ajax({
        type: 'GET',
        data: {data: data},
        url: "/get-filtered-systems", success: function (result) {

            //console.log(result);
            $.each(result, function (key, value) {
                $(systemsDropDown).append('<option value=' + value.id + '>' + value.tag + ',  ' + value.description + '</option>');
            });
            systemsDropDown.selectpicker();
            setSelectPickerFonts();
        }
    });
}
/** ===============================================================
 *  Get Filtered Down Task Types based on Groups Selected
 * ===============================================================
 */
function getFilteredTypesByGroup() {
    console.log('filtering Types');
    if (!dropdownsResetting) {
        data = $('#search-groups').val();
        var updateDropDown = $('#search-types');
        updateDropDown.selectpicker('destroy');
        updateDropDown.empty();
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/get-filtered-types", success: function (result) {

                $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.id + '>' + value.name + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}
/** ===============================================================
 *  Get Filtered Down Individual user by Company
 * ===============================================================
 */
function getFilteredUserByCompany() {
    //console.log('filtering Types');
    if (!dropdownsResetting) {
        data = $('#responsible_party').val();
        var updateDropDown = $('#attention_to');
        updateDropDown.selectpicker('destroy');
        updateDropDown.empty();
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/get-filtered-user-by-company", success: function (result) {

                //console.log(result);
                $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.id + '>' + value.name + ' ' + value.surname + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}

/** ===============================================================
 *  Get Filtered Rooms by Floor - for Punchlist
 * ===============================================================
 */
function getFilteredRoomsByFloor() {
    //console.log('filtering Types');
    if (!dropdownsResetting) {
        data = $('#floors').val();
        var updateDropDown = $('#rooms');
        updateDropDown.selectpicker('destroy');
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/get-filtered-rooms-by-floor", success: function (result) {
                updateDropDown.empty();
                //console.log(result);
                $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.name + '>' + value.name + ' ' + value.description + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}

/** ===============================================================
 *
 *  Show Task Gantt Chart Window
 *
 * ===============================================================
 */
function showTaskGanttChart() {

    var chartData = JSON.stringify(ganttChartData);
    screenWidth = $(document).width() - 50;
    screenHeight = $(document).height() + 200;
    window.sessionStorage.setItem('chartData', chartData);

    params = 'width=' + screen.width;
    params += ', height=' + screen.height;
    params += ', top=0, left=0';
    params += ', fullscreen=yes';

    window.open('/tasks/gantt', 'newwindow', params);
}
/** ===============================================================
 *
 *  Show Schedule Gantt Chart Window
 *
 * ===============================================================
 */
function drawScheduleGanttChart() {

    var chartData = JSON.stringify(ganttChartData);
    screenWidth = $(document).width() - 50;
    screenHeight = $(document).height() + 200;
    window.sessionStorage.setItem('chartData', chartData);

    params = 'width=' + screen.width;
    params += ', height=' + screen.height;
    params += ', top=0, left=0';
    params += ', fullscreen=yes';

    window.open('/schedule/chart/gantt', 'newwindow', params);
}
/** ===============================================================
 *
 * Help - Hints
 * ===============================================================
 */

function showHelpSection(number) {
    console.log('show help section');
    $('#help-section-' + number).removeClass('hide');
}


/** ===============================================================
 *  Get Task Names for Search Input Field as User searchs
 * ===============================================================
 */
$('#schedule-number-input').keyup(function () {
    console.log('get schedule number');
    var inputTextObj = $('#schedule-number-input');
    var tag = inputTextObj.val();
    var source = "/get-schedule-number-autocomplete-list/" + tag;
    inputTextObj.autocomplete({
        source: source,
        minLength: 1,
        //autoOpen: true,
        //width: 500,
        //modal: false,
        // zIndex: 10000,
        //appendTo: "search-task-number",
        //title: 'Duplicate',
        select: function (event, ui) {
            inputTextObj.val(ui.item.value);
            $('#schedule-number').val(ui.item.id);
            getTaskSchedulerDates(ui.item.id);
        }
    });
});

/** ===============================================================
 *  Navbar - wanted to close the navbar when the body was clicked
 *  but where one clicks it is the body too
 *
 * ===============================================================
 */
/*var navBarAvtive = false;
 var navBarActivationButton = $('#activate-navbar');
 navBarActivationButton.click(function () {
 navBarAvtive = true;
 console.log(navBarAvtive);
 });

 $('body').click(function () {
 console.log('body clicked');
 if (navBarAvtive) {
 // $('.nav-close').click();
 navBarAvtive = false;
 console.log(navBarAvtive);
 }
 });*/




/**
 * Created by ronanwhelan on 27/3/16.
 */

//==================================================
//              DRAW BAR CHART
//==================================================
function drawPercentageChart(chart_id,chart_frame, target, actual) {

    var prefix = '#';
    var chartSectionId = prefix + chart_frame;

    var panelWidth = $(chartSectionId).width();
    target =   Number((target).toFixed(2));
    actual =   Number((actual).toFixed(2));
   // console.log('target = ');console.log(target);
   //console.log('actual = ');console.log(actual);
    //console.log(panelWidth);

    var screenWidth = $(document).width();
    var id = prefix + chart_id;

    var canvas = document.getElementById(chart_id);

    $(id).attr({width: panelWidth * .95, height: panelWidth * .25});
    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;

    if (canvas.getContext) {
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvasWidth, canvasHeight);

        ctx.beginPath();
        var legendFont = (30 * (canvasWidth * .001)) + 'px sans-serif';
        // ====  Chart Title  =========
        /*                ctx.fillStyle = "rgb(0, 0, 0)";
         ctx.font = legendFont;
         ctx.fillText("Chart Title", 0, 20);*/

        // ====  PERCENTAGE LEGEND  =========
        //Line
        ctx.fillStyle = "rgb(128, 128, 128)";//rgb(26, 68, 126) "rgba(0, 0, 200, 0.5)"
        ctx.fillRect(0, canvasHeight * .8, canvasWidth * .995, canvasHeight * .01);

        //0%
        ctx.fillStyle = "rgb(0, 0, 0)";
        ctx.font = legendFont;
        ctx.fillText("0%", 0, canvasHeight * .98);
        ctx.fillStyle = "rgba(0, 0, 0,0.5)";
        ctx.fillRect(0, canvasHeight * .8, canvasWidth * 0.005, canvasHeight * 0.08);
        //25%
        ctx.fillStyle = "rgb(0, 0, 0)";
        ctx.font = legendFont;
        ctx.fillText("25%", canvasWidth * .24, canvasHeight * .98);
        ctx.fillStyle = "rgba(0, 0, 0,0.5)";
        ctx.fillRect(canvasWidth * .25, canvasHeight * .8, canvasWidth * 0.005, canvasHeight * 0.08);
        //50%
        ctx.fillStyle = "rgb(0, 0, 0)";
        ctx.font = legendFont;
        ctx.fillText("50%", canvasWidth * .49, canvasHeight * .98);
        ctx.fillStyle = "rgba(0, 0, 0,0.5)";
        ctx.fillRect(canvasWidth * .5, canvasHeight * .8, canvasWidth * 0.005, canvasHeight * 0.08);
        //75%
        ctx.fillStyle = "rgb(0, 0, 0)";
        ctx.font = legendFont;
        ctx.fillText("75%", canvasWidth * .74, canvasHeight * .98);
        ctx.fillStyle = "rgba(0, 0, 0,0.5)";
        ctx.fillRect(canvasWidth * .75, canvasHeight * .8, canvasWidth * 0.005, canvasHeight * 0.08);
        //75%
        ctx.fillStyle = "rgb(0, 0, 0)";
        ctx.font = legendFont;
        ctx.fillText("100%", canvasWidth * .92, canvasHeight * .98);
        ctx.fillStyle = "rgba(0, 0, 0,0.5)";
        ctx.fillRect(canvasWidth * .995, canvasHeight * .8, canvasWidth * 0.005, canvasHeight * 0.08);


        var actualText = 'Actual ' + (Math.round(actual * 100)) + '%';
        var targetText = 'Target ' + (Math.round(target * 100)) + '%';
        var fillColour = "rgb(21, 198, 28)";
        if (actual < target) {
            fillColour = "rgb(255, 0, 0)";
        }


        var targetRect = {
            'x': 0,
            'y': canvasHeight * .05,
            'width': (target * canvasWidth),
            'height': canvasHeight * .7,
            'fill': 'rgba(26, 68, 126, 0.3)'
        };
        var actualRect = {
            'x': 0,
            'y': canvasHeight * .17,
            'width': actual * canvasWidth,
            'height': canvasHeight * .5,
            'fill': fillColour
        };


        var percentageFont = (35 * (canvasWidth * .001)) + 'px sans-serif';

        /*
         var counter = 0;
         target = 0;
         actual = 0;
         myInterval = setInterval(function () {
         //drawPercentageChart(chartId,target,actual);
         target = target + .05;
         actual = actual + .05;
         ++counter;
         if (target > 1) {
         clearInterval(myInterval);
         }
         console.log(target);
         }, 100);*/

        //Target %
        ctx.fillStyle = targetRect.fill;
        ctx.fillRect(targetRect.x, targetRect.y, targetRect.width, targetRect.height);
        ctx.fillStyle = "rgba(51,147,255, 0.8)";
        ctx.font = percentageFont;
        ctx.fillText(targetText, (target * canvasWidth) * .75, canvasHeight * .12);

        //Actual %
        ctx.fillStyle = actualRect.fill;
        ctx.fillRect(actualRect.x, actualRect.y, actualRect.width, actualRect.height);
        ctx.fillStyle = "rgba(0, 0, 0,1)";
        ctx.font = percentageFont;
        ctx.fillText(actualText, (actual * (canvasWidth * .75)), canvasHeight * .4);


        //Legend Text
        //Target
        legendFont = (12 * (canvasWidth * .001)) + 'px sans-serif';
        ctx.fillStyle = "rgba(0, 0, 0, 1)";
        ctx.font = legendFont;
        //ctx.fillText('Actual ' + actualText, 5, actualRect.y * 1.6 );
        // ctx.fillText('Target ' + targetText, 4, targetRect.height * 1.05);

        ctx.closePath();


       $(chartSectionId).hide();$(chartSectionId).fadeIn(3000);

    }
}

//==================================================
//              PROJECT
//==================================================

function drawProjectBarCharts(projectid) {
    //var graph_div_id = '#' + graph_id;
    var barGraphData;
    var loadingSectionId = '';
    $.ajax({
        type: "GET",
            url: "/tasks/stats/project-bar-data/" + projectid, success: function (data) {
            //drawPercentageChart(chartId, data[0][1], data[0][1]);
            //console.log(data);
            var chartSectionId = 'chart-section-prep';
            var chartId = 'percentage-chart-canvas-prep';
            drawPercentageChart(chartId,chartSectionId, data[0][1], data[0][2]);
            chartSectionId = 'chart-section-exe';
            chartId = 'percentage-chart-canvas-exe';
            drawPercentageChart(chartId,chartSectionId, data[0][3], data[0][4]);
            var title = $("em").attr("title");
            $('.loading-section').addClass('hide');
            $('table.highchart').highchartTable({
                plotOptions: {
                    series: {
                        animation: {
                            duration: 2000
                        }
                    }
                }
            });
        }
    });
}
//==================================================
//              AREAS
//==================================================
function drawAreaBarCharts(areaid) {
    var loadingSectionId = '';
    $.ajax({
        type: "GET",
        url: "/tasks/stats/area-bar-data/" + areaid, success: function (data) {
            var chartSectionId = 'chart-section-prep-' + areaid;
            var chartId = 'percentage-chart-canvas-prep-'+ areaid;
            //console.log(data[0][1] + ' ' +  data[0][2]);
            drawPercentageChart(chartId,chartSectionId, data[0][1], data[0][2]);
            chartSectionId = 'chart-section-exe-'+ areaid;
            chartId = 'percentage-chart-canvas-exe-'+ areaid;
            drawPercentageChart(chartId,chartSectionId, data[0][3], data[0][4]);
            $('.loading-section').addClass('hide');
            $('table.highchart').highchartTable({
                plotOptions: {
                    series: {
                        animation: {
                            duration: 2000
                        }
                    }
                }
            });
        }
    });
}

//==================================================
//              GROUPS
//==================================================
function drawGroupBarCharts(groupid,areaId) {
    $.ajax({
        type: "GET",
        //url: "/tasks/stats/group-bar-data/" + groupid + '?areaid =' +areaId , success: function (data) {
            url: "/tasks/stats/group-bar-data/" + groupid + '/' +areaId , success: function (data) {
            console.log(data);
            var chartSectionId = 'chart-section-prep-' + groupid;
            var chartId = 'percentage-chart-canvas-prep-'+ groupid;
            drawPercentageChart(chartId,chartSectionId, data[0][1], data[0][2]);
            chartSectionId = 'chart-section-exe-'+ groupid;
            chartId = 'percentage-chart-canvas-exe-'+ groupid;
            drawPercentageChart(chartId,chartSectionId, data[0][3], data[0][4]);
            $('.loading-section').addClass('hide');
            $('table.highchart').highchartTable({
                plotOptions: {
                    series: {
                        animation: {
                            duration: 2000
                        }
                    }
                }
            });
        }
    });
}

//==================================================
//              SYSTEMS
//==================================================
function getSystemGraphData(id) {
    //var graph_div_id = '#' + graph_id;
    var barGraphData;
    var loadingSectionId = '';
    $.ajax({
        type: "GET",
        url: "/tasks/stats/system-bar-data/" + id, success: function (data) {
            DrawChart(data[0][1], data[0][2], 'chart-' + id, data[0][0]);
            loadingSectionId = '#loading-spinner-section-' + id;
            $(loadingSectionId).addClass('hide');
            $('table.highchart').highchartTable({
                plotOptions: {
                    series: {
                        animation: {
                            duration: 2000
                        }
                    }
                }
            });
        }
    });
}


//Setup the Variables
var p1Val = 44;
var p1PerText = '46%';
var p1Colour = 'green';

var p2Val = 1;
var p2PerText = '';
var p2Colour = 'black';

var p3Val = 15;
var p3PerText = '60%';
var p3Colour = 'green';

var p4Val = 40;
var p4PerText = '';
var p4Colour = 'lightgrey';

var chartTitle = 'Upstream Process Generation';

var chartId = 'chart_div1';

var targetVal;
var actualVal;

var graphData = [];
var seriesData = [];
var options = {};

function calculateTheGraphData(targetVal, actualVal) {
    if (actualVal > targetVal) {

        p1Val = targetVal - 1;
        p1Colour = 'green';
        p1PerText = '';

        p2Val = 1;
        p2Colour = 'black';
        p2PerText = targetVal + '%';

        p3Val = actualVal - targetVal;
        p3Colour = 'green';
        p3PerText = actualVal + '%';

        p4Val = 100 - (p1Val + p2Val + p3Val);
        p4Colour = 'lightgrey';
        p4PerText = '';

    }

    if (targetVal > actualVal) {

        p1Val = actualVal;
        p1Colour = 'red';
        p1PerText = actualVal + '%';

        p2Val = (targetVal - actualVal) - 1;
        p2Colour = 'lightgrey';
        p2PerText = '';

        p3Val = 1;
        p3Colour = 'black';
        p3PerText = targetVal + '%';

        p4Val = 100 - (p1Val + p2Val + p3Val);
        p4Colour = 'lightgrey';
        p4PerText = '';
    }

    if (targetVal === actualVal) {
        p1Val = actualVal - 1;
        p1Colour = 'green';
        p1PerText = actualVal + '%';

        p2Val = 1;
        p2Colour = 'black';
        p2PerText = targetVal + '%';

        p3Val = 100 - actualVal;
        p3Colour = 'lightgrey';
        p3PerText = '';

        p4Val = 100 - (p1Val + p2Val + p3Val);
        p4Colour = 'lightgrey';
        p4PerText = '';

    }
    graphData = [
        ['Area',
            'p1', {role: 'annotation'},
            'p2', {role: 'annotation'},
            'p3', {role: 'annotation'},
            'p4', {role: 'annotation'}],
        ['', p1Val, p1PerText,
            p2Val, p2PerText,
            p3Val, p3PerText,
            p4Val, p4PerText
        ]
        // ['', 44, '45%', 1, '', 15, '60%', 40, '']
    ];
    graphData = [
        ['Area',
            'p1', {role: 'annotation'},
            'p2', {role: 'annotation'},
            'p3', {role: 'annotation'},
            'p4', {role: 'annotation'}],
        ['', p1Val, '0',
            p2Val, '',
            p3Val, '',
            p4Val, ''
        ]
        // ['', 44, '45%', 1, '', 15, '60%', 40, '']
    ];

/*    graphData = [
        ['Area',
            'p1', {role: 'annotation'}


            ],
        ['', 10, ''

        ]
        // ['', 44, '45%', 1, '', 15, '60%', 40, '']
    ];*/


    seriesData = [
        {color: p1Colour, visibleInLegend: false},
        {color: p2Colour, visibleInLegend: false},
        {color: p3Colour, visibleInLegend: false},
        {color: p4Colour, visibleInLegend: false}
    ];

    //graphData = ['', 44, '45%', 1, '', 15, '60%', 40, ''];

    options = {
        animation: {"startup": true, duration: 1000, easing: 'inAndOut'},
        chartArea: {left: 10, top: 10, width: "85%", height: "50%"},
        enableInteractivity: false,//this disables interactivity like clicking
        //tooltip: { trigger: 'selection' }, //This enables the click event andshow percentage
        series: seriesData,
        title: chartTitle,
        isStacked: 'percent',
        legend: {position: 'bottom', maxLines: 3},
        hAxis: {
            minValue: 0,
            ticks: [0, .25, .50, .75, 1]
        }
    };
}
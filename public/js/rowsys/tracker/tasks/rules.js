/**
 * Created by ronanwhelan on 15/2/16.
 */

var taskRule = 'not updated'; //placeholder for the task rule details - to populate the dropdowns on the update task rule modal


/** ===============================================================
 *
 * Show UpdateTask Rule Modal and populate the Current Values
 *
 * ===============================================================
 */
function showUpdateTaskRuleModal(id, title) {

    getTaskRuleDetailsJson(id);
    //when the AJAX call is complete populate the Dropdowns
    $(document).ajaxComplete(function (event, request, settings) {

        //set the value for the dropdowns
        var rule = JSON.parse(taskRule);
        //console.log(rule.gen_ex_applicable);
        $('#gen-applicable').val(rule.gen_ex_applicable);
        $('#rev-applicable').val(rule.rev_applicable);
        $('#re-issu-applicable').val(rule.re_issu_applicable);
        $('#s-off-applicable').val(rule.s_off_applicable);

        $('#gen-hrs').val(rule.gen_ex_hrs_perc);
        if (rule.gen_ex_applicable === '0') {
            $('#gen-hrs').prop('disabled', 'disabled');
        }
        $('#rev-hrs').val(rule.rev_hrs_perc);
        if (rule.rev_applicable === '0') {
            $('#rev-hrs').prop('disabled', 'disabled');
        }
        $('#re-issu-hrs').val(rule.re_issu_hrs_perc);
        if (rule.re_issu_applicable === '0') {
            $('#re-issu-hrs').prop('disabled', 'disabled');
        }
        $('#s-off-hrs').val(rule.s_off_hrs_perc);
        if (rule.sign_off_hrs_applicable === '0') {
            $('#s-off-hrs').prop('disabled', 'disabled');
        }

        validateUpdateRulesForm();

    });
    var row_id = '#' + id;
    $('#myModalLabel').text(title);
    $('#task-rule-edit-modal').modal('show');


}

/** ===============================================================
 *
 *  Get Task Rule Details View - for Display
 *  page is built by the server and sent to the client
 *  - jquery adds the page to the section
 *  Get the JSON for a selected task Rule used to display on a page
 * ===============================================================
 */
function getTaskRuleDetailsJson(id) {
    $.ajax({
        url: "/get-task-rule-details/" + id, success: function (result) {
            $("#task-rule-details").html(result);
        }
    });
}
/** ===============================================================
 *
 *  Validate Rule Form - when a value is changed
 *
 *  Disable and Enable Dropdowns as Needed and ensure the Rule is
 *  Validated before sent to the Server
 * ===============================================================
 */
function validateUpdateRulesForm() {

    var validationPassed = true;
    var ValidationErrorsArray = [];
    var hrsErrorsMessage = $('#hrs-error-message');
    hrsErrorsMessage.addClass('hide');
    //console.log('Form changed!');

    // ------------------------
    //  Applicable
    // ------------------------
    var genApplicable = $('#gen-applicable');
    var revApplicable = $('#rev-applicable');
    var reIssuApplicable = $('#re-issu-applicable');
    var sOffApplicable = $('#s-off-applicable');

    // ------------------------
    // Buffer Days
    // ------------------------
    var min = -1000,  max  = 1000;
    var genBufferDaysObj = $('#gen-days-buffer');
    var revBufferDaysObj = $('#rev-days-buffer');
    var reIssuBufferDaysObj = $('#re-issu-days-buffer');
    var sOffBufferDaysObj = $('#s-off-days-buffer');

    if( genApplicable.val() !== '0' && (!$.isNumeric(genBufferDaysObj.val())  || genBufferDaysObj.val() < min )){
        validationPassed = false;
        hrsErrorsMessage.removeClass('hide');
    }
    if( revApplicable.val() !== '0' && (!$.isNumeric(revBufferDaysObj.val()) || revBufferDaysObj.val() < min)){
        validationPassed = false;
        hrsErrorsMessage.removeClass('hide');
    }
    if( reIssuApplicable.val() !== '0' && (!$.isNumeric(reIssuBufferDaysObj.val()) || reIssuBufferDaysObj.val() < min)){
        validationPassed = false;
        hrsErrorsMessage.removeClass('hide');
    }
    if(reIssuApplicable.val() !== '0' && (!$.isNumeric(sOffBufferDaysObj.val())  || sOffBufferDaysObj.val() < min) ){
        validationPassed = false;
        hrsErrorsMessage.removeClass('hide');
    }
    // Total Number of Days for Task Type - not used
    //var totalDays = parseInt(genBufferDaysObj.val()) + parseInt(revBufferDaysObj.val()) + parseInt(reIssuBufferDaysObj.val()) + parseInt(sOffBufferDaysObj.val());

    // ------------------------
    // Hours Percentage
    // ------------------------
    var genHrsPerc = $('#gen-hrs');
    var revHrsPerc = $('#rev-hrs');
    var reIssuHrsPerc = $('#re-issu-hrs');
    var sOffHrsPerc = $('#s-off-hrs');
    var genHrsPercInt = parseFloat(genHrsPerc.val());
    var revHrsPercInt = parseFloat(revHrsPerc.val());
    var reIssuHrsPercInt = parseFloat(reIssuHrsPerc.val());
    var sOffHrsPercInt = parseFloat(sOffHrsPerc.val());

    //  Gen / Ex
    if (genApplicable.val() === '0') {
        genHrsPercInt = 0.0;
        genHrsPerc.prop('disabled', 'disabled').val(0);
        genBufferDaysObj.prop('disabled', 'disabled').val(0);
    } else {
        genHrsPerc.prop('disabled', false);
        genBufferDaysObj.prop('disabled', false);
    }
    //  Review
    if (revApplicable.val() === '0') {
        revHrsPercInt = 0.0;
        revHrsPerc.prop('disabled', 'disabled').val(0);
        revBufferDaysObj.prop('disabled', 'disabled').val(0);
    } else {
        revHrsPerc.prop('disabled', false);
        revBufferDaysObj.prop('disabled', false);
    }
    //  Re Issue
    if (reIssuApplicable.val() === '0') {
        reIssuHrsPercInt = 0.0;
        reIssuHrsPerc.prop('disabled', 'disabled').val(0);
        reIssuBufferDaysObj.prop('disabled', 'disabled').val(0);
    } else {
        reIssuHrsPerc.prop('disabled', false);
        reIssuBufferDaysObj.prop('disabled', false);
    }
    //  Sign Off
    if (sOffApplicable.val() === '0') {
        sOffHrsPercInt = 0.0;
        sOffHrsPerc.prop('disabled', 'disabled').val(0);
        sOffBufferDaysObj.prop('disabled', 'disabled').val(0);
    } else {
        sOffBufferDaysObj.prop('disabled', false);
        sOffHrsPerc.prop('disabled', false);
    }

    // Total Hours % - Must be equal to 100%
    var total = genHrsPercInt + revHrsPercInt + reIssuHrsPercInt + sOffHrsPercInt;
    $('#totalPercValue').text('Current Value is ' + total + '%');
    if (total !== 100) {
        $('#totalPercentWarning').removeClass('hide');
        validationPassed = false;
    } else {
        $('#totalPercentWarning').addClass('hide');
    }

    // Check thr Validation Flag
    if (! validationPassed) {
        $('#validation-errors').removeClass('hide');
        //$.('#validation-errors').removeClass('hide');
    }else{
        $('#validation-errors').addClass('hide');
    }
    console.log(validationPassed);
    return validationPassed;
}
/** ===============================================================
 *
 *  Update Task Rule
 *  uses the standard update an item form  - see below
 * ===============================================================
 */
function updateTaskRule() {
    console.log('update Task Rule');
    var validationPassed = validateUpdateRulesForm();
    console.log(validationPassed);
    //Checked if the Validation has passed
    if (validationPassed) {
        var taskRuleId = $('#edit-task-rule-id').val();
        console.log('validation passed');

        //a token is needed to prevent cross site
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        var form = $('#update-task-rule-form');
        form.find(':input').prop('disabled', false);
        var method = form.find('input[name="_method"]').val() || 'POST';
        var datastring = $(form).serialize();
        var validationSection = $('#validation-errors');
        var numOfTasks = $('#task-rule-count').val();
        console.log(numOfTasks);

        $('#save-task-rule-btn').attr('disabled', 'true');

        $.ajax({
            type: 'POST',
            url: '/post-update-task-rule' + '/' + taskRuleId,
            data: datastring,
            success: function (result) {
                $('#task-rule-edit-modal').modal('hide');
                //pleaseWait('Updating Task Rules');
                console.log(result);
                var message = '<strong>' + numOfTasks + '</strong> Tasks will be updated.' + '<br>'+ result.message;
                $.notify({
                    title: '<strong>Processing...</strong><br>',
                    message: message
                },{
                    type: 'success',
                    offset: {
                        x: 50,
                        y: 100
                    },
                    showProgressbar: false,
                    delay:5000
                });
                validationSection.addClass('hide');

            },
            beforeSend: function(){
               var ruleCount = $('#task-rule-count').val();
                var message = 'Updating ' + ruleCount +'  Task ';
                //pleaseWait(message);
                // Handle the beforeSend event

            },
            complete: function(){
                // Handle the complete event
                //setTimeout(myFunction, 3000);
                //waitDone();
                setTimeout(function(){
                    location.reload();
                },5500);


            },

            // Handles the Errors if returned back from the Server -
            // Including any Validation Errors
            error: function (xhr, status, error) {
                waitDone();
                validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
                var errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                var validationErrorText = ' Validation Errors<br>';
                var list = $("<ul class=''></ul>");
                var contactAdminText = ' - Contact the PES Administrator';

                // If there is a Validation Error then show the user the validation error Text
                if (error === 'Unprocessable Entity' || xhr.status === 422) {
                    validationSection.append(validationErrorText);
                    jQuery.each(xhr.responseJSON, function (i, val) {
                        validationErrorText = '<li>' + val + '</li>';
                        list.append(validationErrorText);
                    });
                } else {
                    errorText = errorText + contactAdminText;
                }
                validationSection.removeClass('hide').append(list).append('<br>' + errorText);
                console.log(xhr);

            }
        });

        //Validation Failed
    } else {
        //tell the user validation failed
    }
}



function multiUpdateStatus() {
    $('#multi-edit-modal').modal('show');

}

function backgroundColor(text) {
    console.log('value = ' + text);
    var notStartedColor = "#ff0000";
    var halfWayColor = "#ff0000";
    var completeColor = "lightgreen";
    var nAColor = "darkgray";
    var updateColor = "#ff0000";

    if (text === "100%") {
        updateColor = completeColor;
    }
    if (text === "N/A") {
        updateColor = nAColor;
    }


    return updateColor;
}
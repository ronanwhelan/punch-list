/**
 * Created by ronanwhelan on 15/1/16.
 */

/** ===============================================================
 *
 *   Disable an input text field from loading if the user hits the return key
 *
 * ===============================================================
 */
function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text")) {
        return false;
    }
}
document.onkeypress = stopRKey;


/** ===============================================================
 *
 *   Search Modal - Task Number on Keyup function to get list of Task Numbers over ajax
 *
 * ===============================================================
 */

function clearWorkspace() {
    $('#workspace').clear();
}

function pleaseWait(message) {
    var current_effect = 'bounce'; //
    run_waitMe(current_effect, message);
    console.log('wait me called');
}

function waitDone() {
    $('#main-content').waitMe('hide');
}

function run_waitMe(effect, message) {
    $('#main-content').waitMe({
        //http://www.jqueryscript.net/demo/jQuery-Plugin-For-Creating-Loading-Overlay-with-CSS3-Animations-waitMe/
        //none, rotateplane, stretch, orbit, roundBounce, win8,
        //win8_linear, ios, facebook, rotation, timer, pulse,
        //progressBar, bouncePulse or img
        effect: 'bouncePulse',

        //place text under the effect (string).
        text: 'Please wait ...' + message,

        //background for container (string).
        bg: 'rgba(255,255,255,0.7)',

        //color for background animation and text (string).
        color: '#000',

        //change width for elem animation (string).
        sizeW: '',

        //change height for elem animation (string).
        sizeH: '',

        // url to image
        source: 'smartadmin/img/logo/novartis_logo_3.jpg',

        // callback
        onClose: function () {
        }

    });
}
/** -----------------------------------------------------------------
 * *************  SEARCH SECTION **********************
 *
 */
/** ===============================================================
 *  Show the Search Modal
 * ===============================================================
 */
function showSearchModal(choice) {
    $.ajax({
        url: "/get-task-search-form", success: function (result) {
            $("#search-options-section").html(result);
            $('#from-date').datepicker({format: "dd/mm/yyyy"});
            var modal = $('#task-search-modal');
            if(choice === 'parameters'){
                $('#show-parameters-button').removeClass('hide');
                $('#show-task-button').addClass('hide');
            }else{
                $('#show-parameters-button').addClass('hide');
                $('#show-task-button').removeClass('hide');
            }
            modal.modal('show');
            //Modal loosing focus after inputs updated - temp fix
            if(onMobile){
                // Position modal absolute and bump it down to the scrollPosition
                modal.css({
                    position: 'absolute',
                    marginTop: $(window).scrollTop() + 'px',
                    bottom: 'auto'
                });
                // Position backdrop absolute and make it span the entire page
                //
                // Also dirty, but we need to tap into the backdrop after Boostrap
                // positions it but before transitions finish.
                //
                setTimeout( function() {
                    $('.modal-backdrop').css({
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: '100%',
                        height: Math.max(
                            document.body.scrollHeight, document.documentElement.scrollHeight,
                            document.body.offsetHeight, document.documentElement.offsetHeight,
                            document.body.clientHeight, document.documentElement.clientHeight
                        ) + 'px'
                    });
                }, 0);
            }

            $("#search-task-form").change(function () {
                if(!dropdownsResetting){
                    getSearchStats();
                }

            });
            $(".selectpicker").selectpicker();
            $(".select2").select2();
        }
    });
}

/** ===============================================================
 *  Show Task Parameters View - with Filter result
 * ===============================================================
 */
function showTaskParametersView() {
    $('#go-to-task-parameters-view').val('1');
    submitTaskSearchFromSearchModal();
    //$( "#search-task-form" ).submit();
}


/** ===============================================================
 *  Get the Search Stats for the Search Section
 * ===============================================================
 */
function getSearchStats() {

    console.log('.get search stats called');
    $('#search-stat-total').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-complete').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-complete-percent').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-past-due').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-non-recoverable').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-seven-days').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-fourteen-days').html('<i class="fa fa-spinner fa-spin"></i>');
    $('#search-stat-one-month').html('<i class="fa fa-spinner fa-spin"></i>');
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $('#search-task-form');
    var datastring = $(form).serialize();
    var stats = '';

    $.ajax({
        type: 'GET',
        data: datastring,
        url: "/get-task-search-stats", success: function (result) {
            // var data = result.stats;
            //console.log(result);
            stats = result.stats;
        },
        complete: function(){
            $('#search-stat-total').text(stats.total);
            $('#search-stat-complete').text(stats.complete);
            $('#search-stat-complete-percent').text(stats.completePercent + ' %');
            $('#search-stat-past-due').text(stats.pastDue);
            $('#search-stat-non-recoverable').text(stats.nonRecoverable);
            $('#search-stat-seven-days').text(stats.dueInSevenDays);
            $('#search-stat-fourteen-days').text(stats.dueInFourteenDays);
            $('#search-stat-one-month').text(stats.dueInFourteenDays);
            showSelectAllResultsOption(stats.total);
            setFocusOnSearchModal();

        }
    });



}
/** ===============================================================
 *  Show Task Parameters View - with Filter result
 * ===============================================================
 */
function showSelectAllResultsOption(taskCount) {
    console.log('task count is ' + taskCount);
    if(taskCount > 250){
        $('#limit-search-results-section').removeClass('hide');
    }else{
        $('#limit-search-results-section').addClass('hide');
    }
}

function submitTaskSearchFromSearchModal(){
    $('#task-search-modal').modal('hide');
    var taskCount = $('#search-stat-total').text();
    //$(this).find("button[type='submit']").text('searching..').append('<i class="fa fa-spinner fa-spin"></i>').prop('disabled',true);
    pleaseWait('searching...' + taskCount + ' tasks');
   // $('#show-task-button').button('disable');
    //$('#show-task-button').text('searching..').append('<i class="fa fa-spinner fa-spin"></i>').attr('disable','disable');
    setTimeout(function(){ $('#search-task-form').submit(); }, 3);
    //$('#show-task-button').text('searching..').append('<i class="fa fa-spinner fa-spin"></i>').attr('disable','disable');
}

/** ===============================================================
 *  Search and Return Task Table
 * ===============================================================
 */
function searchAndGotoTasksTable(formName, urlText, itemId) {
    var formId = '#' + formName;
    //a token is needed to prevent cross site
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $(formId);
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var validationSection = $('#validation-errors');

    $.ajax({
        type: 'POST',
        url: urlText + '/' + itemId,
        data: datastring,
        success: function (result) {
            console.log(result);
            $.smallBox({
                title: " Success! ",
                content: "<i class='fa fa-clock-o'></i> <i></i>" + result.message,
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 2000
            });
            validationSection.addClass('hide');
        },
        beforeSend: function () {
            // Handle the beforeSend event
            pleaseWait();
        },
        complete: function () {
            // Handle the complete event
            waitDone();
        },

        // Handles the Errors if returned back from the Server -
        // Including any Validation Errors
        error: function (xhr, status, error) {
            waitDone();
            validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
            var errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
            var validationErrorText = ' Validation Errors<br>';
            var list = $("<ul class=''></ul>");
            var contactAdminText = ' - Contact the PES Administrator';

            // If there is a Validation Error then show the user the validation error Text
            if (error === 'Unprocessable Entity' || xhr.status === 422) {
                validationSection.append(validationErrorText);
                jQuery.each(xhr.responseJSON, function (i, val) {
                    validationErrorText = '<li>' + val + '</li>';
                    list.append(validationErrorText);
                });
            } else {
                errorText = errorText + contactAdminText;
            }
            validationSection.removeClass('hide').append(list).append('<br>' + errorText);
            console.log(xhr);

        }
    });
}


/** ===============================================================
 *  Get Filtered Test Specs based on Type Selected
 * ===============================================================
 */
function getFilteredSubTypesByType() {
    if (!dropdownsResetting) {
        data = $('#search-types').val();
        var updateDropDown = $('#search-sub-types');
        updateDropDown.selectpicker('destroy');
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/get-filtered-sub-types", success: function (result) {
                updateDropDown.empty();
                //console.log(result);
                $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.id + '>' + value.name + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}

/** ===============================================================
 *  Get Filtered Down System list based on Areas Selected
 * ===============================================================
 */
function getAddTaskFilteredSystemsByArea() {
    if (!dropdownsResetting) {
        var data = $('#area').val();
        var systemsDropDown = $('#system');
        systemsDropDown.selectpicker('destroy');
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/get-filtered-systems", success: function (result) {
                systemsDropDown.empty();
                $.each(result, function (key, value) {
                    $(systemsDropDown).append('<option value=' + value.id + '>' + value.tag + ',  ' + value.description + '</option>');
                });
                systemsDropDown.selectpicker();
                setSelectPickerFonts();
            }
        });
    }
}
/** ===============================================================
 *  Get Filtered Down Task Types based on Groups Selected
 * ===============================================================
 */
function getAddTaskFilteredTypesByGroup() {
    //console.log('filtering Types');
    if (!dropdownsResetting) {
        data = $('#group').val();
        var updateDropDown = $('#type');
        updateDropDown.selectpicker('destroy');
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/get-filtered-types", success: function (result) {
                updateDropDown.empty();
                $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.id + '>' + value.short_name + ', ' + value.name + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}
/** ===============================================================
 *  Get Filtered Test Specs based on Type Selected
 * ===============================================================
 */
function getAddTaskFilteredSubTypeByType() {
    if (!dropdownsResetting) {
        data = $('#type').val();
        var updateDropDown = $('#sub-type');
        updateDropDown.selectpicker('destroy');
        $.ajax({
            type: 'GET',
            data: {data: data},
            url: "/get-filtered-sub-types", success: function (result) {
                updateDropDown.empty();
                $(updateDropDown).append('<option value=""></option>');
                $.each(result, function (key, value) {
                    $(updateDropDown).append('<option value=' + value.id + '>' + value.name + ', ' + value.description + '</option>');
                });
                updateDropDown.selectpicker();
            }
        });
    }
}
/** ===============================================================
 *  Close Select after selected
 * ===============================================================
 */


/** ===============================================================
 *  Show User Filter Modal
 * ===============================================================
 */
function showUserFilterModal() {
    var modal = $('#user-filter-modal');
    modal.modal('show');
    //Modal loosing focus after inputs updated - temp fix
    if(onMobile){
        // Position modal absolute and bump it down to the scrollPosition
        modal.css({
            position: 'absolute',
            marginTop: $(window).scrollTop() + 'px',
            bottom: 'auto'
        });
        // Position backdrop absolute and make it span the entire page
        //
        // Also dirty, but we need to tap into the backdrop after Boostrap
        // positions it but before transitions finish.
        //
        setTimeout( function() {
            $('.modal-backdrop').css({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: Math.max(
                    document.body.scrollHeight, document.documentElement.scrollHeight,
                    document.body.offsetHeight, document.documentElement.offsetHeight,
                    document.body.clientHeight, document.documentElement.clientHeight
                ) + 'px'
            });
        }, 0);
    }
    //$('#user-filter-modal').modal('show');
    $('.selectpicker').selectpicker({
        iconBase: 'fontawesome',
        tickIcon: 'fa fa-check'
    });
}




/** ===============================================================
 *  Search and Return Cards View
 * ===============================================================
 */
function showCardViewList() {
    location.href = '/tasks/cards';
}
function testDate() {

    $('#time').data("DateTimePicker").date("18:56:00");
    console.log('date picker callled');
    $('#from-date  input').datepicker({});
    $('#from-date').datepicker({
        format: "dd/mm/yyyy"
    });
}
function searchTasks() {
    //get the for details
    $('#task-search-modal').modal('show');
}

function taskTableShowSearchOptions() {
    $('#search-options').fadeIn(2500);
}

/** ===============================================================
 *
 * JSON - SEARCH FUNCTIONS
 *
 *  Functions that can search through JSON objects - https://gist.github.com/iwek/3924925
 *
 * ===============================================================
 */

//return an array of objects according to key, value, or key and value matching
function jsonSearchGetObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(jsonSearchGetObjects(obj[i], key, val));
        } else
        //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
        if (i == key && obj[i] == val || i == key && val == '') { //
            objects.push(obj);
        } else if (obj[i] == val && key == '') {
            //only add if the object is not already in the array
            if (objects.lastIndexOf(obj) == -1) {
                objects.push(obj);
            }
        }
    }
    return objects;
}

//return an array of values that match on a certain key
function jsonSearchGetValues(obj, key) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(jsonSearchGetValues(obj[i], key));
        } else if (i == key) {
            objects.push(obj[i]);
        }
    }
    return objects;
}

//return an array of keys that match on a certain value
function jsonSearchGetKeys(obj, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(jsonSearchGetKeys(obj[i], val));
        } else if (obj[i] == val) {
            objects.push(i);
        }
    }
    return objects;
}
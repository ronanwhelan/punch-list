/**
 * Created by ronanwhelan on 15/1/16.
 */
// Returns an AJAX request with the origin Terminal Details
function showCable(id) {
    $('html, body').animate({ scrollTop: 10 }, 'slow');
    $.ajax({url: "get-update-cable-details-ajax/" + id, success: function (result) {
        $("#cable_details").html(result);
    }});
}

//on key up set the value to positive
//onkeyup="this.value=this.value.replace(/[^\d]/,'')"
/** ===============================================================
 *
 *  Item Form update task rule over ajax
 *
 *  updates a new Item over Ajax and return and process any errors
 *
 * ===============================================================
 */
function updateItemFromForm(formName, urlText, itemId) {

    var formId = '#' + formName;

    //a token is needed to prevent cross site
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $(formId );
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var validationSection = $('#validation-errors');

    $.ajax({
        type: 'POST',
        url: urlText + '/' + itemId,
        data: datastring,
        success: function (result) {
            console.log(result);
            $.smallBox({
                title: " Success! ",
                content: "<i class='fa fa-clock-o'></i> <i></i>" + result.message,
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 2000
            });
            validationSection.addClass('hide');
        },
        beforeSend: function(){
            // Handle the beforeSend event
            pleaseWait();
        },
        complete: function(){
            // Handle the complete event
            waitDone();
        },

        // Handles the Errors if returned back from the Server -
        // Including any Validation Errors
        error: function (xhr, status, error) {
            waitDone();
            validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
            var errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
            var validationErrorText = ' Validation Errors<br>';
            var list = $("<ul class=''></ul>");
            var contactAdminText = ' - Contact the PES Administrator';

            // If there is a Validation Error then show the user the validation error Text
            if (error === 'Unprocessable Entity' || xhr.status === 422) {
                validationSection.append(validationErrorText);
                jQuery.each(xhr.responseJSON, function (i, val) {
                    validationErrorText = '<li>' + val + '</li>';
                    list.append(validationErrorText);
                });
            } else {
                errorText = errorText + contactAdminText;
            }
            validationSection.removeClass('hide').append(list).append('<br>' + errorText);
            console.log(xhr);

        }
    });
}
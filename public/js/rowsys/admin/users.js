/**
 * Created by ronanwhelan on 21/4/16.
 */

function getUserDetails(id){
    $.ajax({
        type: 'GET',
        url: '/admin/users/get-user-details/' + id, success: function (result) {
            $('#user-details-section').html(result);
            $(".selectpicker").selectpicker();
        }
    });
}

/** ===============================================================
 *
 *  Update a Model - from Update Modal Form
 *
 *  updates a Model over Ajax and return and process any errors
 *  used for all updates to Model - common Function
 *  The form is loaded to the update modal for each model
 *
 *  Note: Parameters are updated in the TaskController
 *
 * @param formName
 * @param urlText
 * @param itemId
 * ===============================================================
 */
function updateUserDetails(formName, urlText, userId) {
    var formId = '#' + formName;
    //a token is needed to prevent cross site
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $(formId);
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var validationSection = $('#update-user-validation-errors');
    $.ajax({
        type: 'POST',
        url: urlText,
        data: datastring,
        success: function (result) {
            console.log(result);
            $('#update-modal').modal('hide');
            $.notify({
                title: '<strong>Success!</strong><br>',
                message: result + ""
            },{
                animate: {
                    enter: 'animated fadeInLeft',
                    exit: 'animated fadeOutRight'
                },
                type: 'success',
                //offset: {x: 100, y: 100},
                //placement: {from: "bottom"},
                showProgressbar: false,
                delay:1000
            });
            setTimeout(function(){
                //window.location.reload(true);
            },1500);
            validationSection.addClass('hide');
            //getUserDetails(userId);
        },
        beforeSend: function () {
            //pleaseWait();
        },
        complete: function () {
            waitDone();
        },
        // Handles the Errors if returned back from the Server -
        // Including any Validation Errors
        error: function (xhr, status, error) {

            //waitDone();
            validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
            var errorText = '';
            var validationErrorText = ' Validation Errors';
            var list = $("<ul class=''></ul>");
            var contactAdminText = ' - Contact the PES Administrator';

            // If there is a Validation Error then show the user the validation error Text
            if (error === 'Unprocessable Entity' || xhr.status === 422) {
                //validationSection.append(validationErrorText);
                jQuery.each(xhr.responseJSON, function (i, val) {
                    validationErrorText = '<li>' + val + '</li>';
                    list.append(validationErrorText);
                    console.log(validationErrorText);
                });
            } else {
                errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                errorText = errorText + contactAdminText;
            }
            validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
        }
    });
}

//--------------------------------------------------------
//                      NEW COMPANY
//--------------------------------------------------------
function addNewCompany(formName, urlText) {
    var formId = '#' + formName;
    //a token is needed to prevent cross site
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $(formId);
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var validationSection = $('#add-company-validation-errors');
    $.ajax({
        type: 'POST',
        url: urlText,
        data: datastring,
        success: function (result) {
            console.log(result);
            $('#update-modal').modal('hide');
            $.notify({
                title: '<strong>Success!</strong><br>',
                message: result + ""
            },{
                animate: {
                    enter: 'animated fadeInLeft',
                    exit: 'animated fadeOutRight'
                },
                type: 'success',
                //offset: {x: 100, y: 100},
                //placement: {from: "bottom"},
                showProgressbar: false,
                delay:1000
            });
            setTimeout(function(){
                //window.location.reload(true);
            },1500);
            validationSection.addClass('hide');
        },
        beforeSend: function () {
            // pleaseWait();
        },
        complete: function () {
            //waitDone();
        },
        // Handles the Errors if returned back from the Server -
        // Including any Validation Errors
        error: function (xhr, status, error) {

            waitDone();
            validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
            var errorText = '';
            var validationErrorText = ' Validation Errors';
            var list = $("<ul class=''></ul>");
            var contactAdminText = ' - Contact the PES Administrator';

            // If there is a Validation Error then show the user the validation error Text
            if (error === 'Unprocessable Entity' || xhr.status === 422) {
                //validationSection.append(validationErrorText);
                jQuery.each(xhr.responseJSON, function (i, val) {
                    validationErrorText = '<li>' + val + '</li>';
                    list.append(validationErrorText);
                    console.log(validationErrorText);
                });
            } else {
                errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                errorText = errorText + contactAdminText;
            }
            validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
        }
    });
}

function getCompanyDetails(id){
    $.ajax({
        type: 'GET',
        url: '/admin/company/details/' + id, success: function (result) {
            $('#company-details-section').html(result);
            $(".selectpicker").selectpicker();
        }
    });
}
function updateCompanyDetails(formName, urlText, userId) {
    var formId = '#' + formName;
    //a token is needed to prevent cross site
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var form = $(formId);
    var method = form.find('input[name="_method"]').val() || 'POST';
    var datastring = $(form).serialize();
    var validationSection = $('#add-company-update-validation-errors');
    $.ajax({
        type: 'POST',
        url: urlText,
        data: datastring,
        success: function (result) {
            console.log(result);
            $('#update-modal').modal('hide');
            $.notify({
                title: '<strong>Success!</strong><br>',
                message: result + ""
            },{
                animate: {
                    enter: 'animated fadeInLeft',
                    exit: 'animated fadeOutRight'
                },
                type: 'success',
                //offset: {x: 100, y: 100},
                //placement: {from: "bottom"},
                showProgressbar: false,
                delay:1000
            });
            setTimeout(function(){
                //window.location.reload(true);
            },1500);
            validationSection.addClass('hide');
            getUserDetails(userId);
        },
        beforeSend: function () {
            // pleaseWait();
        },
        complete: function () {
            //waitDone();
        },
        // Handles the Errors if returned back from the Server -
        // Including any Validation Errors
        error: function (xhr, status, error) {

            waitDone();
            validationSection.removeClass('hide').html('<i class="fa-fw fa fa-info"></i> <strong>Errors</strong><br><br>');
            var errorText = '';
            var validationErrorText = ' Validation Errors';
            var list = $("<ul class=''></ul>");
            var contactAdminText = ' - Contact the PES Administrator';

            // If there is a Validation Error then show the user the validation error Text
            if (error === 'Unprocessable Entity' || xhr.status === 422) {
                //validationSection.append(validationErrorText);
                jQuery.each(xhr.responseJSON, function (i, val) {
                    validationErrorText = '<li>' + val + '</li>';
                    list.append(validationErrorText);
                    console.log(validationErrorText);
                });
            } else {
                errorText = 'Error : ' + error + ' ( Num: ' + xhr.status + ' )';
                errorText = errorText + contactAdminText;
            }
            validationSection.removeClass('hide').append(list).append('<br>' + errorText);// console.log(xhr);
        }
    });
}
